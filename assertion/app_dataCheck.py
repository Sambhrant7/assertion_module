import os
import sys
import scriptRunner
from helper import check

class runner:
    def __init__(self, campaignModule):
        self.campaignModule = campaignModule

    def start(self):
        self.performCheck()

    def performCheck(self):
        print("-"*35)
        print("Performing app_data checks:")
        app_dataList=[]
        # app_list=[]
        demo_list=[]    #empyty list
        check.blockPrint()
        xrun=scriptRunner.runner(self.campaignModule)
        for x in range(5):
            store_me=str(xrun.runScript(False))
            app_dataList.append(len(store_me))
            demo_list.append(store_me)

            # app_list.append(str(xrun.runScript(False)))
        check.enablePrint()
        check.removeTempFiles(self, '/', '/temp', '/campaign_files')
        maximum=max(set(app_dataList))
        minimum=min(set(app_dataList))

        for i in range(len(app_dataList)):
            if app_dataList[i]==maximum:
                print "\n"
                print "your maximum app_data is  "+str(demo_list[i])
                break
            else:
                print "\n"

                print "Your else minimum data is  "+str(demo_list[i])
                print "\n"  
        print "\n"
        print "values in list  "+str(app_dataList)
        print "\n"
        print "app_data is in range of "+str(minimum)+" to "+str(maximum)
        # print "yours app_data"+str(app_list)

def startAssertion(campaignName):
    getPath=os.getcwd()
    sys.path.insert(0, getPath + '/campaign_files')
    campaignModule = __import__(campaignName)
    runner(campaignModule).start()