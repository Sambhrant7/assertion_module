# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
from sdk import NameLists
import time
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from Crypto.Cipher import AES
import string
import base64


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'co.spoonme',
	'app_size'			 : 35.0,#36.0, 38.0,#25.0,#21.0,39.0
	'app_name' 			 :'Spoon Radio - Live Stream',
	'app_version_name' 	 :'4.3.1 (181)',#'4.3.0 (180)',#4.2.1 (179), '4.2.0 (178)',#'4.1.4 (177)',#4.1.3 (176), 4.1.2 (175), 4.1.1 (174), 4.1.0 (173), '4.0.4 (172)',#'4.0.3 (171)',
	'app_version_code' 	 :'181',#'180',#179, '178',#'177',#176, 174, 173, '172',#'171',
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '5.0',
	'tracker'			 : 'adjust',
	'adjust'		 : {
		'app_token'	 : 'pdq0ujqz8ge8',
		'sdk'		 : 'android4.18.0', #android4.17.0
		'secret_key' : '93519062710338863901653754841300666284',
		'secret_id'	 : '3',
		},
	'ad-brix'		 : {
		'app_key'	 	 : '24113087',
		'ver'	 	 	 : '4.5.5a',
		'key_getreferral': '24d23eff8d2b4aec24d23eff8d2b4aec',
		'iv_getreferral' : '24d23eff8d2b4aec',
		'key_tracking' 	 : 'srkterowgawrsozerruly82nfij625w9',
		'iv_tracking' 	 : 'srkterowgawrsoze',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	pushtoken(app_data)
	set_androidUUID(app_data)
	update_activity(app_data,device_data)
	get_demo_value(app_data,device_data)
	def_sec(app_data,device_data)

	email_sha1 = ""
	for i in range(0,random.randint(2,3)):
		return_userinfo(app_data)
	email_sha1 = email_sha1+util.sha1(app_data.get('user_info').get('email'))+'|'
	app_data['email_ag'] = email_sha1[:len(email_sha1)-1]
	
	################generating_realtime_differences##################
	s1=3
	s2=4
	a1=0
	a2=0
	r1=4
	r2=6
	click_time1=True
	c1=572
	c2=577
	ir1=2
	ir2=3
	click_time2=True
	in1=6
	in2=10
	app_data['app_first_open_time'] = int(time.time())
	e1=4
	e2=10


	if not app_data.get('signup_complete'):
		app_data['signup_complete']=False

 ###########	 CALLS		 ############
		
	print '\n'+'AD BRIX get Referral'
	get_referral = ad_brix_get_referral(campaign_data, app_data, device_data, app_data['app_first_open_time'],app_data['email_ag'])
	app_data['api_hit_time']=time.time()
	get_referral_result = util.execute_request(**get_referral)
	try:
		get_referral_result_decode = json.loads(get_referral_result.get('data'))
		app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
		app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))
	except:
		app_data['user_number'] = "726130493424396814"
		app_data['shard_number'] = "4"

	print '\n'+'Ad Brix Get Schedule'
	ad_brix_schedule = ad_brix_get_schedule(campaign_data, app_data, device_data)
	util.execute_request(**ad_brix_schedule)
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)
	util.execute_request(**request)

	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='install_referrer',t1=ir1,t2=ir2)
	util.execute_request(**request)

	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time1,source='reftag',t1=r1,t2=r2,t3=c1,t4=c2)
	util.execute_request(**request)

	time.sleep(random.randint(2,3))
	app_open(campaign_data, app_data, device_data,e1,e2)
		
	time.sleep(random.randint(1,2))
	print '\n'+'Ad Brix Tracking Start '
	ad_brix_tracking_1 = ad_brix_tracking_start(campaign_data, app_data, device_data, app_data['app_first_open_time'],app_data['email_ag'])
	util.execute_request(**ad_brix_tracking_1)
		
	time.sleep(random.randint(1,2))
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)
		
	time.sleep(random.randint(1,2))
	print '\n'+'Adjust : SDK INFO____________________________________'
	request=adjust_sdkinfo(campaign_data, app_data, device_data,t1=in1,t2=in2)
	util.execute_request(**request)

	time.sleep(random.randint(4,5))
	print "config call-----------"
	req = measurement_config(campaign_data, app_data, device_data)
	util.execute_request(**req)

	if random.randint(1,100)<=50:
		
		time.sleep(random.randint(1,2))
		print '\n'+"ad-brix event end"
		tracking_event_end=ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'],group='session',param='76105',activity='end')
		util.execute_request(**tracking_event_end)
		
		print '\n'+"ad-brix event start"
		tracking_event_start=ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'],group='session',param='',activity='start')
		util.execute_request(**tracking_event_start)

	options=['live', 'cast', 'talk']
	random.shuffle(options)

	for i in range(random.randint(1,3)):

		live_listening=False
		cast_listening=False

		choice=random.randint(0, len(options)-1)

		choose=options[choice]

		if choose=='live' and random.randint(1,100)<=60:
			time.sleep(random.randint(5,40))
			live_listen(campaign_data, app_data, device_data,e1,e2)
			live_listening=True

		elif choose=='cast' and random.randint(1,100)<=48:
			time.sleep(random.randint(5,30))
			cast_listen(campaign_data, app_data, device_data,e1,e2)
			cast_listening=True			

		elif choose=='talk' and random.randint(1,100)<=35:
			time.sleep(random.randint(5,30))
			talk_listen(campaign_data, app_data, device_data)

		if random.randint(1,100)<=45 and app_data.get('signup_complete')==False:
			time.sleep(random.randint(20,30))
			signup(campaign_data, app_data, device_data)
			app_data['signup_complete']=True			

		if app_data.get('signup_complete')==True :
			if cast_listening==True and random.randint(1,100)<=95:
				time.sleep(random.randint(8,10))
				cast_share(campaign_data, app_data, device_data)				

			if live_listening==True and random.randint(1,100)<=75:
				time.sleep(random.randint(8,10))
				live_share(campaign_data, app_data, device_data)		

			if random.randint(1,100)<=75 and live_listening == True:
				time.sleep(random.randint(60,300))
				like(campaign_data, app_data, device_data)
					

	if random.randint(1,100)<=55 and app_data.get('signup_complete')==True:	
		time.sleep(random.randint(60,100))
		print '\n'+"ad-brix event open_offerwall"
		tracking_event_open_offerwall=ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'],group='adpopcorn',param='5.1.7a',activity='open_offerwall')
		util.execute_request(**tracking_event_open_offerwall)

	print "\n User Demographics"
	r=ad_brix_tracking_SetUserDemographic(app_data, device_data)
	util.execute_request(**r)


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	if not app_data.get('times'):
		print "Please wait installing..."
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')


	if not app_data.get('group') or not app_data.get('activity'):
		update_activity(app_data,device_data)

	pushtoken(app_data)
	set_androidUUID(app_data)

	get_demo_value(app_data,device_data)
	def_sec(app_data,device_data)

	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())


	if not app_data.get('email_ag'):
		email_sha1 = ""
		for i in range(0,random.randint(2,3)):
			return_userinfo(app_data)
		email_sha1 = email_sha1+util.sha1(app_data.get('user_info').get('email'))+'|'
		app_data['email_ag'] = email_sha1[:len(email_sha1)-1]

	if not app_data.get('app_first_open_time'):
		app_data['app_first_open_time'] = int(time.time())

	if not app_data.get('signup_complete'):
		app_data['signup_complete']=False


	########## calls ################

	app_open(campaign_data, app_data, device_data)

	if not app_data.get('shard_number') or not app_data.get('user_number'):
		print '\n'+'AD BRIX get Referral'
		get_referral = ad_brix_get_referral(campaign_data, app_data, device_data, app_data['app_first_open_time'],app_data['email_ag'])
		get_referral_result = util.execute_request(**get_referral)
		try:
			get_referral_result_decode = json.loads(get_referral_result.get('data'))
			app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
			app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))
		except:
			app_data['user_number'] = "726130493424396814"
			app_data['shard_number'] = "4"

	time.sleep(1)
	print '\n'+'Ad Brix Tracking Start '
	ad_brix_tracking_1 = ad_brix_tracking_open(campaign_data, app_data, device_data, app_data['app_first_open_time'],app_data['email_ag'])
	util.execute_request(**ad_brix_tracking_1)

	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=1,t2=2, isOpen=True)
	util.execute_request(**request)

	time.sleep(random.randint(4,6))
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=0,t2=1)
	util.execute_request(**request)

	if random.randint(1,100)<=50:
		
		time.sleep(random.randint(1,2))
		print '\n'+"ad-brix event end"
		tracking_event_end=ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'],group='session',param='76105',activity='end')
		util.execute_request(**tracking_event_end)
		
		print '\n'+"ad-brix event start"
		tracking_event_start=ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'],group='session',param='',activity='start')
		util.execute_request(**tracking_event_start)

	options=['live', 'cast', 'talk']
	random.shuffle(options)

	for i in range(random.randint(1,3)):

		live_listening=False
		cast_listening=False

		choice=random.randint(0, len(options)-1)

		choose=options[choice]

		if choose=='live' and random.randint(1,100)<=45:
			time.sleep(random.randint(5,40))
			live_listen(campaign_data, app_data, device_data)
			live_listening=True

		elif choose=='cast' and random.randint(1,100)<=33:
			time.sleep(random.randint(5,30))
			cast_listen(campaign_data, app_data, device_data)
			cast_listening=True
			

		elif choose=='talk' and random.randint(1,100)<=20:
			time.sleep(random.randint(5,30))
			talk_listen(campaign_data, app_data, device_data)

		if random.randint(1,100)<=30 and app_data.get('signup_complete')==False:

			time.sleep(random.randint(20,30))
			signup(campaign_data, app_data, device_data)
			app_data['signup_complete']=True
			

		if app_data.get('signup_complete')==True :

			if cast_listening==True and random.randint(1,100)<=80:
				time.sleep(random.randint(8,10))
				cast_share(campaign_data, app_data, device_data)

			if live_listening==True and random.randint(1,100)<=60:
				time.sleep(random.randint(8,10))
				live_share(campaign_data, app_data, device_data)			

			if random.randint(1,100)<=60 and live_listening==True:
				time.sleep(random.randint(60,240))
				like(campaign_data, app_data, device_data)

					

	if random.randint(1,100)<=30 and app_data.get('signup_complete')==True:	
		time.sleep(random.randint(60,100))
		print '\n'+"ad-brix event open_offerwall"
		tracking_event_open_offerwall=ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['app_first_open_time'],app_data['email_ag'],group='adpopcorn',param='5.1.7a',activity='open_offerwall')
		util.execute_request(**tracking_event_open_offerwall)

	print "\n User Demographics"
	r=ad_brix_tracking_SetUserDemographic(app_data, device_data)
	util.execute_request(**r)



	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def app_open(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________86mhgk'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='86mhgk',t1=t1,t2=t2)
	util.execute_request(**request)

def live_listen(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________zcoazt'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='zcoazt',t1=t1,t2=t2)
	util.execute_request(**request)

def cast_listen(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________u4aa2j'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='u4aa2j',t1=t1,t2=t2)
	util.execute_request(**request)

def talk_listen(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________imevqs'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='imevqs',t1=t1,t2=t2)
	util.execute_request(**request)

def signup(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________9t1q5x'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='9t1q5x',t1=t1,t2=t2)
	util.execute_request(**request)

def cast_share(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________v6bzsu'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='v6bzsu',t1=t1,t2=t2)
	util.execute_request(**request)

def live_share(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________ef7mh3'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='ef7mh3',t1=t1,t2=t2)
	util.execute_request(**request)

def like(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________8qc6f2'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='8qc6f2',t1=t1,t2=t2)
	util.execute_request(**request)

###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	pushtoken(app_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')

	created_at=get_date_by_ts(app_data,device_data,ts=time.time())
	time.sleep(random.randint(t1,t2))
	sent_at=get_date_by_ts(app_data,device_data,ts=time.time())
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())

	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),
		"X-NewRelic-ID" : "VQIOUVdVCRADXVVVBAQBVA==",

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"default_tracker" : "1ix4w5",
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src" : "service",
		"hardware_name" : device_data.get('hardware'),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"tracking_enabled" : "1",
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if isOpen:
		def_(app_data,'subsession_count')
		data['created_at'] 		= get_date_by_ts(app_data,device_data,ts=time.time())
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('appCloseTime')-app_data.get('sess_len')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('appCloseTime')-app_data.get('sess_len')
		app_data['sess_len']=int(time.time())
		data['push_token']=app_data.get('pushtoken')


	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'session')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,t3=0,t4=0,callback_params=None,partner_params=None):
	pushtoken(app_data)
	reftag = ''
	if app_data.get('referrer'):
		temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
		if temp_b.get('adjust_reftag'):
			reftag = temp_b.get('adjust_reftag')[0]
	sdk_clk_time=get_date_by_ts(app_data,device_data,ts=time.time())
	time.sleep(random.randint(t3,t4))
	created_at=get_date_by_ts(app_data,device_data,ts=time.time())
	time.sleep(random.randint(t1,t2))
	sent_at=get_date_by_ts(app_data,device_data,ts=time.time())
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),
		"X-NewRelic-ID" : "VQIOUVdVCRADXVVVBAQBVA==",

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		# "default_tracker" : "1ix4w5",
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src" : "service",
		"hardware_name" : device_data.get('hardware'),
		# "install_begin_time" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time")),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"last_interval" : "0",
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"referrer" : "utm_source=(not%20set)&utm_medium=(not%20set)",
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : 1,
		"session_length" : session_length,
		"source" : source,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		if click_time:
			data['click_time']=sdk_clk_time
		elif data.get("click_time"):
			del data['click_time']
		# del data['session_length']
		# del data['subsession_count']
		# del data['time_spent']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		if click_time:
			data['click_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("click_time"))
		elif data.get("click_time"):
			del data['click_time']
		
		if data.get('raw_referrer'):
			del data['raw_referrer']
		
		data['install_begin_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time"))
		# del data['last_interval']



	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'click')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}
###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	created_at=get_date_by_ts(app_data,device_data,ts=time.time())
	time.sleep(random.randint(t1,t2))
	sent_at=get_date_by_ts(app_data,device_data,ts=time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),
		"X-NewRelic-ID" : "VQIOUVdVCRADXVVVBAQBVA==",

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"event_count" : app_data.get('event_count'),
		"event_token" : event_token,
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src" : "service",
		"hardware_name" : device_data.get('hardware'),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"session_length" : session_length,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if app_data.get('event_count')>1:
		data['push_token']=app_data.get('pushtoken')

	if app_data.get('counter')==8 or app_data.get('counter')==1:
		data['queue_size']=1
	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,t1=0,t2=0):
	inc_(app_data,'subsession_count')
	created_at=get_date_by_ts(app_data,device_data,ts=time.time())
	time.sleep(random.randint(t1,t2))
	sent_at=get_date_by_ts(app_data,device_data,ts=time.time())
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"User-Agent" : get_ua(device_data),
		"X-NewRelic-ID" : "VQIOUVdVCRADXVVVBAQBVA==",

		}
	params = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"created_at" : created_at,
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src" : "service",
		"initiated_by" : "backend",
		"needs_response_details" : "1",
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"push_token" : app_data.get('pushtoken'),
		"sent_at" : sent_at,
		"tracking_enabled" : "1",

		}
	data = {

		}

	
	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('gps_adid'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_sdkinfo(campaign_data, app_data,device_data,t1=0,t2=0):
	pushtoken(app_data)
	created_at=get_date_by_ts(app_data,device_data,ts=time.time())
	time.sleep(random.randint(t1,t2))
	sent_at=get_date_by_ts(app_data,device_data,ts=time.time())
	method = "post"
	url = 'http://app.adjust.com/sdk_info'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),
		"X-NewRelic-ID" : "VQIOUVdVCRADXVVVBAQBVA==",

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"attribution_deeplink" : "1",
		"created_at" : created_at,
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src" : "service",
		"needs_response_details" : "1",
		"push_token" : app_data.get('pushtoken'),
		"sent_at" : sent_at,
		"source" : "push",
		"tracking_enabled" : "1",

		}

	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'info')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def ad_brix_get_schedule(campaign_data, app_data, device_data):
	
	url = 'http://campaign.ad-brix.com/v1/CampaignVer2/GetSchedule'
	method ="post"

	headers = {
		"Accept-Charset" : "UTF-8",
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded; charset=utf-8",
		"User-Agent" : 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',

		}

	data = {
		"checksum" : 0,
		"co" : device_data.get('locale').get('country'),
		"google_ad_id" : device_data.get('adid'),
		"k" : campaign_data.get('ad-brix').get('app_key'),
		"la" : device_data.get('locale').get('language'),
		"os" : 'a_'+device_data.get('os_version'),
		"puid" : '',
		"version" : 'a_'+device_data.get('os_version'),

		}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': data}	

def ad_brix_get_referral(campaign_data, app_data, device_data,app_first_open_time_1,email_ag_1):

	url = 'http://cvr.ad-brix.com/v1/conversion/GetReferral'
	method ="post"

	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = int(temp_a['sn'][0])
		else:
			session_number_value = random.randint(11111111111,99999999999)
		
		if temp_a.get('ck'):
			conversion_key_value = int(temp_a['ck'][0])
		else:
			conversion_key_value = random.randint(1111111,9999999)
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = random.randint(1111111,9999999)
		session_number_value = random.randint(11111111111,99999999999)
		
	
	headers = {
		"Accept-Charset" : "UTF-8",
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded; charset=utf-8",
		"User-Agent" : 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',

		}

	data = {
		"k" : campaign_data.get('ad-brix').get('app_key'),

		}
	data_value = {
		"activity_info" : [],
		"adbrix_user_info" : {	"adbrix_user_no" : -1,
			"app_launch_count" : 1,
			"install_datetime" : "",
		"install_mdatetime" : int(time.time()*1000),
		"last_referral_data" : "",
			"last_referral_datetime" : "",
			"last_referral_key" : -1,
			"life_hour" : -1,
			"reengagement_conversion_key" : -1,
		"referral_key" : conversion_key_value,
		"set_referral_key" : True,
			"shard_no" : -1,
			"sig_type" : 0,
		},
		"app_version_code" : eval(campaign_data.get('app_version_code')),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appkey" : campaign_data.get('ad-brix').get('app_key'),
		"cohort_info" : {},
		"complete_conversions" : [],
		"conversion_cache" : [],
		"demographics" : [],
		"device_info" : {"build_id" : device_data.get('build'),
	"height" : int(device_data.get('resolution').split('x')[0]),
		"is_portrait" : True,
			"is_wifi_only" : False,
		"kn" : device_data.get('kernel'),
	"model" : device_data.get('model'),
		"network" : "wifi",
		"noncustomnetwork" : int(device_data.get('mnc')),
	"os" : "a_"+device_data.get('os_version'),
		"ptype" : "android",
		"utc_offset" : float(app_data.get('sec'))/float(60)/float(60),
		"vendor" : "google",
		"width" : int(device_data.get('resolution').split('x')[1]),
	},
		"impression_info" : [],
		"installation_info" : {	"install_actions_timestamp" : {	"app_first_open" : int(app_first_open_time_1),	"app_install_completed" : int(app_data.get('times').get('install_complete_time')),	"app_install_start" : int(app_data.get('times').get('download_end_time')),	"market_install_btn_clicked" : int(app_data.get('times').get('click_time')),},
	"market_referrer" : urlparse.unquote(app_data.get('referrer')),
	},
		"installer" : "com.android.vending",
		"package_name" : campaign_data.get('package_name'),
		"referral_info" : {"conversion_key" : conversion_key_value,
	"referrer_param" : app_data.get('referrer'),
	"session_no" : session_number_value,
	},
		"request_info" : {"client_timestamp" : int(time.time()),
	},
		"user_info" : {"ag" : email_ag_1,
	"android_id" : base64.b64encode(device_data.get('android_id')),
	"android_id_md5" : util.md5(device_data.get('android_id')),
	"android_id_sha1" : util.sha1(device_data.get('android_id')),
	"carrier" : device_data.get('carrier'),
	"country" : device_data.get('locale').get('country'),
	"device_id_md5" : "",
	"device_id_sha1" : "",
	"first_install_time" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y%m%d%H%M%S'),
	"first_install_time_offset" : app_data.get('sec'),
	"google_ad_id" : device_data.get('adid'),
		"google_ad_id_opt_out" : False,
		"initial_ad_id" : device_data.get('adid'),
	"language" : device_data.get('locale').get('language'),
	"mudid" : "",
	"odin" : util.sha1(device_data.get('android_id')),
	"openudid" : "",
	"openudid_md5" : "",
	"openudid_sha1" : "",
	"puid" : "",
	},
		"version" : campaign_data.get('ad-brix').get('ver'),
	
		}

	key = campaign_data.get('ad-brix').get('key_getreferral')
	iv = campaign_data.get('ad-brix').get('iv_getreferral')
	aes = AESCipher(key)
	data['j'] = aes.encrypt(iv,json.dumps(data_value))
	
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}

def ad_brix_tracking_open(campaign_data, app_data, device_data,app_first_open_time_1,email_ag_1):
	get_demo_value(app_data,device_data)
	url = 'http://tracking.ad-brix.com/v1/tracking'
	method ="post"

	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = int(temp_a['sn'][0])
		else:
			session_number_value = random.randint(11111111111,99999999999)
		
		if temp_a.get('ck'):
			conversion_key_value = int(temp_a['ck'][0])
		else:
			conversion_key_value = random.randint(1111111,9999999)
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = random.randint(1111111,9999999)
		session_number_value = random.randint(11111111111,99999999999)
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)

	
	headers = {
		"Accept-Charset" : "UTF-8",
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded; charset=utf-8",
		"User-Agent" : 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',

		}

	data = {
		"k" : campaign_data.get('ad-brix').get('app_key'),

		}

	params=util.get_random_string('decimal',7)

	event_date_1 = get_date(app_data,device_data,para1=0,para2=0)
	event_date = get_date(app_data,device_data,para1=1,para2=2)
	data_value = {
		"activity_info" : [{"prev_group":app_data.get('group'),"prev_activity":app_data.get('activity'),"group":"session","activity":"end","param":params,"event_id":str(uuid.uuid4()),"created_at":event_date},{"prev_group":"","prev_activity":"","group":"session","activity":"retention","param":"","event_id":str(uuid.uuid4()),"created_at":event_date},{"prev_group":"","prev_activity":"","group":"session","activity":"start","param":"","event_id":str(uuid.uuid4()),"created_at":event_date}],
		"adbrix_user_info" : {"adbrix_user_no" : int(app_data.get('user_number')),"app_launch_count" : 1,"install_datetime" : event_date_1,"install_mdatetime" : int(time.time()*1000),"last_referral_data" : "","last_referral_datetime" : "","last_referral_key" : -1,"life_hour" : 0,"reengagement_conversion_key" : -1,"referral_key" : conversion_key_value,"set_referral_key" : True,"shard_no" : int(app_data.get('shard_number')),"sig_type" : 0,},
		"app_version_code" : eval(campaign_data.get('app_version_code')),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appkey" : campaign_data.get('ad-brix').get('app_key'),
		"cohort_info" : {},
		"complete_conversions" : [],
		"conversion_cache" : [],
		"demographics" : [{"demo_key":"userId","demo_value":app_data.get('demo_value')}],
		"device_info" : {"build_id" : device_data.get('build'),"height" : int(device_data.get('resolution').split('x')[0]),"is_portrait" : False,"is_wifi_only" : False,"kn" : device_data.get('kernel'),"model" : device_data.get('model'),"network" : "wifi","noncustomnetwork" : int(device_data.get('mnc')),"os" : "a_"+device_data.get('os_version'),"ptype" : "android","utc_offset" : float(app_data.get('sec'))/float(60)/float(60),"vendor" : "google","width" : int(device_data.get('resolution').split('x')[1]),},
		"impression_info" : [],
		"installation_info" : {"install_actions_timestamp" : {"market_install_btn_clicked" : int(app_data.get('times').get('click_time')),"app_install_start" : int(app_data.get('times').get('download_end_time')),"app_install_completed" : int(app_data.get('times').get('install_complete_time')),"app_first_open" : int(app_first_open_time_1),},"market_referrer" : urlparse.unquote(app_data.get('referrer')),},
		"installer" : "com.android.vending",
		"package_name" : campaign_data.get('package_name'),
		"referral_info" : {"conversion_key" : conversion_key_value,"referrer_param" : app_data.get('referrer'),"session_no" : session_number_value,},
		"request_info" : {"client_timestamp" : int(time.time()),},
		"user_info" : {"ag" : email_ag_1,"android_id" : base64.b64encode(device_data.get('android_id')),"android_id_md5" : util.md5(device_data.get('android_id')),"android_id_sha1" : util.sha1(device_data.get('android_id')),"carrier" : device_data.get('carrier'),"country" : 'KR',"device_id_md5" : "","device_id_sha1" : "","first_install_time" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y%m%d%H%M%S'),"first_install_time_offset" : app_data.get('sec'),"google_ad_id" : device_data.get('adid'),"google_ad_id_opt_out" : False,"initial_ad_id" : device_data.get('adid'),"language" : 'ko',"mudid" : "","odin" : util.sha1(device_data.get('android_id')),"openudid" : "","openudid_md5" : "","openudid_sha1" : "","puid" : "",},
		"version" : campaign_data.get('ad-brix').get('ver'),
	
		}

	print "--------------------------"
	print data_value
	print "--------------------------"

	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,json.dumps(data_value))
	
	update_activity(app_data,device_data,'session','start')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}	


		
def ad_brix_tracking_start(campaign_data, app_data, device_data,app_first_open_time_1,email_ag_1):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	method ="post"

	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = int(temp_a['sn'][0])
		else:
			session_number_value = random.randint(11111111111,99999999999)
		
		if temp_a.get('ck'):
			conversion_key_value = int(temp_a['ck'][0])
		else:
			conversion_key_value = random.randint(1111111,9999999)
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = random.randint(1111111,9999999)
		session_number_value = random.randint(11111111111,99999999999)
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)

	
	headers = {
		"Accept-Charset" : "UTF-8",
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded; charset=utf-8",
		"User-Agent" : 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		"X-NewRelic-ID" : "VQIOUVdVCRADXVVVBAQBVA==",

		}

	data = {
		"k" : campaign_data.get('ad-brix').get('app_key'),

		}

	event_date_1 = get_date(app_data,device_data,para1=0,para2=0)
	event_date = get_date(app_data,device_data,para1=1,para2=2)
	data_value = {
		"activity_info" : [{"activity":"retention","created_at" : event_date,"event_id":"4155ba3d-7189-4119-8da2-32577e1cb1b4","group":"session","param":"","prev_activity":"","prev_group":"",},{"activity":"start","created_at" : event_date,"event_id":"3069ac64-e81b-4f1c-aaf0-65c893fbf7e2","group":"session","param":"","prev_activity":"","prev_group":"",},],
		"adbrix_user_info" : {"adbrix_user_no" : int(app_data.get('user_number')),
		"app_launch_count" : 1,
		"install_datetime" : event_date_1,
	"install_mdatetime" : int(time.time()*1000),
	"last_referral_data" : "",
	"last_referral_datetime" : "",
		"last_referral_key" : -1,
			"life_hour" : 0,
			"reengagement_conversion_key" : -1,
		"referral_key" : conversion_key_value,
		"set_referral_key" : True,
		"shard_no" : int(app_data.get('shard_number')),
		"sig_type" : 0,
		},
		"app_version_code" : eval(campaign_data.get('app_version_code')),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appkey" : campaign_data.get('ad-brix').get('app_key'),
		"cohort_info" : {},
		"complete_conversions" : [],
		"conversion_cache" : [],
		"demographics" : [],
		"device_info" : {"build_id" : device_data.get('build'),
	"height" : int(device_data.get('resolution').split('x')[0]),
		"is_portrait" : True,
			"is_wifi_only" : False,
		"kn" : device_data.get('kernel'),
	"model" : device_data.get('model'),
		"network" : "wifi",
		"noncustomnetwork" : int(device_data.get('mnc')),
	"os" : "a_"+device_data.get('os_version'),
		"ptype" : "android",
		"utc_offset" : float(app_data.get('sec'))/float(60)/float(60),
		"vendor" : "google",
		"width" : int(device_data.get('resolution').split('x')[1]),
	},
		"impression_info" : [],
		"installation_info" : {	"install_actions_timestamp" : {	"app_first_open" : int(app_first_open_time_1),	"app_install_completed" : int(app_data.get('times').get('install_complete_time')),	"app_install_start" : int(app_data.get('times').get('download_end_time')),	"market_install_btn_clicked" : int(app_data.get('times').get('click_time')),},
	"market_referrer" : urlparse.unquote(app_data.get('referrer')),
	},
		"installer" : "com.android.vending",
		"package_name" : campaign_data.get('package_name'),
		"referral_info" : {"conversion_key" : conversion_key_value,
	"referrer_param" : app_data.get('referrer'),
	"session_no" : session_number_value,
	},
		"request_info" : {"client_timestamp" : int(time.time()),
	},
		"user_info" : {"ag" : email_ag_1,
	"android_id" : base64.b64encode(device_data.get('android_id')),
	"android_id_md5" : util.md5(device_data.get('android_id')),
	"android_id_sha1" : util.sha1(device_data.get('android_id')),
	"carrier" : device_data.get('carrier'),
	"country" : device_data.get('locale').get('country'),
	"device_id_md5" : "",
	"device_id_sha1" : "",
	"first_install_time" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y%m%d%H%M%S'),
	"first_install_time_offset" : app_data.get('sec'),
	"google_ad_id" : device_data.get('adid'),
		"google_ad_id_opt_out" : False,
		"initial_ad_id" : device_data.get('adid'),
	"language" : device_data.get('locale').get('language'),
	"mudid" : "",
	"odin" : util.sha1(device_data.get('android_id')),
	"openudid" : "",
	"openudid_md5" : "",
	"openudid_sha1" : "",
	"puid" : "",
	},
		"version" : campaign_data.get('ad-brix').get('ver'),
	
		}

	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,json.dumps(data_value))
	
	update_activity(app_data,device_data,'session','start')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


def ad_brix_tracking_event(campaign_data, app_data, device_data,app_first_open_time_1,email_ag_1,group='',param='',activity=''):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	method ="post"

	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = int(temp_a['sn'][0])
		else:
			session_number_value = random.randint(11111111111,99999999999)
		
		if temp_a.get('ck'):
			conversion_key_value = int(temp_a['ck'][0])
		else:
			conversion_key_value = random.randint(1111111,9999999)
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = random.randint(1111111,9999999)
		session_number_value = random.randint(11111111111,99999999999)
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	
	headers = {
		"Accept-Charset" : "UTF-8",
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded; charset=utf-8",
		"Cookie" : "language-type=en",
		"User-Agent" : 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		"X-NewRelic-ID" : "VQIOUVdVCRADXVVVBAQBVA==",

		}

	data = {
		"k" : campaign_data.get('ad-brix').get('app_key'),

		}

	event_date_1 = get_date(app_data,device_data,para1=0,para2=0)
	event_date = get_date(app_data,device_data,para1=60,para2=100)
	data_value = {
		"activity_info" : [{"activity" : activity,"created_at" : event_date,"event_id" : str(uuid.uuid4()),"group" : group,"param" : param,"prev_activity" : app_data.get('activity'),"prev_group" : app_data.get('group'),}],
		"adbrix_user_info" : {"adbrix_user_no" : int(app_data.get('user_number')),
		"app_launch_count" : 2,
		"install_datetime" : event_date_1,
	"install_mdatetime" : int(time.time()*1000),
	"last_referral_data" : "",
	"last_referral_datetime" : "",
		"last_referral_key" : -1,
			"life_hour" : 0,
			"reengagement_conversion_key" : -1,
		"referral_key" : conversion_key_value,
		"set_referral_key" : True,
		"shard_no" : int(app_data.get('shard_number')),
		"sig_type" : 0,
		},
		"app_version_code" : eval(campaign_data.get('app_version_code')),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appkey" : campaign_data.get('ad-brix').get('app_key'),
		"cohort_info" : {},
		"complete_conversions" : [],
		"conversion_cache" : [],
		"demographics" : [],
		"device_info" : {"build_id" : device_data.get('build'),
	"height" : int(device_data.get('resolution').split('x')[0]),
		"is_portrait" : True,
			"is_wifi_only" : False,
		"kn" : device_data.get('kernel'),
	"model" : device_data.get('model'),
		"network" : "wifi",
		"noncustomnetwork" : int(device_data.get('mnc')),
	"os" : "a_"+device_data.get('os_version'),
		"ptype" : "android",
		"utc_offset" : float(app_data.get('sec'))/float(60)/float(60),
		"vendor" : "google",
		"width" : int(device_data.get('resolution').split('x')[1]),
	},
		"impression_info" : [],
		"installation_info" : {	"install_actions_timestamp" : {	"app_first_open" : int(app_first_open_time_1),	"app_install_completed" : int(app_data.get('times').get('install_complete_time')),	"app_install_start" : int(app_data.get('times').get('download_end_time')),	"market_install_btn_clicked" : int(app_data.get('times').get('click_time')),},
	"market_referrer" : urlparse.unquote(app_data.get('referrer')),
	},
		"installer" : "com.android.vending",
		"package_name" : campaign_data.get('package_name'),
		"referral_info" : {"conversion_key" : conversion_key_value,
	"referrer_param" : app_data.get('referrer'),
	"session_no" : session_number_value,
	},
		"request_info" : {"client_timestamp" : int(time.time()),
	},
		"user_info" : {"ag" : email_ag_1,
	"android_id" : base64.b64encode(device_data.get('android_id')),
	"android_id_md5" : util.md5(device_data.get('android_id')),
	"android_id_sha1" : util.sha1(device_data.get('android_id')),
	"carrier" : device_data.get('carrier'),
	"country" : device_data.get('locale').get('country'),
	"device_id_md5" : "",
	"device_id_sha1" : "",
	"first_install_time" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y%m%d%H%M%S'),
	"first_install_time_offset" : app_data.get('sec'),
	"google_ad_id" : device_data.get('adid'),
		"google_ad_id_opt_out" : False,
		"initial_ad_id" : device_data.get('adid'),
	"language" : device_data.get('locale').get('language'),
	"mudid" : "",
	"odin" : util.sha1(device_data.get('android_id')),
	"openudid" : "",
	"openudid_md5" : "",
	"openudid_sha1" : "",
	"puid" : "",
	},
		"version" : campaign_data.get('ad-brix').get('ver'),
	
		}

	if app_data.get('signup_complete')==True:
		data_value['demographics']=[{       "demo_key": "userId","demo_value": app_data.get('demo_value')}]


	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,json.dumps(data_value))

	update_activity(app_data,device_data,group,activity)
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}

def ad_brix_tracking_SetUserDemographic(app_data, device_data):
	get_demo_value(app_data,device_data)

	url = 'http://tracking.ad-brix.com/v1/tracking/SetUserDemographic'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	# referrer_additional_info = ""
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value = {"puid":"","google_ad_id":device_data.get('adid'),"user_demo_info":[{"demo_key":"userId","demo_value":app_data.get('demo_value')}]}	
	
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,json.dumps(data_value))
	
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}		

def measurement_config(campaign_data, app_data, device_data):

	url = 'http://app-measurement.com/config/app/1%3A108800953922%3Aandroid%3A5cae721a19acb31d'
	
	header={
		'User-Agent': get_ua(device_data),
		'Accept-Encoding': 'gzip',
	}
		
	params={
		'app_instance_id':	'676728507ca0a7a09bad394a963397d4',
		'platform':	'android',
		'gmp_version':	'17785',
	}
	data=None

	return {'url': url, 'httpmethod': 'head', 'headers': header, 'params': params, 'data': data}

class AESCipher(object):

    def __init__(self, key): 
        self.bs = 16
        self.key = key.encode()
		
    def encrypt(self, iv,raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')
        
    def decrypt(self, enc):
        # enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

class AESCipherTracking(object):

    def __init__(self, key): 
        self.bs = 16
        self.key = key.encode()
		
    def encrypt(self, iv,raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')
        
    def decrypt(self, enc):
        # enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]	

def def_gcmToken(app_data):
	if not app_data.get('gcm'):
		app_data['gcm']='APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def update_activity(app_data,device_data,group='',activity=''):
	app_data['group']=group
	app_data['activity']=activity
	
def get_demo_value(app_data,device_data):
	if not app_data.get('demo_value'):
		app_data['demo_value']='438'+util.get_random_string('decimal',4)
	
def return_userinfo(app_data):
	
	# gender_n = app_data.get('gender')
	# app_data['gender'] = random.choice(['male', 'female'])
	# gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	user_data = util.generate_name(gender=random.choice(['male', 'female']))
	user_info = {}
	# user_info['username'] = user_data.get('username').lower()
	user_info['email'] = user_data.get('username').lower() + '@gmail.com'
	# user_info['f_name'] = user_data.get('firstname')
	# user_info['l_name'] = user_data.get('lastname')
	# user_info['sex'] = gender_n
	# user_info['gender'] = str(1) if gender_n == 'female' else str(2)
	# user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
	# user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
	# user_info['password'] = util.get_random_string(type='all',size=16)
	app_data['user_info'] = user_info	
	
	return app_data.get('user_info')

def get_date(app_data,device_data,para1=0,para2=0):
	time.sleep(random.randint(para1,para2))
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y%m%d%H%M%S")
	return date

def update_id(app_data,device_data,log_id=''):
	app_data['log_id']=log_id




###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	return app_data.get('android_uuid')


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']='f0L4WFS5AEk:'+util.get_random_string('google_token',140)
	return app_data.get('pushtoken')

def get_auth(device_data,app_data,created_at,gps_adid,activity_type):
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activity_type)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'

def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"