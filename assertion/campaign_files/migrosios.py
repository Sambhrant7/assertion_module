# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
import time
import random
import json
import datetime
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.technoface.migros',
	'app_size'			 : 107.3,
	'app_name' 			 : 'Migros',
	'app_version_name' 	 : '9.6', #9.5
	'app_version_code' 	 : '990', #980
	'app_id' 			 : '529488079',
	'ctr' 				 : 1,
	'sdk' 		 		 : 'ios',
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '11.0',
	'tracker'		 	 : 'branch',
	'branch':{
								'key':'key_live_joMcMp0nelwYSwkEwsKR4eheBri5p04d',
								'sdk': 'ios0.28.1',
							},	
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	

	###########		INITIALIZE		############	

	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')
	
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')

	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')


 	###########	 CALLS		 ############

	print "Branch Install"
	branchInstall = branch_install(campaign_data, app_data, device_data)
	app_data['api_hit_time']=time.time()
	branchInstallRequest = util.execute_request(**branchInstall)
	try:
		response = json.loads(branchInstallRequest.get('data'))
		app_data['device_fingerprint_id1'] = response.get('device_fingerprint_id')
		app_data['identity_id'] = response.get('identity_id')
		app_data['session_id'] = response.get('session_id')

		if not app_data.get('device_fingerprint_id1'):
			app_data['device_fingerprint_id1']='680031755927107292'

		if not app_data.get('identity_id'):
			app_data['identity_id']='680031755968950241'

		if not app_data.get('session_id'):
			app_data['session_id']='680031755973486670'

	except:
		print 'could not fetch data from branch response'
		app_data['device_fingerprint_id1']='680031755927107292'
		app_data['identity_id']='680031755968950241'
		app_data['session_id']='680031755973486670'

	print "Branch Sdk"
	request=branch_uriski(campaign_data, app_data, device_data)
	util.execute_request(**request)


	if random.randint(1,100) <= 90:

		time.sleep(random.randint(10,15))
		print "\nBranch custom event GirisSayfasi \n"
		request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='GirisSayfasi',instrumentation={"/v1/install-brtt":"1365"})
		util.execute_request(**request)

		if random.randint(1,100) <= 40:
			time.sleep(random.randint(6,8))
			print "\nBranch custom event Migroskop_GirisSayfasi \n"
			request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='Migroskop_GirisSayfasi',instrumentation={"/v2/event/custom-brtt":"409"})
			util.execute_request(**request)

			if random.randint(1,100) <= 50:

				time.sleep(random.randint(4,6))
				print "\nBranch custom event Migroskop_SolaSwipe_GirisSayfasi \n"
				request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='Migroskop_SolaSwipe_GirisSayfasi',instrumentation={"/v2/event/custom-brtt":"898"})
				util.execute_request(**request)


		if random.randint(1,100) <= 95:
			time.sleep(random.randint(5,10))
			print "\nBranch custom event GirisSayfasi_GSMGiris_DevamEt \n"
			request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='GirisSayfasi_GSMGiris_DevamEt',instrumentation={"/v2/event/custom-brtt":"258"})
			util.execute_request(**request)	


			if random.randint(1,100) <= 95:
				time.sleep(random.randint(5,10))
				print "\nBranch custom event KayitSayfasi_Bitir \n"
				request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='KayitSayfasi_Bitir',instrumentation={"/v2/event/custom-brtt":"448"})
				util.execute_request(**request)	


				if random.randint(1,100) <= 97:
					time.sleep(random.randint(5,10))
					print "\nBranch custom event KayitSayfasi_OTPSifreGiris \n"
					request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='KayitSayfasi_OTPSifreGiris',instrumentation={"/v2/event/custom-brtt":"519"})
					util.execute_request(**request)	


					if random.randint(1,100) <= 60 and not app_data.get('login_success'):
						time.sleep(random.randint(5,10))
						print "\nBranch custom event GirisSayfasi_OTPSifreGiris \n"
						request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='GirisSayfasi_OTPSifreGiris',instrumentation={"/v2/event/custom-brtt":"458"})
						util.execute_request(**request)	

						app_data['login_success'] = True


		if random.randint(1,100) <= 50:

			time.sleep(random.randint(5,10))
			print "Branch Close"
			request=branch_close(campaign_data, app_data, device_data)
			util.execute_request(**request)

			time.sleep(random.randint(10,20))
			print 'Branch open'
			request = branch_open(campaign_data, app_data, device_data)
			util.execute_request(**request)


		for _ in range(random.randint(0,3)):

			time.sleep(random.randint(6,8))
			print "\nBranch custom event Migroskop_GirisSayfasi \n"
			request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='Migroskop_GirisSayfasi',instrumentation={"/v2/event/custom-brtt":"409"})
			util.execute_request(**request)


			for _ in range(random.randint(1,3)):

				time.sleep(random.randint(4,6))
				print "\nBranch custom event Migroskop_SolaSwipe_GirisSayfasi \n"
				request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='Migroskop_SolaSwipe_GirisSayfasi',instrumentation={"/v2/event/custom-brtt":"898"})
				util.execute_request(**request)

	time.sleep(random.randint(5,10))
	print "Branch Close"
	request=branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)

	
	return {'status':'true'}

#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):


	###########		INITIALIZE		############	

	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')
	
	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')

	if not app_data.get('idfv_id'):
		if not device_data.get('idfv_id'):
			app_data['idfv_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfv_id'] = device_data.get('idfv_id')


 	###########	 CALLS		 ############

 	if not app_data.get('device_fingerprint_id1') or not app_data.get('identity_id') or not app_data.get('session_id'): 
	 	print "Branch Install"
		branchInstall = branch_install(campaign_data, app_data, device_data)
		branchInstallRequest = util.execute_request(**branchInstall)
		try:
			response = json.loads(branchInstallRequest.get('data'))
			app_data['device_fingerprint_id1'] = response.get('device_fingerprint_id')
			app_data['identity_id'] = response.get('identity_id')
			app_data['session_id'] = response.get('session_id')

			if not app_data.get('device_fingerprint_id1'):
				app_data['device_fingerprint_id1']='680031755927107292'

			if not app_data.get('identity_id'):
				app_data['identity_id']='680031755968950241'

			if not app_data.get('session_id'):
				app_data['session_id']='680031755973486670'

		except:
			print 'could not fetch data from branch response'
			app_data['device_fingerprint_id1']='680031755927107292'
			app_data['identity_id']='680031755968950241'
			app_data['session_id']='680031755973486670'


	print 'Branch open'
	request = branch_open(campaign_data, app_data, device_data)
	util.execute_request(**request)


	for _ in range(random.randint(0,3)):

		time.sleep(random.randint(6,8))
		print "\nBranch custom event Migroskop_GirisSayfasi \n"
		request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='Migroskop_GirisSayfasi',instrumentation={"/v2/event/custom-brtt":"576"})
		util.execute_request(**request)


		for _ in range(random.randint(1,3)):

			time.sleep(random.randint(4,6))
			print "\nBranch custom event Migroskop_SolaSwipe_GirisSayfasi \n"
			request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='Migroskop_SolaSwipe_GirisSayfasi',instrumentation={"/v2/event/custom-brtt":"754"})
			util.execute_request(**request)


	if random.randint(1,100) <= 50:

		time.sleep(random.randint(5,10))
		print "Branch Close"
		request=branch_close(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(10,20))
		print 'Branch open'
		request = branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)


	if random.randint(1,100) <= 15 and not app_data.get('login_success'):
		time.sleep(random.randint(5,10))
		print "\nBranch custom event GirisSayfasi_GSMGiris_DevamEt \n"
		request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='GirisSayfasi_GSMGiris_DevamEt',instrumentation={"/v2/event/custom-brtt":"362"})
		util.execute_request(**request)	


		if random.randint(1,100) <= 90:
			time.sleep(random.randint(5,10))
			print "\nBranch custom event KayitSayfasi_Bitir \n"
			request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='KayitSayfasi_Bitir',instrumentation={"/v2/event/custom-brtt":"965"})
			util.execute_request(**request)	


			if random.randint(1,100) <= 90:
				time.sleep(random.randint(5,10))
				print "\nBranch custom event KayitSayfasi_OTPSifreGiris \n"
				request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='KayitSayfasi_OTPSifreGiris',instrumentation={"/v2/event/custom-brtt":"452"})
				util.execute_request(**request)	


				if random.randint(1,100) <= 60 and not app_data.get('login_success'):
					time.sleep(random.randint(5,10))
					print "\nBranch custom event GirisSayfasi_OTPSifreGiris \n"
					request=branch_custom_event( campaign_data, device_data, app_data ,eventname ='GirisSayfasi_OTPSifreGiris',instrumentation={"/v2/event/custom-brtt":"458"})
					util.execute_request(**request)	

					app_data['login_success'] = True


	time.sleep(random.randint(5,10))
	print "Branch Close"
	request=branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)


	return {'status':'true'}


###################################
#		Branch_install()
###################################

def branch_install(campaign_data, app_data, device_data):
	method = 'post'
	url='http://api2.branch.io/v1/install'
	header={
			'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
			'Content-Type': 'application/json',
			"Accept-Encoding": "br, gzip, deflate",
			"Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),

			}
	params={}
	data={
			"sdk":campaign_data.get('branch').get('sdk'),
			"facebook_app_link_checked":False,
			"language":device_data.get('locale').get('language'),
			"user_agent":device_data.get('User-Agent'),
			"screen_height":int(device_data.get('resolution').split('x')[1]),
			"country":device_data.get('locale').get('country'),
			"app_version":campaign_data.get('app_version_name'),
			"update":0,
			"ad_tracking_enabled":True,
			"brand":"Apple",
			"retryNumber":0,
			"os":"iOS",
			"hardware_id":app_data.get('idfv_id'),
			"ios_vendor_id":app_data.get('idfa_id'),
			"ios_bundle_id":campaign_data.get('package_name'),
			"screen_width":int(device_data.get('resolution').split('x')[0]),
			"hardware_id_type":"idfa",
			"debug":False,
			"model":device_data.get('device_platform'),
			"os_version":device_data.get('os_version'),
			"is_hardware_id_real":True,
			"branch_key":campaign_data.get('branch').get('key'),
			"apple_ad_attribution_checked":False,
			"first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
			"latest_install_time":int(app_data.get('times').get('install_complete_time')*1000),
			"local_ip": device_data.get('private_ip'),
			"lastest_update_time":int(app_data.get('times').get('install_complete_time')*1000),
			'apple_receipt' : install_receipt(),
			'apple_testflight':False,
			'uri_scheme':'migrosmagazam',

			}
	return {'url':url, 'httpmethod':method, 'headers':header, 'params': None, 'data': json.dumps(data)}


###################################
#		Branch_uriski()
###################################

def branch_uriski(campaign_data, app_data, device_data):
	url= "http://cdn.branch.io/sdk/uriskiplist_v0.json"
	method= "get"
	headers= {       
        "Accept-Encoding": "br, gzip, deflate",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "User-Agent": campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
        }

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def branch_custom_event( campaign_data, device_data, app_data ,eventname ='',instrumentation=''):
	url= "http://api2.branch.io/v2/event/custom"
	method= "post"
	headers= {       "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "Content-Type": "application/json",
        "User-Agent": campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],}

	params= None

	data= {       "branch_key": campaign_data.get('branch').get('key'),
        "custom_data": {       "screenName": eventname},
        "event_data": {       },
        "instrumentation": instrumentation,
        "name": eventname,
        "retryNumber": 0,
        "user_data": {       "app_version": campaign_data.get('app_version_name'),
                             "brand": "Apple",
                             "country":device_data.get('locale').get('country'),
                             "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
                             "environment": "FULL_APP",
                             "idfa": app_data.get('idfa_id'),
                             "idfv": app_data.get('idfv_id'),
                             "language": device_data.get('locale').get('language'),
                             "local_ip": device_data.get('private_ip'),
                             "model": device_data.get('device_platform'),
                             "os": "iOS",
                             "os_version": device_data.get('os_version'),
                             "screen_dpi": 2,
                             "screen_height": int(device_data.get('resolution').split('x')[1]),
                             "screen_width": int(device_data.get('resolution').split('x')[0]),
                             "sdk": "ios",
                             "sdk_version": campaign_data.get('branch').get('sdk').split('ios')[1],
                             "user_agent":device_data.get('User-Agent')}}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def branch_close(campaign_data, app_data, device_data,call_seq=1):
	method = 'post'
	url='http://api2.branch.io/v1/close'
	header={
			'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
			'Content-Type': 'application/json',
			"Accept-Encoding": "br, gzip, deflate",
			"Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
			}
	params={}
	data={
			"session_id":app_data.get('session_id'),
			"language":device_data.get('locale').get('language'),
			"user_agent":device_data.get('User-Agent'),
			"country":device_data.get('locale').get('country'),
			"screen_height":int(device_data.get('resolution').split('x')[1]),
			"ad_tracking_enabled":True,
			"brand":"Apple",
			"retryNumber":0,
			"identity_id":app_data.get('identity_id'),
			"os":"iOS",
			"hardware_id":app_data.get('idfv_id'),
			"ios_vendor_id":app_data.get('idfa_id'),
			"screen_width":int(device_data.get('resolution').split('x')[0]),
			"hardware_id_type":"idfa",
			"instrumentation":{"/v2/event/custom-brtt":str(random.randint(200,700))},
			"os_version":device_data.get('os_version'),
			"is_hardware_id_real":True,
			"model":device_data.get('device_platform'),
			"branch_key":campaign_data.get('branch').get('key'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"sdk":campaign_data.get('branch').get('sdk'),
			"local_ip":device_data.get('private_ip'),
			
			}

	if call_seq==2:
		data['instrumentation']={"/v1/open-brtt":"432"}

	if call_seq==3:
		data['instrumentation']={"/v1/open-brtt":"418"}		

	return {'url':url, 'httpmethod':method, 'headers':header, 'params': None, 'data': json.dumps(data)}	


###################################
#		Branch_open()
###################################

def branch_open(campaign_data, app_data, device_data,call_seq=1):
	method = 'post'
	url='http://api2.branch.io/v1/open'
	header={
			'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
			'Content-Type': 'application/json',
			"Accept-Encoding": "br, gzip, deflate",
			"Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),

			}
	params={}
	data={
			"sdk":campaign_data.get('branch').get('sdk'),
			"apple_testflight": False,
			"facebook_app_link_checked":False,
			"language":device_data.get('locale').get('language'),
			"user_agent":device_data.get('User-Agent'),
			"screen_height":int(device_data.get('resolution').split('x')[1]),
			"country":device_data.get('locale').get('country'),
			"app_version":campaign_data.get('app_version_name'),
			"update":1,
			"ad_tracking_enabled":False,
			"brand":"Apple",
			"retryNumber":0,
			"os":"iOS",
			"hardware_id":app_data.get('idfv_id'),
			"ios_vendor_id":app_data.get('idfa_id'),
			"ios_bundle_id":campaign_data.get('package_name'),
			"screen_width":int(device_data.get('resolution').split('x')[0]),
			"hardware_id_type":"idfa",
			"debug":False,
			"model":device_data.get('device_platform'),
			"os_version":device_data.get('os_version'),
			"is_hardware_id_real":True,
			"branch_key":campaign_data.get('branch').get('key'),
			"apple_ad_attribution_checked":False,
			"first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
			"latest_install_time":int(app_data.get('times').get('install_complete_time')*1000),
			"local_ip": device_data.get('private_ip'),
			"lastest_update_time":int(app_data.get('times').get('install_complete_time')*1000),
			"cd":{"pn":campaign_data.get('package_name'),"mv":"-1"},
			"device_fingerprint_id" : app_data.get('device_fingerprint_id1'),
			"identity_id" : app_data.get('identity_id'),
			#"instrumentation" : {"/v1/close-brtt":str(random.randint(300,600))},
			"previous_update_time" : int(app_data.get('times').get('install_complete_time')*1000),
			"uri_scheme": "migrosmagazam",
			}


	return {'url':url, 'httpmethod':method, 'headers':header, 'params': None, 'data': json.dumps(data)}
	
# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"


def install_receipt():
	rr= 'MIISiQYJKoZIhvcNAQcCoIISejCCEnYCAQExCzAJBgUrDgMCGgUAMIICKgYJKoZIhvcNAQcBoIICGwSCAhcxggITMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMAwCAQ4CAQEEBAICAI0wDQIBAwIBAQQFDAM5OTAwDQIBCwIBAQQFAgMGkL0wDQIBDQIBAQQFAgMB1YgwDQIBEwIBAQQFDAM5OTAwDgIBAQIBAQQGAgQfj1jPMA4CAQkCAQEEBgIEUDI1MzAOAgEQAgEBBAYCBDGs/yMwEAIBDwIBAQQIAgZTt67C+BkwFAIBAAIBAQQMDApQcm9kdWN0aW9uMBgCAQQCAQIEECyKcKIpnug80+dCoXkE6nowHAIBBQIBAQQUfxPK5E0Scmg3Tj/x1wHcFLy+c4kwHgIBCAIBAQQWFhQyMDE5LTExLTAyVDEwOjA5OjA2WjAeAgEMAgEBBBYWFDIwMTktMTEtMDJUMTA6MDk6MDZaMB4CARICAQEEFhYUMjAxOS0xMS0wMlQxMDowOTowNlowHwIBAgIBAQQXDBVjb20udGVjaG5vZmFjZS5taWdyb3MwQwIBBwIBAQQ7ypGaecKCNUbvbFI4/zV6zrFg/Bdowo8DMrTs/6i1igXEbvf7CuqvOOuDYSsCnXfRVGfIvs6oUfwHWZYwSgIBBgIBAQRCK0vR7qlDlQHz133vqO0u09yTcEC27E/IBa58AZAQpVq9ujyiRagupKv6lYA5Utha7SwwbYFfGAWSwFLTAR7PLHhCoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQBdHr+l+PTAOU6UnjO3n7z/hJeIfDGbN+DzmmbHmpYLw8wScPjG+HHIVqdnRCVcXmRO7SL1dX32qyR8S/6U9ZqyK3WCJ9Nck91+4wJ0M5uQgGvVAa5LtFZDcHpAFrEC8NGo7Pbc5qDgofNnRY/Vq4XEJa9oxaZlah4hPSTOkCZHSQ6p7QUBxeHQ4wM5RhYa0tsSihbAty/cA7RaB/FQFpKnT8g1qAd+SEsarl3Dw1062g/kvfMi8tUncyzbQaSrbBBA/w8H+BTyO8jGDjOamNW5CSEZWSzEeeCiLnD8PuEkxeyYw6o7he3TnfZwuJvfakgFRY7pX46ERRTr1A+K6J7n'
	
	return rr