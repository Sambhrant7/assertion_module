# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


import sys,uuid
from sdk import getsleep
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import util,installtimenew
from datetime import datetime
import json,time,hashlib,urllib,random,string,base64
import clicker
import Config

campaign_data = {
				'package_name' : 'com.boardcontest.carromclash',
				'sdk' : 'Android',
				'no_referrer':False,
				'ctr' : 1,
				'app_version_name' : '1.4',
				'supported_os':'4.4',
				'app_name' : 'Carrom',
				'supported_countries': 'WW',
				'device_targeting':True,
				'app_size' :22.0,
				'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
				'tracker':'singular',
				'singular':{
					'version':'Singular/v9.2.5',
					'key':'gamedroidlabs_f26de500',
					'secret':'a983488d1d08c451a91ac06078e45706'
				},
				'retention':{
							1:50,
							2:48,
							3:46,
							4:44,
							5:40,
							6:38,
							7:35,
							8:33,
							9:32,
							10:32,
							11:31,
							12:30,
							13:29,
							14:28,
							15:25,
							16:24,
							17:23,
							18:22,
							19:20,
							20:19,
							21:18,
							22:15,
							23:14,
							24:13,
							25:13,
							26:12,
							27:11,
							28:11,
							29:10,
							30:8,
							31:7,
							32:6,
							33:5,
							34:4,
							35:3,
		}
}





def install(app_data, device_data):
	##########INITIALIZE#############
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	global start_s

	start_s = str(int(app_data.get('times').get('install_complete_time')*1000))	
		
	#user_agent(campaign_data,app_data,device_data)
	#generate_gcm_id(app_data)

	if not app_data.get('singular_install_id'):
		app_data['singular_install_id'] = str(uuid.uuid4())

	time.sleep(random.randint(1,2))
 	print '\n singular Resolve \n'	
	singular_Resolve = singularResolve(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**singular_Resolve)

	if not app_data.get('userid'):
		app_data['userid']=str(uuid.uuid4())


	time.sleep(random.randint(1,2))
	print '\n singular START \n'	
	singular_start = singularStart(campaign_data, app_data, device_data,lag='0.0'+str(random.randint(91,99)))
	util.execute_request(**singular_start)

	time.sleep(random.randint(1,2))
	if app_data.get('referrer'):
		print '\n singular EVENT - __InstallReferrer \n'	
		singular_event = singularEvent(campaign_data, app_data, device_data,'__InstallReferrer',value={"referrer":app_data.get('referrer'),"referrer_source":"service","clickTimestampSeconds":int(app_data.get('times').get('click_time')),"installBeginTimestampSeconds":int(app_data.get('times').get('download_begin_time')),"current_device_time":int(time.time()*1000),"is_revenue_event":False})
		util.execute_request(**singular_event)

	
	time.sleep(random.randint(2,5))
	menuevent(app_data,campaign_data,device_data)

	time.sleep(random.randint(1,2))
	print '\n singular EVENT - __LicensingStatus \n'	
	singular_event = singularEvent(campaign_data, app_data, device_data,'__LicensingStatus',value={"responseCode":"-1","signedData":"Exception: java.lang.SecurityException: Not allowed to bind to service Intent { act=com.android.vending.licensing.ILicensingService pkg=com.android.vending }, Message: Not allowed to bind to service Intent { act=com.android.vending.licensing.ILicensingService pkg=com.android.vending }","signature":"","is_revenue_event":False})
	util.execute_request(**singular_event)


	if random.randint(1,100) <=90:
		time.sleep(random.randint(10,30))
		menuevent(app_data,campaign_data,device_data)

		call_pattern(app_data,campaign_data,device_data,day=None,calltype='install')

	
	return {'status':True}


def open(app_data, device_data,day):
	##########INITIALIZE#############
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	global start_s
	
	start_s = str(int(time.time()*1000))
		
	#user_agent(campaign_data,app_data,device_data)
	#generate_gcm_id(app_data)

	if not app_data.get('singular_install_id'):
		app_data['singular_install_id'] = str(uuid.uuid4())

	time.sleep(random.randint(1,2))
 	print '\n singular Resolve \n'	
	singular_Resolve = singularResolve(campaign_data, app_data, device_data)	
	util.execute_request(**singular_Resolve)

	if not app_data.get('userid'):
		app_data['userid']=str(uuid.uuid4())


	time.sleep(random.randint(1,2))
	print '\n singular START \n'	
	singular_start = singularStart(campaign_data, app_data, device_data,lag='0.0'+str(random.randint(91,99))+str('00000000000000')+str(random.randint(1,9)),isOpen=True)
	util.execute_request(**singular_start)

	app_open_retention(app_data,campaign_data,device_data, day = 'day_' + str(day))
	
	time.sleep(random.randint(2,5))
	menuevent(app_data,campaign_data,device_data)

	if random.randint(1,100) <=90:
		time.sleep(random.randint(10,30))
		menuevent(app_data,campaign_data,device_data)

		call_pattern(app_data,campaign_data,device_data,day=int(day),calltype='open')	


	return {'status':True}


def call_pattern(app_data,campaign_data,device_data,day=None,calltype='install'):

	if not app_data.get('level'):
		app_data['level'] = 1

	if calltype == 'install':
		game_play = random.choice([6,7,8,9,10,10,10,10,12,12])
	else:
		if day == 1 or day == 2:
			game_play = random.choice([6,7,8,9,10,10,10])
		else:
			game_play = random.choice([3,5,7,8])	


	if random.randint(1,100) <= 95:
		for i in range(game_play):

			time.sleep(random.randint(15,60))  # Play  stage 
			print "play stage ",app_data.get('level')
			playstage(app_data,campaign_data,device_data)

			if app_data.get('level')%5 == 0: # after every 5 playstage
				#time.sleep(random.randint(300,600))
				print "play level ",str(app_data.get('level')/5)
				playlevel(app_data,campaign_data,device_data,level=str(app_data.get('level')/5))	

			if random.randint(1,100) <= 15: # play arcade
				time.sleep(random.randint(15,60))
				playarcade(app_data,campaign_data,device_data)

			if random.randint(1,100) <= 4:
				time.sleep(random.randint(15,60))
				shopevent(app_data,campaign_data,device_data)

			if random.randint(1,100) <= 2:
				time.sleep(random.randint(15,60))
				moregames(app_data,campaign_data,device_data)

			if random.randint(1,100) <= 1:
				time.sleep(random.randint(30,60))
				rateus(app_data,campaign_data,device_data)

			if random.randint(1,100) <= 1:
				time.sleep(random.randint(30,60))
				help(app_data,campaign_data,device_data)


			if random.randint(1,100) <= 1:
				time.sleep(random.randint(30,60))
				share(app_data,campaign_data,device_data)


			app_data['level']+=1




def app_open_retention(app_data,campaign_data,device_data, day):
	print '----------------------------- singular EVENT -----------------------------'	
	eve_val = {"is_revenue_event":False}
	singular_event = singularEvent(campaign_data, app_data, device_data,'app_open_' + str(day),value=eve_val)
	util.execute_request(**singular_event)

def menuevent(app_data,campaign_data,device_data):
	print '----------------------------- singular EVENT ----------------MENU-------------'	
	eve_val = {"is_revenue_event":False}
	singular_event = singularEvent(campaign_data, app_data, device_data,'MENU',value=eve_val)
	util.execute_request(**singular_event)

def playstage(app_data,campaign_data,device_data):
	print '----------------------------- singular EVENT ----------------PLAY_STAGE-------------'	
	eve_val = {"is_revenue_event":False}
	singular_event = singularEvent(campaign_data, app_data, device_data,'PLAY_STAGE',value=eve_val)
	util.execute_request(**singular_event)

def playlevel(app_data,campaign_data,device_data,level=''):
	print '----------------------------- singular EVENT ----------------Play Level-------------'	
	eve_val = {"Level":"custom_level"+str(level),"is_revenue_event":False}
	singular_event = singularEvent(campaign_data, app_data, device_data,'Play Level',value=eve_val)
	util.execute_request(**singular_event)

def playarcade(app_data,campaign_data,device_data):
	print '----------------------------- singular EVENT ----------------PLAY_ARCADE-------------'	
	eve_val = {"is_revenue_event":False}
	singular_event = singularEvent(campaign_data, app_data, device_data,'PLAY_ARCADE',value=eve_val)
	util.execute_request(**singular_event)

def shopevent(app_data,campaign_data,device_data):
	print '----------------------------- singular EVENT ----------------SHOP-------------'	
	eve_val = {"is_revenue_event":False}
	singular_event = singularEvent(campaign_data, app_data, device_data,'SHOP',value=eve_val)
	util.execute_request(**singular_event)

def moregames(app_data,campaign_data,device_data):
	print '----------------------------- singular EVENT ----------------more_games-------------'	
	eve_val ={"name":"more_games","is_revenue_event":False}
	singular_event = singularEvent(campaign_data, app_data, device_data,'more_games',value=eve_val)
	util.execute_request(**singular_event)

def rateus(app_data,campaign_data,device_data):
	print '----------------------------- singular EVENT ----------------rateUs-------------'	
	eve_val = {"name":"rateUs","is_revenue_event":False}
	singular_event = singularEvent(campaign_data, app_data, device_data,'rateUs',value=eve_val)
	util.execute_request(**singular_event)


def help(app_data,campaign_data,device_data):
	print '----------------------------- singular EVENT ----------------Help-------------'	
	eve_val = {"name":"help me","is_revenue_event":False}
	singular_event = singularEvent(campaign_data, app_data, device_data,'Help',value=eve_val)
	util.execute_request(**singular_event)

def share(app_data,campaign_data,device_data):
	print '----------------------------- singular EVENT ----------------Share-------------'	
	eve_val ={"name":"Share","is_revenue_event":False}
	singular_event = singularEvent(campaign_data, app_data, device_data,'Share',value=eve_val)
	util.execute_request(**singular_event)
	
	

#########################################
#										#
# 			AppSalar funcation 			#
#										#
#########################################

def generate_gcm_id(app_data):
	if not app_data.get('device_gcm_id'):
		app_data['device_gcm_id'] = 'APA' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(137))
	return app_data.get('device_gcm_id')

def singularResolve(campaign_data, app_data, device_data):
	url = 'https://sdk-api-v1.singular.net/api/v1/resolve' 
	method = 'post'
	headers = {
		'User-Agent': 'Singular/SDK-v9.2.5.PROD',#'Singular/SDK-v9.0.4.PROD',
		'Accept-Encoding': 'gzip',
		'Content-Type': 'application/json',
	}

	data_dict = {
		"a":campaign_data.get('singular').get('key'),
		"c":"wifi",
		"i":campaign_data.get('package_name'),
		"k":"AIFA",
		"lag":str(generateLag()),
		"p":"Android",
		"pi":"1",
		"rt":"json",
		"u":device_data.get('adid'),
		"v":device_data.get('os_version'),
	}


	if app_data.get('userid'):
		data_dict['custom_user_id'] = str(app_data.get('userid'))
		

	params = get_params(data_dict)

	# 	params ='a='+campaign_data.get('singular').get('key')+'&c=wifi&custom_user_id='+app_data.get('userid')+'&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(str(generateLag()))+'&p='+'Android'+'&pi=1&rt=json&u='+device_data.get('adid')+'&v='+device_data.get('os_version')
	# else:
	# 	params ='a='+campaign_data.get('singular').get('key')+'&c=wifi'+'&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(str(generateLag()))+'&p='+'Android'+'&pi=1&rt=json&u='+device_data.get('adid')+'&v='+device_data.get('os_version')


	app_data['singular_time']=time.time()

	# params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('singular').get('secret'))
	params = params+'&h='+h

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps({}) }


def singularStart(campaign_data, app_data, device_data,lag='',isOpen=False):
	# cal_seq(app_data)

	url = "https://sdk-api-v1.singular.net/api/v1/start"
	method = 'post'
	headers = {
		'User-Agent': 'Singular/SDK-v9.2.5.PROD', #'Singular/SDK-v9.0.4.PROD',
		'Accept-Encoding': 'gzip',
		'Content-Type': 'application/json',
	}
	
	# if not app_data.get('s'):
	# 	app_data['s'] = str(int(time.time()*1000)-random.randint(5,10))

	data_dict = {
				"a":campaign_data.get('singular').get('key'),
				"ab":device_data.get('cpu_abi',"armeabi"),
				"aifa":device_data.get('adid'),
				"av":campaign_data.get('app_version_name'),
				"br":device_data.get('brand'),
				"c":"wifi",
				"current_device_time":str(int(time.time()*1000)),
				"ddl_enabled":"true",
				# "ddl_to":"60",
				"de":device_data.get('device'),
				"device_type":"phone",
				"device_user_agent":get_ua(device_data),
				"dnt":"-1",
				"event_index":str(app_data.get('seq',"0")),
				"i":campaign_data.get('package_name'),
				"install_time":str(int(app_data.get('times').get('download_end_time')*1000)),
				"is":"true",
				"k":"AIFA",
				"lag":str(generateLag()),
				"lc":device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
				"ma":device_data.get('manufacturer'),
				"mo":device_data.get('model'),
				"n":campaign_data.get('app_name'),
				"p":"Android",
				"pr":device_data.get('product'),
				"rt":"json",
				"s":start_s,
				"sdk":campaign_data.get('singular').get('version'),
				"singular_install_id":app_data.get('singular_install_id'),
				"src":"com.android.vending",
				"u":device_data.get('adid'),
				"update_time":str(int(app_data.get('times').get('download_end_time')*1000)),
				"v":device_data.get('os_version'),

	}

	if isOpen:
		data_dict['is'] = 'false'

	# params = 'a='+campaign_data.get('singular').get('key')+'&ab='+device_data.get('cpu_abi',"armeabi")+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&br='+device_data.get('brand')+'&c=wifi&current_device_time='+str(int(time.time()*1000))+'&ddl_enabled=true&ddl_to=60&de='+device_data.get('device')+'&device_type='+device_data.get('device_type')+'&device_user_agent='+ urllib.quote(get_ua(device_data)) +'&dnt=0&event_index=0&i='+campaign_data.get('package_name')+'&install_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&is=true&k=AIFA&lag='+lag+'&lc='+device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')+'&ma='+device_data.get('manufacturer')+'&mo='+device_data.get('model')+'&n='+ urllib.quote(campaign_data.get('app_name')) +'&p=Android&pr='+device_data.get('product')+'&rt=json&s='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&sdk='+urllib.quote_plus(campaign_data.get('singular').get('version'))+'&singular_install_id='+app_data.get('singular_install_id')+'&src=com.android.vending&u='+device_data.get('adid')+'&update_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&v='+device_data.get('os_version')


	params = get_params(data_dict)


	#params = params.replace(' ','+')

	h = getHash('?'+params,campaign_data.get('singular').get('secret'))
	params = params+'&h='+h

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps({}) }


def singularEvent(campaign_data, app_data, device_data,event_name,value='',custom_user_id=False):
	cal_seq(app_data)

	url = "https://sdk-api-v1.singular.net/api/v1/event"

	method = 'post'
	headers = { 'User-Agent': 'Singular/SDK-v9.2.5.PROD',
				'Content-Type': 'application/json',
				'Host': 'sdk-api-v1.singular.net',
				'Connection': 'Keep-Alive',
				'Accept-Encoding': 'gzip',
				'Content-Length': '113',

			}


	t = str(time.time()-app_data.get('singular_time'))[:-8]

	data_dict = {
	"a":campaign_data.get('singular').get('key'),
	"aifa":device_data.get('adid'),#"ee81b619-c3ee-4c49-82ad-f70fce157d1f",
	"av":campaign_data.get('app_version_name'),
	"c":"wifi",
	"event_index":str(app_data.get('seq')),
	"i":campaign_data.get('package_name'),
	"k":"AIFA",
	"lag":str(generateLag()),#"0.032",
	"n":str(event_name),
	"p":"Android",
	"rt":"json",
	"s":start_s,
	"sdk":campaign_data.get('singular').get('version'),
	"seq":str(app_data.get('seq')),
	"singular_install_id":app_data.get('singular_install_id'),#"a6afffe5-d9e0-4298-b715-27a81d517794",
	"t":str(t),#"14.222",
	"u":device_data.get('adid'),#"ee81b619-c3ee-4c49-82ad-f70fce157d1f",
	
	}

	params = get_params(data_dict)	

	# params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('singular').get('secret'))
	params = params+'&h='+h

	q = json.dumps({"e":json.dumps(value)}) # Your payload dict
	q = q.replace(" ","").strip()


	jsonsignature = payloadsignature(q,campaign_data.get('singular').get('secret'))

	data = {
	'payload':q,
	'signature': jsonsignature
	}	

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':json.dumps(data).replace(" ","").strip()}



#########################################
#										#
# 			Self Call function 			#
#										#
#########################################





def gcmToken_call(campaign_data, app_data, device_data):
	url 	= 'http://android.clients.google.com/c2dm/register3'
	method 	= 'post'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent'	  : 'Android-GCM/1.5 ('+device_data.get('product')+' '+device_data.get('build')+')',
		'Content-Type'	  : 'application/x-www-form-urlencoded',
		'app'			  : campaign_data.get('package_name'),
		'gcm_ver'		  : '17455020',
		'Authorization'	  : 'AidLogin 4289720365770577428:2116601572621516374',
		}
	params 	= None
	data 	= {
			'X-subtype':'332916321191',
			'sender':'332916321191',
			'X-app_ver':campaign_data.get('app_version_code'),
			'X-osv':device_data.get('sdk'),
			'X-cliv':'fiid-'+util.get_random_string('decimal',8),
			'X-gmsv':'17455020',
			'X-appid':util.get_random_string('char_all',11),
			'X-scope':'*',
			'X-gmp_app_id':'1:'+util.get_random_string('decimal',12)+':android:'+device_data.get('android_id'),
			'X-app_ver_name':campaign_data.get('app_version_name'),
			'app':campaign_data.get('package_name'),
			'device':'4289720365770577428',
			'cert':util.get_random_string('hex',40),
			'app_ver':campaign_data.get('app_version_code'),
			'gcm_ver':'17455020',
			'plat':'0',
			'target_ver':26,
			'X-Firebase-Client':'fire-android/ fire-core/16.1.0'
	}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

#########################################
#										#
# 			Extra funcation 			#
#										#
#########################################

def cal_seq(app_data):
	if app_data.get('seq') == None:
		app_data['seq'] = 1
	else:
		app_data['seq'] = app_data.get('seq')+1
	# return app_data.get('seq')

def user_agent(campaign_data,app_data,device_data):
	if int(device_data.get("sdk")) >19:
		app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'	
	return app_data.get('ua')

def cal_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def current_location():
	
	url='https://api.letgo.com/api/iplookup.json'
	header={
			'User-Agent': 'okhttp/2.3.0'
			}
	params={}
	data={}
	
	return {'url': url, 'httpmethod': 'get', 'headers': header, 'params': params, 'data': data}

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def generateLag():
	return (random.randint(10,90)*0.001)

def generate_t():
	return "%.3f" %(random.uniform(300,1000))

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())


def payloadsignature(str1,str2, radix=16):
	import hashlib
	sha_obj = hashlib.sha1()
	sha_obj.update(str2.encode('utf-8'))
	sha_obj.update(str1.encode('utf-8'))
	if radix == 16:		
		return sha_obj.hexdigest()
	elif radix == 64:		
		return base64(sha_obj.digest())

def get_params(data_value):
	key = data_value.keys()
	a = ''
	for value in sorted(key):
		a += str(urllib.quote_plus(value).encode('utf-8'))+'='+str(urllib.quote_plus(data_value.get(value)).encode('utf-8'))+'&'
	a = a.rsplit("&",1)[0]	

	return a



#########################################
#										#
# 			Util funcation 				#
#										#
#########################################

def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))


def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'


