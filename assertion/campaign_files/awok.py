# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import installtimenew
from sdk import getsleep
from sdk import util
from sdk import NameLists
from sdk import purchase
from geopy.geocoders import Nominatim
import time
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config

#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################

campaign_data = {
	'package_name'		 : 'com.awok.store',
	'app_size'			 : 25.0,#24.0,#25.0,
	'app_name' 			 : 'AWOK.com',
	'app_version_name' 	 : '1.936.4',#'1.936.2',#'1.934.0',#'1.932.0',#1.927.0, '1.920.0',#'1.919.0',
	'app_version_code'	 : '19364',#'19362',#'19340',#'340',#19270, '19200',#'19190',
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 : '74hbnecuhw1s',
		'sdk'		 : 'android4.17.0',
		'secret_key' : '1946102922147856750216751308151318588737',#30634419120174047241263704224877883809, '806759341151382536616767009051165190881',#'2342623562120221290790010654632046083',
		'secret_id'	 : '11',#9, '6',#'4',
		},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	pushtoken(app_data)
	set_androidUUID(app_data)

	################generating_realtime_differences##################
	s1=2
	s2=4
	a1=0
	a2=0
	ir1=2
	ir2=3
	click_time2=True
	e1=1
	e2=3
	c1=1
	c2=2

 ###########	 CALLS		 ############

	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
		
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='install_referrer',t1=ir1,t2=ir2)
	util.execute_request(**request)

	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,source='reftag',t1=c1,t2=c2)
	util.execute_request(**request)

	time.sleep(random.randint(1,2))
	request=home_selfCall( campaign_data, device_data, app_data )
	response=util.execute_request(**request)
	try:
		output=json.loads(response.get('data')).get('OUTPUT').get('DATA').get('ITEMS')
		globals()['product_data'] = []

		for item in output:
			if item.get('LINKED_PROD_ID') and item.get('NAME') and item.get('PRICES',{}).get('PRICE_NEW'):
				globals()['product_data'].append({'id' : str(item.get('LINKED_PROD_ID')), 'name' : item.get('NAME').encode('utf-8'), 'price' : str(item.get('PRICES').get('PRICE_NEW')) })

		if not globals().get('product_data'):
			globals()['product_data']	=	[  { "id" : "1308483", "name" : "Power Floss Air Powered Dental Water Jet", "price" : "12" }, { "id" : "1301620", "name" : "EverBrite Deluxe Motion Activated LED Solar Outdoor Light", "price" : "19" }, { "id" : "1488179", "name" : "SQ11 Full HD 1920x1080 Sports Mini DV Camera / Digital Video Recorder With Infrared Night Vision, Motion Detector, TF Card Support, Black", "price" : "35" }, { "id" : "3620", "name" : "Apple iPhone 4s 16GB Black - Refurbished", "price" : "199" }, { "id" : "1559143", "name" : "A&M Stylish Luxury Dial With Crystal Studded Bracelet Women's Gift Set, Gold", "price" : "29" }, { "id" : "1573526", "name" : "Wood Log Design Non Woven Fabric Multipurpose Storage Box Step Stool 30x30CM", "price" : "29" }, { "id" : "1572512", "name" : "Keyang 3W+ 8SMD Rechargable Torch, KY-312", "price" : "15" }, { "id" : "1572551", "name" : "Polartec Halogen Oven, PT-0488", "price" : "99" }, { "id" : "1569596", "name" : "Sup Game Box 168 In 1 Games 3.0 inch Pocket Handheld Game Console, Assorted color", "price" : "35" }, { "id" : "1568331", "name" : "Jianxiang Fashionable Women Cotton Vest Tops 6 Pcs Set, Assorted Colors", "price" : "29" }, { "id" : "1558179", "name" : "Eros Delay Spray For Men 14ML-ZH", "price" : "19" }, { "id" : "1540651", "name" : "Socmark Soft Material Multi-design 12 Pairs Men's No-show Cotton Socks, Assorted Color", "price" : "19" }, { "id" : "1511041", "name" : "Mione Xs Max, 4G Dual Sim, 32GB, 6.18+IB0 IPS, Black", "price" : "279" }, { "id" : "1516143", "name" : "5 In 1 Multifunction Outdoor Survival Gear Escape Paracord Bracelet Flint & Whistle & Compass Outdoor Camping Tools Bracelet, Green", "price" : "9" }, { "id" : "1366619", "name" : "Foreo Forever Luna Mini 2 Rechargeable Facial Cleansing Electric Brush, Multicolor", "price" : "19" }, { "id" : "1502831", "name" : "Power Socket Extension With 3 AC Sockets & 6 USB Ports, ZGN-T09", "price" : "39" }, { "id" : "196363", "name" : "Apple iPhone 5S 16GB Silver - Refurbished", "price" : "399" }, { "id" : "235553", "name" : "Sunshade Bluetooth Stylish Sporty Earphones Sunglasses- Rechargeable", "price" : "29" }, { "id" : "1325392", "name" : "Pedi Mate Complete 18Pcs Handheld Pedicure/Manicure Set, 850875", "price" : "15" }, { "id" : "1307837", "name" : "Royal Delux Unisex Free Size Bathrobe With Cap, ARZ-99, Assorted Color", "price" : "25" }  ]
			print '\nDefault value = product list'

		print '\nOk response = product list'

	except:
		print '\nException = product list'
		globals()['product_data']	=	[  { "id" : "1308483", "name" : "Power Floss Air Powered Dental Water Jet", "price" : "12" }, { "id" : "1301620", "name" : "EverBrite Deluxe Motion Activated LED Solar Outdoor Light", "price" : "19" }, { "id" : "1488179", "name" : "SQ11 Full HD 1920x1080 Sports Mini DV Camera / Digital Video Recorder With Infrared Night Vision, Motion Detector, TF Card Support, Black", "price" : "35" }, { "id" : "3620", "name" : "Apple iPhone 4s 16GB Black - Refurbished", "price" : "199" }, { "id" : "1559143", "name" : "A&M Stylish Luxury Dial With Crystal Studded Bracelet Women's Gift Set, Gold", "price" : "29" }, { "id" : "1573526", "name" : "Wood Log Design Non Woven Fabric Multipurpose Storage Box Step Stool 30x30CM", "price" : "29" }, { "id" : "1572512", "name" : "Keyang 3W+ 8SMD Rechargable Torch, KY-312", "price" : "15" }, { "id" : "1572551", "name" : "Polartec Halogen Oven, PT-0488", "price" : "99" }, { "id" : "1569596", "name" : "Sup Game Box 168 In 1 Games 3.0 inch Pocket Handheld Game Console, Assorted color", "price" : "35" }, { "id" : "1568331", "name" : "Jianxiang Fashionable Women Cotton Vest Tops 6 Pcs Set, Assorted Colors", "price" : "29" }, { "id" : "1558179", "name" : "Eros Delay Spray For Men 14ML-ZH", "price" : "19" }, { "id" : "1540651", "name" : "Socmark Soft Material Multi-design 12 Pairs Men's No-show Cotton Socks, Assorted Color", "price" : "19" }, { "id" : "1511041", "name" : "Mione Xs Max, 4G Dual Sim, 32GB, 6.18+IB0 IPS, Black", "price" : "279" }, { "id" : "1516143", "name" : "5 In 1 Multifunction Outdoor Survival Gear Escape Paracord Bracelet Flint & Whistle & Compass Outdoor Camping Tools Bracelet, Green", "price" : "9" }, { "id" : "1366619", "name" : "Foreo Forever Luna Mini 2 Rechargeable Facial Cleansing Electric Brush, Multicolor", "price" : "19" }, { "id" : "1502831", "name" : "Power Socket Extension With 3 AC Sockets & 6 USB Ports, ZGN-T09", "price" : "39" }, { "id" : "196363", "name" : "Apple iPhone 5S 16GB Silver - Refurbished", "price" : "399" }, { "id" : "235553", "name" : "Sunshade Bluetooth Stylish Sporty Earphones Sunglasses- Rechargeable", "price" : "29" }, { "id" : "1325392", "name" : "Pedi Mate Complete 18Pcs Handheld Pedicure/Manicure Set, 850875", "price" : "15" }, { "id" : "1307837", "name" : "Royal Delux Unisex Free Size Bathrobe With Cap, ARZ-99, Assorted Color", "price" : "25" }  ]

	request=adjust_sdkinfo(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(1)
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2 , initiate ='backend')
	util.execute_request(**request)

	time.sleep(1)
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2 , initiate ='sdk')
	util.execute_request(**request)

	time.sleep(random.randint(6,8))
	request=app_measurement( campaign_data, device_data, app_data )
	util.execute_request(**request)

	if random.randint(1,100)<=95:
		# Tutorial
		time.sleep(random.randint(5,10))
		adjust_token_ue8b63(campaign_data, app_data, device_data,e1,e2)
		adjust_token_4rp0kz(campaign_data, app_data, device_data,e1,e2)
		app_data['Tutorial'] = True

	for _ in range(1, random.randint(2,4)):

		if random.randint(1,100) <= 90:

			choice = random.choice([1,1,1,1,1,1,2,2,2,2])

			if choice==1:
				# search
				time.sleep(random.randint(15,30))
				adjust_token_r13ilz(campaign_data, app_data, device_data,e1,e2)

			if choice==2:
				# scroll
				time.sleep(random.randint(15,30))
				adjust_token_67fzsj(campaign_data, app_data, device_data,e1,e2)

		if random.randint(1,100) <= 80:
			# view product
			time.sleep(random.randint(30,90))	
			adjust_token_gqpwus(campaign_data, app_data, device_data,e1,e2)

			if random.randint(1,100) <= 6:
				# share product
				time.sleep(random.randint(15,45))	
				adjust_token_ll8qzh(campaign_data, app_data, device_data,e1,e2)	

		if random.randint(1,100) <= 60 and not app_data.get('registered'):
			# signup
			req = global_awok( campaign_data, device_data, app_data )
			response=util.execute_request(**req)
			try:
				result = json.loads(response.get('data')).get('OUTPUT').get('DATA').get('TOKEN')
				app_data['token'] = result
			except:
				app_data['token'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uX2lkIjoiODBXS2pHMzQwYmM5NGE2MWY4ZjA2ZmUzMDkyN2MwYzE2ZmRlMjljMCIsInVzZXJfbG9naW4iOiJoYXBwdXNpbmdoWTMifQ.AjeDm4Zz7iUNn908XUvmc-kz5VHOf403ND3fYJP-1aw'	

			request=global_awok_users( campaign_data, device_data, app_data )
			response=util.execute_request(**request)
			try:
				result = json.loads(response.get('data')).get('OUTPUT').get('DATA').get('ID')
				app_data['user_id'] = result
			except:
				app_data['user_id'] = random.randint(7760000,7769999)	

			time.sleep(random.randint(30,45))
			adjust_token_9f3xd3(campaign_data, app_data, device_data,e1,e2)
			app_data['registered'] = True

		if app_data.get('registered')==True:
			if random.randint(1,100) <= 50:
				# add to cart
				time.sleep(random.randint(5,30))
				adjust_token_r4hyaz(campaign_data, app_data, device_data,e1,e2)

		if app_data.get('items_in_cart'):
			if random.randint(1,100) <= 10:
				# update product in cart
				time.sleep(random.randint(15,45))
				adjust_token_qibjmh(campaign_data, app_data, device_data,e1,e2)		

		if app_data.get('items_in_cart'):
			if random.randint(1,100) <= 35:
				# remove from cart
				time.sleep(random.randint(15,30))
				adjust_token_bm7rpd(campaign_data, app_data, device_data,e1,e2)

		# settings tab	
		if app_data.get('registered')==True:	
		
			if random.randint(1,100)<=30:
				# add address
				time.sleep(random.randint(45,90))
				adjust_token_53uah3(campaign_data, app_data, device_data,e1,e2)	
				app_data['address_added'] = True

			if random.randint(1,100)<=25 and app_data.get('address_added'):
				# edit address
				time.sleep(random.randint(45,90))
				adjust_token_pnsafa(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=5 and app_data.get('address_added'):

				print 'lat and lon generator'
				lat_lon=current_location(device_data)
				lat_lon_result = util.execute_request(**lat_lon)
				try:
					json_lat_lon_result=json.loads(lat_lon_result.get('data'))
					app_data['lat']=str(json_lat_lon_result.get('geo').get('latitude'))
					app_data['lon']=str(json_lat_lon_result.get('geo').get('longitude'))
				except:
					app_data['lat']="28.6358878"
					app_data['lon']="77.2249742"

				try:
					value=str(app_data.get('lat'))+','+str(app_data.get('lon'))
					geolocator = Nominatim(user_agent="Awok")
					location = geolocator.reverse(value,timeout=None)	
					try:
						result = location.raw.get('address')
						app_data['city'] = result.get('city')
						app_data['postcode'] = result.get('postcode')
						app_data['road'] = result.get('road')
					except:
						app_data['city'] = "Delhi"
						app_data['postcode'] = "1100001"
						app_data['road'] = "Deen Dayal Upadhyay Road"	
				except:
					app_data['city'] = "Delhi"
					app_data['postcode'] = "1100001"
					app_data['road'] = "Deen Dayal Upadhyay Road"		
					
				print "\nself_call_global_awok\n"
				request=global_awok_addresses( campaign_data, device_data, app_data )
				response=util.execute_request(**request)
				try:
					result=json.loads(response.get('data')).get('OUTPUT').get('DATA').get('ADDED_ADDRESS_ID')	
					app_data['address_id'] = result
				except:
					app_data['address_id'] = random.randint(5300000,5309999)

				# delete address
				time.sleep(random.randint(15,45))
				adjust_token_xg18i0(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=10:
				# add card
				time.sleep(random.randint(45,90))
				adjust_token_ldtodm(campaign_data, app_data, device_data,e1,e2)	
				app_data['card_added'] = True

			if random.randint(1,100)<=3 and app_data.get('card_added'):
				# delete card
				time.sleep(random.randint(15,45))
				adjust_token_ybm9oc(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=20:
				# update country
				time.sleep(random.randint(15,45))
				adjust_token_6kd9u3(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=20:	
				# update language
				time.sleep(random.randint(15,45))
				adjust_token_ev0e9f(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=20:	
				# update currency
				time.sleep(random.randint(15,45))
				adjust_token_axw1e9(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=20:	
				# update password
				time.sleep(random.randint(30,60))
				adjust_token_80hhpy(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=20:	
				# customer support
				time.sleep(random.randint(15,45))
				adjust_token_tlebti(campaign_data, app_data, device_data,e1,e2)

			if random.randint(1,100)<=20:	
				# edit_profile
				time.sleep(random.randint(15,45))
				adjust_token_gpw60m(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=5:	
				# reset_password
				time.sleep(random.randint(15,45))
				adjust_token_59hbqm(campaign_data, app_data, device_data,e1,e2)	
				
			if random.randint(1,100)<=5:	
				# event_optional_update
				time.sleep(random.randint(15,45))
				adjust_token_o6teo9(campaign_data, app_data, device_data,e1,e2)				

		if app_data.get('registered')==True:

			if random.randint(1,100) <= 20:
				# apply_coupon
				time.sleep(random.randint(15,45))
				adjust_token_au7iza(campaign_data, app_data, device_data,e1,e2)

			if random.randint(1,100) <= 55 and app_data.get('items_in_cart'):
				# check out initiate
				time.sleep(random.randint(10,30))
				adjust_token_o146jt(campaign_data, app_data, device_data,e1,e2)

				if purchase.isPurchase(app_data,day=1,advertiser_demand=25):
					time.sleep(random.randint(10,30))
					adjust_token_9e62ye(campaign_data, app_data, device_data,e1,e2)

					if random.randint(1,100)<=10:
						# update_delivery
						time.sleep(random.randint(15,45))
						adjust_token_k1ggvh(campaign_data, app_data, device_data,e1,e2)

					if random.randint(1,100)<=10:
						# update_payment
						time.sleep(random.randint(15,45))
						adjust_token_owi8v2(campaign_data, app_data, device_data,e1,e2)
						
					if random.randint(1,100)<=10:
						# cancel_order
						time.sleep(random.randint(15,45))
						adjust_token_uqr512(campaign_data, app_data, device_data,e1,e2)		

	if app_data.get('registered') and random.randint(1,100) <= 5:
		# log out
		time.sleep(random.randint(15,45))
		adjust_token_q6oeqn(campaign_data, app_data, device_data,e1,e2)
		app_data['logged_out'] = True

		if app_data.get('registered') and random.randint(1,100) <= 60:
			# login
			time.sleep(random.randint(15,30))
			adjust_token_rh5u92(campaign_data, app_data, device_data,e1,e2)
			app_data['logged_out'] = False

	set_appCloseTime(app_data)	

	return {'status':'true'}

#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	pushtoken(app_data)
	set_androidUUID(app_data)

	################generating_realtime_differences##################
	s1=2
	s2=4
	e1=1
	e2=3
	ir1=2
	ir2=3
	click_time2=True

 	###########	 CALLS		 ############

 	app_data['items_in_cart'] = list()
	
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2,isOpen=True)
	util.execute_request(**request)

	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='install_referrer',t1=ir1,t2=ir2)
	util.execute_request(**request)

	if app_data.get('registered') and random.randint(1,100) <= 5:
		# log out
		time.sleep(random.randint(15,45))
		adjust_token_q6oeqn(campaign_data, app_data, device_data,e1,e2)
		app_data['logged_out'] = True

		if app_data.get('registered') and random.randint(1,100) <= 60:
			# login
			time.sleep(random.randint(15,30))
			adjust_token_rh5u92(campaign_data, app_data, device_data,e1,e2)
			app_data['logged_out'] = False

	time.sleep(random.randint(1,2))
	request=home_selfCall( campaign_data, device_data, app_data )
	response=util.execute_request(**request)
	try:
		output=json.loads(response.get('data')).get('OUTPUT').get('DATA').get('ITEMS')
		globals()['product_data'] = []

		for item in output:
			if item.get('LINKED_PROD_ID') and item.get('NAME') and item.get('PRICES',{}).get('PRICE_NEW'):
				globals()['product_data'].append({'id' : str(item.get('LINKED_PROD_ID')), 'name' : item.get('NAME').encode('utf-8'), 'price' : str(item.get('PRICES').get('PRICE_NEW')) })

		if not globals().get('product_data'):
			globals()['product_data']	=	[  { "id" : "1308483", "name" : "Power Floss Air Powered Dental Water Jet", "price" : "12" }, { "id" : "1301620", "name" : "EverBrite Deluxe Motion Activated LED Solar Outdoor Light", "price" : "19" }, { "id" : "1488179", "name" : "SQ11 Full HD 1920x1080 Sports Mini DV Camera / Digital Video Recorder With Infrared Night Vision, Motion Detector, TF Card Support, Black", "price" : "35" }, { "id" : "3620", "name" : "Apple iPhone 4s 16GB Black - Refurbished", "price" : "199" }, { "id" : "1559143", "name" : "A&M Stylish Luxury Dial With Crystal Studded Bracelet Women's Gift Set, Gold", "price" : "29" }, { "id" : "1573526", "name" : "Wood Log Design Non Woven Fabric Multipurpose Storage Box Step Stool 30x30CM", "price" : "29" }, { "id" : "1572512", "name" : "Keyang 3W+ 8SMD Rechargable Torch, KY-312", "price" : "15" }, { "id" : "1572551", "name" : "Polartec Halogen Oven, PT-0488", "price" : "99" }, { "id" : "1569596", "name" : "Sup Game Box 168 In 1 Games 3.0 inch Pocket Handheld Game Console, Assorted color", "price" : "35" }, { "id" : "1568331", "name" : "Jianxiang Fashionable Women Cotton Vest Tops 6 Pcs Set, Assorted Colors", "price" : "29" }, { "id" : "1558179", "name" : "Eros Delay Spray For Men 14ML-ZH", "price" : "19" }, { "id" : "1540651", "name" : "Socmark Soft Material Multi-design 12 Pairs Men's No-show Cotton Socks, Assorted Color", "price" : "19" }, { "id" : "1511041", "name" : "Mione Xs Max, 4G Dual Sim, 32GB, 6.18+IB0 IPS, Black", "price" : "279" }, { "id" : "1516143", "name" : "5 In 1 Multifunction Outdoor Survival Gear Escape Paracord Bracelet Flint & Whistle & Compass Outdoor Camping Tools Bracelet, Green", "price" : "9" }, { "id" : "1366619", "name" : "Foreo Forever Luna Mini 2 Rechargeable Facial Cleansing Electric Brush, Multicolor", "price" : "19" }, { "id" : "1502831", "name" : "Power Socket Extension With 3 AC Sockets & 6 USB Ports, ZGN-T09", "price" : "39" }, { "id" : "196363", "name" : "Apple iPhone 5S 16GB Silver - Refurbished", "price" : "399" }, { "id" : "235553", "name" : "Sunshade Bluetooth Stylish Sporty Earphones Sunglasses- Rechargeable", "price" : "29" }, { "id" : "1325392", "name" : "Pedi Mate Complete 18Pcs Handheld Pedicure/Manicure Set, 850875", "price" : "15" }, { "id" : "1307837", "name" : "Royal Delux Unisex Free Size Bathrobe With Cap, ARZ-99, Assorted Color", "price" : "25" }  ]
			print '\nDefault value = product list'

		print '\nOk response = product list'

	except:
		print '\nException = product list'
		globals()['product_data']	=	[  { "id" : "1308483", "name" : "Power Floss Air Powered Dental Water Jet", "price" : "12" }, { "id" : "1301620", "name" : "EverBrite Deluxe Motion Activated LED Solar Outdoor Light", "price" : "19" }, { "id" : "1488179", "name" : "SQ11 Full HD 1920x1080 Sports Mini DV Camera / Digital Video Recorder With Infrared Night Vision, Motion Detector, TF Card Support, Black", "price" : "35" }, { "id" : "3620", "name" : "Apple iPhone 4s 16GB Black - Refurbished", "price" : "199" }, { "id" : "1559143", "name" : "A&M Stylish Luxury Dial With Crystal Studded Bracelet Women's Gift Set, Gold", "price" : "29" }, { "id" : "1573526", "name" : "Wood Log Design Non Woven Fabric Multipurpose Storage Box Step Stool 30x30CM", "price" : "29" }, { "id" : "1572512", "name" : "Keyang 3W+ 8SMD Rechargable Torch, KY-312", "price" : "15" }, { "id" : "1572551", "name" : "Polartec Halogen Oven, PT-0488", "price" : "99" }, { "id" : "1569596", "name" : "Sup Game Box 168 In 1 Games 3.0 inch Pocket Handheld Game Console, Assorted color", "price" : "35" }, { "id" : "1568331", "name" : "Jianxiang Fashionable Women Cotton Vest Tops 6 Pcs Set, Assorted Colors", "price" : "29" }, { "id" : "1558179", "name" : "Eros Delay Spray For Men 14ML-ZH", "price" : "19" }, { "id" : "1540651", "name" : "Socmark Soft Material Multi-design 12 Pairs Men's No-show Cotton Socks, Assorted Color", "price" : "19" }, { "id" : "1511041", "name" : "Mione Xs Max, 4G Dual Sim, 32GB, 6.18+IB0 IPS, Black", "price" : "279" }, { "id" : "1516143", "name" : "5 In 1 Multifunction Outdoor Survival Gear Escape Paracord Bracelet Flint & Whistle & Compass Outdoor Camping Tools Bracelet, Green", "price" : "9" }, { "id" : "1366619", "name" : "Foreo Forever Luna Mini 2 Rechargeable Facial Cleansing Electric Brush, Multicolor", "price" : "19" }, { "id" : "1502831", "name" : "Power Socket Extension With 3 AC Sockets & 6 USB Ports, ZGN-T09", "price" : "39" }, { "id" : "196363", "name" : "Apple iPhone 5S 16GB Silver - Refurbished", "price" : "399" }, { "id" : "235553", "name" : "Sunshade Bluetooth Stylish Sporty Earphones Sunglasses- Rechargeable", "price" : "29" }, { "id" : "1325392", "name" : "Pedi Mate Complete 18Pcs Handheld Pedicure/Manicure Set, 850875", "price" : "15" }, { "id" : "1307837", "name" : "Royal Delux Unisex Free Size Bathrobe With Cap, ARZ-99, Assorted Color", "price" : "25" }  ]

	if day<=3:
		occurance=random.randint(2,3)
	elif day<=7:
		occurance=random.randint(1,2)
	else:
		occurance=random.randint(0,1)

	if not app_data.get('Tutorial') and random.randint(1,100)<=95:
		# Tutorial
		time.sleep(random.randint(5,10))
		adjust_token_ue8b63(campaign_data, app_data, device_data,e1,e2)
		adjust_token_4rp0kz(campaign_data, app_data, device_data,e1,e2)
		app_data['Tutorial'] = True

	for _ in range(occurance):

		if random.randint(1,100) <= 90:

			choice = random.choice([1,1,1,1,1,1,2,2,2,2])

			if choice==1:
				# search
				time.sleep(random.randint(15,30))
				adjust_token_r13ilz(campaign_data, app_data, device_data,e1,e2)

			if choice==2:
				# scroll
				time.sleep(random.randint(15,30))
				adjust_token_67fzsj(campaign_data, app_data, device_data,e1,e2)

		if random.randint(1,100) <= 80:
			# view product
			time.sleep(random.randint(30,90))	
			adjust_token_gqpwus(campaign_data, app_data, device_data,e1,e2)

			if random.randint(1,100) <= 6:
				# share product
				time.sleep(random.randint(15,45))	
				adjust_token_ll8qzh(campaign_data, app_data, device_data,e1,e2)	

		if random.randint(1,100) <= 60 and not app_data.get('registered'):
			# signup
			req = global_awok( campaign_data, device_data, app_data )
			response=util.execute_request(**req)
			try:
				result = json.loads(response.get('data')).get('OUTPUT').get('DATA').get('TOKEN')
				app_data['token'] = result
			except:
				app_data['token'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uX2lkIjoiODBXS2pHMzQwYmM5NGE2MWY4ZjA2ZmUzMDkyN2MwYzE2ZmRlMjljMCIsInVzZXJfbG9naW4iOiJoYXBwdXNpbmdoWTMifQ.AjeDm4Zz7iUNn908XUvmc-kz5VHOf403ND3fYJP-1aw'	

			request=global_awok_users( campaign_data, device_data, app_data )
			response=util.execute_request(**request)
			try:
				result = json.loads(response.get('data')).get('OUTPUT').get('DATA').get('ID')
				app_data['user_id'] = result
			except:
				app_data['user_id'] = random.randint(7760000,7769999)	

			time.sleep(random.randint(30,45))
			adjust_token_9f3xd3(campaign_data, app_data, device_data,e1,e2)
			app_data['registered'] = True

		if app_data.get('registered')==True:
			if random.randint(1,100) <= 50:
				# add to cart
				time.sleep(random.randint(5,30))
				adjust_token_r4hyaz(campaign_data, app_data, device_data,e1,e2)

		if app_data.get('items_in_cart'):
			if random.randint(1,100) <= 10:
				# update product in cart
				time.sleep(random.randint(15,45))
				adjust_token_qibjmh(campaign_data, app_data, device_data,e1,e2)		

		if app_data.get('items_in_cart'):
			if random.randint(1,100) <= 35:
				# remove from cart
				time.sleep(random.randint(15,30))
				adjust_token_bm7rpd(campaign_data, app_data, device_data,e1,e2)

		# settings tab	
		if app_data.get('registered')==True:
		
			if random.randint(1,100)<=30:
				# add address
				time.sleep(random.randint(45,90))
				adjust_token_53uah3(campaign_data, app_data, device_data,e1,e2)	
				app_data['address_added'] = True

			if random.randint(1,100)<=25 and app_data.get('address_added'):
				# edit address
				time.sleep(random.randint(45,90))
				adjust_token_pnsafa(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=5 and app_data.get('address_added'):

				print 'lat and lon generator'
				lat_lon=current_location(device_data)
				lat_lon_result = util.execute_request(**lat_lon)
				try:
					json_lat_lon_result=json.loads(lat_lon_result.get('data'))
					app_data['lat']=str(json_lat_lon_result.get('geo').get('latitude'))
					app_data['lon']=str(json_lat_lon_result.get('geo').get('longitude'))
				except:
					app_data['lat']="28.6358878"
					app_data['lon']="77.2249742"

				try:
					value=str(app_data.get('lat'))+','+str(app_data.get('lon'))
					geolocator = Nominatim(user_agent="Awok")
					location = geolocator.reverse(value,timeout=None)	
					try:
						result = location.raw.get('address')
						app_data['city'] = result.get('city')
						app_data['postcode'] = result.get('postcode')
						app_data['road'] = result.get('road')
					except:
						app_data['city'] = "Delhi"
						app_data['postcode'] = "1100001"
						app_data['road'] = "Deen Dayal Upadhyay Road"	
				except:
					app_data['city'] = "Delhi"
					app_data['postcode'] = "1100001"
					app_data['road'] = "Deen Dayal Upadhyay Road"		
					
				print "\nself_call_global_awok\n"
				request=global_awok_addresses( campaign_data, device_data, app_data )
				response=util.execute_request(**request)
				try:
					result=json.loads(response.get('data')).get('OUTPUT').get('DATA').get('ADDED_ADDRESS_ID')	
					app_data['address_id'] = result
				except:
					app_data['address_id'] = random.randint(5300000,5309999)

				# delete address
				time.sleep(random.randint(15,45))
				adjust_token_xg18i0(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=10:
				# add card
				time.sleep(random.randint(45,90))
				adjust_token_ldtodm(campaign_data, app_data, device_data,e1,e2)	
				app_data['card_added'] = True

			if random.randint(1,100)<=3 and app_data.get('card_added'):
				# delete card
				time.sleep(random.randint(15,45))
				adjust_token_ybm9oc(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=20:
				# update country
				time.sleep(random.randint(15,45))
				adjust_token_6kd9u3(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=20:	
				# update language
				time.sleep(random.randint(15,45))
				adjust_token_ev0e9f(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=20:	
				# update currency
				time.sleep(random.randint(15,45))
				adjust_token_axw1e9(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=20:	
				# update password
				time.sleep(random.randint(30,60))
				adjust_token_80hhpy(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=20:	
				# customer support
				time.sleep(random.randint(15,45))
				adjust_token_tlebti(campaign_data, app_data, device_data,e1,e2)

			if random.randint(1,100)<=20:	
				# edit_profile
				time.sleep(random.randint(15,45))
				adjust_token_gpw60m(campaign_data, app_data, device_data,e1,e2)	

			if random.randint(1,100)<=5:	
				# reset_password
				time.sleep(random.randint(15,45))
				adjust_token_59hbqm(campaign_data, app_data, device_data,e1,e2)	
				
			if random.randint(1,100)<=5:	
				# event_optional_update
				time.sleep(random.randint(15,45))
				adjust_token_o6teo9(campaign_data, app_data, device_data,e1,e2)				

		if app_data.get('registered')==True:

			if random.randint(1,100) <= 20:
				# apply_coupon
				time.sleep(random.randint(15,45))
				adjust_token_au7iza(campaign_data, app_data, device_data,e1,e2)

			if random.randint(1,100) <= 55 and app_data.get('items_in_cart'):
				# check out initiate
				time.sleep(random.randint(10,30))
				adjust_token_o146jt(campaign_data, app_data, device_data,e1,e2)

				if purchase.isPurchase(app_data,day,advertiser_demand=45):
					time.sleep(random.randint(10,30))
					adjust_token_9e62ye(campaign_data, app_data, device_data,e1,e2)

					if random.randint(1,100)<=10:
						# update_delivery
						time.sleep(random.randint(15,45))
						adjust_token_k1ggvh(campaign_data, app_data, device_data,e1,e2)

					if random.randint(1,100)<=10:
						# update_payment
						time.sleep(random.randint(15,45))
						adjust_token_owi8v2(campaign_data, app_data, device_data,e1,e2)
						
					if random.randint(1,100)<=10:
						# cancel_order
						time.sleep(random.randint(15,45))
						adjust_token_uqr512(campaign_data, app_data, device_data,e1,e2)		

	set_appCloseTime(app_data)			

	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def adjust_token_gqpwus(campaign_data, app_data, device_data,t1=0,t2=0):

	selected_product = random.choice(globals().get('product_data'))
	content_name = selected_product.get('name')
	content_id = selected_product.get('id')
	price = selected_product.get('price')

	print 'Adjust : EVENT______________________________gqpwus'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"product_name":content_name,"product_id":str(content_id),"price":str(price),"user_currency_code":util.get_country_name(device_data.get('locale').get('country')).get('currency')})),event_token='gqpwus',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_9f3xd3(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________9f3xd3'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"result":"success","user_id":"7767120"})),event_token='9f3xd3',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_rh5u92(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________rh5u92'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"result":"success","user_id":"7767120"})),event_token='rh5u92',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_r13ilz(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________r13ilz'

	query = random.choice(["phone", "iphone", "clothes", "jeans", "tv", "movies", "shirts", "earphones", "headphones", "ipad", "pendrive", "books"])

	request=global_awok_query( campaign_data, device_data, app_data, query=query)
	response=util.execute_request(**request)
	try:
		result = json.loads(response.get('data'))
		if result.get("exhaustiveFacetsCount"):
			del result['exhaustiveFacetsCount']
		if result.get("exhaustiveNbHits"):
			del result['exhaustiveNbHits']	
		result = json.dumps(result)	
	except:
		result = json.dumps({})		

	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"search_term":query,"search_filter":"No filter","search_result":result})),event_token='r13ilz',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_r4hyaz(campaign_data, app_data, device_data,t1=0,t2=0):

	globals()['selected_product'] = random.choice(globals().get('product_data'))

	content_name = globals().get('selected_product').get('name')
	content_id = globals().get('selected_product').get('id')
	price = globals().get('selected_product').get('price')

	if not app_data.get('items_in_cart'):
		app_data['items_in_cart'] = [globals()['selected_product']]
	else:
		app_data['items_in_cart'].append(globals()['selected_product'])

	print 'Adjust : EVENT______________________________r4hyaz'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"product_id":str(content_id),"product_name":content_name,"quantity":str(len(app_data['items_in_cart']))})),event_token='r4hyaz',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_bm7rpd(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________bm7rpd'

	item_index = random.randint(0,len(app_data.get('items_in_cart'))-1)
	item_id = app_data.get('items_in_cart')[item_index].get('id')
	del app_data.get('items_in_cart')[item_index]

	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"product_id":item_id})),event_token='bm7rpd',t1=t1,t2=t2)
	util.execute_request(**request)

####################### NEW TOKENS #################################

def adjust_token_ue8b63(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________ue8b63'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='ue8b63',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_4rp0kz(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________4rp0kz'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='4rp0kz',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_6kd9u3(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________6kd9u3'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"country":device_data.get('locale').get('country'),"previous_country":"US"})),event_token='6kd9u3',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_ev0e9f(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________ev0e9f'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"language":device_data.get('locale').get('language')})),event_token='ev0e9f',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_axw1e9(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________axw1e9'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"currency":util.get_country_name(device_data.get('locale').get('country')).get('currency'),"previous_currency":"USD"})),event_token='axw1e9',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_80hhpy(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________80hhpy'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id')})),event_token='80hhpy',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_53uah3(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________53uah3'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"personal_email":app_data.get('user').get('email'),"personal_full_name":app_data.get('user').get('firstname')+" "+app_data.get('user').get('lastname')})),event_token='53uah3',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_pnsafa(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________pnsafa'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"personal_email":app_data.get('user').get('email'),"personal_full_name":app_data.get('user').get('firstname')+" "+app_data.get('user').get('lastname')})),event_token='pnsafa',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_xg18i0(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________xg18i0'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"address_id":app_data.get('address_id')})),event_token='xg18i0',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_ldtodm(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________ldtodm'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_country":device_data.get('locale').get('country'),"result":"success"})),event_token='ldtodm',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_ybm9oc(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________ybm9oc'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_country":device_data.get('locale').get('country')})),event_token='ybm9oc',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_tlebti(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________tlebti'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id')})),event_token='tlebti',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_67fzsj(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________67fzsj'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"page_number":"Homescreen/Page "+str(page_id)})),event_token='67fzsj',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_qibjmh(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________qibjmh'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"quantity":str(random.randint(2,3))})),event_token='qibjmh',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_q6oeqn(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________q6oeqn'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='q6oeqn',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_ll8qzh(campaign_data, app_data, device_data,t1=0,t2=0):

	selected_product = random.choice(globals().get('product_data'))
	content_id = selected_product.get('id')

	print 'Adjust : EVENT______________________________ll8qzh'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"link":"View this amazing product at AWOK.com (https://awok.com/dp-"+str(content_id)+"/)"})),event_token='ll8qzh',t1=t1,t2=t2)
	util.execute_request(**request)

############### FROM SMALLI #####################

def adjust_token_gpw60m(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________gpw60m'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id')})),event_token='gpw60m',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_59hbqm(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________59hbqm'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id')})),event_token='59hbqm',t1=t1,t2=t2)
	util.execute_request(**request)	

def adjust_token_o6teo9(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________o6teo9'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id')})),event_token='o6teo9',t1=t1,t2=t2)
	util.execute_request(**request)				

############# PURCHASE #################

def adjust_token_o146jt(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________o146jt'

	count = str(len(app_data.get('items_in_cart')))

	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"Item count":count})),event_token='o146jt',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_9e62ye(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________9e62ye'

	if device_data.get('country').lower() == 'ae':
		currency = "AED"
	elif device_data.get('country').lower() == 'sa':
		currency = "SAR"
	else:
		currency = random.choice(["AED", "SAR"])

	revenue = 0
	for item in app_data.get('items_in_cart'):
		revenue += eval(str(item.get('price')))	

	global OrderId
	OrderId = util.get_random_string("decimal",8) + "A1"

	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"Currency":currency,"OrderId": OrderId,"Revenue" : revenue })),event_token='9e62ye',t1=t1,t2=t2)
	util.execute_request(**request)

############ FROM SMALLI (PURCHASE) ################	

def adjust_token_au7iza(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________au7iza'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id')})),event_token='au7iza',t1=t1,t2=t2)
	util.execute_request(**request)		

def adjust_token_uqr512(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________uqr512'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"OrderId": OrderId})),event_token='uqr512',t1=t1,t2=t2)
	util.execute_request(**request)		

def adjust_token_k1ggvh(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________k1ggvh'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"OrderId": OrderId})),event_token='k1ggvh',t1=t1,t2=t2)
	util.execute_request(**request)		

def adjust_token_owi8v2(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________owi8v2'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"user_id":app_data.get('user_id'),"OrderId": OrderId})),event_token='owi8v2',t1=t1,t2=t2)
	util.execute_request(**request)					

#########################################################################################
def global_awok( campaign_data, device_data, app_data ):
	register_user(app_data)
	url= "https://global.awok.com/api/v2/users/"
	method= "post"
	
	headers= {       "Accept-Encoding": "gzip",
        "App-Version": campaign_data.get('app_version_code'),
        "Authorization": "",
        "Cache-Control": "no-cache",
        "Content-Type": "application/json",
        "Country": device_data.get('locale').get('country'),
        "Currency": "AED",
        "Device-Name": device_data.get('model'),
        "OS": "android",
        "OS-Version": device_data.get('os_version'),
        "User-Agent": "okhttp/3.10.0",
        "langapi": device_data.get('locale').get('language'),
        "x-environment": "zamurd"}

	params= None

	password = util.get_random_string('char_all',10)

	data= {       "cpassword": password,
        "email": app_data.get('user').get('email'),
        "password": password}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


def home_selfCall( campaign_data, device_data, app_data ):
	url= "https://global.awok.com/api/home/"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "App-Version": campaign_data.get('app_version_code'),
        "Authorization": "",
        "Cache-Control": "no-cache",
        "Country": "",
        "Currency": "AED",
        "Device-Name": device_data.get('model'),
        "OS": "android",
        "OS-Version": device_data.get('sdk'),
        "User-Agent": get_ua(device_data),
        "langapi": device_data.get('locale').get('language'),
        "x-environment": "zamurd"}

	global page_id
	page_id = random.randint(1,10)
	params= {'PAGED':page_id}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def app_measurement( campaign_data, device_data, app_data ):
	url= "http://app-measurement.com/config/app/1%3A797131457563%3Aandroid%3Acc4b2dfb254dba1c"
	method= "head"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent": get_ua(device_data)}

	params= {       "app_instance_id": "66dbc1d658ccadfc86d5aa62d108af89",
        "gmp_version": "17785",
        "platform": "android"}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

####################### self calls added #############################

	
def global_awok_query( campaign_data, device_data, app_data, query=''):
	url= "http://AMQISAHL10-dsn.algolia.net/1/indexes/search_AWOKMP/query"
	method= "post"
	headers= {       
		"Accept-Encoding": "gzip",
        "Content-type": "application/json; charset=UTF-8",
        "User-Agent": "Algolia for Android (3.27.0); Android (6.0)",
        "X-Algolia-API-Key": "1be2331a5f0b81ea0f41945c5f336057",
        "X-Algolia-Application-Id": "AMQISAHL10"
        }

	params= None

	data= {       
			"params": "facets=%5B%22*%22%5D&filters=stock%3E0&hitsPerPage=20&query="+str(query)
		}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def global_awok_users( campaign_data, device_data, app_data ):
	url= "https://global.awok.com/api/v2/users/"
	method= "get"
	headers= {       
        "Authorization": app_data.get('token'),
        "Accept-Encoding": "gzip",
        "App-Version": campaign_data.get('app_version_code'),
        "Cache-Control": "no-cache",
        "Content-Type": "application/json",
        "Country": device_data.get('locale').get('country'),
        "Currency": "AED",
        "Device-Name": device_data.get('model'),
        "OS": "android",
        "OS-Version": device_data.get('os_version'),
        "User-Agent": "okhttp/3.10.0",
        "langapi": device_data.get('locale').get('language'),
        "x-environment": "zamurd"
        }

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

def global_awok_addresses( campaign_data, device_data, app_data ):
	register_user(app_data)
	url= "https://global.awok.com//api/addresses_v0.5/"
	method= "post"
	headers= {       
		"Authorization": app_data.get('token'),
        "Accept-Encoding": "gzip",
        "App-Version": campaign_data.get('app_version_code'),
        "Cache-Control": "no-cache",
        "Content-Type": "application/json",
        "Country": device_data.get('locale').get('country'),
        "Currency": "AED",
        "Device-Name": device_data.get('model'),
        "OS": "android",
        "OS-Version": device_data.get('os_version'),
        "User-Agent": "okhttp/3.10.0",
        "langapi": device_data.get('locale').get('language'),
        "x-environment": "zamurd"
        }

	params= None

	data= {       
		"CITY": app_data.get('city'),
        "LOCATION": util.get_random_string(type='decimal',size=4),
        "POSTAL_CODE": app_data.get('postcode'),
        "Personal_Email": app_data.get('user').get('email'),
        "Personal_Full_Name": app_data.get('user').get('firstname')+' '+app_data.get('user').get('lastname'),
        "Personal_Street": app_data.get('road'),
        "Personal_Telephone": util.get_random_string(type='decimal',size=10)
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}	

def current_location(device_data):
	url='http://lumtest.com/myip.json'
		
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}	

###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################

def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	pushtoken(app_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())

	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src" : "service",
		"hardware_name" : device_data.get('hardware'),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"language" : device_data.get('locale').get('language'),
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if isOpen:
		def_(app_data,'subsession_count')
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('appCloseTime')-app_data.get('sess_len')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('appCloseTime')-app_data.get('sess_len')
		app_data['sess_len']=int(time.time())

	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'session')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,t3=0,t4=0,callback_params=None,partner_params=None):
	pushtoken(app_data)
	reftag = ''
	if app_data.get('referrer'):
		temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
		if temp_b.get('adjust_reftag'):
			reftag = temp_b.get('adjust_reftag')[0]
	sdk_clk_time=get_date(app_data,device_data)
	time.sleep(random.randint(t3,t4))
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src" : "service",
		"hardware_name" : device_data.get('hardware'),
		"install_begin_time" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time")),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"referrer" : "adjust_reftag=cPkNoNV4fGRtU&utm_source=Optimise&utm_campaign=UAE_June&utm_content=unknown&utm_term=1125623_",
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : 1,
		"session_length" : session_length,
		"source" : source,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		if click_time:
			data['click_time']=sdk_clk_time
		elif data.get("click_time"):
			del data['click_time']
		# del data['session_length']
		# del data['subsession_count']
		# del data['time_spent']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		if click_time:
			data['click_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("click_time"))
		elif data.get("click_time"):
			del data['click_time']
		
		if data.get('raw_referrer'):
			del data['raw_referrer']
		
		data['install_begin_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time"))
		# del data['last_interval']

	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'click')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,t1=0,t2=0,initiate = ''):
	inc_(app_data,'subsession_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"created_at" : created_at,
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src" : "service",
		"initiated_by" : initiate,
		"needs_response_details" : "1",
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"sent_at" : sent_at,
		"tracking_enabled" : "1",
		'push_token' : app_data.get('pushtoken'),

		}
	data = {

		}
	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('gps_adid'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

def adjust_sdkinfo(campaign_data, app_data, device_data):
	url 	= 'http://app.adjust.com/sdk_info'
	method 	='post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:get_date(app_data,device_data),
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'needs_response_details':'1',
		'push_token'			:app_data.get('pushtoken'),
		'sent_at'				:get_date(app_data,device_data),
		'source'				:'push',
		'tracking_enabled'		:'1',
		'gps_adid_src'			: 'service'

		}

	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'info')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"event_count" : app_data.get('event_count'),
		"event_token" : event_token,
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src" : "service",
		"hardware_name" : device_data.get('hardware'),
		"language" : device_data.get('locale').get('language'),
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"partner_params" : partner_params,
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"session_length" : session_length,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		'push_token' : app_data.get('pushtoken'),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	return app_data.get('android_uuid')


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']='f0L4WFS5AEk:'+util.get_random_string('google_token',140)
	return app_data.get('pushtoken')

def get_auth(device_data,app_data,created_at,gps_adid,activity_type):
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activity_type)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'

def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"


