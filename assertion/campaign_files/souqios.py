from sdk import util, purchase,installtimenew
import json,time,uuid,hashlib,random,string,base64,urllib
from sdk import getsleep
import clicker
import Config

campaign_data = {
				'package_name' : 'com.souq.souq.com',
				'app_id'	   : '675000850',
				'sdk' : 'iOS',
				'app_version_name' : '5.64',#'5.63',#'5.62.2',#'5.62.1',#'5.62',#'5.61',#'5.60',#'5.59',#5.58, '5.57',#'5.56',#'5.55.2',#'5.55',#'5.53',#'5.50',#'5.49',#'5.48.1',#'5.48',#'5.46',#'5.45.1',5.51, 5.52, 5.54
				'app_version_code' : '211',#'207',#'205',#'204',#'198',#'196',#'194',#'191',#187, '180',#'178',#'176',#'174',#'169',#'162',#'161',#'155',#'150',#'149',164, 167, 173
				'supported_os':'9.0',
				'ctr':6,
				'app_name' : 'Souq',
				'supported_countries': 'WW',
				'device_targeting':True,
				'tracker':'apsalar',
				'app_size':168.1,#156.4,#156.3,#156.2,#155.8,#156.4,
				'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
				'apsalar':{
					'version':'Singular/8.4.2',#'Apsalar/7.3.1',
					'key':'amodi',
					'secret':'pm3HpNZK'
				},
				'retention':{
							1:25,
							2:25,
							3:24,
							4:24,
							5:23,
							6:22,
							7:20,
							8:20,
							9:20,
							10:19,
							11:19,
							12:19,
							13:18,
							14:18,
							15:17,
							16:16,
							17:15,
							18:14,
							19:13,
							20:13,
							21:13,
							22:13,
							23:11,
							24:10,
							25:10,
							26:9,
							27:8,
							28:8,
							29:6,
							30:6,
							31:5,
							32:5,
							33:4,
							34:4,
							35:3							
				}
}


########### Already reduced app_data , data is retained in open (view cart)

def install(app_data, device_data):
	print 'Please wait installing...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)

	# [Product_name,Product_id,EAN,Category_name,Category_id,Brand,Product_price,Seller_name,unit_id,Super_Category,Category_Group]

	global product_details
	product_details=[['Nutro Nice Coconut Biscuits - 50 gm', '36780221', '2724644557475', 'Snacks', '582', 'Nutro', '0.800000011920929', 'UNIONCOOP', '2809100582', 'FMCG', 'grocery'], ['Gashi Invisible Star Socks for Unisex', '23619660', '2724463166780', 'Accessories', '466', 'Gashi', '4', 'gashistore', '24372600466', 'Fashion', 'clothing_accessories'], ['Timberland Black Riptide Galloper Full Grain Pull On Boot For Men', '11612207', '2724337770846', 'Boots', '469', 'Timberland', '273', 'Timberland-UAE', '4484000469', 'Fashion', 'footwear'], ['Evenflo Round Trip Light Weight Stroller - Multi Color, 8 Kg', '38885782', '2724668988057', 'Baby Gears', '331', 'Evenflo', '427.489990234375', 'dod_uae', '11794300331', 'LifeStyle', 'baby'], ["Gillette Mach3 men's razor blade refills, 4 count", '11053697', '2724331899192', "Men's Grooming", '299', 'Gillette', '31.99000000000001', 'SouqSuperStore', '1462800299', 'LifeStyle', 'health_personal_care'], ['Eau de Lacoste L.12.12 Yellow Jaune by Lacoste for Men - Eau de Toilette, 100ml', '7781956', '2724298226277,2724299102778', 'Perfumes & Fragrances', '478', 'Lacoste', '120.9', 'dod_uae', '62291000478', 'LifeStyle', 'perfumes'], ['Karcher - High Pressure Washer K 4 Compact - 16373110', '7451210', '2724294966641', 'Power Tools', '97', 'Karcher', '787.8200000000003', 'dod_uae', '2617400097', 'LifeStyle', 'tools_home_improvements'], ['Tefal Tempo Flame 14pcs Set - C0459362', '22416587', '2724446713031', 'Cookware', '425', 'Tefal', '461.4899999999999', 'dod_uae', '14741100425', 'LifeStyle', 'kitchen'], ['FINE 4X More Absorbent Super Kitchen Paper Towel Tissue Rolls - Pack of 2 Rolls, 45 Sheets x 3 Ply', '19249049', '2724414486998', 'Plastic & Paper Products', '579', 'Fine', '9', 'FineBaby', '1497300579', 'FMCG', 'grocery']]


	app_data['signed_in']=False
	app_data['rr']=random.randint(0,len(product_details)-1)


	#user_agent(campaign_data,app_data,device_data)
	generate_gcm_id(app_data)
	get_device_token(app_data)
	Customer_id(app_data)
	omni_id(app_data)
	order_id(app_data)
		
	if not app_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()

	if not app_data.get('Device_id'):
		app_data['Device_id'] = str(uuid.uuid4()).upper()

	if not app_data.get('u'):
		app_data['u'] = str(uuid.uuid4()).upper()		

	print '----------------------------- lat and lon generator -----------------------------'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	try:
		json_lat_lon_result=json.loads(lat_lon_result.get('data'))
		app_data['lat']=str(json_lat_lon_result.get('geo').get('latitude'))
		app_data['lon']=str(json_lat_lon_result.get('geo').get('longitude'))
	except:
		app_data['lat']=''
		app_data['lon']=''

	print '----------------------------- APSALAR Resolve ---------------------------'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data,k='IDFA')
	app_data['api_hit_time'] = time.time()
	util.execute_request(**apsalar_Resolve)

	print '----------------------------- APSALAR Resolve ---------------------------'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data,k='IDFV')
	util.execute_request(**apsalar_Resolve)	
	
	print '----------------------------- APSALAR START -----------------------------'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_start)

	eve_val=json.dumps({"omniturevisitorid":app_data.get('omni_id')})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Initialization',eve_val)
	util.execute_request(**apsalar_event)	

	eve_val=json.dumps({"Logged_in":"false","Country":"","Customer_id":"","App_Version":campaign_data.get('app_version_name'),"Page_name":"App Launch","Advertising_ID":app_data.get('idfa_id'),"OS_Version":device_data.get('os_version'),"Language":"","Device_token":"","Device_id":app_data.get('Device_id'),"Source_of_visit":"Direct","OS":"iOS"})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Launch',eve_val)
	util.execute_request(**apsalar_event)

	# time.sleep(random.randint(8,12))
	# eve_val=json.dumps({"omniturevisitorid":app_data.get('omni_id')})
	# print '----------------------------- APSALAR EVENT -----------------------------'	
	# apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'registration',eve_val)
	# util.execute_request(**apsalar_event)

	
	eve_val=json.dumps({"Logged_in":"true","Country":"","OS":"iOS","App_Version":campaign_data.get('app_version_name'),"Customer_id":app_data.get('c_id'),"Language":"","Device_token":app_data.get('d_token'),"Device_id":app_data.get('Device_id'),"Source_of_visit":"Direct","OS_Version":device_data.get('os_version'),"Page_name":"WelcomePage"})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Launch and Page View Event',eve_val)
	util.execute_request(**apsalar_event)

	time.sleep(random.randint(8,12))
	eve_val=json.dumps({"Version3.1":{"iad-attribution":"false"}})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__iAd_Attribution__',eve_val)
	util.execute_request(**apsalar_event)

	if random.randint(1,100)<=90:

		time.sleep(random.randint(5,10))
		homepage(app_data,device_data)

		if random.randint(1,100)<=78:
			time.sleep(random.randint(60,100))
			signup(app_data,device_data)
			app_data['signed_in']=True
		

		pageviewevent(app_data,device_data)

		if random.randint(1,100)<=89:
			time.sleep(random.randint(10,20))
			search(app_data,device_data)		

		for i in range(0,random.randint(1,3)):
			app_data['rr']=random.randint(0,len(product_details)-1)
			if random.randint(1,100)<=89:
				time.sleep(random.randint(10,40))
				productviewevent(app_data,device_data,'PageView')
		
		time.sleep(random.randint(8,12))	
		categoryviewed(app_data,device_data)


		if random.randint(1,100)<=85:

			app_data['final_list']=[]
			# adapter=[]
			for i in range(0,random.randint(1,3)):
				choose_type=random.choice([1,1,1,1,2,2,2,2,3,3,3,3])

				print choose_type
				app_data['rr']=random.randint(0,len(product_details)-1)
				print app_data['rr']
				app_data['final_list'].append(product_details[app_data['rr']])

				if choose_type==1:
					time.sleep(random.randint(10,40))
					addtocart(app_data,device_data)
				elif choose_type==2:
					time.sleep(random.randint(10,40))
					wishlist(app_data,device_data)
				else:
					time.sleep(random.randint(10,60))
					viewcart(app_data,device_data)
					synccart(app_data,device_data)			

			time.sleep(random.randint(15,60))
			heartbeat(app_data,device_data)


			if random.randint(1,100)<=83:
				time.sleep(random.randint(5,30))
				checkout(app_data,device_data)

				if random.randint(1,100)<=24:
					time.sleep(random.randint(15,30))
					couponapplied(app_data,device_data)

				if random.randint(1,100)<=60:
					time.sleep(random.randint(15,30))	
					paymentevent(app_data,device_data)	
					paymenteventoption(app_data,device_data)	

					if app_data.get('signed_in')==True:
						if purchase.isPurchase(app_data,day=1,advertiser_demand=10):
							time.sleep(random.randint(20,30))
							transaction(app_data,device_data)
							

	if random.randint(1,100)<=30 and app_data.get('signed_in')==True:
		time.sleep(random.randint(5,6))
		sigout(app_data,device_data)
		app_data['signed_out']=True
		app_data['logged']=False

		if random.randint(1,100)<=20 and app_data.get('signed_out')==True:
			sigin(app_data,device_data)
			app_data['logged']=True
			app_data['signed_out']=False

	return {'status':True}
	
	
def open(app_data, device_data,day):

	if not app_data.get('times'):
		print 'Please wait installing...'
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)

	print "inside open"
	generate_gcm_id(app_data)
	get_device_token(app_data)
	Customer_id(app_data)
	omni_id(app_data)
	order_id(app_data)

	if not app_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()

	if not app_data.get('Device_id'):
		app_data['Device_id'] = str(uuid.uuid4()).upper()

	if not app_data.get('u'):
		app_data['u'] = str(uuid.uuid4()).upper()	

	if not app_data.get('signed_in'):
		app_data['signed_in']=False

	if not app_data.get('signed_out'):
		app_data['signed_out']=False

	if not app_data.get('logged'):
		app_data['logged']=False	

	if not app_data.get('final_list'):
		app_data['final_list']=[]	

	global product_details
	product_details=[['Nutro Nice Coconut Biscuits - 50 gm', '36780221', '2724644557475', 'Snacks', '582', 'Nutro', '0.800000011920929', 'UNIONCOOP', '2809100582', 'FMCG', 'grocery'], ['Gashi Invisible Star Socks for Unisex', '23619660', '2724463166780', 'Accessories', '466', 'Gashi', '4', 'gashistore', '24372600466', 'Fashion', 'clothing_accessories'], ['Timberland Black Riptide Galloper Full Grain Pull On Boot For Men', '11612207', '2724337770846', 'Boots', '469', 'Timberland', '273', 'Timberland-UAE', '4484000469', 'Fashion', 'footwear'], ['Evenflo Round Trip Light Weight Stroller - Multi Color, 8 Kg', '38885782', '2724668988057', 'Baby Gears', '331', 'Evenflo', '427.489990234375', 'dod_uae', '11794300331', 'LifeStyle', 'baby'], ["Gillette Mach3 men's razor blade refills, 4 count", '11053697', '2724331899192', "Men's Grooming", '299', 'Gillette', '31.99000000000001', 'SouqSuperStore', '1462800299', 'LifeStyle', 'health_personal_care'], ['Eau de Lacoste L.12.12 Yellow Jaune by Lacoste for Men - Eau de Toilette, 100ml', '7781956', '2724298226277,2724299102778', 'Perfumes & Fragrances', '478', 'Lacoste', '120.9', 'dod_uae', '62291000478', 'LifeStyle', 'perfumes'], ['Karcher - High Pressure Washer K 4 Compact - 16373110', '7451210', '2724294966641', 'Power Tools', '97', 'Karcher', '787.8200000000003', 'dod_uae', '2617400097', 'LifeStyle', 'tools_home_improvements'], ['Tefal Tempo Flame 14pcs Set - C0459362', '22416587', '2724446713031', 'Cookware', '425', 'Tefal', '461.4899999999999', 'dod_uae', '14741100425', 'LifeStyle', 'kitchen'], ['FINE 4X More Absorbent Super Kitchen Paper Towel Tissue Rolls - Pack of 2 Rolls, 45 Sheets x 3 Ply', '19249049', '2724414486998', 'Plastic & Paper Products', '579', 'Fine', '9', 'FineBaby', '1497300579', 'FMCG', 'grocery']]



	app_data['rr']=random.randint(0,len(product_details)-1)	
	

	print '----------------------------- APSALAR Resolve ---------------------------'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data , k='IDFA')
	util.execute_request(**apsalar_Resolve)

	print '----------------------------- APSALAR Resolve ---------------------------'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data, k='IDFA')
	util.execute_request(**apsalar_Resolve)	
	
	print '----------------------------- APSALAR START -----------------------------'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_start)

	time.sleep(random.randint(8,12))
	# eve_val=json.dumps({"omniturevisitorid":app_data.get('omni_id')})
	# print '----------------------------- APSALAR EVENT -----------------------------'	
	# apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'registration',eve_val)
	# util.execute_request(**apsalar_event)
	# if random.randint(1,100)<=90:

	eve_val=json.dumps({"omniturevisitorid":app_data.get('omni_id')})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Initialization',eve_val)
	util.execute_request(**apsalar_event)	

	eve_val=json.dumps({"Logged_in":"false","Country":"","Customer_id":"","App_Version":campaign_data.get('app_version_name'),"Page_name":"App Launch","Advertising_ID":app_data.get('idfa_id'),"OS_Version":device_data.get('os_version'),"Language":"","Device_token":"","Device_id":app_data.get('Device_id'),"Source_of_visit":"Direct","OS":"iOS"})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Launch',eve_val)
	util.execute_request(**apsalar_event)
	time.sleep(random.randint(5,10))
	homepage(app_data,device_data)

	if random.randint(1,100)<=75 and app_data.get('signed_in')==False:
		time.sleep(random.randint(60,100))
		signup(app_data,device_data)
		app_data['signed_in']=True
	

	if random.randint(1,100)<=90:			

		pageviewevent(app_data,device_data)

		if random.randint(1,100)<=89:
			time.sleep(random.randint(10,20))
			search(app_data,device_data)		

		for i in range(0,random.randint(1,3)):
			app_data['rr']=random.randint(0,len(product_details)-1)
			if random.randint(1,100)<=89:
				time.sleep(random.randint(10,40))
				productviewevent(app_data,device_data,'PageView')
		


		if random.randint(1,100)<=85:

			# app_data['final_list']=[]
			# adapter=[]
			for i in range(0,random.randint(1,3)):
				choose_type=random.choice([1,1,1,1,2,2,2,2,3,3,3,3])

				print choose_type
				app_data['rr']=random.randint(0,len(product_details)-1)
				print app_data['rr']
				app_data['final_list'].append(product_details[app_data['rr']])

				if choose_type==1:
					time.sleep(random.randint(10,40))
					addtocart(app_data,device_data)
				elif choose_type==2:
					time.sleep(random.randint(10,40))
					wishlist(app_data,device_data)
				else:
					time.sleep(random.randint(10,60))
					viewcart(app_data,device_data)
					synccart(app_data,device_data)				

			time.sleep(random.randint(10,40))
			heartbeat(app_data,device_data)


			if random.randint(1,100)<=83:
				time.sleep(random.randint(5,30))
				checkout(app_data,device_data)

				if random.randint(1,100)<=24:
					time.sleep(random.randint(15,30))
					couponapplied(app_data,device_data)

				if random.randint(1,100)<=60:
					time.sleep(random.randint(15,30))	
					paymentevent(app_data,device_data)	
					paymenteventoption(app_data,device_data)	

					if app_data.get('signed_in')==True:
						if purchase.isPurchase(app_data,day=day,advertiser_demand=35):
							time.sleep(random.randint(20,30))
							transaction(app_data,device_data)
							


	if random.randint(1,100)<=30 and app_data.get('logged')==True:
		time.sleep(random.randint(5,6))
		sigout(app_data,device_data)
		app_data['signed_out']=True
		app_data['logged']=False

	if random.randint(1,100)<=20 and app_data.get('signed_out')==True:
		sigin(app_data,device_data)
		app_data['logged']=True
		app_data['signed_out']=False


	return {'status':True}


def homepage(app_data,device_data):
	
	eve_val=json.dumps({"Device_token":app_data.get('d_token'),"Device_id":app_data.get('Device_id'),"Country":device_data.get('locale').get('country').lower(),"Page_name":"Home Page","Language":device_data.get('locale').get('language').lower(),"Advertising_ID":app_data.get('u'),"Logged_in":"false","OS":"iOS","App_Version":campaign_data.get('app_version_name'),"OS_Version":device_data.get('os_version'),"Customer_id":"","Source_of_visit":"Direct"})
	print '----------------------------- APSALAR EVENT -----------Home page------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Home page',eve_val)
	util.execute_request(**apsalar_event)

def signup(app_data,device_data):
	
	eve_val=json.dumps({"Device_token":app_data.get('d_token'),"Device_id":app_data.get('Device_id'),"Country":device_data.get('locale').get('country').lower(),"Page_name":"MyAccount","Language":device_data.get('locale').get('language').lower(),"Advertising_ID":app_data.get('u'),"Logged_in":str(app_data.get('signed_in')).lower(),"OS":"iOS","App_Version":campaign_data.get('app_version_name'),"OS_Version":device_data.get('os_version'),"Customer_id":app_data.get('c_id'),"Source_of_visit":"Direct"})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Sign up',eve_val)
	util.execute_request(**apsalar_event)



def heartbeat(app_data,device_data):
	eve_val=json.dumps({})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'heartbeat',eve_val)
	util.execute_request(**apsalar_event)	


def pageviewevent(app_data,device_data):

	pages_available=["DEALS-Curation-CampaignPage-","DEALS-Curation-TagPage-Consumer Electronics","DEALS-Curation-TagPage-3M ","DEALS-Curation-CampaignPage-Home Services","DEALS-Curation-CampaignPage-Top Choice","Deals-Curation-CollectionPage-Pay Week Sale""DEALS-Curation-CampaignPage-Best Sellers","DEALS-Curation-CampaignPage-Laptops & Accessories","FMCG-Curation-CampaignPage-Pet Food | Dogs, Cats & more","Deals-Curation-DealsHome","DEALS-Curation-CampaignPage-Souq Fashion","DEALS-Curation-CampaignPage-Amazon Global Store"]

	

	for i in range(0,random.randint(2,len(pages_available)-1)):
		time.sleep(random.randint(10,20))
		choose_me=random.choice([1,1,1,1,2,2])

		if choose_me==1:

			print "pae_name "+str(pages_available[i])

			if pages_available[i]=='DEALS-Curation-CampaignPage-':
				Source_of_visit="souq:\/\/mobile\/curation?curation_type=DEALS&campaign_id=11&ln="+device_data.get('locale').get('language').lower()+"&co="+device_data.get('locale').get('country').lower()+""
			elif pages_available[i]=='Deals-Curation-CollectionPage-Pay Week Sale':
				Source_of_visit="souq:\/\/mobile\/curation?curation_type=DEALS&collection_id=488&ln="+device_data.get('locale').get('language').lower()+"&co="+device_data.get('locale').get('country').lower()+""
			else:
				Source_of_visit="Direct"

			eve_val=urllib.quote(json.dumps({"OS":"iOS","Advertising_ID":app_data.get('u'),"Device_id":app_data.get('Device_id'),"Page_name":pages_available[i],"OS_Version":device_data.get('os_version'),"Source_of_visit":Source_of_visit,"App_Version":campaign_data.get('app_version_name'),"Device_token":app_data.get('d_token'),"Customer_id":app_data.get('c_id'),"Country":device_data.get('locale').get('country').lower(),"Logged_in":str(app_data.get('signed_in')).lower(),"Language":device_data.get('locale').get('language')}))	
		else:
			eve_val=urllib.quote(json.dumps({"Category_name":product_details[app_data.get('rr')][3],"Category_id":product_details[app_data.get('rr')][4]}))
	
	print '----------------------------- APSALAR EVENT ---------------------Page View Event--------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Page View Event',eve_val)
	util.execute_request(**apsalar_event)	


def search(app_data,device_data):	
	
	eve_val=json.dumps({"Logged_in":str(app_data.get('signed_in')).lower(),"Country":device_data.get('locale').get('country').lower(),"Customer_id":app_data.get('c_id'),"App_Version":campaign_data.get('app_version_name'),"Page_name":"SearchResult:Page","Advertising_ID":app_data.get('u'),"OS_Version":device_data.get('os_version'),"Language":device_data.get('locale').get('language').lower(),"Device_token":app_data.get('d_token'),"Device_id":app_data.get('Device_id'),"Source_of_visit":"Direct","OS":"iOS"})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Search',eve_val)
	util.execute_request(**apsalar_event)


	eve_val=json.dumps({"Advertising_ID":app_data.get('u'),"Currency":"AED","Logged_in":str(app_data.get('signed_in')).lower(),"OS":"iOS","Device_id":app_data.get('Device_id'),"Keyword":"h","Search_results_count":"0","Customer_id":app_data.get('c_id'),"Source_of_visit":"Direct","Language":device_data.get('locale').get('language').lower(),"App_Version":campaign_data.get('app_version_name')})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Search Query Result',eve_val)
	util.execute_request(**apsalar_event)


def productviewevent(app_data,device_data,event_name):

	eve_val=urllib.quote(json.dumps({"Category_name":product_details[app_data.get('rr')][3],"Seller_name":product_details[app_data.get('rr')][7],"Product_id":product_details[app_data.get('rr')][1],"Product_name":product_details[app_data.get('rr')][0],"EAN":product_details[app_data.get('rr')][2],"Unit_id":product_details[app_data.get('rr')][8],"Language":device_data.get('locale').get('language').lower(),"Category_id":product_details[app_data.get('rr')][4],"Has_extended_warranty":"false","Currency":"AED","Page_name":event_name,"Product_price":float(product_details[app_data.get('rr')][6]),"Brand":product_details[app_data.get('rr')][5],"Country":device_data.get('locale').get('country').lower(),"Category_Group":"beauty","Super_Category":product_details[app_data.get('rr')][9],"OS":"iOS","Logged_in":str(app_data.get('signed_in')).lower(),"Advertising_ID":app_data.get('u'),"Device_id":app_data.get('Device_id'),"Customer_id":app_data.get('c_id'),"Source_of_visit":"Direct","OS_Version":device_data.get('os_version'),"Device_token":app_data.get('d_token')}))
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Product View Event',eve_val)
	util.execute_request(**apsalar_event)	



def categoryviewed(app_data,device_data):
	
	eve_val=json.dumps({"Category_id":product_details[app_data.get('rr')][4],"Category_name":product_details[app_data.get('rr')][3]})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Category viewed',eve_val)
	util.execute_request(**apsalar_event)	

def addtocart(app_data,device_data):

	eve_val=json.dumps({"Seller_name":product_details[app_data.get('rr')][7],"Product_id":product_details[app_data.get('rr')][1],"Product_name":product_details[app_data.get('rr')][0],"EAN":product_details[app_data.get('rr')][2],"Language":device_data.get('locale').get('language').lower(),"Product_price":float(product_details[app_data.get('rr')][6]),"Quantity":1,"Product_available_quantity":10,"Cart_size":0,"Currency":"AED","Brand":product_details[app_data.get('rr')][5],"Page_name":"productview","Category_name":product_details[app_data.get('rr')][3],"Device_id":app_data.get('Device_id'),"App_Version":campaign_data.get('app_version_name'),"Advertising_ID":app_data.get('u'),"Logged_in":str(app_data.get('signed_in')).lower(),"OS_Version":device_data.get('os_version'),"Country":device_data.get('locale').get('country').lower(),"Device_token":app_data.get('d_token'),"OS":"iOS","Customer_id":app_data.get('c_id'),"Source_of_visit":"Direct","Category_id":product_details[app_data.get('rr')][4]})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Add to Cart Event',eve_val)
	util.execute_request(**apsalar_event)

def wishlist(app_data,device_data):


	eve_val=json.dumps({"Currency":"AED","OS":"iOS","Category_Group":product_details[app_data.get('rr')][10],"Advertising_ID":app_data.get('u'),"Product_price":float(product_details[app_data.get('rr')][6]),"Logged_in":str(app_data.get('signed_in')).lower(),"Customer_id":app_data.get('c_id'),"Country":device_data.get('locale').get('country').lower(),"Language":device_data.get('locale').get('language').lower(),"Seller_name":product_details[app_data.get('rr')][7],"Unit_id":product_details[app_data.get('rr')][8],"Product_id":product_details[app_data.get('rr')][1],"App_Version":campaign_data.get('app_version_name'),"Category_id":product_details[app_data.get('rr')][4],"Quantity":1,"EAN":product_details[app_data.get('rr')][2],"Brand":product_details[app_data.get('rr')][5],"Device_id":app_data.get('Device_id'),"Device_token":app_data.get('d_token'),"Super_Category":product_details[app_data.get('rr')][9],"Source_of_visit":"Direct","OS_Version":device_data.get('os_version')})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Wishlist',eve_val)
	util.execute_request(**apsalar_event)


def viewcart(app_data,device_data):	
		
	Product_name=''
	Product_id=''
	EAN=''		
	Category_name=''
	Category_id=''
	Brand=''
	Product_price=''
	Seller_name	=''
	Unit_id=''
	Super_Category=''
	Category_Group=''
	total_price=0.0

	for i in range(len(app_data.get('final_list'))):	
		if i == len(app_data.get('final_list'))-1:	
			Product_name+=app_data.get('final_list')[i][0]
			Product_id+=app_data.get('final_list')[i][1]
			EAN+=app_data.get('final_list')[i][2]
			Category_name+=app_data.get('final_list')[i][3]
			Category_id+=app_data.get('final_list')[i][4]
			Brand+=app_data.get('final_list')[i][5]
			Product_price+=app_data.get('final_list')[i][6]
			Seller_name+=app_data.get('final_list')[i][7]
			Unit_id+=app_data.get('final_list')[i][8]
			Super_Category+=app_data.get('final_list')[i][9]
			Category_Group+=app_data.get('final_list')[i][10]	
		else:
			Product_name+=app_data.get('final_list')[i][0]+'|'
			Product_id+=app_data.get('final_list')[i][1]+'|'
			EAN+=app_data.get('final_list')[i][2]+'|'
			Category_name+=app_data.get('final_list')[i][3]+'|'
			Category_id+=app_data.get('final_list')[i][4]+'|'
			Brand+=app_data.get('final_list')[i][5]+'|'
			Product_price+=app_data.get('final_list')[i][6]+'|'
			Seller_name+=app_data.get('final_list')[i][7]+'|'
			Unit_id+=app_data.get('final_list')[i][8]+'|'
			Super_Category+=app_data.get('final_list')[i][9]+'|'
			Category_Group+=app_data.get('final_list')[i][10]+'|'
				

		total_price+=float(app_data.get('final_list')[i][6])



	eve_val=json.dumps({"Logged_in":str(app_data.get('signed_in')).lower(),"OS_Version":device_data.get('os_version'),"Seller_name":Seller_name,"Source_of_visit":"Direct","Page_name":"Cart","Language":device_data.get('locale').get('language').lower(),"Product_id":Product_id,"Product_available_quantity":"7","Device_token":app_data.get('d_token'),"Device_id":app_data.get('Device_id'),"Customer_id":app_data.get('c_id'),"EAN":EAN,"Product_name":Product_name,"Quantity":"1","Advertising_ID":app_data.get('u'),"App_Version":campaign_data.get('app_version_name'),"Country":device_data.get('locale').get('country').lower(),"Cart_size":len(app_data.get('final_list')),"OS":"iOS","Brand":Brand,"Product_price":Product_price,"Currency":"AED","Cart_total_price":total_price,"Super_Category":Super_Category})


	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'View Cart',eve_val)
	util.execute_request(**apsalar_event)

	
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Sync Cart',eve_val)
	util.execute_request(**apsalar_event)


def synccart(app_data,device_data):

	Product_name=''
	Product_id=''
	EAN=''		
	Category_name=''
	Category_id=''
	Brand=''
	Product_price=''
	Seller_name	=''
	Unit_id=''
	Super_Category=''
	Category_Group=''
	total_price=0.0

	for i in range(len(app_data.get('final_list'))):	
		if i == len(app_data.get('final_list'))-1:	
			Product_name+=app_data.get('final_list')[i][0]
			Product_id+=app_data.get('final_list')[i][1]
			EAN+=app_data.get('final_list')[i][2]
			Category_name+=app_data.get('final_list')[i][3]
			Category_id+=app_data.get('final_list')[i][4]
			Brand+=app_data.get('final_list')[i][5]
			Product_price+=app_data.get('final_list')[i][6]
			Seller_name+=app_data.get('final_list')[i][7]
			Unit_id+=app_data.get('final_list')[i][8]
			Super_Category+=app_data.get('final_list')[i][9]
			Category_Group+=app_data.get('final_list')[i][10]	
		else:
			Product_name+=app_data.get('final_list')[i][0]+'|'
			Product_id+=app_data.get('final_list')[i][1]+'|'
			EAN+=app_data.get('final_list')[i][2]+'|'
			Category_name+=app_data.get('final_list')[i][3]+'|'
			Category_id+=app_data.get('final_list')[i][4]+'|'
			Brand+=app_data.get('final_list')[i][5]+'|'
			Product_price+=app_data.get('final_list')[i][6]+'|'
			Seller_name+=app_data.get('final_list')[i][7]+'|'
			Unit_id+=app_data.get('final_list')[i][8]+'|'
			Super_Category+=app_data.get('final_list')[i][9]+'|'
			Category_Group+=app_data.get('final_list')[i][10]+'|'
				

		total_price+=float(app_data.get('final_list')[i][6])


	eve_val=json.dumps({"Logged_in":str(app_data.get('signed_in')).lower(),"OS_Version":device_data.get('os_version'),"Seller_name":Seller_name,"Source_of_visit":"Direct","Page_name":"Cart","Language":device_data.get('locale').get('language').lower(),"Product_id":Product_id,"Product_available_quantity":"7","Device_token":app_data.get('d_token'),"Device_id":app_data.get('Device_id'),"Customer_id":app_data.get('c_id'),"EAN":EAN,"Product_name":Product_name,"Quantity":"1","Advertising_ID":app_data.get('u'),"App_Version":campaign_data.get('app_version_name'),"Country":device_data.get('locale').get('country').lower(),"Cart_size":len(app_data.get('final_list')),"OS":"iOS","Brand":Brand,"Product_price":Product_price,"Currency":"AED","Cart_total_price":total_price,"Super_Category":Super_Category})
	
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Sync Cart',eve_val)
	util.execute_request(**apsalar_event)




def checkout(app_data,device_data):	
	

	Product_name=''
	Product_id=''
	EAN=''		
	Category_name=''
	Category_id=''
	Brand=''
	Product_price=''
	Seller_name	=''
	Unit_id=''
	Super_Category=''
	Category_Group=''
	total_price=0.0

	for i in range(len(app_data.get('final_list'))):	
		if i == len(app_data.get('final_list'))-1:	
			Product_name+=app_data.get('final_list')[i][0]
			Product_id+=app_data.get('final_list')[i][1]
			EAN+=app_data.get('final_list')[i][2]
			Category_name+=app_data.get('final_list')[i][3]
			Category_id+=app_data.get('final_list')[i][4]
			Brand+=app_data.get('final_list')[i][5]
			Product_price+=app_data.get('final_list')[i][6]
			Seller_name+=app_data.get('final_list')[i][7]
			Unit_id+=app_data.get('final_list')[i][8]
			Super_Category+=app_data.get('final_list')[i][9]
			Category_Group+=app_data.get('final_list')[i][10]	
		else:
			Product_name+=app_data.get('final_list')[i][0]+'|'
			Product_id+=app_data.get('final_list')[i][1]+'|'
			EAN+=app_data.get('final_list')[i][2]+'|'
			Category_name+=app_data.get('final_list')[i][3]+'|'
			Category_id+=app_data.get('final_list')[i][4]+'|'
			Brand+=app_data.get('final_list')[i][5]+'|'
			Product_price+=app_data.get('final_list')[i][6]+'|'
			Seller_name+=app_data.get('final_list')[i][7]+'|'
			Unit_id+=app_data.get('final_list')[i][8]+'|'
			Super_Category+=app_data.get('final_list')[i][9]+'|'
			Category_Group+=app_data.get('final_list')[i][10]+'|'
				

		total_price+=float(app_data.get('final_list')[i][6])

	eve_val=json.dumps({"Currency":"AED","OS":"iOS","Advertising_ID":app_data.get('u'),"Product_price":Product_price,"Logged_in":str(app_data.get('signed_in')).lower(),"Customer_id":app_data.get('c_id'),"Country":device_data.get('locale').get('country').lower(),"Language":device_data.get('locale').get('language').lower(),"Seller_name":Seller_name,"Cart_total_price":22.889999389648438,"Category_id":product_details[app_data.get('rr')][4],"App_Version":campaign_data.get('app_version_name'),"Product_id":Product_id,"Product_name":Product_name,"Brand":Brand,"Device_id":app_data.get('Device_id'),"Device_token":app_data.get('d_token'),"EAN":EAN,"Source_of_visit":"Direct","OS_Version":device_data.get('os_version'),"Cart_size":len(app_data.get('final_list')),"Super_Category":Super_Category})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Checkout Event',eve_val)
	util.execute_request(**apsalar_event)


def couponapplied(app_data,device_data):

	eve_val=json.dumps({"Page_name":"Order Summary","Coupon_discount":0,"Coupon_code":random.choice(["hajvs","bahabs",""]),"Coupon_applied_successfully":False,"Language":device_data.get('locale').get('language').lower()})	
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Coupon Applied Event',eve_val)
	util.execute_request(**apsalar_event)

def paymentevent(app_data,device_data):

	couponapplied(app_data,device_data)

	Product_name=''
	Product_id=''
	EAN=''		
	Category_name=''
	Category_id=''
	Brand=''
	Product_price=''
	Seller_name	=''
	Unit_id=''
	Super_Category=''
	Category_Group=''
	total_price=0.0

	for i in range(len(app_data.get('final_list'))):	
		if i == len(app_data.get('final_list'))-1:
			Product_name+=app_data.get('final_list')[i][0]
			Product_id+=app_data.get('final_list')[i][1]
			EAN+=app_data.get('final_list')[i][2]
			Category_name+=app_data.get('final_list')[i][3]
			Category_id+=app_data.get('final_list')[i][4]
			Brand+=app_data.get('final_list')[i][5]
			Product_price+=app_data.get('final_list')[i][6]
			Seller_name+=app_data.get('final_list')[i][7]
			Unit_id+=app_data.get('final_list')[i][8]
			Super_Category+=app_data.get('final_list')[i][9]
			Category_Group+=app_data.get('final_list')[i][10]	
		else:
			Product_name+=app_data.get('final_list')[i][0]+'|'
			Product_id+=app_data.get('final_list')[i][1]+'|'
			EAN+=app_data.get('final_list')[i][2]+'|'
			Category_name+=app_data.get('final_list')[i][3]+'|'
			Category_id+=app_data.get('final_list')[i][4]+'|'
			Brand+=app_data.get('final_list')[i][5]+'|'
			Product_price+=app_data.get('final_list')[i][6]+'|'
			Seller_name+=app_data.get('final_list')[i][7]+'|'
			Unit_id+=app_data.get('final_list')[i][8]+'|'
			Super_Category+=app_data.get('final_list')[i][9]+'|'
			Category_Group+=app_data.get('final_list')[i][10]+'|'
				

		total_price+=float(app_data.get('final_list')[i][6])
	

	eve_val=json.dumps({"Source_of_visit":"Direct","Super_Category":Super_Category,"Advertising_ID":app_data.get('u'),"Page_name":"Order Summary","OS_Version":device_data.get('os_version'),"Cart_total_price":total_price,"Country":device_data.get('locale').get('country').lower(),"Product_name":Product_name,"Currency":"AED","OS":"iOS","Device_token":app_data.get('d_token'),"Logged_in":str(app_data.get('signed_in')).lower(),"Cart_size":len(app_data.get('final_list')),"Product_id":Product_id,"Product_price":Product_price,"Seller_name":Seller_name,"Language":device_data.get('locale').get('language').lower(),"Device_id":app_data.get('Device_id'),"EAN":EAN,"Customer_id":app_data.get('c_id'),"App_Version":campaign_data.get('app_version_name'),"Brand":Brand})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Payment Event',eve_val)
	util.execute_request(**apsalar_event)

def paymenteventoption(app_data,device_data):

	Product_name=''
	Product_id=''
	EAN=''		
	Category_name=''
	Category_id=''
	Brand=''
	Product_price=''
	Seller_name	=''
	Unit_id=''
	Super_Category=''
	Category_Group=''
	total_price=0.0

	for i in range(len(app_data.get('final_list'))):	
		if i == len(app_data.get('final_list'))-1:
			Product_name+=app_data.get('final_list')[i][0]
			Product_id+=app_data.get('final_list')[i][1]
			EAN+=app_data.get('final_list')[i][2]
			Category_name+=app_data.get('final_list')[i][3]
			Category_id+=app_data.get('final_list')[i][4]
			Brand+=app_data.get('final_list')[i][5]
			Product_price+=app_data.get('final_list')[i][6]
			Seller_name+=app_data.get('final_list')[i][7]
			Unit_id+=app_data.get('final_list')[i][8]
			Super_Category+=app_data.get('final_list')[i][9]
			Category_Group+=app_data.get('final_list')[i][10]	
		else:
			Product_name+=app_data.get('final_list')[i][0]+'|'
			Product_id+=app_data.get('final_list')[i][1]+'|'
			EAN+=app_data.get('final_list')[i][2]+'|'
			Category_name+=app_data.get('final_list')[i][3]+'|'
			Category_id+=app_data.get('final_list')[i][4]+'|'
			Brand+=app_data.get('final_list')[i][5]+'|'
			Product_price+=app_data.get('final_list')[i][6]+'|'
			Seller_name+=app_data.get('final_list')[i][7]+'|'
			Unit_id+=app_data.get('final_list')[i][8]+'|'
			Super_Category+=app_data.get('final_list')[i][9]+'|'
			Category_Group+=app_data.get('final_list')[i][10]+'|'
				

		total_price+=float(app_data.get('final_list')[i][6])
	

	eve_val=json.dumps({"App_Version":campaign_data.get('app_version_name'),"Product_price":Product_price,"Category_id":product_details[app_data.get('rr')][4],"Super_Category":Super_Category,"Coupon_code":random.choice(["hajvs","bahabs",""]),"Language":device_data.get('locale').get('language').lower(),"Cart_total_price":total_price,"Country":device_data.get('locale').get('country').lower(),"Page_name":"Payment Options:Page","Coupon_discount":0,"Brand":Brand,"Cart_size":len(app_data.get('final_list')),"Seller_name":Seller_name,"Source_of_visit":"Direct","Product_id":Product_id,"Device_id":app_data.get('Device_id'),"Payment_method_name":"Cash On Delivery (COD)","Advertising_ID":app_data.get('u'),"Device_token":app_data.get('d_token'),"OS":"iOS","OS_Version":device_data.get('os_version'),"Customer_id":app_data.get('c_id'),"EAN":EAN,"Product_name":Product_name,"Logged_in":str(app_data.get('signed_in')).lower(),"Currency":"AED"})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Payment OptionEvent',eve_val)
	util.execute_request(**apsalar_event)


def transaction(app_data,device_data):

	Product_name=''
	Product_id=''
	EAN=''		
	Category_name=''
	Category_id=''
	Brand=''
	Product_price=''
	Seller_name	=''
	Unit_id=''
	Super_Category=''
	Category_Group=''
	total_price=0.0

	for i in range(len(app_data.get('final_list'))):	
		if i == len(app_data.get('final_list'))-1:
			Product_name+=app_data.get('final_list')[i][0]
			Product_id+=app_data.get('final_list')[i][1]
			EAN+=app_data.get('final_list')[i][2]
			Category_name+=app_data.get('final_list')[i][3]
			Category_id+=app_data.get('final_list')[i][4]
			Brand+=app_data.get('final_list')[i][5]
			Product_price+=app_data.get('final_list')[i][6]
			Seller_name+=app_data.get('final_list')[i][7]
			Unit_id+=app_data.get('final_list')[i][8]
			Super_Category+=app_data.get('final_list')[i][9]
			Category_Group+=app_data.get('final_list')[i][10]	
		else:
			Product_name+=app_data.get('final_list')[i][0]+'|'
			Product_id+=app_data.get('final_list')[i][1]+'|'
			EAN+=app_data.get('final_list')[i][2]+'|'
			Category_name+=app_data.get('final_list')[i][3]+'|'
			Category_id+=app_data.get('final_list')[i][4]+'|'
			Brand+=app_data.get('final_list')[i][5]+'|'
			Product_price+=app_data.get('final_list')[i][6]+'|'
			Seller_name+=app_data.get('final_list')[i][7]+'|'
			Unit_id+=app_data.get('final_list')[i][8]+'|'
			Super_Category+=app_data.get('final_list')[i][9]+'|'
			Category_Group+=app_data.get('final_list')[i][10]+'|'
				

		total_price+=float(app_data.get('final_list')[i][6])

	eve_val=json.dumps({"Source_of_visit":"Direct","Advertising_ID":app_data.get('u'),"OS_Version":device_data.get('os_version'),"Country":device_data.get('locale').get('country').lower(),"Product_name":Product_name,"Currency":"AED","OS":"iOS","Device_token":app_data.get('d_token'),"Logged_in":str(app_data.get('signed_in')).lower(),"Product_id":Product_id,"Quantity":1,"Product_price":Product_price,"Unit_id":Unit_id,"Seller_name":Seller_name,"Language":device_data.get('locale').get('language').lower(),"Device_id":app_data.get('Device_id'),"EAN":EAN,"Customer_id":app_data.get('c_id'),"App_Version":campaign_data.get('app_version_name'),"Brand":Brand})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Transaction Event_PER_CART_ITEM',eve_val)
	util.execute_request(**apsalar_event)

	eve_val=json.dumps({"pq":1,"r":total_price,"pn":"","pp":total_price,"pcc":"AED","ps":"amodi","pk":app_data.get('order_id'),"pc":""})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__iap__',eve_val)
	util.execute_request(**apsalar_event)


	eve_val=json.dumps({"Super_Category":Super_Category,"Product_price":Product_price,"App_Version":campaign_data.get('app_version_name'),"Coupon_code":random.choice(["hajvs","bahabs",""]),"Language":device_data.get('locale').get('language').lower(),"Quantity":"1","Country":device_data.get('locale').get('country').lower(),"Coupon_discount":0,"Brand":Brand,"Seller_name":Seller_name,"Source_of_visit":"Direct","Product_id":Product_id,"Device_id":app_data.get('Device_id'),"Order_id":app_data.get('order_id'),"Payment_method_name":"COD","Advertising_ID":app_data.get('u'),"Device_token":app_data.get('d_token'),"OS":"iOS","OS_Version":device_data.get('os_version'),"EAN":EAN,"Customer_id":app_data.get('c_id'),"Product_name":Product_name,"Logged_in":str(app_data.get('signed_in')).lower(),"Order_amount":total_price,"Currency":"AED"})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Transaction Event',eve_val)
	util.execute_request(**apsalar_event)

	eve_val=json.dumps({"OS":"iOS","Advertising_ID":app_data.get('u'),"Device_id":app_data.get('Device_id'),"Page_name":"MyAccount","OS_Version":device_data.get('os_version'),"Source_of_visit":"Direct","App_Version":campaign_data.get('app_version_name'),"Device_token":app_data.get('d_token'),"Customer_id":app_data.get('c_id'),"Country":device_data.get('locale').get('country').lower(),"Logged_in":str(app_data.get('signed_in')).lower(),"Language":device_data.get('locale').get('language').lower()})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'First User on souq',eve_val)
	util.execute_request(**apsalar_event)


	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'First User on App',eve_val)
	util.execute_request(**apsalar_event)


	

def sigin(app_data,device_data):	

	eve_val=json.dumps({"App_Version":campaign_data.get('app_version_name'),"Device_id":app_data.get('Device_id'),"Source_of_visit":"Direct","Customer_id":app_data.get('c_id'),"Advertising_ID":app_data.get('u'),"Page_name":"MyAccount","Country":device_data.get('locale').get('country').lower(),"Logged_in":str(app_data.get('signed_in')).lower(),"Device_token":app_data.get('d_token'),"Language":device_data.get('locale').get('language').lower(),"OS":"iOS","OS_Version":device_data.get('os_version')})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Sign in',eve_val)
	util.execute_request(**apsalar_event)

def sigout(app_data,device_data):	

	eve_val=json.dumps({"App_Version":campaign_data.get('app_version_name'),"Device_id":app_data.get('Device_id'),"Source_of_visit":"Direct","Customer_id":app_data.get('c_id'),"Advertising_ID":app_data.get('u'),"Page_name":"MyAccount","Country":device_data.get('locale').get('country').lower(),"Logged_in":str(app_data.get('signed_in')).lower(),"Device_token":app_data.get('d_token'),"Language":device_data.get('locale').get('language').lower(),"OS":"iOS","OS_Version":device_data.get('os_version')})
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Sign out',eve_val)
	util.execute_request(**apsalar_event)


#########################################
#										#
# 			AppSalar funcation 			#
#										#
#########################################

def generate_gcm_id(app_data):
	if not app_data.get('device_gcm_id'):
		app_data['device_gcm_id'] = 'APA' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(137))
	return app_data.get('device_gcm_id')

def apsalarResolve(campaign_data, app_data, device_data,k=''):
	url = 'http://e-ssl.apsalar.com/api/v1/resolve'
	method = 'get'
	headers = {
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		# 'X-NewRelic-ID': 'UwAOUl5ADgoCUlZWBg==',
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()
	}
	if k=='IDFA':
		params ='a=amodi&k='+k+'&u='+app_data.get('u')+'&p='+campaign_data.get('sdk')+'&v='+device_data.get('os_version')+'&rt=plist&idfa='+app_data.get('idfa_id')+'&idfv='+app_data.get('idfv_id')
	else:
		params ='a=amodi&k='+k+'&u='+app_data.get('u')+'&p='+campaign_data.get('sdk')+'&v='+device_data.get('os_version')+'&rt=plist&idfa='+app_data.get('idfa_id')+'&idfv='+app_data.get('idfv_id')

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarStart(campaign_data, app_data, device_data):
	# relicid(app_data)
	generate_gcm_id(app_data)

	url = 'http://e-ssl.apsalar.com/api/v1/start'
	method = 'get'
	headers = {
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'X-NewRelic-ID': 'UwAOUl5ADgoCUlZWBg==',
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()
	}
	
	if not app_data.get('s'):
		app_data['s'] = str(uuid.uuid4()).upper()

	params ='a='+campaign_data.get('apsalar').get('key')+'&av='+campaign_data.get('app_version_name')+'&c=wifi&i='+campaign_data.get('package_name')+'&lag='+str(generateLag())+'&n='+campaign_data.get('app_name')+'&p='+campaign_data.get('sdk')+'&rt=plist&s='+app_data.get('s')+'&sdk='+campaign_data.get('apsalar').get('version')+'&u='+app_data.get('u')+'&v='+device_data.get('os_version')+'&d='+device_data.get('device_platform')+'&k=IDFA&scs=%5B%22fb68'+str(util.get_random_string('decimal',13))+'%22%2C%22souq%22%2C%22tagmanager.c.com.souq.souq.com%22%2C%22amzn-com.souq.souq.com%22%5D&cr=1&dnt=0&idfa='+app_data.get('idfa_id')+'&idfv='+app_data.get('idfv_id')+'&ddl_enabled=true&ddl_to=60&is=true&install_receipt='+get_install_receipt()+'&install_time='+str(int(app_data.get('times').get('install_complete_time')))+'&update_time=0&device_type=phone'


	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarEvent(campaign_data, app_data, device_data,event_name,value):
	relicid(app_data)
	url = 'http://e-ssl.apsalar.com/api/v1/event'

	method = 'get'
	headers = {
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'X-NewRelic-ID': 'UwAOUl5ADgoCUlZWBg==',
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower()
	}
	t=str(random.randint(1,350))+'.'+str(random.randint(10,99))

	if not app_data.get('s'):
		app_data['s'] = str(uuid.uuid4()).upper()

	params='a=amodi&k=IDFA&u='+app_data.get('u')+'&av='+campaign_data.get('app_version_name')+'&i='+campaign_data.get('package_name')+'&lag='+str(generateLag())+'&n='+event_name+'&e='+value+'&p='+campaign_data.get('sdk')+'&rt=plist&s='+app_data.get('s')+'&t='+t+'&idfa='+app_data.get('idfa_id')+'&idfv='+app_data.get('idfv_id')+'&seq='+getseq(app_data)+'&sdk='+campaign_data.get('apsalar').get('version')+'&device_type=phone'

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


#########################################
#										#
# 			Extra funcation 			#
#										#
#########################################
def getseq(app_data):
	if not app_data.get('count'):
		app_data['count']=1
	else:
		app_data['count']+=1
	return str(app_data['count'])		

def cal_seq(app_data):
	if not app_data.get('seq'):
		app_data['seq'] = 1
	else:
		app_data['seq'] = app_data.get('seq')+1
	return app_data.get('seq')

# def user_agent(campaign_data,app_data,device_data):
# 	if int(device_data.get("sdk")) >19:
# 		app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
# 	else:
# 		app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'	
# 	return app_data.get('ua')

def cal_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def current_location(device_data):
	url='http://lumtest.com/myip.json'
	header={
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
			'Accept-Encoding': 'gzip, deflate',
			'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+','+device_data.get('locale').get('language')+';q=0.9',
			'Cache-Control': 'max-age=0',
			'Upgrade-Insecure-Requests': '1',
			'User-Agent': device_data.get('User-Agent')
	}
	params={}
	data={}
		
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def generateLag():
	return (random.randint(10,90)*0.001)

def generate_t():
	return "%.3f" %(random.uniform(300,1000))

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())
#########################################
#										#
# 			Util funcation 				#
#										#
#########################################

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_device_token(app_data):
	if not app_data.get('d_token'):
		app_data['d_token']=util.get_random_string('hex',64).upper()

def omni_id(app_data):
	if not app_data.get('omni_id'):
		app_data['omni_id']=(util.get_random_string('hex',16)+'-'+util.get_random_string('hex',16)).upper()

def relicid(app_data):
	if not app_data.get('relicid'):
		app_data['relicid']=util.get_random_string('all',18)+'=='
		
def Customer_id(app_data):
	if app_data.get('signed_in')==True:
		if not app_data.get('c_id'):
			app_data['c_id']="23"+util.get_random_string('decimal',6)
	else:
		if not app_data.get('c_id'):
			app_data['c_id']=""

	

def order_id(app_data):
	if not app_data.get('order_id'):
		app_data['order_id']='10380'+(util.get_random_string('decimal',8))

def get_install_receipt():
	return 'MIISlAYJKoZIhvcNAQcCoIIShTCCEoECAQExCzAJBgUrDgMCGgUAMIICNQYJKoZIhvcNAQcBoIICJgSCAiIxggIeMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMAwCAQsCAQEEBAICRX0wDAIBDgIBAQQEAgIAjTANAgEDAgEBBAUMAzIwNzANAgENAgEBBAUCAwHViDANAgETAgEBBAUMAzE0MjAOAgEBAgEBBAYCBCg7shIwDgIBCQIBAQQGAgRQMjUzMA4CARACAQEEBgIEMaWr4DAQAgEPAgEBBAgCBj3jLEoYvzAUAgEAAgEBBAwMClByb2R1Y3Rpb24wGAIBBAIBAgQQSzsbeKzWUC3T7UAEr2waRjAbAgECAgEBBBMMEWNvbS5zb3VxLnNvdXEuY29tMBwCAQUCAQEEFPAWzjvrH1twa5EH0Xk LwVGMrASMB4CAQgCAQEEFhYUMjAxOS0xMC0wNVQwNToyMjo1NlowHgIBDAIBAQQWFhQyMDE5LTEwLTA1VDA1OjIyOjU2WjAeAgESAgEBBBYWFDIwMTgtMDQtMjNUMTE6MzU6NDRaME0CAQYCAQEERYHB3LwVHdelQiO0qlhB2rMtqdq7RTbxHS7hodfiLFmlGFykz5LtmBwDhR7dHQXTsTQweaxI/W0igBRPW 5XvL/R3D4M1jBQAgEHAgEBBEhmQ1zVwnufmWwhkX8zx7vL2f7UW3kZnmsvJRCneqUM1FEsjNKDOzHUgBtnA OMUaqL Ni5gi01RCFvUp yR3SnzFhE///jR9Oggg5lMIIFfDCCBGSgAwIBAgIIDutXh eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc B/SWigVvWh 0j2jMcjuIjwKXEJss9xp/sSg1Vhv kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4 3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7 mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5 givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3 Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn 9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN DAb2yriphcrGvzcNFMI jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz 6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn 9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn 9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t 2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti 9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU 12TZ/wYdV2aeZuTJC 9jVcZ5 oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBABEDpMFFlFJAQIyteJUjTfKd4vUkQU J4ZabXO AhcbLfvATD pfiqYmB9gmdej0QwtD6CDBhCtZPoRRjoQb2J0kkjIc1S/ZeWP6NkcTDSrO3JLocIkIJp6VGntah9hCZhF0A4UCQ0NSvDDgTd4XpOp07VLU7vxQAuta64Nlh7hRebVnX0Du1WsIqRnFmrUcgJGPXfmEPy7KnzgJptGvfDAsBPuxcnkwrRXsEhkmWqrH4QkI21fDHhfssXOsnVaD1ltVqkLIQBHiHVVOSjHG7eJYT2ri/fducSxL1nJ96kS8O/aOSZSE7qDmkv1ikrC7HHK4CJ pN8tHJ/ap1f6GivM='
