# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from sdk import util, installtimenew
from sdk import getsleep
from sdk import NameLists
from sdk import purchase
import time
import random
import json
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 : 'com.licious',
	'app_name' 			 : 'Fresh Meat Online - Licious',
	'app_version_name'	 : '3.4.26',#'3.4.24',
	'no_referrer'		 : False,
	'device_targeting'   : True,
	'app_size'			 : 15.0,#39.1,#58.0,
	'supported_os'		 : '4.4',		
	'supported_countries': 'WW',
	'ctr'				 : 6,
	'tracker'			 : 'branch',
	'api_branch':{
	    'branchkey'  :'key_live_gbJlHTS814kqnJxhgFYcmhkdADm60izV',
	    'sdk'        :'android3.2.0',
	},

	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:25,
		2:25,
		3:24,
		4:24,
		5:23,
		6:22,
		7:20,
		8:20,
		9:20,
		10:19,
		11:19,
		12:19,
		13:18,
		14:18,
		15:17,
		16:16,
		17:15,
		18:14,
		19:13,
		20:13,
		21:13,
		22:13,
		23:11,
		24:10,
		25:10,
		26:9,
		27:8,
		28:8,
		29:6,
		30:6,
		31:5,
		32:5,
		33:4,
		34:4,
		35:3,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	

	print '______Please Wait__________________'
	installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android",min_sleep=0)

	if not app_data.get('cart_items'):
		app_data['cart_items'] = []

	###########		INITIALIZE		############

	time.sleep(random.randint(1,2))
	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	app_data['api_hit_time']=time.time()
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')

		if not app_data.ge('device_fingerprint_id1') or not app_data.get('identity_id') or not app_data.get('session_id'):
			app_data['device_fingerprint_id1']='613284344550945604'
			app_data['identity_id']='641581897171978611'
			app_data['session_id']='641581897189090110'

	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'	


	print 'Branch utm_install event'
	request=api_branch_custom(campaign_data, app_data, device_data,event='utm_install')
	util.execute_request(**request)


	time.sleep(random.randint(1,2))
	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)


	get_required_user_data(campaign_data, device_data, app_data)


	time.sleep(random.randint(25,30))
	print 'Branch profile'
	req = api2_branch_profile( campaign_data, device_data, app_data ,identity= globals().get('identity'))
	util.execute_request(**req)

	if random.randint(1,100) <= 30:

		if not globals().get('category_list'):
			get_category_list(campaign_data, device_data, app_data)
			print globals().get('category_list')


		globals()['choosed_category'] = random.choice(globals().get('category_list'))

		time.sleep(random.randint(10,15))
		request=api_branch_custom(campaign_data, app_data, device_data,event='category_click')
		util.execute_request(**request)


	if random.randint(1,100) <= 50:

		time.sleep(random.randint(30,100))
		print "Branch______close"
		request=api_branch_close(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(5,7))
		print "Branch______open"
		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,3))
		print "Branch______sdk"
		request=branch_call(campaign_data, app_data, device_data,typ='v2')
		util.execute_request(**request)

		time.sleep(random.randint(20,30))
		print 'Branch profile'
		req = api2_branch_profile( campaign_data, device_data, app_data ,identity= globals().get('identity') ,call=2)
		util.execute_request(**req)


	for i in range(random.randint(1,3)):

		if not globals().get('category_list'):
			get_category_list(campaign_data, device_data, app_data)
			print globals().get('category_list')

		globals()['choosed_category'] = random.choice(globals().get('category_list'))


		if not globals().get('product_data_list'):
			get_product_data(campaign_data, device_data, app_data)
			print globals().get('product_data_list')

		globals()['choosed_product'] = random.choice(globals().get('product_data_list'))


		if random.randint(1,100) <= 80:

			time.sleep(random.randint(10,15))
			request=api_branch_custom(campaign_data, app_data, device_data,event='category_click' )
			util.execute_request(**request)


		if random.randint(1,100) <= 70:

			time.sleep(random.randint(10,20))
			request=api_branch_custom(campaign_data, app_data, device_data,event='view_item')
			util.execute_request(**request)


		if random.randint(1,100) <= 60:

			app_data.get('cart_items').append(globals().get('choosed_product'))

			time.sleep(random.randint(20,50))
			request=api_branch_custom(campaign_data, app_data, device_data,event='add_to_cart')
			util.execute_request(**request)


		if random.randint(1,100) <= 30 and len(app_data.get('cart_items')) > 0:

			items_allowed_in_cart = random.randint(2,3)
			length = len(app_data.get('cart_items'))

			while length > items_allowed_in_cart:

				i = random.randint(0,len(app_data.get('cart_items'))-1)

				globals()['removed_product'] = app_data.get('cart_items')[i]
				del app_data.get('cart_items')[i]

				time.sleep(random.randint(20,50))
				request=api_branch_custom(campaign_data, app_data, device_data,event='remove_from_cart')
				util.execute_request(**request)

				length -= 1


		if random.randint(1,100) <= 40 and len(app_data.get('cart_items')) > 0:

			strings_for_checkout = []
			for data in app_data.get('cart_items'):
				strings_for_checkout.append("[{item_name="+ str(data.get('pr_name')) +", quantity=1, price="+ str(data.get('price')) +".0, item_category="+ str(data.get('category')) +", currency=INR, item_id="+ str(data.get('product_id')) +"}]")
			globals()['string_for_checkout'] = '[' + ','.join(strings_for_checkout) + ']'

			total_price = 0
			for data in app_data.get('cart_items'):
				if data.get('price'):
					try:
						price = int(data.get('price'))
					except:
						price = 0
					total_price += price

			if total_price < 699:
				total_price += 39

			globals()['total_price'] = str(total_price)
			globals()['order_id'] = util.get_random_string('all',8)


			time.sleep(random.randint(20,40))
			request=api_branch_custom(campaign_data, app_data, device_data,event='begin_checkout')
			util.execute_request(**request)

			time.sleep(random.randint(3,10))
			request=api_branch_custom(campaign_data, app_data, device_data,event='checkout_progress')
			util.execute_request(**request)


			if random.randint(1,100) <= 90:

				time.sleep(random.randint(20,30))
				request=api_branch_custom(campaign_data, app_data, device_data,event='proceed_to_pay')
				util.execute_request(**request)


				if purchase.isPurchase(app_data,day=1,advertiser_demand=10):

					time.sleep(random.randint(10,30))
					request=api_branch_custom(campaign_data, app_data, device_data,event='newuser_charged')
					util.execute_request(**request)

					time.sleep(random.randint(1,2))
					request=api_branch_custom(campaign_data, app_data, device_data,event='charged')
					util.execute_request(**request)

					app_data['cart_items'] = []

	

	time.sleep(random.randint(55,60))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data,call_seq=2)
	util.execute_request(**request)

		
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):

	print '______Please Wait__________________'
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android",min_sleep=0)

	if not app_data.get('cart_items'):
		app_data['cart_items'] = []

	###########		INITIALIZE		############

	if not app_data.get('device_fingerprint_id1') or not app_data.get('identity_id') or not app_data.get('session_id'):

		time.sleep(random.randint(1,2))
		print "Branch______install"
		request=api_branch_install(campaign_data,app_data,device_data)
		result=util.execute_request(**request)
		try:
			json_result=json.loads(result.get('data'))
			app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
			app_data['identity_id']=json_result.get('identity_id')
			app_data['session_id']=json_result.get('session_id')

			if not app_data.ge('device_fingerprint_id1') or not app_data.get('identity_id') or not app_data.get('session_id'):
				app_data['device_fingerprint_id1']='613284344550945604'
				app_data['identity_id']='641581897171978611'
				app_data['session_id']='641581897189090110'

		except:
			app_data['device_fingerprint_id1']='613284344550945604'
			app_data['identity_id']='641581897171978611'
			app_data['session_id']='641581897189090110'	


	get_required_user_data(campaign_data, device_data, app_data)

	print "Branch______open"
	request=api_branch_open(campaign_data, app_data, device_data)
	util.execute_request(**request)


	if random.randint(1,100) <= 30:

		time.sleep(random.randint(30,100))
		print "Branch______close"
		request=api_branch_close(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(5,7))
		print "Branch______open"
		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,3))
		print "Branch______sdk"
		request=branch_call(campaign_data, app_data, device_data,typ='v2')
		util.execute_request(**request)

		time.sleep(random.randint(20,30))
		print 'Branch profile'
		req = api2_branch_profile( campaign_data, device_data, app_data ,identity= globals().get('identity') ,call=2)
		util.execute_request(**req)


	for i in range(random.randint(1,3)):

		if not globals().get('category_list'):
			get_category_list(campaign_data, device_data, app_data)
			print globals().get('category_list')

		globals()['choosed_category'] = random.choice(globals().get('category_list'))


		if not globals().get('product_data_list'):
			get_product_data(campaign_data, device_data, app_data)
			print globals().get('product_data_list')

		globals()['choosed_product'] = random.choice(globals().get('product_data_list'))


		if random.randint(1,100) <= 70:

			time.sleep(random.randint(10,15))
			request=api_branch_custom(campaign_data, app_data, device_data,event='category_click' )
			util.execute_request(**request)


		if random.randint(1,100) <= 60:

			time.sleep(random.randint(10,20))
			request=api_branch_custom(campaign_data, app_data, device_data,event='view_item')
			util.execute_request(**request)


		if random.randint(1,100) <= 50:

			app_data.get('cart_items').append(globals().get('choosed_product'))

			time.sleep(random.randint(20,50))
			request=api_branch_custom(campaign_data, app_data, device_data,event='add_to_cart')
			util.execute_request(**request)


		if random.randint(1,100) <= 20 and len(app_data.get('cart_items')) > 0:

			items_allowed_in_cart = random.randint(2,3)
			length = len(app_data.get('cart_items'))

			while length > items_allowed_in_cart:

				i = random.randint(0,len(app_data.get('cart_items'))-1)

				globals()['removed_product'] = app_data.get('cart_items')[i]
				del app_data.get('cart_items')[i]

				time.sleep(random.randint(20,50))
				request=api_branch_custom(campaign_data, app_data, device_data,event='remove_from_cart')
				util.execute_request(**request)

				length -= 1


		if random.randint(1,100) <= 30 and len(app_data.get('cart_items')) > 0:

			strings_for_checkout = []
			for data in app_data.get('cart_items'):
				strings_for_checkout.append("[{item_name="+ str(data.get('pr_name')) +", quantity=1, price="+ str(data.get('price')) +".0, item_category="+ str(data.get('category')) +", currency=INR, item_id="+ str(data.get('product_id')) +"}]")
			globals()['string_for_checkout'] = '[' + ','.join(strings_for_checkout) + ']'

			total_price = 0
			for data in app_data.get('cart_items'):
				if data.get('price'):
					try:
						price = int(data.get('price'))
					except:
						price = 0
					total_price += price

			if total_price < 699:
				total_price += 39

			globals()['total_price'] = str(total_price)
			globals()['order_id'] = util.get_random_string('all',8)


			time.sleep(random.randint(20,40))
			request=api_branch_custom(campaign_data, app_data, device_data,event='begin_checkout')
			util.execute_request(**request)

			time.sleep(random.randint(3,10))
			request=api_branch_custom(campaign_data, app_data, device_data,event='checkout_progress')
			util.execute_request(**request)


			if random.randint(1,100) <= 90:

				time.sleep(random.randint(20,30))
				request=api_branch_custom(campaign_data, app_data, device_data,event='proceed_to_pay')
				util.execute_request(**request)


				if purchase.isPurchase(app_data,day=day,advertiser_demand=20):

					time.sleep(random.randint(10,30))
					request=api_branch_custom(campaign_data, app_data, device_data,event='newuser_charged')
					util.execute_request(**request)

					time.sleep(random.randint(1,2))
					request=api_branch_custom(campaign_data, app_data, device_data,event='charged')
					util.execute_request(**request)

					app_data['cart_items'] = []


	time.sleep(random.randint(55,60))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data,call_seq=2)
	util.execute_request(**request)
	
		
	return {'status':'true'}


def test(app_data, device_data):	
	print '______Please Wait__________________'
	installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android",min_sleep=0)
	###########		INITIALIZE		############


	time.sleep(random.randint(1,2))
	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	app_data['api_hit_time']=time.time()
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'	


	request=api_branch_custom(campaign_data, app_data, device_data,event='utm_install')
	util.execute_request(**request)

	time.sleep(random.randint(1,2))
	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(25,30))

	print 'Branch profile'
	req = api2_branch_profile( campaign_data, device_data, app_data ,identity='g_k1hgy6x6')
	util.execute_request(**req)

	for i in range(0,6):
		time.sleep(random.randint(50,60))
		category_list=['Fish & Seafood','Ready to Cook','Eggs','Cold Cuts','Chicken','Chicken']

		request=api_branch_custom(campaign_data, app_data, device_data,event='category_click',event_value=category_list[i])
		util.execute_request(**request)


	time.sleep(random.randint(60,100))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(5,7))
	print "Branch______open"
	request=api_branch_open(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(1,3))
	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data,typ='v2')
	util.execute_request(**request)

	time.sleep(random.randint(18,20))

	print 'Branch profile'
	req = api2_branch_profile( campaign_data, device_data, app_data ,identity='c_k1hh90f0',call=2)
	util.execute_request(**req)


	for i in range(0,3):
		time.sleep(random.randint(25,30))
		category_list=['Fish & Seafood','Fish & Seafood','Chicken']


		request=api_branch_custom(campaign_data, app_data, device_data,event='category_click',event_value=category_list[i])
		util.execute_request(**request)

	
	time.sleep(random.randint(45,50))
	request=api_branch_custom(campaign_data, app_data, device_data,event='add_to_cart')
	util.execute_request(**request)

	time.sleep(random.randint(240,300))

	request=api_branch_custom(campaign_data, app_data, device_data,event='begin_checkout')
	util.execute_request(**request)

	time.sleep(random.randint(28,30))

	request=api_branch_custom(campaign_data, app_data, device_data,event='checkout_progress')
	util.execute_request(**request)

	time.sleep(random.randint(10,15))

	request=api_branch_custom(campaign_data, app_data, device_data,event='checkout_progress')
	util.execute_request(**request)

	time.sleep(random.randint(30,35))


	request=api_branch_custom(campaign_data, app_data, device_data,event='proceed_to_pay')
	util.execute_request(**request)
	time.sleep(random.randint(1,2))

	request=api_branch_custom(campaign_data, app_data, device_data,event='newuser_charged')
	util.execute_request(**request)

	time.sleep(random.randint(1,2))

	request=api_branch_custom(campaign_data, app_data, device_data,event='charged')
	util.execute_request(**request)

	time.sleep(random.randint(30,35))

	request=api_branch_custom(campaign_data, app_data, device_data,event='category_click',event_value='Chicken')
	util.execute_request(**request)


	time.sleep(random.randint(55,60))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data,call_seq=2)
	util.execute_request(**request)


	

		
	return {'status':'true'}




###################################################################

def get_product_data(campaign_data, device_data, app_data):

	if not globals().get('hub_id'):
		globals()['hub_id'] = str(random.randint(30,60))

	print "\nproduct data call\n"
	request=product_data_call( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	try:
		data = json.loads(response.get('data')).get('data')
		globals()['product_data_list'] = []
		for d in data:
			if d.get('product_inventory') and d.get('product_master') and d.get('product_pricing'):
				if d.get('product_inventory').get('product_id') and d.get('product_master').get('pr_name') and d.get('product_pricing').get('base_price'):
					globals()['product_data_list'].append({'product_id' : d.get('product_inventory').get('product_id'), 'pr_name' : d.get('product_master').get('pr_name').encode('utf-8'), 'price' : str(d.get('product_pricing').get('base_price')), 'category':globals().get('choosed_category').get('cat_name')})

		if not globals().get('product_data_list'):
			globals()['product_data_list'] = [{'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_579a5010c8dd6', 'pr_name': 'Premium Goat Curry Cut'}, {'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_57235721c9f9a', 'pr_name': 'Premium Lamb Curry Cut (Large)'}, {'category': 'Lamb & Goat', 'price': '389', 'product_id': u'pr_4oijic0gzad', 'pr_name': 'Goat Curry Cut (Small)'}, {'category': 'Lamb & Goat', 'price': '99', 'product_id': u'pr_576360a8b87f1', 'pr_name': 'Lamb Liver'}, {'category': 'Lamb & Goat', 'price': '389', 'product_id': u'pr_4oijic0hvff', 'pr_name': 'Lamb Curry Cut (Small)'}, {'category': 'Lamb & Goat', 'price': '469', 'product_id': u'pr_579a530679ede', 'pr_name': 'Goat Mince/Keema'}, {'category': 'Lamb & Goat', 'price': '499', 'product_id': u'pr_579a52ccd74d7', 'pr_name': 'Goat Boneless'}, {'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_5760ce5861eeb', 'pr_name': 'Lamb - Biryani Cut (Large)'}, {'category': 'Lamb & Goat', 'price': '449', 'product_id': u'pr_572357479b1a7', 'pr_name': 'Lamb Mince'}, {'category': 'Lamb & Goat', 'price': '199', 'product_id': u'pr_58d3ae77dcce0', 'pr_name': 'Mutton Soup Bones'}, {'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_4dmjfl18hj2', 'pr_name': 'Goat Ribs and Chops'}, {'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_o5pjg4pmg3n', 'pr_name': 'Lamb Ribs and Chops'}, {'category': 'Lamb & Goat', 'price': '449', 'product_id': u'pr_57635c7607b2c', 'pr_name': 'Lamb Leg Steaks (Medium)'}, {'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_57635f9b531d9', 'pr_name': 'Lamb Shanks'}]

		print 'ok response for product data list'
	except:
		print 'exception for product data list'
		globals()['product_data_list'] = [{'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_579a5010c8dd6', 'pr_name': 'Premium Goat Curry Cut'}, {'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_57235721c9f9a', 'pr_name': 'Premium Lamb Curry Cut (Large)'}, {'category': 'Lamb & Goat', 'price': '389', 'product_id': u'pr_4oijic0gzad', 'pr_name': 'Goat Curry Cut (Small)'}, {'category': 'Lamb & Goat', 'price': '99', 'product_id': u'pr_576360a8b87f1', 'pr_name': 'Lamb Liver'}, {'category': 'Lamb & Goat', 'price': '389', 'product_id': u'pr_4oijic0hvff', 'pr_name': 'Lamb Curry Cut (Small)'}, {'category': 'Lamb & Goat', 'price': '469', 'product_id': u'pr_579a530679ede', 'pr_name': 'Goat Mince/Keema'}, {'category': 'Lamb & Goat', 'price': '499', 'product_id': u'pr_579a52ccd74d7', 'pr_name': 'Goat Boneless'}, {'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_5760ce5861eeb', 'pr_name': 'Lamb - Biryani Cut (Large)'}, {'category': 'Lamb & Goat', 'price': '449', 'product_id': u'pr_572357479b1a7', 'pr_name': 'Lamb Mince'}, {'category': 'Lamb & Goat', 'price': '199', 'product_id': u'pr_58d3ae77dcce0', 'pr_name': 'Mutton Soup Bones'}, {'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_4dmjfl18hj2', 'pr_name': 'Goat Ribs and Chops'}, {'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_o5pjg4pmg3n', 'pr_name': 'Lamb Ribs and Chops'}, {'category': 'Lamb & Goat', 'price': '449', 'product_id': u'pr_57635c7607b2c', 'pr_name': 'Lamb Leg Steaks (Medium)'}, {'category': 'Lamb & Goat', 'price': '399', 'product_id': u'pr_57635f9b531d9', 'pr_name': 'Lamb Shanks'}]


def product_data_call( campaign_data, device_data, app_data ):
	url= "http://52.76.90.219/api/catalog/products/all"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent": "okhttp/3.8.0",
        "deviceid": "",
        "macid": "",
        "token": globals().get('token')}

	params= {       "cat_id": globals().get('choosed_category').get('cat_id') , "hub_id": globals().get('hub_id')}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}




def get_category_list(campaign_data, device_data, app_data):

	if not globals().get('hub_id'):
		globals()['hub_id'] = str(random.randint(30,60))

	print "\ncategory list call\n"
	request=cat_list_call( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	try:
		data = json.loads(response.get('data')).get('data')
		globals()['category_list'] = []
		for sub_data in data:
			if sub_data.get('cat_name') and sub_data.get('id'):
				globals()['category_list'].append({'cat_name':sub_data.get('cat_name').encode('utf-8'), 'cat_id':str(sub_data.get('id'))})

		if not globals().get('category_list'):
			globals()['category_list'] = [{'cat_id': '1', 'cat_name': 'Chicken'}, {'cat_id': '2', 'cat_name': 'Lamb & Goat'}, {'cat_id': '3', 'cat_name': 'Fish & Seafood'}, {'cat_id': '4', 'cat_name': 'Ready to Cook'}, {'cat_id': '19', 'cat_name': 'Cold Cuts'}, {'cat_id': '30', 'cat_name': 'Eggs'}, {'cat_id': '20', 'cat_name': 'Spreads'}]

		print 'ok response for category list'

	except:
		print 'exception for category list'
		globals()['category_list'] = [{'cat_id': '1', 'cat_name': 'Chicken'}, {'cat_id': '2', 'cat_name': 'Lamb & Goat'}, {'cat_id': '3', 'cat_name': 'Fish & Seafood'}, {'cat_id': '4', 'cat_name': 'Ready to Cook'}, {'cat_id': '19', 'cat_name': 'Cold Cuts'}, {'cat_id': '30', 'cat_name': 'Eggs'}, {'cat_id': '20', 'cat_name': 'Spreads'}]



def cat_list_call( campaign_data, device_data, app_data ):
	url= "http://52.76.90.219/api/catalog/categories"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent": "okhttp/3.8.0",
        "deviceid": "",
        "macid": "",
        "token": globals().get('token')}

	params= {       "hub_id": globals().get('hub_id')}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}







def get_required_user_data(campaign_data, device_data, app_data):

	print "\nGenerate token call\n"
	request=generate_token_call( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	try:
		data = json.loads(response.get('data'))
		globals()['token'] = data.get('token')
		if not globals().get('token'):
			globals()['token'] = 'tk_k2cxu8ct'
		print '\nok response for token'
	except:
		print '\nexception for token'
		globals()['token'] = 'tk_k2cxu8ct'


	print "\nIdentity Id call\n"
	request= identity_id_call( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	try:
		data = json.loads(response.get('data'))
		globals()['identity'] = data.get('data')
		if not globals().get('identity'):
			globals()['identity'] = 'g_k2cxu8wx'
		print 'ok response for identity'
	except:
		print 'exception for identity'
		globals()['identity'] = 'g_k2cxu8wx'



def identity_id_call( campaign_data, device_data, app_data ):
	url= "http://52.76.90.219/api/guest/user"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent": "okhttp/3.8.0",
        "source": "android",
        "token": globals().get('token')}
	params= None
	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def generate_token_call( campaign_data, device_data, app_data ):
	url= "http://52.76.90.219/api/generate_token"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Content-Type": "application/json; charset=UTF-8",
        "User-Agent": "okhttp/3.8.0",
        "deviceid": "",
        "devicetype": "android",
        "macid": ""}

	params= None

	data= {       "deviceid": "", "hashkey": "728a69a46e4be99517676f04d09fdd97"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


################ BRANCH CALLS ###################

def branch_call(campaign_data, app_data, device_data,typ='v1'):
	url='http://cdn.branch.io/sdk/uriskiplist_'+typ+'.json'
	method='get'
	headers={
		'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
	}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': None}


def api_branch_install(campaign_data,app_data,device_data):
	method='post'
	url='http://api2.branch.io/v1/install'

	headers = {
				'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
				'Content-Type':'application/json',
				'Accept':'application/json',
			}

	data={       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "cd": {       "mv": "-1", "pn": campaign_data.get('package_name')},
        "country": device_data.get('locale').get('country'),
        "debug": False,
        "environment": "FULL_APP",
        "facebook_app_link_checked": False,
        "first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "instrumentation": {       "v1/install-qwt": "0"},
        "is_hardware_id_real": True,
        "is_referrable": 1,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "latest_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "latest_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "previous_update_time": 0,
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "update": 0,
        "wifi": True} 

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}

def api2_branch_profile( campaign_data, device_data, app_data, identity='',call=1):
	url= "http://api2.branch.io/v1/profile"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data)}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "country": device_data.get('locale').get('country'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity": identity,
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {"v2/event/custom-brtt":"596","v1/profile-qwt":"0"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val":"0",
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}

	if call == 2:
		data['instrumentation'] = {"v1/profile-qwt":"0","v1/open-brtt":"1582"}
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


def api_branch_open(campaign_data, app_data, device_data, call_seq=1):	
	
	url='http://api.branch.io/v1/open'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "cd": {       "mv": "-1", "pn": campaign_data.get('package_name')},
        "country": device_data.get('locale').get('country'),
        "debug": False,
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "environment": "FULL_APP",
        "facebook_app_link_checked": False,
        "first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {"v1/open-qwt":"0"},
        "is_hardware_id_real": True,
        "is_referrable": 1,
        "language": "en",
        "lat_val": 0,
        "latest_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "latest_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "previous_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "update": 1,
        "wifi": True}
	

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}


def api_branch_close(campaign_data, app_data, device_data,call_seq=1):	
	
	url='http://api.branch.io/v1/close'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "country": device_data.get('locale').get('country'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {"v2/event/custom-brtt":"784","v1/close-qwt":"1"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val":"0",
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}
	if call_seq==2:
		data['instrumentation']={"v2/event/custom-brtt":"1456","v1/close-qwt":"18"}
	

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def api_branch_custom(campaign_data, app_data, device_data,event='',event_value=''):
	url= "http://api2.branch.io/v2/event/custom"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
        }

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),	
        "instrumentation":{"v1/install-brtt":str(random.randint(199,999)),"v2/event/custom-qwt":str(random.randint(1,99))},
        "metadata": {       },
        "name": event,
        "retryNumber": 0,
        "user_data": {       "aaid": device_data.get('adid'),
                             "android_id": device_data.get('android_id'),
                             "app_version": campaign_data.get('app_version_name'),
                             "brand": device_data.get('brand'),
                             "country": device_data.get('locale').get('country'),
                             "developer_identity": "bnc_no_value",
                             "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
                             "environment": "FULL_APP",
                             "language": device_data.get('locale').get('language'),
                             "limit_ad_tracking": 0,
                             "local_ip": device_data.get('private_ip'),
                             "model": device_data.get('model'),
                             "os": "Android",
                             "os_version": int(device_data.get('sdk')),
                             "screen_dpi":int(device_data.get('dpi')),
                             "screen_height": int(device_data.get('resolution').split('x')[0]),
                             "screen_width": int(device_data.get('resolution').split('x')[1]),
                             "sdk": "android",
                             "sdk_version": '3.2.0',
                             "user_agent":device_data.get('User-Agent')}}


   	if globals().get('identity'):
   		data['user_data']['developer_identity'] = globals().get('identity')


   	if event=='category_click':
   		data['custom_data']={"category_name": globals().get('choosed_category').get('cat_name')}

   	elif event=='view_item':
   		data['custom_data']={"item_id": globals().get('choosed_product').get('product_id') ,"price": str(globals().get('choosed_product').get('price'))+ ".0","item_name": globals().get('choosed_product').get('pr_name') ,"currency":"INR"}

   	elif event=='add_to_cart':
   		data['custom_data']={"item_id": globals().get('choosed_product').get('product_id') ,"quantity":"1.0","price": str(globals().get('choosed_product').get('price'))+ ".0","item_name": globals().get('choosed_product').get('pr_name') ,"currency":"INR"}

   	elif event=='remove_from_cart':
   		data['custom_data']={"item_id": globals().get('removed_product').get('product_id') ,"quantity":"0.0","price": str(globals().get('removed_product').get('price'))+ ".0","item_name": globals().get('removed_product').get('pr_name') ,"currency":"INR"}

	elif event=='begin_checkout':
   		data['custom_data']={"item_list": globals().get('string_for_checkout') ,"checkout_step":"1.0"}

	elif event=='checkout_progress':
   		data['custom_data']={"item_list": globals().get('string_for_checkout') ,"checkout_step":"2.0"}

	elif event=='proceed_to_pay':
   		data['custom_data']={"total": globals().get('total_price') + ".0","payment_method":"COD"}

	elif event=='newuser_charged':
   		data['custom_data']={"order_id": globals().get('order_id') ,"item_list": globals().get('string_for_checkout') ,"value": globals().get('total_price') + ".0" ,"currency":"INR"}

	elif event=='charged':
   		data['custom_data']={"order_id": globals().get('order_id') ,"item_list": globals().get('string_for_checkout') ,"value": globals().get('total_price') + ".0" ,"currency":"INR"}
	


	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def api_branch_event(campaign_data, app_data, device_data, event_name='',event_data='',call=1):	
	
	url='http://api2.branch.io/v1/event'
	header={
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "country": device_data.get('locale').get('country'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "environment": "FULL_APP",
        "event": event_name,
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       "v1/event-qwt": "15110"+str(random.randint(100,9999)),
                                   "v1/profile-brtt": "294"+str(random.randint(100,9999))},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}


	if call == 2:
		data['instrumentation']={"v1/event-qwt":str(random.randint(100,9999)),"v1/profile-brtt":str(random.randint(100,9999))}
	if call == 3:
		data['instrumentation']={"v1/event-qwt":str(random.randint(0,9999)),"v1/open-brtt":str(random.randint(100,9999))}
	if call == 4:
		data['instrumentation']={"v1/event-qwt":str(random.randint(0,9999)),"v1/event-brtt":str(random.randint(0,9999))}

	# if call == 5:
	# 	data['instrumentation']={"v1/event-brtt":"299","v1/event-qwt":"674"}

	# if call == 6:
	# 	data['instrumentation']={"v1/event-brtt":"289","v1/event-qwt":"956"}

	# if call == 7:
	# 	data['instrumentation']={"v1/event-brtt":"1116","v1/event-qwt":"2066"}
		
	if event_name=='PURCHASE':
		data["custom_data"]={"is_first_order":"true"}
		data['instrumentation']={"v2/event/standard-brtt":"1557","v2/event/standard-qwt":"1571"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def connectivity_check(campaign_data, app_data, device_data):
	url = 'http://connectivitycheck.gstatic.com/generate_204'
	method = 'head'
	headers = {
		'Accept-Encoding': 'gzip',
		'User-Agent': device_data.get('User-Agent'),
	}
	params=None
	data = None
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def measurement_config(campaign_data, app_data, device_data):

	url = 'http://app-measurement.com/config/app/1%3A661240310080%3Aandroid%3A0ad3d352a734fdec'
	
	header={
		'User-Agent': get_ua(device_data),
		'Accept-Encoding': 'gzip',
	}
		
	params={
		'app_instance_id':	'ff70eed7ba918a419f66873cebbbca8d',
		'platform':	'android',
		'gmp_version':	'18382',
	}
	data=None

	return {'url': url, 'httpmethod': 'head', 'headers': header, 'params': params, 'data': data}
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)


def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

# def def_sec(app_data,device_data):
# 	timez = device_data.get('timezone')
# 	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
# 	if int(timez) < 0:
# 		sec = (-1) * sec
# 	if not app_data.get('sec'):
# 		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"
# 		app_data['user']['pswd']		= last_name+app_data['user']['dob'].replace('-','')



