# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util, purchase
from sdk import NameLists
import time
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.app.makano',
	'app_size'			 : 22.0,
	'app_name' 			 :'dahmakan - food delivery app',
	'app_version_name' 	 :'40.2.4',#'40.2.3',#'40.1',
	'app_version_code'	 : '36',#'29',
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 : 's6fqyq9yexog',
		'sdk'		 : 'android4.18.1',#'android4.13.0',
		'secret_id'  : '1',
		'secret_key' : '20800773061695728131975030087180942625',
		},
	'api_branch':{
	    'branchkey'  :'key_live_mee9D2fGKqsoG22MEvV91mfbCxjJyc0h',
	    'sdk'        :'android3.2.0',
	},	
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	pushtoken(app_data)
	set_androidUUID(app_data)

	################generating_realtime_differences##################
	s1=33
	s2=39
	a1=38
	a2=41
	r1=3
	r2=4
	click_time1=True
	c1=184
	c2=189
	ir1=41
	ir2=47
	click_time2=True
	e1=1
	e2=3

	if not app_data.get('mixpanel_distinct_id'):
		app_data['mixpanel_distinct_id']=str(uuid.uuid4())

	global addressId_list

	addressId_list=['5d43ef49c221c20a38091f24', '5d7b4ecb4e64420a4e7fe625', '5d43ef49444beb0a77899464', '5d7b4efebafa630a88f44b5f', '5d7b4f1760f8c70a63faebf1', '5d7b4f28702c2f0a717da6c0', '5d7b4fb44ab2560a5ea7d044', '5d7b5001bafa630a88f45c9b', '5d7b505c258eff0a540fa5b6', '5d7b50a84ab2560a5ea7f4a7', '5d7b50c9bafa630a88f46d6e', '5d7b50eb702c2f0a717dd1ea']

 	###########	 CALLS		 ############

 	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'	
	
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
		
	time.sleep(random.randint(4,7))
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='install_referrer',t1=ir1,t2=ir2)
	util.execute_request(**request)
		
	time.sleep(random.randint(1,3))
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)
		
	time.sleep(random.randint(60,120))
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time1,source='reftag',t1=r1,t2=r2,t3=c1,t4=c2)
	util.execute_request(**request)

	if random.randint(1,100)<=95:

		time.sleep(random.randint(60,120))
		print 'lat and lon generator'
		lat_lon=current_location(device_data)
		lat_lon_result = util.execute_request(**lat_lon)
		try:
			json_lat_lon_result=json.loads(lat_lon_result.get('data'))
			app_data['city']=str(json_lat_lon_result.get('geo').get('city')).replace(" ","_")
		except:
			app_data['city']="Kuala_Lumpur"

		print "\nself_call_api_dahmakan\n"
		request=api_dahmakan( campaign_data, device_data, app_data )
		response = util.execute_request(**request)
		try:
			result = json.loads(response.get('data')).get('access_token')
			app_data['access_token'] = result

			if not app_data.get('access_token'):
				app_data['access_token'] = "f390bdb6-78ab-415a-a7bb-bc28ce7b5c8a"	
			
		except:
			app_data['access_token'] = "f390bdb6-78ab-415a-a7bb-bc28ce7b5c8a"	

		print "\nself_call_api_dahmakan\n"
		request=api_dahmakan_users( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			output=json.loads(response.get('data'))
			app_data['identity']=output.get('id')

		except:
			print 'Exception'
			app_data['identity']='5d7b70aa89a34b0ab070ff52'

		time.sleep(random.randint(45,60))
		
		request=api_branch_profile( campaign_data, device_data, app_data )
		util.execute_request(**request)

		time.sleep(random.randint(2,5))
		signup(campaign_data, app_data, device_data,e1,e2)

		app_data['signup_complete']=True

	if app_data.get('signup_complete') == True and random.randint(1,100)<=95:

		time.sleep(random.randint(10,30))
		request=credithistory(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,2))
		request=api_branch_credits( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['address_updated']=True

	if app_data.get('address_updated')==True and random.randint(1,100)<=80: #80

		for i in range(random.randint(1,3)):

			time.sleep(random.randint(20,120))
			add_to_cart(campaign_data, app_data, device_data,e1,e2)

		if purchase.isPurchase(app_data,day=1,advertiser_demand=12):

			if not app_data.get('address_index'):
				app_data['address_index']=random.randint(0, len(addressId_list)-1)

			global time_slotId_list, time_list, start_date, menu_item_id_list, price_list, menu_type, menu_id_list

			time_slotId_list=[]
			time_list=[]
			menu_item_id_list=[]
			price_list=[]
			menu_id_list=[]

			request=get_time_slot_id_call( campaign_data, device_data, app_data )
			response=util.execute_request(**request)

			try:
				output=json.loads(response.get('data')).get('dates')
				choice=random.randint(0, len(output)-1)
				start_date=output[choice].get('date')
				temp=output[choice].get('time_slots')

				for i in range(len(temp)):
					time_slotId_list.append(str(temp[i].get('id')))
					time_list.append(str(temp[i].get('display_text')))

				if len(time_slotId_list)<1 or len(time_list)<1 or len(time_slotId_list)!=len(time_list):

					time_slotId_list=['5c8a10bfb10fb80d02cf0124', '5c8a108e0b44f61f1260a172', '5d0b41730191c9778798fe9d', '5d0b41730191c9778798fe9c', '5c8a10bfb10fb80d02cf0125', '5d4bf6bcc0cfcd9ae5284777', '5d4bf6bcc0cfcd9ae5284776', '5d0b41730191c9778798fe9e', '5ca5dfae012dd9216c825de2', '5ca5deee0a59ea2f5adcde47', '5d4bf6bcc0cfcd9ae5284778', '5d0b41730191c9778798fea1', '5d0b41730191c9778798fea0', '5ca5dfaf012dd9216c825de3', '5d4bf6bcc0cfcd9ae528477b', '5d4bf6bcc0cfcd9ae528477a', '5d0b41730191c9778798fea2', '5c5a63704762d3d8741f21df', '5c5a63704762d3d8741f21de', '5d4bf6bcc0cfcd9ae528477c', '5ca5e05cad4bea6b7bb1e6f6', '5ca5deee0a59ea2f5adcde48', '5c5a63724762d3d8741f21e0', '5d4bf6bcc0cfcd9ae528477f', '5d4bf6bcc0cfcd9ae528477e', '5ca5e05cad4bea6b7bb1e6f7', '5d0b41730191c9778798fea5', '5d0b41730191c9778798fea4', '5d4bf6bcc0cfcd9ae5284780', '5c5a63724762d3d8741f21e2', '5c5a63724762d3d8741f21e1', '5d0b41730191c9778798fea6', '5d4bf6bcc0cfcd9ae5284783', '5d4bf6bcc0cfcd9ae5284782', '5c5a63734762d3d8741f21e3', '5ca5e13f0a59ea2f5adcec2f', '5ca5e0e125bf0725cf39d2f6', '5d4bf6bcc0cfcd9ae5284784', '5d0b41730191c9778798fea9', '5d0b41730191c9778798fea8', '5ca5e13f0a59ea2f5adcec30', '5d4bf6bdc0cfcd9ae5284787', '5d4bf6bdc0cfcd9ae5284786', '5d0b41730191c9778798feaa', '5c63ed64521a5a2df97beb81', '5c63ecddc1eed60d187d1ca7', '5d4bf6bdc0cfcd9ae5284788', '5d0b41730191c9778798fead', '5d0b41730191c9778798feac', '5c63ed64521a5a2df97bebcb', '5d4bf6bdc0cfcd9ae528478b', '5d4bf6bdc0cfcd9ae528478a', '5d0b41730191c9778798feae', '5d0b41730191c9778798feb1', '5d0b41730191c9778798feb0', '5d4bf6bdc0cfcd9ae528478c', '5d0b41730191c9778798feb5', '5d0b41730191c9778798feb4', '5d0b41730191c9778798feb2', '5d4bf6bdc0cfcd9ae528478f', '5d4bf6bdc0cfcd9ae528478e', '5d0b41730191c9778798feb6', '5c8a5d720b44f61f12901fe3', '5c8a5d4012eab11be81454bf', '5d4bf6bdc0cfcd9ae5284790', '58d3c7d0e4b08796c12816c4', '58d3c7d0e4b08796c12816c3', '5c8a5d720b44f61f12901fe2', '5d2748172cf48218291d9c10', '5d2748172cf48218291d9c0f', '58d3c7d0e4b08796c12816c5', '5cefd02c1b4ec58dd7b23e5b', '5cefd02c1b4ec58dd7b23e5a', '5d2748172cf48218291d9c11', '58d3c7d0e4b08796c12816c7', '58d3c7d0e4b08796c12816c6', '5cefd02c1b4ec58dd7b23e5c', '5d2748172cf48218291d9c14', '5d2748172cf48218291d9c13', '58d3c7d0e4b08796c12816c8', '5cefd02c1b4ec58dd7b23e5f', '5cefd02c1b4ec58dd7b23e5e', '5d2748172cf48218291d9c15', '58d3c7d0e4b08796c12816ca', '58d3c7d0e4b08796c12816c9', '5cefd02c1b4ec58dd7b23e60', '5d2748172cf48218291d9c18', '5d2748172cf48218291d9c17', '58d3c7d0e4b08796c12816cb', '5cffca311d5774a872e2c86d', '5cffca301d5774a872e2c86c', '5d2748172cf48218291d9c19', '58d3c7d0e4b08796c12816cd', '58d3c7d0e4b08796c12816cc', '5cffca311d5774a872e2c86e', '5d2748172cf48218291d9c1c', '5d2748172cf48218291d9c1b', '58d3c7d0e4b08796c12816ce', '5cffca311d5774a872e2c871', '5cffca311d5774a872e2c870', '5d2748172cf48218291d9c1d', '5933fa7eff8aed5aa46d251b', '5933fa7eff8aed5aa46d251a', '5cffca311d5774a872e2c872', '5d2748172cf48218291d9c20', '5d2748172cf48218291d9c1f', '5933fa7eff8aed5aa46d251c', '5cffca311d5774a872e2c875', '5cffca311d5774a872e2c874', '5d2748172cf48218291d9c21', '5c8a5d92b10fb80d02ebc96b', '5c8a5d190b44f61f127ef1d9', '5cffca311d5774a872e2c876', '5c8a5d92b10fb80d02ebc96c']
					time_list=['9:00 AM - 9:30 AM', '9:00 AM - 10:00 AM', '9:20 AM - 9:50 AM', '9:20 AM - 10:20 AM', '9:30 AM - 10:00 AM', '9:40 AM - 10:10 AM', '9:40 AM - 10:40 AM', '9:50 AM - 10:20 AM', '10:00 AM - 10:30 AM', '10:00 AM - 11:00 AM', '10:10 AM - 10:40 AM', '10:20 AM - 10:50 AM', '10:20 AM - 11:20 AM', '10:30 AM - 11:00 AM', '10:40 AM - 11:10 AM', '10:40 AM - 11:40 AM', '10:50 AM - 11:20 AM', '11:00 AM - 11:30 AM', '11:00 AM - 12:00 PM', '11:10 AM - 11:40 AM', '11:20 AM - 11:50 AM', '11:20 AM - 12:20 PM', '11:30 AM - 12:00 PM', '11:40 AM - 12:10 PM', '11:40 AM - 12:40 PM', '11:50 AM - 12:20 PM', '12:00 PM - 12:30 PM', '12:00 PM - 1:00 PM', '12:10 PM - 12:40 PM', '12:20 PM - 12:50 PM', '12:20 PM - 1:20 PM', '12:30 PM - 1:00 PM', '12:40 PM - 1:10 PM', '12:40 PM - 1:40 PM', '12:50 PM - 1:20 PM', '1:00 PM - 1:30 PM', '1:00 PM - 2:00 PM', '1:10 PM - 1:40 PM', '1:20 PM - 1:50 PM', '1:20 PM - 2:20 PM', '1:30 PM - 2:00 PM', '1:40 PM - 2:10 PM', '1:40 PM - 2:40 PM', '1:50 PM - 2:20 PM', '2:00 PM - 2:30 PM', '2:00 PM - 3:00 PM', '2:10 PM - 2:40 PM', '2:20 PM - 2:50 PM', '2:20 PM - 3:20 PM', '2:30 PM - 3:00 PM', '2:40 PM - 3:10 PM', '2:40 PM - 3:40 PM', '2:50 PM - 3:20 PM', '3:00 PM - 3:30 PM', '3:00 PM - 4:00 PM', '3:10 PM - 3:40 PM', '3:20 PM - 3:50 PM', '3:20 PM - 4:20 PM', '3:30 PM - 4:00 PM', '3:40 PM - 4:10 PM', '3:40 PM - 4:40 PM', '3:50 PM - 4:20 PM', '4:00 PM - 4:30 PM', '4:00 PM - 5:00 PM', '4:10 PM - 4:40 PM', '4:20 PM - 4:50 PM', '4:20 PM - 5:20 PM', '4:30 PM - 5:00 PM', '4:40 PM - 5:10 PM', '4:40 PM - 5:40 PM', '4:50 PM - 5:20 PM', '5:00 PM - 5:30 PM', '5:00 PM - 6:00 PM', '5:10 PM - 5:40 PM', '5:20 PM - 5:50 PM', '5:20 PM - 6:20 PM', '5:30 PM - 6:00 PM', '5:40 PM - 6:10 PM', '5:40 PM - 6:40 PM', '5:50 PM - 6:20 PM', '6:00 PM - 6:30 PM', '6:00 PM - 7:00 PM', '6:10 PM - 6:40 PM', '6:20 PM - 6:50 PM', '6:20 PM - 7:20 PM', '6:30 PM - 7:00 PM', '6:40 PM - 7:10 PM', '6:40 PM - 7:40 PM', '6:50 PM - 7:20 PM', '7:00 PM - 7:30 PM', '7:00 PM - 8:00 PM', '7:10 PM - 7:40 PM', '7:20 PM - 7:50 PM', '7:20 PM - 8:20 PM', '7:30 PM - 8:00 PM', '7:40 PM - 8:10 PM', '7:40 PM - 8:40 PM', '7:50 PM - 8:20 PM', '8:00 PM - 8:30 PM', '8:00 PM - 9:00 PM', '8:10 PM - 8:40 PM', '8:20 PM - 8:50 PM', '8:20 PM - 9:20 PM', '8:30 PM - 9:00 PM', '8:40 PM - 9:10 PM', '8:40 PM - 9:40 PM', '8:50 PM - 9:20 PM', '9:00 PM - 9:30 PM', '9:00 PM - 10:00 PM', '9:10 PM - 9:40 PM', '9:20 PM - 9:50 PM', '9:20 PM - 10:20 PM', '9:30 PM - 10:00 PM', '9:50 PM - 10:20 PM']
					start_date=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%d-%m-%Y")

			except:
				print 'Exception'
				time_slotId_list=['5c8a10bfb10fb80d02cf0124', '5c8a108e0b44f61f1260a172', '5d0b41730191c9778798fe9d', '5d0b41730191c9778798fe9c', '5c8a10bfb10fb80d02cf0125', '5d4bf6bcc0cfcd9ae5284777', '5d4bf6bcc0cfcd9ae5284776', '5d0b41730191c9778798fe9e', '5ca5dfae012dd9216c825de2', '5ca5deee0a59ea2f5adcde47', '5d4bf6bcc0cfcd9ae5284778', '5d0b41730191c9778798fea1', '5d0b41730191c9778798fea0', '5ca5dfaf012dd9216c825de3', '5d4bf6bcc0cfcd9ae528477b', '5d4bf6bcc0cfcd9ae528477a', '5d0b41730191c9778798fea2', '5c5a63704762d3d8741f21df', '5c5a63704762d3d8741f21de', '5d4bf6bcc0cfcd9ae528477c', '5ca5e05cad4bea6b7bb1e6f6', '5ca5deee0a59ea2f5adcde48', '5c5a63724762d3d8741f21e0', '5d4bf6bcc0cfcd9ae528477f', '5d4bf6bcc0cfcd9ae528477e', '5ca5e05cad4bea6b7bb1e6f7', '5d0b41730191c9778798fea5', '5d0b41730191c9778798fea4', '5d4bf6bcc0cfcd9ae5284780', '5c5a63724762d3d8741f21e2', '5c5a63724762d3d8741f21e1', '5d0b41730191c9778798fea6', '5d4bf6bcc0cfcd9ae5284783', '5d4bf6bcc0cfcd9ae5284782', '5c5a63734762d3d8741f21e3', '5ca5e13f0a59ea2f5adcec2f', '5ca5e0e125bf0725cf39d2f6', '5d4bf6bcc0cfcd9ae5284784', '5d0b41730191c9778798fea9', '5d0b41730191c9778798fea8', '5ca5e13f0a59ea2f5adcec30', '5d4bf6bdc0cfcd9ae5284787', '5d4bf6bdc0cfcd9ae5284786', '5d0b41730191c9778798feaa', '5c63ed64521a5a2df97beb81', '5c63ecddc1eed60d187d1ca7', '5d4bf6bdc0cfcd9ae5284788', '5d0b41730191c9778798fead', '5d0b41730191c9778798feac', '5c63ed64521a5a2df97bebcb', '5d4bf6bdc0cfcd9ae528478b', '5d4bf6bdc0cfcd9ae528478a', '5d0b41730191c9778798feae', '5d0b41730191c9778798feb1', '5d0b41730191c9778798feb0', '5d4bf6bdc0cfcd9ae528478c', '5d0b41730191c9778798feb5', '5d0b41730191c9778798feb4', '5d0b41730191c9778798feb2', '5d4bf6bdc0cfcd9ae528478f', '5d4bf6bdc0cfcd9ae528478e', '5d0b41730191c9778798feb6', '5c8a5d720b44f61f12901fe3', '5c8a5d4012eab11be81454bf', '5d4bf6bdc0cfcd9ae5284790', '58d3c7d0e4b08796c12816c4', '58d3c7d0e4b08796c12816c3', '5c8a5d720b44f61f12901fe2', '5d2748172cf48218291d9c10', '5d2748172cf48218291d9c0f', '58d3c7d0e4b08796c12816c5', '5cefd02c1b4ec58dd7b23e5b', '5cefd02c1b4ec58dd7b23e5a', '5d2748172cf48218291d9c11', '58d3c7d0e4b08796c12816c7', '58d3c7d0e4b08796c12816c6', '5cefd02c1b4ec58dd7b23e5c', '5d2748172cf48218291d9c14', '5d2748172cf48218291d9c13', '58d3c7d0e4b08796c12816c8', '5cefd02c1b4ec58dd7b23e5f', '5cefd02c1b4ec58dd7b23e5e', '5d2748172cf48218291d9c15', '58d3c7d0e4b08796c12816ca', '58d3c7d0e4b08796c12816c9', '5cefd02c1b4ec58dd7b23e60', '5d2748172cf48218291d9c18', '5d2748172cf48218291d9c17', '58d3c7d0e4b08796c12816cb', '5cffca311d5774a872e2c86d', '5cffca301d5774a872e2c86c', '5d2748172cf48218291d9c19', '58d3c7d0e4b08796c12816cd', '58d3c7d0e4b08796c12816cc', '5cffca311d5774a872e2c86e', '5d2748172cf48218291d9c1c', '5d2748172cf48218291d9c1b', '58d3c7d0e4b08796c12816ce', '5cffca311d5774a872e2c871', '5cffca311d5774a872e2c870', '5d2748172cf48218291d9c1d', '5933fa7eff8aed5aa46d251b', '5933fa7eff8aed5aa46d251a', '5cffca311d5774a872e2c872', '5d2748172cf48218291d9c20', '5d2748172cf48218291d9c1f', '5933fa7eff8aed5aa46d251c', '5cffca311d5774a872e2c875', '5cffca311d5774a872e2c874', '5d2748172cf48218291d9c21', '5c8a5d92b10fb80d02ebc96b', '5c8a5d190b44f61f127ef1d9', '5cffca311d5774a872e2c876', '5c8a5d92b10fb80d02ebc96c']
				time_list=['9:00 AM - 9:30 AM', '9:00 AM - 10:00 AM', '9:20 AM - 9:50 AM', '9:20 AM - 10:20 AM', '9:30 AM - 10:00 AM', '9:40 AM - 10:10 AM', '9:40 AM - 10:40 AM', '9:50 AM - 10:20 AM', '10:00 AM - 10:30 AM', '10:00 AM - 11:00 AM', '10:10 AM - 10:40 AM', '10:20 AM - 10:50 AM', '10:20 AM - 11:20 AM', '10:30 AM - 11:00 AM', '10:40 AM - 11:10 AM', '10:40 AM - 11:40 AM', '10:50 AM - 11:20 AM', '11:00 AM - 11:30 AM', '11:00 AM - 12:00 PM', '11:10 AM - 11:40 AM', '11:20 AM - 11:50 AM', '11:20 AM - 12:20 PM', '11:30 AM - 12:00 PM', '11:40 AM - 12:10 PM', '11:40 AM - 12:40 PM', '11:50 AM - 12:20 PM', '12:00 PM - 12:30 PM', '12:00 PM - 1:00 PM', '12:10 PM - 12:40 PM', '12:20 PM - 12:50 PM', '12:20 PM - 1:20 PM', '12:30 PM - 1:00 PM', '12:40 PM - 1:10 PM', '12:40 PM - 1:40 PM', '12:50 PM - 1:20 PM', '1:00 PM - 1:30 PM', '1:00 PM - 2:00 PM', '1:10 PM - 1:40 PM', '1:20 PM - 1:50 PM', '1:20 PM - 2:20 PM', '1:30 PM - 2:00 PM', '1:40 PM - 2:10 PM', '1:40 PM - 2:40 PM', '1:50 PM - 2:20 PM', '2:00 PM - 2:30 PM', '2:00 PM - 3:00 PM', '2:10 PM - 2:40 PM', '2:20 PM - 2:50 PM', '2:20 PM - 3:20 PM', '2:30 PM - 3:00 PM', '2:40 PM - 3:10 PM', '2:40 PM - 3:40 PM', '2:50 PM - 3:20 PM', '3:00 PM - 3:30 PM', '3:00 PM - 4:00 PM', '3:10 PM - 3:40 PM', '3:20 PM - 3:50 PM', '3:20 PM - 4:20 PM', '3:30 PM - 4:00 PM', '3:40 PM - 4:10 PM', '3:40 PM - 4:40 PM', '3:50 PM - 4:20 PM', '4:00 PM - 4:30 PM', '4:00 PM - 5:00 PM', '4:10 PM - 4:40 PM', '4:20 PM - 4:50 PM', '4:20 PM - 5:20 PM', '4:30 PM - 5:00 PM', '4:40 PM - 5:10 PM', '4:40 PM - 5:40 PM', '4:50 PM - 5:20 PM', '5:00 PM - 5:30 PM', '5:00 PM - 6:00 PM', '5:10 PM - 5:40 PM', '5:20 PM - 5:50 PM', '5:20 PM - 6:20 PM', '5:30 PM - 6:00 PM', '5:40 PM - 6:10 PM', '5:40 PM - 6:40 PM', '5:50 PM - 6:20 PM', '6:00 PM - 6:30 PM', '6:00 PM - 7:00 PM', '6:10 PM - 6:40 PM', '6:20 PM - 6:50 PM', '6:20 PM - 7:20 PM', '6:30 PM - 7:00 PM', '6:40 PM - 7:10 PM', '6:40 PM - 7:40 PM', '6:50 PM - 7:20 PM', '7:00 PM - 7:30 PM', '7:00 PM - 8:00 PM', '7:10 PM - 7:40 PM', '7:20 PM - 7:50 PM', '7:20 PM - 8:20 PM', '7:30 PM - 8:00 PM', '7:40 PM - 8:10 PM', '7:40 PM - 8:40 PM', '7:50 PM - 8:20 PM', '8:00 PM - 8:30 PM', '8:00 PM - 9:00 PM', '8:10 PM - 8:40 PM', '8:20 PM - 8:50 PM', '8:20 PM - 9:20 PM', '8:30 PM - 9:00 PM', '8:40 PM - 9:10 PM', '8:40 PM - 9:40 PM', '8:50 PM - 9:20 PM', '9:00 PM - 9:30 PM', '9:00 PM - 10:00 PM', '9:10 PM - 9:40 PM', '9:20 PM - 9:50 PM', '9:20 PM - 10:20 PM', '9:30 PM - 10:00 PM', '9:50 PM - 10:20 PM']
				start_date=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%d-%m-%Y")


			request=get_menu_id_call( campaign_data, device_data, app_data )
			response=util.execute_request(**request)
			try:
				output=json.loads(response.get('data')).get('dates')[0]

				menu_type=output.get('menu_type')

				temp=output.get('menu').get('lines')
				choice=random.randint(0, len(temp)-1)
				li=temp[choice].get('items')
				for i in range(len(li)):
					menu_id_list.append(str(li[i].get('menu_id')))
					price_list.append(li[i].get('price'))
					menu_item_id_list.append(str(li[i].get('id')))

				if len(menu_item_id_list)<1 or len(price_list)<1 or len(menu_id_list)<1 or len(menu_item_id_list)!=len(price_list) or len(price_list)!=len(menu_id_list):

					menu_item_id_list=['5d7af8a6ebc2840a3e4102f5', '5d7af8afbd2ff10a71cf8bb2', '5d7af8c6bd2ff10a71cf8bf5', '5d7af8ce4e64420a4e6e0c6f', '5d7af8e04e64420a4e6e173f', '5d7af8eccaf5af0a6308e092', '5d7af8f5bd2ff10a71cf8d3e', '5d7af8febd2ff10a71cf8da4', '5d7af90e4e64420a4e6e19e1', '5d7af916caf5af0a6308e39d']
					price_list=[19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9]
					menu_id_list=['5ccb18135e07f10cf41eff3a', '5cafe7a95eb3250cf5c452e0', '5cfb57841e80650d6ff66ff4', '5d3e7e6769d46f0a36c5d651', '5d03c6579f39860d5a11aa86', '5d4673d5654ffb0a5e5ececd', '5d03c745385d7e7f5274d2ad', '5d45136c3646cb0a65af67d2', '5d213f3f65b9bb0cec660df2', '5d213e1665b9bb0cec66050e']
					menu_type='Lunch'
				

			except:
				print 'Exception'
				menu_item_id_list=['5d7af8a6ebc2840a3e4102f5', '5d7af8afbd2ff10a71cf8bb2', '5d7af8c6bd2ff10a71cf8bf5', '5d7af8ce4e64420a4e6e0c6f', '5d7af8e04e64420a4e6e173f', '5d7af8eccaf5af0a6308e092', '5d7af8f5bd2ff10a71cf8d3e', '5d7af8febd2ff10a71cf8da4', '5d7af90e4e64420a4e6e19e1', '5d7af916caf5af0a6308e39d']
				price_list=[19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9]
				menu_id_list=['5ccb18135e07f10cf41eff3a', '5cafe7a95eb3250cf5c452e0', '5cfb57841e80650d6ff66ff4', '5d3e7e6769d46f0a36c5d651', '5d03c6579f39860d5a11aa86', '5d4673d5654ffb0a5e5ececd', '5d03c745385d7e7f5274d2ad', '5d45136c3646cb0a65af67d2', '5d213f3f65b9bb0cec660df2', '5d213e1665b9bb0cec66050e']
				menu_type='Lunch'


			request=get_totalRevenue_call( campaign_data, device_data, app_data )
			response=util.execute_request(**request)
			try:
				output=json.loads(response.get('data'))
				app_data['total_revenue']=output.get('net_total')

			except:
				app_data['total_revenue']=19.9

			time.sleep(random.randint(15,60))
			request=branch_purchase_event(campaign_data, device_data, app_data, eventName='placing_order')
			util.execute_request(**request)

			time.sleep(1)
			request=branch_purchase_event(campaign_data, device_data, app_data, eventName='order_placed')
			util.execute_request(**request)

			time.sleep(random.randint(2,3))
			adjust_purchase_event_1(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,2))
			adjust_purchase_event_2(campaign_data, app_data, device_data)

	if app_data.get('address_updated')==True and random.randint(1,100)<=16:
		
		time.sleep(random.randint(5,60))
		request=api_branch_logout( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['logout']=True

	if app_data.get('logout')==True and random.randint(1,100)<=75:

		time.sleep(random.randint(30,60))
		request=api_branch_profile( campaign_data, device_data, app_data )
		util.execute_request(**request)

		time.sleep(random.randint(15,20))
		request=credithistory(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,2))
		request=api_branch_credits( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['logout']=False

	time.sleep(random.randint(5,60))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)

	if random.randint(1,100)<=50:

		time.sleep(random.randint(5,60))
		print "Branch______open"
		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)	

		time.sleep(random.randint(60,180))
		print "Branch______close"
		request=api_branch_close(campaign_data, app_data, device_data)
		util.execute_request(**request)

	

	set_appCloseTime(app_data)


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	if not app_data.get('times'):
		print "Please wait installing..."
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	pushtoken(app_data)
	set_androidUUID(app_data)

	if not app_data.get('mixpanel_distinct_id'):
		app_data['mixpanel_distinct_id']=str(uuid.uuid4())

	if not app_data.get('logout'):
		app_data['logout']=False

	global addressId_list

	addressId_list=['5d43ef49c221c20a38091f24', '5d7b4ecb4e64420a4e7fe625', '5d43ef49444beb0a77899464', '5d7b4efebafa630a88f44b5f', '5d7b4f1760f8c70a63faebf1', '5d7b4f28702c2f0a717da6c0', '5d7b4fb44ab2560a5ea7d044', '5d7b5001bafa630a88f45c9b', '5d7b505c258eff0a540fa5b6', '5d7b50a84ab2560a5ea7f4a7', '5d7b50c9bafa630a88f46d6e', '5d7b50eb702c2f0a717dd1ea']


	############ calls #################

	if not app_data.get('device_fingerprint_id1') or not app_data.get('identity_id') or not app_data.get('session_id'):

		print "Branch______install"
		request=api_branch_install(campaign_data,app_data,device_data)
		result=util.execute_request(**request)
		try:
			json_result=json.loads(result.get('data'))
			app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
			app_data['identity_id']=json_result.get('identity_id')
			app_data['session_id']=json_result.get('session_id')
		except:
			app_data['device_fingerprint_id1']='613284344550945604'
			app_data['identity_id']='641581897171978611'
			app_data['session_id']='641581897189090110'	

	print "Branch______open"
	request=api_branch_open(campaign_data, app_data, device_data)
	util.execute_request(**request)	

	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data, isOpen=True)
	util.execute_request(**request)

	time.sleep(1)
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(2,3))
	request=credithistory(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(0,1))
	request=api_branch_credits( campaign_data, device_data, app_data )
	util.execute_request(**request)

	if not app_data.get('signup_complete') or not app_data.get('access_token'):
		time.sleep(random.randint(60,120))
		print 'lat and lon generator'
		lat_lon=current_location(device_data)
		lat_lon_result = util.execute_request(**lat_lon)
		try:
			json_lat_lon_result=json.loads(lat_lon_result.get('data'))
			app_data['city']=str(json_lat_lon_result.get('geo').get('city')).replace(" ","_")
		except:
			app_data['city']="Kuala_Lumpur"

		print "\nself_call_api_dahmakan\n"
		request=api_dahmakan( campaign_data, device_data, app_data )
		response = util.execute_request(**request)
		try:
			result = json.loads(response.get('data')).get('access_token')
			app_data['access_token'] = result

			if not app_data.get('access_token'):
				app_data['access_token'] = "f390bdb6-78ab-415a-a7bb-bc28ce7b5c8a"	
		except:
			app_data['access_token'] = "f390bdb6-78ab-415a-a7bb-bc28ce7b5c8a"	

		print "\nself_call_api_dahmakan\n"
		request=api_dahmakan_users( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			output=json.loads(response.get('data'))
			app_data['identity']=output.get('id')

		except:
			print 'Exception'
			app_data['identity']='5d7b70aa89a34b0ab070ff52'

		time.sleep(random.randint(45,60))
		request=api_branch_profile( campaign_data, device_data, app_data )
		util.execute_request(**request)


		time.sleep(random.randint(2,5))
		signup(campaign_data, app_data, device_data)


		app_data['signup_complete']=True


	if not app_data.get('address_updated'):
		time.sleep(random.randint(10,30))
		request=credithistory(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,2))
		request=api_branch_credits( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['address_updated']=True

	if app_data.get('logout')==True and random.randint(1,100)<=95:

		time.sleep(random.randint(30,60))
		request=api_branch_profile( campaign_data, device_data, app_data )
		util.execute_request(**request)

		time.sleep(random.randint(15,20))
		request=credithistory(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,2))
		request=api_branch_credits( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['logout']=False

	if app_data.get('logout')==False and random.randint(1,100)<=60: #80

		for i in range(random.randint(1,3)):

			time.sleep(random.randint(20,120))
			add_to_cart(campaign_data, app_data, device_data)


		if purchase.isPurchase(app_data,day=1,advertiser_demand=12):

			if not app_data.get('address_index'):
				app_data['address_index']=random.randint(0, len(addressId_list)-1)

			global time_slotId_list, time_list, start_date, menu_item_id_list, price_list, menu_type, menu_id_list

			time_slotId_list=[]
			time_list=[]
			menu_item_id_list=[]
			price_list=[]
			menu_id_list=[]

			request=get_time_slot_id_call( campaign_data, device_data, app_data )
			response=util.execute_request(**request)

			try:
				output=json.loads(response.get('data')).get('dates')
				choice=random.randint(0, len(output)-1)
				start_date=output[choice].get('date')
				temp=output[choice].get('time_slots')

				for i in range(len(temp)):
					time_slotId_list.append(str(temp[i].get('id')))
					time_list.append(str(temp[i].get('display_text')))

				if len(time_slotId_list)<1 or len(time_list)<1 or len(time_slotId_list)!=len(time_list):

					time_slotId_list=['5c8a10bfb10fb80d02cf0124', '5c8a108e0b44f61f1260a172', '5d0b41730191c9778798fe9d', '5d0b41730191c9778798fe9c', '5c8a10bfb10fb80d02cf0125', '5d4bf6bcc0cfcd9ae5284777', '5d4bf6bcc0cfcd9ae5284776', '5d0b41730191c9778798fe9e', '5ca5dfae012dd9216c825de2', '5ca5deee0a59ea2f5adcde47', '5d4bf6bcc0cfcd9ae5284778', '5d0b41730191c9778798fea1', '5d0b41730191c9778798fea0', '5ca5dfaf012dd9216c825de3', '5d4bf6bcc0cfcd9ae528477b', '5d4bf6bcc0cfcd9ae528477a', '5d0b41730191c9778798fea2', '5c5a63704762d3d8741f21df', '5c5a63704762d3d8741f21de', '5d4bf6bcc0cfcd9ae528477c', '5ca5e05cad4bea6b7bb1e6f6', '5ca5deee0a59ea2f5adcde48', '5c5a63724762d3d8741f21e0', '5d4bf6bcc0cfcd9ae528477f', '5d4bf6bcc0cfcd9ae528477e', '5ca5e05cad4bea6b7bb1e6f7', '5d0b41730191c9778798fea5', '5d0b41730191c9778798fea4', '5d4bf6bcc0cfcd9ae5284780', '5c5a63724762d3d8741f21e2', '5c5a63724762d3d8741f21e1', '5d0b41730191c9778798fea6', '5d4bf6bcc0cfcd9ae5284783', '5d4bf6bcc0cfcd9ae5284782', '5c5a63734762d3d8741f21e3', '5ca5e13f0a59ea2f5adcec2f', '5ca5e0e125bf0725cf39d2f6', '5d4bf6bcc0cfcd9ae5284784', '5d0b41730191c9778798fea9', '5d0b41730191c9778798fea8', '5ca5e13f0a59ea2f5adcec30', '5d4bf6bdc0cfcd9ae5284787', '5d4bf6bdc0cfcd9ae5284786', '5d0b41730191c9778798feaa', '5c63ed64521a5a2df97beb81', '5c63ecddc1eed60d187d1ca7', '5d4bf6bdc0cfcd9ae5284788', '5d0b41730191c9778798fead', '5d0b41730191c9778798feac', '5c63ed64521a5a2df97bebcb', '5d4bf6bdc0cfcd9ae528478b', '5d4bf6bdc0cfcd9ae528478a', '5d0b41730191c9778798feae', '5d0b41730191c9778798feb1', '5d0b41730191c9778798feb0', '5d4bf6bdc0cfcd9ae528478c', '5d0b41730191c9778798feb5', '5d0b41730191c9778798feb4', '5d0b41730191c9778798feb2', '5d4bf6bdc0cfcd9ae528478f', '5d4bf6bdc0cfcd9ae528478e', '5d0b41730191c9778798feb6', '5c8a5d720b44f61f12901fe3', '5c8a5d4012eab11be81454bf', '5d4bf6bdc0cfcd9ae5284790', '58d3c7d0e4b08796c12816c4', '58d3c7d0e4b08796c12816c3', '5c8a5d720b44f61f12901fe2', '5d2748172cf48218291d9c10', '5d2748172cf48218291d9c0f', '58d3c7d0e4b08796c12816c5', '5cefd02c1b4ec58dd7b23e5b', '5cefd02c1b4ec58dd7b23e5a', '5d2748172cf48218291d9c11', '58d3c7d0e4b08796c12816c7', '58d3c7d0e4b08796c12816c6', '5cefd02c1b4ec58dd7b23e5c', '5d2748172cf48218291d9c14', '5d2748172cf48218291d9c13', '58d3c7d0e4b08796c12816c8', '5cefd02c1b4ec58dd7b23e5f', '5cefd02c1b4ec58dd7b23e5e', '5d2748172cf48218291d9c15', '58d3c7d0e4b08796c12816ca', '58d3c7d0e4b08796c12816c9', '5cefd02c1b4ec58dd7b23e60', '5d2748172cf48218291d9c18', '5d2748172cf48218291d9c17', '58d3c7d0e4b08796c12816cb', '5cffca311d5774a872e2c86d', '5cffca301d5774a872e2c86c', '5d2748172cf48218291d9c19', '58d3c7d0e4b08796c12816cd', '58d3c7d0e4b08796c12816cc', '5cffca311d5774a872e2c86e', '5d2748172cf48218291d9c1c', '5d2748172cf48218291d9c1b', '58d3c7d0e4b08796c12816ce', '5cffca311d5774a872e2c871', '5cffca311d5774a872e2c870', '5d2748172cf48218291d9c1d', '5933fa7eff8aed5aa46d251b', '5933fa7eff8aed5aa46d251a', '5cffca311d5774a872e2c872', '5d2748172cf48218291d9c20', '5d2748172cf48218291d9c1f', '5933fa7eff8aed5aa46d251c', '5cffca311d5774a872e2c875', '5cffca311d5774a872e2c874', '5d2748172cf48218291d9c21', '5c8a5d92b10fb80d02ebc96b', '5c8a5d190b44f61f127ef1d9', '5cffca311d5774a872e2c876', '5c8a5d92b10fb80d02ebc96c']
					time_list=['9:00 AM - 9:30 AM', '9:00 AM - 10:00 AM', '9:20 AM - 9:50 AM', '9:20 AM - 10:20 AM', '9:30 AM - 10:00 AM', '9:40 AM - 10:10 AM', '9:40 AM - 10:40 AM', '9:50 AM - 10:20 AM', '10:00 AM - 10:30 AM', '10:00 AM - 11:00 AM', '10:10 AM - 10:40 AM', '10:20 AM - 10:50 AM', '10:20 AM - 11:20 AM', '10:30 AM - 11:00 AM', '10:40 AM - 11:10 AM', '10:40 AM - 11:40 AM', '10:50 AM - 11:20 AM', '11:00 AM - 11:30 AM', '11:00 AM - 12:00 PM', '11:10 AM - 11:40 AM', '11:20 AM - 11:50 AM', '11:20 AM - 12:20 PM', '11:30 AM - 12:00 PM', '11:40 AM - 12:10 PM', '11:40 AM - 12:40 PM', '11:50 AM - 12:20 PM', '12:00 PM - 12:30 PM', '12:00 PM - 1:00 PM', '12:10 PM - 12:40 PM', '12:20 PM - 12:50 PM', '12:20 PM - 1:20 PM', '12:30 PM - 1:00 PM', '12:40 PM - 1:10 PM', '12:40 PM - 1:40 PM', '12:50 PM - 1:20 PM', '1:00 PM - 1:30 PM', '1:00 PM - 2:00 PM', '1:10 PM - 1:40 PM', '1:20 PM - 1:50 PM', '1:20 PM - 2:20 PM', '1:30 PM - 2:00 PM', '1:40 PM - 2:10 PM', '1:40 PM - 2:40 PM', '1:50 PM - 2:20 PM', '2:00 PM - 2:30 PM', '2:00 PM - 3:00 PM', '2:10 PM - 2:40 PM', '2:20 PM - 2:50 PM', '2:20 PM - 3:20 PM', '2:30 PM - 3:00 PM', '2:40 PM - 3:10 PM', '2:40 PM - 3:40 PM', '2:50 PM - 3:20 PM', '3:00 PM - 3:30 PM', '3:00 PM - 4:00 PM', '3:10 PM - 3:40 PM', '3:20 PM - 3:50 PM', '3:20 PM - 4:20 PM', '3:30 PM - 4:00 PM', '3:40 PM - 4:10 PM', '3:40 PM - 4:40 PM', '3:50 PM - 4:20 PM', '4:00 PM - 4:30 PM', '4:00 PM - 5:00 PM', '4:10 PM - 4:40 PM', '4:20 PM - 4:50 PM', '4:20 PM - 5:20 PM', '4:30 PM - 5:00 PM', '4:40 PM - 5:10 PM', '4:40 PM - 5:40 PM', '4:50 PM - 5:20 PM', '5:00 PM - 5:30 PM', '5:00 PM - 6:00 PM', '5:10 PM - 5:40 PM', '5:20 PM - 5:50 PM', '5:20 PM - 6:20 PM', '5:30 PM - 6:00 PM', '5:40 PM - 6:10 PM', '5:40 PM - 6:40 PM', '5:50 PM - 6:20 PM', '6:00 PM - 6:30 PM', '6:00 PM - 7:00 PM', '6:10 PM - 6:40 PM', '6:20 PM - 6:50 PM', '6:20 PM - 7:20 PM', '6:30 PM - 7:00 PM', '6:40 PM - 7:10 PM', '6:40 PM - 7:40 PM', '6:50 PM - 7:20 PM', '7:00 PM - 7:30 PM', '7:00 PM - 8:00 PM', '7:10 PM - 7:40 PM', '7:20 PM - 7:50 PM', '7:20 PM - 8:20 PM', '7:30 PM - 8:00 PM', '7:40 PM - 8:10 PM', '7:40 PM - 8:40 PM', '7:50 PM - 8:20 PM', '8:00 PM - 8:30 PM', '8:00 PM - 9:00 PM', '8:10 PM - 8:40 PM', '8:20 PM - 8:50 PM', '8:20 PM - 9:20 PM', '8:30 PM - 9:00 PM', '8:40 PM - 9:10 PM', '8:40 PM - 9:40 PM', '8:50 PM - 9:20 PM', '9:00 PM - 9:30 PM', '9:00 PM - 10:00 PM', '9:10 PM - 9:40 PM', '9:20 PM - 9:50 PM', '9:20 PM - 10:20 PM', '9:30 PM - 10:00 PM', '9:50 PM - 10:20 PM']
					start_date=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%d-%m-%Y")

			except:
				print 'Exception'
				time_slotId_list=['5c8a10bfb10fb80d02cf0124', '5c8a108e0b44f61f1260a172', '5d0b41730191c9778798fe9d', '5d0b41730191c9778798fe9c', '5c8a10bfb10fb80d02cf0125', '5d4bf6bcc0cfcd9ae5284777', '5d4bf6bcc0cfcd9ae5284776', '5d0b41730191c9778798fe9e', '5ca5dfae012dd9216c825de2', '5ca5deee0a59ea2f5adcde47', '5d4bf6bcc0cfcd9ae5284778', '5d0b41730191c9778798fea1', '5d0b41730191c9778798fea0', '5ca5dfaf012dd9216c825de3', '5d4bf6bcc0cfcd9ae528477b', '5d4bf6bcc0cfcd9ae528477a', '5d0b41730191c9778798fea2', '5c5a63704762d3d8741f21df', '5c5a63704762d3d8741f21de', '5d4bf6bcc0cfcd9ae528477c', '5ca5e05cad4bea6b7bb1e6f6', '5ca5deee0a59ea2f5adcde48', '5c5a63724762d3d8741f21e0', '5d4bf6bcc0cfcd9ae528477f', '5d4bf6bcc0cfcd9ae528477e', '5ca5e05cad4bea6b7bb1e6f7', '5d0b41730191c9778798fea5', '5d0b41730191c9778798fea4', '5d4bf6bcc0cfcd9ae5284780', '5c5a63724762d3d8741f21e2', '5c5a63724762d3d8741f21e1', '5d0b41730191c9778798fea6', '5d4bf6bcc0cfcd9ae5284783', '5d4bf6bcc0cfcd9ae5284782', '5c5a63734762d3d8741f21e3', '5ca5e13f0a59ea2f5adcec2f', '5ca5e0e125bf0725cf39d2f6', '5d4bf6bcc0cfcd9ae5284784', '5d0b41730191c9778798fea9', '5d0b41730191c9778798fea8', '5ca5e13f0a59ea2f5adcec30', '5d4bf6bdc0cfcd9ae5284787', '5d4bf6bdc0cfcd9ae5284786', '5d0b41730191c9778798feaa', '5c63ed64521a5a2df97beb81', '5c63ecddc1eed60d187d1ca7', '5d4bf6bdc0cfcd9ae5284788', '5d0b41730191c9778798fead', '5d0b41730191c9778798feac', '5c63ed64521a5a2df97bebcb', '5d4bf6bdc0cfcd9ae528478b', '5d4bf6bdc0cfcd9ae528478a', '5d0b41730191c9778798feae', '5d0b41730191c9778798feb1', '5d0b41730191c9778798feb0', '5d4bf6bdc0cfcd9ae528478c', '5d0b41730191c9778798feb5', '5d0b41730191c9778798feb4', '5d0b41730191c9778798feb2', '5d4bf6bdc0cfcd9ae528478f', '5d4bf6bdc0cfcd9ae528478e', '5d0b41730191c9778798feb6', '5c8a5d720b44f61f12901fe3', '5c8a5d4012eab11be81454bf', '5d4bf6bdc0cfcd9ae5284790', '58d3c7d0e4b08796c12816c4', '58d3c7d0e4b08796c12816c3', '5c8a5d720b44f61f12901fe2', '5d2748172cf48218291d9c10', '5d2748172cf48218291d9c0f', '58d3c7d0e4b08796c12816c5', '5cefd02c1b4ec58dd7b23e5b', '5cefd02c1b4ec58dd7b23e5a', '5d2748172cf48218291d9c11', '58d3c7d0e4b08796c12816c7', '58d3c7d0e4b08796c12816c6', '5cefd02c1b4ec58dd7b23e5c', '5d2748172cf48218291d9c14', '5d2748172cf48218291d9c13', '58d3c7d0e4b08796c12816c8', '5cefd02c1b4ec58dd7b23e5f', '5cefd02c1b4ec58dd7b23e5e', '5d2748172cf48218291d9c15', '58d3c7d0e4b08796c12816ca', '58d3c7d0e4b08796c12816c9', '5cefd02c1b4ec58dd7b23e60', '5d2748172cf48218291d9c18', '5d2748172cf48218291d9c17', '58d3c7d0e4b08796c12816cb', '5cffca311d5774a872e2c86d', '5cffca301d5774a872e2c86c', '5d2748172cf48218291d9c19', '58d3c7d0e4b08796c12816cd', '58d3c7d0e4b08796c12816cc', '5cffca311d5774a872e2c86e', '5d2748172cf48218291d9c1c', '5d2748172cf48218291d9c1b', '58d3c7d0e4b08796c12816ce', '5cffca311d5774a872e2c871', '5cffca311d5774a872e2c870', '5d2748172cf48218291d9c1d', '5933fa7eff8aed5aa46d251b', '5933fa7eff8aed5aa46d251a', '5cffca311d5774a872e2c872', '5d2748172cf48218291d9c20', '5d2748172cf48218291d9c1f', '5933fa7eff8aed5aa46d251c', '5cffca311d5774a872e2c875', '5cffca311d5774a872e2c874', '5d2748172cf48218291d9c21', '5c8a5d92b10fb80d02ebc96b', '5c8a5d190b44f61f127ef1d9', '5cffca311d5774a872e2c876', '5c8a5d92b10fb80d02ebc96c']
				time_list=['9:00 AM - 9:30 AM', '9:00 AM - 10:00 AM', '9:20 AM - 9:50 AM', '9:20 AM - 10:20 AM', '9:30 AM - 10:00 AM', '9:40 AM - 10:10 AM', '9:40 AM - 10:40 AM', '9:50 AM - 10:20 AM', '10:00 AM - 10:30 AM', '10:00 AM - 11:00 AM', '10:10 AM - 10:40 AM', '10:20 AM - 10:50 AM', '10:20 AM - 11:20 AM', '10:30 AM - 11:00 AM', '10:40 AM - 11:10 AM', '10:40 AM - 11:40 AM', '10:50 AM - 11:20 AM', '11:00 AM - 11:30 AM', '11:00 AM - 12:00 PM', '11:10 AM - 11:40 AM', '11:20 AM - 11:50 AM', '11:20 AM - 12:20 PM', '11:30 AM - 12:00 PM', '11:40 AM - 12:10 PM', '11:40 AM - 12:40 PM', '11:50 AM - 12:20 PM', '12:00 PM - 12:30 PM', '12:00 PM - 1:00 PM', '12:10 PM - 12:40 PM', '12:20 PM - 12:50 PM', '12:20 PM - 1:20 PM', '12:30 PM - 1:00 PM', '12:40 PM - 1:10 PM', '12:40 PM - 1:40 PM', '12:50 PM - 1:20 PM', '1:00 PM - 1:30 PM', '1:00 PM - 2:00 PM', '1:10 PM - 1:40 PM', '1:20 PM - 1:50 PM', '1:20 PM - 2:20 PM', '1:30 PM - 2:00 PM', '1:40 PM - 2:10 PM', '1:40 PM - 2:40 PM', '1:50 PM - 2:20 PM', '2:00 PM - 2:30 PM', '2:00 PM - 3:00 PM', '2:10 PM - 2:40 PM', '2:20 PM - 2:50 PM', '2:20 PM - 3:20 PM', '2:30 PM - 3:00 PM', '2:40 PM - 3:10 PM', '2:40 PM - 3:40 PM', '2:50 PM - 3:20 PM', '3:00 PM - 3:30 PM', '3:00 PM - 4:00 PM', '3:10 PM - 3:40 PM', '3:20 PM - 3:50 PM', '3:20 PM - 4:20 PM', '3:30 PM - 4:00 PM', '3:40 PM - 4:10 PM', '3:40 PM - 4:40 PM', '3:50 PM - 4:20 PM', '4:00 PM - 4:30 PM', '4:00 PM - 5:00 PM', '4:10 PM - 4:40 PM', '4:20 PM - 4:50 PM', '4:20 PM - 5:20 PM', '4:30 PM - 5:00 PM', '4:40 PM - 5:10 PM', '4:40 PM - 5:40 PM', '4:50 PM - 5:20 PM', '5:00 PM - 5:30 PM', '5:00 PM - 6:00 PM', '5:10 PM - 5:40 PM', '5:20 PM - 5:50 PM', '5:20 PM - 6:20 PM', '5:30 PM - 6:00 PM', '5:40 PM - 6:10 PM', '5:40 PM - 6:40 PM', '5:50 PM - 6:20 PM', '6:00 PM - 6:30 PM', '6:00 PM - 7:00 PM', '6:10 PM - 6:40 PM', '6:20 PM - 6:50 PM', '6:20 PM - 7:20 PM', '6:30 PM - 7:00 PM', '6:40 PM - 7:10 PM', '6:40 PM - 7:40 PM', '6:50 PM - 7:20 PM', '7:00 PM - 7:30 PM', '7:00 PM - 8:00 PM', '7:10 PM - 7:40 PM', '7:20 PM - 7:50 PM', '7:20 PM - 8:20 PM', '7:30 PM - 8:00 PM', '7:40 PM - 8:10 PM', '7:40 PM - 8:40 PM', '7:50 PM - 8:20 PM', '8:00 PM - 8:30 PM', '8:00 PM - 9:00 PM', '8:10 PM - 8:40 PM', '8:20 PM - 8:50 PM', '8:20 PM - 9:20 PM', '8:30 PM - 9:00 PM', '8:40 PM - 9:10 PM', '8:40 PM - 9:40 PM', '8:50 PM - 9:20 PM', '9:00 PM - 9:30 PM', '9:00 PM - 10:00 PM', '9:10 PM - 9:40 PM', '9:20 PM - 9:50 PM', '9:20 PM - 10:20 PM', '9:30 PM - 10:00 PM', '9:50 PM - 10:20 PM']
				start_date=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%d-%m-%Y")


			request=get_menu_id_call( campaign_data, device_data, app_data )
			response=util.execute_request(**request)
			try:
				output=json.loads(response.get('data')).get('dates')[0]

				menu_type=output.get('menu_type')

				temp=output.get('menu').get('lines')
				choice=random.randint(0, len(temp)-1)
				li=temp[choice].get('items')
				for i in range(len(li)):
					menu_id_list.append(str(li[i].get('menu_id')))
					price_list.append(li[i].get('price'))
					menu_item_id_list.append(str(li[i].get('id')))

				if len(menu_item_id_list)<1 or len(price_list)<1 or len(menu_id_list)<1 or len(menu_item_id_list)!=len(price_list) or len(price_list)!=len(menu_id_list):

					menu_item_id_list=['5d7af8a6ebc2840a3e4102f5', '5d7af8afbd2ff10a71cf8bb2', '5d7af8c6bd2ff10a71cf8bf5', '5d7af8ce4e64420a4e6e0c6f', '5d7af8e04e64420a4e6e173f', '5d7af8eccaf5af0a6308e092', '5d7af8f5bd2ff10a71cf8d3e', '5d7af8febd2ff10a71cf8da4', '5d7af90e4e64420a4e6e19e1', '5d7af916caf5af0a6308e39d']
					price_list=[19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9]
					menu_id_list=['5ccb18135e07f10cf41eff3a', '5cafe7a95eb3250cf5c452e0', '5cfb57841e80650d6ff66ff4', '5d3e7e6769d46f0a36c5d651', '5d03c6579f39860d5a11aa86', '5d4673d5654ffb0a5e5ececd', '5d03c745385d7e7f5274d2ad', '5d45136c3646cb0a65af67d2', '5d213f3f65b9bb0cec660df2', '5d213e1665b9bb0cec66050e']
					menu_type='Lunch'
				

			except:
				print 'Exception'
				menu_item_id_list=['5d7af8a6ebc2840a3e4102f5', '5d7af8afbd2ff10a71cf8bb2', '5d7af8c6bd2ff10a71cf8bf5', '5d7af8ce4e64420a4e6e0c6f', '5d7af8e04e64420a4e6e173f', '5d7af8eccaf5af0a6308e092', '5d7af8f5bd2ff10a71cf8d3e', '5d7af8febd2ff10a71cf8da4', '5d7af90e4e64420a4e6e19e1', '5d7af916caf5af0a6308e39d']
				price_list=[19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9, 19.9]
				menu_id_list=['5ccb18135e07f10cf41eff3a', '5cafe7a95eb3250cf5c452e0', '5cfb57841e80650d6ff66ff4', '5d3e7e6769d46f0a36c5d651', '5d03c6579f39860d5a11aa86', '5d4673d5654ffb0a5e5ececd', '5d03c745385d7e7f5274d2ad', '5d45136c3646cb0a65af67d2', '5d213f3f65b9bb0cec660df2', '5d213e1665b9bb0cec66050e']
				menu_type='Lunch'


			request=get_totalRevenue_call( campaign_data, device_data, app_data )
			response=util.execute_request(**request)
			try:
				output=json.loads(response.get('data'))
				app_data['total_revenue']=output.get('net_total')

			except:
				app_data['total_revenue']=19.9

			time.sleep(random.randint(15,60))
			request=branch_purchase_event(campaign_data, device_data, app_data, eventName='placing_order')
			util.execute_request(**request)

			time.sleep(1)
			request=branch_purchase_event(campaign_data, device_data, app_data, eventName='order_placed')
			util.execute_request(**request)

			time.sleep(random.randint(2,3))
			adjust_purchase_event_1(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,2))
			adjust_purchase_event_2(campaign_data, app_data, device_data)



	if app_data.get('logout')==False and random.randint(1,100)<=10:

		time.sleep(random.randint(5,60))
		request=api_branch_logout( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['logout']=True

	if app_data.get('logout')==True and random.randint(1,100)<=50:

		time.sleep(random.randint(30,60))
		request=api_branch_profile( campaign_data, device_data, app_data )
		util.execute_request(**request)

		time.sleep(random.randint(15,20))
		request=credithistory(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,2))
		request=api_branch_credits( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['logout']=False

	time.sleep(random.randint(5,60))

	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)

	if random.randint(1,100)<=50:

		time.sleep(random.randint(5,60))
		print "Branch______open"
		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)	

		time.sleep(random.randint(60,180))
		print "Branch______close"
		request=api_branch_close(campaign_data, app_data, device_data)
		util.execute_request(**request)


	set_appCloseTime(app_data)

	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def signup(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________t3dnyc'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='t3dnyc',t1=t1,t2=t2)
	util.execute_request(**request)

def add_to_cart(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________oratr3'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='oratr3',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_purchase_event_1(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________kqlkum'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='kqlkum',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_purchase_event_2(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________f1xug2'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='f1xug2',t1=t1,t2=t2)
	util.execute_request(**request)

###################################################################
#
#
#			Self  Calls
#
#
###################################################################	

def api_dahmakan( campaign_data, device_data, app_data ):
	register_user(app_data)
	url= "http://api.dahmakan.com/v3/users/create"
	method= "post"
	headers= {       
		"Accept-Encoding": "gzip",
        "Accept-Language": device_data.get('locale').get('language'),
        "City": app_data.get('city'),
        "Content-Type": "application/json",
        "User-Agent": "android_dahmakan_app",
        "X-Device-Id": device_data.get('mac'),
        "app_version": campaign_data.get('app_version_code')
        }

	params= {       
				"hashkey": util.get_random_string(type='hex',size=32)
			}

	data= {       
		"device_token": app_data.get('pushtoken'),
        "email": app_data.get('user').get('email'),
        "password": app_data.get('user').get('username')
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

	
def api_dahmakan_users( campaign_data, device_data, app_data ):
	url= "http://api.dahmakan.com/v3/users"
	method= "get"
	headers= {       
		"Accept-Encoding": "gzip",
        "Accept-Language": device_data.get('locale').get('language'),
        "Authorization": "Bearer "+app_data.get('access_token'),
        "City": app_data.get('city'),
        "Content-Type": "application/json",
        "User-Agent": "android_dahmakan_app",
        "X-Device-Id": device_data.get('mac'),
        "app_version": campaign_data.get('app_version_code')
        }

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	


def get_time_slot_id_call( campaign_data, device_data, app_data ):
	url= "http://api.dahmakan.com/v7/menus/calendar"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "Accept-Language": device_data.get('locale').get('language'),
        "Authorization": "Bearer "+app_data.get('access_token'),
        "City": "Kuala_Lumpur",
        "User-Agent": "android_dahmakan_app",
        "X-Address-Id": addressId_list[app_data.get('address_index')],
        "X-Device-Id": device_data.get('mac'),
        "app_version": campaign_data.get('app_version_code')}

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def get_menu_id_call( campaign_data, device_data, app_data ):

	try:
		app_data['time_index']=random.randint(0, len(time_slotId_list)-1)
		time_slot=time_slotId_list[app_data.get('time_index')]		

	except:
		print 'Exception'
		time_slot='5d4bf6bcc0cfcd9ae5284777'

	url= "http://api.dahmakan.com/v7/menus/stream"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "Accept-Language": device_data.get('locale').get('language'),
        "Authorization": "Bearer "+app_data.get('access_token'),
        "City": "Kuala_Lumpur",
        "User-Agent": "android_dahmakan_app",
        "X-Address-Id": addressId_list[app_data.get('address_index')],
        "X-Device-Id": device_data.get('mac'),
        "X-Time-Slot-Id": time_slot,
        "app_version": campaign_data.get('app_version_code')}

	params= {       "endDate": start_date,
        "imageDimension": "Medium",
        "startDate": start_date,
        "use_company": "false"}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def get_totalRevenue_call( campaign_data, device_data, app_data ):
	try:
		if not app_data.get('time_index'):
			app_data['time_index']=random.randint(0, len(time_slotId_list)-1)

		time_slot=time_slotId_list[app_data.get('time_index')]

		selected_time=time_list[app_data.get('time_index')]

		if not app_data.get('menu_index'):
			app_data['menu_index']=random.randint(0, len(menu_item_id_list)-1)

		menu_id=menu_id_list[app_data.get('menu_index')]
		menu_item_id=menu_item_id_list[app_data.get('menu_index')]
		price=price_list[app_data.get('menu_index')]

	except:
		print 'Exception'
		time_slot='5d4bf6bcc0cfcd9ae5284777'
		selected_time='9:40 AM - 10:10 AM'
		menu_id='5d3e7e6769d46f0a36c5d651'
		menu_item_id='5d7af8ce4e64420a4e6e0c6f'
		price=19.9

	url= "http://api.dahmakan.com/v4/orders/prices"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Accept-Language": device_data.get('locale').get('language'),
        "Authorization": "Bearer "+app_data.get('access_token'),
        "City": "Kuala_Lumpur",
        "Content-Type": "application/json; charset=UTF-8",
        "User-Agent": "android_dahmakan_app",
        "X-Address-Id": addressId_list[app_data.get('address_index')],
        "X-Device-Id": device_data.get('mac'),
        "X-Time-Slot-Id": time_slot,
        "app_version": campaign_data.get('app_version_code')}



	params= None

	data= {       "orders": [       {       "address_id": addressId_list[app_data.get('address_index')],
                                  "any_item_available": False,
                                  "available": False,
                                  "can_use_fpx_payment": False,
                                  "date": start_date,
                                  "menu_item": [       {       "id": menu_id,
                                                               "item_type": "Item",
                                                               "menu_item_id": menu_item_id,
                                                               "price": price,
                                                               "quantity": 1,
                                                               "selected_time_frame_open": False}],
                                  "menu_type": menu_type,
                                  "selected_time_frame_open": False,
                                  "selected_time_slot_details": {       "formatted_time_frame": selected_time,
                                                                        "time_frame_display_text": selected_time,
                                                                        "time_slot_id": time_slot},
                                  "time_slot_id": time_slot,
                                  "use_company": False}]}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


def current_location(device_data):
	url='http://lumtest.com/myip.json'
		
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}

###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	pushtoken(app_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())

	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "0",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"vm_isa" : "arm64",

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if isOpen:
		def_(app_data,'subsession_count')
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('appCloseTime')-app_data.get('sess_len')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('appCloseTime')-app_data.get('sess_len')
		app_data['sess_len']=int(time.time())


	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'session')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,t3=0,t4=0,callback_params=None,partner_params=None):
	pushtoken(app_data)
	reftag = ''
	if app_data.get('referrer'):
		temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
		if temp_b.get('adjust_reftag'):
			reftag = temp_b.get('adjust_reftag')[0]
	sdk_clk_time=get_date(app_data,device_data)
	time.sleep(random.randint(t3,t4))
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"install_begin_time" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time")),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "0",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"referrer" : "adjust_reftag=cYLf35aouFof5&utm_source=XYAds_V1_0305&utm_campaign=2051&utm_content=238&utm_term=7_90221",
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : 1,
		"session_length" : session_length,
		"source" : source,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"vm_isa" : "arm64",

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		if click_time:
			data['click_time']=sdk_clk_time
		elif data.get("click_time"):
			del data['click_time']
		# del data['session_length']
		# del data['subsession_count']
		# del data['time_spent']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		if click_time:
			data['click_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("click_time"))
		elif data.get("click_time"):
			del data['click_time']
		
		if data.get('raw_referrer'):
			del data['raw_referrer']
		
		data['install_begin_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time"))
		# del data['last_interval']



	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'click', source = data.get('source'))
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,t1=0,t2=0):
	inc_(app_data,'subsession_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"attribution_deeplink" : "1",
		"created_at" : created_at,
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"initiated_by" : "backend",
		"needs_response_details" : "1",
		"sent_at" : sent_at,
		"tracking_enabled" : "1",

		}
	data = {

		}

	
	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('gps_adid'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"event_count" : app_data.get('event_count'),
		"event_token" : event_token,
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "0",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"session_length" : session_length,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"vm_isa" : "arm64",

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if event_token=='kqlkum':
		data['currency']='MYR'
		revenue=str(app_data.get('total_revenue'))
		if '.' in revenue:
			data['revenue']=str(app_data.get('total_revenue'))+'0000'
		else:
			data['revenue']=str(app_data.get('total_revenue'))+'.00000'


	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
################ BRANCH CALLS ###################

def branch_call(campaign_data, app_data, device_data):
	url='http://cdn.branch.io/sdk/uriskiplist_v2.json'
	method='get'
	headers={
		'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
	}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': None}


def api_branch_install(campaign_data,app_data,device_data):
	method='post'
	url='http://api.branch.io/v1/install'

	headers = {
				'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
				'Content-Type':'application/json',
				'Accept':'application/json',
			}

	data={"app_version":campaign_data.get('app_version_name'),
		"facebook_app_link_checked":False,
		"is_referrable":1,
		"update":0,
		"debug":False,
		"metadata":{},
		"hardware_id":device_data.get('android_id'),
		# 'clicked_referrer_ts':int(app_data.get('times').get('click_time')),
		# 'install_begin_ts':int(app_data.get('times').get('download_begin_time')),
		"is_hardware_id_real":True,
		"brand":device_data.get('brand').upper(),
		"model":device_data.get('model'),
		"screen_dpi":int(device_data.get('dpi')),
		"screen_height":int(device_data.get('resolution').split('x')[0]),
		"screen_width":int(device_data.get('resolution').split('x')[1]),
		"wifi":True,
		"os":"Android",
		"os_version":int(device_data.get('sdk')),
		"country":device_data.get('locale').get('country'),
		"language":device_data.get('locale').get('language'),
		"local_ip":device_data.get('private_ip'),
		"cd":{"mv":"-1","pn":campaign_data.get('package_name')},
		"google_advertising_id":device_data.get('adid'),
		"lat_val":0,
		"instrumentation":{"v1/install-qwt":"0"},
		"sdk":campaign_data.get('api_branch').get('sdk'),
		"retryNumber":0,
		"branch_key":campaign_data.get('api_branch').get('branchkey'),
		'environment':'FULL_APP',
		'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
		'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'ui_mode':'UI_MODE_TYPE_NORMAL',
		'previous_update_time':0
		} 

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}

def api_branch_profile( campaign_data, device_data, app_data ):
	url= "http://api2.branch.io/v1/profile"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data),}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand').upper(),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity": app_data.get('identity'),
        "identity_id": app_data.get('identity_id'),

        "instrumentation": {       "v1\\/install-brtt": "1936",
                                   "v1\\/profile-qwt": "5"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {       "$mixpanel_distinct_id": app_data.get('mixpanel_distinct_id')},
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),		
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def credithistory( campaign_data, app_data, device_data ):
	url= "http://api2.branch.io/v1/credithistory"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent":get_ua(device_data),}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand').upper(),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "direction": 0,
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       "v1\\/credithistory-qwt": "2",
                                   "v1\\/profile-brtt": "1926"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "length": 100,
        "local_ip": device_data.get('private_ip'),
        "metadata": {       "$mixpanel_distinct_id": app_data.get('mixpanel_distinct_id')},
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height":  int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk":  campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def api_branch_credits( campaign_data, device_data, app_data ):
	url= "http://api2.branch.io/v1/credits/"+str(app_data.get('identity_id'))
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent": get_ua(device_data),}

	params= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "google_advertising_id": device_data.get('adid'),
        "lat_val": "0",
        "metadata": str({"$mixpanel_distinct_id":app_data.get('mixpanel_distinct_id')},),
        "retryNumber": "0",
        "sdk": campaign_data.get('api_branch').get('sdk'),}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def api_branch_logout( campaign_data, device_data, app_data ):
	url= "http://api2.branch.io/v1/logout"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data),}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand').upper(),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       "v1\\/logout-qwt": "0", "v1\\/open-brtt": "703"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {       "$mixpanel_distinct_id": app_data.get('mixpanel_distinct_id')},
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk":  campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


def api_branch_open(campaign_data, app_data, device_data, call_seq=1):	
	
	url='http://api.branch.io/v1/open'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"app_version": campaign_data.get('app_version_name'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand').upper(),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			"country":device_data.get('locale').get('country'),
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/close-brtt":"1065","v1/open-qwt":"0"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
			"cd":{'mv':'-1','pn':campaign_data.get('package_name')},
			"debug" :False,
			"environment" :'FULL_APP',
			"facebook_app_link_checked":False,
			"google_advertising_id":device_data.get('adid'),
			"is_referrable":1,
			"lat_val":0,
			"update":1,
			'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
			'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
			'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
			'previous_update_time':int(app_data.get('times').get('install_complete_time')*1000)
	}
	if call_seq==2:
		data['instrumentation']={"v1/close-brtt":"891","v1/open-qwt":"0"}
	if call_seq==3:
		data['instrumentation']={"v1/close-brtt":"681","v1/open-qwt":"0"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}


def api_branch_close(campaign_data, app_data, device_data,call_seq=1):	
	
	url='http://api.branch.io/v1/close'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"session_id":app_data.get('session_id'),
			"app_version": campaign_data.get('app_version_name'),
			'google_advertising_id': device_data.get('adid'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand').upper(),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			"country":device_data.get('locale').get('country'),
			"lat_val":0,
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/close-qwt":"2","v1/install-brtt":"648"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
	}
	if call_seq==2:
		data['instrumentation']={"v1/close-qwt":"10","v1/open-brtt":"692"}
	if call_seq==3:
		data['instrumentation']={"v1/close-qwt":"1","v1/open-brtt":"507"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}


def branch_purchase_event( campaign_data, device_data, app_data, eventName ):
	url= "http://api2.branch.io/v1/event"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data),}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand').upper(),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "environment": "FULL_APP",
        "event": eventName,
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id":app_data.get('identity_id'),
        "instrumentation": {       "v1\\/credithistory-brtt": "1208",
                                   "v1\\/credits\\/-brtt": "514",
                                   "v1\\/credits\\/-qwt": "1210",
                                   "v1\\/event-qwt": "1"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {       "$mixpanel_distinct_id": app_data.get('mixpanel_distinct_id'),
                            },
        "model": device_data.get('model'),
        "os": "Android",
      	"os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk":  campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}

	if eventName=='placing_order':
		try:
			if not app_data.get('time_index'):
				app_data['time_index']=random.randint(0, len(time_slotId_list)-1)

			time_frame=time_list[app_data.get('time_index')]
			if not app_data.get('menu_index'):
				app_data['menu_index']=random.randint(0, len(menu_id_list)-1)

			menu_id=menu_id_list[app_data.get('menu_index')]

		except:
			print 'Exception'
			time_frame='9:40 AM - 10:10 AM'
			menu_id='5d3e7e6769d46f0a36c5d651'

		data['metadata']['orders']= [       {       "address_id": addressId_list[app_data.get('address_index')],
		                                              "date": start_date,
		                                              "instant_delivery": False,
		                                              "menu_item": [       {       "id": menu_id,
		                                                                           "quantity": 1}],
		                                              "menu_type": menu_type,
		                                              "time_frame": time_frame}]

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	return app_data.get('android_uuid')


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']='f0L4WFS5AEk:'+util.get_random_string('google_token',140)
	return app_data.get('pushtoken')

def get_auth(device_data,app_data,created_at,gps_adid,activity_type, source = ''):

	if activity_type == 'click':
		data1 = str(created_at+activity_type+source+campaign_data['adjust']['secret_key']+gps_adid)
		sign= util.sha256(data1)
		return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at activity_kind source app_secret gps_adid"'
	else:
		data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activity_type)
		sign= util.sha256(data1)
		return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'

def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"