from sdk import util, purchase, installtimenew
import json,uuid,urlparse,time,random,datetime,array,hashlib, urllib
from sdk import getsleep
import Config, clicker

campaign_data = {
				'package_name' : 'com.naturalmotion.customstreetracer2',
				'ctr'		  :6,
				'app_name' : 'CSR Racing 2',
				'app_version_name' : '2.7.2',#'2.7.0',#2.6.3, '2.6.2',#'2.6.1',#'2.6.0',#'2.5.4',#'2.5.3',#'2.5.0',#'2.4.1',#'2.4.0',#'2.3.2',#'2.3.0',#'2.2.0',#'1.23.1',#'1.22.0',#'1.20.1',#'1.20.0', 1.21.0, 2.0.0, 2.1.1
				'supported_countries': 'WW',
				'device_targeting':True,
				'no_referrer': True,
				'supported_os': '4.4', #Requires Android 4.4 and up
				'tracker' : 'adjust',
				'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
				'adjust':
						{
							'app_token': 'fsjc6iavopa8',
							'sdk':'unity4.17.0@android4.17.0', #unity4.10.0@android4.10.2
							'secret_key': '150160368716931889489033704662113098080',
							'secret_id'	: '1',
						},
				'csr':{
							'sdk_dr':'Apsalar/6.6-UnitySSL',
							'api_key':'naturalmotion',
							'secret':'mLK2PJFc',
				},
				'app_size':65.0,#64.0,#66.6,#66.0,#64.0,
				'purchase_var':{
				'1':'1.99000',
				'2':'2.99000',
				'3':'4.99000',
				},
				'retention':{
							1:70,
							2:68,
							3:66,
							4:64,
							5:61,
							6:59,
							7:57,
							8:52,
							9:50,
							10:47,
							11:45,
							12:43,
							13:40,
							14:37,
							15:35,
							16:31,
							17:30,
							18:28,
							19:26,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5
				}
}

###########################################################
#														  #
#						INSTALL							  #
#														  #
###########################################################
def install(app_data, device_data):
	print 'please wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)	

	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;

	print '---------------------------Install---------------------------------'
	

	if int(device_data.get("sdk")) >19:
		app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		app_data['ua'] = 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

	###########		Initialize		############
	app_data['session_count'] = 0
	app_data['subsession_count'] = 0
	app_data['event_count'] = 0
	app_data['android_uuid'] = str(uuid.uuid4())
	app_data['gameID']=str(random.randint(5000000,5005000))
	app_data['ZID']=str(random.randint(72379000000,72379100000))

	##########			Calls		############
	print "csr"	
	csr_2 = apsalar_resolve(campaign_data,app_data,device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**csr_2)

	print '\n\t\t\t\tAPSALAR START'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_start)

	
	print '\n\t\t\t\tAPSALAR EVENT'	 
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='CSR2 Apsalar Start')
	util.execute_request(**apsalar_event)
	
	# print '\n\t\t\t\tAPSALAR EVENT'	 
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='__InstallReferrer',e={"referrer":app_data.get('referrer')})
	util.execute_request(**apsalar_event)

	app_data['adjust_call_time']=int(time.time())
	print "\n\n------------------------ADJUST SESSION-------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	print "\n\n-----------------------ADJUST SDK Click-------------------------------------"
	adjust_sdkClick(campaign_data, app_data, device_data)

	print "selfcall-- for registration"
	req=registration_selfcall(app_data,campaign_data,device_data)
	util.execute_request(**req)

	
	print '\n\t\t\t\tAPSALAR EVENT'	 
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='registration',e={"users_zid":app_data.get('ZID')})
	util.execute_request(**apsalar_event)

	# print '\n\t\t\t\tAPSALAR EVENT'
	# apsalar_event = apsalarEvent_referrer(campaign_data, app_data, device_data)
	# util.execute_request(**apsalar_event)

	print "\n\n-----------------------ADJUST Event-------------------------------------"
	interval = random.randint(15,30)
	adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="vk8ygv",event_name="adjust_device_tracking")
	util.execute_request(**adjustEvent)

	print "\n\n-----------------------ADJUST Event-------------------------------------"
	interval = random.randint(15,30)
	adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="swuzfp",event_name="AppOpen")
	util.execute_request(**adjustEvent)

	time.sleep(random.randint(5,15))
	print "\n\n-----------------------ADJUST Attribution-------------------------------------"
	request = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)

	print "\n\n-----------------------ADJUST SDK Click-------------------------------------"
	adjust_sdkClick(campaign_data, app_data, device_data,source='install_referrer')


	print "\n\n-----------------------ADJUST Event-------------------------------------"
	interval = random.randint(15,30)
	adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="6xvpxi",event_name="ZIDRegistration")
	util.execute_request(**adjustEvent)
	
	if random.randint(1,100)<=50:
		print '\n\t\t\t\tAPSALAR EVENT'	 
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='heartbeat')
		util.execute_request(**apsalar_event)


	call_event(app_data,device_data,'install')

	print '\n\t\t\t\tAPSALAR EVENT'	 
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='end_session')
	util.execute_request(**apsalar_event)

	###########		Finalize		############
	app_data['app_close_time'] = int(time.time())
	return {'status':True}

###########################################################
#														  #
#						OPEN							  #
#														  #
###########################################################
def open(app_data, device_data,day=1):
	print '---------------------------Open---------------------------------'
	if not app_data.get('times'):
		print 'please wait...'
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)	

	###########		Initialize		############
	if not app_data.get('sec'):
		timez = device_data.get('timezone')
		sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
		if int(timez) < 0:
			sec = (-1) * sec
		if not app_data.get('sec'):
			app_data['sec'] = sec;
	if not app_data.get('session_count'):
		app_data['session_count'] = 0
	if not app_data.get('subsession_count'):	
		app_data['subsession_count'] = 0
	if not app_data.get('event_count'):
		app_data['event_count'] = 0
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	if not app_data.get('gameID'):
		app_data['gameID']=str(random.randint(5000000,5005000))
	if not app_data.get('ZID'):
		app_data['ZID']=str(random.randint(72379000000,72379100000))

	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())

	if not app_data.get('app_close_time'):
		app_data['app_close_time']=int(time.time())
	#########			Calls		############
	print "csr"	
	csr_2 = apsalar_resolve(campaign_data,app_data,device_data)
	util.execute_request(**csr_2)

	print '\n\t\t\t\tAPSALAR START'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_start)
	
	print '\n\t\t\t\tAPSALAR EVENT'	 
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='CSR2 Apsalar Start')
	util.execute_request(**apsalar_event)
	
	# print '\n\t\t\t\tAPSALAR EVENT'	 
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='__InstallReferrer',e={"referrer":app_data.get('referrer')})
	util.execute_request(**apsalar_event)

	print "\n\n------------------------ADJUST SESSION-------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,event="open")
	util.execute_request(**adjustSession)
	app_data['adjust_call_time']=int(time.time())

	print '\n\t\t\t\tAPSALAR EVENT'	 
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='registration',e={"users_zid":app_data.get('ZID')})
	util.execute_request(**apsalar_event)


	print "\n\n-----------------------ADJUST Event-------------------------------------"
	adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="vk8ygv",event_name="adjust_device_tracking")
	util.execute_request(**adjustEvent)

	print "\n\n-----------------------ADJUST Event-------------------------------------"
	adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="swuzfp",event_name="AppOpen")
	util.execute_request(**adjustEvent)

	time.sleep(random.randint(5,15))
	print "\n\n-----------------------ADJUST Attribution-------------------------------------"
	request = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)

	print "\n\n-----------------------ADJUST Event-------------------------------------"
	adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="6xvpxi",event_name="ZIDRegistration")
	util.execute_request(**adjustEvent)

	print "\n\n-----------------------ADJUST Event-------------------------------------"
	adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="6xvpxi",event_name="ZIDRegistration")
	util.execute_request(**adjustEvent)	

	if random.randint(1,100)<=50:
		print '\n\t\t\t\tAPSALAR EVENT'	 
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='heartbeat')
		util.execute_request(**apsalar_event)	

	call_event(app_data,device_data,'open')

	if purchase.isPurchase(app_data,day,advertiser_demand=10):
		# pk=random.choice(["com.naturalmotion.customstreetracer2.2016.starterpack12","com.naturalmotion.customstreetracer2.2016.tier2bundle"])
		# print '\n\t\t\t\tAPSALAR EVENT'	 
		# apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='__iap__',e={"ps":"NM_GP","pk":pk,"pn":"TEXT_FS_BUNDLE_NAME","pc":"BUNDLE","pcc":"USD","pq":1,"pp":"1.99","r":"1.39"})
		# util.execute_request(**apsalar_event)

		print "\n\n-----------------------ADJUST Event-------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="pszt2x",event_name="Purchase")
		util.execute_request(**adjustEvent)	


	print '\n\t\t\t\tAPSALAR EVENT'	 
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='end_session')
	util.execute_request(**apsalar_event)			

	#########		Finalize		############
	app_data['app_close_time'] = int(time.time())
	return {'status':True}




def call_event(app_data,device_data,type='install'):
	if not app_data.get('level'):
		app_data['level']=2

	if not app_data.get('smp5'):
		app_data['smp5']=False

	if not app_data.get('smp10'):
		app_data['smp10']=False	

	if not app_data.get('crew'):
		app_data['crew']=False

	rate=0	
	
	if type=='install':
		if random.randint(1,100)<=70:
			top=random.randint(0,4)
		else:
			top=random.randint(4,6)

	if type=='open':
		if random.randint(1,100)<=60:
			top=random.randint(0,3)
		else:
			top=random.randint(3,5)		

	if app_data.get('level')==2:
		print "Im level 1"
		time.sleep(random.randint(200,240))
	if app_data.get('level')==3:
		print "Im level 3"
		time.sleep(random.randint(450,500))
	if app_data.get('level')==4:
		print "Im level 4"
		time.sleep(random.randint(500,600))
	if app_data.get('level')>=5:
		print "Im level 5"
		time.sleep(random.randint(750,850))
	

	if random.randint(1,100)<=90:
		for x in range(top):
			print "\n\n-----------------------ADJUST Event-------------------------------------"
			adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="jxontr",event_name="LeveledUp")
			util.execute_request(**adjustEvent)
	
			print '\n\t\t\t\tAPSALAR EVENT'	 
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='Level-Up')
			util.execute_request(**apsalar_event)


			if random.randint(1,100)<=90 and app_data.get('crew')== False:
				time.sleep(random.randint(60,100))
				print "\n\n-----------------------ADJUST Event-------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="facvu3",event_name="CrewJoin")
				util.execute_request(**adjustEvent)
				app_data['crew']=True
	
				time.sleep(random.randint(60,100))
				print '\n\t\t\t\tAPSALAR EVENT'	 
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='Crew Join')
				util.execute_request(**apsalar_event)
						
			if app_data.get('level')==2:
				time.sleep(random.randint(60,100))
				print "\n\n-----------------------ADJUST Event-------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="1kny0d",event_name="FTUEComplete")
				util.execute_request(**adjustEvent)

				time.sleep(random.randint(100,150))
				print '\n\t\t\t\tAPSALAR EVENT'	 
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='FTUE Complete')
				util.execute_request(**apsalar_event)

			if app_data.get('smp5')==False and random.randint(1,100)<=20+rate:
				time.sleep(random.randint(600,700))
				print "\n\n-----------------------ADJUST Event-------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="l804db",event_name="SMPRace5")
				util.execute_request(**adjustEvent)
				app_data['smp5']=True
				rate=0

				print '\n\t\t\t\tAPSALAR EVENT'	 
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='SMP Race 5')
				util.execute_request(**apsalar_event)


			if app_data.get('smp5')==True and app_data.get('smp10')==False and random.randint(1,100)<=rate:
				time.sleep(random.randint(60,100))
				print "\n\n-----------------------ADJUST Event-------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,event_token="p8w88i",event_name="SMPRace10")
				util.execute_request(**adjustEvent)
				app_data['smp10'] = True

				print '\n\t\t\t\tAPSALAR EVENT'	 
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,n='SMP Race 10')
				util.execute_request(**apsalar_event)
			time.sleep(random.randint(5,15))
			rate+=10
			app_data['level']+=1		
				



###########################################################
#														  #
#						ADJUST							  #
#														  #
###########################################################
def adjust_session(campaign_data, app_data, device_data,event='install'):
	make_subsession_count(app_data,device_data)
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent' : app_data.get('ua'),
		'Accept-Encoding' : 'gzip'
		}
	params=None
	app_data['session_count'] += 1

	data = {
		'needs_response_details':'1',
		'api_level':device_data.get('sdk'),
		'event_buffering_enabled':'0',
		'hardware_name':device_data.get('build'),
		'cpu_type':device_data.get('cpu_abi'),
		'screen_format': get_screen_format(device_data),
		'device_manufacturer':device_data.get('brand').upper(),
		'session_count':str(app_data.get('session_count')),
		'device_type':device_data.get('device_type'),
		'screen_size': 'normal',
		'connectivity_type':1,
		'installed_at':datetime.datetime.fromtimestamp((app_data.get('times').get('install_complete_time'))).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		'network_type':13,
		'mcc' : device_data.get('mcc'),
		'mnc' : device_data.get('mnc'),
		'os_build': device_data.get('build'),
		'updated_at': datetime.datetime.fromtimestamp((app_data.get('times').get('install_complete_time'))).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		# 'vm_isa':'arm',
		'package_name':campaign_data.get('package_name'),
		'app_version':campaign_data.get('app_version_name'),
		'android_uuid':app_data.get('android_uuid'),
		'display_width':device_data.get('resolution').split('x')[0],
		'country':device_data.get('locale').get('country'),
		'os_version':device_data.get('os_version'),
		'environment':'production',
		'device_name':device_data.get('model'),
		'os_name': 'android',
		'tracking_enabled': '1',
		'created_at': get_date(app_data,device_data,early=random.uniform(0.6,0.7)),
		'app_token':campaign_data.get('adjust').get('app_token'),
		'screen_density': get_screen_density(device_data),
		'language':device_data.get('locale').get('language'),
		'display_height':device_data.get('resolution').split('x')[1],
		'gps_adid':device_data.get('adid'),
		'gps_adid_src' : 'service',
		'sent_at': get_date(app_data,device_data),
		'attribution_deeplink': '1',
		'callback_params':json.dumps({"ZID":app_data.get('ZID')}),#json.dumps({"game_id":app_data.get('gameID'),"ZID":app_data.get('ZID'),"android_id":device_data.get('android_id')})
		}

	if event=='open':
		timeSpent = app_data.get('app_close_time')-app_data.get('adjust_call_time')
		app_data['timeSpent'] = timeSpent
		data['time_spent']=timeSpent
		data['session_length']=timeSpent+random.randint(1,5)
		data['subsession_count']=app_data.get('subsession_count')
		data['last_interval']= int(time.time()-app_data.get('app_close_time'))

	headers['Authorization']= get_auth(app_data,activity_kind='session',gps_adid=str(data['gps_adid']),created_at=data['created_at'])

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_sdkClick(campaign_data, app_data, device_data,source='reftag'):
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent' : app_data.get('ua'),
		'Accept-Encoding' : 'gzip'
		}
	temp_b = urlparse.parse_qs(app_data.get('referrer'))
	if temp_b.get('adjust_reftag'):
		reftag = temp_b.get('adjust_reftag')[0]
	else:
		reftag=""

	timeSpent = int(time.time())-app_data.get('adjust_call_time')	
	app_data['timeSpent'] = timeSpent
		
	data = {
		'needs_response_details':'1',
		'event_buffering_enabled':'0',
		'referrer':app_data.get('referrer') or '',
		'created_at':get_date(app_data,device_data,early=random.uniform(2.5,3)),
		'gps_adid':device_data.get('adid'),
		'gps_adid_src' : 'service',
		'click_time':get_date(app_data,device_data,early=random.uniform(4,4.5)),
		'environment':'production',
		# 'source':'reftag',
		'tracking_enabled':'1',
		'app_token':campaign_data.get('adjust').get('app_token'),
		'sent_at':get_date(app_data,device_data),
		'reftag':reftag,
		'callback_params':json.dumps({"ZID":app_data.get('ZID')}),
		'attribution_deeplink': '1',


		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		# 'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		# 'attribution_deeplink'	:'1',
		# 'click_time'			:click_time,
		'connectivity_type'		:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		# 'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('brand'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[1],
		'display_width'			:device_data.get('resolution').split('x')[0],
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:datetime.datetime.fromtimestamp((app_data.get('times').get('install_complete_time'))).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		# 'install_begin_time'	:install_begin_time,
		'language'				:device_data.get('locale').get('language'),
		# 'needs_response_details':'1',
		'mcc'					: device_data.get('mcc'),
		'mnc'					: device_data.get('mnc'),
		'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		# 'referrer'				:urllib.unquote(app_data.get('referrer')),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'large',
		# 'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'session_length'		:timeSpent,
		'source'				:source,
		'subsession_count'		:app_data.get('subsession_count'),
		'time_spent'			:timeSpent,
		# 'tracking_enabled'		:'1',
		'updated_at'			:datetime.datetime.fromtimestamp((app_data.get('times').get('install_complete_time'))).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		# 'vm_isa'				: 'arm',
		'raw_referrer'			: app_data.get('referrer'),
		# 'referrer'				: urllib.unquote(app_data.get('referrer')),
		'last_interval'			: 0,

		}

	if source=='install_referrer':
		data['click_time']=datetime.datetime.fromtimestamp((app_data.get('times').get('click_time'))).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
		data['install_begin_time']=datetime.datetime.fromtimestamp((app_data.get('times').get('download_end_time'))).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
		del data['raw_referrer']
		del data['reftag']
		# data['raw_referrer']=app_data.get('referrer')
		# data['referrer']=urllib.unquote(app_data.get('referrer'))
		# data['reftag']=reftag
		# data['last_interval']=0
		# data['click_time']= app_data.get('click_time')

	adjustSDKclick = {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}
	headers['Authorization']= get_auth(app_data,activity_kind='click',gps_adid=str(data['gps_adid']),created_at=data['created_at'])

	util.execute_request(**adjustSDKclick)

def adjust_attribution(campaign_data, app_data, device_data):
	url = 'http://app.adjust.com/attribution'
	method='head'
	headers = {
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent' : app_data.get('ua'),
		'Accept-Encoding' : 'gzip'
		}
	params = {
		'created_at': get_date(app_data,device_data,early=random.uniform(2.5,3)),
		'event_buffering_enabled':'0',
		'needs_response_details':'1',
		'app_token':campaign_data.get('adjust').get('app_token'),
		'environment':'production',
		'tracking_enabled':'1',
		'gps_adid':device_data.get('adid'),
		'gps_adid_src' : 'service',
		'sent_at':get_date(app_data,device_data),
		'attribution_deeplink': '1',
		'android_uuid' :app_data.get('android_uuid'),
		'api_level'	:device_data.get('sdk'),
		'app_version' :campaign_data.get('app_version_name'),
		'device_name' :device_data.get('model'),
		'device_type' :device_data.get('device_type'),
		'initiated_by' : 'backend',
		'os_name' :'android',
		'os_version' :device_data.get('os_version'),
		'package_name' :campaign_data.get('package_name'),
		}
	headers['Authorization']= get_auth(app_data,activity_kind='attribution',gps_adid=str(params['gps_adid']),created_at=params['created_at'])

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

def adjust_event(campaign_data, app_data, device_data,event_token,event_name):
	make_subsession_count(app_data,device_data)
	url = 'http://app.adjust.com/event'
	method = 'post'
	headers = {
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent' : app_data.get('ua'),
		'Accept-Encoding' : 'gzip'
		}
	params=None

	timeSpent = int(time.time())-app_data.get('adjust_call_time')	
	app_data['timeSpent'] = timeSpent

	app_data['event_count'] += 1
	data = {
		'needs_response_details':'1',
		'api_level':device_data.get('sdk'),
		'event_buffering_enabled':'0',
		'hardware_name':device_data.get('build'),
		'cpu_type':device_data.get('cpu_abi'),
		'queue_size':'0',
		'screen_format': get_screen_format(device_data),
		'device_manufacturer':device_data.get('brand').upper(),
		'session_count':str(app_data.get('session_count')),
		'device_type':device_data.get('device_type'),
		'screen_size': 'normal',
		'package_name':campaign_data.get('package_name'),
		'app_version':campaign_data.get('app_version_name'),
		'android_uuid':app_data.get('android_uuid'),
		'display_width':device_data.get('resolution').split('x')[0],
		'country':device_data.get('locale').get('country'),
		'os_version':device_data.get('os_version'),
		'environment':'production',
		'device_name':device_data.get('model'),
		'os_name': 'android',
		'tracking_enabled': '1',
		'created_at': get_date(app_data,device_data,early=random.uniform(0.6,0.7)),
		'app_token':campaign_data.get('adjust').get('app_token'),
		'screen_density': get_screen_density(device_data),
		'language':device_data.get('locale').get('language'),
		'display_height':device_data.get('resolution').split('x')[1],
		'gps_adid':device_data.get('adid'),
		'gps_adid_src' : 'service',
		'sent_at': get_date(app_data,device_data),
		'time_spent':timeSpent,
		'subsession_count':app_data.get('subsession_count'),
		'session_length': timeSpent+random.randint(1,5),
		'event_count':app_data.get('event_count'),
		'event_token':event_token,
		'attribution_deeplink': '1',
		'connectivity_type':1,
		# 'installed_at': app_data.get('installed_at'),
		'network_type':13,
		'mcc' : device_data.get('mcc'),
		'mnc' : device_data.get('mnc'),
		'os_build': device_data.get('build'),
		# 'updated_at': app_data.get('installed_at'),
		# 'vm_isa':'arm',
		}
	if event_name == 'AppOpen':
		data['queue_size'] = '1'
		data['callback_params']=json.dumps({
				"event_name":event_name,
				# "game_id":app_data.get('gameID'),
				"ZID":app_data.get('ZID'),
				# "android_id":device_data.get('android_id'),
				"UASDKVersion":"2.7.1"#"Unity-2.4.0"
				})

	if event_name =='adjust_device_tracking':
		data['callback_params']=json.dumps({
				"event_name":event_name,
				# "game_id":app_data.get('gameID'),
				"ZID":app_data.get('ZID'),
				# "android_id":device_data.get('android_id'),
				# "device_id":"DeviceAdvertisingID",
				# "device_type":device_data.get('manufacturer').upper()+' '+device_data.get('model'),
				# "os":"Android",
				# "os_version":device_data.get('os_version'),
				# "app_version":campaign_data.get('app_version_name'),
				"client_device_ts":str(int(time.time()))+'.'+str(random.randint(50000,60000))
				})


	if event_name == 'LeveledUp':
		data['callback_params']=json.dumps({
				"event_name":event_name,
				# "game_id":app_data.get('gameID'),
				"ZID":app_data.get('ZID'),
				# "android_id":device_data.get('android_id'),
				"newLevel":str(app_data.get('level'))
				})


	if event_name == 'FTUEComplete' or event_name == 'SMPRace5' or event_name == 'CrewJoin' or event_name == 'SMPRace10':
		data['callback_params']=json.dumps({
				"event_name":event_name,
				# "game_id":app_data.get('gameID'),
				"ZID":app_data.get('ZID'),
				# "android_id":device_data.get('android_id'),
				})


	if event_name == 'ZIDRegistration':
		data['callback_params']=json.dumps({
				"event_name":event_name,
				# "game_id":app_data.get('gameID'),
				"ZID":app_data.get('ZID'),
				# "android_id":device_data.get('android_id'),
				"users_zid" : app_data.get('ZID'),
				})

	if event_name == "Purchase":
		r=random.randint(1,100)
		if r<=85:
			revenue=campaign_data.get('purchase_var').get('1')
		elif r>85 and r<95:
			revenue=campaign_data.get('purchase_var').get('2')
		else:
			revenue=campaign_data.get('purchase_var').get('3')

		transaction_id="GPA.3312-"+util.get_random_string('decimal',4)+"-"+util.get_random_string('decimal',4)+"-"+util.get_random_string('decimal',5)
		data['currency']="USD"
		data['revenue']= revenue
		data['callback_params']=json.dumps({
				"event_name":event_name,
				"game_id":app_data.get('gameID'),
				"receipt_signature":util.get_random_string('google_token',347)+"==",
				"transaction_id":transaction_id,
				"ZID":app_data.get('ZID'),
				"receipt_data":json.dumps({'orderId':transaction_id,"packageName":campaign_data.get('package_name'),"productId":"com.naturalmotion.customstreetracer2.2016.starterpack12","purchaseTime":int(time.time()*1000),"purchaseState":0,"developerPayload":"64a54be2337721dd87b8a00ca38619b8f0f5e3c0","purchaseToken":"hknadeaenjjijlnkohdbdjpj.AO-J1OwNrpTQnL74O9fnX8M45QAmeCWifo-EWJIlxk6QfZLKKH8ZIfpc6FQppN0Cy7QFKJw3QzVwIuq0l4uffrYgWcZoEArSDocorsCpb0WsRlewyFkRiCJLnRh2PDKSez14L1jlsqoYHRjkKXWGDfVv2zfgoaEvJg0aeOyoZdu9VsSdam0DSb8iAoNaZmVA3XOzQcB6oFX0"}),
				"android_id":device_data.get('android_id'),
				})
	headers['Authorization']= get_auth(app_data,activity_kind='event',gps_adid=str(data['gps_adid']),created_at=data['created_at'])

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':params, 'data': data}


def apsalar_login(campaign_data, app_data, device_data):
	url = 'http://login.dragonsoulgame.com/login'
	method = 'get'
	headers = {
		'User-Agent' : 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',	
		'Accept-Encoding':'gzip',
		'Content-Type': 'application/x-www-form-urlencoded'
		}

	params = {
		'advertisingIdentifier': device_data.get('adid'),
		'aPMacAddress':	device_data.get('mac'),
		'buildSource':	'GOOGLE',
		'country': device_data.get('locale').get('country'),
		'email': app_data.get('user_info').get('email') if app_data.get('user_info').get('email') else return_userinfo(app_data),
		'imei': device_data.get('imei'),
		'language':	device_data.get('locale').get('language'),
		'platform':	'ANDROID',
		'shardID': '0',
		'uniqueIdentifier': device_data.get('android_id'),
		'userID': '0',
		'version': '68',
		}

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }		
		
def apsalar_canonical(campaign_data,app_data,device_data):
	method = 'get'
	url = 'http://e.apsalar.com/api/v1/canonical'

	headers = {
					'User-Agent' : 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
					'Accept-Encoding':'gzip'
	}
	
	params = {
					'a':campaign_data.get('csr').get('api_key'),
					'br':device_data.get('brand'),
					'c':'wifi',
					'de':device_data.get('device'),
					'ma':device_data.get('manufacturer'),
					'mo':device_data.get('model'),
					'n':campaign_data.get('app_name'),
					'p':'Android',
					'pr':device_data.get('product'),
					'sdk':campaign_data.get('csr').get('sdk_dr'),
					'v':device_data.get('os_version'),
					'sup':'[ "AIFA", "AIFA" ]',
					'ps':'true',
	}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

def apsalar_resolve(campaign_data,app_data,device_data):
	method = 'get'
	url = 'http://e-ssl.apsalar.com/api/v1/resolve'

	headers = {
					'User-Agent' : 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',	
					'Accept-Encoding':'gzip'
	}
	
	params = {
					'a':campaign_data.get('csr').get('api_key'),
					'i': campaign_data.get('package_name'),
					'k':'AIFA',
					'u':device_data.get('adid'),
					'p':'Android',
					'pi':	'1',
					'v':device_data.get('os_version'),
					'rt':'json',
	}	

	app_data['value'] = str('?a='+campaign_data.get('csr').get('api_key')+'&i='+campaign_data.get('package_name')+'&k=AIFA&u='+device_data.get('adid')+'&p=Android&pi=1&v='+device_data.get('os_version')+'&rt=json')

	params['h'] = calculate_h(campaign_data,app_data,device_data)
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

def apsalarStart(campaign_data, app_data, device_data):
	url = 'http://e-ssl.apsalar.com/api/v1/start'
	method = 'get'
	headers = {
					'User-Agent' : 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',	
					'Accept-Encoding':'gzip'
	}
	
	s_s1 = app_data.get('s') if app_data.get('s') else make_s(app_data)
	
	params ='a='+campaign_data.get('csr').get('api_key')+'&ab='+device_data.get('cpu_abi')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&br='+device_data.get('brand')+'&c=wifi&de='+device_data.get('device')+'&dnt=0&i='+campaign_data.get('package_name')+'&k=AIFA&lag=1.796&ma='+device_data.get('manufacturer')+'&mo='+device_data.get('model')+'&n='+campaign_data.get('app_name')+'&p=Android'+'&pr='+device_data.get('product')+'&rt=json&s='+s_s1+'&sdk='+campaign_data.get('csr').get('sdk_dr')+'&src=com.android.vending&u='+device_data.get('adid')+'&v='+device_data.get('os_version')

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('csr').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarEvent(campaign_data, app_data, device_data,n=None,e=None):
	url = 'http://e-ssl.apsalar.com/api/v1/event'
	method = 'get'
	headers = {
		'User-Agent' : 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',	
		'Accept-Encoding':'gzip'
		}
	lag=str(round(random.uniform(0.01,0.05),3))
	print str(round(random.uniform(8,9),3))
	print str(round(random.uniform(310,320),3)) 

	print str(round(random.uniform(310,320),9)) 
	print str(round(random.uniform(310,320),5)) 
	time.sleep(5)
		
	if n=='login':
		t= str(round(random.uniform(8,9),3)) 
		
	elif n=='DEFEAT_1_1':
		t=	str(round(random.uniform(310,320),3)) 	
	
	elif n=='TutorialFinished':
		t=	str(round(random.uniform(320,330),3)) 
		
	else:
		t=	str(round(random.uniform(300,310),3)) 
		
		
	params = {
		'a':campaign_data.get('csr').get('api_key'),
		'av':campaign_data.get('app_version_name'),
		'e':json.dumps(e) if e else "",
		'i':campaign_data.get('package_name'),
		'n':n,
		'p':'Android',
		'rt':'json',
		's':app_data.get('s') if app_data.get('s') else make_s(app_data),
		'sdk':campaign_data.get('csr').get('sdk_dr'),
		't':t,
		'u':device_data.get('adid'),
		'seq':'1',
		'k':'AIFA',
		'lag':lag,
		}
		
		
		
	s_s1 = app_data.get('s') if app_data.get('s') else make_s(app_data)
		
	arr = 'a='+campaign_data.get('csr').get('api_key')+'&av='+campaign_data.get('app_version_name')+'&e='+str(params.get('e'))+'&i='+campaign_data.get('package_name')+'&n='+n+'&p=Android'+'&rt=json&s='+s_s1+'&sdk='+campaign_data.get('csr').get('sdk_dr')+'&seq=1&t='+t+'&u='+device_data.get('adid')+'&k=AIFA&lag='+lag
	h = getHash('?'+arr,campaign_data.get('csr').get('secret'))
	params['h'] = h
	
	# if n=='login':
	# 	params['e'] = '{"user_id":'+str(app_data.get('user_id')) if app_data.get('user_id') else str(make_user_id(app_data))+'}'
	
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }
	
def apsalarEvent_referrer(campaign_data, app_data, device_data):
	
	if app_data.get('referrer'):
		url = 'http://e.apsalar.com/api/v1/event'
		method = 'get'
		headers = {
			'User-Agent' : 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',	
			'Accept-Encoding':'gzip'
			}

		params = {
			'a':campaign_data.get('csr').get('api_key'),
			'av':campaign_data.get('app_version_name'),
			'e':app_data.get('referrer'),
			'i':campaign_data.get('package_name'),
			'n':'__InstallReferrer',
			'p':'Android',
			'rt':'json',
			's':app_data.get('s') if app_data.get('s') else make_s(app_data),
			'sdk':campaign_data.get('csr').get('sdk_dr'),
			't':str(round(random.uniform(4,5),3)),
			'u':device_data.get('adid'),
			'k':'AIFA',
			'lag':str(round(random.uniform(0.01,0.05),3)),
			}
			
		s_s1 = app_data.get('s') if app_data.get('s') else make_s(app_data)

		arr = 'a='+campaign_data.get('csr').get('api_key')+'&av='+campaign_data.get('app_version_name')+'&e='+app_data.get('referrer')+'&i='+campaign_data.get('package_name')+'&n=__InstallReferre&p=Android'+'&rt=json&s='+s_s1+'&sdk='+campaign_data.get('csr').get('sdk_dr')+'&t='+params.get('t')+'&u='+device_data.get('adid')+'&k=AIFA&lag='+params.get('lag')
		h = getHash('?'+arr,campaign_data.get('csr').get('secret'))
		params['h'] = h
		
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }	

def registration_selfcall(app_data,campaign_data,device_data):
	url='https://api.crittercism.com/android_v2/update_user_metadata'
	method='post'
	params=None

	app_id=util.get_random_string('hex',40)
	device_id=str(uuid.uuid4())

	data={"app_id":app_id,
	"hashed_device_id":device_id,
	"library_version":"5.8.2",
	"metadata":{"username":app_data.get('ZID')}}
	headers={
	'Accept-Encoding':'gzip',
	'User-Agent': 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
	'Content-type':'application/json',
	'CRAppId': app_id,
	'CRPlatform': 'android',
	'CRVersion': '5.8.2',

	}

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data) }


###########################################################
#														  #
#						UTIL							  #
#														  #
###########################################################
def make_subsession_count(app_data,device_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 0
	else:
		app_data['subsession_count'] += 1
		
	return app_data.get('subsession_count')

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid = device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'

def get_date(app_data,device_data,early=0):
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date


def calculate_h(campaign_data,app_data,device_data):

	s3 = "AGk0IH84"
	s3_bytes = array.array('B',s3)
	s2 = app_data.get('value') if app_data.get('value') else make_value(campaign_data,app_data,device_data)
	s2_bytes = array.array('B',s2)
	sha_obj = hashlib.sha1()
	sha_obj.update(s3_bytes)
	sha_obj.update(s2_bytes)
	return sha_obj.hexdigest()

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def make_s(app_data):
	app_data['s'] = str(uuid.uuid4())
	return app_data.get('s')

def make_value(campaign_data,app_data,device_data):
	if not app_data.get('value'):
		app_data['value']=str('?a='+campaign_data.get('csr').get('api_key')+'&i='+campaign_data.get('package_name')+'&k=AIFA&u='+device_data.get('adid')+'&p=Android&pi=1&v='+device_data.get('os_version')+'&rt=json')
	return app_data.get('value')

def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	if not gender_n:
		app_data['gender'] = random.choice(['male', 'female'])
		gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	if not user_info:
		user_data = util.generate_name(gender=gender_n)
		user_info = {}
		user_info['username'] = user_data.get('username').lower()
		user_info['email'] = user_data.get('username').lower() + '@gmail.com'
		user_info['f_name'] = user_data.get('firstname')
		user_info['l_name'] = user_data.get('lastname')
		user_info['sex'] = gender_n
		user_info['gender'] = str(1) if gender_n == 'female' else str(2)
		user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
		user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
		user_info['password'] = util.get_random_string(type='all',size=16)
		app_data['user_info'] = user_info

def get_auth(app_data,activity_kind,gps_adid,created_at):
	final_str=str(created_at)+str(campaign_data.get('adjust').get('secret_key'))+str(gps_adid)+str(activity_kind)
	sign=util.sha256(final_str)
	auth='Signature secret_id="'+str(campaign_data.get('adjust').get('secret_id'))+'",signature="'+str(sign)+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind "'
	return auth
	