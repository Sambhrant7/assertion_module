# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
import time
import random
import json
import datetime
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'irlapp.com.IRL',
	'app_size'			 : 51.8,#53.5,
	'app_name' 			 : 'IRL',
	'app_version_name' 	 : '2.1.0',#'2.0.14',#'2.0.13',
	'app_version_code' 	 : '3954',#'3589',#'2.0.13.1',
	'app_id' 			 : '1332596741',
	'ctr' 				 : 1,
	'sdk' 		 		 : 'ios',
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '11.0',
	'tracker'		 	 : 'branch',
	'branch':{
								'key':'key_live_mjQmBvmUKbCFoVeqYCepripaCDlr8loZ',
								'sdk': 'ios0.26.0',
							},	
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')
	
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')

	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	################generating_realtime_differences##################

 	###########	 CALLS		 ############
	print "Branch Install"
	branchInstall = branch_install(campaign_data, app_data, device_data)
	app_data['api_hit_time']=time.time()
	branchInstallRequest = util.execute_request(**branchInstall)
	try:
		response = json.loads(branchInstallRequest.get('data'))
		app_data['device_fingerprint_id1'] = response.get('device_fingerprint_id')
		app_data['identity_id'] = response.get('identity_id')
		app_data['session_id'] = response.get('session_id')
	except:
		print 'could not fetch data from branch response'
		app_data['device_fingerprint_id1']='680031755927107292'
		app_data['identity_id']='680031755968950241'
		app_data['session_id']='680031755973486670'

	print "Branch Sdk"
	request=branch_uriski(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(80,100))
	print "Branch Close"
	request=branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(8,10))
	print "Branch Open"
	req = branch_open(campaign_data, app_data, device_data)
	util.execute_request(**req)

	for _ in range(random.randint(1,3)):

		time.sleep(random.randint(80,100))
		print "Branch Close"
		request=branch_close(campaign_data, app_data, device_data,call_seq=2)
		util.execute_request(**request)

		time.sleep(random.randint(8,10))
		print "Branch Open"
		req = branch_open(campaign_data, app_data, device_data,call_seq=2)
		util.execute_request(**req)

	time.sleep(random.randint(80,100))
	print "Branch Close"
	request=branch_close(campaign_data, app_data, device_data,call_seq=3)
	util.execute_request(**request)

	return {'status':'true'}

#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')
	
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')

	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')


 	###########	 CALLS		 ############

	if not app_data.get('device_fingerprint_id1') or not app_data.get('identity_id') or not app_data.get('session_id'):
		print "Branch Install"
		branchInstall = branch_install(campaign_data, app_data, device_data)
		branchInstallRequest = util.execute_request(**branchInstall)
		try:
			response = json.loads(branchInstallRequest.get('data'))
			app_data['device_fingerprint_id1'] = response.get('device_fingerprint_id')
			app_data['identity_id'] = response.get('identity_id')
			app_data['session_id'] = response.get('session_id')
		except:
			print 'could not fetch data from branch response'
			app_data['device_fingerprint_id1']='680031755927107292'
			app_data['identity_id']='680031755968950241'
			app_data['session_id']='680031755973486670'
	else:
		print "Branch Open"
		req = branch_open(campaign_data, app_data, device_data)
		util.execute_request(**req)		


	for _ in range(random.randint(1,3)):

		time.sleep(random.randint(80,100))
		print "Branch Close"
		request=branch_close(campaign_data, app_data, device_data,call_seq=2)
		util.execute_request(**request)

		time.sleep(random.randint(8,10))
		print "Branch Open"
		req = branch_open(campaign_data, app_data, device_data,call_seq=2)
		util.execute_request(**req)

	time.sleep(random.randint(80,100))
	print "Branch Close"
	request=branch_close(campaign_data, app_data, device_data,call_seq=3)
	util.execute_request(**request)

	return {'status':'true'}


###################################
#		Branch_install()
###################################

def branch_install(campaign_data, app_data, device_data):
	method = 'post'
	url='http://api2.branch.io/v1/install'
	header={
			'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
			'Content-Type': 'application/json',
			"Accept-Encoding": "br, gzip, deflate",
			"Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),

			}
	params={}
	data={
			"sdk":campaign_data.get('branch').get('sdk'),
			"facebook_app_link_checked":False,
			"language":device_data.get('locale').get('language'),
			"user_agent":device_data.get('User-Agent'),
			"screen_height":int(device_data.get('resolution').split('x')[1]),
			"country":device_data.get('locale').get('country'),
			"app_version":campaign_data.get('app_version_name'),
			"update":0,
			"ad_tracking_enabled":False,
			"brand":"Apple",
			"retryNumber":0,
			"os":"iOS",
			"hardware_id":app_data.get('idfv_id'),
			"ios_vendor_id":app_data.get('idfa_id'),
			"ios_bundle_id":campaign_data.get('package_name'),
			"screen_width":int(device_data.get('resolution').split('x')[0]),
			"hardware_id_type":"idfa",
			"debug":False,
			"model":device_data.get('device_platform'),
			"os_version":device_data.get('os_version'),
			"is_hardware_id_real":True,
			"branch_key":campaign_data.get('branch').get('key'),
			"apple_ad_attribution_checked":False,
			"first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
			"latest_install_time":int(app_data.get('times').get('install_complete_time')*1000),
			"local_ip": device_data.get('private_ip'),
			"lastest_update_time":int(app_data.get('times').get('install_complete_time')*1000),

			}
	return {'url':url, 'httpmethod':method, 'headers':header, 'params': None, 'data': json.dumps(data)}


###################################
#		Branch_uriski()
###################################

def branch_uriski(campaign_data, app_data, device_data):
	url= "http://cdn.branch.io/sdk/uriskiplist_v0.json"
	method= "get"
	headers= {       
        "Accept-Encoding": "br, gzip, deflate",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "User-Agent": campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
        }

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def branch_close(campaign_data, app_data, device_data,call_seq=1):
	method = 'post'
	url='http://api2.branch.io/v1/close'
	header={
			'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
			'Content-Type': 'application/json',
			"Accept-Encoding": "br, gzip, deflate",
			"Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
			}
	params={}
	data={
			"session_id":app_data.get('session_id'),
			"language":device_data.get('locale').get('language'),
			"user_agent":device_data.get('User-Agent'),
			"country":device_data.get('locale').get('country'),
			"screen_height":int(device_data.get('resolution').split('x')[1]),
			"ad_tracking_enabled":False,
			"brand":"Apple",
			"retryNumber":0,
			"identity_id":app_data.get('identity_id'),
			"os":"iOS",
			"hardware_id":app_data.get('idfv_id'),
			"ios_vendor_id":app_data.get('idfa_id'),
			"screen_width":int(device_data.get('resolution').split('x')[0]),
			"hardware_id_type":"idfa",
			"instrumentation":{"/v1/install-brtt":"500"},
			"os_version":device_data.get('os_version'),
			"is_hardware_id_real":True,
			"model":device_data.get('device_platform'),
			"branch_key":campaign_data.get('branch').get('key'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"sdk":campaign_data.get('branch').get('sdk'),
			"local_ip":device_data.get('private_ip'),
			
			}

	if call_seq==2:
		data['instrumentation']={"/v1/open-brtt":"432"}

	if call_seq==3:
		data['instrumentation']={"/v1/open-brtt":"418"}		

	return {'url':url, 'httpmethod':method, 'headers':header, 'params': None, 'data': json.dumps(data)}	


###################################
#		Branch_open()
###################################

def branch_open(campaign_data, app_data, device_data,call_seq=1):
	method = 'post'
	url='http://api2.branch.io/v1/open'
	header={
			'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
			'Content-Type': 'application/json',
			"Accept-Encoding": "br, gzip, deflate",
			"Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),

			}
	params={}
	data={
			"sdk":campaign_data.get('branch').get('sdk'),
			"facebook_app_link_checked":False,
			"language":device_data.get('locale').get('language'),
			"user_agent":device_data.get('User-Agent'),
			"screen_height":int(device_data.get('resolution').split('x')[1]),
			"country":device_data.get('locale').get('country'),
			"app_version":campaign_data.get('app_version_name'),
			"update":1,
			"ad_tracking_enabled":False,
			"brand":"Apple",
			"retryNumber":0,
			"os":"iOS",
			"hardware_id":app_data.get('idfv_id'),
			"ios_vendor_id":app_data.get('idfa_id'),
			"ios_bundle_id":campaign_data.get('package_name'),
			"screen_width":int(device_data.get('resolution').split('x')[0]),
			"hardware_id_type":"idfa",
			"debug":False,
			"model":device_data.get('device_platform'),
			"os_version":device_data.get('os_version'),
			"is_hardware_id_real":True,
			"branch_key":campaign_data.get('branch').get('key'),
			"apple_ad_attribution_checked":False,
			"first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
			"latest_install_time":int(app_data.get('times').get('install_complete_time')*1000),
			"local_ip": device_data.get('private_ip'),
			"lastest_update_time":int(app_data.get('times').get('install_complete_time')*1000),
			"cd":{"pn":campaign_data.get('package_name'),"mv":"-1"},
			"device_fingerprint_id" : app_data.get('device_fingerprint_id1'),
			"identity_id" : app_data.get('identity_id'),
			"instrumentation" : {"/v1/close-brtt":"375"},
			"previous_update_time" : int(app_data.get('times').get('install_complete_time')*1000)
			}

	if call_seq==2:
		data['instrumentation']={"/v1/close-brtt":"569"}

	return {'url':url, 'httpmethod':method, 'headers':header, 'params': None, 'data': json.dumps(data)}
	
# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"