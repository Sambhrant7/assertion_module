# -*- coding: utf-8 -*-
from sdk import util
from sdk import NameLists
from sdk import installtimenew
from sdk import getsleep
import time
import random
import json
import urlparse
import string
import datetime
import uuid
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
			'package_name':'com.aniplex.magireco',
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'ctr':6,
			'app_name' : 'Magia Records',
			'app_id':'1164785360',
			'no_referrer': True,
			'device_targeting' : True,
			'app_version_name': '1.7.8',#'1.5.8',
			'app_size' : 62.3,
			'supported_os':'7.0',
			'supported_countries': 'WW',
			'tracker' : 'adforce',
			'adforce':{
				'app_id'     :'6140',
				'sdk_version':'4.5.0',
				'sdk'        :'4.5.0'
	             },
			'retention':{
							1:50,
							2:48,
							3:46,
							4:44,
							5:40,
							6:38,
							7:35,
							8:32,
							9:31,
							10:30,
							11:29,
							12:28,
							13:27,
							14:26,
							15:25,
							16:24,
							17:23,
							18:22,
							19:21,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
						}
		}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	

	###########		INITIALIZE		############	

	print 'plz wait installing ........'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')

	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()	

	a=str({"Version3.1":{"iad-attribution":"false"}})
	app_data['searchAds']=util.base64(a)
	app_data['install_id'] = str(uuid.uuid4()).upper()
		
	###########		CALLS		################

	print '----------------------------- adforce Analytics -----------------------------'	
	request = adforce_analytics(campaign_data, app_data, device_data,isFirstCall=True)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	print '----------------------------- Ad-Force tmck -----------------------------'	
	request = adforce_tmck(campaign_data, app_data, device_data)
	result = util.execute_request(**request)
	try:
		app_data['uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]
	except:
		app_data['uid']='00bb2cf133ad737d9b8dcf15238d52e0c9'

	print '----------------------------- Ad-Force collect -----------------------------'	
	request = adforce_collect(campaign_data, app_data, device_data)
	util.execute_request(**request)	

	print '----------------------------- adforce fprc -----------------------------'	
	request = adforce_fprc(campaign_data, app_data, device_data)
	util.execute_request(**request)
	
	print '----------------------------- adforce fp -----------------------------'	
	request = adforce_fp(campaign_data, app_data, device_data)
	result = util.execute_request(**request)
	try:
		uid=json.loads(result.get('data'))
 		app_data['fp_tdl'] = uid.get('tdl')
 		app_data['fp_id'] = uid.get('id')
	except:
		app_data['fp_id'] = '6.8.2:1A9FC57676FC6AC358A573E83061A04CEAC18C9B|tid:tGC9IZ1gMtVA' 
		app_data['fp_tdl'] = '19798436' 

	print '----------------------------- adforce_cv-----------------------------'	
	request = adforce_cv(campaign_data, app_data, device_data)
	result = util.execute_request(**request)
	try:
		app_data['uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]
	except:
		app_data['uid']='00bb2cf133ad737d9b8dcf15238d52e0c9'


	if random.randint(1,100) <= 50:

		time.sleep(random.randint(20,60))
		print '----------------------------- adforce Analytics -----------------------------'	
		request = adforce_analytics(campaign_data, app_data, device_data)
		util.execute_request(**request)

	
	###########		FINALIZE	################
		
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	

	###########		INITIALIZE		############

	print 'plz wait  ........'
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')

	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		if not app_data.get('idfv_id'):
			app_data['idfv_id'] = str(uuid.uuid4()).upper()	

	a=str({"Version3.1":{"iad-attribution":"false"}})

	if not app_data.get('searchAds'):
		app_data['searchAds']=util.base64(a)

	if not app_data.get('install_id'):
		app_data['install_id'] = str(uuid.uuid4()).upper()
		
	###########		CALLS		################

	if not app_data.get('uid'):

		print '----------------------------- Ad-Force tmck -----------------------------'	
		request = adforce_tmck(campaign_data, app_data, device_data)
		result = util.execute_request(**request)
		try:
			app_data['uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]
		except:
			app_data['uid']='00bb2cf133ad737d9b8dcf15238d52e0c9'


	print '----------------------------- adforce Analytics -----------------------------'	
	request = adforce_analytics(campaign_data, app_data, device_data)
	util.execute_request(**request)
	

	if not app_data.get('chapter_1') and random.randint(1,100) <= 75:

		# chapter one completed
		
		time.sleep(random.randint(12*60,15*60))

		print '----------------------------- adforce Analytics -----------------------------'	
		request = adforce_analytics(campaign_data, app_data, device_data)
		util.execute_request(**request)



		if not app_data.get('fp_id') or not app_data.get('fp_tdl'):

			print '----------------------------- adforce fp -----------------------------'	
			request = adforce_fp(campaign_data, app_data, device_data)
			result = util.execute_request(**request)
			try:
				uid=json.loads(result.get('data'))
				app_data['fp_id'] = uid.get('id') 
				app_data['fp_tdl'] = uid.get('tdl') 
			except:
				app_data['fp_id'] = '6.8.2:1A9FC57676FC6AC358A573E83061A04CEAC18C9B|tid:tGC9IZ1gMtVA' 
				app_data['fp_tdl'] = '19798436' 


		print '----------------------------- adforce_cv-----------------------------'	
		request = adforce_cv(campaign_data, app_data, device_data, isOpen = True)
		result = util.execute_request(**request)
		try:
			app_data['uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]
		except:
			app_data['uid']='00bb2cf133ad737d9b8dcf15238d52e0c9'

		app_data['chapter_1'] = True


	if random.randint(1,100) <= 50:

		# app relaunch
		print '----------------------------- adforce Analytics -----------------------------'	
		request = adforce_analytics(campaign_data, app_data, device_data)
		util.execute_request(**request)


	###########		CALLS		################
	
	return {'status':'true'}


###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def adforce_tmck(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/p/tmck'
	method='get'
	headers = {
		'User-Agent': 'ADMAGE SMPH SDK/iPhone/'+campaign_data.get('adforce').get('sdk')+'/CYZ/'+device_data.get('os_version')+'/'+device_data.get('device_type')+'/GL',#+device_data.get('model')+'/'
		'Accept-Language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding':'br, gzip, deflate',

	}
	params = {
				'_app':	campaign_data.get('adforce').get('app_id'),
				'_bundle_id': campaign_data.get('package_name'),
				'_bv':	campaign_data.get('app_version_name'),
				'_model': device_data.get('device_platform'),
				'_os_ver':	device_data.get('os_version'),
				'_sdk_ver':	campaign_data.get('adforce').get('sdk_version')
	}

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}
	
def adforce_analytics(campaign_data, app_data, device_data,isFirstCall=False):
	url = 'http://analytics.app-adforce.jp/fax/analytics'
	method='post'
	headers = {
		'User-Agent': 'ADMAGE SMPH SDK/iPhone/'+campaign_data.get('adforce').get('sdk')+'/CYZ/'+device_data.get('os_version')+'/'+device_data.get('device_type')+'/GL',#+device_data.get('model')+'/'
		'Content-Type': 'application/x-www-form-urlencoded',
		'Accept-Language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding':'br, gzip, deflate',
		
	}
	if not isFirstCall:
		headers['Cookie'] = 'uid='+app_data.get('uid')

	data = {
				'd' :0,
				'e': 1,
				'p':'00'+util.md5(app_data.get('idfa_id')),
				'v' : campaign_data.get('adforce').get('sdk_version')
	}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}	
	
def adforce_cv(campaign_data, app_data, device_data, isOpen = False):
	url = 'http://app-adforce.jp/ad/p/cv'
	method='get'

	agent = 'ADMAGE SMPH SDK/iPhone/'+campaign_data.get('adforce').get('sdk')+'/CYZ/'+device_data.get('os_version')+'/'+device_data.get('device_type')+'/GL'

	headers = {
		'User-Agent': 'ADMAGE SMPH SDK/iPhone/'+campaign_data.get('adforce').get('sdk')+'/CYZ/'+device_data.get('os_version')+'/'+device_data.get('device_type')+'/GL',#+device_data.get('model')+'/'
		'Accept-Language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding':'br, gzip, deflate',
	    'Cookie': 'uid='+app_data.get('uid'),
	}
	params = {
				'_adid':'00'+util.md5(app_data.get('idfa_id')),
				'_adte': 1,
				'_app':	campaign_data.get('adforce').get('app_id'),
				'_buid' :'',
				'_build': device_data.get('build'),
				'_bundle_id': campaign_data.get('package_name'),
				'_bv' : campaign_data.get('app_version_name'),
				'_carrier' : device_data.get('carrier'),
				'_country' : device_data.get('locale').get('country'),
				'_cv_target' : 'true',
				'_hash': util.get_random_string('hex',40),
				'_idfv' : app_data.get('idfv_id'),
				'_install_id' : app_data.get('install_id'),
				'_language' : device_data.get('locale').get('language'),
				'_model':device_data.get('device_platform'),
				'_os_ver':device_data.get('os_version'),
				'_rurl' : 'default',
				'_sdk_ver':campaign_data.get('adforce').get('sdk_version'),
				'_searchAdsErrorCode'	: '1',
				'_searchAdsErrorDetail' :	'Error Domain=ADClientErrorDomain Code=1 "Limit Ad Tracking is enabled for this device" UserInfo={NSLocalizedDescription=Limit Ad Tracking is enabled for this device}',
				'_searchAdsRetriedCount' :	'1',
				'_ua': device_data.get('User-Agent'),
				'_use_bw' : 'false',
				'_xevent' : 1,
				'_xuid'   : '',
				'_xuniq': app_data.get('idfa_id'),				
				'_xuniq_type' : 2,
				'_fpid'	: urlparse.unquote(app_data.get('fp_id')),
				'_fptdl':app_data.get('fp_tdl'),
	}
	data=None

	if isOpen:

		try:
			headers['Cookie'] = "_app="+ campaign_data.get('adforce').get('app_id') +"; _app_inner=1; _sdk="+ agent +"; _xroute=; _xtid="+ app_data.get('idfv_id') +"; _xuid=; _xuniq="+ app_data.get('idfa_id') +"; uid=" + app_data.get('uid')

		except:
			pass
		
		params= {       "_adid": '00'+util.md5(app_data.get('idfa_id')),
				        "_adte": "0",
				        "_app": campaign_data.get('adforce').get('app_id'),
				        "_buid": "",
				        "_cvpoint": util.get_random_string('decimal', 4),
				        "_install_id": app_data.get('install_id'),
				        "_model": device_data.get('device_platform'),
				        "_rurl": "",
				        "_sdk_ver": campaign_data.get('adforce').get('sdk_version'),
				        "_xtid": app_data.get('idfv_id'),
				        "_xuniq": app_data.get('idfa_id'),	
				        }

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adforce_cv2(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/p/cv'
	method='get'
	headers = {
		'User-Agent': 'ADMAGE SMPH SDK/iPhone/'+campaign_data.get('adforce').get('sdk')+'/CYZ/'+device_data.get('os_version')+'/'+device_data.get('device')+'/GL',#+device_data.get('model')+'/'
		'Accept-Language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding':'br, gzip, deflate',
	    'Cookie': 'uid='+app_data.get('uid'),
	}
	params = {
				'_adid':'00'+util.md5(app_data.get('idfa_id')),
				'_adte': 1,
				'_app':	campaign_data.get('adforce').get('app_id'),
				'_buid' :'8620673',
				'_carrier' : device_data.get('carrier'),
				'_install_id' : app_data.get('install_id'),
				'_model':device_data.get('model'),
				'_cvpoint' : random.randint(2700,3000),
				'_rurl' : '',
				'_sdk_ver':campaign_data.get('adforce').get('sdk_version'),
				'_xtid' : app_data.get('idfv_id'),
				'_xuniq': app_data.get('idfa_id'),	
				'age' : str(random.randint(20,30)),
				'gender' : 'male',
				'location' : '5',			
	}
	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


def adforce_collect(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/view/collect_ios.html'
	
	method='get'
	
	headers = {		
				'User-Agent': device_data.get('User-Agent'),
				'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Accept-Encoding': 'br, gzip, deflate',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
	}
	params = None

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adforce_fprc(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/i/fprc'
	method='get'
	headers = {
		'User-Agent': device_data.get('User-Agent'),
		'Accept': '*/*',
		'Referer': 'http://app-adforce.jp/ad/view/collect_ios.html',
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Cookie': 'uid='+app_data.get('uid'),

	}
	params = None	

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
def adforce_fp(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/p/fp'
	method='get'
	headers = {
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'User-Agent': device_data.get('User-Agent'),		
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Cookie': 'uid='+app_data.get('uid'),
	}
	
	params = {
				'_fpsd' : 'Nda44j1d7lY5BNvcKyAdMUDFBpBeA0fUmCbiyIkIr2oazOvjZwxoPwe_tMcpMlgebiYMpz6y8GGEDd5iD01PngnU77qZoOSix5ezdstlYyWesj2XaT42pidPZW2AUMnGWVQdgMVQdgQESwalftckcKyAd65hz7qTvtE0EREHQxbiyInrGfmeM_TYKU5meTuCUMwUs_43wuZPss38fDnCwVYPIG_qvoPfybYb5Eu5fXwD9OsJELzDz.ICMpwoNRe9B5JuUshpDObeAuyPBDjaY2ftckuyPB884akHGOg4B2ho4HLL5JpnOai.uJtHoqvynx9MsFyxYMI9Ps.HY6S8fs3dNk8HlqDJ3tG2hiwAy3nwgjjNpyXs.HrI9Pv37lY5BSpFY5BNkOWhsDpvnJwrTcEhRcxG2htNMuIEmVzdYI25lMk.1KF'
	}	

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		


	
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = str(int(time.time()*1000))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = 'ffffffff-'+random_string(4)+'-'+random_string(4)+'-0000-0000'+random_string(8)

def get_batteryLevel():
	return str(random.randint(10,100))+".0"

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : MISC
#
###########################################################
def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"