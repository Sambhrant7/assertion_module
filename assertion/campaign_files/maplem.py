# -*- coding: utf8 -*-
from sdk import util,purchase,installtimenew
from sdk import getsleep
from sdk import NameLists
import time
import random
import json
import datetime
import urllib
import uuid
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'app_size'           : 82.0,#81.0,#82.0, #81.0
	'package_name'		 :'com.nexon.nsc.maplem',
	'app_name' 			 :u'메이플스토리M'.encode('utf-8'),
	'app_version_name'	 : '1.44.922',#'1.42.895',#'1.42.893',#'1.41.860',#'1.41.858',#'1.40.842',#'1.39.826',#'1.39.824',#'1.39.822',#1.38.797, 1.38.794, '1.38.791',#1.37.770, 1.36.744, '1.36.743',#'1.36.736',#1.35.714, '1.33.663',#'1.32.624',#'1.32.622',#'1.31.601',#'1.26.476',1.31.597, 1.34.687
	'no_referrer'		 : True,
	'device_targeting'	 :True,
	'supported_os'		 :'4.4', #Requires Android 4.2 and up
	'ctr'				 :6,
	'tracker'			 :'adjust',		
	'supported_countries':'WW',
	'adjust':{
		'app_token'	: 'pimzm0762vi8',
		'sdk'		: 'unity4.12.5@android4.12.4',	
		'secret_key': '6724512391371997178344526624819879064',
		'secret_id' : '3',
	},
	'unity3D'   :{
                            'localprojectid'  :   '914ebfb0d11b7ae46a868f49de2ec4ce',
                            'platformid':'11',
                            "X-Unity-Version": '2017.4.17f1',#"2017.3.1f1",
                            
    }, 
    'purchase_var':{
    '1':'1.97',
    '2':'4.93',
    '3':'8.88',
    },
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:23,
		22:21,
		23:20,
		24:19,
		25:18,
		26:17,
		27:16,
		28:14,
		29:12,
		30:11,
		31:10,
		32:9,
		33:8,
		34:7,
		35:5,
	},
}

session_ID=False
unity_data_block_id=False
unity_user_id=False
unity_event_count=False
continous_Request_counter=False
build_GUID=False
device_ID=False	


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)	
	set_sessionLength(app_data,forced=True,length=0)
	def_sec(app_data,device_data)

	app_data['installed_at']=datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	app_data['click']=datetime.datetime.utcfromtimestamp((app_data.get('times').get('click_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	app_data['created_at']=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	app_data['sent_at']=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	
	app_data['adjust_call_time']=int(time.time())

	charecters='ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'

	app_data['uid']=''.join(random.choice(charecters) for i in range(10))


	###########		CALLS		################	
	print '\nAdjust : xunity self call____________________________________'
	request=adjust_xunity(campaign_data, app_data, device_data)
	util.execute_request(**request)
	

	print '\nAdjust : xunity self call____________________________________'
	request=adjust_xunity_event(campaign_data, app_data, device_data,continous_Request_counter, unity_event_count,call=True)
	util.execute_request(**request)

	print '\nAdjust : xunity self call____________________________________'
	request=adjust_xunity_event(campaign_data, app_data, device_data,continous_Request_counter, unity_event_count,call=False)
	util.execute_request(**request)

	print '\nAdjust : stamp self call____________________________________'
	request=adjust_stamp_selfcall(campaign_data, app_data, device_data)
	util.execute_request(**request)
	
	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)

	print '\nAdjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,source='reftag')
	util.execute_request(**request)

	print '\nAdjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,source='install_referrer')
	util.execute_request(**request)

	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(5,30))
	print '\nAdjust : Event____________________________________'
	# interval 		= random.randint(39,41)
	# set_sessionLength(app_data,length=interval)
	callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
	request=adjust_event(campaign_data, app_data, device_data,event_token='f0d3bh',callback_params=callback_params)
	util.execute_request(**request)
	time.sleep(random.randint(5,10))

	
	if random.randint(1,100)<=90:

		time.sleep(random.randint(180,450))
		print '\nAdjust : Event_______________________after inapp download_____________'
		# interval 		= random.randint(4500,4600)
		# set_sessionLength(app_data,length=interval)
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='1bi9yq',callback_params=callback_params)
		util.execute_request(**request)
		
		time.sleep(random.randint(15,35))		
		print '\nAdjust : Event______________________tap to start______________'
		# interval 		= random.randint(4500,4600)
		# set_sessionLength(app_data,length=interval)
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='yefh3e',callback_params=callback_params)
		util.execute_request(**request)
		
	
		print '\nAdjust : Event__________________________select a server__________'
		time.sleep(random.randint(1,2))
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='4in7xr',callback_params=callback_params)
		util.execute_request(**request)


		app_data['server_selected']=True
		
		
		if random.randint(1,100)<=89:
			time.sleep(random.randint(60,150))
			print '\nAdjust : Event________________________after server choose____________'
			callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
			request=adjust_event(campaign_data, app_data, device_data,event_token='atm4ko',callback_params=callback_params)
			util.execute_request(**request)
			
			time.sleep(random.randint(30,45))
			print '\nAdjust : Event__________________________character create__________'
			callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
			request=adjust_event(campaign_data, app_data, device_data,event_token='jevt6o',callback_params=callback_params)
			util.execute_request(**request)

			time.sleep(random.randint(1,3))
			print '\nAdjust : Event__________________________character create_________'
			callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
			request=adjust_event(campaign_data, app_data, device_data,event_token='k17tsj',callback_params=callback_params)
			util.execute_request(**request)


			print '\nAdjust : Event__________________________start game_________'
			time.sleep(random.randint(5,140))
			callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
			request=adjust_event(campaign_data, app_data, device_data,event_token='4yhm1z',callback_params=callback_params)
			util.execute_request(**request)

			app_data['game_started']=True

			if random.randint(1,100)<=79:
				time.sleep(random.randint(40,100))
				print '\nAdjust : Event__________________________complete tutorial_________'
				callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
				request=adjust_event(campaign_data, app_data, device_data,event_token='bv7r9p',callback_params=callback_params)
				util.execute_request(**request)
				app_data['tut_complete']=True

				print "hey-------------"
				call_function(app_data,campaign_data,device_data,typee='install')


	###########		FINALIZE	################
	set_appCloseTime(app_data)
	
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

	set_sessionLength(app_data,forced=True,length=4000)
	def_sec(app_data,device_data)

	if not app_data.get('installed_at'):
		app_data['installed_at']=datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

		app_data['click']=datetime.datetime.utcfromtimestamp((app_data.get('times').get('click_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

		app_data['created_at']=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
		time.sleep(random.randint(4,6))
		app_data['sent_at']=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	
	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())

	check_appCloseTime(app_data)

	if not app_data.get('server_selected'):
		app_data['server_selected']=False
	
	if not app_data.get('tut_complete'):
		app_data['tut_complete']=False
		
	if not app_data.get('game_started'):
		app_data['game_started']=False
	

	if not app_data.get('uid'):
		charecters='ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
		app_data['uid']=''.join(random.choice(charecters) for i in range(10))

	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,isOpen=True)
	util.execute_request(**request)

	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)


	if app_data.get('server_selected')==False:
		print '\nAdjust : Event_______________________after inapp download_____________'
		time.sleep(random.randint(180,450))
		# interval 		= random.randint(4500,4600)
		# set_sessionLength(app_data,length=interval)
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='1bi9yq',callback_params=callback_params)
		util.execute_request(**request)
			

		print '\nAdjust : Event______________________tap to start______________'
		time.sleep(random.randint(15,35))	
		# interval 		= random.randint(4500,4600)
		# set_sessionLength(app_data,length=interval)
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='yefh3e',callback_params=callback_params)
		util.execute_request(**request)
		
	
		print '\nAdjust : Event__________________________select a server__________'
		time.sleep(random.randint(1,2))
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='4in7xr',callback_params=callback_params)
		util.execute_request(**request)
		app_data['server_selected']=True
		



	if app_data.get('game_started')==False:	
		time.sleep(random.randint(60,150))
		print '\nAdjust : Event________________________after server choose____________'
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='atm4ko',callback_params=callback_params)
		util.execute_request(**request)
		time.sleep(random.randint(30,45))
		print '\nAdjust : Event__________________________character create__________'
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='jevt6o',callback_params=callback_params)
		util.execute_request(**request)


		time.sleep(random.randint(1,3))
		print '\nAdjust : Event__________________________character create_________'
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='k17tsj',callback_params=callback_params)
		util.execute_request(**request)

		print '\nAdjust : Event__________________________start game_________'
		time.sleep(random.randint(5,140))
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='4yhm1z',callback_params=callback_params)
		util.execute_request(**request)
		app_data['game_started']=True

	if app_data.get('tut_complete')==False:
		time.sleep(random.randint(40,100))	
		print '\nAdjust : Event__________________________complete tutorial_________'
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='bv7r9p',callback_params=callback_params)
		util.execute_request(**request)
		app_data['tut_complete']=True

	print "Hey There"
	if app_data.get('tut_complete')==True:
		call_function(app_data,campaign_data,device_data,typee='open',day=day)

	
	if purchase.isPurchase(app_data,day,advertiser_demand=10):
		time.sleep(random.randint(20,30))
		rr=random.randint(1,100)

		if rr<=80:
			choose='1'
		elif rr<=90:
			choose='2'
		else:
			choose='3'	

		revenue=campaign_data.get('purchase_var').get(choose)

		print '\nAdjust : Event__________________________purchase_________'
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='wcp0lm',callback_params=callback_params)
		util.execute_request(**request)
##########################################################################################################################
##########################################################################################################################
#########################################################################################################################
#
#								Event_token for wrong app('wcp0lm')
#
##########################################################################################################################
#########################################################################################################################
##########################################################################################################################

		print '\nAdjust : Event__________________________purchase with revenue_________'
		callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
		request=adjust_event(campaign_data, app_data, device_data,event_token='l23mxi',callback_params=callback_params,rev=revenue)
		util.execute_request(**request)


	set_appCloseTime(app_data)
	
	return {'status':'true'}


def call_function(app_data,campaign_data,device_data,typee='',day=None):

	if not app_data.get('level'):
		app_data['level']=0

	if typee=='install':
		level_list=random.choice([1,2,3,4,5,6,7,8,9,10,11,11,11,11])
	if typee=='open':
		if day==1:
			level_list=random.choice([6,7,8,9,10,10,11,11,11,11,11])
		else:
			level_list=random.choice([6,7,8,9,10,10,11,11])

	if random.randint(1,100)<=60:
		for x in range(level_list):
			time.sleep(random.randint(60,80))
			if app_data.get('level')==10:				
				print '\nAdjust : Event__________________________level 10_________'
				callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
				request=adjust_event(campaign_data, app_data, device_data,event_token='j8jlt4',callback_params=callback_params)
				util.execute_request(**request)

				print '\nAdjust : Event__________________________level 10__________'
				callback_params=json.dumps({"uid":'0G'+app_data.get('uid')})
				request=adjust_event(campaign_data, app_data, device_data,event_token='bzhuwp',callback_params=callback_params)
				util.execute_request(**request)



			
			app_data['level']+=1	



###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,isOpen=False,callback_params=None):
	set_androidUUID(app_data)
	inc_(app_data,'session_count')


	url 	= 'http://app.adjust.com/session'
	method 	= 'post'
	headers = {
		'Client-SDK'		: campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/x-www-form-urlencoded',
		'User-Agent'		: get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:app_data.get('created_at'),
		'device_manufacturer'	:device_data.get('brand').upper(),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[1],
		'display_width'			:device_data.get('resolution').split('x')[0],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:app_data.get('installed_at'),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:app_data.get('sent_at'),
		'session_count'			:app_data.get('session_count'),
		'tracking_enabled'		:'1',
		'updated_at'			:app_data.get('installed_at'),
		'vm_isa'				:'arm64',
		'connectivity_type'		:'1',
		'network_type'			:'13',
		'mcc'            		:device_data.get('mcc'),
		'mnc'              		:device_data.get('mnc')
		}
	if isOpen:
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['created_at'] 		= get_date(app_data,device_data,early=random.uniform(1,3))
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('appCloseTime')-app_data.get('adjust_call_time')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('appCloseTime')-app_data.get('adjust_call_time')
		app_data['adjust_call_time']=int(time.time())

	headers['Authorization']= get_auth(app_data,activitykind='session',created_at=data['created_at'],gps_adid=data['gps_adid'])

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=None,partner_params=None,rev=None):
	set_androidUUID(app_data)
	inc_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	def_sessionLength(app_data)

	created_at=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')	
	time.sleep(random.randint(1,3))
	sent_at=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	time_spent=int(time.time())-app_data.get('adjust_call_time')

	url 	= 'http://app.adjust.com/event'
	method 	= 'post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('brand').upper(),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[1],
		'display_width'			:device_data.get('resolution').split('x')[0],
		'event_count'			:app_data.get('event_count'),
		'event_token'			:event_token,
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'session_length'		:time_spent,
		'subsession_count'		:app_data.get('subsession_count'),
		'tracking_enabled'		:'1',
		'time_spent'			:time_spent,
		'vm_isa'				:'arm64',
		'callback_params'       :callback_params,
		'connectivity_type'		:'1',
		'network_type'			:'13',
		'mcc'            :device_data.get('mcc'),
		'mnc'              :device_data.get('mnc')
		}

	if rev:
		data['revenue']=str(rev)+util.get_random_string('decimal',3)
		data['currency']='USD'

	headers['Authorization']= get_auth(app_data,activitykind='event',created_at=data['created_at'],gps_adid=data['gps_adid'])

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,source='install_referrer'):
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')

	time_spent=int(time.time())-app_data.get('adjust_call_time')
	
	url 	= 'http://app.adjust.com/sdk_click'
	method 	='post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	data = {
		'click_time'			:app_data.get('click') if source=="install_referrer" else get_date(app_data,device_data),
		'referrer'				:urllib.unquote(app_data.get('referrer')),
		# 'reftag'				:reftag,
		'source'				:source,
		
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:app_data.get('created_at'),
		'device_manufacturer'	:device_data.get('brand').upper(),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[1],
		'display_width'			:device_data.get('resolution').split('x')[0],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:app_data.get('installed_at'),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:app_data.get('sent_at'),
		'session_count'			:app_data.get('session_count'),
		'tracking_enabled'		:'1',
		'updated_at'			:app_data.get('installed_at'),
		'vm_isa'				:'arm64',
		'connectivity_type'		: '1',
		# 'queue_size':8,
		'session_length'		: time_spent,
		'subsession_count'		: app_data.get('subsession_count'),
		'time_spent'			: time_spent,
		'last_interval'			:'0',
		'network_type'			:'13',
		'mcc'            :device_data.get('mcc'),
		'mnc'              :device_data.get('mnc')
		}



	if app_data.get('referrer'):
		data['referrer'] = urllib.unquote(app_data.get('referrer'))	
		data['raw_referrer']=urllib.unquote(app_data.get('referrer'))		
	else :
		data['referrer'] = "adjust_reftag=cHJk1UraCT84Z&utm_source=taptica_NxK_UASus4&utm_campaign=KR_AOS_NxK&utm_content=2231416&utm_term=40079"
		data['raw_referrer']="adjust_reftag=cHJk1UraCT84Z&utm_source=taptica_NxK_UASus4&utm_campaign=KR_AOS_NxK&utm_content=2231416&utm_term=40079"
	
	if source=='install_referrer':
		def_sec(app_data,device_data)
		data['install_begin_time']=str(datetime.datetime.utcfromtimestamp((app_data.get('times').get('download_begin_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'))
		del data['raw_referrer']
		del data['last_interval']
		

	headers['Authorization']= get_auth(app_data,activitykind='click',created_at=data['created_at'],gps_adid=data['gps_adid'])
		
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}

###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data, device_data):

	time.sleep(random.randint(10,12))
	sent_at=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	url 	= 'http://app.adjust.com/attribution'
	method 	='head'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	params = {
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:app_data.get('created_at'),
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'needs_response_details':'1',
		'sent_at'				:sent_at,
		'tracking_enabled'		:'1',
		}

	headers['Authorization']= get_auth(app_data,activitykind='attribution',created_at=params['created_at'],gps_adid=params['gps_adid'])

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

###################################self call#####################################################

def adjust_xunity(campaign_data, app_data, device_data):
	url 	= 'https://config.uca.cloud.unity3d.com/'
	method 	='post'
	headers = {
		'Content-Type': 'application/json',
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		'X-Unity-Version': '2017.3.1f1',

		 }
	data={
		"common":{
				"appid":"local."+campaign_data.get('unity3D').get('localprojectid'),
				"build_guid":get_build_guid(build_GUID),
				"deviceid":get_deviceid(device_ID),
				"localprojectid":campaign_data.get('unity3D').get('localprojectid'),
				"platform":"AndroidPlayer",
				"platformid":int(campaign_data.get('unity3D').get('platformid')),
				"sdk_ver":"u"+campaign_data.get('unity3D').get('X-Unity-Version'),
				"sessionid": int(get_sessionid(session_ID)),
				"userid":get_userid(unity_user_id)
			}
	}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':json.dumps(data)}

def adjust_xunity_event(campaign_data, app_data, device_data,continous_Request_counter, unity_event_count,call=True):
	url 	= 'http://cdp.cloud.unity3d.com/v1/events'
	method 	='post'

	if not continous_Request_counter:
		continous_Request_counter=1
	else:
		continous_Request_counter+=1

	if not unity_event_count:
		unity_event_count=1
	else:
		unity_event_count+=1

	headers = {
		'Content-Type': 'application/json',
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		'X-Unity-Version': campaign_data.get('unity3D').get('X-Unity-Version'),
		'continuous_request': str(continous_Request_counter),
		'data_block_id': get_data_block_id(unity_data_block_id),
		'data_retry_count': '1',
		'event_count': str(unity_event_count),
		'expired_session_dropped': '0'

		 }
	data={
		'common':{
		"appid":"local."+campaign_data.get('unity3D').get('localprojectid'),
		"build_guid":get_build_guid(build_GUID),
		"deviceid":get_deviceid(device_ID),
		"localprojectid":campaign_data.get('unity3D').get('localprojectid'),
		"platform":"AndroidPlayer",
		"platformid":campaign_data.get('unity3D').get('platformid'),
		"sdk_ver":"u"+campaign_data.get('unity3D').get('X-Unity-Version'),
		"sessionid": str(get_sessionid(session_ID))+'E+18',
		"userid":get_userid(unity_user_id),
		
	}
	}

	if call==True:
		value=str({"type":"analytics.appStart.v1", 
				"msg":{"ts":int(time.time()*1000)}})
		
	if call==False:
		value=str({"type":"analytics.appInstall.v1","msg":{"ts":int(time.time()*1000),"unity_ver":campaign_data.get('unity3D').get('X-Unity-Version'),"app_ver":campaign_data.get('app_version_name')}})
		
	json_str=json.dumps(data)+value
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':json_str}

def adjust_stamp_selfcall(campaign_data, app_data, device_data):
	url 	= 'https://stamp.mp.nexon.com/sdk/v1/enter'
	method 	='get'
	headers = {
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		'Content-Type': 'application/x-www-form-urlencoded',
		'X-Toy-Ticket': 'ttc.ne1.8f65f4b1-cfce-4c30-88ab-2d8ec6ffc261',
		 }
	params = {
		'market_type':	'gps',
		'client_id':	'NTQwMzgzODAz'
		}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())


def get_sessionid(session_ID):
	if not session_ID:
		session_ID= util.get_random_string('decimal',19) 
	return session_ID

def get_build_guid(build_GUID):
	if not build_GUID:
		build_GUID= util.get_random_string('hex', 32) 
	return build_GUID

def get_deviceid(device_ID):
	if not device_ID:
		device_ID= util.get_random_string('hex', 32) 
	return device_ID

def get_data_block_id(unity_data_block_id):
	if not unity_data_block_id:
		unity_data_block_id= util.get_random_string('hex', 32) 
	return unity_data_block_id

def get_userid(unity_user_id):
	if not unity_user_id:
		unity_user_id= util.get_random_string('hex', 32) 
	return unity_user_id


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def set_installedAT(app_data,device_data):
	if not app_data.get('installed_at'):
		app_data['installed_at'] =  get_date(app_data,device_data,early=random.uniform(145,155))

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = str(datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'))
	return date

def get_auth(app_data,activitykind,created_at,gps_adid):
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activitykind)	
	sign= util.sha256(data1)
	auth= 'Signature secret_id="'+campaign_data['adjust']['secret_id']+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'	
	return auth