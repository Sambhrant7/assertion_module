# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import NameLists
from sdk import util
from sdk import purchase
from sdk import getsleep
import time
import random
import urllib
import datetime
import urlparse
import json
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'app_size'           : 12.0,
	'package_name'		 :'com.ionicframework.conference463383',
	'app_name' 			 :u'eZhire'.encode('utf-8'),
	'app_version_code'   : '137',
	'app_version_name' 	 : '0.1.47',#'0.1.37',
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 : 'l8fil8iox3i8',
		'sdk'		 : 'android4.17.0',#'cordova4.17.1@android4.17.0',
		},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android', min_sleep=0)

	register_user(app_data)

	set_androidUUID(app_data)

	app_data['adjust_call_time']=int(time.time())

	###########		CALLS		############
	click_time1=get_date(app_data,device_data)
	time.sleep(random.randint(2,4))
	created_at_session=get_date(app_data,device_data)
	created_at_sdk1=get_date(app_data,device_data)
	created_at_sdk2=get_date(app_data,device_data)
	time.sleep(random.randint(0,1))
	sent_at_session=get_date(app_data,device_data)

	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)

	sent_at_sdk1=get_date(app_data,device_data)
	time.sleep(random.randint(2,4))
	created_at_attr=get_date(app_data,device_data)
	time.sleep(random.randint(0,1))
	sent_at_attr=get_date(app_data,device_data)
	time.sleep(random.randint(2,4))
	sent_at_sdk2=get_date(app_data,device_data)
	t1=0
	t2=2
		
	if app_data.get('referrer'):
		print '\n'+'Adjust : SDK CLICK____________________________________'
		request=adjust_sdkclick(campaign_data, app_data, device_data,created_at=created_at_sdk1,sent_at=sent_at_sdk1,click_time=click_time1,source='reftag')
		util.execute_request(**request)

		time.sleep(random.randint(5,8))

		print '\n'+'Adjust : SDK CLICK____________________________________'
		request=adjust_sdkclick(campaign_data, app_data, device_data,created_at=created_at_sdk2,sent_at=sent_at_sdk2,click_time=click_time1,source='install_referrer')
		util.execute_request(**request)
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,created_at=created_at_attr,sent_at=sent_at_attr)
	util.execute_request(**request)

	login(campaign_data,app_data,device_data,t1=t1,t2=t2)


	if random.randint(1,100)<=95:
		time.sleep(random.randint(20,35))
		request=registration(campaign_data, app_data, device_data)
		response=util.execute_request(**request)
		try:
			app_data['login_data'] = json.loads(response.get('data')).get('id')

		except:
			app_data['login_data'] = '18'+str(random.randint(100,999))

		registration_page(campaign_data,app_data,device_data,t1=t1,t2=t2)
		app_data['registration_flag'] = True

		if random.randint(1,100)<=95:
			time.sleep(random.randint(5,15))
			user_generated(campaign_data,app_data,device_data,t1=t1,t2=t2)
			app_data['user_flag'] = True

			if random.randint(1,100)<=95:
				time.sleep(random.randint(5,15))
				book_page(campaign_data,app_data,device_data,t1=t1,t2=t2)

				for x in range(4,8):
					random_events = random.randint(1,4)

					if random_events==1 and random.randint(1,100)<=80:
						time.sleep(random.randint(5,15))
						history_page(campaign_data,app_data,device_data,t1=t1,t2=t2)

					if random_events==2 and random.randint(1,100)<=50:
						time.sleep(random.randint(5,15))
						message_screen(campaign_data,app_data,device_data,t1=t1,t2=t2)

					if random_events==3 and random.randint(1,100)<=50:
						time.sleep(random.randint(5,15))
						profile_page(campaign_data,app_data,device_data,t1=t1,t2=t2)

					if random_events==4 and random.randint(1,100)<=25:
						time.sleep(random.randint(5,15))
						help_page(campaign_data,app_data,device_data,t1=t1,t2=t2)

	set_appCloseTime(app_data)
	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android', min_sleep=0)

	set_androidUUID(app_data)
	created_at_session=get_date(app_data,device_data)
	time.sleep(random.randint(1,2))
	sent_at_session=get_date(app_data,device_data)
	t1=0
	t2=2

	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())

	if not app_data.get('registration_flag'):
		app_data['registration_flag'] = False
	if not app_data.get('user_flag'):
		app_data['user_flag'] = False

	register_user(app_data)


	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session,isOpen=True)
	util.execute_request(**request)

	app_data['adjust_call_time']=int(time.time())

	if not app_data.get('login_data'):

		request=registration(campaign_data, app_data, device_data)
		response=util.execute_request(**request)
		try:
			app_data['login_data'] = json.loads(response.get('data')).get('id')

		except:
			app_data['login_data'] = '18'+str(random.randint(100,999))

	if app_data.get('registration_flag') == False:
		time.sleep(random.randint(20,35))

		registration_page(campaign_data,app_data,device_data,t1=t1,t2=t2)
		app_data['registration_flag'] = True

	if app_data.get('user_flag') == False:
		time.sleep(random.randint(5,15))
		user_generated(campaign_data,app_data,device_data,t1=t1,t2=t2)
		app_data['user_flag'] = True


	book_page(campaign_data,app_data,device_data,t1=t1,t2=t2)

	for x in range(4,8):
		random_events = random.randint(1,4)

		if random_events==1 and random.randint(1,100)<=80:
			time.sleep(random.randint(5,15))
			history_page(campaign_data,app_data,device_data,t1=t1,t2=t2)

		if random_events==2 and random.randint(1,100)<=50:
			time.sleep(random.randint(5,15))
			message_screen(campaign_data,app_data,device_data,t1=t1,t2=t2)

		if random_events==3 and random.randint(1,100)<=50:
			time.sleep(random.randint(5,15))
			profile_page(campaign_data,app_data,device_data,t1=t1,t2=t2)

		if random_events==4 and random.randint(1,100)<=25:
			time.sleep(random.randint(5,15))
			help_page(campaign_data,app_data,device_data,t1=t1,t2=t2)

	if purchase.isPurchase(app_data,day,advertiser_demand=5):
		time.sleep(random.randint(30,60))
		randompurchase = random.randint(1,100)

		booking_generated(campaign_data,app_data,device_data,t1=t1,t2=t2)
		time.sleep(random.randint(5,15))
		booking_confirmed(campaign_data,app_data,device_data,t1=t1,t2=t2)

	if app_data.get('flag_isPurchaseDone') == True and random.randint(1,100)<=10:
		time.sleep(random.randint(5,10))
		booking_details(campaign_data,app_data,device_data,t1=t1,t2=t2)


	set_appCloseTime(app_data)
	return {'status':'true'}


############### Events definition #######################

def login(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="u5sfqb",t1=0,t2=0)
	util.execute_request(**request)

def registration_page(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="hyt1mj",t1=0,t2=0)
	util.execute_request(**request)

def user_generated(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="ic0q3r",t1=0,t2=0,callback_params=json.dumps({"user_id":str(app_data.get('login_data'))}))
	util.execute_request(**request)

def book_page(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="iqjjh8",t1=0,t2=0)
	util.execute_request(**request)

def history_page(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="wo7n3l",t1=0,t2=0)
	util.execute_request(**request)

def message_screen(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="cdlfat",t1=0,t2=0)
	util.execute_request(**request)

def profile_page(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="frgl89",t1=0,t2=0)
	util.execute_request(**request)

def help_page(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="hj7aji",t1=0,t2=0)
	util.execute_request(**request)

def booking_generated(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="pk2dnu",t1=0,t2=0)
	util.execute_request(**request)

def booking_confirmed(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="edzhqx",t1=0,t2=0)
	util.execute_request(**request)

def booking_details(campaign_data,app_data,device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token="36zz57",t1=0,t2=0)
	util.execute_request(**request)



###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,created_at,sent_at,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	if not app_data.get('sessionLength'):
		app_data['sessionLength']=0
	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"language" : device_data.get('locale').get('language'),
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"gps_adid_src" : "service",
		"device_known" : 0,
		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if isOpen:
		def_(app_data,'subsession_count')
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('appCloseTime')-app_data.get('adjust_call_time')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('appCloseTime')-app_data.get('adjust_call_time')
		app_data['sess_len']=int(time.time())


	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,created_at,sent_at,click_time='',source='',callback_params=None,partner_params=None):
	reftag = ''
	temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
	if temp_b.get('adjust_reftag'):
		reftag = temp_b.get('adjust_reftag')[0]
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	app_data['sessionLength']=time_spent
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"raw_referrer" : "utm_source=(not%20set)&utm_medium=(not%20set)",
		"referrer" : "utm_source=(not set)&utm_medium=(not set)",
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"source" : source,
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"gps_adid_src" : "service",
		"session_count" :	1,
		"session_length":	0,
		"subsession_count":	1,
		"time_spent"    :	0,
		"device_known" : 0,
		"last_interval" : 0,
		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		if click_time:
			data['click_time']=click_time
		elif data.get("click_time"):
			del data['click_time']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		if click_time:
			data['click_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("click_time"))
		
		if data.get('raw_referrer'):
			del data['raw_referrer']
		
		data['install_begin_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_end_time"))

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,created_at='',sent_at=''):
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"User-Agent" : get_ua(device_data),

		}
		
	params = {
		"attribution_deeplink" : "1",
		"gps_adid" : device_data.get('adid'),
		"event_buffering_enabled" : "0",
		"package_name" : campaign_data.get('package_name'),
		"initiated_by" : "sdk",
		"created_at" : created_at,
		"tracking_enabled" : "1",
		"needs_response_details" : "1",
		"device_name" : device_data.get('model'),
		"environment" : "production",
		"os_version" : device_data.get('os_version'),
		"os_name" : "android",
		"gps_adid_src" : "service",
		"android_uuid" : app_data.get('android_uuid'),
		"device_type" : device_data.get('device_type'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"api_level" : device_data.get('sdk'),
		"sent_at" : sent_at,
		"device_known" : 0,
		}

	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'subsession_count')
	inc_(app_data,'event_count')

	if not event_token=='5ckulj':
		created_at=get_date(app_data,device_data)
		time.sleep(random.randint(t1,t2))
		sent_at=get_date(app_data,device_data)

	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	app_data['sessionLength']=time_spent
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		# "created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"event_count" : app_data.get('event_count'),
		"event_token" : event_token,
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"language" : device_data.get('locale').get('language'),
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		# "sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"session_length" : session_length,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"gps_adid_src" : "service",
		"device_known" : 0,
		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if event_token=='5ckulj':
		data['created_at'] 	= t1
		data['sent_at'] 	= t2	
	else:
		data['created_at'] 	= created_at
		data['sent_at'] 	= sent_at	

	if event_token == 'edzhqx':
		data['currency'] = 'AED'
		data['revenue'] = random.choice(['69','79','89','129','69','79','89','129','69','79','89','139','149'])
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}



def registration(campaign_data, app_data,device_data):
	method = "post"
	url = 'https://ezhire.life/rental/fUsers/'
	headers = {
		"Accept-Encoding" : "gzip",
		'Content-Type': 'application/json; charset=UTF-8',
		"User-Agent" : 'okhttp/3.14.1',
		'Authorization':'',

		}
	dob = random.randint(0,9)
	dl = random.randint(0,9)
	number = util.get_random_string(type='decimal',size=random.randint(8,9))
		
	params =None

	data = {"country":"+971",
			"country_code":"+971",
			"date_of_birth":"01/1/199"+str(dob),
			"dl_issue_date":"01/1/200"+str(dl),
			"dl_country":"+971",
			"email":app_data.get('user').get('email').lower(),
			"extra_field1":"",
			"extra_field2":"",
			"fcm_key":"",
			"first_name":app_data.get('user').get('firstname'),
			"id":0,"last_name":app_data.get('user').get('lastname'),
			"mobile":"+971"+number,
			"nationality":"+971",
			"password":util.get_random_string(type='all',size=random.randint(10,20)),
			"renting_in":1,
			"is_resident":"+971",
			"role_type":0,
			"user_source1":"Android",
			"telephone":"+971"+number,
			"user_id":0,
			"username":app_data.get('user').get('email').lower(),
			"code_number":""}

	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	return app_data.get('android_uuid')


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_auth(device_data,app_data,created_at,gps_adid,activity_type):
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activity_type)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'

#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

#########################################################################3
#
#			Neccessary Methods
#
#########################################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0


def register_user(app_data,country=random.choice(['united states','germany'])):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"