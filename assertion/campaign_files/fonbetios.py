import random,time,datetime,uuid
from sdk import util,installtimenew
from sdk import getsleep
import clicker,Config

campaign_data = {
			'app_size' : 111.4,#113.3,#113.9,#115.5,#115.2,#114.3,#156.1,115.1,
			'package_name' : 'ru.bkfon.fonbet',
			'app_version_code' : '1509',#'1502',#'1484',#'1481',#'1472',#'1468',#'1466',#'1443',#'1430',#'1424',#'1418',#1409,#'1408',#1400, '1373',#'1291',#'1253',1278
			'app_version_name' : '6.3',#'6.2',#'6.1.1',#'6.1',#'6.0.2',#'6.0.1',#'5.16',#'5.14',#'5.12',#'5.11.1',#'5.11',#5.10.3,#'5.10.2',#5.10, '5.9.1',#'5.8.3',#'5.7.2',5.8
			'supported_os':'8.0',
			'app_name':'Fonbet',
			'app_id':'1166619854',
			'device_targeting':True,
			'ctr':6,
			'tracker':'adjust',
			'adjust':
						{
							'app_token':'pkj9hf0p2y2o',
							'sdk':'ios4.13.0',
							'app_updated_at': '2019-09-13T19:11:51.000Z',#'2019-09-09T22:21:15.000Z',#'2019-08-27T16:13:59.000Z',#'2019-08-21T22:25:27.000Z',#'2019-08-10T12:06:55.000Z',#'2019-08-06T19:22:40.000Z',#'2019-08-01T20:32:22.000Z',#'2019-07-04T20:56:05.000Z',#'2019-06-13T15:29:35.000Z',#'2019-05-31T18:54:01.000Z',#'2019-05-24T18:01:37.000Z',#2019-05-07T18:22:27.000Z, 2019-04-20T15:06:43.000Z, '2019-04-04T17:33:08.000Z',#'2018-12-20T12:54:47.000Z',#'2018-10-30T21:26:33.000Z',
						},

			'supported_countries': 'WW',
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention':{
							1:50,
							2:48,
							3:46,
							4:44,
							5:40,
							6:38,
							7:35,
							8:32,
							9:31,
							10:30,
							11:29,
							12:28,
							13:27,
							14:26,
							15:25,
							16:24,
							17:23,
							18:22,
							19:21,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
			}
}

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def make_device_token(app_data):
	if not app_data.get('deviceToken'):
		app_data['deviceToken']=str(util.get_random_string('all',22))
	return app_data.get('deviceToken')	

###########################################################
#						INSTALL							  #
###########################################################
def install(app_data, device_data):

	print '*************Install**********************'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)
	get_ts=app_data.get('times').get('install_complete_time')
	app_data['installed_at']=get_date_ts(app_data,device_data,get_ts)
	time.sleep(random.randint(60,100))

	global session_time
	session_time=get_date(app_data,device_data)

	time.sleep(random.randint(1,2))
	global click_time
	click_time=get_date(app_data,device_data)

	time.sleep(random.randint(2,4))
	global attribution_sent
	attribution_sent=get_date(app_data,device_data)

	def_(app_data,'subsession_count')
	inc_(app_data,'subsession_count')
	def_(app_data,'session_count')
	inc_(app_data,'session_count')
	apple_receipt(app_data)
	###########		Initialize		############
	
	app_data['adjust_call_time']=int(time.time())
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	###########			Calls		############   
	print "\n---------------------------- Self_call ----------------------------------------"
	adjustSession = self_call1(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	print "\n---------------------------- ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)

	time.sleep(random.randint(0,1))	
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	time.sleep(random.randint(5,10))
	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,initiated_by='backend')
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,initiated_by='sdk')
	util.execute_request(**adjustSession)

	time.sleep(random.randint(1,2))
	print "\n----------------------------Self_call ----------------------------------------"
	adjustSession = self_call2(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	time.sleep(random.randint(2,4))
	print '\nAppmeasurement : config____________________________________'
	request=measurement_config(campaign_data, app_data, device_data)
	util.execute_request(**request)

	print "\n----------------------------Self_call ----------------------------------------"
	adjustSession = self_call3(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	if random.randint(1,100)<=60:
		time.sleep(random.randint(60,120))
		apply_for_age_verification(campaign_data, app_data, device_data)

	###########		Finalize	############
	set_appCloseTime(app_data)

	return {'status':True}

###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):

	###########		Initialize		############

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")
		get_ts=app_data.get('times').get('install_complete_time')
		app_data['installed_at']=get_date_ts(app_data,device_data,get_ts)
		time.sleep(random.randint(60,100))

	global session_time
	session_time=get_date(app_data,device_data)

	def_(app_data,'subsession_count')
	inc_(app_data,'subsession_count')
	def_(app_data,'session_count')
	inc_(app_data,'session_count')
	apple_receipt(app_data)

	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())
		
	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,type1='open')
	util.execute_request(**adjustSession)

	if random.randint(1,100)<=50:
		time.sleep(random.randint(60,120))
		apply_for_age_verification(campaign_data, app_data, device_data)

	return {'status':True}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def apply_for_age_verification(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________eo5ckt'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='eo5ckt',t1=t1,t2=t2)
	util.execute_request(**request)


def self_call1(campaign_data, app_data, device_data):
	url = 'https://ota.lokalise.co/v2.0/ios'
	method = 'head'
	headers = {
		'Accept': '*/*',
		'Accept-Language': 'ru',
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': 'Lokalise SDK; 0.8.1; iOS; '+campaign_data.get('package_name')+'; '+campaign_data.get('app_version_code')+';',
		'x-lokalise-sdk-build':'801',
		'x-lokalise-api-key':'cb6af745d1ad7f8230d604b399cce781af8bfcd2',
		'x-lokalise-uid':'8A8A259816854B4B9CBB26E62B0AB780',
		'x-lokalise-project-id':'384424575abd0e0095ca14.94283099',
		'x-lokalise-device-language':'ru',
		'x-lokalise-app-build':campaign_data.get('app_version_code'),
		'x-lokalise-prerelease':'0',
		'x-lokalise-app-language':device_data.get('locale').get('language'),
		'x-lokalise-current-bundle':'0',

	}
	params=None

	data = None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def self_call2(campaign_data, app_data, device_data):
	url = 'http://line.bkfon-resource.ru/line/updatesFromVersion/0/ru'
	method = 'head'
	headers = {
		'Accept': '*/*',
		'Accept-Language': 'ru;q=1',
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': 'REDFonbet/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
	}
	
	params={
		'deviceid':'I7-'+app_data.get('idfv_id'),
		'lang' :'rus',
		'sysId':'5'
	}

	data = {}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def self_call3(campaign_data, app_data, device_data):
	url = 'http://line.bkfon-resource.ru/line/updatesFromVersion/0/ru'
	method = 'head'
	headers = {
		'Accept-Language': 'ru;q=1',
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': 'REDFonbet/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
	}
	params={
		'lastUpdate':'0',
		'locale' :'ru',
		'sysId':'5'
	}
	data = {}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def measurement_config(campaign_data, app_data, device_data):
	url = 'http://app-measurement.com/config/app/1:1048389700023:ios:6a34b2988d5faa85'
	method = 'head'
	headers = {
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': '%D0%A4%D0%BE%D0%BD%D0%B1%D0%B5%D1%82/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': 'ru'
	}
	params={
			'app_instance_id':'8C292B15662E46AAA681387FFACFD3B7',
			'gmp_version':'50200',
			'platform':'ios'
	}
	data = {}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}
	
###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,type1='install'):
	time_spent=int(time.time())-app_data.get('adjust_call_time')
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': 'ru',
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'connectivity_type':	'2',
			'created_at': session_time,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'sent_at': session_time,
			'session_count': app_data.get('session_count'),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':app_data.get('installed_at'),
			'install_receipt': app_data.get('install_receipt'),
			'os_build':	device_data.get('build'),
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'mcc':device_data.get('mcc'),
			'mnc':device_data.get('mnc'),
			'network_type' : 'CTRadioAccessTechnologyHSDPA'
		}
	
	if type1 == "open":
		def_(app_data,'subsession_count')
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = time_spent
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] =time_spent

		app_data['adjust_call_time']=int(time.time())
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adjust_sdkclick(campaign_data, app_data, device_data):

	time_spent=int(time.time())-app_data.get('adjust_call_time')
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': 'ru',
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}

	data = {
			'details': '{"Version3.1":{"iad-attribution":"false"}}',
			'source': 'iad3',
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'connectivity_type' :'2',
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': click_time,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid' :app_data.get('ios_uuid'),
			'sent_at':click_time,
			'session_count': app_data.get('session_count'),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':app_data.get('installed_at'),
			'install_receipt': app_data.get('install_receipt'),
			'os_build':	device_data.get('build'),
			'last_interval': get_lastInterval(app_data),
			'session_length': time_spent,
			'subsession_count': app_data.get('subsession_count'),
			'time_spent' : time_spent,
			'mcc':device_data.get('mcc'),
			'mnc':device_data.get('mnc'),
			'network_type' : 'CTRadioAccessTechnologyHSDPA'
			}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
def adjust_attribution(campaign_data, app_data, device_data,initiated_by='backend'):
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': 'ru',
		'Accept-Encoding': 'br, gzip, deflate',
	}
	data={}

	params = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink' : 1,
			'created_at': session_time,
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'needs_response_details': 1,
			'sent_at': attribution_sent,
			'initiated_by':	initiated_by
			}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	
def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=None,partner_params=None,t1=0,t2=0):
	url = 'http://app.adjust.com/event'
	method = 'post'
	headers = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': 'ru',
				'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
			}
			
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	data = {

			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'hardware_name':device_data.get('hardware'),	
			'event_buffering_enabled':	0,
			'cpu_type':device_data.get('cpu_type'),
			'attribution_deeplink': '1',
			'os_name':'ios',
			'environment':'production',
			'needs_response_details':1,
			'event_count':app_data.get('event_count'),
			'session_count':str(app_data.get('session_count')),
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			'created_at': created_at,
			'connectivity_type':	'2',
			'event_token':event_token,
			'bundle_id':campaign_data.get('package_name'),
			'subsession_count':app_data.get('subsession_count'),
			'os_version':device_data.get('os_version'),
			'app_version': campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country'),
			'language':	device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': '1',
			'device_name':device_data.get('device_platform'),
			'sent_at': sent_at,
			'os_build':	device_data.get('build'),
			'install_receipt': app_data.get('install_receipt'),
			'mcc':device_data.get('mcc'),
			'mnc':device_data.get('mnc'),
			}
	time_spent= int(time.time())-app_data.get('adjust_call_time')
	data['session_length']=time_spent
	data['time_spent']=time_spent
	if callback_params:
		data['callback_params']	= callback_params
	if partner_params:
		data['partner_params']	= partner_params
	if app_data.get('event_count')==1:
		inc_(app_data,'subsession_count')
		data['subsession_count']=app_data.get('subsession_count')

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':None, 'data': data}	
	
###########################################################
#						Self Calls						  #
###########################################################	
	
###########################################################
#						UTIL							  #
###########################################################

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_ts(app_data,device_data,get_ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(get_ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_auth(app_data,activity_kind,idfa,created_at):
	final_str=campaign_data.get('adjust').get('secret_key')+str(activity_kind)+str(idfa)+str(created_at)
	sign=util.sha256(final_str)
	auth='Signature secret_id="'+str(campaign_data.get('adjust').get('secret_id'))+'",signature="'+str(sign)+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'
	return auth

def apple_receipt(app_data):
	if not app_data.get('install_receipt'):
		app_data['install_receipt']='MIISqgYJKo'+util.get_random_string('google_token',random.randint(6000,6500)).replace('-','+').replace('_','/')
