import random,time,datetime,uuid,json,urllib
from sdk import NameLists
from sdk import getsleep
from sdk import util,purchase,installtimenew
import clicker,Config

campaign_data = {
	'package_name' : 'com.lykan.LuxuryCloset',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'ctr':6,
	'app_name' : 'The Luxury Closet - Buy & Sell Authentic Luxury Items',
	'app_id':'1085470991',
	'app_version_code': '708',#'699',#'657',#'655',#621, '612',#592, '580',#'565',#'516',#'498',#'493',#'458',#'415',  #'399',#'''383',#'339',#'319',#'306', 368, 408
	'supported_os':'9.0',
	'app_version_name': '1.22.12',#'1.22.11',#'1.22.10',#'1.22.9',#1.22.6, '1.22.5',#1.22.4, '1.22.3',#'1.22.2',#'1.21.0',#'1.20.1',#'1.20.0',#'1.19.7',#'1.19.4',#'1.19.2',#'1.19.1',#'1.18.2',#'1.18.1',#'1.18.0', 1.19.0, 1.19.3
	'supported_countries': 'WW',
	'app_size' : 47.9,#48.0,#47.9,#48.3,#47.5,47.6
	'device_targeting':True,
	'tracker':'adjust',
	'sdk' : 'ios',
	'adjust':{
				'app_token': 'vew3e2hygow0',
				'sdk':'ios4.18.2',#'ios4.17.2',#'ios4.17.1',#'ios4.11.5',
				'secret_key': '204934370319811266991752037436280949207',#'1549409519174765993928681158529458047',#'11738492639863623214114421721407385720',#'811305082735079550548526352517687643',     #97190823175712867410097827301609069251
				'secret_id': '6',#'4',#'3',#'2', #1
			},
	'gmp_version':'3900',		
	'app_updated_at': '2019-10-26T15:22:02.000Z',#'2019-10-02T14:07:26.000Z',#'2019-08-02T10:08:43.000Z',#'2019-07-31T17:14:29.000Z',#2019-07-02T20:26:52.000Z, '2019-06-21T10:41:42.000Z',#2019-06-04T14:27:56.000Z, '2019-05-20T17:46:22.000Z',#'2019-04-11T12:12:43.000Z',#'2019-01-28T17:47:35.000Z',#'2019-01-08T09:37:42.000Z',#'2018-12-28T15:58:09.000Z',#'2018-12-01T15:48:53.000Z',#'2018-11-08T18:11:22.000Z',#'2018-10-15T18:55:04.000Z',#'2018-09-13T09:43:48.000Z',#'2018-08-20T13:50:35.000Z',#'2018-08-06T11:43:23.000Z',
	'retention':{
					1:65,
					2:63,
					3:60,
					4:58,
					5:55,
					6:53,
					7:50,
					8:48,
					9:45,
					10:43,
					11:40,
					12:38,
					13:35,
					14:32,
					15:30,
					16:28,
					17:26,
					18:23,
					19:21,
					20:20,
					21:19,
					22:18,
					23:17,
					24:16,
					25:15,
					26:14,
					27:13,
					28:12,
					29:11,
					30:10,
					31:9,
					32:8,
					33:7,
					34:6,
					35:5,
	}
}

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	
	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')
		
def make_subsession_count(app_data,device_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 1
	else:
		app_data['subsession_count'] += 1
		
	return app_data.get('subsession_count')

	

###########################################################
#						INSTALL							  #
###########################################################
def install(app_data, device_data):
	print 'plz wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)

	time.sleep(random.randint(30,60))	
	print '*************Install**********************'

	###########		Initialize		############
	global idList,categoryIDlist,countryCode,dialCode,PHPSESSID
	PHPSESSID=False
	idList = []
	categoryIDlist=[]
	# categoryID=''
	countryCode=''
	dialCode=''
	global amount,productDetail
	amount=''
	productDetail=[]
	app_data['added_to_cart'] = False
	app_data['registered'] = False
	# if not app_data.get('installed_at'):
	# 	app_data['installed_at'] = get_date(app_data,device_data,early=random.uniform(230,250))

	app_data['adjust_begin_time'] = int(time.time())	
			
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)

	if not app_data.get('install_receipt'):
		app_data['install_receipt'] = "MIISawYJKoZIhvc"+ util.get_random_string('google_token',6277)

	# if not app_data.get('make_dpi'):
	# 	app_data['make_dpi'] = device_data.get('dpi') if device_data.get('dpi') else '326'
	
	###########			Calls		############   

	print "\n---------------------------luxury_closet_brands------------------------------------"
	request = luxury_closet_brands(campaign_data, app_data, device_data)
	result=util.execute_request(**request)
	try:
		catList=json.loads(result.get('data'))
		for i in range(len(catList)):
			if catList[i].get('category_ids'):
				catIds = catList[i].get('category_ids').split(',')
			categoryIDlist.extend(catIds)
	except:
		categoryIDlist=getCategoryIDList()
	if len(categoryIDlist)<1:
		categoryIDlist=getCategoryIDList()

	print "\n---------------------------luxury_closet_COUNTRY_code------------------------------------"
	request = luxury_closet_COUNTRY_code(campaign_data, app_data, device_data)
	result=util.execute_request(**request)
	try:
		countryIDLIST=json.loads(result.get('data'))
		for i in range(len(countryIDLIST)):
			if countryIDLIST[i].get('country_code')==device_data.get('locale').get('country'):
				countryCode = countryIDLIST[i].get('id')
				dialCode = countryIDLIST[i].get('dial_code')
	except:
		countryCode='1'
		dialCode='+1'

	set_sessionLength(app_data,forced=True,length=0)
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)
	
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)


	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data, source="deeplink")
	util.execute_request(**adjustSession)


	time.sleep(random.randint(2,5))

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	request = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)

	print "Measurement config call--------------\n\n"
	request=configcall(app_data,campaign_data,device_data)
	util.execute_request(**request)
	
	if random.randint(1,100)<=90:
		print "\n---------------------------luxury_closet_register------------------------------------"
		request = luxury_closet_register(campaign_data, app_data, device_data)
		result=util.execute_request(**request)
		try:
			response=json.loads(result.get('data'))
			app_data['access_token'] = response.get('access-token')
			app_data['userID'] = response.get('information').get('user_id')
			PHPSESSID=result.get('res').headers.get('Set-Cookie').split('PHPSESSID=')[1].split(';')[0]
		except:
			app_data['access_token'] = 'IqsWjaukaxxhxj5ljvA-9v2bWm1uK0Cf'
			app_data['userID'] = '580177'
			PHPSESSID = '1f4rk545ruk1c6l31dg2daerb4'
		
		time.sleep(random.randint(2,7))

		sign_up(campaign_data, app_data, device_data)
		app_data['registered'] = True
		time.sleep(random.randint(2,7))

		print "\n---------------------------luxury_closet_products_List------------------------------------"
		request = luxury_closet_products_List(campaign_data, app_data, device_data)
		result=util.execute_request(**request)
		try:
			response=json.loads(result.get('data'))
			itmeList = response.get('items')
			for i in range(len(itmeList)):
				idList.append(itmeList[i].get('id'))
			app_data['item_id'] = random.choice(idList)			
		except:
			try:				
				j=0
				while(len(idList)==0):
					if j==5:
						break

					print "\n---------------------------luxury_closet_products_List------------------------------------"
					request = luxury_closet_products_List(campaign_data, app_data, device_data)
					result=util.execute_request(**request)
					for i in range(len(itmeList)):
						idList.append(itmeList[i].get('id'))
					j=j+1
				app_data['item_id'] = random.choice(idList)
			except:	
				idList = [131778,131773,121234,40450,46451,40350,98634,73519,61668,51917,46453,18829,19247,36456,38091]
				if len(idList)<1:
					idList = [131778,131773,121234,40450,46451,40350,98634,73519,61668,51917,46453,18829,19247,36456,38091]
				app_data['item_id'] = random.choice(idList)


		print "\n---------------------------luxury_closet_product------------------------------------"
		request =luxury_closet_products(campaign_data, app_data, device_data)
		util.execute_request(**request)

		if random.randint(1,100)<=78:

			for i in range(random.randint(1,3)):
				app_data['item_id'] = random.choice(idList)
				print "\n---------------------------luxury_closet_add_to_cart------------------------------------"
				request =luxury_closet_add_to_cart(campaign_data, app_data, device_data)
				result=util.execute_request(**request)

				try:
					response=json.loads(result.get('data'))
					product = response.get('message').get('items')[0]
					product_name = product.get('name')
					amount = product.get('price_tlc')
					product_price = str(amount.split('.')[0])+'.000000'
					if product.get('category_name_full'):
						product_category = product.get('category_name_full').split('> ')[0]
					else:
						product_category = " "
					productSplited = product_name.split(' ')
					productVariant = productSplited[random.choice([2,2,2,1,1])]
					for j in range(len(productSplited)):
						if productSplited[j]=="White" or productSplited[j]=="Silver" or productSplited[j]=="Gold" or productSplited[j]=="Black" or productSplited[j]=="Red" or productSplited[j]=="Blue" or productSplited[j]=="Grey" or productSplited[j]=="Yellow" or productSplited[j]=="Orange":
							productVariant = productSplited[j]
					productDetail=[product_name,product_price,product_category,productVariant]
					print "--------------------------------------------"
					print productDetail
					# time.sleep(5)
					if len(productDetail)<1:
						productDetail = [u'Dior Black Leather Large Lady Dior Tote', '1410.000000', ' ', u'Black']
						amount = '1410.35'

				
				except:
					app_data['item_id'] = random.choice(idList)
					request =luxury_closet_add_to_cart(campaign_data, app_data, device_data)
					result=util.execute_request(**request)
					try:
						response=json.loads(result.get('data'))
						product = response.get('message').get('items')[0]
						product_name = product.get('name')
						amount = product.get('price_tlc')
						product_price = str(amount.split('.')[0])+'.000000'
						if product.get('category_name_full'):
							product_category = product.get('category_name_full').split('> ')[0]
						else:
							product_category = " "
						productSplited = product_name.split(' ')
						productVariant = productSplited[random.choice([2,2,2,1,1])]
						for j in range(len(productSplited)):
							if productSplited[j]=="White" or productSplited[j]=="Silver" or productSplited[j]=="Gold" or productSplited[j]=="Black" or productSplited[j]=="Red" or productSplited[j]=="Blue" or productSplited[j]=="Grey" or productSplited[j]=="Yellow" or productSplited[j]=="Orange":
								productVariant = productSplited[j]
						productDetail=[product_name,product_price,product_category,productVariant]
						print "--------------------------------------------"
						print productDetail
						# time.sleep(5)
						if len(productDetail)<1:
							productDetail = [u'Dior Black Leather Large Lady Dior Tote', '1410.000000', ' ', u'Black']
							amount = '1410.35'

					except:
						productDetail = [u'Dior Black Leather Large Lady Dior Tote', '1410.000000', ' ', u'Black']
						amount = '1410.35'

				time.sleep(random.randint(10,30))
				content_view(campaign_data, app_data, device_data)
				if random.randint(1,100) <= 85:
					time.sleep(random.randint(20,40))
					add_to_cart(campaign_data, app_data, device_data)
					app_data['added_to_cart'] = True

	if random.randint(1,100)<=10:

		time.sleep(random.randint(100,180))
		sell_any_item(campaign_data, app_data, device_data)

	if app_data.get('added_to_cart')==True:

		if purchase.isPurchase(app_data,day=1, advertiser_demand=4):

			if len(productDetail)<1:
				productDetail = [u'Dior Black Leather Large Lady Dior Tote', '1410.000000', ' ', u'Black']
				amount = '1410.35'

			print "\n---------------------------luxury_closet_checkout------------------------------------"
			request =luxury_closet_checkout(campaign_data, app_data, device_data)
			result=util.execute_request(**request)
			try:
				response=json.loads(result.get('data'))
				app_data['vat'] = str(response.get('vat')).split('.')[0]
				app_data['shipping_fee'] = response.get('shipping_fee')
			except:
				app_data['vat']='70'
				app_data['shipping_fee']='0'

			print "\n---------------------------luxury_closet_checkout_create------------------------------------"
			request =luxury_closet_checkout_create(campaign_data, app_data, device_data, app_data.get('vat'))
			result=util.execute_request(**request)
			try:
				response=json.loads(result.get('data'))
				app_data['order_id'] = response.get('order_id')
			except:
				app_data['order_id'] = '83647'

			time.sleep(random.randint(2,7))

			purchase_cod(campaign_data, app_data, device_data,app_data.get('vat'),app_data.get('shipping_fee'),app_data.get('order_id'))

			purchase_cod_2(campaign_data, app_data, device_data,app_data.get('vat'),app_data.get('shipping_fee'),app_data.get('order_id'))

			# if random.randint(1,100)<=10:
			after_purchase(campaign_data, app_data, device_data,app_data.get('vat'),app_data.get('shipping_fee'),app_data.get('order_id'))

	###########		Finalize	############
	set_appCloseTime(app_data)

	return {'status':True}

###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):	
	
	###########		Initialize		############
	# app_data['total_time']=1

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)
		time.sleep(random.randint(30,60))	


	global idList,categoryIDlist,countryCode,dialCode,PHPSESSID
	PHPSESSID=False
	idList = []
	categoryIDlist=[]
	# categoryID=''
	countryCode=''
	dialCode=''
	global amount,productDetail
	amount=''
	productDetail=[]
	if not app_data.get('added_to_cart'):
		app_data['added_to_cart'] = False
	if not app_data.get('registered'):
		app_data['registered'] = False
	# if not app_data.get('installed_at'):
	# 	app_data['installed_at'] = get_date(app_data,device_data,early=random.uniform(230,250))

	if not app_data.get('adjust_begin_time'):
		app_data['adjust_begin_time'] = int(time.time())

	if not app_data.get('appCloseTime'):
		app_data['appCloseTime'] = int(time.time())

	app_data['sess_len'] = app_data.get('appCloseTime')-app_data.get('adjust_begin_time')	
	app_data['adjust_begin_time'] = int(time.time())		
			
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)

	if not app_data.get('install_receipt'):
		app_data['install_receipt'] = "MIISawYJKoZIhvc"+ util.get_random_string('google_token',6277)

	# if not app_data.get('make_dpi'):
	# 	app_data['make_dpi'] = device_data.get('dpi') if device_data.get('dpi') else '326'
	
	###########			Calls		############   

	print "\n---------------------------luxury_closet_brands------------------------------------"
	request = luxury_closet_brands(campaign_data, app_data, device_data)
	result=util.execute_request(**request)
	try:
		catList=json.loads(result.get('data'))
		for i in range(len(catList)):
			if catList[i].get('category_ids'):
				catIds = catList[i].get('category_ids').split(',')
			categoryIDlist.extend(catIds)
	except:
		categoryIDlist=getCategoryIDList()
	if len(categoryIDlist)<1:
		categoryIDlist=getCategoryIDList()

	print "\n---------------------------luxury_closet_COUNTRY_code------------------------------------"
	request = luxury_closet_COUNTRY_code(campaign_data, app_data, device_data)
	result=util.execute_request(**request)
	try:
		countryIDLIST=json.loads(result.get('data'))
		for i in range(len(countryIDLIST)):
			if countryIDLIST[i].get('country_code')==device_data.get('locale').get('country'):
				countryCode = countryIDLIST[i].get('id')
				dialCode = countryIDLIST[i].get('dial_code')
	except:
		countryCode='1'
		dialCode='+1'

	set_sessionLength(app_data,forced=True,length=0)
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,type='open')
	util.execute_request(**adjustSession)

	time.sleep(random.randint(2,5))

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	print "Measurement config call--------------\n\n"
	request=configcall(app_data,campaign_data,device_data)
	util.execute_request(**request)
	
	if app_data.get('registered')==False:
		print "\n---------------------------luxury_closet_register------------------------------------"
		request = luxury_closet_register(campaign_data, app_data, device_data)
		result=util.execute_request(**request)
		try:
			response=json.loads(result.get('data'))
			app_data['access_token'] = response.get('access-token')
			app_data['userID'] = response.get('information').get('user_id')
			PHPSESSID=result.get('res').headers.get('Set-Cookie').split('PHPSESSID=')[1].split(';')[0]
		except:
			app_data['access_token'] = 'IqsWjaukaxxhxj5ljvA-9v2bWm1uK0Cf'
			app_data['userID'] = '580177'
			PHPSESSID = '1f4rk545ruk1c6l31dg2daerb4'
		time.sleep(random.randint(2,7))

		sign_up(campaign_data, app_data, device_data)
		app_data['registered'] = True

	if app_data.get('registered')==True:
		print "\n---------------------------luxury_closet_login------------------------------------"
		request = luxury_closet_login(campaign_data, app_data, device_data)
		result=util.execute_request(**request)
		try:
			response=json.loads(result.get('data'))
			if not app_data.get('access_token'):
				app_data['access_token'] = response.get('access-token')
				app_data['userID'] = response.get('information').get('user_id')
		except:
			app_data['access_token'] = 'IqsWjaukaxxhxj5ljvA-9v2bWm1uK0Cf'
			app_data['userID'] = '580177'

		time.sleep(random.randint(2,7))
		print "\n---------------------------luxury_closet_products_List------------------------------------"
		request = luxury_closet_products_List(campaign_data, app_data, device_data)
		result=util.execute_request(**request)
		try:
			
			response=json.loads(result.get('data'))
			itmeList = response.get('items')
			for i in range(len(itmeList)):
				idList.append(itmeList[i].get('id'))
			app_data['item_id'] = random.choice(idList)	
		except:
			try:				
				j=0
				while(len(idList)==0):
					if j==5:
						break

					print "\n---------------------------luxury_closet_products_List------------------------------------"
					request = luxury_closet_products_List(campaign_data, app_data, device_data)
					result=util.execute_request(**request)
					for i in range(len(itmeList)):
						idList.append(itmeList[i].get('id'))
					j=j+1
				app_data['item_id'] = random.choice(idList)
			except:	
				idList = [131778,131773,121234,40450,46451,40350,98634,73519,61668,51917,46453,18829,19247,36456,38091]
				if len(idList)<1:
					idList = [131778,131773,121234,40450,46451,40350,98634,73519,61668,51917,46453,18829,19247,36456,38091]
				app_data['item_id'] = random.choice(idList)

		print "\n---------------------------luxury_closet_product------------------------------------"
		request =luxury_closet_products(campaign_data, app_data, device_data)
		util.execute_request(**request)

		if random.randint(1,100)<=70:
			if day<=4:
				varr=random.randint(1,3)
			else:
				varr=random.randint(1,2)

			for i in range(varr):
				if random.randint(1,100)<=70:
					app_data['item_id'] = random.choice(idList)
					print "\n---------------------------luxury_closet_add_to_cart------------------------------------"
					request =luxury_closet_add_to_cart(campaign_data, app_data, device_data)
					result=util.execute_request(**request)
					try:
						response=json.loads(result.get('data'))
						product = response.get('message').get('items')[0]
						product_name = product.get('name')
						amount = product.get('price_tlc')
						product_price = str(amount.split('.')[0])+'.000000'
						if product.get('category_name_full'):
							product_category = product.get('category_name_full').split('> ')[0]
						else:
							product_category = " "
						productSplited = product_name.split(' ')
						productVariant = productSplited[random.choice([2,2,2,1,1])]
						for j in range(len(productSplited)):
							if productSplited[j]=="White" or productSplited[j]=="Silver" or productSplited[j]=="Gold" or productSplited[j]=="Black" or productSplited[j]=="Red" or productSplited[j]=="Blue" or productSplited[j]=="Grey" or productSplited[j]=="Yellow" or productSplited[j]=="Orange":
								productVariant = productSplited[j]
						productDetail=[product_name,product_price,product_category,productVariant]
						print "--------------------------------------------"
						print productDetail
						# time.sleep(5)
						if len(productDetail)<1:
							productDetail = [u'Dior Black Leather Large Lady Dior Tote', '1410.000000', ' ', u'Black']
							amount = '1410.35'
					
					except:
						app_data['item_id'] = random.choice(idList)
						request =luxury_closet_add_to_cart(campaign_data, app_data, device_data)
						result=util.execute_request(**request)
						try:
							response=json.loads(result.get('data'))
							product = response.get('message').get('items')[0]
							product_name = product.get('name')
							amount = product.get('price_tlc')
							product_price = str(amount.split('.')[0])+'.000000'
							if product.get('category_name_full'):
								product_category = product.get('category_name_full').split('> ')[0]
							else:
								product_category = " "
							productSplited = product_name.split(' ')
							productVariant = productSplited[random.choice([2,2,2,1,1])]
							for j in range(len(productSplited)):
								if productSplited[j]=="White" or productSplited[j]=="Silver" or productSplited[j]=="Gold" or productSplited[j]=="Black" or productSplited[j]=="Red" or productSplited[j]=="Blue" or productSplited[j]=="Grey" or productSplited[j]=="Yellow" or productSplited[j]=="Orange":
									productVariant = productSplited[j]
							productDetail=[product_name,product_price,product_category,productVariant]
							print "--------------------------------------------"
							print productDetail
							# time.sleep(5)
							if len(productDetail)<1:
								productDetail = [u'Dior Black Leather Large Lady Dior Tote', '1410.000000', ' ', u'Black']
								amount = '1410.35'

						except:
							productDetail = [u'Dior Black Leather Large Lady Dior Tote', '1410.000000', ' ', u'Black']
							amount = '1410.35'

					time.sleep(random.randint(10,30))
					content_view(campaign_data, app_data, device_data)
					if random.randint(1,100) <= 85:
						time.sleep(random.randint(20,40))
						add_to_cart(campaign_data, app_data, device_data)
						app_data['added_to_cart'] = True

		if random.randint(1,100)<=10:
			time.sleep(random.randint(100,180))
			sell_any_item(campaign_data, app_data, device_data)

	if app_data.get('added_to_cart')==True:

		if purchase.isPurchase(app_data,day=day, advertiser_demand=26):

			if len(productDetail)<1:
				productDetail = [u'Dior Black Leather Large Lady Dior Tote', '1410.000000', ' ', u'Black']
				amount = '1410.35'

			print "\n---------------------------luxury_closet_checkout------------------------------------"
			request =luxury_closet_checkout(campaign_data, app_data, device_data)
			result=util.execute_request(**request)
			try:
				response=json.loads(result.get('data'))
				app_data['vat'] = str(response.get('vat')).split('.')[0]
				app_data['shipping_fee'] = response.get('shipping_fee')
			except:
				app_data['vat']='70'
				app_data['shipping_fee']='0'

			print "\n---------------------------luxury_closet_checkout_create------------------------------------"
			request =luxury_closet_checkout_create(campaign_data, app_data, device_data, app_data.get('vat'))
			result=util.execute_request(**request)
			try:
				response=json.loads(result.get('data'))
				app_data['order_id'] = response.get('order_id')
			except:
				app_data['order_id'] = '83647'

			time.sleep(random.randint(2,7))

			purchase_cod(campaign_data, app_data, device_data,app_data.get('vat'),app_data.get('shipping_fee'),app_data.get('order_id'))

			purchase_cod_2(campaign_data, app_data, device_data,app_data.get('vat'),app_data.get('shipping_fee'),app_data.get('order_id'))

			# if random.randint(1,100)<=10:
			after_purchase(campaign_data, app_data, device_data,app_data.get('vat'),app_data.get('shipping_fee'),app_data.get('order_id'))

	###########		Finalize	############
	set_appCloseTime(app_data)

	return {'status':True}
	

def sign_up(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT____________________________sign_up'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)
	partner_params=json.dumps({"label":"Email Registration","ios_lang":device_data.get('locale').get('language'),"user_id":app_data.get('userID'),"category":"Registration","action":"Register button"})
	request=adjust_event(campaign_data, app_data, device_data,'n8lizh',partner_params=partner_params)
	util.execute_request(**request)

def add_to_cart(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT__________________________add_to_cart'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)
	partner_params=json.dumps({"product_variant":productDetail[3],
				"product_quantity":"1",
				"currencyCode":"USD",
				"product_price":productDetail[1],
				"product_category":productDetail[2],
				"product_brand":productDetail[0].split(' ')[0],
				"ios_lang":device_data.get('locale').get('language'),
				"product_name":productDetail[0]})
	request=adjust_event(campaign_data, app_data, device_data,'e7g4m3',partner_params=partner_params)
	util.execute_request(**request)

def content_view(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT__________________________content_view'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	if app_data.get('item_id'):
		item_id = str(app_data.get('item_id'))
	else:
		item_id = util.get_random_string('decimal',5)

	if app_data.get('registered'):
		user_id = str(app_data.get('userID'))
	else:
		user_id = util.get_random_string('decimal',6)

	partner_params = json.dumps({"quantity":"1","id":item_id,"user_id":user_id,"name":productDetail[0],"price":productDetail[1],"category":productDetail[2]})

	request=adjust_event(campaign_data, app_data, device_data,'wm88a1',partner_params=partner_params)
	util.execute_request(**request)

def purchase_cod(campaign_data, app_data, device_data,vat,shipping_fee,order_id):
	print '\nAdjust : EVENT__________________________purchase_cod'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)
	partner_params=json.dumps({"id":order_id,"ios_lang":device_data.get('locale').get('language'),"tax":"$"+str(vat),"shipping":"$"+str(shipping_fee)})
	request=adjust_event(campaign_data, app_data, device_data,'fffk92',partner_params=partner_params)
	util.execute_request(**request)	

def purchase_cod_2(campaign_data, app_data, device_data,vat,shipping_fee,order_id):
	print '\nAdjust : EVENT__________________________purchase_cod_2'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	if app_data.get('registered'):
		user_id = str(app_data.get('userID'))
	else:
		user_id = util.get_random_string('decimal',6)

	partner_params=json.dumps({"id":str(order_id),"user_id":user_id,"idfa":app_data.get('idfa_id')})
	request=adjust_event(campaign_data, app_data, device_data,'be2xkq',partner_params=partner_params)
	util.execute_request(**request)	

def after_purchase(campaign_data, app_data, device_data,vat,shipping_fee,order_id):
	print '\nAdjust : EVENT__________________________after_purchase'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)
	partner_params=json.dumps({"id":order_id,"ios_lang":device_data.get('locale').get('language'),"tax":str(vat)+" KWD","shipping":str(shipping_fee)+" KWD"})
	request=adjust_event(campaign_data, app_data, device_data,'lz1las',partner_params=partner_params)
	util.execute_request(**request)	

def sell_any_item(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT________________________sell_any_item'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)
	partner_params=json.dumps({"action":"Finish item Submission","ios_lang":device_data.get('locale').get('language')})
	request=adjust_event(campaign_data, app_data, device_data,'vfg9qw',partner_params=partner_params)
	util.execute_request(**request)	
###########################################################
#						SELF CALLS						  #
###########################################################

def luxury_closet_brands(campaign_data, app_data, device_data):
	url = 'http://api.theluxurycloset.com/ios/api/web/v2/brands'
	method = 'get'
	headers = {
		'Content-Language': device_data.get('locale').get('language'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1',
		'Accept-Encoding': 'gzip, deflate',
		'Content-Type': 'application/json',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
		'ios_app': 'Platform',
		'Content-Country': device_data.get('locale').get('country')
	}
	data=None

	params = {'type':'all'}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def	luxury_closet_COUNTRY_code(campaign_data, app_data, device_data):
	url = 'http://api.theluxurycloset.com/ios/api/web/v2/countries'
	method = 'get'
	headers = {
		'Content-Language': device_data.get('locale').get('language'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1',
		'Accept-Encoding': 'gzip, deflate',
		'Content-Type': 'application/json',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
		'ios_app': 'Platform',
		'Content-Country': device_data.get('locale').get('country')
	}
	data=None

	params = {'type':'all'}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def luxury_closet_register(campaign_data, app_data, device_data):
	register_user(app_data,country='united states')
	url = 'http://api.theluxurycloset.com/ios/api/web/v2/users/register'
	method = 'post'
	headers = {
		'Content-Language': device_data.get('locale').get('language'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1',
		'Accept-Encoding': 'gzip, deflate',
		'Content-Type': 'application/json',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
		'ios_app': 'Platform',
		'Content-Country': device_data.get('locale').get('country')
	}
	
	data={"email":app_data.get('user').get('email'),"gender":app_data.get('user').get('sex'),"password":app_data.get('user').get('password'),"email_promo":True,"mobile_user_id":app_data.get('user').get('userid')}

	params = None
	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}		

def luxury_closet_login(campaign_data, app_data, device_data):
	url = 'http://api.theluxurycloset.com/ios/api/web/v2/users/login'
	method = 'post'
	headers = {
		'Content-Language': device_data.get('locale').get('language'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1',
		'Accept-Encoding': 'gzip, deflate',
		'Content-Type': 'application/json',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
		'ios_app': 'Platform',
		'Content-Country': device_data.get('locale').get('country')
	}
	
	data={"mobile_user_id":app_data.get('user').get('userid'),"username":app_data.get('user').get('email'),"password":app_data.get('user').get('password')}

	params = None
	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def luxury_closet_products_List(campaign_data, app_data, device_data):
	url = 'http://api.theluxurycloset.com/ios/api/web/v2/products'
	method = 'get'
	headers = {
		'Content-Language': device_data.get('locale').get('language'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1',
		'Accept-Encoding': 'gzip, deflate',
		'Content-Type': 'application/json',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
		'ios_app': 'Platform',
		'Content-Country': device_data.get('locale').get('country')
	}
	if PHPSESSID:
		headers['Cookie'] = 'PHPSESSID='+PHPSESSID

	data=None

	params = {
	'access-token':app_data.get('access_token'),
	'brands': random.choice(categoryIDlist),#102,
	'only_available':0,
	'page':1
}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}	

def luxury_closet_products(campaign_data, app_data, device_data):
	url = 'http://api.theluxurycloset.com/ios/api/web/v2/products/'+str(app_data.get('item_id'))
	method = 'get'
	headers = {
		'Content-Language': device_data.get('locale').get('language'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1',
		'Accept-Encoding': 'gzip, deflate',
		'Content-Type': 'application/json',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
		'ios_app': 'Platform',
		'Content-Country': device_data.get('locale').get('country')
	}
	if PHPSESSID:
		headers['Cookie'] = 'PHPSESSID='+PHPSESSID

	data=None

	params = {
	'access-token':app_data.get('access_token'),
}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}	

def luxury_closet_add_to_cart(campaign_data, app_data, device_data):
	url = 'http://api.theluxurycloset.com/ios/api/web/v2/carts'
	method = 'post'
	headers = {
		'Content-Language': device_data.get('locale').get('language'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1',
		'Accept-Encoding': 'gzip, deflate',
		'Content-Type': 'application/json',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
		'ios_app': 'Platform',
		'Content-Country': device_data.get('locale').get('country')
	}
	if PHPSESSID:
		headers['Cookie'] = 'PHPSESSID='+PHPSESSID

	data={"product_id":app_data.get('item_id'),"quantity":1,"currency":"USD","is_vat":0,"installments":0,"country_id":countryCode}

	params = {'access-token':app_data.get('access_token')}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def	luxury_closet_checkout(campaign_data, app_data, device_data):
	url = 'http://api.theluxurycloset.com/ios/api/web/v2/checkouts/shipping'
	method = 'get'
	headers = {
		'Content-Language': device_data.get('locale').get('language'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1',
		'Accept-Encoding': 'gzip, deflate',
		'Content-Type': 'application/json',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
		'ios_app': 'Platform',
		'Content-Country': device_data.get('locale').get('country')
	}
	if PHPSESSID:
		headers['Cookie'] = 'PHPSESSID='+PHPSESSID

	data=None

	params = {
			'product_ids':	app_data.get('item_id'),
			'payment_method':	'cash_on_location',
			'delivery_method':	'pickup',
			'country_id':	countryCode,
			'sub_total':	amount,
			'access-token':	app_data.get('access_token')
			}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def luxury_closet_checkout_create(campaign_data, app_data, device_data, vat=False):
	url = 'http://api.theluxurycloset.com/ios/api/web/v2/checkouts/create'
	method = 'post'
	headers = {
		'Content-Language': device_data.get('locale').get('language'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1',
		'Accept-Encoding': 'gzip, deflate',
		'Content-Type': 'application/json',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
		'ios_app': 'Platform',
		'Content-Country': device_data.get('locale').get('country')
	}
	if PHPSESSID:
		headers['Cookie'] = 'PHPSESSID='+PHPSESSID

	data={"OrderBilling":{"firstname":app_data.get('user').get('firstname'),
		 "lastname":app_data.get('user').get('lastname'),
		 "country_id":countryCode,
		 "address":util.get_random_string(random.randint(5,15)),
		 "state_province":util.get_random_string(random.randint(5,15)),
		 "city":util.get_random_string(random.randint(5,15)),
		 "postcode":app_data.get('user').get('postCode'),
		 "email":app_data.get('user').get('email'),
		 "dial_code":dialCode,
		 "phone":app_data.get('user').get('Mobile_number'),
		 "different_delivery":"0"},
		 "data": {"Product":{"id":[app_data.get('item_id')]},
		 "total_amount":amount.split('.')[0],
		 "currency":"USD",
		 "delivery_method":"pick_up",
		 "payment_method":"cash_on_location",
		 "tax":"0",
		 "duty_amount":"0",
		 "vat":vat,
		 "app_version":campaign_data.get('app_version_name'),
		 "country_code":device_data.get('locale').get('country')} }

	params = {
			'access-token':	app_data.get('access_token')
			}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}


###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,type='install'):
	app_data['subsession_count'] = 1
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+ '/' + campaign_data.get('app_version_code') +' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	
	params={}

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'connectivity_type':	'2',
			'created_at': get_date(app_data,device_data,early=random.uniform(1,3)),
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			# 'ios_uuid':	app_data.get('ios_uuid'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'sent_at': get_date(app_data,device_data),
			'session_count': make_session_count(app_data,device_data),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('app_updated_at')+device_data.get('timezone'),
			'installed_at': get_date_ts(app_data,device_data,early=0, ts = app_data.get('times').get('install_complete_time')),
			'install_receipt':app_data.get('install_receipt'),
			'os_build':	device_data.get('build'),
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'mcc':device_data.get('mcc'),
			'mnc': device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyHSDPA',
			# 'queue_size':'2',
			# 'tce':	1,
			}
	
	if type == "open":
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = app_data.get('sess_len')
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = app_data.get('sess_len')
	
	
	# app_data['time_passes'] = int(time.time()*1000)
	headers['Authorization']=get_signature(campaign_data, app_data, device_data,app_secret=str(campaign_data.get('adjust').get('secret_key')),action='session',idfa=str(data.get('idfa')),created_at=str(data.get('created_at')),)

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adjust_sdkclick(campaign_data, app_data, device_data, source=None):
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/' + campaign_data.get('app_version_code') +' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	click=get_date(app_data,device_data)
	params={}

	data = {
			'details': '{"Version3.1":{"iad-attribution":"false"}}',
			'source': 'iad3',
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': get_date(app_data,device_data,early=random.uniform(1,3)),
			'connectivity_type':	'2',
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			# 'ios_uuid':	app_data.get('ios_uuid'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'sent_at': get_date(app_data,device_data),
			# 'session_count': app_data.get('session_count','1'),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('app_updated_at')+device_data.get('timezone'),
			'installed_at': get_date_ts(app_data,device_data,early=0, ts = app_data.get('times').get('install_complete_time')),
			'install_receipt':app_data.get('install_receipt'),
			'os_build':	device_data.get('build'),
			# 'tce':	1,
			# 'last_interval': 0,
			# 'session_length': 0,
			# 'subsession_count': 1,
			# 'time_spent' : 1,
			# 'persistent_ios_uuid':app_data.get('ios_uuid'),
			'mcc':device_data.get('mcc'),
			'mnc': device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyHSDPA',
			}

	if source=='deeplink':
		data['click_time']= click
		data['source']= source
		data['deeplink'] = 'fb826698920721438://authorize/#state=%7B%22challenge%22%3A%22a8%252FavznygZWr2W%252F3D4Zs0pGEp2E%253D%22%2C%220_auth_logger_id%22%3A%229D60A0A5-DCEE-4384-B9A8-1367ED83C24E%22%2C%22com.facebook.sdk_client_state%22%3Atrue%2C%223_method%22%3A%22sfvc_auth%22%7D&granted_scopes=email%2Cpublic_profile&denied_scopes=&signed_request=uc6mrGBmEqGNrbK6DNflxa_nY7bBDU5lHY31vpYTEDg.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUURwbmJGdHlnWjRubzVqcW9Ha1NNeFNIOTRWTVBXd2dPY3ZvNnNuanNCazA2M2tabkprWFBURnI0X2tHUnlEbnVJVVRPc1pzN01PQzBSeDNiZTJhMVh5MV9nVkszMXk1cEFuWUl1VWsyQVBPX01VbUJpeVVDNU9ZZ3BGR0xJVkJfUWk2MWF2LWxIMGlEa3FqdGh3a3EwWW0wSUxCWklhYUpkRUppQXJqOHppSnd2ZU1UT0l4c1pDTlVaR3RSQzZobm1RVzJQTkNCRUR0bGJTU2JGQldlcTJiMVVGV0I1SkxiVk5LeC1Cb2RVVFQ0bGF4NlNIYjJJajBmUkg0Mkp6S240R1ZpZFUteFlKT1pkWjNJYWdvWHkxV1QtUVAwQzVRVG81blJDeWtfd0VNaEtNdHJIX0l1RGM5NG90RUJuWGVCWE1UbHFveGY1UTFZWG9TekdBVm1KR0p1ZUU5QS1sY09uV3dkV1hfV3R4bFEiLCJpc3N1ZWRfYXQiOjE1MzUwODcwNTcsInVzZXJfaWQiOiIxNTAzNDk1ODU4NjI4MDIifQ&access_token=EAALv4NeBfB4BAEHGqEhJa8UezIjPdakuZCZAj2gjO7K0XZB8EYGZADDILrqzdp8dgJ8VaGZBePhiHv4ZCLLDZAekGlZB8Y5M4MkKOmxZBi9jz1xVLp3IwtZCJU5yYY6VSntN1ZBvbsYH0yxIDPC0Ci0glcDVfYDAmcvgEIW69Pu2X64fpXnqVE2xpcSjbMc3MYWrZAzZB71TCPkZAWfwZDZD&expires_in=5184000'	
		del data['details']

	headers['Authorization']=get_signature(campaign_data, app_data, device_data,app_secret=str(campaign_data.get('adjust').get('secret_key')),action='click',idfa=str(data.get('idfa')),created_at=str(data.get('created_at')),)	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
def adjust_attribution(campaign_data, app_data, device_data):
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/' + campaign_data.get('app_version_code') +' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	
	data={}

	params = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink' : 1,
			# 'created_at': datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'created_at': get_date(app_data,device_data,early=random.uniform(10,15)),
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'needs_response_details': 1,
			# 'sent_at': datetime.datetime.utcfromtimestamp(time.time()+random.uniform(0,1)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'sent_at': get_date(app_data,device_data),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'bundle_id': campaign_data.get('package_name'),
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'initiated_by':'backend',
			'os_build':	device_data.get('build'),
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid':app_data.get('ios_uuid')
			}
	
	headers['Authorization']=get_signature(campaign_data, app_data, device_data,app_secret=str(campaign_data.get('adjust').get('secret_key')),action='attribution',idfa=str(data.get('idfa')),created_at=str(data.get('created_at')),)

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	
def adjust_event(campaign_data, app_data, device_data,event_token,partner_params=''):
	def_sessionLength(app_data)
	def_(app_data,'subsession_count')
	
	# if not app_data.get('device_token'):	
	# 	app_data['device_token'] = util.get_random_string("all",22)	
		
	url = 'http://app.adjust.com/event'
	method = 'post'
	header = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/' + campaign_data.get('app_version_code') +' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
			}
			
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	
	data = {
			# 'queue_size':'2',
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'hardware_name':'D101AP',	
			'event_buffering_enabled':	0,
			'cpu_type':'CPU_SUBTYPE_ARM64_V8',
			'attribution_deeplink': '1',
			# 'ios_uuid':app_data.get('ios_uuid'),
			'os_name':'ios',
			'environment':'production',
			'needs_response_details':1,
			'event_count':app_data.get('event_count'),
			'time_spent':int(time.time())-app_data.get('adjust_begin_time'),
			'session_count':str(app_data.get('session_count')),
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			# 'created_at': datetime.datetime.utcfromtimestamp((time.time()-0.04)-app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'created_at': get_date(app_data,device_data,early=random.uniform(1,3)),
			'connectivity_type':	'2',
			'event_token':event_token,
			# 'callback_params':json.dumps({"world_id":"4"}),
			'bundle_id':campaign_data.get('package_name'),
			'subsession_count':str(app_data.get('session_count')),
			'os_version':device_data.get('os_version'),
			'app_version': campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country'),
			'language':	device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'session_length':int(time.time())-app_data.get('adjust_begin_time'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': '1',
			'device_name':device_data.get('device_platform'),
			# 'sent_at': datetime.datetime.utcfromtimestamp((time.time())-app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'sent_at': get_date(app_data,device_data),
			'os_build':	device_data.get('build'),
			# 'tce': '1',
			'install_receipt':app_data.get('install_receipt'),
			'mcc':device_data.get('mcc'),
			'mnc': device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyHSDPA',
			}
	
	if event_token=="fffk92":
		data['revenue'] = productDetail[1]
		data['currency'] = 'USD'

	if event_token=='lz1las':
		data['revenue']='235.00000'

	# if callback_params:
	# 	data['callback_params']	= callback_params
	if partner_params:
		data['partner_params']	= partner_params

	header['Authorization']=get_signature(campaign_data, app_data, device_data,app_secret=str(campaign_data.get('adjust').get('secret_key')),action='event',idfa=str(data.get('idfa')),created_at=str(data.get('created_at')),)	

		
	return {'url': url, 'httpmethod':method, 'headers': header, 'params':None, 'data': data}	
	
###########################################################
#						Self Calls						  #
###########################################################	


def configcall(app_data,campaign_data,device_data):
	url="http://app-measurement.com/config/app/1:342427557617:ios:7fcf52239ab187b5"
	method="head"
	headers={
				# 'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				# 'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent': urllib.quote(campaign_data.get('app_name'))+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}

	params={
		'app_instance_id':util.get_random_string('hex',32).upper(),
		'gmp_version':	campaign_data.get('gmp_version'),
		'platform':	'ios',
	}

	data=None

	return {'url': url, 'httpmethod':method, 'headers': headers, 'params':params, 'data': data}	

	
###########################################################
#						UTIL							  #
###########################################################

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	#st = str(int(time.time()*1000))
	st=device_data.get("device_id", str(int(time.time()*1000)))


	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('app_id'))
	
def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	

def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_ts(app_data,device_data,early=0, ts = time.time()):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((ts-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		# app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['password'] 	= last_name+app_data['user']['dob'].replace('-','')
		app_data['user']['userid'] 		= util.get_random_string(16)
		app_data['user']['postCode'] 		= util.get_random_string('decimal',5)
		app_data['user']['Mobile_number'] 	= str(random.randint(6,9))+util.get_random_string('decimal',9)

def get_signature(campaign_data, app_data, device_data,app_secret=False,action=False,idfa=False,created_at=False):
	header_authorization = 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+util.sha256(str(app_secret)+str(action)+str(idfa)+str(created_at))+'",algorithm="sha256",headers=" app_secret activity_kind idfa created_at"'
	return header_authorization

def getCategoryIDList():
	return [u'104', u'97', u'98', u'104', u'28', u'51', u'50', u'104', u'95', u'27', u'103', u'98', u'97', u'95', u'104', u'97', u'98', u'51', u'41', u'17', u'43', u'46', u'118', u'42', u'44', u'47', u'104', u'95', u'97', u'98', u'100', u'51', u'104', u'95', u'27', u'97', u'98', u'100', u'104', u'28', u'51', u'41', u'44', u'49', u'50', u'104', u'95', u'103', u'104', u'97', u'98', u'104', u'97', u'28', u'28', u'28', u'104', u'97', u'104', u'97', u'103', u'50', u'103', u'50', u'51', u'50', u'104', u'103', u'51', u'44', u'104', u'97', u'98', u'51', u'44', u'104', u'97', u'98', u'51', u'44', u'104', u'97', u'98', u'103', u'50', u'51', u'104', u'28', u'103', u'50', u'95', u'41', u'27', u'266', u'51', u'104', u'97', u'98', u'100', u'51', u'41', u'47', u'104', u'95', u'97', u'98', u'100', u'103', u'50', u'51', u'42', u'104', u'98', u'51', u'41', u'42', u'44', u'47', u'50', u'104', u'95', u'96', u'97', u'98', u'100', u'103', u'103', u'50', u'103', u'50', u'103', u'50', u'51', u'50', u'104', u'103', u'60', u'95', u'41', u'51', u'41', u'42', u'44', u'47', u'104', u'95', u'97', u'98', u'100', u'103', u'50', u'103', u'50', u'103', u'50', u'97', u'97', u'103', u'50', u'51', u'41', u'42', u'44', u'47', u'49', u'104', u'95', u'97', u'98', u'100', u'102', u'51', u'41', u'50', u'104', u'28', u'95', u'103', u'17', u'95', u'41', u'103', u'50', u'103', u'50', u'103', u'50', u'51', u'41', u'44', u'47', u'104', u'95', u'97', u'100', u'28', u'28', u'51', u'41', u'42', u'44', u'47', u'49', u'50', u'104', u'95', u'97', u'98', u'100', u'102', u'103', u'51', u'41', u'42', u'44', u'47', u'49', u'50', u'104', u'95', u'97', u'98', u'100', u'102', u'103', u'51', u'41', u'50', u'104', u'28', u'95', u'98', u'103', u'51', u'41', u'50', u'104', u'28', u'95', u'98', u'103', u'51', u'41', u'50', u'104', u'28', u'95', u'98', u'103', u'51', u'41', u'104', u'95', u'51', u'41', u'104', u'95', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'15', u'23', u'24', u'26', u'33', u'71', u'75', u'87', u'92', u'100', u'28', u'51', u'41', u'50', u'104', u'28', u'95', u'103', u'104', u'97', u'98', u'51', u'104', u'95', u'97', u'98', u'100', u'51', u'42', u'44', u'104', u'97', u'98', u'104', u'97', u'72', u'46', u'85', u'95', u'41', u'96', u'43', u'97', u'44', u'27', u'28', u'98', u'42', u'100', u'47', u'101', u'48', u'105', u'52', u'103', u'50', u'266', u'104', u'28', u'95', u'27', u'104', u'95', u'98', u'100', u'17', u'95', u'41', u'103', u'50', u'28', u'103', u'50', u'51', u'41', u'104', u'95', u'97', u'98', u'100', u'51', u'41', u'50', u'104', u'28', u'95', u'103', u'51', u'44', u'97', u'95', u'51', u'41', u'52', u'42', u'47', u'104', u'95', u'105', u'98', u'100', u'51', u'42', u'104', u'97', u'98', u'51', u'42', u'104', u'97', u'98', u'104', u'97', u'98', u'100', u'103', u'50', u'51', u'50', u'104', u'103', u'104', u'97', u'103', u'50', u'104', u'98', u'98', u'97', u'44', u'104', u'97', u'103', u'50', u'103', u'50', u'103', u'50', u'103', u'50', u'103', u'50', u'103', u'50', u'28', u'51', u'44', u'104', u'97', u'103', u'50', u'60', u'95', u'41', u'50', u'51', u'41', u'104', u'28', u'95', u'28', u'103', u'50', u'104', u'97', u'104', u'97', u'104', u'97', u'98', u'51', u'41', u'44', u'47', u'50', u'104', u'28', u'95', u'97', u'98', u'100', u'103', u'98', u'51', u'41', u'42', u'44', u'47', u'49', u'104', u'28', u'95', u'97', u'98', u'100', u'102', u'51', u'41', u'42', u'44', u'47', u'49', u'104', u'28', u'95', u'97', u'98', u'100', u'102', u'51', u'41', u'42', u'44', u'47', u'49', u'104', u'28', u'95', u'97', u'98', u'100', u'102', u'51', u'42', u'44', u'104', u'95', u'97', u'98', u'51', u'42', u'44', u'104', u'95', u'97', u'98', u'17', u'60', u'95', u'41', u'98', u'42', u'102', u'49', u'103', u'50', u'103', u'50', u'51', u'50', u'104', u'103', u'97', u'51', u'104', u'104', u'97', u'98', u'51', u'44', u'50', u'104', u'97', u'103', u'103', u'50', u'95', u'41', u'27', u'266', u'51', u'41', u'85', u'42', u'44', u'47', u'51', u'104', u'98', u'97', u'17', u'95', u'41', u'28', u'51', u'104', u'17', u'72', u'46', u'85', u'95', u'41', u'96', u'43', u'97', u'44', u'98', u'42', u'100', u'47', u'101', u'48', u'105', u'52', u'102', u'49', u'103', u'50', u'103', u'50', u'103', u'50', u'95', u'41', u'27', u'266', u'104', u'98', u'103', u'50', u'103', u'50', u'103', u'103', u'50', u'51', u'104', u'51', u'104', u'104', u'97', u'100', u'51', u'104', u'97', u'103', u'104', u'100', u'51', u'41', u'42', u'44', u'47', u'104', u'95', u'97', u'98', u'100', u'103', u'50', u'51', u'47', u'104', u'98', u'100', u'51', u'41', u'42', u'44', u'47', u'50', u'104', u'95', u'97', u'98', u'100', u'103', u'51', u'41', u'42', u'44', u'47', u'50', u'104', u'95', u'97', u'98', u'100', u'103', u'51', u'104', u'51', u'104', u'104', u'98', u'41', u'95', u'103', u'50', u'103', u'50', u'17', u'72', u'46', u'85', u'95', u'41', u'96', u'43', u'97', u'44', u'27', u'28', u'98', u'42', u'100', u'47', u'101', u'48', u'105', u'52', u'102', u'49', u'103', u'50', u'266', u'51', u'104', u'51', u'41', u'50', u'104', u'103', u'110', u'51', u'41', u'50', u'104', u'103', u'110', u'51', u'41', u'50', u'104', u'103', u'110', u'17', u'72', u'46', u'85', u'95', u'41', u'96', u'43', u'97', u'44', u'27', u'28', u'98', u'42', u'100', u'47', u'102', u'49', u'103', u'50', u'266', u'104', u'97', u'103', u'50', u'51', u'42', u'44', u'104', u'97', u'98', u'60', u'95', u'41', u'103', u'50', u'104', u'97', u'98', u'100', u'104', u'97', u'103', u'50', u'103', u'50', u'51', u'50', u'104', u'103', u'103', u'50', u'103', u'50', u'103', u'50', u'103', u'50', u'104', u'97', u'98', u'103', u'50', u'51', u'44', u'104', u'97', u'103', u'50', u'104', u'97', u'104', u'97', u'98', u'104', u'97', u'98', u'51', u'47', u'104', u'95', u'98', u'100', u'51', u'44', u'104', u'97', u'103', u'50', u'51', u'104', u'98', u'103', u'50', u'51', u'44', u'50', u'104', u'97', u'103', u'51', u'104', u'98', u'98', u'51', u'44', u'104', u'95', u'97', u'98', u'51', u'44', u'104', u'95', u'97', u'98', u'28', u'51', u'104', u'103', u'50', u'98', u'51', u'41', u'44', u'104', u'95', u'97', u'98', u'51', u'41', u'44', u'104', u'95', u'97', u'98', u'103', u'50', u'51', u'104', u'98', u'100', u' 47', u'97', u'44', u'95', u'42', u'104', u'98', u'103', u'50', u'103', u'50', u'103', u'50', u'51', u'41', u'17', u'43', u'46', u'48', u'52', u'60', u'85', u'118', u'42', u'20', u'53', u'88', u'93', u'44', u'35', u'37', u'57', u'59', u'61', u'73', u'83', u'45', u'47', u'5', u'40', u'58', u'70', u'78', u'49', u'2', u'8', u'80', u'89', u'50', u'104', u'28', u'7', u'11', u'14', u'22', u'55', u'66', u'95', u'27', u'72', u'96', u'101', u'105', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'15', u'23', u'24', u'26', u'33', u'71', u'75', u'87', u'92', u'100', u'4', u'29', u'62', u'69', u'77', u'102', u'103', u'51', u'44', u'47', u'104', u'97', u'100', u'104', u'98', u'100', u'51', u'42', u'47', u'50', u'104', u'97', u'98', u'103', u'51', u'42', u'47', u'50', u'104', u'97', u'98', u'103', u'104', u'97', u'98', u'28', u'104', u'95', u'97', u'98', u'103', u'50', u'103', u'50', u'104', u'97', u'98', u'28', u'104', u'97', u'98', u'103', u'50', u'103', u'50', u'95', u'41', u'27', u'104', u'97', u'98', u'51', u'41', u'17', u'43', u'46', u'48', u'60', u'85', u'107', u'42', u'44', u'47', u'104', u'95', u'72', u'96', u'101', u'105', u'97', u'98', u'100', u'104', u'28', u'103', u'50', u'51', u'50', u'104', u'97', u'98', u'103', u'51', u'104', u'98', u'103', u'50', u'104', u'28', u'51', u'44', u'47', u'104', u'97', u'98', u'100', u'51', u'41', u'47', u'104', u'95', u'97', u'98', u'100', u'51', u'42', u'44', u'47', u'104', u'97', u'98', u'100', u'104', u'28', u'97', u'98', u'51', u'41', u'42', u'49', u'50', u'104', u'28', u'95', u'98', u'102', u'103', u'51', u'104', u'17', u'60', u'95', u'41', u'17', u'60', u'95', u'41', u'104', u'95', u'97', u'98', u'103', u'50', u'51', u'44', u'104', u'97', u'98', u'51', u'44', u'104', u'97', u'98', u'104', u'97', u'98', u'100', u'104', u'97', u'98', u'100', u'103', u'50', u'104', u'98', u'103', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'103', u'50', u'60', u'95', u'41', u'103', u'50', u'51', u'50', u'104', u'103', u'103', u'50', u'104', u'95', u'27', u'97', u'98', u'100', u'98', u'28', u'103', u'50', u'103', u'50', u'28', u'28', u'103', u'50', u'103', u'50', u'51', u'104', u'104', u'97', u'98', u'104', u'97', u'98', u'103', u'50', u'103', u'50', u'104', u'97', u'103', u'50', u'51', u'42', u'44', u'47', u'104', u'97', u'98', u'100', u'51', u'42', u'44', u'47', u'104', u'97', u'98', u'100', u'104', u'97', u'17', u'95', u'41', u'28', u'103', u'50', u'28', u'28', u'104', u'95', u'41', u'27', u'28', u'266', u'103', u'50', u'104', u'97', u'104', u'97', u'72', u'46', u'85', u'95', u'41', u'96', u'43', u'97', u'44', u'27', u'28', u'98', u'42', u'100', u'47', u'101', u'48', u'105', u'52', u'102', u'49', u'266', u'104', u'97', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'103', u'50', u'103', u'50', u'51', u'44', u'104', u'97', u'98', u'103', u'50', u'103', u'50', u'51', u'50', u'104', u'103', u'104', u'97', u'98', u'104', u'97', u'103', u'50', u'51', u'104', u'103', u'50', u'51', u'41', u'44', u'50', u'104', u'95', u'97', u'98', u'103', u'51', u'41', u'44', u'50', u'104', u'95', u'97', u'98', u'103', u'104', u'97', u'104', u'97', u'98', u'103', u'50', u'51', u'41', u'60', u'104', u'95', u'17', u'60', u'95', u'41', u'98', u'42', u'102', u'49', u'103', u'50', u'17', u'95', u'41', u'96', u'43', u'97', u'44', u'27', u'28', u'98', u'42', u'100', u'47', u'105', u'52', u'103', u'50', u'266', u'51', u'41', u'44', u'47', u'50', u'104', u'95', u'97', u'98', u'100', u'103', u'28', u'28', u'104', u'97', u'104', u'100', u'98', u'95', u'41', u'104', u'97', u'98', u'104', u'95', u'97', u'98', u'28', u'51', u'44', u'47', u'104', u'95', u'97', u'98', u'100', u'104', u'100', u'51', u'50', u'104', u'103', u'51', u'50', u'104', u'103', u'104', u'97', u'98', u'103', u'50', u'103', u'50', u'103', u'50', u'104', u'97', u'51', u'41', u'50', u'104', u'28', u'7', u'11', u'14', u'22', u'55', u'66', u'95', u'27', u'103', u'51', u'41', u'50', u'104', u'28', u'7', u'11', u'14', u'22', u'55', u'66', u'95', u'27', u'103', u'103', u'50', u'51', u'41', u'42', u'44', u'47', u'104', u'95', u'97', u'98', u'100', u'51', u'41', u'42', u'44', u'47', u'104', u'95', u'97', u'98', u'100', u'51', u'41', u'42', u'44', u'47', u'104', u'95', u'97', u'98', u'100', u'104', u'97', u'98', u'51', u'104', u'50', u'103', u'98', u'42', u'102', u'49', u'103', u'50', u'103', u'50', u'103', u'50', u'103', u'50', u'103', u'50', u'103', u'50', u'51', u'41', u'42', u'44', u'47', u'50', u'104', u'95', u'97', u'98', u'100', u'103', u'51', u'50', u'104', u'28', u'103', u'104', u'97', u'104', u'97', u'51', u'41', u'85', u'44', u'50', u'104', u'28', u'95', u'97', u'98', u'103', u'28', u'104', u'95', u'97', u'98', u'100', u'104', u'97', u'98', u'104', u'97', u'98', u'104', u'97', u'98', u'104', u'97', u'98', u'103', u'60', u'95', u'41', u'51', u'44', u'35', u'37', u'57', u'59', u'61', u'73', u'83', u'104', u'97', u'98', u'104', u'97', u'98', u'104', u'98', u'103', u'50', u'104', u'97', u'98', u'51', u'44', u'50', u'104', u'97', u'51', u'44', u'50', u'104', u'97', u'104', u'98', u'104', u'97', u'51', u'104', u'51', u'42', u'44', u'35', u'37', u'57', u'59', u'61', u'73', u'83', u'47', u'104', u'95', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'100', u'104', u'28', u'104', u'97', u'19', u'104', u'95', u'27', u'6', u'10', u'13', u'21', u'54', u'65', u'104', u'98', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'51', u'44', u'47', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'100', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'104', u'97', u'98', u'100', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'51', u'41', u'42', u'44', u'35', u'37', u'57', u'59', u'61', u'73', u'83', u'47', u'104', u'95', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'100', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'51', u'41', u'42', u'44', u'47', u'104', u'95', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'100', u'104', u'95', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'100', u'104', u'97', u'19', u'34', u'36', u'38', u'56', u'74', u'76', u'82', u'86', u'91', u'98', u'51', u'50', u'104', u'98', u'15', u'23', u'24', u'26', u'33', u'71', u'75', u'87', u'92', u'104', u'51', u'41', u'17', u'51', u'104', u'98', u'104', u'28', u'7', u'11', u'14', u'22', u'55', u'66', u'119', u'119', u'119', u'51', u'44', u'104', u'97', u'119', u'119', u'119', u'51', u'42', u'44', u'104', u'97', u'98', u'104', u'97', u'119', u'119', u'119', u'119', u'119', u'119', u'104', u'104', u'51', u'42', u'44', u'47', u'104', u'97', u'98', u'100', u'51', u'47', u'104', u'100', u'104', u'100', u'104', u'28', u'104', u'95', u'119', u'98', u'51', u'44', u'104', u'97', u'98', u'50', u'103', u'100', u' 47', u'98', u'100', u'97', u'98', u'98', u'100', u'98', u'97', u'44', u'98', u'98', u'100', u'100', u'98', u'97', u'97', u'50', u'98', u'100', u'97', u'98', u'28', u'41', u'97', u'97', u'50', u'103', u'50', u'103', u'98', u'50', u'44', u'98', u'98', u'98', u'100', u'97', u'98', u'97', u'100', u' 47', u'100', u'98', u'95', u'97', u'100', u'100', u'97', u'100', u'100', u'51', u'50', u'104', u'103', u'97', u'95', u'47', u'98', u'95', u'98', u'97', u'95', u'98', u'44', u'98', u'98', u'95', u'100', u' 47', u'98', u'98', u'100', u' 47', u'98', u'97', u'97', u'97', u'98', u'100', u'98', u'100', u' 47', u'97', u'44', u'50', u'103', u'104', u'28', u'98', u'98', u'100', u'28', u'100', u' 47', u'97', u'44', u'100', u'97', u'98', u'97', u'44', u'98', u'98', u'50', u'98', u'100', u' 47', u'97', u'44', u'98', u'98', u'97', u'44', u'98', u'97', u'44', u'100', u' 47', u'100', u' 47', u'97', u'44', u'98', u'100', u'51', u'47', u'104', u'100', u'97', u'98', u'95', u'98', u'51', u'104', u'50', u'98', u'100', u'98', u'95', u'98', u'97', u'98', u'98', u'95', u' 41', u'97', u'98', u'97', u'44', u'97', u'98', u'97', u'97', u'103', u'100', u'98', u'100', u' 47', u'97', u'98', u'100', u' 47', u'97', u'98', u'98', u'98', u'100', u'98', u'97', u'28', u'97', u'100', u' 47', u'97', u'28', u'98', u'47', u'100', u' 47', u'98', u'97', u'95', u'28', u'98', u'98', u'100', u'97', u'98', u'98', u'100', u' 47', u'97', u'44', u'51', u'47', u'104', u'100', u'98', u'100', u' 47', u'97', u'28', u'98', u'100', u' 47', u'95', u' 41', u'98', u'97', u'100', u' 47', u'95', u'50', u'100', u'97', u'95', u'100', u'98', u'95', u'28', u'98', u'95', u'50', u'98', u'98', u'95', u' 41', u'98', u'51', u'44', u'104', u'97', u'98', u'97', u'95', u'97', u'44', u'97', u'44', u'104', u'95', u'27', u'104', u'98', u'51', u'50', u'104', u'103', u'51', u'41', u'104', u'95']