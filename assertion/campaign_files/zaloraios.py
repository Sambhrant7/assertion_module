# -*- coding: utf-8 -*-
import random,time,datetime,uuid,json,urllib,re, hashlib
from sdk import getsleep
from bs4 import BeautifulSoup
from sdk import util,installtimenew,purchase
import clicker,Config

campaign_data = {
	'package_name' : 'com.zalora',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	
	'app_name' : 'ZALORA',
	'app_id':'624639017',
	'app_version_code': '1',#'4',#'3',#'5',#'3',#'4',#'1',#9,  '1',#'4',#'1',#4, '1',#'2',#'1',#'2',#'1', #5
	'app_version_name': '7.6.1',#'7.6.0',#'7.5.0',#'7.4.0',#'7.3.0',#'7.2.0',#'7.1.0',#'7.0.1',#7.0.0, '6.18.0',#'6.17.0',#'6.16.0',#6.15.0, '6.14.3',#'6.14.2',#'6.14.1',#'6.14.0',#'6.13.1',#'6.9.0',#6.6.0, 6.8.0
	'supported_os':'10.0',#9.0 #Requires iOS 9.0 or later
	'ctr':6,
	'supported_countries': 'WW',
	'device_targeting':True,
	'sdk' : 'ios',
	'tracker':'adjust',
	'app_size' : 97.6,#96.0,#96.6,#95.2,#94.9,#94.6,#92.9,#87.7,#87.6, 86.5,#77.0,
	'adjust':{
				'app_token': 'ky7vttx2fzue',
				'sdk':'ios4.18.2',#'ios4.18.0',#'ios4.17.2',#'ios4.15.0', #ios4.14.3
				'app_updated_at': '2019-10-03T18:18:30.000Z',#'2019-09-20T21:21:49.000Z',#'2019-09-12T10:32:54.000Z',#'2019-09-02T07:10:22.000Z',#'2019-08-23T08:29:42.000Z',#'2019-08-06T13:15:05.000Z',#'2019-07-26T13:32:03.000Z',#'2019-07-12T10:06:17.000Z',#2019-07-08T10:43:34.000Z, '2019-06-19T16:03:21.000Z',#'2019-06-17T09:54:45.000Z',#'2019-06-04T12:32:13.000Z',#2019-05-27T13:45:20.000Z, '2019-05-24T11:27:03.000Z',#'2019-05-16T14:58:18.000Z',#'2019-05-07T09:15:48.000Z',#'2019-04-29T10:20:54.000Z',#'2019-04-17T13:57:11.000Z',#'2019-01-17T10:31:57.000Z', #2018-11-02T12:07:27.000Z, 2018-12-17T10:56:31.000Z
				'secret_id': '40',#'39',#'38',#'36',#'35',#'33',#'32',#'31',#30, "29",#'28',#'27',#25, '24',#'22',#'21',#'20',#'19',#'12', #6, 10
				'secret_key': '1128260544912022321501861814310275634',#'182227446698108352918120443011850181566',#'14296625420178527763674349991620765579',#'182733487115626231062076694522664775726',#'5950842278902207294234739291632467331',#'5951769126881146331545408567473935734',#'6181432051097667446162997304590166601',#'15518942647777262321608808324345903306',#27106740256148545711454410631828618314, '9940401301061459782724273851302949928',#'106177952524291121819650366892115835343',#'4398588023494893091836642205694529588',#159774669565746839216159116281062741329, '2510727921944062004205400669645399759',#'16843689555909535632737527611657496306',#'73032565420851601281299980916325884455',#'316846913420621919752457620702102957',#'56452581418084839221273404839451742615',#'131674535819766692774309911671041855847' #156953618821212497941697743024400448853, 18250275659892965120892987392120305260
			},
	'appload':{
			'version':'5.6.7',
			'protocol_version':'1.0.0',
			'appid':'5548d9ce8172e25e67906bac'
	},

	'voc':{
			'version':'19.23.29',
			'id':'tbdJ2lWk',
			'hash':'u7rlsfmT6AH1',
	},
	'zalora':{
		'url_version':'1570.150000',#'1452.230000',
		'sdk':'19.23.29',
	},
	'retention':{
					1:65,
					2:63,
					3:60,
					4:58,
					5:55,
					6:53,
					7:50,
					8:48,
					9:45,
					10:43,
					11:40,
					12:38,
					13:35,
					14:32,
					15:30,
					16:28,
					17:27,
					18:26,
					19:25,
					20:24,
					21:23,
					22:21,
					23:20,
					24:19,
					25:18,
					26:17,
					27:15,
					28:14,
					29:13,
					30:12,
					31:11,
					32:9,
					33:8,
					34:7,
					35:5,
	}
}


###########################################################
#						INSTALL							  #
###########################################################
def install(app_data, device_data):
	print 'plz wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)
	def_sec(app_data,device_data)
	app_data['adjust_call_time']=int(time.time())

	if not app_data.get('installed_at'):
		app_data['installed_at'] = datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	return_userinfo(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'subsession_count')
	def_(app_data,'session_count')
	inc_(app_data,'session_count')
		
	app_data['signup_flag']=False
	app_data['login_flag']=False
	app_data['in_cart']=0
	app_data['in_wishlist']=0
	app_data['cart_value'] = list()
	app_data['cart_price'] = 0

	global currency
	currency = util.country_list.get(device_data.get('locale').get('country')).get('currency')

	global session_time
	session_time=get_date(app_data,device_data)
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)	

	global self_sent_at
	self_sent_at=datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'
	time.sleep(random.randint(60,100))
	
	global click_created
	click_created=get_date(app_data,device_data)
	time.sleep(random.randint(10,12))

	global event_sent
	event_sent=get_date(app_data,device_data)
	time.sleep(random.randint(10,12))

	global info_created
	info_created=get_date(app_data,device_data)
	time.sleep(random.randint(6,8))

	global info_sent
	info_sent=get_date(app_data,device_data)

	if app_data.get('user_info').get('sex')=='male':
		app_data['segment']='men'

	else:
		app_data['segment']='women'

	global screen_size

	screen_size=random.choice(['640.000000/1136.000000','750.000000/1334.000000'])

	print '*************Install**********************'

	###########		Initialize		############
	app_data['device_id']=str(uuid.uuid4()).upper()
	app_data['binary_uid']=util.get_random_string('hex',32)
				
	###########			Calls		############ 

	print 'lat and lon generator'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	try:
		json_lat_lon_result=json.loads(lat_lon_result.get('data')).get('geo')
		app_data['lat']=str(json_lat_lon_result.get('latitude'))
		app_data['lon']=str(json_lat_lon_result.get('longitude'))
		app_data['city']=str(json_lat_lon_result.get('city'))
		app_data['region']=str(json_lat_lon_result.get('region'))
		app_data['postal_code']=str(json_lat_lon_result.get('postal_code'))
	except:
		print "Exception"
		app_data['lat']= '28.6667'
		app_data['lon']= '77.2167'
		app_data['city']='Noida'
		app_data['region']='Uttar Pradesh'
		app_data['postal_code']='110091'


	# req=selfcall1(campaign_data, app_data, device_data)
	# util.execute_request(**req)

	# req=selfcall2(campaign_data, app_data, device_data)
	# util.execute_request(**req)

	app_data['device_id1']=str(uuid.uuid4()).upper()

	global cacheKey, colors, countries, version

	request=zalora_init(campaign_data, app_data, device_data, call=1)
	response=util.execute_request(**request)	
	try:
		output=response.get('data').encode('utf-8')
		soup = str(BeautifulSoup(output,"html.parser"))

		cacheKey=str(str(soup).split('cms/staticBlock/about-us-app')[3].split('https://dynamic.zacdn.com')[0]).split(' ')[1][:32]

		colors=str(str(soup).split('Bhttps://www.zalora.sg/customer/exchange_return/detail/?order_id=%s')[7].split('black')[0]).split(' ')[1][:32]

		countries=str(str(soup).split('newarrivals_kids_750x248')[6].split('BNFAS')[0]).split(' ')[1][:32]

		version=str(str(soup).split('https://dynamic.zacdn.com')[1].split('3.0.0')[0]).split(' ')[1][:32]

	except:
		print 'exception______'
		cacheKey='7de05146ccac8091029d03136b33fb77'
		colors='0929d0bb8e65dc10dccce5c9314d1be8'
		countries='ed98f554dceb16ba7187240aad5318dc'
		version='ba0ef964056309e77a28726bcba30ba0'

	# req=selfcall5(campaign_data, app_data, device_data)
	# util.execute_request(**req)	

	# req=selfcall3(campaign_data, app_data, device_data)
	# util.execute_request(**req)

	# request=zalora_catalog_categories(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	time.sleep(random.randint(10,15))
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data,source='iad3')
	util.execute_request(**adjustSession)	

	time.sleep(random.randint(5,8))
	request=measurement(campaign_data, app_data, device_data)
	util.execute_request(**request)

	# time.sleep(random.randint(5,8))
	# request=zalora_init(campaign_data, app_data, device_data, call=2)
	# util.execute_request(**request)	

	# req=selfcall4(campaign_data, app_data, device_data)
	# util.execute_request(**req)
	
	# request=zalora_catalog_categories(campaign_data, app_data, device_data, call=2)
	# util.execute_request(**request)

	# request=zalora_webcontent(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	# request=zalora_homescreen(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	request=form_login(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(8,10))
	request=zalora_feed(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	global csa_list
	csa_list=[]
	try:
		output=json.loads(response.get('data')).get('data')
		print json.dumps(output[0])

		for i in range(0, len(output)):
			if output[i].get('data').get('media'):
				csa=str(output[i].get('data').get('media').get('data').get('internal_promotion_name'))
				csa_list.append(csa)

			else:
				pass

		app_data['csa']=random.choice(csa_list).split('|')[2].split('|')[0]

		if not csa_list:
			csa_list=['SVC|Men|cwfourcnyredthirtyfive|All|NA', 'SVC|Men|cwfourwinterflashsale|All|NA', 'SRE|Men|cwfourwinterwear|All|NA']

	except:
		print 'exception______'
		csa_list=['SVC|Men|cwfourcnyredthirtyfive|All|NA', 'SVC|Men|cwfourwinterflashsale|All|NA', 'SRE|Men|cwfourwinterwear|All|NA']
		app_data['csa']='cwfourcnyredthirtyfive'

	time.sleep(random.randint(1,2))
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data,source='deeplink')
	util.execute_request(**adjustSession)

	time.sleep(random.randint(1,2))
	print "\n----------------------------ADJUST sdk_info ------------------------------------"
	adjust_sdk = adjust_sdkinfo(campaign_data, app_data, device_data)
	util.execute_request(**adjust_sdk)
	
	print "\n----------------------------Adjust attribution----------------------------------"
	request=adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)

	print "\n----------------------------Adjust attribution----------------------------------"
	request=adjust_attribution(campaign_data, app_data, device_data, initiated_by='backend')
	util.execute_request(**request)

	# time.sleep(random.randint(35,40))
	# request=zalora_tutorial(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	if random.randint(1,100)<=90:
		time.sleep(random.randint(1,2))
		print "\n EVENT 1--------------------------------------choose country-----------------------------------------------------"
		callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"user_segment":"4","device_type":device_data.get('device_platform'),"duration":"0"})
		partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"user_segment":"4","device_type":device_data.get('device_platform'),"duration":"0"})
		call_event(campaign_data, app_data, device_data,'kyc4ia',callback_params,partner_params)

		duration=str(random.randint(7000,7500))
		print "\n EVENT 2---------------------------------------choose gender----------------------------------------------------"
		callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"user_segment":"4","device_type":device_data.get('device_platform'),"duration":duration})
		partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"user_segment":"4","device_type":device_data.get('device_platform'),"duration":duration})
		call_event(campaign_data, app_data, device_data,'kyc4ia',callback_params,partner_params)

	time.sleep(random.randint(2,4))
	request=form_login(campaign_data, app_data, device_data, call=2)
	util.execute_request(**request)

	# time.sleep(random.randint(1,2))
	# request=zalora_register(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	# global cookie1, doraemon_sessionId1, doraemon_sessionId2, doraemon_sessionId3

	time.sleep(random.randint(30,35))
	request=zalora_customers(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	try:
		output=response.get('data').encode('utf-8')

		app_data['cookie1']=response.get('res').headers.get('Set-Cookie').split(';')[0]
		app_data['doraemon_sessionId1']=response.get('res').headers.get('Set-Cookie').split('doraemonSessionId=')[1].split(';')[0]

		soup = str(BeautifulSoup(output,"html.parser"))

		temp=(str(soup).split(' ')[2])
		app_data['user_id']= re.findall("\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d",temp)[0]

	except:
		print 'exception______'
		app_data['user_id']='10815480715988554'
		app_data['cookie1']='f59f9fd9576397c679c3d3eb52622f8d=de08e001f0df57f6a497310b68d53c31d2d4fe4c25b58152e9225886436bc779b8e26c9d0ad0da718d225f5d8a03bdd604daa0e17021a73ed5d81f06953c602e%3A529146d5cda785a6684096ebb240a34a2b97b6816a06f1c529f9f6563c935daf'
		app_data['doraemon_sessionId1']='MTU0ODA3MTU5OXxsclNiMTU0OF9CUXAwLWRIa0dlWmI0d0Q4SHpPMmF1cXlUb0MwVWRta1lNOXc1MmJneHpOTkNIYmQxMkxvNENjS0xHYVlsUklhZUlLTkRTSmtPQUdtNThSTVZrODZxNXZyQmc4b1l2d3NjMklrRkVGNkU5aVk5ZkJqYUI0ZkZRMWZhWU5GdUFYNnRzOUxiTW5fcWd0M3oxZGgtSE1xMXVTbVIya2ZhY1JKVy0wLS1qTWQ1VTVaX1IwWXJ6YXJZLS1sQUNGVVgyWW8tb1BUNHdzMUpfeHhDaWIweDNhM0QtUVhsWFQyVTBSRXZmRWxjcEdIcEt2TGpLTzhjOHFKSUZxR2FQb25kX0RLZnlDZ1JWZVNwb0NtdGFWSHJkWjRsOEtoZU5YbG1xSzl4S3ZWWjlaZnJ4ZVpGd1hvdmN5NElUNkotVFhJVEJoenBBYWkyZ0E4MUNGT3N3NzlPU2k1OEE5dHBHTE9kTi1oT3ZldHprWlRrNnl6Mm03M0NwSFdTN0pIYldmZTU4d05LZlpnNmI0ZkRpcnQxa3h0LWxTTEFsQjc5azdqZk10Ri1LUXdjMXZQZlRIcmZhYTlLSllQeldDVUJrLVJkSTZxdlpNQklGdnlSLTk2R3BYQnBuMURoZWFTbzlQaGdjS2hZa0pycU9HS1ZCY1NSOWlQQ0Qwc05fYkoyc3dVSWF1X2ZydjRuOHZ3VDlSWjlnYWFJOV82bVFhU29kYnpLWDl4MDZDUUNfZUZKOXoyWlNUamhzMWVXWkNwWXhEeUZkWVVkNlRaR2wyRmZLYWxfSWgwVVlBOFVCY2F6OEllSjRZdGFkNEloZFREbTJ3Q0VwTnhYaHowVENKYzhDcmxxNHd3MWVCR0tKNElzeEhKYV9tdW5TUE8xNS1wVmVpUkwyWkNzeGZVTWw1aGlFTHZucnYwUWttejhwOWY3RG1YS2cyWlRlbkYya2NLdV9hYnVhV2RzbloyZ1JzM25pWEdJZHJUOWFCNXJ5WGhYdTAxaDkzN3VTa1ctRDJndzhrTDJHSlNpM2U0cHFYcmFINzBSeURWWXNxTGtnZmUySFFwZlBRM2QzS1RNRXlaZWpwZl8zdkNRRk1wc05Wbjc4UnY0Yi1XQUV4VmI5RDE2VDFzOGtxRWpjVm9OZXo1LTRaTGxfcHdWakkxdnJxM0cyQTQ4VkpaWjBRZGo1OHEwOTY3ZXd5eC1KTkJNNnFvYjNETVd2X0diRXVQSUR4YWV0TFA5RlhWNGc5THFaak0wZkVxcW50S2JkM2JKd1JDdnFKejBlNVRzaWViTDNpWV9hUEExOFFRZGxoeXA4MHVxMG1hX29TUmM2S1pxYXNzRm9zZjd1M1h5VE1EdzBsUEpkVHdqRnRkSUJSQ0FyZ0piQ2stbkdsaTRfb1UyVDlBNlE4Y3NBPXxA-yDtHe1UDmGwOSWT4y7xVSn-vw%3D%3D'

	time.sleep(random.randint(1,2))
	request=zalora_checkoutSync(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	try:
		app_data['doraemon_sessionId2']=response.get('res').headers.get('Set-Cookie').split('doraemonSessionId=')[1].split(';')[0]

	except:
		print 'exception______'
		app_data['doraemon_sessionId2']='MTU0ODA3MTYxOXxvbDhvT3dGakFYdEU5SjU0WGVhSHhEeGVvV05HUmFmT0NTYUxjUktPNUdiNFhhcFB5TElWblAyRTlLWVFpVzIzMi1pU3NvdXhIdHNVTlFfeldIVHRmUUw1NmJTajhRNGQ1aHFrRjBKcjF4N0FTUWtyYlFheTVsYUNlQ2I1VUVhalY1VlFUOWx2RHB3a0hUbTdGX1ZsUS1VaXJiLVo5bThMY0lmN1V1UWhlU2l6aWJ5NWhMcDRJX3lmWmhtNHQ2QWU5ZHE0Mm5ndWs4VE1oWEhBM2hxazlzMFFDbEdxTVpodGU3Y3pvendWUjd3bThiWDNOQU9EdVZiOUlmSlhpMVhHeVpwLVluMk8ybER6bzE0c2lsVHdXVzFNOTQwRlhvT216eWtMN1Q4S1hKamRMSm5kUHFRRnZDaHdqaXBXWllIeTg1M2F6V0dGbE5Gb0g4N08zMzNXWWRaeHdCUUxYSmNjd1JIaFNsTkEzZThPOEVXOTRHWkxRNFVTVlBDRm14UWlHUndoWmNTRTV0R083ZklBMkhCUnJ6OG82RGVzbHU3aUwxUFpRUTZfOGNDcTluRWZtTk5RY3haR0xGcG1vZmh5VGZ4T1BZaDc5NUFtNUgydHhTaTB4R2ZadDBkcVJBQVdINDFMU29aeFA4QUMzQW9lZkMtUUdadGR4STVwaEY4WTZSRVE4VXJIQU11VWRhMXhrVTBDdXhWQXgzbXJLSDQ3VExYYlNfVHc0WWFZNHJNV1F6bl93eFdYUENfc25xbzVCbk1GVmM3NjNZR0R6bmRuODRhcjMwclpIYmFVM1kyYjlLa0dDOHpneGZUYVY4aFRpVnhmRkViSlhLd2Q2MXIwNy1WX0ktWWFQRUxwTGRmT0h4OENoWVpNWi1HTWM3b081RzBoMTVQakFnVE1yUjg0dVYwSlAxLW5PdURraXBGcy04UmRkV0MxSVNyVGZwU1ZITVprVDhlQjREeEFRbWh6TW13d3Q5ckI2eHR5bUZTZzFESmc4d1M2WHdOMkxBajdvRkt6U1llQkxKN3BfUWhjeTUwWUVmLUtxSTFrckRYaTFqUVVvZGl3cmxtNzlHb2RIQjFnWWZ6SXJjWnJfeW1NNlVqazljclc0d3pHZEJkNEJqOENjc3NyZ2ZHNEFkekowZHIyNUNLVjJpYmxsX3JGR1hYXy1pRnMtRzQyeGMxNnVtaENuUEtoUmRjYjA4WFZOUGN5OVhUX1c3NUhVSUJfRWpPamFnc0JwazJNZ2JtN0hOZzdNR0h3c3k5QnRsYm1UdVYxOVpsU1FlcDZoc0lmcEZHbmVUam00YXczcy1ZZ1BTa1VGYU53SjI0cUtpN1lWMkNUSHdmTkxsbkZyclVLOURDd3VqN3huZlFZV2JCcnJ3bWd4NGxOZW9RZHF2dTUydVZmajBJPXy0XOCqmARGsyKYYfeFLATf_4U2Hw%3D%3D'

	time.sleep(random.randint(1,2))
	request=zalora_addresses(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	try:
		app_data['doraemon_sessionId3']=response.get('res').headers.get('Set-Cookie').split('doraemonSessionId=')[1].split(';')[0]
		print 'doraemon_sessionId3___________'

	except:
		print 'exception______'
		app_data['doraemon_sessionId3']='MTU0ODA3MTY0MXxjOGM5TFMwd0tNMEpTRXhGRXdGMDRFb3BlV1MtSkVoQVNiSnBqTjloY2ttNU1MLUp6VzczUmlCUkhldFRkbWhSQVNvOWtNQkR5MlNDUnNPNWFHOXBlRFRWOG9hME9EUW95ZDFwMUVjNXdJUFdZVjd5SEY2eTBEMU4xWWdoX3RESWxtenc3VTRCUEY0cHU2elIxVUJsQjJhQmRTQkw1aEhVcDcyLWFoakZSSU9TWHJobnhlQUZhYUFqeDZoTVNwU3hYZHhOSWEwSGJzLXo4eU9zdXkza0xPLUhUQXRReFJzWXN4RmxiM1ZDRkg2dHBJa0dnUXVCQmo3bWU2NFZoQ1A4SzUySFo3Znp3aThJRXc3RFBpNmtHZXBIaG16bUZ0VlpjcW5JNGtwMDAyZjZXMmdjWkI1WHhYN2wybENFa3liVlJBVDlMY0JwUVFvMXJUSGduak1zY2pTZE1jTDRKQ09rYzh0elM2X002RUxJUVlFNWlQTUZVQnZvaFFfU21OWmYxU3Uta2YwQUQtLTFHUjhJR211eTNzbUZreWVhUlB5X0FmVHlVN3lLX1FNTWlpQkZPNXJjekpsY1lxZjRPcnhsOW1pNklNTkExbHZVR1BkaHVCbk9FNlQyQnJpRDQ5N0dzVFlfZHpDYzhCblR0cFFXaWdDRGxIaFVBVmdMR2paV2VwbzdaVms4bWRmTEZOanhMYmxyU1IwX3kwTFhhUVJVOTBJemR5amdxVnJKcUd2aE44a3dKYVZZYk4wTWMxVGVERXYyNC0yS29NMzVjLWNLcDY3SngwT0J3bG01N2Q4c2tQb1Q4WjdhSnkxRWhwelExMHRaaFFXd203X3VYcWw2VTZQblRvQzNCUVpaT3lleTRHa2NxYVA0czQ3cDdNMzlPZVRhdEpaeFdYS0ZMWXVycF9lcFVTMzZmblUydFAzdy15SnM0Qk1ZWDF4X0VvYnd6TWY5ZGNpRjUyQTMzNWFaalNUN2paZ3ZyNjMwVGdRajd1dEZtZlZZeGNqckkxZmxGWXBkMUlhejM5VnVnVEp2MHhfdmRHd3ZLb19GcEotWHRQcnE2YUNWRmVNc1RXdndCUUJENDlWNDd2TUlWUmlJZGdVWkw4eWx2Q3VpZnlBQmc3N216STBJWEpBckh3NlRzSVBQcnlFNkZYWXVjZE81QlZ3aW05bko4MlFwbHRESVZpRmozYlRTMTF3YWVpWGQ4ZHF4V0t5QWV0dURwNzZxbnMydy1jaGFfdFhNTzB2U19LSlltWTFTUUl6eUFMMEJfc3JlcF9ocHhQd21KU0tSaUhSbURrMGxnQVhGVEQ3dVRseEFfLXJsV3pGd1kwYkZlUERRT1RJOUJaNFJBVHM3YkdOMjVPdHFzdjNhTTl2dG81MXM2WUhCZ0lOUEZlOHdjYjlmTzZNPXxCF4A6_QgVtkQwfKdRryVkvQ8R-Q%3D%3D'
	
	global checkout_cookie_key,checkout_cookie_value
	print "_______________checkout_coupon_________________"
	request=checkout_coupon(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	try:
		cookie=response.get('res').headers.get('Set-Cookie')
		checkout_cookie_key=cookie.split('=')[0]
		checkout_cookie_value=cookie.split('=')[1].split(';')[0]
	except:
		checkout_cookie_key="dorpc_d50acd0c74fa3d0cb876238f029fe091"
		checkout_cookie_value="P6FXa4kir17wgBTSyuaCnwLmr6fAIE2j"
	

	global product_list,price_list,brand_list
	
	time.sleep(random.randint(2,6))
	print "_______________product list_________________"
	request=zalora_products(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	product_list=[]
	price_list=[]
	brand_list=[]
	try:
		output=response.get('data').encode('utf-8')
		result=str(output)

		value=result.split('http://')

		for i in range(len(value)):
			print "-----------------------------------"
			if re.findall("\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w",value[i]):
				product= re.findall("\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w",value[i])
				product=product[0]
				if "_" in product:
					continue
				else:
					product_list.append(product)
				price= re.findall("\d+\.\d+", value[i])
				# print price
				if len(price)==2:
					price_list.append(price[1])
				else:
					price_list.append(price[0])
				
				rep= re.findall("\w+|['&]",value[i])
				# print rep
				for i in range(len(rep)):
					if rep[i]=='00':
						start=i+1
						ext=str(rep[len(rep)-1])
						if len(ext)==1:
							end=len(rep)-1
						else:
							end=len(rep)
						
						brand=rep[start]
						if start!=end:
							for i in range(start+1,end):
								if rep[i]=="'" or rep[i-1]=="'":
									brand=brand+rep[i]
								else:
									brand=brand+' '+rep[i]
				brand_list.append(brand)

		if not product_list:
			product_list=['42DC0AA72390BBGS', '2FEBBAA52816C4GS', '73176AAC229025GS', 'DA9C3AA19DEC30GS', 'A3712AA204FCBCGS', '9F74EAAC2D2B03GS', 'AA077SHAC56FD1GS', '753BASH6E5417AGS', '30637SHA6F1D5AGS', '11B3DSH8004762GS', 'CBE7AAA6813617GS', '61FA9AAEF6E1A9GS', '70A6EAA762DB1AGS', '76960AA5A13BB3GS', '4CA1DAA55CC179GS', '8D129AAE5DDC72GS', '95BE5AA872A179GS', '80B94AA9500E4CGS', '8F607AABFE14C6GS', 'AA9F5AADC79122GS']
		
		if not price_list:
			price_list=['249.00', '249.00', '239.00', '249.00', '620.00', '530.00', '1599.00', '989.00', '989.00', '489.00', '799.00', '169.00', '249.00', '460.00', '799.00', '229.00', '540.00', '899.00', '229.00', '659.00']
		
		if not brand_list:
			brand_list=['ZALORA', 'ZALORA', 'ZALORA', 'Something Borrowed', 'Abercrombie & Fitch', 'Hollister', 'Geox', 'ALDO', 'ALDO', 'Keds', 'Mango', 'ZALORA BASICS', 'ZALORA', 'Dorothy Perkins', 'Lipsy', 'Something Borrowed', 'TOPSHOP', 'ESPRIT', 'ZALORA BASICS', 'Ivy Park']
		

	except:
		print 'exception______'

		product_list=['42DC0AA72390BBGS', '2FEBBAA52816C4GS', '73176AAC229025GS', 'DA9C3AA19DEC30GS', 'A3712AA204FCBCGS', '9F74EAAC2D2B03GS', 'AA077SHAC56FD1GS', '753BASH6E5417AGS', '30637SHA6F1D5AGS', '11B3DSH8004762GS', 'CBE7AAA6813617GS', '61FA9AAEF6E1A9GS', '70A6EAA762DB1AGS', '76960AA5A13BB3GS', '4CA1DAA55CC179GS', '8D129AAE5DDC72GS', '95BE5AA872A179GS', '80B94AA9500E4CGS', '8F607AABFE14C6GS', 'AA9F5AADC79122GS']
		
		price_list=['249.00', '249.00', '239.00', '249.00', '620.00', '530.00', '1599.00', '989.00', '989.00', '489.00', '799.00', '169.00', '249.00', '460.00', '799.00', '229.00', '540.00', '899.00', '229.00', '659.00']
		
		brand_list=['ZALORA', 'ZALORA', 'ZALORA', 'Something Borrowed', 'Abercrombie & Fitch', 'Hollister', 'Geox', 'ALDO', 'ALDO', 'Keds', 'Mango', 'ZALORA BASICS', 'ZALORA', 'Dorothy Perkins', 'Lipsy', 'Something Borrowed', 'TOPSHOP', 'ESPRIT', 'ZALORA BASICS', 'Ivy Park']

	age=int(app_data.get('installed_at').split('-')[0])-int(app_data.get('user_info').get('dob').split('-')[0])
	
	if random.randint(1,100)<=50:

		time.sleep(random.randint(0,1))
		print "\n EVENT 3-----------------------------------------signup--------------------------------------------------"
		callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
		partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"customer_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
		call_event(campaign_data, app_data, device_data,'n6uqc2',callback_params,partner_params)
		
		app_data['signup_flag']=True
		app_data['login_flag']=True

	if random.randint(1,100)<=85:

		time.sleep(random.randint(1,2))
		print "\n EVENT 4------------------------------------------homepage-------------------------------------------------"
		callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","device_type":device_data.get('device_platform')})
		partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","device_type":device_data.get('device_platform')})
		call_event(campaign_data, app_data, device_data,'i5whdy',callback_params,partner_params)

		time.sleep(random.randint(1,2))
		print "\n EVENT 5-------------------------------------------homepage------------------------------------------------"
		callback_params=json.dumps({"region":app_data.get('region'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"city":app_data.get('city'),"user_segment":"4","shop_country":device_data.get('locale').get('country'),"device_type":device_data.get('device_platform'),"amount_sessions":"1"})
		partner_params=json.dumps({"region":app_data.get('region'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"city":app_data.get('city'),"user_segment":"4","shop_country":device_data.get('locale').get('country'),"device_type":device_data.get('device_platform'),"amount_sessions":"1"})
		call_event(campaign_data, app_data, device_data,'vj9bs1',callback_params,partner_params)

		time.sleep(random.randint(1,2))
		print "\n EVENT 6--------------------------------------------homepage-----------------------------------------------"
		call_event(campaign_data, app_data, device_data,'qlbnsf')

		if random.randint(1,100)<=95:
			ch=random.choice([1,1,1,1,1,1,2,2,2,2])
			global products_view,brand_view,price_view
			products_view=[]
			brand_view=[]
			price_view=[]
			if ch==1:
				if len(product_list)>=3:
					limit=3
				else:
					limit=len(product_list)
				for i in range(0,limit):
					products_view.append(product_list[i])
					brand_view.append(brand_list[i])
					price_view.append(price_list[i])

				time.sleep(random.randint(10,60))
				print "\n EVENT 6--------------------------------------view_list-----------------------------------------------------"
				callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","products":str(products_view),"device_type":device_data.get('device_platform')})
				partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","products":str(products_view),"device_type":device_data.get('device_platform')})
				call_event(campaign_data, app_data, device_data,'1agzpl',callback_params,partner_params)
	
				time.sleep(random.randint(0,1))
				print "\n EVENT 7--------------------------------------view_list-----------------------------------------------------"
				callback_params=json.dumps({"region":app_data.get('region'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"city":app_data.get('city'),"user_segment":"4","skus":str(products_view),"shop_country":device_data.get('locale').get('country'),"device_type":device_data.get('device_platform'),"amount_sessions":"1"})
				partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","city":app_data.get('city'),"skus":str(products_view)})
				call_event(campaign_data, app_data, device_data,'k9ttpn',callback_params,partner_params)
	
				time.sleep(random.randint(1,3))
				print "\n EVENT 8--------------------------------------view_list-----------------------------------------------------"
				callback_params=json.dumps({"user_segment":"4"})
				partner_params=json.dumps({"criteo_p":urllib.quote(str(products_view)),"user_segment":"4"})
				call_event(campaign_data, app_data, device_data,'lr6wyl',callback_params,partner_params)

			else:
				product_list=[]
				price_list=[]
				brand_list=[]
				time.sleep(random.randint(2,6))
				print "==========search product=========="
				request=search_product(campaign_data, app_data, device_data)
				response=util.execute_request(**request)
				try:
					output=response.get('data').encode('utf-8')
					result=str(output)

					value=result.split('http://')

					for i in range(len(value)):
						print "-----------------------------------"
						if re.findall("\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w",value[i]):
							product= re.findall("\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w",value[i])
							product=product[0]
							if "_" in product:
								continue
							else:
								product_list.append(product)
							price= re.findall("\d+\.\d+", value[i])
							if price!=[]:
								price=price[0]
								price_list.append(price)	
							
							rep= re.findall("\w+|['&]",value[i])
							# print rep
							for i in range(len(rep)):
								if rep[i]=='00':
									start=i+1
									ext=str(rep[len(rep)-1])
									if len(ext)==1:
										end=len(rep)-1
									else:
										end=len(rep)
									
									brand=rep[start]
									if start!=end:
										for i in range(start+1,end):
											if rep[i]=="'" or rep[i-1]=="'":
												brand=brand+rep[i]
											else:
												brand=brand+' '+rep[i]
							brand_list.append(brand)

					if not 	product_list:	
						product_list=['FFC88AA73B9F46GS', 'D13B6AA33C06B3GS', '2CDCEAA9F9ECB0GS', '03388AA1133E21GS', '177C6AA7B7A231GS', '4BF4FAA2475BD3GS', '9CB71AA118C21FGS', '23C2EAABB75748GS', '11236AA6AC0437GS', '4DAB6AA7F45100GS', '2C434AAFD17B11GS', '2B47CAA6F04DF8GS', '228C8AA88B64EEGS', '08BA2AAEFEC83DGS', '31CCCAA7AD1874GS', '6099EAA7640557GS', 'CE364AA214E92BGS', '87539AA950DD84GS', 'BECC2AA51D6551GS', '77ACAAAE37B71DGS']
					
					if not 	price_list:
						price_list=['1250.00', '1150.00', '1250.00', '1450.00', '795.00', '1450.00', '995.00', '1250.00', '1250.00', '895.00', '795.00', '995.00', '995.00', '990.00', '1100.00', '1150.00', '555.00', '1150.00', '1150.00', '850.00']
					
					if not 	brand_list:
						brand_list=['MARKS & SPENCER', 'DEBENHAMS', 'BOSS', 'BOSS', 'MANGO Man', 'MARKS & SPENCER', 'MARKS & SPENCER', 'MARKS & SPENCER', 'MARKS & SPENCER', 'DEBENHAMS', 'Topman', 'MARKS & SPENCER', 'MARKS & SPENCER', 'STANCE', 'Calvin Klein', 'DEBENHAMS', 'ALDO', 'MARKS & SPENCER', 'MARKS & SPENCER', 'Tommy Hilfiger']
		

				except:
					print 'exception______'

					product_list=['FFC88AA73B9F46GS', 'D13B6AA33C06B3GS', '2CDCEAA9F9ECB0GS', '03388AA1133E21GS', '177C6AA7B7A231GS', '4BF4FAA2475BD3GS', '9CB71AA118C21FGS', '23C2EAABB75748GS', '11236AA6AC0437GS', '4DAB6AA7F45100GS', '2C434AAFD17B11GS', '2B47CAA6F04DF8GS', '228C8AA88B64EEGS', '08BA2AAEFEC83DGS', '31CCCAA7AD1874GS', '6099EAA7640557GS', 'CE364AA214E92BGS', '87539AA950DD84GS', 'BECC2AA51D6551GS', '77ACAAAE37B71DGS']
					
					price_list=['1250.00', '1150.00', '1250.00', '1450.00', '795.00', '1450.00', '995.00', '1250.00', '1250.00', '895.00', '795.00', '995.00', '995.00', '990.00', '1100.00', '1150.00', '555.00', '1150.00', '1150.00', '850.00']
					
					brand_list=['MARKS & SPENCER', 'DEBENHAMS', 'BOSS', 'BOSS', 'MANGO Man', 'MARKS & SPENCER', 'MARKS & SPENCER', 'MARKS & SPENCER', 'MARKS & SPENCER', 'DEBENHAMS', 'Topman', 'MARKS & SPENCER', 'MARKS & SPENCER', 'STANCE', 'Calvin Klein', 'DEBENHAMS', 'ALDO', 'MARKS & SPENCER', 'MARKS & SPENCER', 'Tommy Hilfiger']
				
				if len(product_list)>=3:
					limit=3
				else:
					limit=len(product_list)
				for i in range(0,limit):
					products_view.append(product_list[i])
					brand_view.append(brand_list[i])
					price_view.append(price_list[i])

				time.sleep(random.randint(30,60))
				print "\n EVENT 9--------------------------------------search item-----------------------------------------------------"
				callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"keywords":"Socks","user_segment":"4","device_type":device_data.get('device_platform')})
				partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"keywords":"Socks","user_segment":"4","device_type":device_data.get('device_platform')})
				call_event(campaign_data, app_data, device_data,'tppxtu',callback_params,partner_params)
	
				print "\n EVENT 10-------------------------------------search item------------------------------------------------------"
				callback_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"category":"Clothing","fb_content_type":"product","app_version":campaign_data.get('app_version_name'),"category_id":"95","query":"Socks","amount_sessions":"1","device_type":device_data.get('device_platform'),"user_segment":"4","city":app_data.get('city'),"region":app_data.get('region'),"segment":app_data.get('segment'),"fb_content_id":str(products_view)})
				partner_params=json.dumps({"category":"Clothing","app_version":campaign_data.get('app_version_name'),"city":app_data.get('city'),"category_id":"95","segment":app_data.get('segment'),"fb_content_id":str(products_view),"user_segment":"4","shop_country":device_data.get('locale').get('country'),"fb_currency":currency,"fb_content_type":"product"})
				call_event(campaign_data, app_data, device_data,'d5rgsj',callback_params,partner_params)
	
				print "\n EVENT 11-------------------------------------search item------------------------------------------------------"
				callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","products":str(products_view),"device_type":device_data.get('device_platform')})
				partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","products":str(products_view),"device_type":device_data.get('device_platform')})
				call_event(campaign_data, app_data, device_data,'1agzpl',callback_params,partner_params)

				print "\n EVENT 12--------------------------------------search item-----------------------------------------------------"
				callback_params=json.dumps({"region":app_data.get('region'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"city":app_data.get('city'),"user_segment":"4","skus":str(products_view),"shop_country":device_data.get('locale').get('country'),"device_type":device_data.get('device_platform'),"amount_sessions":"1"})
				partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","city":app_data.get('city'),"skus":str(products_view)})
				call_event(campaign_data, app_data, device_data,'k9ttpn',callback_params,partner_params)
	
				print "\n EVENT 13--------------------------------------search item-----------------------------------------------------"
				callback_params=json.dumps({"user_segment":"4"})
				partner_params=json.dumps({"criteo_p":urllib.quote(str(products_view)),"user_segment":"4"})
				call_event(campaign_data, app_data, device_data,'lr6wyl',callback_params,partner_params)

			if random.randint(1,100)<=88:
				global product_id,product_name,product_price
				index=random.randint(0,len(products_view)-1)
				product_id=products_view[index]
				product_name=brand_view[index]
				product_price=price_view[index]
				time.sleep(random.randint(5,60))
				print "\n EVENT 14--------------------------------------view product-----------------------------------------------------"
				callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"product":product_id,"user_segment":"4","device_type":device_data.get('device_platform')})
				partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"product":product_id,"user_segment":"4","device_type":device_data.get('device_platform')})
				call_event(campaign_data, app_data, device_data,'pakbsu',callback_params,partner_params)

				time.sleep(random.randint(1,2))
				print "\n EVENT 15--------------------------------------view product-----------------------------------------------------"
				callback_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"discount":"false","tree":"men,PDV","app_version":campaign_data.get('app_version_name'),"brand":product_name,"amount_sessions":"1","sku":product_id,"currency_code":currency,"device_type":device_data.get('device_platform'),"price":product_price,"user_segment":"4","city":app_data.get('city'),"region":app_data.get('region'),"segment":app_data.get('segment')})
				partner_params=json.dumps({"segment":app_data.get('segment'),"app_version":campaign_data.get('app_version_name'),"city":app_data.get('city'),"sku":product_id,"price":product_price,"fb_content_id":product_id,"user_segment":"4","shop_country":device_data.get('locale').get('country'),"brand":product_name,"fb_currency":currency,"fb_content_type":"product"})
				call_event(campaign_data, app_data, device_data,'5fcgo2',callback_params,partner_params)

				time.sleep(random.randint(0,1))
				print "\n EVENT 16--------------------------------------view product-----------------------------------------------------"
				callback_params=json.dumps({"user_segment":"4"})
				partner_params=json.dumps({"criteo_p":product_id,"user_segment":"4"})
				call_event(campaign_data, app_data, device_data,'w2rdm6',callback_params,partner_params)

				if random.randint(1,100)<=50:
					ch=random.choice([1,1,1,1,1,1,2,2,2,2])
					time.sleep(random.randint(5,30))
					if ch==1:
						print "\n EVENT 17--------------------------------------add to cart-----------------------------------------------------"
						callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_segment":"4","price":product_price,"shop_country":device_data.get('locale').get('country'),"currency_code":currency,"device_type":device_data.get('device_platform')})
						partner_params=json.dumps({"fb_content_id":product_id,"app_version":campaign_data.get('app_version_name'),"sku":product_id,"price":product_price,"user_segment":"4","shop_country":device_data.get('locale').get('country'),"fb_currency":currency,"device_type":device_data.get('device_platform'),"fb_content_type":"product"})
						call_event(campaign_data, app_data, device_data,'w3szhs',callback_params,partner_params)
					if ch==2:
						print "\n EVENT 17--------------------------------------add to cart-----------------------------------------------------"
						callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"price":product_price,"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"currency_code":currency,"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
						partner_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"fb_content_type":"product","criteo_email_hash":hash_digest(app_data),"age":str(age),"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_id":app_data.get('user_id'),"user_segment":"0","price":product_price,"device_type":device_data.get('device_platform'),"customer_id":app_data.get('user_id'),"fb_currency":currency,"fb_content_id":product_id})
						call_event(campaign_data, app_data, device_data,'w3szhs',callback_params,partner_params)

					app_data.get('cart_value').append([product_id,product_price]) 
					app_data['cart_price'] += float(product_price)
					app_data['in_cart'] += 1 	

				if random.randint(1,100)<=38:	
					ch=random.choice([1,1,1,1,1,1,2,2,2,2])
					time.sleep(random.randint(5,30))
					if ch==1:
						print "\n EVENT 18--------------------------------------add to wishlist-----------------------------------------------------"
						callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_segment":"4","price":product_price,"shop_country":device_data.get('locale').get('country'),"currency_code":currency,"device_type":device_data.get('device_platform')})
						partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_segment":"4","price":product_price,"shop_country":device_data.get('locale').get('country'),"currency_code":currency,"device_type":device_data.get('device_platform')})
						call_event(campaign_data, app_data, device_data,'i0lx69',callback_params,partner_params)
					if ch==2:
						print "\n EVENT 18--------------------------------------add to wishlist-----------------------------------------------------"
						callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"price":product_price,"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"currency_code":currency,"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
						partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"price":product_price,"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"currency_code":currency,"device_type":device_data.get('device_platform'),"customer_id":app_data.get('user_id'),"criteo_email_hash":hash_digest(app_data)})
						call_event(campaign_data, app_data, device_data,'i0lx69',callback_params,partner_params)

					app_data['in_wishlist'] += 1 	

				if random.randint(1,100)<=20 and app_data.get('in_wishlist')>0:
					time.sleep(random.randint(5,20))
					print "\n EVENT 19--------------------------------------remove from wishlist-----------------------------------------------------"
					callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_segment":"4","price":product_price,"shop_country":device_data.get('locale').get('country'),"currency_code":currency,"device_type":device_data.get('device_platform')})
					partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_segment":"4","price":product_price,"shop_country":device_data.get('locale').get('country'),"currency_code":currency,"device_type":device_data.get('device_platform')})
					call_event(campaign_data, app_data, device_data,'vgehf9',callback_params,partner_params)

				if random.randint(1,100)<=13:
					time.sleep(random.randint(10,60))
					print "\n EVENT 20--------------------------------------view wishlist-----------------------------------------------------"
					callback_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"sku":product_id,"user_id":app_data.get('user_id'),"app_version":campaign_data.get('app_version_name'),"amount_transactions":"1","currency_code":currency,"brand":product_name,"segment":app_data.get('segment'),"discount":"false","amount_sessions":"1","city":app_data.get('city'),"size":"9.5","quantity":"1","gender":app_data.get('gender'),"age":str(age),"total_wishlist":product_price,"user_segment":"1","price":product_price,"device_type":device_data.get('device_platform'),"colour":"BLACK","criteo_email_hash":hash_digest(app_data)})
					partner_params=json.dumps({"quantity":"1","shop_country":device_data.get('locale').get('country'),"criteo_email_hash":hash_digest(app_data),"age":str(age),"app_version":campaign_data.get('app_version_name'),"brand":product_name,"sku":product_id,"user_id":app_data.get('user_id'),"user_segment":"1","price":product_price,"total_wishlist":product_price,"city":app_data.get('city'),"customer_id":app_data.get('user_id'),"fb_currency":currency,"gender":app_data.get('gender'),"segment":app_data.get('segment')})
					call_event(campaign_data, app_data, device_data,'1qt9yi',callback_params,partner_params)

				if random.randint(1,100)<=55:
					print "\n EVENT 21--------------------------------------view cart-----------------------------------------------------"
					time.sleep(random.randint(10,60))
					for i in range(len(app_data.get('cart_value'))):
						callback_params=json.dumps({"total_cart":str(app_data.get('cart_price')),"shop_country":device_data.get('locale').get('country'),"sku":app_data.get('cart_value')[i][0],"user_id":app_data.get('user_id'),"app_version":campaign_data.get('app_version_name'),"amount_transactions":"1","currency_code":currency,"brand":product_name,"segment":app_data.get('segment'),"discount":"false","amount_sessions":"1","city":app_data.get('city'),"size":"9.5","quantity":"1","gender":app_data.get('gender'),"age":str(age),"user_segment":"1","price":app_data.get('cart_value')[i][1],"device_type":device_data.get('device_platform'),"colour":"BLACK","criteo_email_hash":hash_digest(app_data)})
						partner_params=json.dumps({"quantity":"1","discount":"false","shop_country":device_data.get('locale').get('country'),"criteo_email_hash":hash_digest(app_data),"age":str(age),"app_version":campaign_data.get('app_version_name'),"brand":product_name,"sku":app_data.get('cart_value')[i][0],"user_id":app_data.get('user_id'),"user_segment":"1","price":app_data.get('cart_value')[i][1],"city":"Manila","total_cart":str(app_data.get('cart_price')),"customer_id":app_data.get('user_id'),"fb_currency":currency,"gender":app_data.get('gender'),"segment":app_data.get('segment')})
						call_event(campaign_data, app_data, device_data,'fgw1ip',callback_params,partner_params)

					print "\n EVENT 21--------------------------------------view cart-----------------------------------------------------"
					callback_params={"gender":app_data.get('gender'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"age":str(age),"user_segment":"1","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)}
					partner_params={"gender":app_data.get('gender'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"age":str(age),"user_segment":"1","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"customer_id":app_data.get('user_id'),"criteo_email_hash":hash_digest(app_data)}
					for i in range(len(app_data.get('cart_value'))):
						if i==0:
							key = 'product'
						else:	
							key = 'product'+str((i))
						callback_params[key]=json.dumps({"quantity":"1","price":app_data.get('cart_value')[i][1],"currency_code":currency,"sku":app_data.get('cart_value')[i][0]})
						partner_params[key]=json.dumps({"quantity":"1","price":app_data.get('cart_value')[i][1],"currency_code":currency,"sku":app_data.get('cart_value')[i][0]})
					call_event(campaign_data, app_data, device_data,'jfvk77',json.dumps(callback_params),json.dumps(partner_params))

					print "\n EVENT 21--------------------------------------view cart-----------------------------------------------------"
					callback_params=json.dumps({"age":str(age),"user_segment":"1","criteo_email_hash":hash_digest(app_data)})
					partner_params=json.dumps({"age":str(age),"customer_id":app_data.get('user_id'),"user_segment":"1","criteo_p":urllib.quote(str([{"i":product_id,"pr":float(product_price),"q":1}])),"criteo_email_hash":hash_digest(app_data)})
					call_event(campaign_data, app_data, device_data,'7x3rgx',callback_params,partner_params)

					if random.randint(1,100)<=55 and app_data.get('in_cart')>0:
						if len(app_data.get('cart_value'))>0:
							n = random.randint(1,len(app_data.get('cart_value')))
							del_prod = app_data.get('cart_value')[n-1][0]
							del_price = app_data.get('cart_value')[n-1][1]
							time.sleep(random.randint(5,30))
							print "\n EVENT 22--------------------------------------remove from cart-----------------------------------------------------"
							callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":del_prod,"price":del_price,"age":str(age),"user_segment":"1","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"currency_code":currency,"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
							partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":del_prod,"price":del_price,"age":str(age),"user_segment":"1","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"currency_code":currency,"device_type":device_data.get('device_platform'),"customer_id":app_data.get('user_id'),"criteo_email_hash":hash_digest(app_data)})
							call_event(campaign_data, app_data, device_data,'ovz729',callback_params,partner_params)

							del app_data.get('cart_value')[n-1]

					if purchase.isPurchase(app_data,day=1,advertiser_demand=45):

						if len(app_data.get('cart_value'))>0 and app_data.get('login_flag')==True:
							n = random.randint(1,len(app_data.get('cart_value')))
							del_prod = app_data.get('cart_value')[n-1][0]
							del_price = app_data.get('cart_value')[n-1][1]

							app_data['transaction_id'] = str(random.randint(210000000,219999999))
							app_data['fb_order_id'] = device_data.get('locale').get('country')+app_data.get('transaction_id')

							print "\n EVENT 23--------------------------------------purchase -----------------------------------------------------"
							callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform')})
							partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform')})
							call_event(campaign_data, app_data, device_data,'m1q07h',callback_params,partner_params)

							print "\n EVENT 23--------------------------------------purchase -----------------------------------------------------"
							callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"transaction_id":app_data.get('transaction_id'),"skus":"[\""+del_prod+"\"]","device_type":device_data.get('device_platform')})
							partner_params=json.dumps({"app_version":"6.13.0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"transaction_id":app_data.get('transaction_id'),"skus":"[\""+del_prod+"\"]","device_type":device_data.get('device_platform')})
							call_event(campaign_data, app_data, device_data,'w7stgj',callback_params,partner_params)

							print "\n EVENT 23--------------------------------------purchase -----------------------------------------------------"
							callback_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"sku":del_prod,"user_id":app_data.get('user_id'),"transaction_id":app_data.get('transaction_id'),"amount_transactions":"1","app_version":campaign_data.get('app_version_name'),"brand":product_name,"currency_code":currency,"discount":"false","amount_sessions":"1","fb_order_id":app_data.get('fb_order_id'),"city":app_data.get('city'),"region":app_data.get('region'),"segment":app_data.get('segment'),"new_customer":"true","size":"One Size","quantity":"1","gender":app_data.get('gender'),"age":str(age),"user_segment":"1","price":del_price,"device_type":device_data.get('device_platform'),"colour":"Navy","total_transaction":del_price,"criteo_email_hash":hash_digest(app_data)})
							partner_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"sku":del_prod,"user_id":app_data.get('user_id'),"transaction_id":app_data.get('transaction_id'),"fb_content_type":"product","app_version":campaign_data.get('app_version_name'),"brand":product_name,"fb_order_id":app_data.get('fb_order_id'),"segment":app_data.get('segment'),"city":app_data.get('city'),"customer_id":app_data.get('user_id'),"new_customer":"true","gender":app_data.get('gender'),"fb_currency":currency,"age":str(age),"user_segment":"1","price":del_price,"category_id":"354","fb_content_id":del_prod,"total_transaction":del_price,"criteo_email_hash":hash_digest(app_data)})
							call_event(campaign_data, app_data, device_data,'xs26pa',callback_params,partner_params)

							print "\n EVENT 23--------------------------------------purchase -----------------------------------------------------"
							callback_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"criteo_email_hash":hash_digest(app_data),"age":str(age),"transaction_id":app_data.get('transaction_id'),"app_version":campaign_data.get('app_version_name'),"product":json.dumps({"quantity":"1","price":del_price,"currency_code":currency,"sku":del_prod}),"new_customer":"true","user_id":app_data.get('user_id'),"user_segment":"1","device_type":device_data.get('device_platform'),"gender":"male","segment":"men"})
							partner_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"criteo_email_hash":hash_digest(app_data),"age":str(age),"transaction_id":app_data.get('transaction_id'),"app_version":campaign_data.get('app_version_name'),"product":json.dumps({"quantity":"1","price":del_price,"currency_code":currency,"sku":del_prod,}),"new_customer":"true","user_id":app_data.get('user_id'),"user_segment":"1","device_type":device_data.get('device_platform'),"customer_id":app_data.get('user_id'),"gender":"male","segment":"men"})
							call_event(campaign_data, app_data, device_data,'ji2gpx',callback_params,partner_params)

							print "\n EVENT 23--------------------------------------purchase -----------------------------------------------------"
							callback_params=json.dumps({"age":str(age),"user_segment":"1","criteo_email_hash":hash_digest(app_data)})
							partner_params=json.dumps({"transaction_id":app_data.get('transaction_id'),"new_customer":app_data.get('user_id'),"age":str(age),"user_segment":"1","customer_id":app_data.get('user_id'),"criteo_p":urllib.quote(str([{"i":del_prod,"pr":del_price,"q":1}])),"criteo_email_hash":hash_digest(app_data)})
							call_event(campaign_data, app_data, device_data,'dhrfn9',callback_params,partner_params)	

							del app_data.get('cart_value')[n-1]		


		if random.randint(1,100)<=15 and app_data.get('login_flag')==True:	
			time.sleep(random.randint(10,60))
			print "\n EVENT 22----------------------------------logout ---------------------------------------------------------"
			callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"user_segment":"0","user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
			partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"customer_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
			call_event(campaign_data, app_data, device_data,'3xifab',callback_params,partner_params)	

			app_data['login_flag'] = False	

			if random.randint(1,100)<=80 and app_data.get('login_flag')==False and app_data.get('signup_flag')==True:	
				time.sleep(random.randint(30,60))
				print "\n EVENT 22----------------------------------login ---------------------------------------------------------"
				callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
				partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"customer_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
				call_event(campaign_data, app_data, device_data,'1qbeau',callback_params,partner_params)	

				app_data['login_flag'] = True			 
 
	###########		Finalize	############
	set_appCloseTime(app_data)

	return {'status':True}

###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):

	###########		Initialize		############
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)

	def_sec(app_data,device_data)

	if not app_data.get('appCloseTime'):
		app_data['appCloseTime']=int(time.time())

	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())	

	if not app_data.get('installed_at'):
		app_data['installed_at'] = datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	return_userinfo(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'subsession_count')
	def_(app_data,'session_count')
	inc_(app_data,'session_count')
		
	if not app_data.get('signup_flag'):
		app_data['signup_flag']=False
	if not app_data.get('login_flag'):	
		app_data['login_flag']=False
	if not app_data.get('in_cart'):	
		app_data['in_cart']=0
	if not app_data.get('in_wishlist'):	
		app_data['in_wishlist']=0
	if not app_data.get('cart_value'):	
		app_data['cart_value'] = list()
	if not app_data.get('cart_price'):	
		app_data['cart_price'] = 0

	global currency
	currency = util.country_list.get(device_data.get('locale').get('country')).get('currency')

	global session_time
	session_time=get_date(app_data,device_data)
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,call_type='open')
	util.execute_request(**adjustSession)	

	app_data['adjust_call_time'] = int(time.time())

	global self_sent_at
	self_sent_at=datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'
	time.sleep(random.randint(60,100))
	
	global click_created
	click_created=get_date(app_data,device_data)
	time.sleep(random.randint(10,12))

	global event_sent
	event_sent=get_date(app_data,device_data)
	time.sleep(random.randint(10,12))

	global info_created
	info_created=get_date(app_data,device_data)
	time.sleep(random.randint(6,8))

	global info_sent
	info_sent=get_date(app_data,device_data)

	if app_data.get('user_info').get('sex')=='male':
		app_data['segment']='men'

	else:
		app_data['segment']='women'

	global screen_size

	screen_size=random.choice(['640.000000/1136.000000','750.000000/1334.000000'])

	print '*************Install**********************'

	###########		Initialize		############
	app_data['device_id']=str(uuid.uuid4()).upper()
	app_data['binary_uid']=util.get_random_string('hex',32)
				
	###########			Calls		############ 

	print 'lat and lon generator'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	try:
		json_lat_lon_result=json.loads(lat_lon_result.get('data')).get('geo')
		app_data['lat']=str(json_lat_lon_result.get('latitude'))
		app_data['lon']=str(json_lat_lon_result.get('longitude'))
		app_data['city']=str(json_lat_lon_result.get('city'))
		app_data['region']=str(json_lat_lon_result.get('region'))
		app_data['postal_code']=str(json_lat_lon_result.get('postal_code'))
	except:
		print "Exception"
		app_data['lat']= '28.6667'
		app_data['lon']= '77.2167'
		app_data['city']='Noida'
		app_data['region']='Uttar Pradesh'
		app_data['postal_code']='110091'


	# req=selfcall1(campaign_data, app_data, device_data)
	# util.execute_request(**req)

	# req=selfcall2(campaign_data, app_data, device_data)
	# util.execute_request(**req)

	app_data['device_id1']=str(uuid.uuid4()).upper()

	global cacheKey, colors, countries, version

	request=zalora_init(campaign_data, app_data, device_data, call=1)
	response=util.execute_request(**request)	
	try:
		output=response.get('data').encode('utf-8')
		soup = str(BeautifulSoup(output,"html.parser"))

		cacheKey=str(str(soup).split('cms/staticBlock/about-us-app')[3].split('https://dynamic.zacdn.com')[0]).split(' ')[1][:32]

		colors=str(str(soup).split('Bhttps://www.zalora.sg/customer/exchange_return/detail/?order_id=%s')[7].split('black')[0]).split(' ')[1][:32]

		countries=str(str(soup).split('newarrivals_kids_750x248')[6].split('BNFAS')[0]).split(' ')[1][:32]

		version=str(str(soup).split('https://dynamic.zacdn.com')[1].split('3.0.0')[0]).split(' ')[1][:32]

	except:
		print 'exception______'
		cacheKey='7de05146ccac8091029d03136b33fb77'
		colors='0929d0bb8e65dc10dccce5c9314d1be8'
		countries='ed98f554dceb16ba7187240aad5318dc'
		version='ba0ef964056309e77a28726bcba30ba0'

	# req=selfcall5(campaign_data, app_data, device_data)
	# util.execute_request(**req)	

	# req=selfcall3(campaign_data, app_data, device_data)
	# util.execute_request(**req)

	# request=zalora_catalog_categories(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	time.sleep(random.randint(10,15))
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data,source='iad3')
	util.execute_request(**adjustSession)	

	time.sleep(random.randint(5,8))
	request=measurement(campaign_data, app_data, device_data)
	util.execute_request(**request)

	# time.sleep(random.randint(5,8))
	# request=zalora_init(campaign_data, app_data, device_data, call=2)
	# util.execute_request(**request)	

	# req=selfcall4(campaign_data, app_data, device_data)
	# util.execute_request(**req)
	
	# request=zalora_catalog_categories(campaign_data, app_data, device_data, call=2)
	# util.execute_request(**request)

	# request=zalora_webcontent(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	# request=zalora_homescreen(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	# request=form_login(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	time.sleep(random.randint(8,10))
	request=zalora_feed(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	global csa_list
	csa_list=[]
	try:
		output=json.loads(response.get('data')).get('data')
		print json.dumps(output[0])

		for i in range(0, len(output)):
			if output[i].get('data').get('media'):
				csa=str(output[i].get('data').get('media').get('data').get('internal_promotion_name'))
				csa_list.append(csa)

			else:
				pass

		app_data['csa']=random.choice(csa_list).split('|')[2].split('|')[0]

		if not csa_list:
			csa_list=['SVC|Men|cwfourcnyredthirtyfive|All|NA', 'SVC|Men|cwfourwinterflashsale|All|NA', 'SRE|Men|cwfourwinterwear|All|NA']

	except:
		print 'exception______'
		csa_list=['SVC|Men|cwfourcnyredthirtyfive|All|NA', 'SVC|Men|cwfourwinterflashsale|All|NA', 'SRE|Men|cwfourwinterwear|All|NA']
		app_data['csa']='cwfourcnyredthirtyfive'

	time.sleep(random.randint(1,2))
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data,source='deeplink')
	util.execute_request(**adjustSession)

	# time.sleep(random.randint(35,40))
	# request=zalora_tutorial(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	time.sleep(random.randint(1,2))
	print "\n EVENT 1--------------------------------------choose country-----------------------------------------------------"
	callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"user_segment":"4","device_type":device_data.get('device_platform'),"duration":"0"})
	partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"user_segment":"4","device_type":device_data.get('device_platform'),"duration":"0"})
	call_event(campaign_data, app_data, device_data,'kyc4ia',callback_params,partner_params)

	duration=str(random.randint(7000,7500))
	print "\n EVENT 2---------------------------------------choose gender----------------------------------------------------"
	callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"user_segment":"4","device_type":device_data.get('device_platform'),"duration":duration})
	partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"user_segment":"4","device_type":device_data.get('device_platform'),"duration":duration})
	call_event(campaign_data, app_data, device_data,'kyc4ia',callback_params,partner_params)

	# request=form_login(campaign_data, app_data, device_data, call=2)
	# util.execute_request(**request)

	# request=zalora_register(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	# global cookie1, doraemon_sessionId1, doraemon_sessionId2, doraemon_sessionId3

	if not app_data.get('cookie1'):
		time.sleep(random.randint(30,35))
		request=zalora_customers(campaign_data, app_data, device_data)
		response=util.execute_request(**request)
		try:
			output=response.get('data').encode('utf-8')

			app_data['cookie1']=response.get('res').headers.get('Set-Cookie').split(';')[0]
			app_data['doraemon_sessionId1']=response.get('res').headers.get('Set-Cookie').split('doraemonSessionId=')[1].split(';')[0]

			soup = str(BeautifulSoup(output,"html.parser"))

			temp=(str(soup).split(' ')[2])
			app_data['user_id']= re.findall("\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d",temp)[0]

		except:
			print 'exception______'
			app_data['user_id']='10815480715988554'
			app_data['cookie1']='f59f9fd9576397c679c3d3eb52622f8d=de08e001f0df57f6a497310b68d53c31d2d4fe4c25b58152e9225886436bc779b8e26c9d0ad0da718d225f5d8a03bdd604daa0e17021a73ed5d81f06953c602e%3A529146d5cda785a6684096ebb240a34a2b97b6816a06f1c529f9f6563c935daf'
			app_data['doraemon_sessionId1']='MTU0ODA3MTU5OXxsclNiMTU0OF9CUXAwLWRIa0dlWmI0d0Q4SHpPMmF1cXlUb0MwVWRta1lNOXc1MmJneHpOTkNIYmQxMkxvNENjS0xHYVlsUklhZUlLTkRTSmtPQUdtNThSTVZrODZxNXZyQmc4b1l2d3NjMklrRkVGNkU5aVk5ZkJqYUI0ZkZRMWZhWU5GdUFYNnRzOUxiTW5fcWd0M3oxZGgtSE1xMXVTbVIya2ZhY1JKVy0wLS1qTWQ1VTVaX1IwWXJ6YXJZLS1sQUNGVVgyWW8tb1BUNHdzMUpfeHhDaWIweDNhM0QtUVhsWFQyVTBSRXZmRWxjcEdIcEt2TGpLTzhjOHFKSUZxR2FQb25kX0RLZnlDZ1JWZVNwb0NtdGFWSHJkWjRsOEtoZU5YbG1xSzl4S3ZWWjlaZnJ4ZVpGd1hvdmN5NElUNkotVFhJVEJoenBBYWkyZ0E4MUNGT3N3NzlPU2k1OEE5dHBHTE9kTi1oT3ZldHprWlRrNnl6Mm03M0NwSFdTN0pIYldmZTU4d05LZlpnNmI0ZkRpcnQxa3h0LWxTTEFsQjc5azdqZk10Ri1LUXdjMXZQZlRIcmZhYTlLSllQeldDVUJrLVJkSTZxdlpNQklGdnlSLTk2R3BYQnBuMURoZWFTbzlQaGdjS2hZa0pycU9HS1ZCY1NSOWlQQ0Qwc05fYkoyc3dVSWF1X2ZydjRuOHZ3VDlSWjlnYWFJOV82bVFhU29kYnpLWDl4MDZDUUNfZUZKOXoyWlNUamhzMWVXWkNwWXhEeUZkWVVkNlRaR2wyRmZLYWxfSWgwVVlBOFVCY2F6OEllSjRZdGFkNEloZFREbTJ3Q0VwTnhYaHowVENKYzhDcmxxNHd3MWVCR0tKNElzeEhKYV9tdW5TUE8xNS1wVmVpUkwyWkNzeGZVTWw1aGlFTHZucnYwUWttejhwOWY3RG1YS2cyWlRlbkYya2NLdV9hYnVhV2RzbloyZ1JzM25pWEdJZHJUOWFCNXJ5WGhYdTAxaDkzN3VTa1ctRDJndzhrTDJHSlNpM2U0cHFYcmFINzBSeURWWXNxTGtnZmUySFFwZlBRM2QzS1RNRXlaZWpwZl8zdkNRRk1wc05Wbjc4UnY0Yi1XQUV4VmI5RDE2VDFzOGtxRWpjVm9OZXo1LTRaTGxfcHdWakkxdnJxM0cyQTQ4VkpaWjBRZGo1OHEwOTY3ZXd5eC1KTkJNNnFvYjNETVd2X0diRXVQSUR4YWV0TFA5RlhWNGc5THFaak0wZkVxcW50S2JkM2JKd1JDdnFKejBlNVRzaWViTDNpWV9hUEExOFFRZGxoeXA4MHVxMG1hX29TUmM2S1pxYXNzRm9zZjd1M1h5VE1EdzBsUEpkVHdqRnRkSUJSQ0FyZ0piQ2stbkdsaTRfb1UyVDlBNlE4Y3NBPXxA-yDtHe1UDmGwOSWT4y7xVSn-vw%3D%3D'

	if not app_data.get('doraemon_sessionId2'):
		time.sleep(random.randint(1,2))
		request=zalora_checkoutSync(campaign_data, app_data, device_data)
		response=util.execute_request(**request)
		try:
			app_data['doraemon_sessionId2']=response.get('res').headers.get('Set-Cookie').split('doraemonSessionId=')[1].split(';')[0]

		except:
			print 'exception______'
			app_data['doraemon_sessionId2']='MTU0ODA3MTYxOXxvbDhvT3dGakFYdEU5SjU0WGVhSHhEeGVvV05HUmFmT0NTYUxjUktPNUdiNFhhcFB5TElWblAyRTlLWVFpVzIzMi1pU3NvdXhIdHNVTlFfeldIVHRmUUw1NmJTajhRNGQ1aHFrRjBKcjF4N0FTUWtyYlFheTVsYUNlQ2I1VUVhalY1VlFUOWx2RHB3a0hUbTdGX1ZsUS1VaXJiLVo5bThMY0lmN1V1UWhlU2l6aWJ5NWhMcDRJX3lmWmhtNHQ2QWU5ZHE0Mm5ndWs4VE1oWEhBM2hxazlzMFFDbEdxTVpodGU3Y3pvendWUjd3bThiWDNOQU9EdVZiOUlmSlhpMVhHeVpwLVluMk8ybER6bzE0c2lsVHdXVzFNOTQwRlhvT216eWtMN1Q4S1hKamRMSm5kUHFRRnZDaHdqaXBXWllIeTg1M2F6V0dGbE5Gb0g4N08zMzNXWWRaeHdCUUxYSmNjd1JIaFNsTkEzZThPOEVXOTRHWkxRNFVTVlBDRm14UWlHUndoWmNTRTV0R083ZklBMkhCUnJ6OG82RGVzbHU3aUwxUFpRUTZfOGNDcTluRWZtTk5RY3haR0xGcG1vZmh5VGZ4T1BZaDc5NUFtNUgydHhTaTB4R2ZadDBkcVJBQVdINDFMU29aeFA4QUMzQW9lZkMtUUdadGR4STVwaEY4WTZSRVE4VXJIQU11VWRhMXhrVTBDdXhWQXgzbXJLSDQ3VExYYlNfVHc0WWFZNHJNV1F6bl93eFdYUENfc25xbzVCbk1GVmM3NjNZR0R6bmRuODRhcjMwclpIYmFVM1kyYjlLa0dDOHpneGZUYVY4aFRpVnhmRkViSlhLd2Q2MXIwNy1WX0ktWWFQRUxwTGRmT0h4OENoWVpNWi1HTWM3b081RzBoMTVQakFnVE1yUjg0dVYwSlAxLW5PdURraXBGcy04UmRkV0MxSVNyVGZwU1ZITVprVDhlQjREeEFRbWh6TW13d3Q5ckI2eHR5bUZTZzFESmc4d1M2WHdOMkxBajdvRkt6U1llQkxKN3BfUWhjeTUwWUVmLUtxSTFrckRYaTFqUVVvZGl3cmxtNzlHb2RIQjFnWWZ6SXJjWnJfeW1NNlVqazljclc0d3pHZEJkNEJqOENjc3NyZ2ZHNEFkekowZHIyNUNLVjJpYmxsX3JGR1hYXy1pRnMtRzQyeGMxNnVtaENuUEtoUmRjYjA4WFZOUGN5OVhUX1c3NUhVSUJfRWpPamFnc0JwazJNZ2JtN0hOZzdNR0h3c3k5QnRsYm1UdVYxOVpsU1FlcDZoc0lmcEZHbmVUam00YXczcy1ZZ1BTa1VGYU53SjI0cUtpN1lWMkNUSHdmTkxsbkZyclVLOURDd3VqN3huZlFZV2JCcnJ3bWd4NGxOZW9RZHF2dTUydVZmajBJPXy0XOCqmARGsyKYYfeFLATf_4U2Hw%3D%3D'

	if not app_data.get('doraemon_sessionId3'):
		time.sleep(random.randint(1,2))
		request=zalora_addresses(campaign_data, app_data, device_data)
		response=util.execute_request(**request)
		try:
			app_data['doraemon_sessionId3']=response.get('res').headers.get('Set-Cookie').split('doraemonSessionId=')[1].split(';')[0]
			print 'doraemon_sessionId3___________'

		except:
			print 'exception______'
			app_data['doraemon_sessionId3']='MTU0ODA3MTY0MXxjOGM5TFMwd0tNMEpTRXhGRXdGMDRFb3BlV1MtSkVoQVNiSnBqTjloY2ttNU1MLUp6VzczUmlCUkhldFRkbWhSQVNvOWtNQkR5MlNDUnNPNWFHOXBlRFRWOG9hME9EUW95ZDFwMUVjNXdJUFdZVjd5SEY2eTBEMU4xWWdoX3RESWxtenc3VTRCUEY0cHU2elIxVUJsQjJhQmRTQkw1aEhVcDcyLWFoakZSSU9TWHJobnhlQUZhYUFqeDZoTVNwU3hYZHhOSWEwSGJzLXo4eU9zdXkza0xPLUhUQXRReFJzWXN4RmxiM1ZDRkg2dHBJa0dnUXVCQmo3bWU2NFZoQ1A4SzUySFo3Znp3aThJRXc3RFBpNmtHZXBIaG16bUZ0VlpjcW5JNGtwMDAyZjZXMmdjWkI1WHhYN2wybENFa3liVlJBVDlMY0JwUVFvMXJUSGduak1zY2pTZE1jTDRKQ09rYzh0elM2X002RUxJUVlFNWlQTUZVQnZvaFFfU21OWmYxU3Uta2YwQUQtLTFHUjhJR211eTNzbUZreWVhUlB5X0FmVHlVN3lLX1FNTWlpQkZPNXJjekpsY1lxZjRPcnhsOW1pNklNTkExbHZVR1BkaHVCbk9FNlQyQnJpRDQ5N0dzVFlfZHpDYzhCblR0cFFXaWdDRGxIaFVBVmdMR2paV2VwbzdaVms4bWRmTEZOanhMYmxyU1IwX3kwTFhhUVJVOTBJemR5amdxVnJKcUd2aE44a3dKYVZZYk4wTWMxVGVERXYyNC0yS29NMzVjLWNLcDY3SngwT0J3bG01N2Q4c2tQb1Q4WjdhSnkxRWhwelExMHRaaFFXd203X3VYcWw2VTZQblRvQzNCUVpaT3lleTRHa2NxYVA0czQ3cDdNMzlPZVRhdEpaeFdYS0ZMWXVycF9lcFVTMzZmblUydFAzdy15SnM0Qk1ZWDF4X0VvYnd6TWY5ZGNpRjUyQTMzNWFaalNUN2paZ3ZyNjMwVGdRajd1dEZtZlZZeGNqckkxZmxGWXBkMUlhejM5VnVnVEp2MHhfdmRHd3ZLb19GcEotWHRQcnE2YUNWRmVNc1RXdndCUUJENDlWNDd2TUlWUmlJZGdVWkw4eWx2Q3VpZnlBQmc3N216STBJWEpBckh3NlRzSVBQcnlFNkZYWXVjZE81QlZ3aW05bko4MlFwbHRESVZpRmozYlRTMTF3YWVpWGQ4ZHF4V0t5QWV0dURwNzZxbnMydy1jaGFfdFhNTzB2U19LSlltWTFTUUl6eUFMMEJfc3JlcF9ocHhQd21KU0tSaUhSbURrMGxnQVhGVEQ3dVRseEFfLXJsV3pGd1kwYkZlUERRT1RJOUJaNFJBVHM3YkdOMjVPdHFzdjNhTTl2dG81MXM2WUhCZ0lOUEZlOHdjYjlmTzZNPXxCF4A6_QgVtkQwfKdRryVkvQ8R-Q%3D%3D'
	
	global checkout_cookie_key,checkout_cookie_value
	print "_______________checkout_coupon_________________"
	request=checkout_coupon(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	try:
		cookie=response.get('res').headers.get('Set-Cookie')
		checkout_cookie_key=cookie.split('=')[0]
		checkout_cookie_value=cookie.split('=')[1].split(';')[0]
	except:
		checkout_cookie_key="dorpc_d50acd0c74fa3d0cb876238f029fe091"
		checkout_cookie_value="P6FXa4kir17wgBTSyuaCnwLmr6fAIE2j"
	

	global product_list,price_list,brand_list
	
	time.sleep(random.randint(2,6))
	print "_______________product list_________________"
	request=zalora_products(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	product_list=[]
	price_list=[]
	brand_list=[]
	try:
		output=response.get('data').encode('utf-8')
		result=str(output)

		value=result.split('http://')

		for i in range(len(value)):
			print "-----------------------------------"
			if re.findall("\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w",value[i]):
				product= re.findall("\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w",value[i])
				product=product[0]
				if "_" in product:
					continue
				else:
					product_list.append(product)
				price= re.findall("\d+\.\d+", value[i])
				# print price
				if len(price)==2:
					price_list.append(price[1])
				else:
					price_list.append(price[0])
				
				rep= re.findall("\w+|['&]",value[i])
				# print rep
				for i in range(len(rep)):
					if rep[i]=='00':
						start=i+1
						ext=str(rep[len(rep)-1])
						if len(ext)==1:
							end=len(rep)-1
						else:
							end=len(rep)
						
						brand=rep[start]
						if start!=end:
							for i in range(start+1,end):
								if rep[i]=="'" or rep[i-1]=="'":
									brand=brand+rep[i]
								else:
									brand=brand+' '+rep[i]
				brand_list.append(brand)

		if not product_list:		
			product_list=['42DC0AA72390BBGS', '2FEBBAA52816C4GS', '73176AAC229025GS', 'DA9C3AA19DEC30GS', 'A3712AA204FCBCGS', '9F74EAAC2D2B03GS', 'AA077SHAC56FD1GS', '753BASH6E5417AGS', '30637SHA6F1D5AGS', '11B3DSH8004762GS', 'CBE7AAA6813617GS', '61FA9AAEF6E1A9GS', '70A6EAA762DB1AGS', '76960AA5A13BB3GS', '4CA1DAA55CC179GS', '8D129AAE5DDC72GS', '95BE5AA872A179GS', '80B94AA9500E4CGS', '8F607AABFE14C6GS', 'AA9F5AADC79122GS']
		
		if not price_list:
			price_list=['249.00', '249.00', '239.00', '249.00', '620.00', '530.00', '1599.00', '989.00', '989.00', '489.00', '799.00', '169.00', '249.00', '460.00', '799.00', '229.00', '540.00', '899.00', '229.00', '659.00']
		
		if not brand_list:
			brand_list=['ZALORA', 'ZALORA', 'ZALORA', 'Something Borrowed', 'Abercrombie & Fitch', 'Hollister', 'Geox', 'ALDO', 'ALDO', 'Keds', 'Mango', 'ZALORA BASICS', 'ZALORA', 'Dorothy Perkins', 'Lipsy', 'Something Borrowed', 'TOPSHOP', 'ESPRIT', 'ZALORA BASICS', 'Ivy Park']
		

	except:
		print 'exception______'

		product_list=['42DC0AA72390BBGS', '2FEBBAA52816C4GS', '73176AAC229025GS', 'DA9C3AA19DEC30GS', 'A3712AA204FCBCGS', '9F74EAAC2D2B03GS', 'AA077SHAC56FD1GS', '753BASH6E5417AGS', '30637SHA6F1D5AGS', '11B3DSH8004762GS', 'CBE7AAA6813617GS', '61FA9AAEF6E1A9GS', '70A6EAA762DB1AGS', '76960AA5A13BB3GS', '4CA1DAA55CC179GS', '8D129AAE5DDC72GS', '95BE5AA872A179GS', '80B94AA9500E4CGS', '8F607AABFE14C6GS', 'AA9F5AADC79122GS']
		
		price_list=['249.00', '249.00', '239.00', '249.00', '620.00', '530.00', '1599.00', '989.00', '989.00', '489.00', '799.00', '169.00', '249.00', '460.00', '799.00', '229.00', '540.00', '899.00', '229.00', '659.00']
		
		brand_list=['ZALORA', 'ZALORA', 'ZALORA', 'Something Borrowed', 'Abercrombie & Fitch', 'Hollister', 'Geox', 'ALDO', 'ALDO', 'Keds', 'Mango', 'ZALORA BASICS', 'ZALORA', 'Dorothy Perkins', 'Lipsy', 'Something Borrowed', 'TOPSHOP', 'ESPRIT', 'ZALORA BASICS', 'Ivy Park']

	age=int(app_data.get('installed_at').split('-')[0])-int(app_data.get('user_info').get('dob').split('-')[0])
	
	if app_data.get('signup_flag')==False:

		time.sleep(random.randint(0,1))
		print "\n EVENT 3-----------------------------------------signup--------------------------------------------------"
		callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
		partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"customer_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
		call_event(campaign_data, app_data, device_data,'n6uqc2',callback_params,partner_params)
		
		app_data['signup_flag']=True
		app_data['login_flag']=True

	time.sleep(random.randint(1,2))
	print "\n EVENT 4------------------------------------------homepage-------------------------------------------------"
	callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","device_type":device_data.get('device_platform')})
	partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","device_type":device_data.get('device_platform')})
	call_event(campaign_data, app_data, device_data,'i5whdy',callback_params,partner_params)

	time.sleep(random.randint(1,2))
	print "\n EVENT 5-------------------------------------------homepage------------------------------------------------"
	callback_params=json.dumps({"region":app_data.get('region'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"city":app_data.get('city'),"user_segment":"4","shop_country":device_data.get('locale').get('country'),"device_type":device_data.get('device_platform'),"amount_sessions":"1"})
	partner_params=json.dumps({"region":app_data.get('region'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"city":app_data.get('city'),"user_segment":"4","shop_country":device_data.get('locale').get('country'),"device_type":device_data.get('device_platform'),"amount_sessions":"1"})
	call_event(campaign_data, app_data, device_data,'vj9bs1',callback_params,partner_params)

	time.sleep(random.randint(1,2))
	print "\n EVENT 6--------------------------------------------homepage-----------------------------------------------"
	call_event(campaign_data, app_data, device_data,'qlbnsf')

	if random.randint(1,100)<=80:
		ch=random.choice([1,1,1,1,1,1,2,2,2,2])
		global products_view,brand_view,price_view
		products_view=[]
		brand_view=[]
		price_view=[]
		if ch==1:
			if len(product_list)>=3:
				limit=3
			else:
				limit=len(product_list)
			for i in range(0,limit):
				products_view.append(product_list[i])
				brand_view.append(brand_list[i])
				price_view.append(price_list[i])

			time.sleep(random.randint(10,60))
			print "\n EVENT 6--------------------------------------view_list-----------------------------------------------------"
			callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","products":str(products_view),"device_type":device_data.get('device_platform')})
			partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","products":str(products_view),"device_type":device_data.get('device_platform')})
			call_event(campaign_data, app_data, device_data,'1agzpl',callback_params,partner_params)

			time.sleep(random.randint(0,1))
			print "\n EVENT 7--------------------------------------view_list-----------------------------------------------------"
			callback_params=json.dumps({"region":app_data.get('region'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"city":app_data.get('city'),"user_segment":"4","skus":str(products_view),"shop_country":device_data.get('locale').get('country'),"device_type":device_data.get('device_platform'),"amount_sessions":"1"})
			partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","city":app_data.get('city'),"skus":str(products_view)})
			call_event(campaign_data, app_data, device_data,'k9ttpn',callback_params,partner_params)

			time.sleep(random.randint(1,3))
			print "\n EVENT 8--------------------------------------view_list-----------------------------------------------------"
			callback_params=json.dumps({"user_segment":"4"})
			partner_params=json.dumps({"criteo_p":urllib.quote(str(products_view)),"user_segment":"4"})
			call_event(campaign_data, app_data, device_data,'lr6wyl',callback_params,partner_params)

		else:
			product_list=[]
			price_list=[]
			brand_list=[]
			time.sleep(random.randint(2,6))
			print "==========search product=========="
			request=search_product(campaign_data, app_data, device_data)
			response=util.execute_request(**request)
			try:
				output=response.get('data').encode('utf-8')
				result=str(output)

				value=result.split('http://')

				for i in range(len(value)):
					print "-----------------------------------"
					if re.findall("\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w",value[i]):
						product= re.findall("\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w\w",value[i])
						product=product[0]
						if "_" in product:
							continue
						else:
							product_list.append(product)
						price= re.findall("\d+\.\d+", value[i])
						if price!=[]:
							price=price[0]
							price_list.append(price)	
						
						rep= re.findall("\w+|['&]",value[i])
						# print rep
						for i in range(len(rep)):
							if rep[i]=='00':
								start=i+1
								ext=str(rep[len(rep)-1])
								if len(ext)==1:
									end=len(rep)-1
								else:
									end=len(rep)
								
								brand=rep[start]
								if start!=end:
									for i in range(start+1,end):
										if rep[i]=="'" or rep[i-1]=="'":
											brand=brand+rep[i]
										else:
											brand=brand+' '+rep[i]
						brand_list.append(brand)

				if not product_list:		
					product_list=['FFC88AA73B9F46GS', 'D13B6AA33C06B3GS', '2CDCEAA9F9ECB0GS', '03388AA1133E21GS', '177C6AA7B7A231GS', '4BF4FAA2475BD3GS', '9CB71AA118C21FGS', '23C2EAABB75748GS', '11236AA6AC0437GS', '4DAB6AA7F45100GS', '2C434AAFD17B11GS', '2B47CAA6F04DF8GS', '228C8AA88B64EEGS', '08BA2AAEFEC83DGS', '31CCCAA7AD1874GS', '6099EAA7640557GS', 'CE364AA214E92BGS', '87539AA950DD84GS', 'BECC2AA51D6551GS', '77ACAAAE37B71DGS']
				
				if not price_list:
					price_list=['1250.00', '1150.00', '1250.00', '1450.00', '795.00', '1450.00', '995.00', '1250.00', '1250.00', '895.00', '795.00', '995.00', '995.00', '990.00', '1100.00', '1150.00', '555.00', '1150.00', '1150.00', '850.00']
				
				if not brand_list:
					brand_list=['MARKS & SPENCER', 'DEBENHAMS', 'BOSS', 'BOSS', 'MANGO Man', 'MARKS & SPENCER', 'MARKS & SPENCER', 'MARKS & SPENCER', 'MARKS & SPENCER', 'DEBENHAMS', 'Topman', 'MARKS & SPENCER', 'MARKS & SPENCER', 'STANCE', 'Calvin Klein', 'DEBENHAMS', 'ALDO', 'MARKS & SPENCER', 'MARKS & SPENCER', 'Tommy Hilfiger']
		

			except:
				print 'exception______'

				product_list=['FFC88AA73B9F46GS', 'D13B6AA33C06B3GS', '2CDCEAA9F9ECB0GS', '03388AA1133E21GS', '177C6AA7B7A231GS', '4BF4FAA2475BD3GS', '9CB71AA118C21FGS', '23C2EAABB75748GS', '11236AA6AC0437GS', '4DAB6AA7F45100GS', '2C434AAFD17B11GS', '2B47CAA6F04DF8GS', '228C8AA88B64EEGS', '08BA2AAEFEC83DGS', '31CCCAA7AD1874GS', '6099EAA7640557GS', 'CE364AA214E92BGS', '87539AA950DD84GS', 'BECC2AA51D6551GS', '77ACAAAE37B71DGS']
				
				price_list=['1250.00', '1150.00', '1250.00', '1450.00', '795.00', '1450.00', '995.00', '1250.00', '1250.00', '895.00', '795.00', '995.00', '995.00', '990.00', '1100.00', '1150.00', '555.00', '1150.00', '1150.00', '850.00']
				
				brand_list=['MARKS & SPENCER', 'DEBENHAMS', 'BOSS', 'BOSS', 'MANGO Man', 'MARKS & SPENCER', 'MARKS & SPENCER', 'MARKS & SPENCER', 'MARKS & SPENCER', 'DEBENHAMS', 'Topman', 'MARKS & SPENCER', 'MARKS & SPENCER', 'STANCE', 'Calvin Klein', 'DEBENHAMS', 'ALDO', 'MARKS & SPENCER', 'MARKS & SPENCER', 'Tommy Hilfiger']
			
			if len(product_list)>=3:
				limit=3
			else:
				limit=len(product_list)
			for i in range(0,limit):
				products_view.append(product_list[i])
				brand_view.append(brand_list[i])
				price_view.append(price_list[i])

			time.sleep(random.randint(30,60))
			print "\n EVENT 9--------------------------------------search item-----------------------------------------------------"
			callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"keywords":"Socks","user_segment":"4","device_type":device_data.get('device_platform')})
			partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"keywords":"Socks","user_segment":"4","device_type":device_data.get('device_platform')})
			call_event(campaign_data, app_data, device_data,'tppxtu',callback_params,partner_params)

			print "\n EVENT 10-------------------------------------search item------------------------------------------------------"
			callback_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"category":"Clothing","fb_content_type":"product","app_version":campaign_data.get('app_version_name'),"category_id":"95","query":"Socks","amount_sessions":"1","device_type":device_data.get('device_platform'),"user_segment":"4","city":app_data.get('city'),"region":app_data.get('region'),"segment":app_data.get('segment'),"fb_content_id":str(products_view)})
			partner_params=json.dumps({"category":"Clothing","app_version":campaign_data.get('app_version_name'),"city":app_data.get('city'),"category_id":"95","segment":app_data.get('segment'),"fb_content_id":str(products_view),"user_segment":"4","shop_country":device_data.get('locale').get('country'),"fb_currency":currency,"fb_content_type":"product"})
			call_event(campaign_data, app_data, device_data,'d5rgsj',callback_params,partner_params)

			print "\n EVENT 11-------------------------------------search item------------------------------------------------------"
			callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","products":str(products_view),"device_type":device_data.get('device_platform')})
			partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","products":str(products_view),"device_type":device_data.get('device_platform')})
			call_event(campaign_data, app_data, device_data,'1agzpl',callback_params,partner_params)

			print "\n EVENT 12--------------------------------------search item-----------------------------------------------------"
			callback_params=json.dumps({"region":app_data.get('region'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"city":app_data.get('city'),"user_segment":"4","skus":str(products_view),"shop_country":device_data.get('locale').get('country'),"device_type":device_data.get('device_platform'),"amount_sessions":"1"})
			partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"user_segment":"4","city":app_data.get('city'),"skus":str(products_view)})
			call_event(campaign_data, app_data, device_data,'k9ttpn',callback_params,partner_params)

			print "\n EVENT 13--------------------------------------search item-----------------------------------------------------"
			callback_params=json.dumps({"user_segment":"4"})
			partner_params=json.dumps({"criteo_p":urllib.quote(str(products_view)),"user_segment":"4"})
			call_event(campaign_data, app_data, device_data,'lr6wyl',callback_params,partner_params)

		if random.randint(1,100)<=75:
			global product_id,product_name,product_price
			index=random.randint(0,len(products_view)-1)
			product_id=products_view[index]
			product_name=brand_view[index]
			product_price=price_view[index]
			time.sleep(random.randint(5,60))
			print "\n EVENT 14--------------------------------------view product-----------------------------------------------------"
			callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"product":product_id,"user_segment":"4","device_type":device_data.get('device_platform')})
			partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"segment":app_data.get('segment'),"product":product_id,"user_segment":"4","device_type":device_data.get('device_platform')})
			call_event(campaign_data, app_data, device_data,'pakbsu',callback_params,partner_params)

			time.sleep(random.randint(1,2))
			print "\n EVENT 15--------------------------------------view product-----------------------------------------------------"
			callback_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"discount":"false","tree":"men,PDV","app_version":campaign_data.get('app_version_name'),"brand":product_name,"amount_sessions":"1","sku":product_id,"currency_code":currency,"device_type":device_data.get('device_platform'),"price":product_price,"user_segment":"4","city":app_data.get('city'),"region":app_data.get('region'),"segment":app_data.get('segment')})
			partner_params=json.dumps({"segment":app_data.get('segment'),"app_version":campaign_data.get('app_version_name'),"city":app_data.get('city'),"sku":product_id,"price":product_price,"fb_content_id":product_id,"user_segment":"4","shop_country":device_data.get('locale').get('country'),"brand":product_name,"fb_currency":currency,"fb_content_type":"product"})
			call_event(campaign_data, app_data, device_data,'5fcgo2',callback_params,partner_params)

			time.sleep(random.randint(0,1))
			print "\n EVENT 16--------------------------------------view product-----------------------------------------------------"
			callback_params=json.dumps({"user_segment":"4"})
			partner_params=json.dumps({"criteo_p":product_id,"user_segment":"4"})
			call_event(campaign_data, app_data, device_data,'w2rdm6',callback_params,partner_params)

			if random.randint(1,100)<=50:
				ch=random.choice([1,1,1,1,1,1,2,2,2,2])
				time.sleep(random.randint(5,30))
				if ch==1:
					print "\n EVENT 17--------------------------------------add to cart-----------------------------------------------------"
					callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_segment":"4","price":product_price,"shop_country":device_data.get('locale').get('country'),"currency_code":currency,"device_type":device_data.get('device_platform')})
					partner_params=json.dumps({"fb_content_id":product_id,"app_version":campaign_data.get('app_version_name'),"sku":product_id,"price":product_price,"user_segment":"4","shop_country":device_data.get('locale').get('country'),"fb_currency":currency,"device_type":device_data.get('device_platform'),"fb_content_type":"product"})
					call_event(campaign_data, app_data, device_data,'w3szhs',callback_params,partner_params)
				if ch==2:
					print "\n EVENT 17--------------------------------------add to cart-----------------------------------------------------"
					callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"price":product_price,"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"currency_code":currency,"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
					partner_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"fb_content_type":"product","criteo_email_hash":hash_digest(app_data),"age":str(age),"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_id":app_data.get('user_id'),"user_segment":"0","price":product_price,"device_type":device_data.get('device_platform'),"customer_id":app_data.get('user_id'),"fb_currency":currency,"fb_content_id":product_id})
					call_event(campaign_data, app_data, device_data,'w3szhs',callback_params,partner_params)

				app_data.get('cart_value').append([product_id,product_price]) 
				app_data['cart_price'] += float(product_price)
				app_data['in_cart'] += 1 	

			if random.randint(1,100)<=25:	
				ch=random.choice([1,1,1,1,1,1,2,2,2,2])
				time.sleep(random.randint(5,30))
				if ch==1:
					print "\n EVENT 18--------------------------------------add to wishlist-----------------------------------------------------"
					callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_segment":"4","price":product_price,"shop_country":device_data.get('locale').get('country'),"currency_code":currency,"device_type":device_data.get('device_platform')})
					partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_segment":"4","price":product_price,"shop_country":device_data.get('locale').get('country'),"currency_code":currency,"device_type":device_data.get('device_platform')})
					call_event(campaign_data, app_data, device_data,'i0lx69',callback_params,partner_params)
				if ch==2:
					print "\n EVENT 18--------------------------------------add to wishlist-----------------------------------------------------"
					callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"price":product_price,"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"currency_code":currency,"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
					partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"price":product_price,"age":str(age),"user_segment":"0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"currency_code":currency,"device_type":device_data.get('device_platform'),"customer_id":app_data.get('user_id'),"criteo_email_hash":hash_digest(app_data)})
					call_event(campaign_data, app_data, device_data,'i0lx69',callback_params,partner_params)

				app_data['in_wishlist'] += 1 	

			if random.randint(1,100)<=15 and app_data.get('in_wishlist')>0:
				time.sleep(random.randint(5,20))
				print "\n EVENT 19--------------------------------------remove from wishlist-----------------------------------------------------"
				callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_segment":"4","price":product_price,"shop_country":device_data.get('locale').get('country'),"currency_code":currency,"device_type":device_data.get('device_platform')})
				partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":product_id,"user_segment":"4","price":product_price,"shop_country":device_data.get('locale').get('country'),"currency_code":currency,"device_type":device_data.get('device_platform')})
				call_event(campaign_data, app_data, device_data,'vgehf9',callback_params,partner_params)

			if random.randint(1,100)<=10:
				time.sleep(random.randint(10,60))
				print "\n EVENT 20--------------------------------------view wishlist-----------------------------------------------------"
				callback_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"sku":product_id,"user_id":app_data.get('user_id'),"app_version":campaign_data.get('app_version_name'),"amount_transactions":"1","currency_code":currency,"brand":product_name,"segment":app_data.get('segment'),"discount":"false","amount_sessions":"1","city":app_data.get('city'),"size":"9.5","quantity":"1","gender":app_data.get('gender'),"age":str(age),"total_wishlist":product_price,"user_segment":"1","price":product_price,"device_type":device_data.get('device_platform'),"colour":"BLACK","criteo_email_hash":hash_digest(app_data)})
				partner_params=json.dumps({"quantity":"1","shop_country":device_data.get('locale').get('country'),"criteo_email_hash":hash_digest(app_data),"age":str(age),"app_version":campaign_data.get('app_version_name'),"brand":product_name,"sku":product_id,"user_id":app_data.get('user_id'),"user_segment":"1","price":product_price,"total_wishlist":product_price,"city":app_data.get('city'),"customer_id":app_data.get('user_id'),"fb_currency":currency,"gender":app_data.get('gender'),"segment":app_data.get('segment')})
				call_event(campaign_data, app_data, device_data,'1qt9yi',callback_params,partner_params)

			if random.randint(1,100)<=25:
				print "\n EVENT 21--------------------------------------view cart-----------------------------------------------------"
				time.sleep(random.randint(10,60))
				for i in range(len(app_data.get('cart_value'))):
					callback_params=json.dumps({"total_cart":str(app_data.get('cart_price')),"shop_country":device_data.get('locale').get('country'),"sku":app_data.get('cart_value')[i][0],"user_id":app_data.get('user_id'),"app_version":campaign_data.get('app_version_name'),"amount_transactions":"1","currency_code":currency,"brand":product_name,"segment":app_data.get('segment'),"discount":"false","amount_sessions":"1","city":app_data.get('city'),"size":"9.5","quantity":"1","gender":app_data.get('gender'),"age":str(age),"user_segment":"1","price":app_data.get('cart_value')[i][1],"device_type":device_data.get('device_platform'),"colour":"BLACK","criteo_email_hash":hash_digest(app_data)})
					partner_params=json.dumps({"quantity":"1","discount":"false","shop_country":device_data.get('locale').get('country'),"criteo_email_hash":hash_digest(app_data),"age":str(age),"app_version":campaign_data.get('app_version_name'),"brand":product_name,"sku":app_data.get('cart_value')[i][0],"user_id":app_data.get('user_id'),"user_segment":"1","price":app_data.get('cart_value')[i][1],"city":app_data.get('city'),"total_cart":str(app_data.get('cart_price')),"customer_id":app_data.get('user_id'),"fb_currency":currency,"gender":app_data.get('gender'),"segment":app_data.get('segment')})
					call_event(campaign_data, app_data, device_data,'fgw1ip',callback_params,partner_params)

				print "\n EVENT 21--------------------------------------view cart-----------------------------------------------------"
				callback_params={"gender":app_data.get('gender'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"age":str(age),"user_segment":"1","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)}
				partner_params={"gender":app_data.get('gender'),"app_version":campaign_data.get('app_version_name'),"segment":app_data.get('segment'),"age":str(age),"user_segment":"1","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform'),"customer_id":app_data.get('user_id'),"criteo_email_hash":hash_digest(app_data)}
				for i in range(len(app_data.get('cart_value'))):
					if i==0:
						key = 'product'
					else:	
						key = 'product'+str((i))
					callback_params[key]=json.dumps({"quantity":"1","price":app_data.get('cart_value')[i][1],"currency_code":currency,"sku":app_data.get('cart_value')[i][0]})
					partner_params[key]=json.dumps({"quantity":"1","price":app_data.get('cart_value')[i][1],"currency_code":currency,"sku":app_data.get('cart_value')[i][0]})
				call_event(campaign_data, app_data, device_data,'jfvk77',json.dumps(callback_params),json.dumps(partner_params))

				print "\n EVENT 21--------------------------------------view cart-----------------------------------------------------"
				callback_params=json.dumps({"age":str(age),"user_segment":"1","criteo_email_hash":hash_digest(app_data)})
				partner_params=json.dumps({"age":str(age),"customer_id":app_data.get('user_id'),"user_segment":"1","criteo_p":urllib.quote(str([{"i":product_id,"pr":float(product_price),"q":1}])),"criteo_email_hash":hash_digest(app_data)})
				call_event(campaign_data, app_data, device_data,'7x3rgx',callback_params,partner_params)

				if random.randint(1,100)<=40 and app_data.get('in_cart')>0:
					if len(app_data.get('cart_value'))>0:
						n = random.randint(1,len(app_data.get('cart_value')))
						del_prod = app_data.get('cart_value')[n-1][0]
						del_price = app_data.get('cart_value')[n-1][1]
						time.sleep(random.randint(5,30))
						print "\n EVENT 22--------------------------------------remove from cart-----------------------------------------------------"
						callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":del_prod,"price":del_price,"age":str(age),"user_segment":"1","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"currency_code":currency,"device_type":device_data.get('device_platform'),"criteo_email_hash":hash_digest(app_data)})
						partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"sku":del_prod,"price":del_price,"age":str(age),"user_segment":"1","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"currency_code":currency,"device_type":device_data.get('device_platform'),"customer_id":app_data.get('user_id'),"criteo_email_hash":hash_digest(app_data)})
						call_event(campaign_data, app_data, device_data,'ovz729',callback_params,partner_params)

						del app_data.get('cart_value')[n-1]


				if purchase.isPurchase(app_data,day,advertiser_demand=55):

					if len(app_data.get('cart_value'))>0:
						n = random.randint(1,len(app_data.get('cart_value')))
						del_prod = app_data.get('cart_value')[n-1][0]
						del_price = app_data.get('cart_value')[n-1][1]

						app_data['transaction_id'] = str(random.randint(210000000,219999999))
						app_data['fb_order_id'] = device_data.get('locale').get('country')+app_data.get('transaction_id')

						print "\n EVENT 23--------------------------------------purchase -----------------------------------------------------"
						callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform')})
						partner_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"device_type":device_data.get('device_platform')})
						call_event(campaign_data, app_data, device_data,'m1q07h',callback_params,partner_params)

						print "\n EVENT 23--------------------------------------purchase -----------------------------------------------------"
						callback_params=json.dumps({"app_version":campaign_data.get('app_version_name'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"transaction_id":app_data.get('transaction_id'),"skus":"[\""+del_prod+"\"]","device_type":device_data.get('device_platform')})
						partner_params=json.dumps({"app_version":"6.13.0","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id'),"transaction_id":app_data.get('transaction_id'),"skus":"[\""+del_prod+"\"]","device_type":device_data.get('device_platform')})
						call_event(campaign_data, app_data, device_data,'w7stgj',callback_params,partner_params)

						print "\n EVENT 23--------------------------------------purchase -----------------------------------------------------"
						callback_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"sku":del_prod,"user_id":app_data.get('user_id'),"transaction_id":app_data.get('transaction_id'),"amount_transactions":"1","app_version":campaign_data.get('app_version_name'),"brand":product_name,"currency_code":currency,"discount":"false","amount_sessions":"1","fb_order_id":app_data.get('fb_order_id'),"city":app_data.get('city'),"region":app_data.get('region'),"segment":app_data.get('segment'),"new_customer":"true","size":"One Size","quantity":"1","gender":app_data.get('gender'),"age":str(age),"user_segment":"1","price":del_price,"device_type":device_data.get('device_platform'),"colour":"Navy","total_transaction":del_price,"criteo_email_hash":hash_digest(app_data)})
						partner_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"sku":del_prod,"user_id":app_data.get('user_id'),"transaction_id":app_data.get('transaction_id'),"fb_content_type":"product","app_version":campaign_data.get('app_version_name'),"brand":product_name,"fb_order_id":app_data.get('fb_order_id'),"segment":app_data.get('segment'),"city":app_data.get('city'),"customer_id":app_data.get('user_id'),"new_customer":"true","gender":app_data.get('gender'),"fb_currency":currency,"age":str(age),"user_segment":"1","price":del_price,"category_id":"354","fb_content_id":del_prod,"total_transaction":del_price,"criteo_email_hash":hash_digest(app_data)})
						call_event(campaign_data, app_data, device_data,'xs26pa',callback_params,partner_params)

						print "\n EVENT 23--------------------------------------purchase -----------------------------------------------------"
						callback_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"criteo_email_hash":hash_digest(app_data),"age":str(age),"transaction_id":app_data.get('transaction_id'),"app_version":campaign_data.get('app_version_name'),"product":json.dumps({"quantity":"1","price":del_price,"currency_code":currency,"sku":del_prod}),"new_customer":"true","user_id":app_data.get('user_id'),"user_segment":"1","device_type":device_data.get('device_platform'),"gender":"male","segment":"men"})
						partner_params=json.dumps({"shop_country":device_data.get('locale').get('country'),"criteo_email_hash":hash_digest(app_data),"age":str(age),"transaction_id":app_data.get('transaction_id'),"app_version":campaign_data.get('app_version_name'),"product":json.dumps({"quantity":"1","price":del_price,"currency_code":currency,"sku":del_prod,}),"new_customer":"true","user_id":app_data.get('user_id'),"user_segment":"1","device_type":device_data.get('device_platform'),"customer_id":app_data.get('user_id'),"gender":"male","segment":"men"})
						call_event(campaign_data, app_data, device_data,'ji2gpx',callback_params,partner_params)

						print "\n EVENT 23--------------------------------------purchase -----------------------------------------------------"
						callback_params=json.dumps({"age":str(age),"user_segment":"1","criteo_email_hash":hash_digest(app_data)})
						partner_params=json.dumps({"transaction_id":app_data.get('transaction_id'),"new_customer":app_data.get('user_id'),"age":str(age),"user_segment":"1","customer_id":app_data.get('user_id'),"criteo_p":urllib.quote(str([{"i":del_prod,"pr":del_price,"q":1}])),"criteo_email_hash":hash_digest(app_data)})
						call_event(campaign_data, app_data, device_data,'dhrfn9',callback_params,partner_params)	

						del app_data.get('cart_value')[n-1]			


				 
 
	###########		Finalize	############
	set_appCloseTime(app_data)

	return {'status':True}
	

def call_event(campaign_data, app_data, device_data,event_name,callback_params=None,partner_params=None):

	print '\nAdjust : EVENT____________________________'+event_name
	callback_params=callback_params
	partner_params=partner_params
	request=adjust_event(campaign_data, app_data, device_data,event_name,callback_params,partner_params)
	util.execute_request(**request)

###########################################################
#						ADJUST							  #
###########################################################
def selfcall3(campaign_data, app_data, device_data):

	url = 'https://prod-lookup.pvoc-anaina.com:443/Anaina/v0/Lookup'
	method = 'get'
	headers = {

		'User-Agent': 'MAPSDK/19.23.29 (ios; '+device_data.get("device_type")+'; '+device_data.get("os_version")+') '+campaign_data.get("package_name")+'/'+campaign_data.get("app_version_name"),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'Akamai-Mobile-Connectivity':'type=wifi;websdk=19.23.29;prepositioned=true;appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3',
		'Akamai-Sdk': 'MAPSDK/19.23.29 (ios; '+device_data.get("device_type")+'; '+device_data.get("os_version")+') '+campaign_data.get("package_name")+'/'+campaign_data.get("app_version_name")
	}
	
	params={
		'mobileNetworkCode':device_data.get('mnc'),
		'pushToken':'',
		'deviceId':app_data.get('device_id1'),
		'deviceType':device_data.get('device_type'),
		'platform':'ios',
		'appId':campaign_data.get('package_name'),
		'version':campaign_data.get('voc').get('version'),
		'mobileCountryCode':device_data.get('mcc'),
		'publicKey':'92015c520f2df1f54be67696a5c39d234aac593181b98877591c417fbfe637d2',
		'hostAppVersion':campaign_data.get('app_version_name'),
		'pushReference.bundleIdentifier':campaign_data.get('package_name'),
		'pushReference.production':'1'
	}

	data = {}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def selfcall1(campaign_data, app_data, device_data): #.....1

	url = 'http://device-provisioning.googleapis.com/checkin'
	method = 'post'
	headers = {

		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'Content-Type':'application/json'
	}
	
	params={}

	data ={
		"locale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
		"digest":"",
		"checkin":{"iosbuild":{"model":device_data.get('device_platform'),
		"os_version":"IOS_"+device_data.get('os_version')},
		"last_checkin_msec":0,
		"user_number":0,
		"type":2},
		"user_serial_number":0,
		"id":0,
		"timezone":device_data.get("local_tz_name"),
		"logging_id":2846142430,
		"version":2,
		"security_token":0,
		"fragment":0
		}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def selfcall4(campaign_data, app_data, device_data): #..4

	url = 'https://fe-map-us1.pvoc-anaina.com:443/Anaina/v0/Register'
	method = 'post'
	headers = {

		'User-Agent': 'MAPSDK/19.23.29 (ios; '+device_data.get("device_type")+'; '+device_data.get("os_version")+') '+campaign_data.get("package_name")+'/'+campaign_data.get("app_version_name"),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'Content-Type':'application/json',
		'Akamai-Mobile-Connectivity':'type=wifi;websdk=19.23.29;prepositioned=true;appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3',
		'Akamai-Sdk': 'MAPSDK/19.23.29 (ios; '+device_data.get("device_type")+'; '+device_data.get("os_version")+') '+campaign_data.get("package_name")+'/'+campaign_data.get("app_version_name")
	}
	
	params={}

	data ={
			"serverState":{"tenantId":"map_us1_1475509825",
			"schemaName":"pvoc_zalora_south_east_asia_pte_ltd1475509825527"},
			"mobileNetworkCode":device_data.get('mnc'),
			"version":campaign_data.get('voc').get('version'),
			"appId":campaign_data.get('package_name'),
			"deviceId":app_data.get('device_id1'),
			"deviceType":device_data.get('device_type'),
			"mobileCountryCode":device_data.get('mcc'),
			"pushReference":{"bundleIdentifier":campaign_data.get('package_name'),
			"production":True},
			"platform":"ios",
			"pushToken":"",
			"publicKey":"92015c520f2df1f54be67696a5c39d234aac593181b98877591c417fbfe637d2",
			"hostAppVersion":campaign_data.get('app_version_name')
		}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}	

def selfcall2(campaign_data, app_data, device_data): #....2

	url = 'https://5-6-7-ios.appload.ingest.crittercism.com/v0/config'
	method = 'post'
	headers = {

		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'Content-Type':'application/json',
		'CRAppId': app_data.get('appid'),
		'CRDevelopmentPlatform': 'ios',
		'CRDeviceId': app_data.get('device_id'),
		'CRPlatform': 'ios',
		'CRVersion': campaign_data.get('appload').get('version'),
	}
	
	params={}

	bia = device_data.get('cpu').get('name') if device_data.get('cpu') else "Swift, ARM v7"

	data ={
			"binary_image_architecture":bia.split(' ')[1] if " " in bia else bia,
			"bundle_identifier":campaign_data.get('package_name'),
			"mnc":device_data.get('mnc'),
			"binary_image_name":campaign_data.get('app_name'),
			"system_version":device_data.get('os_version'),
			"app_version":campaign_data.get('app_version_name')+' ('+campaign_data.get('app_version_code')+')',
			"development_platform":"ios",
			"background_session":False,
			"device_id":app_data.get('device_id'),
			"library_version":campaign_data.get('appload').get('version'),
			"binary_image_uuid":app_data.get('binary_uid'),
			"locale":device_data.get('locale').get('language'),
			"system":device_data.get('device_type'),
			"app_id":campaign_data.get('appload').get('appid'),
			"type":"appConfig",
			"system_name":"iOS",
			"cold_launched":True,
			"mcc":device_data.get('mcc'),
			"protocol_version":campaign_data.get('appload').get('protocol_version'),
			"platform":"ios",
			"carrier":device_data.get('carrier'),
			"sent_at":self_sent_at,
			"model":device_data.get('device_platform'),
			"occurred_at":self_sent_at,
			"session_number":1
			}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}	

def selfcall5(campaign_data, app_data, device_data): #...5

	url = 'https://5-6-7-ios.appload.ingest.crittercism.com/v0/appload'
	method = 'post'
	headers = {

		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'Content-Type':'application/json',
		'CRAppId': campaign_data.get('appload').get('appid'),
		'CRDevelopmentPlatform': 'ios',
		'CRDeviceId': app_data.get('device_id1'),
		'CRPlatform': 'ios',
		'CRVersion': campaign_data.get('appload').get('version'),
	}
	
	params={}

	data =[{
		"appLoads":{
		"development_platform":"ios",
		"carrier":device_data.get('carrier'),
		"mobileNetworkCode":device_data.get('mnc'),
		"osName":"iOS",
		"crPlatform":"ios",
		"platformSpecificData":{"bundleIdentifier":campaign_data.get('package_name')},
		"osVersion":device_data.get('os_version'),
		"mobileCountryCode":device_data.get('mcc'),
		"appID":campaign_data.get('appload').get('appid'),
		"locale":device_data.get('locale').get('language'),
		"deviceModel":device_data.get('device_platform'),
		"sessionNumber":1,
		"rate":1,
		"deviceID":app_data.get('device_id1'),
		"crVersion":campaign_data.get('appload').get('version'),
		"appVersion":campaign_data.get('app_version_name')+' ('+campaign_data.get('app_version_code')+')'},
		"count":1,
		"current":True
		}]

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def adjust_session(campaign_data, app_data, device_data,call_type='install'):
	
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	params={}

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': session_time,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'sent_at':session_time,
			'session_count': app_data.get('session_count'),
			'tracking_enabled':	0,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':app_data.get('installed_at'),
			'install_receipt':install_receipt(),
			'os_build':	device_data.get('build'),
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'connectivity_type':'2',
			'mcc' : device_data.get('mcc'),
			'mnc' : device_data.get('mnc'),
			'network_type' : 'CTRadioAccessTechnologyHSDPA',
			}

	# data['queue_size']=2
	
	if call_type == "open":
		time_spent=app_data.get('appCloseTime')-app_data.get('adjust_call_time')
		def_(app_data,'subsession_count')
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = time_spent
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] =time_spent
	
	headers['Authorization']= get_auth(app_data,activity_kind='session',idfa=data['idfa'],created_at=data['created_at'])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adjust_sdkclick(campaign_data, app_data, device_data,source=False):
	pushToken(app_data)
	if source=='iad3':
		created=click_created
		sent=session_time
	else:
		created=get_date(app_data,device_data)
		sent=get_date(app_data,device_data)

	
	time_spent=int(time.time())-app_data.get('adjust_call_time')
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',

		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.pathname2url(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	params={}

	data = {
			'source': source,
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': created,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'details':json.dumps({"Version3.1":{"iad-attribution":"false"}}),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'connectivity_type':'2',
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'sent_at': sent,
			'tracking_enabled':	0,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':app_data.get('installed_at'),
			'install_receipt': install_receipt(),
			'os_build':	device_data.get('build'),
			'last_interval': get_lastInterval(app_data),
			'session_count' : app_data.get('session_count'),
			'session_length' : time_spent,
			'time_spent' : time_spent-random.randint(2,3),
			'subsession_count' : app_data.get('subsession_count'),
			'persistent_ios_uuid' :  app_data.get('ios_uuid'),
			'mcc' : device_data.get('mcc'),
			'mnc' : device_data.get('mnc'),
			'network_type' : 'CTRadioAccessTechnologyHSDPA',
			}
	if source=='deeplink':
		inc_(app_data,'subsession_count')
		data['click_time']=get_date(app_data,device_data)
		data['deeplink']='zalora://hk/urlc_s/m/products/?specialKey=all&segment='+app_data.get('segment')+'&discount=10-80&csa='+app_data.get('csa')+'&shop=m'
		del data['details']
		data['persistent_ios_uuid']= app_data.get('ios_uuid')
		data['push_token']=app_data.get('push_token'),
		data['session_count']=app_data.get('session_count')
		data['session_length']=time_spent
		data['time_spent']=time_spent-random.randint(2,3)
		data['subsession_count']=app_data.get('subsession_count')

	headers['Authorization']= get_auth(app_data,activity_kind='click',idfa=data['idfa'],created_at=data['created_at'])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_attribution(campaign_data, app_data, device_data, initiated_by='sdk'):
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'User-Agent':urllib.pathname2url(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	data={}
	params = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink' : 1,
			'bundle_id': campaign_data.get('package_name'),
			'created_at': get_date(app_data,device_data,early=random.uniform(4,4.5)),
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'initiated_by'	: initiated_by,
			'needs_response_details': 1,
			'os_build':	device_data.get('build'),
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid' : app_data.get('ios_uuid'),
			'sent_at': get_date(app_data,device_data),
			}
	
	headers['Authorization']= get_auth(app_data,activity_kind='attribution',idfa=params['idfa'],created_at=params['created_at'])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}			
	
def adjust_sdkinfo(campaign_data, app_data, device_data):
	pushToken(app_data)
	url = 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent':urllib.pathname2url(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	params={}

	data = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink' : 1,
			'created_at':info_created,
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'needs_response_details': 1,
			'sent_at': info_sent,
			'persistent_ios_uuid':	app_data.get('ios_uuid'),
			'push_token':app_data.get('push_token'),
			'source' :'push'
			}
	
	headers['Authorization']= get_auth(app_data,activity_kind='info',idfa=data['idfa'],created_at=data['created_at'])	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	
def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=None,partner_params=None,purchase=False,t1=0,t2=0):
	time_spent=int(time.time())-app_data.get('adjust_call_time')	
	url = 'http://app.adjust.com/event'
	method = 'post'
	headers = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
	
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}
			
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	if app_data.get('event_count')==1 or app_data.get('event_count')==2:
		created=session_time
		sent=event_sent
	else:
		created=get_date(app_data,device_data)
		time.sleep(random.randint(t1,t2))
		sent=get_date(app_data,device_data)


	data = {
			'hardware_name':device_data.get('hardware'),	
			'event_buffering_enabled':	0,
			'cpu_type':device_data.get('cpu_type'),
			'attribution_deeplink': '1',
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'connectivity_type':'2',
			'os_name':'ios',
			'environment':'production',
			'needs_response_details':1,
			'event_count':app_data.get('event_count'),
			'time_spent':time_spent,
			'session_count':str(app_data.get('session_count')),
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			'created_at': created,
			'event_token':event_token,
			'bundle_id':campaign_data.get('package_name'),
			'subsession_count':str(app_data.get('subsession_count')),
			'os_version':device_data.get('os_version'),
			'app_version': campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country'),
			'language':	device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'session_length':time_spent,
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': '0',
			'device_name':device_data.get('device_platform'),
			'sent_at': sent,
			'os_build':	device_data.get('build'),
			'install_receipt': install_receipt(),
			'mcc' : device_data.get('mcc'),
			'mnc' : device_data.get('mnc'),
			'network_type' : 'CTRadioAccessTechnologyHSDPA',
			}
			
	if callback_params:
		data['callback_params']	= callback_params
	if partner_params:
		data['partner_params']	= partner_params

	if event_token=='k9ttpn' or data['event_count']==1:
		data['queue_size']='1'

	if app_data.get('push_token'):
		data['push_token']=app_data.get('push_token')

	headers['Authorization']= get_auth(app_data,activity_kind='event',idfa=data['idfa'],created_at=data['created_at'])	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':None, 'data': data}	
	
###########################################################
#						Self Calls						  #
###########################################################	

def zalora_init(campaign_data, app_data, device_data, call=1):
	url='https://api.zalora.sg/v1/init'
	method='get'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': device_data.get('locale').get('country')
	}

	params={
		'cacheKey':'dummy',
		'colors':'dummy',
		'contentVersion':'{"shopbys":"9","shop_catalogs":"2"}',
		'countries':'dummy',
		'features':'campaign_pin,campaign_qr,native_mobile_thumbor,native_mobile_webp,forced_login,app_quicksilver_checkout_v1,app_membership_program,search_suggest_app,zendesk_sdk_faq,app_membership_program_2',
		'services':'dummy',
		'setLang':'',
		'shop':'m',
		'version':'dummy',
	}

	data = {}

	if call==2:
		url='https://api.zalora.com.hk/v1/init'
		params['cacheKey']=cacheKey
		params['colors']=colors
		params['countries']=countries
		params['version']=version
		params['setLang']=[device_data.get('locale').get('language'), device_data.get('locale').get('language')]

		headers['Akamai-Map-Download']= campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name')
		headers['Akamai-Mobile-Connectivity']='type=wifi;v=tpv1;rtt=90;tpresult=0;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('app_version_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport'
		headers['Akamai-Referrer']= campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name')
		headers['Akamai-Sdk']='MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name')
		headers['Zalora-Lang']= device_data.get('locale').get('language')

		headers['Zalora-Country']='HK'

	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def zalora_catalog_categories(campaign_data, app_data, device_data, call=1):
	url='https://api.zalora.sg/v1/catalog/categories'
	method='get'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': device_data.get('locale').get('country').lower(),
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=90;tpresult=0;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('app_version_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name')
	}

	params={
		'shop'	: 'm'
	}

	data = {}

	if call==2:
		headers['Zalora-Lang']= device_data.get('locale').get('language')

		headers['Zalora-Country']='HK'
		url='https://api.zalora.com.hk/v1/catalog/categories'
		params['setLang']=device_data.get('locale').get('language')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def zalora_homescreen(campaign_data, app_data, device_data):
	url='https://api.zalora.com.hk/v1/homescreen'
	method='get'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=1300;tpresult=2;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language')
	}

	params={
		'formFactor' : device_data.get('device_type'),
		'segment' : app_data.get('segment'),
		'setLang':device_data.get('locale').get('language'),
		'shop':'m',
	}

	data = {}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def zalora_webcontent(campaign_data, app_data, device_data):
	url='https://api.zalora.com.hk/v1/webcontent/quicksilver'
	method='get'
	headers={
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language'),
		'Accept': 'application/vnd.zalora.webcontent.v1+thrift.binary',
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate'
	}

	params=None
	data=None
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def form_login(campaign_data, app_data, device_data, call=1):
	url='https://api.zalora.com.hk/v1/forms/login'
	method='get'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=1300;tpresult=2;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language')
	}
	params={}
	data=None

	if call==2:
		params['setLang']=device_data.get('locale').get('language')
		params['version']=2

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def zalora_tutorial(campaign_data, app_data, device_data):
	url='https://api.zalora.com.hk/v1/tutorials'
	method='get'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=1300;tpresult=2;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language')
	}

	params={
		'deviceType' : 'ios',
		'fromVersion': '',
		'setLang':device_data.get('locale').get('language'),
		'toVersion': campaign_data.get('app_version_name')
	}

	data={}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def zalora_register(campaign_data, app_data, device_data):
	url='https://api.zalora.com.hk/v1/forms/register'
	method='get'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=1300;tpresult=2;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language')
	}

	params={}
	data={}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def zalora_customers(campaign_data, app_data, device_data):
	url='https://api.zalora.com.hk/v1/customers/'
	method='post'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=1300;tpresult=2;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language')
	}

	data={
		'birthday'	:app_data.get('user_info').get('dob'),
		'email':app_data.get('user_info').get('email'),
		'firstName':app_data.get('user_info').get('f_name'),
		'gender':app_data.get('user_info').get('sex'),
		'lastName':app_data.get('user_info').get('l_name'),
		'newsletter':1,
		'password':app_data.get('user_info').get('password')
	}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}

def zalora_feed(campaign_data, app_data, device_data):
	url='https://feed.zalora.io/feed'
	method='get'
	headers={
		'Accept': 'application/json, text/plain, */*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
	}
	params={
		'language':device_data.get('locale').get('language'),
		'limit':5,
		'segment':app_data.get('segment'),
		'venture':'hk'
	}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

def zalora_checkoutSync(campaign_data, app_data, device_data):
	url='https://api.zalora.com.hk/v1/checkout/sync'
	method='post'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=1300;tpresult=2;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language'),
		'Cookie':app_data.get('cookie1')+'; doraemonSessionId='+app_data.get('doraemon_sessionId1')
	}
	params=None
	data=None
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def zalora_addresses(campaign_data, app_data, device_data):
	url='https://api.zalora.com.hk/v1/addresses'
	method='get'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=1300;tpresult=2;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language'),
		'Cookie':app_data.get('cookie1')+'; doraemonSessionId='+app_data.get('doraemon_sessionId2')
	}
	params=None
	data=None
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def zalora_products(campaign_data, app_data, device_data):
	url='https://api.zalora.com.hk/v1/products'
	method='get'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=1300;tpresult=2;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language'),
		'Cookie':app_data.get('cookie1')+'; doraemonSessionId='+app_data.get('doraemon_sessionId3')
	}

	params={
		'csa':app_data.get('csa'),
		'discount':'10-80',
		'limit':20,
		'offset':0,
		'segment':app_data.get('segment'),
		'setLang':device_data.get('locale').get('language'),
		'shop':'m',
		'specialKey':'all'
	}

	data=None
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def checkout_coupon(campaign_data, app_data, device_data):
	url='https://api.zalora.com.hk/v1/checkout/coupon'
	method='post'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=1300;tpresult=2;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language'),
		# 'Cookie':cookie1+'; doraemonSessionId='+doraemon_sessionId3
	}

	params=None

	data={
		'coupon' : 'VACAY30',
		'fromDeeplink' : 1
	}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def search_product(campaign_data, app_data, device_data):
	url='https://api.zalora.com.hk/v1/products'
	method='get'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1'+', fr-FR;q=0.9, en-FR;q=0.8, ja-FR;q=0.7, en-GB;q=0.6, it-FR;q=0.5',
		'User-Agent': 'iOS/'+campaign_data.get('zalora').get('url_version')+'/'+device_data.get('device_type')+'/'+campaign_data.get('app_version_code')+'///'+app_data.get('idfv_id')+'//'+screen_size+'/'+campaign_data.get('app_version_name'),
		'Akamai-Map-Download': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Mobile-Connectivity':'type=wifi;v=tpv1;rtt=1300;tpresult=2;websdk='+campaign_data.get('zalora').get('sdk')+';appdata='+campaign_data.get('package_name')+';carrier='+device_data.get('carrier')+'/0,0;devicetype=3;optimization=transport',
		'Akamai-Referrer': campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Akamai-Sdk': 'MAPSDK/'+campaign_data.get('zalora').get('sdk')+' (ios; iPhone; '+device_data.get('os_version')+') '+campaign_data.get('package_name')+'/'+campaign_data.get('app_version_name'),
		'Zalora-Country': 'HK',
		'Zalora-Lang'	: device_data.get('locale').get('language'),
		'Cookie':checkout_cookie_key+'='+checkout_cookie_value
	}

	params={
		'limit':'20',
		'offset':'0',
		'query':'Socks',
		'segment':app_data.get('segment'),
		'shop':'m|o',
		'setLang':device_data.get('locale').get('language')
	}

	data=None
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

######################################################################################
def current_location(device_data):
	url='http://lumtest.com/myip.json'
	header=None
	params=None
	data=None
		
	return {'url': url, 'httpmethod': 'get', 'headers': header, 'params': params, 'data': data}


def measurement(campaign_data, app_data, device_data):
	url='http://app-measurement.com/config/app/1:401005157362:ios:62f11fa87c47afb0'
	method='head'
	headers={
		'Accept-Encoding':'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
	}

	params={
		'app_instance_id':'3D1134CE4A2B424D872BEC032B6A5CB0',
		'gmp_version':'50001',
		'platform':'ios',
	}
	data=None
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}
	
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def pushToken(app_data):
	if not app_data.get('push_token'):
		app_data['push_token']=util.get_random_string('hex',64)

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def install_receipt():
	return 'MIISXAYJKoZIhvcNAQcCoIISTTCCEkkCAQExCzAJBgUrDgMCGgUAMIIB/QYJKoZIhvcNAQcBoIIB7gSCAeoxggHmMAoCARQCAQEEAgwAMAsCAQMCAQEEAwwBMzALAgEOAgEBBAMCAVowCwIBEwIBAQQDDAExMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMA0CAQsCAQEEBQIDHxlxMA0CAQ0CAQEEBQIDAdUmMA4CAQECAQEEBgIEJTs8KTAOAgEJAgEBBAYCBFAyNTMwDgIBEAIBAQQGAgQxoI9HMBACAQ8CAQEECAIGHD8gxnTgMBQCAQACAQEEDAwKUHJvZHVjdGlvbjAUAgECAgEBBAwMCmNvbS56YWxvcmEwGAIBBAIBAgQQy90hLcDFfdD+HeeCGDnMcjAcAgEFAgEBBBQYzR+KsL7MDcLSoijaAyfLJq2+ITAeAgEIAgEBBBYWFDIwMTktMDktMDVUMTA6MTk6MzFaMB4CAQwCAQEEFhYUMjAxOS0wOS0wNVQxMDoxOTozMVowHgIBEgIBAQQWFhQyMDE4LTA2LTA1VDA3OjIxOjM1WjAyAgEHAgEBBCoJgEPkQZqpn298v7MD8bwnLIHKzgRprUMtcGBZKfiltheanorgyN9sIM8wPgIBBgIBAQQ2YVCm4tDtXYTqWgk3I/NApheGJwpXSRPsZXvNcjxsOMctj2UARoKRISSnVmKytZqaHDC5cYCEoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQB3QdMRimkJ53BiMcHPR9TRpAiJtX76jp4w0acdHOBpnXxi4DK8aRXb7QOwDRRhifDZK10GrqF+SXS0ze6aEIjJMR1vRdA1mpL0EFWiz9x45A4p94AffFwTnifrWIl+avF0kJv49H4fDvD+OON8OqcgLvOPm6GRh+ecHwfYxHs5a1u8llMtNSPpUTw+mQpzenATZ0kKir2EfGkYnmyAzqok1g1XiLd/s9RPK7hsxz8P2V0GLZ6S/0wGhFkSd6Pg/tHyMEn9qSe6xP5PQQEE4JOgcUjE1aOS8UkZuvmT7ZqEWRiztTWp2kpv6KfS3ngG1D/TP82ixIsLknyfYnyYV1Qs'
	
def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_auth(app_data,activity_kind,idfa,created_at):
	final_str=campaign_data.get('adjust').get('secret_key')+str(activity_kind)+str(idfa)+str(created_at)
	sign=util.sha256(final_str)
	auth='Signature secret_id="'+str(campaign_data.get('adjust').get('secret_id'))+'",signature="'+str(sign)+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'
	return auth

def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	if not gender_n:
		app_data['gender'] = random.choice(['male', 'female'])
		gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	if not user_info:
		user_data = util.generate_name(gender=gender_n)
		user_info = {}
		username = user_data.get('username').lower()
		user_info['email'] = username.lower() + '@gmail.com'
		user_info['f_name'] = user_data.get('firstname')
		user_info['l_name'] = user_data.get('lastname')
		user_info['sex'] = gender_n
		# user_info['gender'] = str(1) if gender_n == 'female' else str(2)
		# user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
		user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
		user_info['password'] = util.get_random_string(type='all',size=16)
		app_data['user_info'] = user_info	

def hash_digest(app_data):
	if not app_data.get('email_hash'):
		string=str(app_data.get('user_info').get('email'))
		result = hashlib.md5(string.encode())
		app_data['email_hash']=result.hexdigest().upper()

	return app_data.get('email_hash')

##############################################################################
#
#				Neccessary Methods
#
##############################################################################	
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

