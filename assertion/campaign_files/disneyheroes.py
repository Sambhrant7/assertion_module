from sdk import util,purchase,installtimenew
import json, time, uuid, urllib, random, urlparse, hashlib, base64, hmac,re
from sdk import getsleep
import clicker
import Config



campaign_data = {
				'app_size' :96.0,#97.3, 91.37, #98.51,#91.40,#97.2, 91.16,#96.0,#90.79,#94.33,#90.0, #96.68, 89.85, 96.7
				'package_name' : 'com.perblue.disneyheroes',
				'sdk' : 'Android',
				'app_version_name' : '1.13.1',#1.12.4, '1.12.3',#1.12.2, '1.12.1',#1.12, '1.11.4',#'1.11',#'1.10.4',#'1.10.2',#1.10.1, '1.10',#1.9.3,#'1.9.2',#1.9, '1.8.2',#'1.8.1',#'1.8',#'1.7.2',#'1.7.1',#1.7, '1.6.4',#'1.6.1',#'1.5.2',#'1.5.1',#'1.5',#'1.4.1',#'1.3', 1.4, 1.6.2
				'app_name' : 'Disney Heroes',
				'supported_countries': 'WW',
				'device_targeting':True,
				'tracker' : 'apsalar',
				'ctr':6,
				'supported_os': '5.0', #4.4
				'no_referrer' : True,
				'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
				'apsalar':{
					'version':'Singular/v7.0.1.PROD',
					'key':'perblue_unified',
					'secret':'91941952b6cc0379dee6b02db47b39b4'
				},
				'service_fyber' : {
				'sdk_version': '8.12.2',
				},
				'helpshift':{
				'platform_id':'perblue_platform_20171220164200412-1154c5214c923ff',
				'key':'c75c3dfeaf7bcb174958af56e081b500',
				'version': '7.5.0',#'7.3.0', #6.4.0
				's': 'android'
				},
				'purchase_var':{
				'1':'2.99',
				'2':'4.99',
				'3':'7.99',
				'4':'9.99',
				},
				'retention':{
							1:70,
							2:68,
							3:66,
							4:64,
							5:61,
							6:59,
							7:57,
							8:52,
							9:50,
							10:47,
							11:45,
							12:43,
							13:40,
							14:37,
							15:35,
							16:31,
							17:30,
							18:28,
							19:26,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
				}
}


def install(app_data, device_data):
	print "Please wait..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

	# app_data['installedtime']=int(app_data.get('times').get('install_complete_time'))*1000
	app_data['TutorialFinished']=False
	
	time.sleep(random.randint(60,100))


	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;

	
	print '-----------------------helpshift_config-----------------------------'
	elex_helpshift = helpshift_config(campaign_data,app_data,device_data,uri='config/')
	util.execute_request(**elex_helpshift)
	
	print '-----------------------helpshift_config-----------------------------'
	elex_helpshift = helpshift_config(campaign_data,app_data,device_data,uri='ws-config/')
	util.execute_request(**elex_helpshift)

	print '-----------------------helpshift_event-----------------------------'
	elex_helpshift = helpshift_event(campaign_data,app_data,device_data)
	util.execute_request(**elex_helpshift)

	print '----------------------------- APSALAR Resolve ---------------------------'	
	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**apsalar_Resolve)
	
	time.sleep(2)
	print '----------------------------- APSALAR START -----------------------------'
	l=str(random.randint(10,99)*0.001)
	apsalar_start = apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_start)

	print '----------------------------- Self1 -----------------------------'	
	apsalar_start = self1(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_start)

	time.sleep(random.randint(2,5))

	# print '----------------------------- Self2 -----------------------------'	
	# apsalar_start = self2(campaign_data, app_data, device_data)
	# util.execute_request(**apsalar_start)


	# print "\nself_call_service_fyber\n"
	# request=service_fyber( campaign_data, device_data, app_data )
	# util.execute_request(**request)


	# print "\nself_call_engine_fyber\n"
	# request=engine_fyber( campaign_data, device_data, app_data )
	# util.execute_request(**request)


	# print "\nTapjoy connect\n"
	# request = tapjoycall(app_data,campaign_data,device_data)
	# util.execute_request(**request)


	print '----------------------------- APSALAR EVENT ------------------__InstallReferrer-----------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__InstallReferrer',{"referrer":app_data.get('referrer')})
	util.execute_request(**apsalar_event)


	if random.randint(1,100)<=95:
		time.sleep(random.randint(9,11))
		print '----------------------------- APSALAR EVENT ------------login-----------------'
		if random.randint(1,100)<=80:
			app_data['val']=str(random.randint(1,9))+'000000000'+str(util.get_random_string('decimal',7))
			l={"user_id":app_data.get('val')}
		else:
			app_data['val']=util.get_random_string('decimal',7)
			l={"user_id":app_data.get('val')}



		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'login',l)
		util.execute_request(**apsalar_event)
	
		if random.randint(1,100)<=90:
			time.sleep(random.randint(150,190))
			print '----------------------------- APSALAR EVENT defeat-----------------------------'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'DEFEAT_1_1')
			util.execute_request(**apsalar_event)
			time.sleep(8)
			print '----------------------------- APSALAR EVENT TutorialFinished-----------------------------'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'TutorialFinished')
			util.execute_request(**apsalar_event)

			app_data['TutorialFinished']=True

			call_pattern(app_data,campaign_data,device_data,calltype='install')		

	
	return {'status':True}
	
	
def open(app_data, device_data,day):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

	if not app_data.get('TutorialFinished'):
		app_data['TutorialFinished']=False


	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;

	if day==3:
		print "-----------------3-Day retention token---------------"
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'3DayRetention')
		util.execute_request(**apsalar_event)

	if day==7:
		print "-----------------3-Day retention token---------------"
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'7DayRetention')
		util.execute_request(**apsalar_event)




	print '----------------------------- APSALAR START -----------------------------'
	l=str(random.randint(10,99)*0.001)
	apsalar_start = apsalarStart(campaign_data, app_data, device_data,isTrue=False)
	util.execute_request(**apsalar_start)

	print "Apsalar----------------Resolve--------------"
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_Resolve)


	if random.randint(1,100)<=95:
		time.sleep(7)
		print '----------------------------- APSALAR EVENT Login-----------------------------'
		if not app_data.get('val'):
			if random.randint(1,100)<=80:
				app_data['val']=str(random.randint(1,9))+'000000000'+str(util.get_random_string('decimal',7))
				l={"user_id":app_data.get('val')}
			else:
				app_data['val']=util.get_random_string('decimal',7)
				l={"user_id":app_data.get('val')}
		else:
			l={"user_id":app_data.get('val')}


		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'login',l)
		util.execute_request(**apsalar_event)
		


		if app_data.get('TutorialFinished')==False:

			print '----------------------------- APSALAR EVENT defeat-----------------------------'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'DEFEAT_1_1')
			util.execute_request(**apsalar_event)
			time.sleep(8)
			print '----------------------------- APSALAR EVENT TutorialFinished-----------------------------'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'TutorialFinished')
			util.execute_request(**apsalar_event)

			app_data['TutorialFinished']=True

		if app_data.get('TutorialFinished')==True:
			call_pattern(app_data,campaign_data,device_data,calltype='open',day=day)	

			if purchase.isPurchase(app_data,day,advertiser_demand=10):

				rr=random.randint(1,100)
				if rr<=80:
					choice='1'
				elif rr<=90:
					choice='2'
				else:
					choice=random.choice(['3','4'])	

				r=campaign_data.get('purchase_var').get(choice)	
				time.sleep(random.randint(15,30))

				print "-----------------Purchase-------------------------"
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__iap__',revenue=r)
				util.execute_request(**apsalar_event)

	
	return {'status':True}

#########################################
#										#
# 			AppSalar funcation 			#
#										#
#########################################


def call_pattern(app_data,campaign_data,device_data,calltype='',day=None):
	if not app_data.get('level'):
		app_data['level']=1

	
	if calltype=='install':
		if random.randint(1,100)<=80:
			level_list=random.choice([7,7,8,9,11,11,11,11,11,11])
		else:
			level_list=random.choice([5,6,7])
	if calltype=='open':
		if random.randint(1,100)<=90:
			if day==1 or day==2:
				level_list=random.choice([5,5,5,5,6,6,7,8,9])
			else:
				level_list=random.choice([1,2,3,4,5])
					
		else:
			level_list=random.choice([1,2,3,4,5])	

	
	# if random.randint(1,100)<=90:

	for i in range(level_list):

		print "************************************************"

		print app_data.get('level')
		print "\n"
		print day

		print "************************************************"			
		time.sleep(random.randint(180,200))

		if i>5:
			print "-------playing---------"
			time.sleep(random.randint(600,620))


		if app_data.get('level')==5:
			
			print '----------------------------- APSALAR EVENT TL5-----------------------------'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'TL5')
			util.execute_request(**apsalar_event)
			
			print '----------------------------- APSALAR EVENT TL7-----------------------------'
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'TL7')
			util.execute_request(**apsalar_event)

			
		if app_data.get('level')==10:
			
			print '----------------------------- APSALAR EVENT TL10-----------------------------'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'TL10')
			util.execute_request(**apsalar_event)
			


		if app_data.get('level')==15:
			
			print '----------------------------- APSALAR EVENT TL15-----------------------------'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'TL15')
			util.execute_request(**apsalar_event)




		app_data['level']+=1



def apsalarResolve(campaign_data, app_data, device_data):

	url = 'http://e-ssl.apsalar.com/api/v1/resolve'
	method = 'get'
	headers = {
		'User-Agent': "Singular/SDK-v7.0.1.PROD",
		'Accept-Encoding': 'gzip'
	}
	
	data_dict = {
		"a": campaign_data.get('apsalar').get('key'),
		"c":"wifi",
		"i":campaign_data.get('package_name'),
		"k":"AIFA",
		"lag":str(generateLag()),
		"p":"Android",
		"pi":"1",
		"rt":"json",
		"u":device_data.get('adid'),
		"v":device_data.get('os_version'),
	}

	params = get_params(data_dict)
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarStart(campaign_data, app_data, device_data,isTrue=True):

	url = 'http://e-ssl.apsalar.com/api/v1/start'
	method = 'get'
	headers = {
		'User-Agent': "Singular/SDK-v7.0.1.PROD",
		'Accept-Encoding': 'gzip'
	}

	data_dict = {
				"a": campaign_data.get('apsalar').get('key'),
				"ab":device_data.get('cpu_abi',"armeabi"),
				"aifa":device_data.get('adid'),
				"av":campaign_data.get('app_version_name'),
				"br":device_data.get('brand'),
				"c":"wifi",
				"ddl_enabled":"false",
				"de":device_data.get('device'),
				"dnt":"1",
				"i":campaign_data.get('package_name'),
				"k":"AIFA",
				"lag":str(generateLag()),
				"lc":device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
				"ma":device_data.get('manufacturer'),
				"mo":device_data.get('model'),
				"n":campaign_data.get('app_name'),
				"p":"Android",
				"pr":device_data.get('product'),
				"rt":"json",
				"s":str(int(app_data.get('times').get('install_complete_time')*1000)),
				"sdk": campaign_data.get('apsalar').get('version'),
				"src":"com.android.vending",
				"u":device_data.get('adid'),
				"v":device_data.get('os_version'),
	}

	if isTrue:
		data_dict['is'] = 'true'

	params = get_params(data_dict)
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarEvent(campaign_data, app_data, device_data,event_name,value=False,revenue=None):

	url = 'http://e-ssl.apsalar.com/api/v1/event'
	method = 'get'
	headers = {
		'User-Agent':"Singular/SDK-v7.0.1.PROD",
		'Accept-Encoding': 'gzip'
	}


	data_dict = {
	"a": campaign_data.get('apsalar').get('key'),
	"aifa":device_data.get('adid'),
	"av":campaign_data.get('app_version_name'),
	"c":"wifi",
	"i":campaign_data.get('package_name'),
	"k":"AIFA",
	"lag":str(generateLag()),
	"n":str(event_name),
	"p":"Android",
	"rt":"json",
	"s":str(int(app_data.get('times').get('install_complete_time')*1000)),
	"sdk": campaign_data.get('apsalar').get('version'),
	"seq": str(cal_seq(app_data)),
	"t":str(generate_t()),
	"u":device_data.get('adid'),
	}

	if value:
		data_dict['e'] = json.dumps(value).replace(" ","").strip()

	if revenue:
		data_dict['e'] = json.dumps({"pcc":"USD","r":str(revenue)}).replace(" ","").strip()


	params = get_params(data_dict)	
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def helpshift_event(campaign_data,app_data,device_data):
	url='http://perblue.helpshift.com/api/lib/2/events/'
	method='post'
	headers={

		'User-Agent':'Helpshift-Android/'+campaign_data.get('helpshift').get('version')+'/'+device_data.get('os_version'),
		'Accept-Language': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')+';q=1.0',
		'Accept-Encoding': 'gzip',
		'X-HS-V': 'Helpshift-Android/'+campaign_data.get('helpshift').get('version')

	}
	data={		
		'av': campaign_data.get('app_version_name'),
		'dm': device_data.get('model'),
		'e': '[{"ts":"'+str(time.time())+'","t":"a"}]',
		'id': str(uuid.uuid4()),
		'ln': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
		'os': device_data.get('os_version'),
		'platform-id': campaign_data.get('helpshift').get('platform_id'),
		'rs': device_data.get('kernel')+'-'+device_data.get('fingerprint'),
		's': campaign_data.get('helpshift').get('s'),
		'timestamp': str(time.time()),
		'sm' : '{"rs":true,"fp":true,"atai":true,"ia":true,"clc":true}',
		'v': campaign_data.get('helpshift').get('version'),
	}
	
	str1='av='+data.get('av')+'&dm='+data.get('dm')+'&e='+data.get('e')+'&id='+data.get('id')+'&ln='+data.get('ln')+'&method='+method.upper()+'&os='+data.get('os')+'&platform-id='+data.get('platform-id')+'&rs='+data.get('rs')+'&s='+data.get('s')+'&timestamp='+data.get('timestamp')+'&uri='+urlparse.urlparse(url).path+'&v='+data.get('v')
	data['signature']=hmac.new(campaign_data.get('helpshift').get('key'), str1, hashlib.sha256).hexdigest()
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}

def helpshift_config(campaign_data,app_data,device_data,uri):
	
	url='http://perblue.helpshift.com/api/lib/2/'+uri
	method='get'
	headers={
		'User-Agent':'Helpshift-Android/'+campaign_data.get('helpshift').get('version')+'/'+device_data.get('os_version'),
		'Accept-Language': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')+';q=1.0',
		'Accept-Encoding': 'gzip',
		'X-HS-V': 'Helpshift-Android/'+campaign_data.get('helpshift').get('version')
	}
	params={
		'timestamp': str(int(time.time()))+'.000',
		'platform-id': campaign_data.get('helpshift').get('platform_id'),
		'sm' : '{"rs":true,"fp":true,"atai":true,"ia":true,"clc":true}',

	}
	
	str1='method='+method.upper()+'&platform-id='+params.get('platform-id')+'&sm='+params.get('sm')+'&timestamp='+params.get('timestamp')+'&uri='+urlparse.urlparse(url).path
	params['signature']=hmac.new(campaign_data.get('helpshift').get('key'), str1, hashlib.sha256).hexdigest()
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}	


def self1(campaign_data,app_data,device_data):
	
	url='https://login.disneyheroesgame.com/login'
	method='get'
	headers={
		'Content-type': 'application/x-www-form-urlencoded',
		'User-Agent': user_agent(campaign_data,app_data,device_data),
		'Accept-Encoding': 'gzip',

	}
	params={
		'advertisingIdentifier' : device_data.get('adid'),
		'aPMacAddress' : device_data.get('mac'),
		'buildSource' :	'GOOGLE',
		'country' : device_data.get('locale').get('country'),
		'email' : "",	
		'imei' : device_data.get('imei'),
		'isCJK': 'false',
		'language' :device_data.get('locale').get('language'),
		'platform' : 'ANDROID',
		'rawTimezoneOffset': app_data.get('sec')*1000,
		'shardID' : '0',
		'uniqueIdentifier' : device_data.get('android_id'), #util.get_random_string('hex',16),
		'userID' : '0',
		'version' : campaign_data.get('app_version_name'),
		}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}



def service_fyber( campaign_data, device_data, app_data ):
	url= "http://service.fyber.com/installs/v2"
	method= "get"
	headers= {       'Accept-Encoding': 'gzip',
        'User-Agent': user_agent(campaign_data,app_data,device_data)
        }

	if not globals().get('service_fyber_session_id'):
		globals()['service_fyber_session_id'] = str(uuid.uuid4())

	if not globals().get('service_fyber_uid'):
		globals()['service_fyber_uid'] = util.get_random_string('decimal', 17) + '.' + util.get_random_string('decimal', 2)

	params= {       'app_bundle_name': campaign_data.get('package_name'),
        'app_version': campaign_data.get('app_version_name'),
        'appid': '111520',
        'carrier_country': device_data.get('locale').get('language'),
        'carrier_name': device_data.get('carrier'),
        'client': 'sdk',
        'google_ad_id': device_data.get('adid'),
        'google_ad_id_limited_tracking_enabled': 'false',
        'language': device_data.get('locale').get('language') + '_' + device_data.get('locale').get('country'), 
        'manufacturer': device_data.get('brand').upper(),
        'network_connection_type': 'wifi',
        'os_version': device_data.get('os_version'),
        'phone_version': device_data.get('brand').upper() + '_' + device_data.get('model'),
        'platform': 'android',
        'screen_density_x': '228.6',
        'screen_density_y': '227.356',
        'screen_height': device_data.get('resolution').split('x')[1],
        'screen_width': device_data.get('resolution').split('x')[0],
        'sdk_features': 'MPI,VPL,JUD,BLE,INV,IVE',
        'sdk_version': campaign_data.get('service_fyber').get('sdk_version'), 
        'session_id':   globals().get('service_fyber_session_id'),
        'timestamp': int(time.time()),
        'timezone_id': device_data.get('local_tz_name'),  #'Asia/Calcutta',
        'uid': globals().get('service_fyber_uid'),
        }

	data= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


def engine_fyber( campaign_data, device_data, app_data ):
	url= "https://engine.fyber.com/video-cache"
	method= "get"
	headers= {       'Accept-Encoding': 'gzip',
        'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; Lenovo PB2-650M Build/MRA58K)'}

	if not globals().get('service_fyber_session_id'):
		globals()['service_fyber_session_id'] = str(uuid.uuid4())

	if not globals().get('service_fyber_uid'):
		globals()['service_fyber_uid'] = util.get_random_string('decimal', 17) + '.' + util.get_random_string('decimal', 2)

	params= {       'app_bundle_name': campaign_data.get('package_name'),
        'app_version': campaign_data.get('app_version_name'),
        'appid': '111520',
        'carrier_country': device_data.get('locale').get('language'),
        'carrier_name': device_data.get('carrier'),
        'client': 'sdk',
        'google_ad_id': device_data.get('adid'),
        'google_ad_id_limited_tracking_enabled': 'false',
        'language': device_data.get('locale').get('language') + '_' + device_data.get('locale').get('country'), 
        'manufacturer': device_data.get('brand').upper(),
        'network_connection_type': 'wifi',
        'os_version': device_data.get('os_version'),
        'phone_version': device_data.get('brand').upper() + '_' + device_data.get('model'),
        'platform': 'android',
        # 'screen_density_x': '228.6',
        # 'screen_density_y': '227.356',
        # 'screen_height': device_data.get('resolution').split('x')[1],
        # 'screen_width': device_data.get('resolution').split('x')[0],
        'sdk_features': 'MPI,VPL,JUD,BLE,INV,IVE',
        'sdk_version': campaign_data.get('service_fyber').get('sdk_version'), 
        'session_id':   globals().get('service_fyber_session_id'),
        'timestamp': int(time.time()),
        'timezone_id': device_data.get('local_tz_name'),  #'Asia/Calcutta',
        'uid':  globals().get('service_fyber_uid'),
        }

	data= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


def self2(campaign_data,app_data,device_data):
	
	url='http://app-measurement.com/config/app/1%3A39805059380%3Aandroid%3A20b40acb84f2fd31'
	method='get'
	headers={
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding': 'gzip',

	}
	params={
		'app_instance_id' : 'ca4b0fa8e3b863cffe3f755fb0287d5b',
		'gmp_version' : '14366',
		'platform' : device_data.get('manufacturer').lower()
		}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}


def tapjoycall(app_data,campaign_data,device_data):

	url='http://connect.tapjoy.com/api/connect/v3.json'

	method='post'

	headers={
	'Accept-Encoding':'gzip',
	'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
	'Content-Type':'application/x-www-form-urlencoded',

	}

	params=None

	data={
	'ad_tracking_enabled':	'true',
	'advertising_id':	device_data.get('adid'),
	'analytics_id':device_data.get('android_id'),
	'android_id':	device_data.get('android_id'),
	'app_id':	'cf070f32-92d8-48f2-be1e-d81f4070c0b5',
	'app_version':	campaign_data.get('app_version_name'),
	'bridge_version':	'1.0.9',
	'connection_type':	device_data.get('network').lower(),
	'country_code':	device_data.get('locale').get('country').upper(),
	'device_gps_version':	'12874020',
	'device_location':	'false',
	'device_manufacturer':	device_data.get('manufacturer'),
	'device_name'	:device_data.get('model'),
	'device_type'	:'android',
	'display_d'	:'240',
	'display_h':	device_data.get('resolution').split('x')[1],
	'display_multiplier':	'1.0',
	'display_w'	:device_data.get('resolution').split('x')[0],
	'fq30'	:'1',
	'fq7'	:'1',
	'install_id'	:util.get_random_string('hex',64),
	'installed'	:int(app_data.get('times').get('install_complete_time'))*1000,
	'installer'	:'com.android.vending',
	'language_code'	:device_data.get('locale').get('language'),
	'library_revision'	:'68cf46897',
	'library_version':	'11.11.1',
	'mac_address'	:device_data.get('mac').replace(':',''),
	'os_version'	:device_data.get('os_version'),
	'packaged_gps_version':'11910000',
	'pkg_id':	campaign_data.get('package_name'),
	'pkg_rev'	:'134963',
	'pkg_sign'	:util.get_random_string('char_all',27)+'=',
	'pkg_ver'	:campaign_data.get('app_version_name'),
	'platform':	'android',
	'plugin'	:'native',
	'screen_density':	'240',
	'screen_layout_size':	'2',
	'sdk_type'	:'event',
	'session_id'	:util.get_random_string('hex',64),
	'session_total_count':	'3',
	'store_view'	:'true',
	'timestamp'	:int(time.time()),
	'timezone':device_data.get('local_tz_name'),
	'verifier'	:util.get_random_string('hex',64),
	'carrier_country_code': device_data.get('locale').get('language'),
	'carrier_name': device_data.get('carrier'),
	'country_sim'	:	device_data.get('locale').get('country').upper(),
	'mobile_country_code' : device_data.get('mcc'),
	'mobile_network_code' : device_data.get('mnc'),
	}

	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

#########################################
#										#
# 			Extra funcation 			#
#										#
#########################################

def cal_seq(app_data):
	if not app_data.get('seq'):
		app_data['seq'] = 1
	else:
		app_data['seq'] = app_data.get('seq')+1
	return app_data.get('seq')

def user_agent(campaign_data,app_data,device_data):
	if int(device_data.get("sdk")) >19:
		app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'	
	return app_data.get('ua')

def cal_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def generateLag():
	return (random.randint(10,90)*0.001)

def generate_t():
	return "%.3f" %(random.uniform(300,1000))

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())
#########################################
#										#
# 			Util funcation 				#
#										#
#########################################

def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)


def get_params(data_value):
	key = data_value.keys()
	a = ''
	for value in sorted(key):
		a += str(urllib.quote_plus(str(value)).encode('utf-8'))+'='+str(urllib.quote_plus(str(data_value.get(value))).encode('utf-8'))+'&'
	a = a.rsplit("&",1)[0]	

	return a