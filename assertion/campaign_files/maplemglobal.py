# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
from sdk import NameLists
import time
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.nexon.maplem.global',
	'app_size'			 : 86.0,
	'app_name' 			 :'MapleStory M - Open World',
	'app_version_name' 	 :'1.4500.614',
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 : 'jslhucdjlv5s',
		'sdk'		 : 'unity4.12.5@android4.12.4',
		'secret_id'  : '1',
		'secret_key' : '90252570549264994710147058441512431084',
		},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	

	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	set_androidUUID(app_data)

	################generating_realtime_differences##################
	s1=3
	s2=5
	a1=5
	a2=10
	r1=2
	r2=4
	click_time1=True
	c1=16
	c2=18
	ir1=1
	ir2=2
	click_time2=True
	e1=0
	e2=2


	if not app_data.get('uid'):
		app_data['uid'] = util.get_random_string('all',12).upper()

 	###########	 CALLS		 ############
		
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='install_referrer',t1=ir1,t2=ir2)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)
	util.execute_request(**request)
		
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time1,source='reftag',t1=r1,t2=r2,t3=c1,t4=c2)
	util.execute_request(**request)
		
	time.sleep(random.randint(3,4))
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)

	# app open
	time.sleep(random.randint(10,15))
	adjust_token_call(campaign_data, app_data, device_data,event_token = "f0d3bh",t1=e1,t2=e2)

	if random.randint(1,100) <= 90:

		# in app start
		time.sleep(random.randint(15,20))
		adjust_token_xopsek(campaign_data, app_data, device_data,e1,e2)


		if random.randint(1,100) <= 95:

			# in app downloaded - 1
			time.sleep(random.randint(60,90))
			adjust_token_call(campaign_data, app_data, device_data,event_token = "6chj6t",t1=e1,t2=e2)

			# in app downloaded - 2
			time.sleep(random.randint(900,1200))
			adjust_token_8qg41o(campaign_data, app_data, device_data,e1,e2)

			# after inapp download
			adjust_token_call(campaign_data, app_data, device_data,event_token = "1bi9yq",t1=e1,t2=e2)

			# tap to start
			time.sleep(random.randint(15,20))
			adjust_token_call(campaign_data, app_data, device_data,event_token = "yefh3e",t1=e1,t2=e2)

			# server list
			adjust_token_call(campaign_data, app_data, device_data,event_token = "4in7xr",t1=e1,t2=e2)

			app_data['inapp_downloaded'] = True


			if random.randint(1,100) <= 95:

				# select server
				time.sleep(random.randint(5,10))
				adjust_token_2wamuh(campaign_data, app_data, device_data,e1,e2)

				# create charcter
				time.sleep(random.randint(20,40))
				adjust_token_3ysgic(campaign_data, app_data, device_data,e1,e2)

				# login character - 1
				time.sleep(random.randint(10,20))
				adjust_token_vbhqst(campaign_data, app_data, device_data,e1,e2)

				# login character - 2
				adjust_token_call(campaign_data, app_data, device_data,event_token = "k17tsj",t1=e1,t2=e2)

				app_data['charanter_created'] = True


				if random.randint(1,100) <= 95:

					# complete tutorial - 1
					time.sleep(random.randint(15,25))
					adjust_token_call(campaign_data, app_data, device_data,event_token = "bv7r9p",t1=e1,t2=e2)

					# complete tutorial - 2
					time.sleep(random.randint(300,350))
					adjust_token_oz33d1(campaign_data, app_data, device_data,e1,e2)

					app_data['tutorial_complete'] = True


					if random.randint(1,100) <= 85:


						# level 10 achieved - 1
						time.sleep(random.randint(12*60,16*60))
						adjust_token_call(campaign_data, app_data, device_data,event_token = "mivim1",t1=e1,t2=e2)


						# level 10 achieved - 2
						adjust_token_call(campaign_data, app_data, device_data,event_token = "j8jlt4",t1=e1,t2=e2)


						# level 10 achieved - 3
						adjust_token_call(campaign_data, app_data, device_data,event_token = "bzhuwp",t1=e1,t2=e2)

						app_data['level_10'] = True


	set_appCloseTime(app_data)

	return {'status':'true'}


#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	
	###########		INITIALIZE		############	

	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	set_androidUUID(app_data)

	################generating_realtime_differences##################

	s1=1
	s2=3
	a1=2
	a2=4
	e1=0
	e2=1


	if not app_data.get('uid'):
		app_data['uid'] = util.get_random_string('all',12).upper()

 	###########	 CALLS		 ############

 	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2, isOpen=True)
	util.execute_request(**request)
		
	time.sleep(random.randint(3,4))
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)


	if not app_data.get('inapp_downloaded'):

		# app open
		time.sleep(random.randint(10,15))
		adjust_token_call(campaign_data, app_data, device_data,event_token = "f0d3bh",t1=e1,t2=e2)

		# in app start
		time.sleep(random.randint(15,20))
		adjust_token_xopsek(campaign_data, app_data, device_data,e1,e2)

		# in app downloaded - 1
		time.sleep(random.randint(60,90))
		adjust_token_call(campaign_data, app_data, device_data,event_token = "6chj6t",t1=e1,t2=e2)

		# in app downloaded - 2
		time.sleep(random.randint(900,1200))
		adjust_token_8qg41o(campaign_data, app_data, device_data,e1,e2)

		# after inapp download
		adjust_token_call(campaign_data, app_data, device_data,event_token = "1bi9yq",t1=e1,t2=e2)

		# tap to start
		time.sleep(random.randint(15,20))
		adjust_token_call(campaign_data, app_data, device_data,event_token = "yefh3e",t1=e1,t2=e2)

		# server list
		adjust_token_call(campaign_data, app_data, device_data,event_token = "4in7xr",t1=e1,t2=e2)

		app_data['inapp_downloaded'] = True


	if not app_data.get('charanter_created'):

		# select server
		time.sleep(random.randint(5,10))
		adjust_token_2wamuh(campaign_data, app_data, device_data,e1,e2)

		# create charcter
		time.sleep(random.randint(20,40))
		adjust_token_3ysgic(campaign_data, app_data, device_data,e1,e2)

		# login character - 1
		time.sleep(random.randint(10,20))
		adjust_token_vbhqst(campaign_data, app_data, device_data,e1,e2)

		# login character - 2
		adjust_token_call(campaign_data, app_data, device_data,event_token = "k17tsj",t1=e1,t2=e2)

		app_data['charanter_created'] = True


	if not app_data.get('tutorial_complete'):

		# complete tutorial - 1
		time.sleep(random.randint(15,25))
		adjust_token_call(campaign_data, app_data, device_data,event_token = "bv7r9p",t1=e1,t2=e2)

		# complete tutorial - 2
		time.sleep(random.randint(300,350))
		adjust_token_oz33d1(campaign_data, app_data, device_data,e1,e2)

		app_data['tutorial_complete'] = True


	if not app_data.get('level_10'):

		# level 10 achieved - 1
		time.sleep(random.randint(12*60,16*60))
		adjust_token_call(campaign_data, app_data, device_data,event_token = "mivim1",t1=e1,t2=e2)


		# level 10 achieved - 2
		adjust_token_call(campaign_data, app_data, device_data,event_token = "j8jlt4",t1=e1,t2=e2)


		# level 10 achieved - 3
		adjust_token_call(campaign_data, app_data, device_data,event_token = "bzhuwp",t1=e1,t2=e2)

		app_data['level_10'] = True


	time.sleep(random.randint(0,5*60))


	set_appCloseTime(app_data)

	return {'status':'true'}


def test(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	pushtoken(app_data)
	set_androidUUID(app_data)

	################generating_realtime_differences##################
	s1=3
	s2=5
	a1=5
	a2=10
	r1=2
	r2=4
	click_time1=True
	c1=16
	c2=18
	ir1=1
	ir2=2
	click_time2=True
	e1=2
	e2=3

 ###########	 CALLS		 ############
		
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='install_referrer',t1=ir1,t2=ir2)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)
	util.execute_request(**request)
		
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time1,source='reftag',t1=r1,t2=r2,t3=c1,t4=c2)
	util.execute_request(**request)
		
	time.sleep(random.randint(3,4))
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)

	time.sleep(random.randint(150,180))
	adjust_token_xopsek(campaign_data, app_data, device_data,e1,e2)

	time.sleep(random.randint(900,1200))
	adjust_token_8qg41o(campaign_data, app_data, device_data,e1,e2)

	time.sleep(random.randint(40,60))
	adjust_token_2wamuh(campaign_data, app_data, device_data,e1,e2)

	time.sleep(random.randint(80,120))
	adjust_token_3ysgic(campaign_data, app_data, device_data,e1,e2)

	time.sleep(random.randint(30,40))
	adjust_token_vbhqst(campaign_data, app_data, device_data,e1,e2)

	time.sleep(random.randint(300,350))
	adjust_token_oz33d1(campaign_data, app_data, device_data,e1,e2)


	return {'status':'true'}


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def adjust_token_call(campaign_data, app_data, device_data,event_token,t1=0,t2=0):
	print 'Adjust : EVENT ' + event_token
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"uid":app_data.get('uid')})),partner_params=None,event_token=event_token,t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_xopsek(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________xopsek'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"uid":app_data.get('uid')})),partner_params=None,event_token='xopsek',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_8qg41o(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________8qg41o'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"uid":app_data.get('uid')})),partner_params=None,event_token='8qg41o',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_2wamuh(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________2wamuh'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"uid":app_data.get('uid')})),partner_params=None,event_token='2wamuh',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_3ysgic(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________3ysgic'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"uid":app_data.get('uid')})),partner_params=None,event_token='3ysgic',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_vbhqst(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________vbhqst'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"uid":app_data.get('uid')})),partner_params=None,event_token='vbhqst',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_oz33d1(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________oz33d1'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"uid":app_data.get('uid')})),partner_params=None,event_token='oz33d1',t1=t1,t2=t2)
	util.execute_request(**request)


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,t3=0,t4=0,callback_params=None,partner_params=None):
	reftag = ''
	if app_data.get('referrer'):
		temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
		if temp_b.get('adjust_reftag'):
			reftag = temp_b.get('adjust_reftag')[0]
	sdk_clk_time=get_date(app_data,device_data)
	time.sleep(random.randint(t3,t4))
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[1],
		"display_width" : device_data.get('resolution').split('x')[0],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"install_begin_time" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time")),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "0",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"referrer" : "adjust_reftag=ckkIQ6EX8MH11&utm_source=Rankat_NKGL_UASus1&utm_campaign=MY_AOS_NKGL&utm_content=unknown&utm_term=1903_10_",
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : 1,
		"session_length" : session_length,
		"source" : source,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"vm_isa" : "arm64",

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		if click_time:
			data['click_time']=sdk_clk_time
		elif data.get("click_time"):
			del data['click_time']
		# del data['session_length']
		# del data['subsession_count']
		# del data['time_spent']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		if click_time:
			data['click_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("click_time"))
		elif data.get("click_time"):
			del data['click_time']
		
		if data.get('raw_referrer'):
			del data['raw_referrer']
		
		data['install_begin_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time"))
		# del data['last_interval']



	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'click')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())

	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[1],
		"display_width" : device_data.get('resolution').split('x')[0],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "0",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"vm_isa" : "arm64",

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if isOpen:
		def_(app_data,'subsession_count')
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('appCloseTime')-app_data.get('sess_len')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('appCloseTime')-app_data.get('sess_len')
		app_data['sess_len']=int(time.time())


	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'session')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,t1=0,t2=0):
	inc_(app_data,'subsession_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"attribution_deeplink" : "1",
		"created_at" : created_at,
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"needs_response_details" : "1",
		"sent_at" : sent_at,
		"tracking_enabled" : "1",

		}
	data = None

	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('gps_adid'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"callback_params" : callback_params,
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[1],
		"display_width" : device_data.get('resolution').split('x')[0],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"event_count" : app_data.get('event_count'),
		"event_token" : event_token,
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "0",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"session_length" : session_length,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"vm_isa" : "arm64",

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params


	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}




###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	return app_data.get('android_uuid')


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_auth(device_data,app_data,created_at,gps_adid,activity_type):
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activity_type)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'

def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"