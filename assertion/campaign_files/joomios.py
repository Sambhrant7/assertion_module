import random,time,datetime,uuid,urlparse,json,urllib,requests
from sdk import util,purchase, installtimenew
from sdk import getsleep
import clicker,Config

campaign_data = {
	'app_size'	 	: 82.2,#78.4,#60.2,#60.3,#60.1,#56.4,
	'package_name' : 'com.joom.joom',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'ctr':6,
	'app_name' : 'Joom',
	'app_id':'1117424833',
	'app_version_code': '1269',#'1265',#'1', #'1030',#'966',#'957',#'949',#'942',#'934',#'921',#'915',#'898',#'853',#'790',#'408',
	'app_version_name': '2.26.2',#'2.26.1',#'2.26',#'2.25.3',#'2.25.2',#'2.25',#'2.24.1',#'2.24',#'2.23.2',#'2.23.1',#'2.23',#'2.22',#'2.20',#'2.18',#'2.1.2',
	'supported_os':'10.0',#'7.0',
	'supported_countries': 'WW',
	'device_targeting':True,
	'sdk' : 'ios',
	'tracker' : 'Adjust',
	'adjust':{
				'app_token': 'xsssdu6eun0g',
				'sdk': 'ios4.13.0',#'ios4.11.4',
				'app_updated_at': '2019-10-26T19:42:10.000Z',#'2019-10-25T05:45:51.000Z',#'2019-10-15T23:44:48.000Z', #'2019-09-24T22:32:10.000Z',#'2019-08-26T21:22:53.000Z',#'2019-08-16T19:04:48.000Z',#'2019-08-07T20:48:24.000Z',#'2019-07-26T22:04:46.000Z',#'2019-07-23T01:13:09.000Z',#'2019-07-11T14:18:51.000Z',#'2019-07-01T21:52:00.000Z',#'2019-05-31T21:55:45.000Z',#'2019-04-23T03:40:25.000Z',#'2019-02-14T16:48:57.000Z',#'2017-12-21T18:21:37.000Z',
				'secret_id':'16',
				'secret_key':'15359623198029267771258793505735996376',
			},
		
			
	'retention':{
					1:65,
					2:63,
					3:60,
					4:58,
					5:55,
					6:53,
					7:50,
					8:48,
					9:45,
					10:43,
					11:40,
					12:38,
					13:35,
					14:32,
					15:30,
					16:28,
					17:26,
					18:23,
					19:21,
					20:20,
					21:19,
					22:18,
					23:17,
					24:16,
					25:15,
					26:14,
					27:13,
					28:12,
					29:11,
					30:10,
					31:9,
					32:8,
					33:7,
					34:6,
					35:5,
	}
}

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	
	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')
		
def make_subsession_count(app_data,device_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 1
	else:
		app_data['subsession_count'] += 1
		
	return app_data.get('subsession_count')


###########################################################
#						INSTALL							  #
###########################################################

def install(app_data, device_data):	

	print 'please wait installing......................................'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")

	print 'Please wait installing...............'

	if not app_data.get('sec'):
		make_sec(app_data,device_data)
			
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4()).lower()

	app_data['trg_feed'] = "31"+util.get_random_string('decimal',5)+"_1"
		

	###########			Calls		############ 
		
	set_sessionLength(app_data,forced=True,length=0)
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)	

	time.sleep(random.randint(10,15))
	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data, initiated_by='backend')
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST sdk_info ------------------------------------"
	adjustSession = adjust_sdkinfo(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	print "\napi_j_device_create\n"
	request=api_j_device_create( campaign_data, device_data, app_data )
	res = util.execute_request(**request)
	try:
	    app_data['accessToken'] = json.loads(res.get('data')).get('payload').get('accessToken')
	    app_data['AWSALB'] = res.get('res').headers.get('Set-Cookie').split('AWSALB=')[1].split(';')[0]
	    if not app_data.get('accessToken'):
	    	app_data['accessToken'] = 'SEV0001MTU2MTk4MDg5OXxzMzB1dFB1MVVTWjdoSGhwaGZsWEtNVlBTTTFicjFjb1pnX1Uzbl8yUV91dHVLdnM4WFVESHgyYmw3bmdIT25MdHIzMTR1T3lkNWRRRnc3S1J0T0hFcDVxSkMyMEZ0a0ZhTVQtQ2FRdkdOVGVIOHUyLS1VLVBNeTdPNEZRY0c3azBsclZkN0wxRXRxVm1vckZlem9Ld0pxRWU0Z3hvZzhTbVBjMGQxX2dSMkxKbE4wPXxUfb0tEAQDLQ_2w6bhtWBDgYJV1G52Je2LVYjKS11bdg=='
	    if not app_data.get('AWSALB'):
	    	app_data['AWSALB'] = 'kv9kXqnmeAA9gdcbYz632t1skbjEGsetuyuSLFnS74NUFYLZ6E7OGXKy8wqWePfXjeeHFv8d6hQIBJ2Ii5ND+SFLXE3pUQuW1OwLDAFca0bb/BFre4iO93whWdXv'
	except:
	    app_data['accessToken'] = 'SEV0001MTU2MTk4MDg5OXxzMzB1dFB1MVVTWjdoSGhwaGZsWEtNVlBTTTFicjFjb1pnX1Uzbl8yUV91dHVLdnM4WFVESHgyYmw3bmdIT25MdHIzMTR1T3lkNWRRRnc3S1J0T0hFcDVxSkMyMEZ0a0ZhTVQtQ2FRdkdOVGVIOHUyLS1VLVBNeTdPNEZRY0c3azBsclZkN0wxRXRxVm1vckZlem9Ld0pxRWU0Z3hvZzhTbVBjMGQxX2dSMkxKbE4wPXxUfb0tEAQDLQ_2w6bhtWBDgYJV1G52Je2LVYjKS11bdg=='
	    app_data['AWSALB'] = 'kv9kXqnmeAA9gdcbYz632t1skbjEGsetuyuSLFnS74NUFYLZ6E7OGXKy8wqWePfXjeeHFv8d6hQIBJ2Ii5ND+SFLXE3pUQuW1OwLDAFca0bb/BFre4iO93whWdXv'


	print "\napi_j_self\n"
	request=api_j_self( campaign_data, device_data, app_data )
	res = util.execute_request(**request)
	try:
		app_data['customer_id'] = json.loads(res.get('data')).get('payload').get('id')
		if not app_data.get('customer_id'):
			app_data['customer_id'] = "5d1c32054e564d3501bc700f"

	except:
		app_data['customer_id'] = "5d1c32054e564d3501bc700f"


	print "\napi_j_product_list\n"
	request=api_j_product_list( campaign_data, device_data, app_data )
	res = util.execute_request(**request)
	try:
		result = json.loads(res.get('data'))
		contexts = result.get('contexts')
		product_list = []
		for context in contexts:
		    if context.get('value'):
		        for value in context.get('value'):
		            if value.get('id'):
		                product_list.append(value.get('id'))
		if product_list:
			app_data['product_list'] = product_list
		else:
			app_data['product_list'] = [u'1511444897045378033-16-1-26193-1437452708', u'5ccea59d8b2c3701013a939c', u'1497012282591235072-117-1-709-1550304182', u'5cb5a5ac1436d40101085809', u'5c89d45236b54d0101cccadc', u'5cc03b426ecda80101500a43', u'5c80e0638b2c3701018dc3ed', u'5bcd95811436d401016dad6d', u'5ca9a3558b2c370101d0f4e0', u'5cd1502b8b451301012f5bc5', u'1517483394334418687-51-5-26193-4173396764', u'5ca2dcde8b4513010115146e', u'5c7f8f2428fc71010108210d', u'5bf5020d8b4513010121d566', u'5c9df9858b45130101af8980', u'5bfd0d621436d40101a87d3f', u'5c653fbb6ecda8010178e732', u'5c7e1fee36b54d0101189310', u'5c41881c6ecda80101fa3cfb', u'5cbede7328fc7101010d71f5']
	except:
		app_data['product_list'] = [u'1511444897045378033-16-1-26193-1437452708', u'5ccea59d8b2c3701013a939c', u'1497012282591235072-117-1-709-1550304182', u'5cb5a5ac1436d40101085809', u'5c89d45236b54d0101cccadc', u'5cc03b426ecda80101500a43', u'5c80e0638b2c3701018dc3ed', u'5bcd95811436d401016dad6d', u'5ca9a3558b2c370101d0f4e0', u'5cd1502b8b451301012f5bc5', u'1517483394334418687-51-5-26193-4173396764', u'5ca2dcde8b4513010115146e', u'5c7f8f2428fc71010108210d', u'5bf5020d8b4513010121d566', u'5c9df9858b45130101af8980', u'5bfd0d621436d40101a87d3f', u'5c653fbb6ecda8010178e732', u'5c7e1fee36b54d0101189310', u'5c41881c6ecda80101fa3cfb', u'5cbede7328fc7101010d71f5']


	print "\napi_j_product_data\n"
	request=api_j_product_data( campaign_data, device_data, app_data )
	res = util.execute_request(**request)
	try:
	    result = json.loads(res.get('data'))
	    app_data['price'] = result.get('payload').get('prices').get('min')
	    if not app_data.get('price'):
	    	app_data['price'] = random.choice(['13','4.5','2','8','6.5'])
	except:
	    app_data['price'] = random.choice(['13','4.5','2','8','6.5'])


	time.sleep(random.randint(10,20))
	if random.randint(0,100)<90:
		for i in range(0,3):
			interval = random.randint(10,15)
			set_sessionLength(app_data,length=interval)
			print '-----------------------ADJUST Event Product List-------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,'8s8kc2') 
			util.execute_request(**adjust_event_req)


	time.sleep(random.randint(10,20))
	if random.randint(0,100)<90:
		interval = random.randint(10,20)
		set_sessionLength(app_data,length=interval)		
		print '-----------------------ADJUST Event Product View--------------'
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,'e3jy3c') 
		util.execute_request(**adjust_event_req)


		if random.randint(1,100) <= 45:
			interval = random.randint(10,20)
			set_sessionLength(app_data,length=interval)		
			print '-----------------------ADJUST Add to Cart --------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,'h37rbm') 
			util.execute_request(**adjust_event_req)


		if random.randint(1,100) <= 45:
			interval = random.randint(10,20)
			set_sessionLength(app_data,length=interval)		
			print '-----------------------ADJUST View Basket --------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,'ukp9l8') 
			util.execute_request(**adjust_event_req)

	time.sleep(random.randint(10,20))

	random_number = random.randint(1,100)
	ok_purchase = False
	if random_number <= 80 and float(app_data.get('price')) <= 10:
		ok_purchase = True
	elif float(app_data.get('price')) > 10 and float(app_data.get('price')) <= 20:
		ok_purchase = True

	if random.randint(1,100)<=10:
	
		terms_and_cond( campaign_data, device_data, app_data )	

	if ok_purchase and purchase.isPurchase(app_data,day=1):
		
		interval = random.randint(30,60)
		set_sessionLength(app_data,length=interval)	
		print '-----------------------ADJUST Event Purchase----------------------'
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,'dz6rnp',True) 
		util.execute_request(**adjust_event_req)

	
	###########		Finalize	############
	set_appCloseTime(app_data)

	return {'status':True}

###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):

	if not app_data.get('times'):
		print 'please wait ......................................'
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")


	if not app_data.get('sec'):
		make_sec(app_data,device_data)
			
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4()).lower()

	if not app_data.get('trg_feed'):
		app_data['trg_feed'] = "31"+util.get_random_string('decimal',5)+"_1"
		

	###########			Calls		############ 
		

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,'open')
	util.execute_request(**adjustSession)
	set_sessionLength(app_data,forced=True,length=0)	
	
	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	print "\napi_j_device_create\n"
	request=api_j_device_create( campaign_data, device_data, app_data )
	res = util.execute_request(**request)
	try:
	    app_data['accessToken'] = json.loads(res.get('data')).get('payload').get('accessToken')
	    app_data['AWSALB'] = res.get('res').headers.get('Set-Cookie').split('AWSALB=')[1].split(';')[0]
	    if not app_data.get('accessToken'):
	    	app_data['accessToken'] = 'SEV0001MTU2MTk4MDg5OXxzMzB1dFB1MVVTWjdoSGhwaGZsWEtNVlBTTTFicjFjb1pnX1Uzbl8yUV91dHVLdnM4WFVESHgyYmw3bmdIT25MdHIzMTR1T3lkNWRRRnc3S1J0T0hFcDVxSkMyMEZ0a0ZhTVQtQ2FRdkdOVGVIOHUyLS1VLVBNeTdPNEZRY0c3azBsclZkN0wxRXRxVm1vckZlem9Ld0pxRWU0Z3hvZzhTbVBjMGQxX2dSMkxKbE4wPXxUfb0tEAQDLQ_2w6bhtWBDgYJV1G52Je2LVYjKS11bdg=='
	    if not app_data.get('AWSALB'):
	    	app_data['AWSALB'] = 'kv9kXqnmeAA9gdcbYz632t1skbjEGsetuyuSLFnS74NUFYLZ6E7OGXKy8wqWePfXjeeHFv8d6hQIBJ2Ii5ND+SFLXE3pUQuW1OwLDAFca0bb/BFre4iO93whWdXv'
	except:
	    app_data['accessToken'] = 'SEV0001MTU2MTk4MDg5OXxzMzB1dFB1MVVTWjdoSGhwaGZsWEtNVlBTTTFicjFjb1pnX1Uzbl8yUV91dHVLdnM4WFVESHgyYmw3bmdIT25MdHIzMTR1T3lkNWRRRnc3S1J0T0hFcDVxSkMyMEZ0a0ZhTVQtQ2FRdkdOVGVIOHUyLS1VLVBNeTdPNEZRY0c3azBsclZkN0wxRXRxVm1vckZlem9Ld0pxRWU0Z3hvZzhTbVBjMGQxX2dSMkxKbE4wPXxUfb0tEAQDLQ_2w6bhtWBDgYJV1G52Je2LVYjKS11bdg=='
	    app_data['AWSALB'] = 'kv9kXqnmeAA9gdcbYz632t1skbjEGsetuyuSLFnS74NUFYLZ6E7OGXKy8wqWePfXjeeHFv8d6hQIBJ2Ii5ND+SFLXE3pUQuW1OwLDAFca0bb/BFre4iO93whWdXv'


	print "\napi_j_self\n"
	request=api_j_self( campaign_data, device_data, app_data )
	res = util.execute_request(**request)
	try:
		app_data['customer_id'] = json.loads(res.get('data')).get('payload').get('id')
		if not app_data.get('customer_id'):
			app_data['customer_id'] = "5d1c32054e564d3501bc700f"
	except:
		app_data['customer_id'] = "5d1c32054e564d3501bc700f"


	if not app_data.get('customer_id'):
		print "\napi_j_self\n"
		request=api_j_self( campaign_data, device_data, app_data )
		res = util.execute_request(**request)
		try:
			app_data['customer_id'] = json.loads(res.get('data')).get('payload').get('id')
			if not app_data.get('customer_id'):
				app_data['customer_id'] = "5d1c32054e564d3501bc700f"

		except:
			app_data['customer_id'] = "5d1c32054e564d3501bc700f"


	print "\napi_j_product_list\n"
	request=api_j_product_list( campaign_data, device_data, app_data )
	res = util.execute_request(**request)
	try:
		result = json.loads(res.get('data'))
		contexts = result.get('contexts')
		product_list = []
		for context in contexts:
		    if context.get('value'):
		        for value in context.get('value'):
		            if value.get('id'):
		                product_list.append(value.get('id'))
		if product_list:
			app_data['product_list'] = product_list
		else:
			app_data['product_list'] = [u'1511444897045378033-16-1-26193-1437452708', u'5ccea59d8b2c3701013a939c', u'1497012282591235072-117-1-709-1550304182', u'5cb5a5ac1436d40101085809', u'5c89d45236b54d0101cccadc', u'5cc03b426ecda80101500a43', u'5c80e0638b2c3701018dc3ed', u'5bcd95811436d401016dad6d', u'5ca9a3558b2c370101d0f4e0', u'5cd1502b8b451301012f5bc5', u'1517483394334418687-51-5-26193-4173396764', u'5ca2dcde8b4513010115146e', u'5c7f8f2428fc71010108210d', u'5bf5020d8b4513010121d566', u'5c9df9858b45130101af8980', u'5bfd0d621436d40101a87d3f', u'5c653fbb6ecda8010178e732', u'5c7e1fee36b54d0101189310', u'5c41881c6ecda80101fa3cfb', u'5cbede7328fc7101010d71f5']
	except:
		app_data['product_list'] = [u'1511444897045378033-16-1-26193-1437452708', u'5ccea59d8b2c3701013a939c', u'1497012282591235072-117-1-709-1550304182', u'5cb5a5ac1436d40101085809', u'5c89d45236b54d0101cccadc', u'5cc03b426ecda80101500a43', u'5c80e0638b2c3701018dc3ed', u'5bcd95811436d401016dad6d', u'5ca9a3558b2c370101d0f4e0', u'5cd1502b8b451301012f5bc5', u'1517483394334418687-51-5-26193-4173396764', u'5ca2dcde8b4513010115146e', u'5c7f8f2428fc71010108210d', u'5bf5020d8b4513010121d566', u'5c9df9858b45130101af8980', u'5bfd0d621436d40101a87d3f', u'5c653fbb6ecda8010178e732', u'5c7e1fee36b54d0101189310', u'5c41881c6ecda80101fa3cfb', u'5cbede7328fc7101010d71f5']


	print "\napi_j_product_data\n"
	request=api_j_product_data( campaign_data, device_data, app_data )
	res = util.execute_request(**request)
	try:
	    result = json.loads(res.get('data'))
	    app_data['price'] = result.get('payload').get('prices').get('min')
	    if not app_data.get('price'):
	    	app_data['price'] = random.choice(['13','4.5','2','8','6.5'])
	except:
	    app_data['price'] = random.choice(['13','4.5','2','8','6.5'])
	
	time.sleep(random.randint(10,20))
	if random.randint(0,100)<90:
		for i in range(0,3):
			interval = random.randint(10,15)
			set_sessionLength(app_data,length=interval)
			print '-----------------------ADJUST Event Product List-------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,'8s8kc2') 
			util.execute_request(**adjust_event_req)
			
	time.sleep(random.randint(10,20))

	if random.randint(0,100)<90:
		interval = random.randint(10,20)
		set_sessionLength(app_data,length=interval)
		print '-----------------------ADJUST Event Product View--------------'
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,'e3jy3c') 
		util.execute_request(**adjust_event_req)

		if random.randint(1,100) <= 45:
			interval = random.randint(10,20)
			set_sessionLength(app_data,length=interval)		
			print '-----------------------ADJUST Add to Cart --------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,'h37rbm') 
			util.execute_request(**adjust_event_req)


		if random.randint(1,100) <= 45:
			interval = random.randint(10,20)
			set_sessionLength(app_data,length=interval)		
			print '-----------------------ADJUST View Basket --------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,'ukp9l8') 
			util.execute_request(**adjust_event_req)
		
	time.sleep(random.randint(10,20))

	random_number = random.randint(1,100)
	ok_purchase = False
	if random_number <= 80 and float(app_data.get('price')) <= 10:
		ok_purchase = True
	elif float(app_data.get('price')) > 10 and float(app_data.get('price')) <= 20:
		ok_purchase = True

	if random.randint(1,100)<=10:
	
		terms_and_cond( campaign_data, device_data, app_data )		
	

	if ok_purchase and purchase.isPurchase(app_data,day,advertiser_demand=10):

		interval = random.randint(30,60)
		set_sessionLength(app_data,length=interval)	
		print '-----------------------ADJUST Event Purchase----------------------'
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,'dz6rnp',True) 
		util.execute_request(**adjust_event_req)

		
	if random.randint(0,100)<20:
		interval = random.randint(10,15)
		set_sessionLength(app_data,length=interval)	
		print "\n----------------------------ADJUST sdk_click ------------------------------------"
		adjustSession = adjust_sdkclick(campaign_data, app_data, device_data)
		util.execute_request(**adjustSession)
		
		print '-----------------------ADJUST Event Purchase----------------------'
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,'c1f8vl',False,True) 
		util.execute_request(**adjust_event_req)
		
	
	set_appCloseTime(app_data)
	return {'status':True}


########################################
#	self calls
########################################


def api_j_self( campaign_data, device_data, app_data ):
    url= "https://api.joom.com/1.1/users/self"
    method= "get"
    headers= {
        "Accept-Encoding": "br, gzip, deflate",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "Authorization": "Bearer " + app_data['accessToken'],
        "Cookie": "AWSALB="+ app_data['AWSALB'],
        "Fingerprint": util.get_random_string('hex',16).upper(),
        "Timestamp": str(int(time.time()*1000)),
        "User-Agent": campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]}

    params= None

    data= None

    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def api_j_device_create( campaign_data, device_data, app_data ):
    url= "https://api.joom.com/1.1/device/create"
    method= "post"
    app_data['app_session_id'] = str(uuid.uuid4())
    headers= {     
        "Accept-Encoding": "br, gzip, deflate",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "App-Session-Id": app_data['app_session_id'], #"eaf86bed-8b08-4b18-a353-20f15eae3b36",
        "Content-Type": "application/json",
        "Fingerprint": util.get_random_string('hex',16).upper(), #"357C47C274B73107",
        "Timestamp": str(int(time.time()*1000)), #"1561808581145",
        "User-Agent": campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]}

    params= { "requestId": str(uuid.uuid4()) } #"e7938d2e-039d-4f43-8827-3670727f02b3"}

    data= {       "version": {       "appType": "iOS",
                           "appVersion": campaign_data.get('app_version_name') + ".0",
                           "hardwareModel": device_data.get('device_platform'),
                           "hardwareType": "iPhone",
                           "osLevel": device_data.get('os_version')+".0",
                           "osType": "iOS",
                           "osVersion": "Version "+ device_data.get('os_version') +" (Build "+ device_data.get('build') +")"}}

    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


def api_j_product_list( campaign_data, device_data, app_data ):
    url= "https://api.joom.com/1.1/productGroups/0-0-5-0-0/products"
    method= "get"
    headers= {       
        "Accept-Encoding": "br, gzip, deflate",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "App-Session-Id": app_data['app_session_id'], #"eaf86bed-8b08-4b18-a353-20f15eae3b36",
        "Authorization": "Bearer "+app_data['accessToken'],
        "Cookie": "AWSALB="+ app_data['AWSALB'],
        "Fingerprint": util.get_random_string('hex',16).upper(), #"C88AA099CBFFE7DF",
        "Timestamp": str(int(time.time()*1000)), #"1561808595023",
        "User-Agent": campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]}

    params= None

    data= None
    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def api_j_product_data( campaign_data, device_data, app_data ):
    app_data['product_id'] = random.choice(app_data.get('product_list'))
    url= "https://api.joom.com/1.1/products/" + app_data['product_id']
    method= "get"
    headers= {      
        "Accept-Encoding": "br, gzip, deflate",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "App-Session-Id": app_data['app_session_id'], #"eaf86bed-8b08-4b18-a353-20f15eae3b36",
        "Authorization": "Bearer "+app_data['accessToken'],
        "Cookie": "AWSALB="+ app_data['AWSALB'],
        "Fingerprint": util.get_random_string('hex',16).upper(), #"E8B8E0DD8F03849F",
        "Timestamp": str(int(time.time()*1000)), #"1561808750340",
        "User-Agent": campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]}

    params= None

    data= None
    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

#############################################################

def terms_and_cond( campaign_data, device_data, app_data ):

	time.sleep(random.randint(10,60))
	print "\nself_call_app_adjust\n"
	request=app_adjust_session( campaign_data, device_data, app_data )
	util.execute_request(**request)

	time.sleep(random.randint(5,30))
	callback_params = {"url":"https://www.joom.com/en/app-view/legal"}
	print "\nself_call_app_adjust\n"
	request=app_adjust( campaign_data, device_data, app_data, callback_params=callback_params)
	util.execute_request(**request)

	time.sleep(random.randint(5,30))
	callback_params = {"url":"https://www.joom.com/en/app-view/legal/terms"}
	print "\nself_call_app_adjust\n"
	request=app_adjust( campaign_data, device_data, app_data, callback_params=callback_params)
	util.execute_request(**request)

	time.sleep(random.randint(5,30))
	callback_params = {"url":"https://www.joom.com/en/app-view/legal"}
	print "\nself_call_app_adjust\n"
	request=app_adjust( campaign_data, device_data, app_data, callback_params=callback_params)
	util.execute_request(**request)

	time.sleep(random.randint(5,30))
	callback_params = {"url":"https://www.joom.com/en/app-view/legal/privacy"}
	print "\nself_call_app_adjust\n"
	request=app_adjust( campaign_data, device_data, app_data, callback_params=callback_params)
	util.execute_request(**request)

	time.sleep(random.randint(5,30))
	callback_params = {"url":"https://www.joom.com/en/app-view/legal"}
	print "\nself_call_app_adjust\n"
	request=app_adjust( campaign_data, device_data, app_data, callback_params=callback_params)
	util.execute_request(**request)

	time.sleep(random.randint(5,30))
	callback_params = {"url":"https://www.joom.com/en/app-view/legal/ipr-protection"}
	print "\nself_call_app_adjust\n"
	request=app_adjust( campaign_data, device_data, app_data, callback_params=callback_params)
	util.execute_request(**request)



#########################################################################################

	
def app_adjust_session( campaign_data, device_data, app_data ):
	url= "https://app.adjust.com/session"
	method= "get"
	headers= {       
		"Accept": "*/*",
        "Accept-Encoding": "br, gzip, deflate",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "Access-Control-Request-Headers": "client-sdk",
        "Access-Control-Request-Method": "GET",
        "Origin": "https://www.joom.com",
        "Referer": "https://www.joom.com/en/app-view/legal",
        "User-Agent": device_data.get('User-Agent')
        }

	params= {       
		"app_token": "rrjrofpr96gw",
        "environment": "production",
        "idfa": app_data.get('idfa_id'),
        "os_name": "iOS"
        }

	data= None
	requests.options(url,headers=headers, params=params, data=data,verify = False)
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def app_adjust( campaign_data, device_data, app_data, callback_params={}):
	url= "https://app.adjust.com/event"
	method= "get"
	headers= {       
		"Accept": "*/*",
        "Accept-Encoding": "br, gzip, deflate",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "Client-SDK": "js4.1.0",
        "Origin": "https://www.joom.com",
        "Referer": "https://www.joom.com/en/app-view/legal",
        "User-Agent": device_data.get('User-Agent')
        }

	params= {       
		"app_token": "rrjrofpr96gw",
        "callback_params": json.dumps(callback_params),
        "environment": "production",
        "event_token": "84rgq1",
        "idfa": app_data.get('idfa_id'),
        "os_name": "iOS"
        }

	data= None
	requests.options(url,headers=headers, params=params, data=data,verify = False)
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,type='install'):

	app_data['subsession_count'] = 1
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'app_updated_at': campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': get_date(app_data,device_data,early=random.uniform(1,3)),
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'sent_at': get_date(app_data,device_data),
			'session_count': make_session_count(app_data,device_data),
			'tracking_enabled':	0,
			'os_build':device_data.get('build'),
			'installed_at': datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'connectivity_type':'2',
			'install_receipt': install_receipt(),
			'mcc':device_data.get('mcc'),
			'mnc':device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyHSDPA',
			}
	
	if type == "open":
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = app_data.get('sessionLength')
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = app_data.get('sessionLength')
	

	headers['Authorization']= get_auth(app_data,activity_kind='session',idfa=data['idfa'],created_at=data['created_at'])

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adjust_sdkclick(campaign_data, app_data, device_data):
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	header = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}
	data = {
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'attribution_deeplink': '1',
			'os_name': 'ios',
			'cpu_type':'CPU_SUBTYPE_ARM64_V8',
			'environment':'production',
			'session_count':str(app_data.get('session_count')),
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			'created_at': datetime.datetime.utcfromtimestamp((time.time()-0.04)-app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'idfv':app_data.get('idfv_id'),
			'bundle_id':campaign_data.get('package_name'),
			'os_version':device_data.get('os_version'),
			'event_buffering_enabled':0,
			'needs_response_details':1,
			'app_version':campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country').upper(),
			'language':device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': '0',
			'os_build':device_data.get('build'),
			'installed_at':datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'hardware_name':'N53AP',
			'details':json.dumps({"Version3.1":{"iad-attribution":"false"}}),
			'source':'iad3',
			'device_name':device_data.get('device_platform'),
			'sent_at': datetime.datetime.utcfromtimestamp((time.time())-app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'connectivity_type':'2',
			'install_receipt': install_receipt(),
			'mcc':device_data.get('mcc'),
			'mnc':device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyHSDPA',
			}
	
	def_(app_data,'subsession_count')
	def_sessionLength(app_data)
	
	data['last_interval'] = get_lastInterval(app_data)
	data['session_length'] = app_data.get('sessionLength')
	data['subsession_count'] = app_data.get('subsession_count')
	data['time_spent'] = app_data.get('sessionLength')

	if not app_data.get('last_interval_1'):
		app_data['last_interval_1'] = int(time.time()*1000)

	header['Authorization']= get_auth(app_data,activity_kind='click',idfa=data['idfa'],created_at=data['created_at'])


	return {'url':url, 'httpmethod':'post', 'headers':header, 'params':None, 'data':data}
	
def adjust_attribution(campaign_data, app_data, device_data, initiated_by='sdk'):
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	
	data={}

	params = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink' : 1,
			'created_at': get_date(app_data,device_data,early=random.uniform(4,4.5)),
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'needs_response_details': 1,
			'sent_at': get_date(app_data,device_data),
			'initiated_by':initiated_by,
			}
	
	
	headers['Authorization']= get_auth(app_data,activity_kind='attribution',idfa=params['idfa'],created_at=params['created_at'])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	


def adjust_sdkinfo(campaign_data, app_data, device_data):
	app_data['push_token'] = util.get_random_string('hex',64)
	url = 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}
	
	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink':	1,
			'created_at': datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'needs_response_details': 1,
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'push_token':app_data.get('push_token'),
			'source': 'push',
			'sent_at': datetime.datetime.utcfromtimestamp((time.time())-app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			}

	headers['Authorization']= get_auth(app_data,activity_kind='info',idfa=data['idfa'],created_at=data['created_at'])

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	
def adjust_event(campaign_data, app_data, device_data,event_token,purchase=False,order=False):
	def_sessionLength(app_data)
	def_(app_data,'subsession_count')
	if not app_data.get('device_token'):	
		app_data['device_token'] = util.get_random_string("all",22)	
		
	url = 'http://app.adjust.com/event'
	method = 'post'
	header = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}

	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4()).lower()

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	if not app_data.get('user_id'):
		app_data['user_id'] = util.get_random_string(type='decimal',size=8)

	data = {
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'hardware_name':'D101AP',	
			'event_buffering_enabled':	0,
			'cpu_type':'CPU_SUBTYPE_ARM64_V8',
			'attribution_deeplink': '1',
			'os_name':'ios',
			'environment':'production',
			'needs_response_details':1,
			'event_count':app_data.get('event_count'),
			'time_spent':app_data.get('sessionLength'),
			'session_count':str(app_data.get('session_count')),
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			'created_at': get_date(app_data,device_data,early=random.uniform(1,3)),
			'event_token':event_token,
			'bundle_id':campaign_data.get('package_name'),
			'subsession_count':str(app_data.get('session_count')),
			'os_version':device_data.get('os_version'),
			'app_version': campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country'),
			'language':	device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'session_length':app_data.get('sessionLength'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': '0',
			'device_name':device_data.get('device_platform'),
			'sent_at': get_date(app_data,device_data),
			'os_build':	device_data.get('build'),
			'connectivity_type':'2',
			'install_receipt': install_receipt(),
			'mcc':device_data.get('mcc'),
			'mnc':device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyHSDPA',#'CTRadioAccessTechnologyEdge'			
			}

	if not app_data.get('x'):
		app_data['x']=str(util.get_random_string('hex',24))
	if not app_data.get('y'):
		app_data['y']=str(util.get_random_string('decimal',19))+'-'+str(random.randint(10,100))+'-1-'+str(util.get_random_string('decimal',5))+'-'+str(util.get_random_string('decimal',10))
	if not app_data.get('z'):
		app_data['z']=str(util.get_random_string('hex',24))

	if not app_data.get('customer_id'):
		app_data['customer_id'] = util.get_random_string('hex',24)


	if order== True:
		data['partner_params']= {"criteo_deeplink":"joom:\/\/orders"}


	if purchase== True:
		data['currency']= 'USD'
		j=  urllib.quote(str([{"i":app_data.get('product_id').encode('utf-8'),"pr":float(app_data.get('price')),"q":1}]))
		data['partner_params']=json.dumps({"criteo_p":j,"transaction_id":app_data.get('product_id'),"customer_id":app_data.get('customer_id')})
		data['revenue']=app_data.get('price')


	if event_token=='8s8kc2':
		random.shuffle(app_data.get('product_list'))
		if len(app_data.get('product_list')) >=3:
			j = [ x.encode('utf-8') for x in app_data.get('product_list')[:3] ]
		else:
			j = [app_data.get('x'),app_data.get('y'),app_data.get('z')]

		j= urllib.quote(str(j))
		data["partner_params"]=json.dumps({"trg_feed":app_data.get('trg_feed'),"criteo_p":j,"customer_id":app_data.get('customer_id')})


	if event_token=='e3jy3c':
		data["partner_params"]=json.dumps({"trg_feed":app_data.get('trg_feed'),"criteo_p":app_data.get('product_id'),"customer_id":app_data.get('customer_id'),"productid":app_data.get('product_id')})

	if event_token=='ukp9l8':
		j= urllib.quote(str([{"i":app_data.get('product_id').encode('utf-8'),"pr":float(app_data.get('price')),"q":1}]))
		data["partner_params"]=json.dumps({"trg_feed":app_data.get('trg_feed'),"criteo_p":j,"customer_id":app_data.get('customer_id')})

		
	if event_token=='h37rbm':
		data["partner_params"]=json.dumps({"trg_feed":app_data.get('trg_feed'),"productid":app_data.get('product_id').encode('utf-8')})


	if app_data.get('push_token'):
		data['push_token']=app_data.get('push_token')

	header['Authorization']= get_auth(app_data,activity_kind='event',idfa=data['idfa'],created_at=data['created_at'])	

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':None, 'data': data}	
	
###########################################################
#						Self Calls						  #
###########################################################	

def branch_install(campaign_data, app_data, device_data):
	get_hardwareid(app_data)
	method = 'post'
	url='https://api.branch.io/v1/install'
	header={
			'User-Agent': 'Joom/408 CFNetwork/808.0.2 Darwin/16.0.0',
			'Content-Type': 'application/json',
			}
	params={}
	data={
			"sdk":campaign_data.get('branch').get('sdk'),
			"facebook_app_link_checked":0,
			"language":device_data.get('locale').get('language'),
			"user_agent":device_data.get('User-Agent'),
			"screen_height":int(device_data.get('resolution').split('x')[1]),
			"country":device_data.get('locale').get('country'),
			"app_version":campaign_data.get('app_version_code'),
			"update":0,
			"ad_tracking_enabled":1,
			"brand":"Apple",
			"retryNumber":0,
			"uri_scheme":"joom",
			"os":"iOS",
			"hardware_id":app_data.get('hardware_id'),
			"ios_vendor_id":app_data.get('idfv_id'),
			"ios_bundle_id":campaign_data.get('package_name'),
			"screen_width":int(device_data.get('resolution').split('x')[0]),
			"hardware_id_type":"idfa",
			"debug":0,
			"model":device_data.get('device_platform'),
			"os_version":device_data.get('os_version'),
			"is_hardware_id_real":1,
			"branch_key":campaign_data.get('branch').get('key'),
			"apple_ad_attribution_checked":0
			}
	return {'url':url, 'httpmethod':method, 'headers':header, 'params': None, 'data': json.dumps(data)}
	
def branch_close(campaign_data, app_data, device_data):
	get_hardwareid(app_data)
	method = 'post'
	url='http://api.branch.io/v1/close'
	header={
			'User-Agent': 'Joom/408 CFNetwork/808.0.2 Darwin/16.0.0',
			'Content-Type': 'application/json',
			}
	params={}
	data={
			"session_id":app_data.get('session_id'),
			"language":device_data.get('locale').get('language'),
			"user_agent":device_data.get('User-Agent'),
			"country":device_data.get('locale').get('country'),
			"screen_height":int(device_data.get('resolution').split('x')[1]),
			"ad_tracking_enabled":1,
			"brand":"Apple",
			"retryNumber":0,
			"identity_id":app_data.get('identity_id'),
			"os":"iOS",
			"hardware_id":app_data.get('hardware_id'),
			"ios_vendor_id":app_data.get('idfv_id'),
			"screen_width":int(device_data.get('resolution').split('x')[0]),
			"hardware_id_type":"idfa",
			"instrumentation":[{"/v1/install-brtt":"1253"}],
			"os_version":device_data.get('os_version'),
			"is_hardware_id_real":1,
			"model":device_data.get('device_platform'),
			"branch_key":device_data.get('device_platform'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"sdk":campaign_data.get('branch').get('sdk')
			
			}

	return {'url':url, 'httpmethod':method, 'headers':header, 'params': None, 'data': json.dumps(data)}		
			
def current_location():
	
	url='https://api.letgo.com/api/iplookup.json'
	header={
			'User-Agent': 'okhttp/2.3.0'
			}
	params={}
	data={}
	
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}	


###########################################################
#						UTIL							  #
###########################################################
def def_puuid(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid']=str(uuid.uuid4()).lower()
		
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat ='ios'):
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('app_id'))
	
def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_hardwareid(app_data):
	if not app_data.get('hardware_id'):
		app_data['hardware_id']=str(uuid.uuid4()).upper()

def get_auth(app_data,activity_kind,idfa,created_at):
	final_str=campaign_data.get('adjust').get('secret_key')+str(activity_kind)+str(idfa)+str(created_at)
	sign=util.sha256(final_str)
	auth='Signature secret_id="'+str(campaign_data.get('adjust').get('secret_id'))+'",signature="'+str(sign)+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'
	return auth	


def install_receipt():
	return 'MIISlAYJKoZIhvcNAQcCoIIShTCCEoECAQExCzAJBgUrDgMCGgUAMIICNQYJKoZIhvcNAQcBoIICJgSCAiIxggIeMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgCNMA0CAQMCAQEEBQwDOTY2MA0CAQoCAQEEBRYDMTIrMA0CAQ0CAQEEBQIDAdWIMA0CARMCAQEEBQwDODk4MA4CAQECAQEEBgIEQpqMwTAOAgEJAgEBBAYCBFAyNTMwDgIBCwIBAQQGAgQHDN8zMA4CARACAQEEBgIEMZ9IrzAQAgEPAgEBBAgCBh75p6ie4jAUAgEAAgEBBAwMClByb2R1Y3Rpb24wFwIBAgIBAQQPDA1jb20uam9vbS5qb29tMBgCAQQCAQIEEDqi6llLs+sIErpZWjDBUUUwHAIBBQIBAQQUBrOarIptFy4ICp9KAd9L8fiqlOUwHgIBCAIBAQQWFhQyMDE5LTA4LTMxVDA0OjQxOjIyWjAeAgEMAgEBBBYWFDIwMTktMDgtMzFUMDQ6NDE6MjJaMB4CARICAQEEFhYUMjAxOS0wNi0yOVQxMDoyMToyNFowTgIBBgIBAQRGjOz7tEzXX29FwtZjZ4ttUbkxoX8Kh/a1O3G2TdAYT2ZNBJdtMnyLEF1PxFsPaeXvy6AVVE4fDnelWWCt0n0+aJgjUWRpiDBQAgEHAgEBBEhif9FNmm2bIRQ9c+L5i2T6hUg59XqZe6n1at1VWY/XT0vRWmoCrXWfpP3xry+M353ANFs6kxLmpvZzJWWkohNqWr5vbLEXqAaggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAFDFg6J1VE3zMVax1fecw+BgJh+QE72HoJeg1yQX7Zrt1xeOGdndIkP396esgJYgg31K1oR9DKKBJUIccLHzsA8Vy49cmuniDcjPotxdtcnHKBsjS6ehApytNYT8Qd/TcWXeP0SH9cCzRo6RhFAIiCH0Ivizv5rP/tfXJdmN4/kiTTs/qX0ZiRaZmEmJKG95W/FXMz1y67FkMBX87vqjU/37T4KU/6Ix9QVmI1FT68mNCrqQSkl8mrKwZerfK/m/tFNyLqzlMA7ulbm6vDGpi0JbaG9ZDOhum2vJNhl6nScy59UV0+oyIfYbhr2EHmONEbolHe/AjS9fN1CqZaCzX7g='
	

