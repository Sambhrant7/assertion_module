from sdk import util,installtimenew
from sdk import NameLists
from sdk import getsleep
import time
import random
import json
import string
import datetime
import urllib,uuid
import clicker
import Config

#######################################################

# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'com.etoro.openbook',
	'app_name' 			 :'Etoro',
	'app_version_code'	 : '1114',#'1113',#'1107',#'1106',#'1095',#'1058',#'1056',#'1042',#'1035',
	'app_version_name'	 : '189.0.0',#'187.0.0',#'182.0.0',#'181.0.0',#'162.0.0',#'127.0.0',#'124.0.0',#'108.0.0',#'102.0.0',
	'no_referrer'		 : True,
	'supported_os'		 :'4.4',
	'app_size'			 : 25.0,#24.0,
	'device_targeting':True,
	'ctr':6	,
	'supported_countries':'WW',
	'CREATE_DEVICE_MODE':3,
	'tracker'			 :'appsflyer',
	'appsflyer':{
		'key'		 : 'pVyuiv9hzGa3xRsQtzCcL',
		'dkh'		 : 'pVyuiv9h',
		'buildnumber': '4.8.19',#'4.7.4',
		'version'	 : 'v4',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	print 'plz wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")
	time.sleep(random.randint(10,20))
	###########		INITIALIZE		############	
	def_eventsRecords(app_data)
	app_data['registration_flag']=False
	###########		CALLS		################	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))

	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])

	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))


 	print "gcm token call"
	req=android_clients_google( campaign_data, device_data, app_data )
	res=util.execute_request(**req)
	try:
		app_data['gcm_token']=res.get('data').split('token=')[1]
		print app_data['gcm_token']
	except:
		app_data['gcm_token']='dt6tJzSx-m4:APA91bF7wZuNfRDqQ7QzoHtf021Ywb5Wb5lzRNZCJaRdrQwQ3aGeQqe9K1_uT5PjOfzjgkR8PH-5sLRnWSux1UfzWdlnM87dGcHQBLLVTLoH3yWMKkrO5W-bPfILHGBWbmX5xRxTdS0E'



	print "Apps Flyer register call\n==========================================================\n"
	appsflyerTrack=appsflyer_register(campaign_data, app_data, device_data)
	util.execute_request(**appsflyerTrack)

	print '\nAppsflyer : EVENT__________________________enableUninstallTracking'
	timeSinceLastCall = random.randint(40,60)
	eventName		  = "enableUninstallTracking Android"
	eventValue		  = json.dumps({"status":"success"})
	request	= appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall,eventName,eventValue)
	util.execute_request(**request)
	
	time.sleep(random.randint(10,20))

	print '\nAppsflyer : TRACK____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true")
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	print '\nself call 1___________________________________'
	self_call  = self_countrie_ip_me(campaign_data, app_data, device_data)
	result=util.execute_request(**self_call)

	print '\nself call 1___________________________________'
	self_call  = self_rankings_metadata(campaign_data, app_data, device_data)
	result=util.execute_request(**self_call)
	try:
		cookie = result.get('res').headers.get('Set-Cookie')
		app_data['incap_sess_2'] = cookie.split('incap_ses_')[1].split(';')[0]
		app_data['TS01'] = cookie.split('TS01')[1].split(';')[0]
		app_data['nlbi_'] = cookie.split('nlbi_')[1].split(';')[0]
		app_data['visid_'] = cookie.split('visid_')[1].split(';')[0]

		print app_data['incap_sess_2']
		print app_data['TS01']
		print app_data['nlbi_']
		print app_data['visid_']

	except:
		app_data['incap_sess_2'] = '736_20269=ierdGDo7r3GFwcJls8w2Chr+GFsAAAAA6pcj9Ol9BHoWFmQtzpbWUA=='
		app_data['TS01'] = 'cb89f3=01f1b32d7e052a6a02bdcff72602345592ea3641d0b18f7fd35798bb9ac47daec2c3ae0ec3cc32d454b51b27c070902c4fd2835621'
		app_data['nlbi_'] = '20269=2wGbZu//lhphZ8xYW3HQvwAAAAA8REDPHo4tmElD6jKYFeXD'
		app_data['visid_'] = 'incap_20269=1ql6fp6nTa6nsnr16eQMZBr+GFsAAAAAQUIPAAAAAADPvyxqVoR0etcrM4k+VcI0'


	# try:
	# 	app_data['incap_sess'] = str(result.get('res').headers.get('Set-Cookie')).split('incap_ses_')[1].split(';')[0]
	# except:
	# 	app_data['incap_sess'] = '736_20269=Uw1Qbo48FWwcwcJls8w2ChL+GFsAAAAA9d7+S8XGlFd5UhHVw1r/Kw=='

	# print '\nself call 1___________________________________'
	# self_call  = self_instruments(campaign_data, app_data, device_data, method='get')
	# self_call['params']['client_request_id']= app_data.get('client_request_id')
	# self_call['params']['InstrumentDataFilters']= 'Activity,Rates'
	# result=util.execute_request(**self_call)
	# try:
	# 	cookie = result.get('res').headers.get('Set-Cookie')
	# 	app_data['incap_sess_2'] = cookie.split('incap_ses_')[1].split(';')[0]
	# 	app_data['TS01'] = cookie.split('TS01')[1].split(';')[0]
	# 	app_data['nlbi_'] = cookie.split('nlbi_')[1].split(';')[0]
	# 	app_data['visid_'] = cookie.split('visid_')[1].split(';')[0]
	# except:
	# 	app_data['incap_sess_2'] = '736_20269=ierdGDo7r3GFwcJls8w2Chr+GFsAAAAA6pcj9Ol9BHoWFmQtzpbWUA=='
	# 	app_data['TS01'] = 'cb89f3=01f1b32d7e052a6a02bdcff72602345592ea3641d0b18f7fd35798bb9ac47daec2c3ae0ec3cc32d454b51b27c070902c4fd2835621'
	# 	app_data['nlbi_'] = '20269=2wGbZu//lhphZ8xYW3HQvwAAAAA8REDPHo4tmElD6jKYFeXD'
	# 	app_data['visid_'] = 'incap_20269=1ql6fp6nTa6nsnr16eQMZBr+GFsAAAAAQUIPAAAAAADPvyxqVoR0etcrM4k+VcI0'

	
	if random.randint(1,100)<=10:
		return {'status':'true'}

	print '\nAppsflyer : TRACK____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false")
	util.execute_request(**request)

	print '\nAppsflyer : STATS____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)

	print '\nAppsflyer : TRACK____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false")
	util.execute_request(**request)

	print '\nAppsflyer : STATS____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)

	# print '\nAppsflyer : API____________________________________'
	# request  = appsflyer_api(campaign_data, app_data, device_data)
	# response = util.execute_request(**request)
	# catch(response,app_data,"appsflyer")

	# print '\nAppsflyer : REGISTER____________________________________'
	# request  = appsflyer_register(campaign_data, app_data, device_data)
	# util.execute_request(**request)

	# Uncomment and use this as per your own event requirement.
	call_pattern(campaign_data, app_data, device_data)

	print '\nAppsflyer : STATS____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)

	

	###########		FINALIZE	################
		
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	##########		INITIALIZE		############	
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")
	def_eventsRecords(app_data)
	if not app_data.get('registration_flag'):
		app_data['registration_flag']=False

	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))

	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])

	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))


	##########		CALLS		################	
	print '\nAppsflyer : TRACK____________________________________'
	timeSinceLastCall = random.randint(45,90)
	request = appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall,isOpen=True)
	util.execute_request(**request)

	call_pattern(campaign_data, app_data, device_data)

	print '\nAppsflyer : STATS____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)

	###########		FINALIZE	################
		
	return {'status':'true'}


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################
def call_pattern(campaign_data, app_data, device_data):

	if app_data.get('registration_flag')==False and random.randint(1,100)<=90:

		print '\n self_register_user___________________________________'
		self_call  = self_register_user(campaign_data, app_data, device_data)
		registered_result=util.execute_request(**self_call)
		try:
			cookie = registered_result.get('res').headers.get('Set-Cookie')
			LoginJsonData = json.loads(registered_result.get('data')).get('LoginData')
			app_data['OrigCID'] = cookie.split('OrigCID=')[1].split(';')[0]
			app_data['GCID'] = cookie.split('GCID=')[1].split(';')[0]
			app_data['RegistrationPixels'] = cookie.split('RegistrationPixels=')[1].split(';')[0]
			app_data['TS01'] = cookie.split('TS01')[1].split(';')[0]
			app_data['accessToken'] =LoginJsonData.get('accessToken')
			app_data['antiCsrfToken'] =LoginJsonData.get('antiCsrfToken')
			app_data['realCid'] =LoginJsonData.get('realCid')
		except:
			app_data['OrigCID'] = "11213659"
			app_data['GCID'] = "9905882"
			app_data['RegistrationPixels'] = '%7B%22AffiliateId%22%3A56663%2C%22SerialId%22%3Anull%2C%22Custom%22%3Anull%2C%22CID%22%3A9618982%2C%22GCID%22%3A9905882%2C%22OriginalCorrelationId%22%3A%22c8e04679-8ede-4518-8d63-3d429b2aff32%22%7D'
			app_data['TS01'] = 'cb89f3=01f1b32d7ef7637896720e2d4ebd1f0ebcbc2b345bb18f7fd35798bb9ac47daec2c3ae0ec3d3f5b66996e15c6282141d88da832aae144b4d20b8438f610db96c7891dcefa86764eed3fe03dcda68c1d93dd7541be31de532a080f00e42d8731fb54f4c7e6cc73085fd2fcc6304f4ff8545ded93952'
			app_data['accessToken'] = 'lJF4vMnt4O4NEnAnQiK49Mpy0vSzQMA0VdMvGbQkqfNqJJyJZw5c3z8aPWKEDj8z04tlVEstYtx-RcBqZ8QTXdIbq2WQF2XiWeutyRRbiwNtWAye6LoHRYU1I7HoCm0AC5LYIXkqvsFaU3cPzZEWVOvKkOMv9BwGM2LGPZnBXPXSUe2UiVEsvpuEkssnF6iJ4mTub6wN8Ib1P310%7C5d9JLbXoWhPCiFNeklhRMDvARXkGeG7QvUzANisU6zA82SoxpdZyFLVTcvmoB3SqDTNlOd4pczv9ULgq%7CjqV-bB-n-STButu%7ClFAUS3rGk4hUYI%7ClKcKRuG%7CJMuvzlu1ImJyg__'
			app_data['antiCsrfToken'] = 'P6nMKu7NVoYUWCdi1lccDg__'
			app_data['realCid'] = '9618982'
			

		print '\nself_Login_user___________________________________'
		self_call  = self_Login_user(campaign_data, app_data, device_data)
		util.execute_request(**self_call)

		print '\nAppsflyer : EVENT__________________________registration'
		timeSinceLastCall = random.randint(40,60)
		eventName		  = "registration"
		eventValue		  = json.dumps({"af_content_id":str(app_data.get('realCid')) if app_data.get('realCid') else get_af_content_id(app_data)})
		request	= appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall,eventName,eventValue)
		util.execute_request(**request)
		app_data['registration_flag']=True
		time.sleep(random.randint(20,40))


	for i in range(0,random.randint(2,5)):
		print '\nAppsflyer : TRACK____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false")
		util.execute_request(**request)

		print '\nAppsflyer : STATS____________________________________'
		request  = appsflyer_stats(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(20,40))



def get_af_content_id(app_data):
	if not app_data.get('af_content_id'):
		app_data['af_content_id']=str(util.get_random_string('decimal',7))
	return app_data.get('af_content_id')

###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=False):
	def_installDate(app_data,device_data)
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')

	method  = 'post'
	url 	= 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}		
	params  = {
		'app_id'		  : campaign_data.get('package_name'),
		'buildnumber'	  : campaign_data.get('appsflyer').get('buildnumber'),
		}	
	data    = {
		"advertiserId"			: device_data.get('adid'),
		"advertiserIdEnabled"	: "true",
		"af_events_api"			: "1",
		"af_preinstalled"		: "false",
		"af_timestamp"			: timestamp(),
		# "android_id"			: device_data.get('android_id'),

		# "af_gcm_token": app_data.get('gcm_token'),
		"app_version_code"		: campaign_data.get('app_version_code'),
		"app_version_name"		: campaign_data.get('app_version_name'),
		"appsflyerKey"			: campaign_data.get('appsflyer').get('key'),
		"brand"					: device_data.get('brand'),
		"carrier"				: device_data.get('carrier'),
		"counter"				: str(app_data.get('counter')),
		"country"				: device_data.get('locale').get('country'),
		"date1"					: app_data.get('installDate'),
		"date2"					: app_data.get('installDate'),
		"device"				: device_data.get('device'),
		"deviceData"			: {
									"arch": "",
									"btch": "no",
									"btl": get_batteryLevel(),
									"build_display_id": device_data.get('build'),
									"cpu_abi": device_data.get('cpu_abi'),
									"cpu_abi2": device_data.get('cpu_abi'),
									"dim"					: {
																"d_dpi": device_data.get('dpi'),
																"size": app_data.get('dim_size'),
																"x_px": device_data.get('resolution').split('x')[1],
																"xdp": "221.225",
																"y_px": device_data.get('resolution').split('x')[0],
																"ydp": "221.672"}
									},
		"deviceRm"				: device_data.get('build'),
		"deviceType"			: "user",
		"firstLaunchDate"		: app_data.get('firstLaunchDate'),
		# "gaidError"				: "0: ClassNotFoundException",
		"iaecounter"			: str(app_data.get('iaecounter')),
		"installDate"			: app_data.get('installDate'),
		"installer_package"		: "com.android.vending",
		"isFirstCall"			: isFirstCall,
		"isGaidWithGps"			: "true",
		"lang"					: util.get_language_name(device_data.get('locale').get('language')),
		"lang_code"				: device_data.get('locale').get('language'),
		"model"					: device_data.get('model'),
		"network"				: "WIFI",
		"operator"				: device_data.get('carrier'),
		"platformextension"		: "android_cordova",
		"product"				: device_data.get('product'),
		"referrer"				: "utm_source=google-play&utm_medium=organic",
		"sdk"					: device_data.get('sdk'),	
		"timepassedsincelastlaunch": str(timeSinceLastCall),
		# "tokenRefreshConfigured": False,
		"uid"					: app_data.get('uid'),
		"registeredUninstall" 	: False,
		"rfr" : {
						"clk" : int(app_data.get("times").get("click_time")),
						"code" : "0",
						"install" : int(app_data.get("times").get("download_begin_time")),
						"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",
				},
		"ivc"					: False,
		"open_referrer"			: "android-app://com.acer.android.home"
		}			
	if app_data.get('referrer'):
		data['referrer'] = urllib.unquote(app_data.get('referrer'))
	
	if app_data.get('counter')>2:
		del data['rfr']
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+\
						data.get('uid')[0:7]+\
						data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+\
						data.get('af_timestamp')+\
						data.get('uid')+\
						data.get('installDate')+\
						data.get('counter')+\
						data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data['cksm_v1']=cksm_v1(data.get('installDate'), data.get('af_timestamp'))

	if not isOpen:
		# data['af_sdks'] 	 = "0000000000"
		data['batteryLevel'] = get_batteryLevel()

	if app_data.get('gcm_token'):
		data["af_gcm_token"]= app_data.get('gcm_token')


	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],sdk=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = getKeyHalf(device_data.get('sdk'),data.get('lang'),data["af_timestamp"])	
	data["kef"+kefKey] = kefVal	

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)

	url 	= 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	method 	= 'get'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent'	  : get_ua(device_data),
		}
	params 	= {
		'devkey'		  : campaign_data.get('appsflyer').get('key'),
		'device_id' 	  : app_data.get('uid')
		}
	data 	= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


def appsflyer_stats(campaign_data, app_data, device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	inc_(app_data,'launchCounter')

	method 	= 'post'
	url 	= 'https://stats.appsflyer.com/stats' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}
	params = None
	data   = {
		"advertiserId"			: device_data.get('adid'),
		"app_id"				: campaign_data.get('package_name'),
		"channel"				: 'null',
		"deviceFingerPrintId"	: app_data.get('deviceFingerPrintId'),
		"devkey"				: campaign_data.get('appsflyer').get('key'),
		"gcd_conversion_data_timing": "0",
		"launch_counter"		: str(app_data.get('launchCounter')),
		"originalAppsflyerId"	: app_data.get('uid'),
		"platform"				: "Android",
		"statType"				: "user_closed_app",
		"time_in_app"			: str(int(random.randint(20,60))),
		"uid"					: app_data.get('uid'),
		}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_installDate(app_data,device_data)
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')

	method 	= 'post'
	url 	= 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}
	params  = {
		'app_id'		  : campaign_data.get('package_name'),
		'buildnumber'	  : campaign_data.get('appsflyer').get('buildnumber'),
		}	
	data 	= {
		"advertiserId"			: device_data.get('adid'),
		"advertiserIdEnabled"	: "true",
		"af_events_api"			: "1",
		"af_preinstalled"		: "false",
		"af_timestamp"			: timestamp(),
		# "af_gcm_token": app_data.get('gcm_token'),
		# "android_id"			: device_data.get('android_id'),
		# "appUserId"				: app_data.get('user').get('username'),
		"app_version_code"		: campaign_data.get('app_version_code'),
		"app_version_name"		: campaign_data.get('app_version_name'),
		"appsflyerKey"			: campaign_data.get('appsflyer').get('key'),
		"brand"					: device_data.get('brand'),
		"carrier"				: device_data.get('carrier'),
		"counter"				: str(app_data.get('counter')),
		"country"				: device_data.get('locale').get('country'),
		"date1"					: app_data.get('installDate'),
		"date2"					: app_data.get('installDate'),
		"device"				: device_data.get('device'),
		"deviceData"			: {
									"arch": "",
									"build_display_id": device_data.get('build'),
									"cpu_abi": device_data.get('cpu_abi'),
									"cpu_abi2": device_data.get('cpu_abi'),
									"dim"					: {
																"d_dpi": device_data.get('dpi'),
																"size": app_data.get('dim_size'),
																"x_px": device_data.get('resolution').split('x')[1],
																"xdp": "221.225",
																"y_px": device_data.get('resolution').split('x')[0],
																"ydp": "221.672"}
									},
		# "deviceFingerPrintId"	: app_data.get('deviceFingerPrintId'),
		"deviceRm"				: device_data.get('build'),
		"deviceType"			: "user",
		"firstLaunchDate"		: app_data.get('firstLaunchDate'),
		# "gaidError"				: "0: ClassNotFoundException",
		"iaecounter"			: str(app_data.get('iaecounter')),
		"installDate"			: app_data.get('installDate'),
		"installer_package"		: "com.android.vending",
		"isFirstCall"			: "false",
		"isGaidWithGps"			: "true",
		"lang"					: util.get_language_name(device_data.get('locale').get('language')),
		"lang_code"				: device_data.get('locale').get('language'),
		"model"					: device_data.get('model'),
		"network"				: "WIFI",
		"operator"				: device_data.get('carrier'),
		"platformextension"		: "android_cordova",
		"product"				: device_data.get('product'),
		"referrer"				: "utm_source=google-play&utm_medium=organic",
		"sdk"					: device_data.get('sdk'),	
		# "timepassedsincelastlaunch": str(timeSinceLastCall),
		# "tokenRefreshConfigured": False,
		"uid"					: app_data.get('uid'),
		"registeredUninstall" 	: False,
		"ivc"					: False,

	}			
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+\
						data.get('uid')[0:7]+\
						data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+\
						data.get('af_timestamp')+\
						data.get('uid')+\
						data.get('installDate')+\
						data.get('counter')+\
						data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data['cksm_v1']=cksm_v1(data.get('installDate'), data.get('af_timestamp'))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('gcm_token'):
		data["af_gcm_token"]= app_data.get('gcm_token')

	if app_data.get('user'):
		data['appUserId'] = app_data.get('user').get('username')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and \
			app_data.get('prev_event_value') and \
			app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
	update_eventsRecords(app_data,eventName,eventValue)
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],sdk=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = getKeyHalf(device_data.get('sdk'),data.get('lang'),data["af_timestamp"])	
	data["kef"+kefKey] = kefVal
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

#######################################################
# 				self calls 
#######################################################
def self_countrie_ip_me(campaign_data, app_data, device_data):

	method 	= 'head'#'get'
	url 	= 'https://uapi-front.etoro.com/api/v1/countries/ip/me' 
	headers = {
		'User-Agent': 'Mozilla/5.0 (Linux; Android 5.1.1; '+device_data.get('model')+' Build/'+device_data.get('build')+'; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/64.0.3282.137 Mobile Safari/537.36',
		'Accept-Encoding': 'gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')

		}
	params = None
	data   = None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def self_rankings_metadata(campaign_data, app_data, device_data):

	method 	= 'get'
	url 	= 'https://www.etoro.com/sapi/rankings/metadata' 
	headers = {
		'User-Agent': 'Mozilla/5.0 (Linux; Android 5.1.1; '+device_data.get('model')+' Build/'+device_data.get('build')+'; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/64.0.3282.137 Mobile Safari/537.36',
		'Accept-Encoding': 'gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
		'Accept': 'application/json, text/plain, */*',
		'AccountType': 'Real',
		'ApplicationIdentifier': 'ReToroAndroid',
		'ApplicationVersion': campaign_data.get('app_version_name'),
		}

	if not app_data.get('client_request_id'):
		app_data['client_request_id']=str(uuid.uuid4())

	params = {
		'client_request_id': app_data.get('client_request_id')
	}
	data   = None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def self_instruments(campaign_data, app_data, device_data,method='get'):

	method 	= method
	url 	= 'http://www.etoro.com/sapi/trade-real/instruments/'
	headers = {
		'User-Agent': 'Mozilla/5.0 (Linux; Android 5.1.1; '+device_data.get('model')+' Build/'+device_data.get('build')+'; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/64.0.3282.137 Mobile Safari/537.36',
		'Accept-Encoding': 'gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
		'Accept': 'application/json, text/plain, */*',
		'AccountType': 'Real',
		'ApplicationIdentifier': 'ReToroAndroid',
		'ApplicationVersion': campaign_data.get('app_version_name'),
		}

	if not app_data.get('client_request_id'):
		app_data['client_request_id']=str(uuid.uuid4())

	params = {}
	data   = {}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def self_register_user(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	register_user(app_data,country='united states')
	if not app_data.get('TMIS2'):
		app_data['TMIS2']=util.get_random_string('hex',258)
	method 	= 'post'
	url 	= 'https://www.etoro.com/api/registration/v2/users' 
	headers = {
		'User-Agent': 'Mozilla/5.0 (Linux; Android 5.1.1; '+device_data.get('model')+' Build/'+device_data.get('build')+'; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/64.0.3282.137 Mobile Safari/537.36',
		'Accept-Encoding': 'gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept': 'application/json, text/plain, */*',
		'AccountType': 'Real',
		'ApplicationIdentifier': 'ReToroAndroid',
		'ApplicationVersion': campaign_data.get('app_version_name'),
		'Origin': 'file://',
		'Content-Type': 'application/json;charset=UTF-8',
		'X-STS-ClientTime': datetime.datetime.utcfromtimestamp(int(time.time())).strftime("%Y-%m-%dT%H%M%S"),
		'Cookie': 'TMIS2='+app_data.get('TMIS2')+';'
		}
	if app_data.get('incap_sess_2'):
		headers['Cookie'] += ' incap_ses'+app_data.get('incap_sess_2')+';'
	if app_data.get('nlbi_'):
		headers['Cookie'] += ' nlbi_'+app_data.get('nlbi_')+';'
	if app_data.get('visid_'):
		headers['Cookie'] += ' visid_'+app_data.get('visid_')+';'
	if app_data.get('incap_sess'):
		headers['Cookie'] += ' incap_ses'+app_data.get('incap_sess')+';'
	if app_data.get('TS01'):
		headers['Cookie'] += ' TS01'+app_data.get('TS01')+';'

	if not app_data.get('client_request_id'):
		app_data['client_request_id']=str(uuid.uuid4())

	params = {
		'client_request_id': app_data.get('client_request_id'),
	}
	data   = {
				"Username":app_data.get('user').get('username'),
				"Email":app_data.get('user').get('email'),
				"Password":app_data.get('user').get('password'),
				"Locale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
				"Terms":{"RegulationUrl":"https://uapi-front.etoro.com/api/v1/tnc/regulations/1.pdf",
				"PrivacyPolicyUrl":"http://www.etoro.com/about-us/privacy.aspx",
				"RiskDisclosureUrl":"http://www.etoro.com/general-risk-disclosure/",
				"FinancialServicesGuideUrl":'null',
				"EndUserLicenseAgreementUrl":'null',
				"CookiePolicyUrl":"https://www.etoro.com/customer-service/cookies/",
				"SignedSaveDataAgreement":'false'},
				"ContactInfo":{"FirstName":"",
								"LastName":"",
								"PhoneCountryPrefix":"91",
								"Phone":app_data.get('user').get('phone'),},
				"TrackingData":{"FormId":"1",
								"FunnelFromId":'43',
								"FunnelId":'43',
								"AffiliateId":'56663',
								"AppsFlyerId":app_data.get('uid'),
				"OriginRefererUrl":"file:///android_asset/www/index.html"}}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def self_Login_user(campaign_data, app_data, device_data):
	if not app_data.get('TMIS2'):
		app_data['TMIS2']=util.get_random_string('hex',258)
	method 	= 'get'
	url 	= 'https://www.etoro.com/api/logininfo/v1.1/logindata' 
	headers = {
		'User-Agent': 'Mozilla/5.0 (Linux; Android 5.1.1; '+device_data.get('model')+' Build/'+device_data.get('build')+'; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/64.0.3282.137 Mobile Safari/537.36',
		'Accept-Encoding': 'gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept': 'application/json, text/plain, */*',
		'AccountType': 'Real',
		'ApplicationIdentifier': 'ReToroAndroid',
		'ApplicationVersion': campaign_data.get('app_version_name'),
		'Cookie': 'TMIS2='+app_data.get('TMIS2')+'; OriginalProviderID=0;',
		'Authorization': app_data.get('accessToken'),
		'X-CSRF-TOKEN': app_data.get('antiCsrfToken'), 
		}

	if app_data.get('incap_sess_2'):
		headers['Cookie'] += ' incap_ses'+app_data.get('incap_sess_2')+';'
	if app_data.get('nlbi_'):
		headers['Cookie'] += ' nlbi_'+app_data.get('nlbi_')+';'
	if app_data.get('visid_'):
		headers['Cookie'] += ' visid_'+app_data.get('visid_')+';'
	if app_data.get('incap_sess'):
		headers['Cookie'] += ' incap_ses'+app_data.get('incap_sess')+';'
	if app_data.get('TS01'):
		headers['Cookie'] += ' TS01'+app_data.get('TS01')+';'
	if app_data.get('OrigCID'):
		headers['Cookie'] += ' OrigCID='+app_data.get('OrigCID')+';'
	if app_data.get('GCID'):
		headers['Cookie'] += ' GCID='+app_data.get('GCID')+';'
	if app_data.get('RegistrationPixels'):
		headers['Cookie'] += ' RegistrationPixels='+app_data.get('RegistrationPixels')+';'

	if not app_data.get('client_request_id'):
		app_data['client_request_id']=str(uuid.uuid4())

	params = {
		'client_request_id': app_data.get('client_request_id'),
		'conditionIncludeDisplayableInstruments':'false',
		'conditionIncludeMarkets':'false',
		'conditionIncludeMetadata':'false',
		'conditionIncludeMirrorValidation':	'false',
	}
	data   = None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}



def android_clients_google( campaign_data, device_data, app_data ):
	url= "https://android.clients.google.com/c2dm/register3"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Authorization": "AidLogin 4222865371253775297:8918866590732316351",
        "User-Agent":  'Android-GCM/1.5 ('+device_data.get('product')+' '+device_data.get('build')+')',
        "app": campaign_data.get('package_name'),     
        "content-type": "application/x-www-form-urlencoded",
        "gcm_ver": "19275016"}

	params= None

	data= {       "X-app_ver": campaign_data.get('app_version_code'),
        "X-app_ver_name": campaign_data.get('app_version_name'),
        "X-appid": util.get_random_string('char_all',11),
        "X-cliv": 'fiid-'+util.get_random_string('decimal',8),
        "X-gmp_app_id": "1:390302719905:"+'android:'+util.get_random_string('hex',16),
        "X-gmsv": "19275016",
        "X-osv": device_data.get('sdk'),
        "X-scope": "*",
        "X-subtype": "390302719905",
        "app":campaign_data.get('package_name'),  
        "app_ver":campaign_data.get('app_version_code'),
        "cert": util.get_random_string('hex',40),
        "device": "4222865371253775297",
        "gcm_ver": "19275016",
        "info": "Iw69zyPSGmEUUEfGgBpd_71F9_vkwRY",
        "plat": "0",
        "sender": "390302719905",
        "target_ver": "27"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}
	

def appsflyer_register(campaign_data, app_data, device_data):

	def_installDate(app_data,device_data)
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)

	# def_google_token(app_data)
	# app_data['afgcmtoken']=app_data.get('google_token')+':APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

	url = 'http://register.appsflyer.com/api/v4/androidevent'
	method = 'post'
	headers = {
		'User-Agent':app_data.get('ua'),
		'Content-Type':'application/json',
		'Accept-Encoding':'gzip',
		}
	params = {
		'buildnumber': campaign_data.get('appsflyer').get('buildnumber'),
		'app_id': campaign_data.get('package_name')
	}
	data = {
		'advertiserId':device_data.get('adid'),
		# "platform": "Android",
		# "gcd_conversion_data_timing": "0",
		"devkey": campaign_data.get('appsflyer').get('key'),
		# "statType": "user_closed_app",
		# "time_in_app":str(app_data.get('timepassedsincelastlaunch')),
		# "app_id": campaign_data.get('package_name'),
		"uid"					: app_data.get('uid'),
		# "originalAppsflyerId" : app_data.get('appsflyer_uid'),
		# "channel": "(null)",
		"launch_counter": str(app_data.get('counter')),
		"af_gcm_token": app_data.get('gcm_token'),
		"installDate" : app_data.get('installDate'),
		"sdk" : device_data.get('sdk'),
		"app_name": campaign_data.get('app_name'),
		'app_version_name' : campaign_data.get('app_version_name'),
		'app_version_code' : campaign_data.get('app_version_code'),
		# 'appUserId': '',
		'model': device_data.get('model'),
		'network': 'WIFI',
		'operator':'',
		'brand': device_data.get('brand'),
		'carrier':'',
		}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}



#######################################################
# Utility methods : DEFAULT 
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st   = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		if len(username)>15:
			username=username[:15]
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username.replace('.','')
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['password'] 	= last_name+str(random.choice(['@','1','@','5','@','7','7']))+app_data['user']['dob'].replace('-','')
		app_data['user']['phone'] 		= str(random.choice(['9','9','9','8','6','7','8']))+util.get_random_string('decimal',9)



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = 'ffffffff-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel():
	return str(random.randint(10,100))+".0"

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	
def def_installDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('installDate'):
		date = int(app_data.get('times').get('install_complete_time'))
		app_data['installDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+'+0000'

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+'+0000'

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))


###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()

def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100
		
	sb = insert_char(sb,23,str(n3))
	return sb

def generateValue1get(ts, firstLaunch, sdk_version):
    gethash = sha256(ts+firstLaunch+sdk_version)
    return gethash[:16]

def generateValue(b,x,s,p,ts,fl,sdk):
    part1=generateValue1get(ts,fl,sdk)
    print part1
    str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, 'utf-8')
    for i in range(len(str_)):
        str_[i] = int((str_[i] ^ ((i % 2) + 42)))

    stringBuilder = ""
    for toHexString in str_:
        toHexString2 = str(hex(int(toHexString))).strip("0x")
        if 1 == len(toHexString2):
            toHexString2 = "0"+str(toHexString2)
        stringBuilder+=toHexString2
    part2 = stringBuilder.__str__()
    return part1+part2

def getKeyHalf(sdk, lang, ts):
    ts = ts[::-1]
    appends = [sdk,lang,ts]
    lengths = [len(sdk),len(lang),len(ts)]
    l = sorted(lengths)
    least = l[0]
    out = ""
    for x in range(least):
        numb = None
        for y in range(len(appends)):
            charAt = ord(appends[y][x])
            if numb:
                charAt = int((charAt ^ int(numb)))

            numb = charAt
            
        out+=str(hex(numb)).strip("0x")

    if len(out)>4:
    	out = out[:4]
    elif len(out)<4:
    	while len(out)<4:
    		out+="1"
    
    return out