from sdk import util,purchase,installtimenew
from sdk import getsleep
import json,time,hashlib,urllib,random,base64
import clicker
import Config

campaign_data = {
				'package_name' : 'com.nhnent.SKQUEST',
				'sdk' : 'Android',
				'app_version_name' : '4.17.1.KG',#'4.17.0.KG',#4.16.3.KG, '4.16.2.KG',#'4.15.4.KG',#'4.14.0.KG',#'4.13.1.KG',#'4.12.1.KG',#'4.11.1.KG',#'4.5.3.KG', 4.7.0.KG, 4.8.0.KG, 4.9.1.KG
				'app_name' : 'Crusaders',
				'no_referrer': True,
				'supported_countries': 'WW',
				'ctr':6,
				'device_targeting':True,
				'app_size' : 80.0,#76.0,#79.0,
				'supported_os':'4.4',
				'tracker' : 'apsalar',
				'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
				'apsalar':{
					'version':'Singular/v7.4.2.PROD',#'Singular/v7.3.2.PROD',
					'key':'nhnentertainment_5e020fb1',
					'secret':'dc1f8895c3731a9680bb5f7329dfd544'
				},

				'purchase':{
						'1':{
							'revenue':'2.990000009536743'
						},
						'2':{
							'revenue':'4.990000004521743'
						},
						'3':{
							'revenue':'9.990000008512375'
						},
				},
				'retention':{
							1:50,
							2:48,
							3:46,
							4:44,
							5:40,
							6:38,
							7:35,
							8:32,
							9:31,
							10:30,
							11:29,
							12:28,
							13:27,
							14:26,
							15:25,
							16:24,
							17:23,
							18:22,
							19:21,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5
				}
}


def install(app_data, device_data):
	print 'please wait..........'
	# time.sleep(random.randint(10,15))
	installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android")
	# user_agent(campaign_data,app_data,device_data)

	
	global start_s
	start_s = str(int(app_data.get('times').get('install_complete_time')*1000))	


	app_data['level_clear'] = 5

	print '----------------------------- APSALAR Resolve ---------------------------'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**apsalar_Resolve)
	
	
	print '----------------------------- APSALAR START -----------------------------'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data, Is=True)
	util.execute_request(**apsalar_start)


	install_referrer(campaign_data,app_data,device_data)

	app_install(campaign_data,app_data,device_data)

	app_open(campaign_data,app_data,device_data)

	LicensingStatus(campaign_data,app_data,device_data)

	if random.randint(1,100)<=95:
		time.sleep(random.randint(20,25))
		nickname(campaign_data,app_data,device_data)
		app_data['nickname_flag'] = True

		call_pattern(app_data,device_data,type='install')

	return {'status':True}
	
	
def open(app_data, device_data,day):

	# user_agent(campaign_data,app_data,device_data)
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android")

	global start_s
	start_s = str(int(time.time()*1000))
	
	if not app_data.get('level_clear'):
		app_data['level_clear'] = 5

	if not app_data.get('nickname_flag'):
		app_data['nickname_flag'] = False

	print '----------------------------- APSALAR Resolve ---------------------------'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_Resolve)
	
	
	print '----------------------------- APSALAR START -----------------------------'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_start)

	app_open(campaign_data,app_data,device_data)

	LicensingStatus(campaign_data,app_data,device_data)

	if app_data.get('nickname_flag')==False:
		nickname(campaign_data,app_data,device_data)
		app_data['nickname_flag'] = True

	call_pattern(app_data,device_data,type='open',day=day)

	time.sleep(random.randint(20,25))
	purchasee(campaign_data, app_data, device_data,day)
	
	return {'status':True}



def install_referrer(campaign_data,app_data,device_data):
	if not app_data.get('referrer'):
		app_data['referrer'] = 'utm_source=(not set)&utm_medium=(not set)'

	eve_val=json.dumps({"referrer":urllib.quote(app_data.get('referrer')) ,"referrer_source":"service","clickTimestampSeconds":str(int(time.time())),"installBeginTimestampSeconds":str(int(time.time())),"current_device_time":str(int(time.time()*1000))})

	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__InstallReferrer',value=eve_val)
	util.execute_request(**apsalar_event)


def app_install(campaign_data,app_data,device_data):
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent2(campaign_data, app_data, device_data,'Install',generateLag=generateLag())
	util.execute_request(**apsalar_event)

def app_open(campaign_data,app_data,device_data):
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent2(campaign_data, app_data, device_data,'Open',generateLag=generateLag())
	util.execute_request(**apsalar_event)


def LicensingStatus(campaign_data,app_data,device_data):
	eve_val=json.dumps({"responseCode":"0","signedData":"0|655040020|com.nhnent.SKQUEST|522|ANlOHQPr5+rtG5J7+YwgOk+DWPqycpjIPQ==|1554440772619:VT=9223372036854775807","signature":"aCX6Ys0ragq5RUKUh\/FtzNrove36a474jBdS\/6XiCPocLqHk+TJhk3kZJqVy7CwM+ReWMMFfZOCdv7XokOtOgTibzj9TCsEnW3KMoVU66AqFzhNKSkPXILTMFv8jbav7tToQF9K4aR7cCAbo7LztwPMJIkupZIXAQHKw+n9XZ4KcdqltIslmkgBfq\/w1rotNRaQJZpVA\/+k+Fo7ZlNnQSuDaX4wyfkV4eIBA7vWTZcjyos97GhWsaVm3Mb+S\/Lj00u01zlDUIzsiZyqsgFpZCSfnRXc9F8WYH+NCqsh8sVTj5+I3W+YDauDTCZMQdpkflGG0mK8eKectlPJ8lgTsJQ=="})

	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__LicensingStatus',value=eve_val,generateLag=generateLag())
	util.execute_request(**apsalar_event)


def nickname(campaign_data,app_data,device_data):
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Nickname',generateLag=generateLag())
	util.execute_request(**apsalar_event)

def level3(campaign_data,app_data,device_data):
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'level_3',generateLag=generateLag())
	util.execute_request(**apsalar_event)

def level(campaign_data,app_data,device_data):
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'level_'+str(app_data.get('level_clear')),generateLag=generateLag())
	util.execute_request(**apsalar_event)



def call_pattern(app_data,device_data,type=False,day=False):
	if not app_data.get('level3_flag'):
		app_data['level3_flag'] = False

	if type=='install':
		top = random.choice([1,2,2,3,3,3,3,4,4,4,4,4,5,6])

	if type == "open":
		if random.randint(1,100)<=65:
			top = random.randint(1,4)
		else:
			top = random.randint(0,4)	

	if random.randint(1,100)<=90:
		for i in range(top):
			time.sleep(random.randint(10,25))
			if app_data.get('level3_flag') == False:
				level3(campaign_data,app_data,device_data)
				app_data['level3_flag'] = True

			if app_data.get('level3_flag') ==True:
				level(campaign_data,app_data,device_data)

			if app_data.get('level_clear')==10:
				break

			app_data['level_clear'] += 1
			print '*****************************************************'
			print app_data.get('level_clear')   

def purchasee(campaign_data, app_data, device_data,day):
	if purchase.isPurchase(app_data,day):
		flag = 1
		randompurchase = random.randint(1,100)
		if randompurchase <= 80:
			num = "1"
		elif randompurchase > 80 and randompurchase <=95:
			num = "2"
		elif randompurchase > 95 and randompurchase <=100:
			num = "3"
		else:
			flag = 0
		if flag == 1:
			print '----------------------------- APSALAR Purchase EVENT -----------------------------'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__iap__',generateLag=generateLag(),value=json.dumps({"pcc":"USD","r":campaign_data.get('purchase').get(num).get('revenue')}))
			util.execute_request(**apsalar_event)
			
		else:
			print "purchase failed"

#########################################
#										#
# 			AppSalar funcation 			#
#										#
#########################################

# def generate_gcm_id(app_data):
# 	if not app_data.get('device_gcm_id'):
# 		app_data['device_gcm_id'] = 'APA' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(137))
# 	return app_data.get('device_gcm_id')

def apsalarResolve(campaign_data, app_data, device_data):
	url = 'http://e-ssl.apsalar.com/api/v1/resolve'
	method = 'get'
	headers = {
		'User-Agent': 'Singular/SDK-v7.4.2.PROD',#'Singular/SDK-v7.3.2.PROD',
		'Accept-Encoding': 'gzip'
	}
	
	params ='a='+campaign_data.get('apsalar').get('key')+'&c=wifi&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(generateLag())+'&p='+campaign_data.get('sdk')+'&pi=1&rt=json&u='+device_data.get('adid')+'&v='+device_data.get('os_version')

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarStart(campaign_data, app_data, device_data,Is= False):
	if(Is==True):
		var='&is=true&k=AIFA'
	else:
		var='&is=false&k=AIFA'

	url = 'http://e-ssl.apsalar.com/api/v1/start'
	method = 'get'
	headers = {
		'User-Agent': 'Singular/SDK-v7.4.2.PROD',#'Singular/SDK-v7.3.2.PROD', 
		'Accept-Encoding': 'gzip'
	}

	device_user_agent = get_ua(device_data)
	
	if not app_data.get('s'):
		app_data['s'] = str(int(time.time()*1000))

	params ='a='+campaign_data.get('apsalar').get('key')+'&ab='+device_data.get('cpu_abi')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&br='+device_data.get('brand')+'&c=wifi'+'&current_device_time='+str(int(time.time()*1000))+'&ddl_enabled=true&ddl_to=60&de='+device_data.get('device')+'&device_type=tablet&device_user_agent='+device_user_agent+'&dnt=0&i='+campaign_data.get('package_name')+'&install_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+var+'&lag='+str(generateLag())+'&lc='+device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')+'&ma='+device_data.get('manufacturer')+'&mo='+device_data.get('model')+'&n='+campaign_data.get('app_name')+'&p='+campaign_data.get('sdk')+'&pr='+device_data.get('product')+'&rt=json&s='+ str(start_s) +'&sdk='+campaign_data.get('apsalar').get('version')+'&src=com.android.vending&u='+device_data.get('adid')+'&update_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&v='+device_data.get('os_version')


	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarEvent(campaign_data, app_data, device_data,event_name,value='',generateLag=''):
	custom_user_id(app_data)
	url = 'http://e-ssl.apsalar.com/api/v1/event'

	method = 'get'
	headers = {
		'User-Agent':'Singular/SDK-v7.4.2.PROD',#'Singular/SDK-v7.3.2.PROD',
		'Accept-Encoding': 'gzip'
	}

	s=str(int(time.time()*1000))

	params='a='+campaign_data.get('apsalar').get('key')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&c=wifi&e='+value+'&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(generateLag)+'&n='+event_name+'&p='+campaign_data.get('sdk')+'&rt=json&s='+str(start_s)+'&sdk='+campaign_data.get('apsalar').get('version')+'&seq='+str(cal_seq(app_data))+'&t='+generate_t()+'&u='+device_data.get('adid')

	if value== '':
		params='a='+campaign_data.get('apsalar').get('key')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&c=wifi&custom_user_id='+app_data.get('custom_user_id')+'&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(generateLag)+'&n='+event_name+'&p='+campaign_data.get('sdk')+'&rt=json&s='+str(start_s)+'&sdk='+campaign_data.get('apsalar').get('version')+'&seq='+str(cal_seq(app_data))+'&t='+generate_t()+'&u='+device_data.get('adid')

	if event_name== '__iap__':
		params='a='+campaign_data.get('apsalar').get('key')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&c=wifi&custom_user_id='+app_data.get('custom_user_id')+'&e='+value+'&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(generateLag)+'&n='+event_name+'&p='+campaign_data.get('sdk')+'&rt=json&s='+str(start_s)+'&sdk='+campaign_data.get('apsalar').get('version')+'&seq='+str(cal_seq(app_data))+'&t='+generate_t()+'&u='+device_data.get('adid')

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarEvent2(campaign_data, app_data, device_data,event_name,generateLag):
	if event_name=='Install':
		temp='&t='+str(generate_t1())
	else:
		temp='&t='+str(generate_t())
	url = 'http://e-ssl.apsalar.com/api/v1/event'

	method = 'get'
	headers = {
		'User-Agent':'Singular/SDK-v7.4.2.PROD',#'Singular/SDK-v7.3.2.PROD',
		'Accept-Encoding': 'gzip'
	}

	s=str(int(time.time()*1000))


	params='a='+campaign_data.get('apsalar').get('key')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&c=wifi'+'&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(generateLag)+'&n='+event_name+'&p='+campaign_data.get('sdk')+'&rt=json&s='+s+'&sdk='+campaign_data.get('apsalar').get('version')+'&seq='+str(cal_seq(app_data))+temp+'&u='+device_data.get('adid')

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


#########################################
#										#
# 			Extra funcation 			#
#										#
#########################################

def cal_seq(app_data):
	if not app_data.get('seq'):
		app_data['seq'] = 1
	else:
		app_data['seq'] = app_data.get('seq')+1
	return app_data.get('seq')





def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def generateLag():
	return (str(random.randint(1,5))+'.'+str(random.randint(100,900)))


def generate_t():
	return "%.3f" %(random.uniform(-2,2))

def generate_t1():
	return "%.16f" %(random.uniform(-2,2))

def custom_user_id(app_data):
	if not app_data.get('custom_user_id'):
		app_data['custom_user_id'] = util.get_random_string('decimal',16)

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())

def get_ua(device_data):
	if int(device_data.get("sdk")) >19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

#########################################
#										#
# 			Util funcation 				#
#										#
#########################################

def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'android'):
    serial = device_data.get('serial')
    agent_id = Config.AGENTID
    random_number = random.randint(1,10)
    source_id = ""
    if random_number < 5:
        source_id = "728"
    elif random_number < 8:
        source_id = "517"
    elif random_number < 9:
        source_id = "604"
    else:
        source_id = "386"

    st =device_data.get("device_id", str(int(time.time()*1000)))

    link = campaign_data['link']
    link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
    return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))
