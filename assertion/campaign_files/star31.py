# -*- coding: utf8 -*-
import sys
from sdk import getsleep
reload(sys)
sys.setdefaultencoding('utf-8')
import uuid,time,random,json,urllib,datetime, base64, string
from sdk import util,purchase
from Crypto.Cipher import AES
import clicker,Config
from sdk import installtimenew


campaign_data = {
            'package_name':'com.igs.mjstar31',
            'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
            'ctr':6,
            'app_name' : u'麻將明星3缺1'.encode("utf-8"),#明星3缺1,#明星3缺1
            'app_version_code': '231',#'218',#'216',#'213',#'211',#'210',#209, '208',#'207',#'157',
            'app_version_name': '6.9.18',#'6.8.62',#'6.8.61',#'6.8.60',#'6.8.58',#'6.8.57',#6.8.56, '6.8.55',#'6.8.54',#'6.7.21',
            'no_referrer'        : True,
            'CREATE_DEVICE_MODE' : 3,
            'app_size'           : 104.0,#86.0,#88.0,#97.0,86.0
            'supported_countries': 'WW',
            'device_targeting':True,
            'supported_os':'4.4',
            'tracker':'appsflyer',
            'mat' :{
                        'version' : '3.2.2',
                        'sdk' : 'android',
                        'advertiser_id' : '8008',
                        'key' : '6cdbaf996e3381201017a5ef44138ff6',
                        'iv' : 'heF9BATUfWuISyO8',
                    },
            'appsflyer':{
                        'key'        : '7g6XSBhwVhHwhzbZpKmSGQ',
                        'dkh'        : '7g6XSBhw',
                        'buildnumber': '4.8.13',#'1.17',
                        'version'    : 'v4',#'v2.3',
                    },
            'purchase_variation':{
                            1:{
                                'revenue1':30,
                                
                            },
                            2:{
                                'revenue2':90,
                                
                            },
                            3:{
                                'revenue3':150,
                                
                            },
                            4:{
                                'revenue4':300,
                                
                            },
            },
            'retention':{
                            1:50,
                            2:48,
                            3:46,
                            4:44,
                            5:40,
                            6:38,
                            7:35,
                            8:32,
                            9:31,
                            10:30,
                            11:29,
                            12:28,
                            13:27,
                            14:26,
                            15:25,
                            16:24,
                            17:23,
                            18:22,
                            19:21,
                            20:20,
                            21:19,
                            22:18,
                            23:17,
                            24:16,
                            25:15,
                            26:14,
                            27:13,
                            28:12,
                            29:11,
                            30:10,
                            31:9,
                            32:8,
                            33:7,
                            34:6,
                            35:5,
                        }
        }


def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0
  
def make_sec(app_data,device_data): 
    timez = device_data.get('timezone')
    sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
    if int(timez) < 0:
        sec = (-1) * sec
    if not app_data.get('sec'):
        app_data['sec'] = sec;
        
    return app_data.get('sec')
    
def make_another_referrer(app_data,device_data):

    if not app_data.get('sec'):
        make_sec(app_data,device_data)
        
    app_data['referrer'] = 'mat_click_id='+util.get_random_string('hex',32)+'-'+datetime.datetime.utcfromtimestamp(int(time.time())+app_data.get('sec')).strftime("%Y%m%d")+'-'+util.get_random_string('decimal',4)
    
    return app_data.get('referrer')
    
def install(app_data, device_data):
    print "Please wait installing..."
    installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)
    def_appsflyerUID(app_data)
    def_eventsRecords(app_data)

    ################generating_realtime_differences##################
    def_firstLaunchDate(app_data,device_data)
    app_data['register']=False
    app_data['google_signin']=False

    app_data['gcm']=str(int(time.time()*1000))

    print '\nAppsflyer : TRACK____________________________________'
    request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true")
    app_data['api_hit_time'] = time.time()
    util.execute_request(**request)

    time.sleep(random.randint(2,3))     
    print '\n'+'Appsflyer : Register____________________________________'
    request = appsflyer_register(campaign_data,app_data,device_data)
    util.execute_request(**request)
        
    print '\n'+'Appsflyer : Register____________________________________'
    request = appsflyer_register(campaign_data,app_data,device_data, call=2)
    util.execute_request(**request)

    print '\nAppsflyer : API____________________________________'
    request  = appsflyer_api(campaign_data, app_data, device_data)
    response = util.execute_request(**request)
    catch(response,app_data,"appsflyer")

    print"MAT "
    request=mat_serve(campaign_data, app_data, device_data)
    response=util.execute_request(**request)
    catch_mat(response,app_data,'mat')
    set_OpenLogID(app_data)
    set_LastOpenLogID(app_data)


    print"MAT 2"
    mat1 = mat_serve(campaign_data, app_data, device_data,'install')
    util.execute_request(**mat1)

    print"MAT 2"
    mat1 = mat_serve(campaign_data, app_data, device_data,'install')
    util.execute_request(**mat1)

    appopen(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        registration(campaign_data, app_data, device_data)
        app_data['register']=True
        if random.randint(1,100)<=20:
            registration(campaign_data, app_data, device_data)
        lobby_layer_main(campaign_data, app_data, device_data)
        if random.randint(1,100)<=90:
            google_bu_bi(campaign_data, app_data, device_data)
            google_bu_bi(campaign_data, app_data, device_data)
        if random.randint(1,100)<=45:
            news_touch_ad_20190417_750x350_3_tjpg(campaign_data, app_data, device_data)
        if random.randint(1,100)<=95:
            shuffle=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38]
            random.shuffle(shuffle)
            for i in range(len(shuffle)):
                print shuffle
                if i==shuffle[0] and random.randint(1,100)<=20:
                    lobby_layer_vip(campaign_data, app_data, device_data)
                if i==shuffle[1] and random.randint(1,100)<=20:
                    lobby_lobby_layer_welfare(campaign_data, app_data, device_data)
                if i==shuffle[2] and random.randint(1,100)<=20:
                    lobby_layer_union_system(campaign_data, app_data, device_data)
                if i==shuffle[3] and random.randint(1,100)<=20:
                    lobby_layer_roulette(campaign_data, app_data, device_data)
                if i==shuffle[4] and random.randint(1,100)<=20:
                    lobby_layer_more(campaign_data, app_data, device_data)
                if i==shuffle[5] and random.randint(1,100)<=20:
                   lobby_layer_shopping_mall(campaign_data, app_data, device_data)
                if i==shuffle[6] and random.randint(1,100)<=20:
                    lobby_19(campaign_data, app_data, device_data)
                if i==shuffle[7] and random.randint(1,100)<=20:
                    lobby_layer_news(campaign_data, app_data, device_data)
                if i==shuffle[8] and random.randint(1,100)<=90:
                    lobby_layer_mission(campaign_data, app_data, device_data)
                    if random.randint(1,100)<=80:
                        open_mission_ad(campaign_data, app_data, device_data)
                        if random.randint(1,100)<=90 and app_data.get('google_signin')==False:
                            lobby_layer_signin(campaign_data, app_data, device_data)
                            google_sign_in(campaign_data, app_data, device_data)
                            app_data['google_signin']=True
                if i==shuffle[9] and random.randint(1,100)<=80:
                    lobby_lobby_layer_scratch_ticket(campaign_data, app_data, device_data)
                    play_time_game_scratch_ticket(campaign_data, app_data, device_data)
                if i==shuffle[10] and random.randint(1,100)<=80:
                    layer_big_2(campaign_data, app_data, device_data)                    
                if i==shuffle[11] and random.randint(1,100)<=75:
                    game_big_2(campaign_data, app_data, device_data)
                if i==shuffle[12] and random.randint(1,100)<=65:
                    happy_fish(campaign_data, app_data, device_data)
                if i==shuffle[13] and random.randint(1,100)<=65 and app_data.get('pur_reven')>=2.99:
                    lucky_21(campaign_data, app_data, device_data)
                if i==shuffle[14] and random.randint(1,100)<=65:
                    monster_island(campaign_data, app_data, device_data)
                if i==shuffle[15] and random.randint(1,100)<=65:
                    fan_tan(campaign_data, app_data, device_data)
                if i==shuffle[16] and random.randint(1,100)<=65:
                    poker_13(campaign_data, app_data, device_data)
                if i==shuffle[17] and random.randint(1,100)<=50:
                    sicbo(campaign_data, app_data, device_data)
                if i==shuffle[18] and random.randint(1,100)<=50:
                    niu_niu(campaign_data, app_data, device_data)
                if i==shuffle[19] and random.randint(1,100)<=50:
                    super_eight(campaign_data, app_data, device_data)
                if i==shuffle[20] and random.randint(1,100)<=50:
                    fpkfh(campaign_data, app_data, device_data)
                if i==shuffle[21] and random.randint(1,100)<=50:
                    fpk4k(campaign_data, app_data, device_data)
                if i==shuffle[22] and random.randint(1,100)<=40 and app_data.get('pur_reven')>=2.99:
                    poseidon_slot(campaign_data, app_data, device_data)
                if i==shuffle[23] and random.randint(1,100)<=40:
                    dragon_shoot(campaign_data, app_data, device_data)
                if i==shuffle[24] and random.randint(1,100)<=40:
                    fpk(campaign_data, app_data, device_data)
                if i==shuffle[25] and random.randint(1,100)<=40 and app_data.get('pur_reven')>=2.99:
                    game_slot(campaign_data, app_data, device_data)
                if i==shuffle[26] and random.randint(1,100)<=40 and app_data.get('pur_reven')>=2.99:
                    queen_slot(campaign_data, app_data, device_data)
                if i==shuffle[27] and random.randint(1,100)<=30:
                    simple_mj(campaign_data, app_data, device_data)
                if i==shuffle[28] and random.randint(1,100)<=20:
                    blood_mj(campaign_data, app_data, device_data)
                if i==shuffle[29] and random.randint(1,100)<=20:
                    play_game_scratch_ticket(campaign_data, app_data, device_data)
                    play_time_game_scratch_ticket(campaign_data, app_data, device_data)
                if i==shuffle[30] and random.randint(1,100)<=20:
                    play_game_union_theme(campaign_data, app_data, device_data)
                if i==shuffle[31] and random.randint(1,100)<=20:
                    play_game_friend_theme(campaign_data, app_data, device_data)
                if i==shuffle[32] and random.randint(1,100)<=8:
                    depositbtn(campaign_data, app_data, device_data)
                    lobby_layer_deposit(campaign_data, app_data, device_data)
                if i==shuffle[33] and random.randint(1,100)<=8:
                    lobby_layer_gold_point(campaign_data, app_data, device_data)
                if i==shuffle[34] and random.randint(1,100)<=8:
                    lobby_layer_treasure_hunt(campaign_data, app_data, device_data)
                if i==shuffle[35] and random.randint(1,100)<=8:
                    lobby_layer_diamond_treasure(campaign_data, app_data, device_data)
                if i==shuffle[36] and random.randint(1,100)<=8:
                    lobby_layer_grouping_prize(campaign_data, app_data, device_data)
                if i==shuffle[37] and random.randint(1,100)<=30 and app_data.get('pur_reven')>=2.99:
                    alice_slot(campaign_data, app_data, device_data)
                if i==shuffle[38] and random.randint(1,100)<=75:
                    star_mj(campaign_data, app_data, device_data)
                    



    return {'status':'true'}
    
def open(app_data,device_data,day):

    print "Please wait ...."
    if not app_data.get('times'):
        installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)
    def_appsflyerUID(app_data)
    def_eventsRecords(app_data)

    ################generating_realtime_differences##################
    def_firstLaunchDate(app_data,device_data)
    if not app_data.get('register'):
        app_data['register']=False

    if not app_data.get('google_signin'):
        app_data['google_signin']=False


    print '\nAppsflyer : TRACK____________________________________'
    request  = appsflyer_track(campaign_data, app_data, device_data)
    util.execute_request(**request)

    print"MAT "
    request=mat_serve(campaign_data, app_data, device_data)
    response=util.execute_request(**request)
    catch_mat(response,app_data,'mat')
    set_OpenLogID(app_data)
    set_LastOpenLogID(app_data)

    appopen(campaign_data, app_data, device_data)

    if app_data.get('register')==False:
        registration(campaign_data, app_data, device_data)
        app_data['register']=True
        if random.randint(1,100)<=20:
            registration(campaign_data, app_data, device_data)

    if app_data.get('register')==True:
        lobby_layer_main(campaign_data, app_data, device_data)        
        google_bu_bi(campaign_data, app_data, device_data)
        google_bu_bi(campaign_data, app_data, device_data)
        if random.randint(1,100)<=20:
            news_touch_ad_20190417_750x350_3_tjpg(campaign_data, app_data, device_data)

        if random.randint(1,100)<=95:
            shuffle=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39]
            random.shuffle(shuffle)
            for i in range(len(shuffle)):
                print shuffle
                if i==shuffle[0] and random.randint(1,100)<=20:
                    lobby_layer_vip(campaign_data, app_data, device_data)
                if i==shuffle[1] and random.randint(1,100)<=20:
                    lobby_lobby_layer_welfare(campaign_data, app_data, device_data)
                if i==shuffle[2] and random.randint(1,100)<=20:
                    lobby_layer_union_system(campaign_data, app_data, device_data)
                if i==shuffle[3] and random.randint(1,100)<=20:
                    lobby_layer_roulette(campaign_data, app_data, device_data)
                if i==shuffle[4] and random.randint(1,100)<=20:
                    lobby_layer_more(campaign_data, app_data, device_data)
                if i==shuffle[5] and random.randint(1,100)<=20:
                   lobby_layer_shopping_mall(campaign_data, app_data, device_data)
                if i==shuffle[6] and random.randint(1,100)<=20:
                    lobby_19(campaign_data, app_data, device_data)
                if i==shuffle[7] and random.randint(1,100)<=20:
                    lobby_layer_news(campaign_data, app_data, device_data)
                if i==shuffle[8] and random.randint(1,100)<=90:
                    lobby_layer_mission(campaign_data, app_data, device_data)
                    if random.randint(1,100)<=80:
                        open_mission_ad(campaign_data, app_data, device_data)
                        if random.randint(1,100)<=90 and app_data.get('google_signin')==False:
                            lobby_layer_signin(campaign_data, app_data, device_data)
                            google_sign_in(campaign_data, app_data, device_data)
                            app_data['google_signin']=True
                if i==shuffle[9] and random.randint(1,100)<=80:
                    lobby_lobby_layer_scratch_ticket(campaign_data, app_data, device_data)
                    play_time_game_scratch_ticket(campaign_data, app_data, device_data)
                if i==shuffle[10] and random.randint(1,100)<=80:
                    layer_big_2(campaign_data, app_data, device_data)                    
                if i==shuffle[11] and random.randint(1,100)<=75:
                    game_big_2(campaign_data, app_data, device_data)
                if i==shuffle[12] and random.randint(1,100)<=65:
                    happy_fish(campaign_data, app_data, device_data)
                if i==shuffle[13] and random.randint(1,100)<=65 and app_data.get('pur_reven')>=2.99:
                    lucky_21(campaign_data, app_data, device_data)
                if i==shuffle[14] and random.randint(1,100)<=65:
                    monster_island(campaign_data, app_data, device_data)
                if i==shuffle[15] and random.randint(1,100)<=65:
                    fan_tan(campaign_data, app_data, device_data)
                if i==shuffle[16] and random.randint(1,100)<=65:
                    poker_13(campaign_data, app_data, device_data)
                if i==shuffle[17] and random.randint(1,100)<=50:
                    sicbo(campaign_data, app_data, device_data)
                if i==shuffle[18] and random.randint(1,100)<=50:
                    niu_niu(campaign_data, app_data, device_data)
                if i==shuffle[19] and random.randint(1,100)<=50:
                    super_eight(campaign_data, app_data, device_data)
                if i==shuffle[20] and random.randint(1,100)<=50:
                    fpkfh(campaign_data, app_data, device_data)
                if i==shuffle[21] and random.randint(1,100)<=50:
                    fpk4k(campaign_data, app_data, device_data)
                if i==shuffle[22] and random.randint(1,100)<=40 and app_data.get('pur_reven')>=2.99:
                    poseidon_slot(campaign_data, app_data, device_data)
                if i==shuffle[23] and random.randint(1,100)<=40:
                    dragon_shoot(campaign_data, app_data, device_data)
                if i==shuffle[24] and random.randint(1,100)<=40:
                    fpk(campaign_data, app_data, device_data)
                if i==shuffle[25] and random.randint(1,100)<=40 and app_data.get('pur_reven')>=2.99:
                    game_slot(campaign_data, app_data, device_data)
                if i==shuffle[26] and random.randint(1,100)<=40 and app_data.get('pur_reven')>=2.99:
                    queen_slot(campaign_data, app_data, device_data)
                if i==shuffle[27] and random.randint(1,100)<=30:
                    simple_mj(campaign_data, app_data, device_data)
                if i==shuffle[28] and random.randint(1,100)<=20:
                    blood_mj(campaign_data, app_data, device_data)
                if i==shuffle[29] and random.randint(1,100)<=20:
                    play_game_scratch_ticket(campaign_data, app_data, device_data)
                    play_time_game_scratch_ticket(campaign_data, app_data, device_data)
                if i==shuffle[30] and random.randint(1,100)<=20:
                    play_game_union_theme(campaign_data, app_data, device_data)
                if i==shuffle[31] and random.randint(1,100)<=20:
                    play_game_friend_theme(campaign_data, app_data, device_data)
                if i==shuffle[32] and random.randint(1,100)<=8:
                    depositbtn(campaign_data, app_data, device_data)
                    lobby_layer_deposit(campaign_data, app_data, device_data)
                if i==shuffle[33] and random.randint(1,100)<=8:
                    lobby_layer_gold_point(campaign_data, app_data, device_data)
                if i==shuffle[34] and random.randint(1,100)<=8:
                    lobby_layer_treasure_hunt(campaign_data, app_data, device_data)
                if i==shuffle[35] and random.randint(1,100)<=8:
                    lobby_layer_diamond_treasure(campaign_data, app_data, device_data)
                if i==shuffle[36] and random.randint(1,100)<=8:
                    lobby_layer_grouping_prize(campaign_data, app_data, device_data)
                if i==shuffle[37] and random.randint(1,100)<=30 and app_data.get('pur_reven')>=2.99:
                    alice_slot(campaign_data, app_data, device_data)
                if i==shuffle[38] and random.randint(1,100)<=75:
                    star_mj(campaign_data, app_data, device_data)
                if i==shuffle[39]:
                    if purchase.isPurchase(app_data,day=day,advertiser_demand=10):
                        print "---------------purchase-------------------------------"
                        var=random.choice([1,2,2,2,2,2,2,2,3,3,4])
                        if var==1:
                            revenue=campaign_data.get('purchase_variation').get(var).get('revenue1')
                        elif var==2:
                            revenue=campaign_data.get('purchase_variation').get(var).get('revenue2')
                        elif var==3:
                            revenue=campaign_data.get('purchase_variation').get(var).get('revenue3')
                        else:
                            revenue=campaign_data.get('purchase_variation').get(var).get('revenue4')

                        app_data['pur_reven']=revenue
                            
                        icoin(campaign_data, app_data, device_data,amount=revenue)
                        





    
    return {'status':'true'}

def layer_big_2(campaign_data, app_data, device_data):
    print "-----------game:layer_big_2-----------------------"
    lobby_layer_big_2(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)
        if random.randint(1,100)<=95:
            for i in range(0,random.randint(1,3)):
                google_bigtwo(campaign_data, app_data, device_data)
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def game_big_2(campaign_data, app_data, device_data):
    print "-----------game:game_big_2-----------------------"
    play_game_big_2(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)
        if random.randint(1,100)<=95:
            for i in range(0,random.randint(1,3)):
                google_bigtwo(campaign_data, app_data, device_data)
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_big_2(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)


def simple_mj(campaign_data, app_data, device_data):
    print "-----------game:simple_mj-----------------------"
    play_game_simple_mj(campaign_data, app_data, device_data)
    if random.randint(1,100)<=90:
        onjoined_game(campaign_data, app_data, device_data)
        play_time_game_simple_mj(campaign_data, app_data, device_data)
        if random.randint(1,100)<=50:
            onleft_game(campaign_data, app_data, device_data)
            onleftgame_simple_mj(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def blood_mj(campaign_data, app_data, device_data):
    print "-----------game:blood_mj-----------------------"
    play_game_blood_mj(campaign_data, app_data, device_data)
    if random.randint(1,100)<=90:
        onjoined_game(campaign_data, app_data, device_data)
        play_time_game_blood_mj(campaign_data, app_data, device_data)
        if random.randint(1,100)<=50:
            onleft_game(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def happy_fish(campaign_data, app_data, device_data):
    print "-----------game:happy_fish-----------------------"
    play_game_happy_fish(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)        
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_happy_fish(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def lucky_21(campaign_data, app_data, device_data):
    print "-----------game:lucky_21-----------------------"
    play_game_lucky_21(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)
        if random.randint(1,100)<=95:
            google_bu_bigoogle_blackjack(campaign_data, app_data, device_data)            
            for i in range(0,random.randint(1,3)):
                google_blackjack(campaign_data, app_data, device_data)
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_lucky_21(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def monster_island(campaign_data, app_data, device_data):
    print "-----------game:monster_island-----------------------"
    play_game_monster_island(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)        
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_monster_island(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def fan_tan(campaign_data, app_data, device_data):
    print "-----------game:fan_tan-----------------------"
    play_game_fan_tan(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)        
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_fan_tan(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def queen_slot(campaign_data, app_data, device_data):
    print "-----------game:queen_slot-----------------------"
    play_game_the_queen_slot(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data) 
        if random.randint(1,100)<=80:
            play_time_game_the_queen_slot(campaign_data, app_data, device_data)       
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)
            

def poker_13(campaign_data, app_data, device_data):
    print "-----------game:poker_13-----------------------"
    play_game_new_poker_13(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)  
        if random.randint(1,100)<=80:
            play_time_game_poker_13(campaign_data, app_data, device_data)      
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)
            

def sicbo(campaign_data, app_data, device_data):
    print "-----------game:sicbo-----------------------"
    play_game_new_sicbo(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)        
        if random.randint(1,100)<=80:
            play_time_game_new_sicbo(campaign_data, app_data, device_data)      
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)


def alice_slot(campaign_data, app_data, device_data):
    print "-----------game:alice_slot-----------------------"
    play_game_alice_slot(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)        
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_alice_slot(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def niu_niu(campaign_data, app_data, device_data):
    print "-----------game:niu_niu-----------------------"
    play_game_niu_niu(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data) 
        if random.randint(1,100)<=80:
            play_time_game_niu_niu(campaign_data, app_data, device_data)       
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def super_eight(campaign_data, app_data, device_data):
    print "-----------game:super_eight-----------------------"
    play_game_super_eight(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)        
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_super_eight(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def fpkfh(campaign_data, app_data, device_data):
    print "-----------game:fpkfh-----------------------"
    play_game_fpkfh(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)        
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_fpkfh(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def dragon_shoot(campaign_data, app_data, device_data):
    print "-----------game:dragon_shoot-----------------------"
    play_game_dragon_shoot(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)        
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_dragon_shoot(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def poseidon_slot(campaign_data, app_data, device_data):
    print "-----------game:poseidon_slot-----------------------"
    play_game_poseidon_slot(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)        
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_poseidon_slot(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)


def game_slot(campaign_data, app_data, device_data):
    print "-----------game:game_slot-----------------------"
    play_game_slot(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data)        
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_slot(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def fpk4k(campaign_data, app_data, device_data):
    print "-----------game:fpk4k-----------------------"
    play_game_fpk4k(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data) 
        if random.randint(1,100)<=80:
            play_time_game_fpk4k(campaign_data, app_data, device_data)       
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)


def fpk(campaign_data, app_data, device_data):
    print "-----------game:fpk-----------------------"
    play_game_new_fpk(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data) 
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)

def star_mj(campaign_data, app_data, device_data):
    print "-----------game:fpk-----------------------"
    play_game_star_mj(campaign_data, app_data, device_data)
    if random.randint(1,100)<=95:
        onjoined_game(campaign_data, app_data, device_data) 
        if random.randint(1,100)<=95:
            google_bu_bigoogle_mj_gamei(campaign_data, app_data, device_data)            
            for i in range(0,random.randint(3,5)):
                google_mj_gamei(campaign_data, app_data, device_data)
        if random.randint(1,100)<=90:
            onleft_game(campaign_data, app_data, device_data)
            play_time_game_star_mj(campaign_data, app_data, device_data)
            if random.randint(1,100)<=10:
                lobby_layer_main(campaign_data, app_data, device_data)




def appopen(campaign_data, app_data, device_data):
    time.sleep(random.randint(5,30))
    print 'Appsflyer : EVENT___________________________Open'
    eventName         = 'Open'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def registration(campaign_data, app_data, device_data):
    time.sleep(random.randint(30,60))
    print 'Appsflyer : EVENT___________________________registration'
    eventName         = 'registration'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)
    


def lobby_layer_main(campaign_data, app_data, device_data):
    time.sleep(random.randint(5,10))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_MAIN'
    eventName         = 'LOBBY_LAYER_MAIN'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def google_bu_bi(campaign_data, app_data, device_data):
    time.sleep(random.randint(0,1))
    print 'Appsflyer : EVENT___________________________Google_BU_BI'
    eventName         = 'Google_BU_BI'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def news_touch_ad_20190417_750x350_3_tjpg(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________News_Touch_AD_20190417_750X350_3_T.jpg'
    eventName         = 'News_Touch_AD_20190417_750X350_3_T.jpg'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)



def lobby_layer_vip(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_VIP'
    eventName         = 'LOBBY_LAYER_VIP'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_lobby_layer_welfare(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LOBBY_LAYER_WELFARE'
    eventName         = 'LOBBY_LOBBY_LAYER_WELFARE'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_union_system(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_UNION_SYSTEM'
    eventName         = 'LOBBY_LAYER_UNION_SYSTEM'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_mission(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_MISSION'
    eventName         = 'LOBBY_LAYER_MISSION'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_grouping_prize(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_GROUPING_PRIZE'
    eventName         = 'LOBBY_LAYER_GROUPING_PRIZE'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_roulette(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_ROULETTE'
    eventName         = 'LOBBY_LAYER_ROULETTE'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_more(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_MORE'
    eventName         = 'LOBBY_LAYER_MORE'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_lobby_layer_scratch_ticket(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    app_data['gamejoin']=time.time()
    print 'Appsflyer : EVENT___________________________LOBBY_LOBBY_LAYER_SCRATCH_TICKET'
    eventName         = 'LOBBY_LOBBY_LAYER_SCRATCH_TICKET'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_time_game_scratch_ticket(campaign_data, app_data, device_data):
    time.sleep(random.randint(120,180))
    scratch_t=round((time.time()-app_data.get('gamejoin'))/60,1)
    print scratch_t
    print type(scratch_t)
    # time.sleep(10)
    print 'Appsflyer : EVENT___________________________Play_Time_Game_Scratch_Ticket'
    eventName         = 'Play_Time_Game_Scratch_Ticket'
    eventValue        = '{"af_revenue":"'+str(scratch_t)+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_big_2(campaign_data, app_data, device_data):
    time.sleep(random.randint(10,15))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_BIG_2'
    eventName         = 'LOBBY_LAYER_BIG_2'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def onjoined_game(campaign_data, app_data, device_data):
    time.sleep(random.randint(8,10))
    app_data['gamejoin']=time.time()
    print 'Appsflyer : EVENT___________________________OnJoined_Game'
    eventName         = 'OnJoined_Game'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_star_mj(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________Play_GAME_STAR_MJ'
    eventName         = 'play_game_star_mj'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_big_2(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,3))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_BIG_2'
    eventName         = 'Play_GAME_BIG_2'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def google_bigtwo(campaign_data, app_data, device_data):
    time.sleep(random.randint(240,300))
    print 'Appsflyer : EVENT___________________________Google_Big Two'
    eventName         = 'Google_Big Two'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def onleft_game(campaign_data, app_data, device_data):
    time.sleep(random.randint(3,5))
    print 'Appsflyer : EVENT___________________________OnLeft_Game'
    eventName         = 'OnLeft_Game'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_time_game_big_2(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    big_time=str((time.time()-app_data.get('gamejoin'))/60)
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_BIG_2'
    eventName         = 'Play_Time_GAME_BIG_2'
    eventValue        = '{"af_revenue":"'+big_time+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_shopping_mall(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_SHOPPING_MALL'
    eventName         = 'LOBBY_LAYER_SHOPPING_MALL'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_happy_fish(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_HAPPY_FISH'
    eventName         = 'Play_GAME_HAPPY_FISH'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)
    

def play_game_lucky_21(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_LUCKY_21'
    eventName         = 'Play_GAME_LUCKY_21'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_monster_island(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_MONSTER_ISLAND'
    eventName         = 'Play_GAME_MONSTER_ISLAND'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_fan_tan(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_FAN_TAN'
    eventName         = 'Play_GAME_FAN_TAN'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_new_poker_13(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_NEW_POKER_13'
    eventName         = 'Play_GAME_NEW_POKER_13'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_new_sicbo(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_NEW_SICBO'
    eventName         = 'Play_GAME_NEW_SICBO'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_blood_mj(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_BLOOD_MJ'
    eventName         = 'Play_GAME_BLOOD_MJ'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_niu_niu(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_NIU_NIU'
    eventName         = 'Play_GAME_NIU_NIU'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_super_eight(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_SUPER_EIGHT'
    eventName         = 'Play_GAME_SUPER_EIGHT'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_fpkfh(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_FPKFH'
    eventName         = 'Play_GAME_FPKFH'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_fpk4k(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_FPK4K'
    eventName         = 'Play_GAME_FPK4K'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_poseidon_slot(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_POSEIDON_SLOT'
    eventName         = 'Play_GAME_POSEIDON_SLOT'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_dragon_shoot(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_DRAGON_SHOOT'
    eventName         = 'Play_GAME_DRAGON_SHOOT'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_new_fpk(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________Play_GAME_NEW_FPK'
    eventName         = 'Play_GAME_NEW_FPK'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_game_slot(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_SLOT'
    eventName         = 'Play_GAME_SLOT'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_19(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_19'
    eventName         = 'LOBBY_19'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_news(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_NEWS'
    eventName         = 'LOBBY_LAYER_NEWS'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def open_mission_ad(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________OPEN_MISSION_AD'
    eventName         = 'OPEN_MISSION_AD'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_signin(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_SIGNIN'
    eventName         = 'LOBBY_LAYER_SIGNIN'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def google_sign_in(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________Google_Sign_in'
    eventName         = 'Google_Sign_in'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_game_the_queen_slot(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_THE_QUEEN_SLOT'
    eventName         = 'Play_GAME_THE_QUEEN_SLOT'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_time_game_the_queen_slot(campaign_data, app_data, device_data):
    time.sleep(random.randint(120,180))
    slot_time=str((time.time()-app_data.get('gamejoin'))/60)
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_THE_QUEEN_SLOT'
    eventName         = 'Play_Time_GAME_THE_QUEEN_SLOT'
    eventValue        = '{"af_revenue":"'+slot_time+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_game_simple_mj(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_SIMPLE_MJ'
    eventName         = 'Play_GAME_SIMPLE_MJ'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_time_game_simple_mj(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    m_t=round((time.time()-app_data.get('gamejoin'))/60,1)
    print m_t
    print type(m_t)
    # time.sleep(10)
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_SIMPLE_MJ'
    eventName         = 'Play_Time_GAME_SIMPLE_MJ'
    eventValue        = '{"af_revenue":"'+str(m_t)+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def onleftgame_simple_mj(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________OnLeftGAME_SIMPLE_MJ'
    eventName         = 'OnLeftGAME_SIMPLE_MJ'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_blood_mj(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    blood_t=str((time.time()-app_data.get('gamejoin'))/60)
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_BLOOD_MJ'
    eventName         = 'Play_Time_GAME_BLOOD_MJ'
    eventValue        = '{"af_revenue":"'+blood_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_game_scratch_ticket(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    app_data['gamejoin']=time.time()
    print 'Appsflyer : EVENT___________________________Play_GAME_SCRATCH_TICKET'
    eventName         = 'Play_GAME_SCRATCH_TICKET'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_game_union_theme(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________Play_GAME_UNION_THEME'
    eventName         = 'Play_GAME_UNION_THEME'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def depositbtn(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________DepositBtn'
    eventName         = 'DepositBtn'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_deposit(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_DEPOSIT'
    eventName         = 'LOBBY_LAYER_DEPOSIT'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def lobby_layer_gold_point(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_GOLD_POINT'
    eventName         = 'LOBBY_LAYER_GOLD_POINT'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_treasure_hunt(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_TREASURE_HUNT'
    eventName         = 'LOBBY_LAYER_TREASURE_HUNT'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def lobby_layer_diamond_treasure(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________LOBBY_LAYER_DIAMOND_TREASURE'
    eventName         = 'LOBBY_LAYER_DIAMOND_TREASURE'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_game_friend_theme(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    print 'Appsflyer : EVENT___________________________Play_GAME_FRIEND_THEME'
    eventName         = 'Play_GAME_FRIEND_THEME'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_game_alice_slot(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    
    print 'Appsflyer : EVENT___________________________Play_GAME_ALICE_SLOT'
    eventName         = 'Play_GAME_ALICE_SLOT'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_time_game_alice_slot(campaign_data, app_data, device_data):
    time.sleep(random.randint(1,5))
    alt=str((time.time()-app_data.get('gamejoin'))/60)
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_ALICE_SLOT'
    eventName         = 'Play_Time_GAME_ALICE_SLOT'
    eventValue        = '{"af_revenue":"'+alt+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def google_bu_bigoogle_mj_gamei(campaign_data, app_data, device_data):
    time.sleep(random.randint(60,120))
    print 'Appsflyer : EVENT___________________________google_bu_bi + Google_MJ_Game(i)'
    eventName         = 'google_bu_bi + Google_MJ_Game(i)'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def google_mj_gamei(campaign_data, app_data, device_data):
    time.sleep(random.randint(60,120))
    print 'Appsflyer : EVENT___________________________Google_MJ_Game(i)'
    eventName         = 'Google_MJ_Game(i)'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_happy_fish(campaign_data, app_data, device_data):
    time.sleep(random.randint(120,180))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_HAPPY_FISH'
    happy_t=round((time.time()-app_data.get('gamejoin'))/60,2)
    print happy_t
    print type(happy_t)
    # time.sleep(10)
    eventName         = 'Play_Time_GAME_HAPPY_FISH'
    eventValue        = '{"af_revenue":"'+str(happy_t)+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def google_bu_bigoogle_blackjack(campaign_data, app_data, device_data):
    time.sleep(random.randint(60,120))
    print 'Appsflyer : EVENT___________________________google_bu_bi + Google_Black Jack'
    eventName         = 'google_bu_bi + Google_Black Jack'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def google_blackjack(campaign_data, app_data, device_data):
    time.sleep(random.randint(60,120))
    print 'Appsflyer : EVENT___________________________Google_Black Jack'
    eventName         = 'Google_Black Jack'
    eventValue        = '{"af_revenue":""}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_time_game_lucky_21(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_LUCKY_21'
    lucky21_t=str((time.time()-app_data.get('gamejoin'))/60)
    eventName         = 'Play_Time_GAME_LUCKY_21'
    eventValue        = '{"af_revenue":"'+lucky21_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_monster_island(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_MONSTER_ISLAND'
    monster_t=str((time.time()-app_data.get('gamejoin'))/60)
    eventName         = 'Play_Time_GAME_MONSTER_ISLAND'
    eventValue        = '{"af_revenue":"'+monster_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_fan_tan(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_FAN_TAN'
    fan_t=str((time.time()-app_data.get('gamejoin'))/60)
    eventName         = 'Play_Time_GAME_FAN_TAN'
    eventValue        = '{"af_revenue":"'+fan_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)



def icoin(campaign_data, app_data, device_data,amount=''):
    time.sleep(random.randint(30,50))
    print 'Appsflyer : EVENT___________________________Icoin'
    eventName         = 'Icoin'
    eventValue        = '{"af_revenue":"'+str(amount)+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)


def play_time_game_poker_13(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_POKER_13'
    poker13_t=str((time.time()-app_data.get('gamejoin'))/60)
    eventName         = 'Play_Time_GAME_POKER_13'
    eventValue        = '{"af_revenue":"'+poker13_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_new_sicbo(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_NEW_SICBO'
    sicbo_t=str((time.time()-app_data.get('gamejoin'))/60)
    eventName         = 'Play_Time_GAME_NEW_SICBO'
    eventValue        = '{"af_revenue":"'+sicbo_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)



def play_time_game_niu_niu(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_NIU_NIU'
    niu_t=str((time.time()-app_data.get('gamejoin'))/60)
    eventName         = 'Play_Time_GAME_NIU_NIU'
    eventValue        = '{"af_revenue":"'+niu_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_super_eight(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_SUPER_EIGHT'
    super8_t=str((time.time()-app_data.get('gamejoin'))/60)
    eventName         = 'Play_Time_GAME_SUPER_EIGHT'
    eventValue        = '{"af_revenue":"'+super8_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_fpkfh(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_FPKFH'
    fpkfh_t=str((time.time()-app_data.get('gamejoin'))/60)
    eventName         = 'Play_Time_GAME_FPKFH'
    eventValue        = '{"af_revenue":"'+fpkfh_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_fpk4k(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_FPK4K'
    fpk4k_t=round((time.time()-app_data.get('gamejoin'))/60,1)
    print fpk4k_t
    print type(fpk4k_t)
    # time.sleep(10)
    eventName         = 'Play_Time_GAME_FPK4K'
    eventValue        = '{"af_revenue":"'+str(fpk4k_t)+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_dragon_shoot(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_DRAGON_SHOOT'
    dragon_t=round((time.time()-app_data.get('gamejoin'))/60,2)
    print dragon_t
    print type(dragon_t)
    # time.sleep(10)
    eventName         = 'Play_Time_GAME_DRAGON_SHOOT'
    eventValue        = '{"af_revenue":"'+str(dragon_t)+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_poseidon_slot(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_POSEIDON_SLOT'
    poseidon_t=round((time.time()-app_data.get('gamejoin'))/60,2)
    print poseidon_t
    print type(poseidon_t)
    # time.sleep(10)
    eventName         = 'Play_Time_GAME_POSEIDON_SLOT'
    eventValue        = '{"af_revenue":"'+str(poseidon_t)+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_slot(campaign_data, app_data, device_data):
    time.sleep(random.randint(180,240))
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_SLOT'
    slot_t=str((time.time()-app_data.get('gamejoin'))/60)
    eventName         = 'Play_Time_GAME_SLOT'
    eventValue        = '{"af_revenue":"'+slot_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

def play_time_game_star_mj(campaign_data, app_data, device_data):
    print 'Appsflyer : EVENT___________________________Play_Time_GAME_STAR_MJ'
    star_mj_t=str((time.time()-app_data.get('gamejoin'))/60)
    eventName         = 'Play_Time_GAME_STAR_MJ'
    eventValue        = '{"af_revenue":"'+star_mj_t+'"}'
    request = appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
    util.execute_request(**request)

class AESCipher:
 def __init__(self, key):
        self.key = key

 def pad(self, raw):
        l = len(raw) % 16
        l = 16 - l
        for x in range(l):
            raw += ' '
        return raw

 def encrypt(self, iv, raw):
        raw = self.pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')

 def decrypt(self, enc , iv):
        enc = enc.decode('hex')#base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return cipher.decrypt(enc)      
    

        

def mat_serve(campaign_data, app_data, device_data,action="session",log=False,data=False):

    event = ""
    if "conversion" in action:
        event   = action.split(":")[1]
        action  = action.split(":")[0]


    url = 'http://%s.engine.mobileapptracking.com/serve' % campaign_data.get('mat').get('advertiser_id')
    headers ={
                'Content-Type': 'application/json',
                'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
                'Accept': 'application/json',
                'Accept-Encoding':'gzip',

                }
    method = 'post'

    params = {
        'action':action,
        'advertiser_id':campaign_data.get('mat').get('advertiser_id'),
        'package_name':campaign_data.get('package_name'),
        'sdk':campaign_data.get('mat').get('sdk'),
        'sdk_retry_attempt':'0',
        'transaction_id':str(uuid.uuid4()),
        'ver':campaign_data.get('mat').get('version'),
        'sdk_retry_attempt':'0',
    }
    if not app_data.get('insdate'):
        app_data['insdate'] = str(int(time.time())-100)

    if not app_data.get('mat_id'):
        app_data['mat_id'] = str(uuid.uuid4())
        
    if not app_data.get('referrer'):
        make_another_referrer(app_data,device_data)
        

    data_value = {
            'connection_type':'WIFI',
            'app_name':campaign_data.get('app_name'),
            'app_version':campaign_data.get('app_version_code'),
            # 'app_version_name':campaign_data.get('app_version_name'),
            'device_brand':device_data.get('manufacturer'),
            # 'device_cpu_type':device_data.get('cpu'),
            'device_model':device_data.get('model'),
            'insdate':app_data.get('insdate'),
            # 'installer':'com.android.vending',
            'language':util.get_language_name(device_data.get('locale').get('language')),
            # 'locale' :device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
            'mat_id':app_data.get('mat_id'),
            'os_version':device_data.get('os_version'),
            'referrer':app_data.get('referrer'),
            'screen_density':util.getdensity(device_data.get('dpi')),
            'screen_layout_size':device_data.get('resolution').split('x')[1]+'x'+device_data.get('resolution').split('x')[0],
            'sdk_version':campaign_data.get('mat').get('version'),
            # 'conversion_user_agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
            'currency_code':'USD',
            'revenue':'0.0',
            'google_aid':device_data.get('adid'),
            'google_ad_tracking_disabled':'0',
            'install_referrer':app_data.get('referrer') if app_data.get('referrer') else make_another_referrer(app_data,device_data),
            # 'system_date':str(int(time.time())),
            'device_id': device_data.get('imei'),
            'existing_user' : '1',
            'mac_address': device_data.get('mac'),
            'android_id'    : device_data.get('android_id'),
            'mobile_country_code':device_data.get('mcc'),
            'mobile_network_code':device_data.get('mnc'),
            "update_log_id": "",
            "user_email": "",
            "user_id": "",
            "user_name": "",
            "is_paying_user": "",
            "install_log_id": "",
            "device_carrier": device_data.get('carrier'),
            "country_code": device_data.get('locale').get('country'),
            }

    if(log):
        if app_data.get('open_log_id'):
            data_value['open_log_id'] = str(app_data.get('open_log_id'))
        if app_data.get('last_open_log_id'):
            data_value['last_open_log_id'] = str(app_data.get('last_open_log_id'))
     


    if action == 'conversion':
        params['site_event_name'] = event

    if action == 'install':
        del data_value['currency_code']
        params['post_conversion']='1'

    
    da_str = urllib.urlencode(data_value)

    key = campaign_data.get('mat').get('key')
    iv = campaign_data.get('mat').get('iv')
    aes = AESCipher(key)
    params['data'] = aes.encrypt(iv, da_str)

    return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

###################################################
#           APPSFLYER
###################################################

def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
    def_firstLaunchDate(app_data,device_data)
    def_appsflyerUID(app_data)
    inc_(app_data,'counter')
    def_(app_data,'iaecounter')
    get_deviceData(app_data, device_data)
    app_data['time_in_app']=int(time.time())
    if app_data.get('timepassedsincelastlaunch'):
        timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
        app_data['timepassedsincelastlaunch']=int(time.time())
    else:
        app_data['timepassedsincelastlaunch']=int(time.time())
        timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

    method = "post"
    url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
    headers = {
        "Accept-Encoding" : "gzip",
        "Content-Type" : "application/json",
        "User-Agent" : get_ua(device_data),

        }
    params = {
        "app_id" : campaign_data.get('package_name'),
        "buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

        }
    data = {
        "advertiserId" : device_data.get('adid'),
        "advertiserIdEnabled" : "true",
        "af_events_api" : "1",
        "af_preinstalled" : "false",
        "af_sdks" : "0000000000",
        "af_timestamp" : timestamp(),
        "app_version_code" : campaign_data.get('app_version_code'),
        "app_version_name" : campaign_data.get('app_version_name'),
        "appsflyerKey" : campaign_data.get('appsflyer').get('key'),
        "batteryLevel" : get_batteryLevel(app_data),
        "brand" : device_data.get('brand'),
        "carrier" : device_data.get('carrier'),
        # "cksm_v1" : util.get_random_string('hex',34),
        "counter" : str(app_data.get('counter')),
        "country" : 'TW',
        "date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
        "date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
        "device" : device_data.get('device'),
        "deviceData" : {
                        "arch" : "",
                        "btch" : "no",
                        "btl" : get_batteryLevel(app_data),
                        "build_display_id" : device_data.get('build'),
                        "cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
                        "cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
                        "dim" : {"d_dpi": device_data.get('dpi'),                               
                                "size": app_data.get('dim_size'),
                                "x_px": device_data.get('resolution').split('x')[1],
                                "xdp": app_data.get('xdp'),
                                "y_px": device_data.get('resolution').split('x')[0],
                                "ydp": app_data.get('ydp'),
                            },
                        "sensors" : [{u'sN': u'LSM6DB0 Gyroscope Sensor', u'sVE': [-0.019536, -0.002442, -0.006105], u'sV': u'STMicroelectronics', u'sVS': [-0.030524999, -0.009768, -0.004884], u'sT': 4}, {u'sN': u'LSM6DB0 Magnetometer Sensor', u'sVE': [26.22, 1.62, -85.08], u'sV': u'STMicroelectronics', u'sVS': [27.720001, 1.5, -85.62], u'sT': 2}, {u'sN': u'LSM6DB0 Accelerometer Sensor', u'sVE': [-0.321724, 0.07774, 10.377692], u'sV': u'STMicroelectronics', u'sVS': [-0.321724, 0.06578, 10.2509165], u'sT': 1}],
        },
        "deviceType" : "user",
        "firstLaunchDate" : app_data.get('firstLaunchDate'),
        "iaecounter" : str(app_data.get('iaecounter')),
        "installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
        "installer_package" : "com.android.vending",
        "isFirstCall" : isFirstCall,
        "isGaidWithGps" : "true",
        "lang" : '中文'.encode('utf-8'),
        "lang_code" : 'zh',
        "model" : device_data.get('model'),
        "network" : device_data.get('network').upper(),
        "operator" : device_data.get('carrier'),
        "platformextension" : "android_cocos2dx",
        "product" : device_data.get('product'),
        "referrer" : 'utm_source=google-play&utm_medium=organic',
        "registeredUninstall" : False,
        "sdk" : device_data.get('sdk'),
        "timepassedsincelastlaunch" : str(timeSinceLastCall),
        "uid" : app_data.get('uid'),

        }           
    data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
    
    if app_data.get('referrer'):
        data['referrer'] = app_data.get('referrer')
    if app_data.get('user_id'):
        data["appUserId"] = app_data.get('user_id')

    if app_data.get('counter')>1:
        data['af_gcm_token']=app_data.get('gcm')
        data['registeredUninstall']=True

    if app_data.get('installAttribution'):
        data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
    
    string_for_afv   =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
    data['af_v']     =  util.sha1(string_for_afv)
    
    string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
    data['af_v2']    =  util.sha1(util.md5(string_for_af_v2))

    if isFirstCall=="false":
        if data.get('af_sdks'):
            del data['af_sdks']
        if data.get('batteryLevel'):
            del data['batteryLevel']
        if data.get('open_referrer'):
            data['open_referrer']="android-app://"+campaign_data.get("package_name")
            
    else:
        data['batteryLevel']=get_batteryLevel(app_data)
    
    data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

    
    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
    def_firstLaunchDate(app_data,device_data)
    def_appsflyerUID(app_data)
    def_(app_data,'counter')
    inc_(app_data,'iaecounter')
    get_deviceData(app_data, device_data)
    method = "post"
    url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
    headers = {
        "Accept-Encoding" : "gzip",
        "Content-Type" : "application/json",
        "User-Agent" : get_ua(device_data),

        }
    params = {
        "app_id" : campaign_data.get('package_name'),
        "buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

        }
    data = {
        "advertiserId" : device_data.get('adid'),
        "advertiserIdEnabled" : "true",
        "af_events_api" : "1",
        "af_preinstalled" : "false",
        "af_timestamp" : timestamp(),
        "app_version_code" : campaign_data.get('app_version_code'),
        "app_version_name" : campaign_data.get('app_version_name'),
        "appsflyerKey" : campaign_data.get('appsflyer').get('key'),
        "brand" : device_data.get('brand'),
        "carrier" : device_data.get('carrier'),
        # "cksm_v1" : util.get_random_string('hex',32),
        "counter" : str(app_data.get('counter')),
        "country" : 'TW',
        "date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
        "date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
        "device" : device_data.get('device'),
        "deviceData" : {
                        "arch" : "",
                        "build_display_id" : device_data.get('build'),
                        "cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
                        "cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
                        "dim" : {"d_dpi": device_data.get('dpi'),                               
                                "size": app_data.get('dim_size'),
                                "x_px": device_data.get('resolution').split('x')[1],
                                "xdp": app_data.get('xdp'),
                                "y_px": device_data.get('resolution').split('x')[0],
                                "ydp": app_data.get('ydp'),
                            },
        },
        "deviceType" : "user",
        "firstLaunchDate" : app_data.get('firstLaunchDate'),
        "iaecounter" : str(app_data.get('iaecounter')),
        "installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
        "installer_package" : "com.android.vending",
        "isFirstCall" : "false",
        "isGaidWithGps" : "true",
        "lang" : '中文'.encode('utf-8'),
        "lang_code" : 'zh',
        "model" : device_data.get('model'),
        "network" : device_data.get('network').upper(),
        "operator" : device_data.get('carrier'),
        "platformextension" : "android_cocos2dx",
        "product" : device_data.get('product'),
        "referrer" : 'utm_source=google-play&utm_medium=organic',
        "registeredUninstall" : True,
        "sdk" : device_data.get('sdk'),
        "uid" : app_data.get('uid'),
        'af_gcm_token':app_data.get('gcm')

        }   
    string_for_afv   =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
    data['af_v']     =  util.sha1(string_for_afv)
        
    data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
    if app_data.get('referrer'):
        data['referrer'] = app_data.get('referrer')

    string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
    data['af_v2']    =  util.sha1(util.md5(string_for_af_v2))

    data["eventValue"]  = eventValue
    data["eventName"]   = eventName

    if app_data.get('installAttribution'):
        data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
    

    if app_data.get('user_id'):
        data["appUserId"] = app_data.get('user_id')

    if eventName=='Icoin':
        data['currency']='TWD'


    if app_data.get('iaecounter')>1:
        if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
            data['prev_event'] = json.dumps({
                                    "prev_event_timestamp"  :app_data.get('prev_event_time'),
                                    "prev_event_value"      :app_data.get('prev_event_value'),
                                    "prev_event_name"       :app_data.get('prev_event_name')
                                    })
        if data['isFirstCall']=='true':
            data['isFirstCall']='false'
            
    update_eventsRecords(app_data,eventName,eventValue)
    
    data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

    
    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def appsflyer_api(campaign_data, app_data, device_data):
    def_appsflyerUID(app_data)

    url     = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
    method  = 'get'
    headers = {
        'Accept-Encoding' : 'gzip',
        'User-Agent'      : get_ua(device_data),
        }
    params  = {
        'devkey'          : campaign_data.get('appsflyer').get('key'),
        'device_id'       : app_data.get('uid')
        }
    data    = None
    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

###################################################################
# appsflyer_register()  : method
# parameter             : campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data, call=''):
    get_google_gcmToken(app_data)
    def_appsflyerUID(app_data)
    def_(app_data,'counter')
    method = "post"
    url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
    headers = {
        "Accept-Encoding" : "gzip",
        "Content-Type" : "application/json",
        "User-Agent" : get_ua(device_data),

        }
    params = {
        "app_id" : campaign_data.get('package_name'),
        "buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

        }
    data = {
        "advertiserId" : device_data.get('adid'),
        "af_gcm_token" : app_data.get('gcm_token'),
        "app_name" : campaign_data.get('app_name'),
        "app_version_code" : campaign_data.get('app_version_code'),
        "app_version_name" : campaign_data.get('app_version_name'),
        "brand" : device_data.get('brand').upper(),
        "carrier" : device_data.get('carrier'),
        "devkey" : campaign_data.get('appsflyer').get('key'),
        "installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
        "launch_counter" : str(app_data.get('counter')),
        "model" : device_data.get('model'),
        "network" : device_data.get('network').upper(),
        "operator" : device_data.get('carrier'),
        "sdk" : device_data.get('sdk'),
        "uid" : app_data.get('uid'),
        }

    if call==2:
        data['af_gcm_token']=app_data.get('gcm')+','+app_data.get('gcm_token')

    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
            
def click(device_data=None, camp_type='market', camp_plat = 'android'):

    package_name = campaign_data.get('package_name');
    serial        = device_data.get('serial')
    agent_id      = Config.AGENTID
    random_number = random.randint(1,10)
    source_id     = ""
    if random_number < 5:
        source_id = "728"
    elif random_number < 8:
        source_id = "517"
    elif random_number < 9:
        source_id = "604"
    else:
        source_id = "386"
    st = device_data.get("device_id", str(int(time.time()*1000)))
    link = campaign_data['link']
    link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
    return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def set_OpenLogID(app_data):
    if app_data.get('log_id'):
        app_data['open_log_id'] = app_data['log_id']

def set_LastOpenLogID(app_data):
    if app_data.get('log_id'):
        app_data['last_open_log_id'] = app_data['log_id']

def catch_mat(response,app_data,paramName):
    try:
        if paramName=="mat":
            app_data['log_id'] = json.loads(response.get('data')).get('log_id')
    except Exception as inst:
        print inst      



def def_appsflyerUID(app_data):
    if not app_data.get('uid'):
        app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def inc_(app_data,paramName):
    def_(app_data,paramName)
    app_data[paramName] += 1

def def_(app_data,paramName):
    if not app_data.get(paramName):
        app_data[paramName] = 0

def catch(response,app_data,paramName):
    try:
        jsonData = json.loads(response.get('data'))
        if paramName=="appsflyer":
            app_data['installAttribution'] = jsonData
    except:
        print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def get_ua(device_data):
    if int(device_data.get("sdk")) >=19:
        return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
    else:
        return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def get_deviceData(app_data, device_data):
    if not app_data.get('dim_size'):
        app_data['dim_size']=str(random.randint(1,5))
    if not app_data.get('xdp'):
        app_data['xdp']='294.'+str(random.randint(600,999))
    if not app_data.get('ydp'):
        app_data['ydp']='295.'+str(random.randint(100,600))
    

def get_batteryLevel(app_data):
    if not app_data.get('btl'):
        app_data['btl']=str(random.randint(10,100))+".0"
    return app_data.get('btl')

def md5(data, radix=16):
    import hashlib
    md5_obj = hashlib.md5()
    md5_obj.update(data)
    if radix == 16:
        return md5_obj.hexdigest()
    elif radix == 64:
        return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
    pk = campaign_data.get("package_name").split('.')
    pk[0], pk[-1] = pk[-1], pk[0]
    pkn='.'.join(pk)
    string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
    print "========cksm_v1========"
    print string
    print "======================="
    sha256_result = sha256(string)  
    md5_result = md5(sha256_result) 
        
    n = ins_time
    sb = md5_result
    n4 = 0

    sb = change_char(sb,17,'f')
    sb = change_char(sb,27,'f')

    for i in range(0,len(str(n))):
        n4 += int(str(n)[i])
        

    insert1 = list('{:02x}'.format(n4))
    sb = change_char(sb,7,insert1[0])
    sb = change_char(sb,8,insert1[1])
            
    j = 0
    n6 = 77
    n3 = 0
    for i in range(0,len(str(sb))):
        n3 += int(str(sb)[i],36)
        
    if n3>100:
        n8 = 90
        n3%=100
        
    sb = insert_char(sb,23,str(n3))
    return sb


def def_firstLaunchDate(app_data,device_data):
    if not app_data.get('firstLaunchDate'):
        date = int(time.time())
        app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"


def def_eventsRecords(app_data):
    app_data['prev_event_name']  = ""
    app_data['prev_event_value'] = ""
    app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
    app_data['prev_event_name']  = eventName
    app_data['prev_event_value'] = eventValue
    app_data['prev_event_time']  = str(int(time.time()*1000))

def get_google_gcmToken(app_data):
    if not app_data.get('gcm_token'):
        app_data['gcm_token'] = util.get_random_string('char_all',11) +':'+'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

###########################################################
# Utility methods : MISC
#
###########################################################

def timestamp():
    return str(int(time.time()*1000))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
    
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
    import hashlib
    sha_obj = hashlib.sha256()
    sha_obj.update(data)
    return sha_obj.hexdigest()