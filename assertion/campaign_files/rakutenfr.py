# -*- coding: utf8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from sdk import installtimenew,purchase
from sdk import getsleep
from sdk import util
from sdk import NameLists
import time,urllib
import random
import json
import string
import datetime
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.priceminister.buyerapp',
	'app_size'			 : 22.0,
	'app_name' 			 :'Rakuten : Shopping en ligne au meilleur prix',
	'app_version_name' 	 :'6.6.2',#'6.6.1',#'6.5.6',#6.5.4, '6.5.3',#'6.5.2',#6.4.3, '6.4.2',#'6.2.1',
	'app_version_code' 	 :'147',#'146',#'144',#142, '140',#'139',#136, '134',#'125',
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '5.0',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'iGzWF7NQmP5G54s3HK3NbL',
		'dkh'		 : 'iGzWF7NQ',
		'buildnumber': '3.7',
		'version'	 : 'v4',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android',min_sleep=20)
	def_sec(app_data,device_data)
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	def_firstLaunchDate(app_data,device_data)
	app_data['time_in_app']=int(time.time())
	register_user(app_data)
	global userAppId,cart,revenue1,revenue2
	userAppId = ""
	cart = list()
	revenue1 = 0.0
	revenue2 = 0.0

	###########		CALLS		############
	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
	time.sleep(random.randint(30,60))

	app_data['categories']=[]

	print "\nself_call_ws_fr_shopping_rakuten\n"
	request=ws_fr_shopping_rakuten_category( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	# global categories
	# categories = list()
	try:
		result = json.loads(response.get('data')).get('categories')
		for item in result:
			app_data.get('categories').append(item.get('name'))

		if  len(app_data.get('categories'))<1:
			app_data['categories'] = ['Jeux-Video-et-Consoles_Cartes-de-jeux', 'Jeux-Video-et-Consoles_Jeux-Video', 'Jeux-Video-et-Consoles_Figurines', 'Jeux-Video-et-Consoles_Accessoires-Jeux-Video', 'Jeux-Video-et-Consoles_lot-console-et-jeux-video', 'Jeux-Video-et-Consoles_Jeux-de-role-et-jeux-de-figurines', 'Jeux-Video-et-Consoles_Consoles', 'Jeux-Video-et-Consoles_Jeux-video-en-pre-commande']	
	except:
		app_data['categories'] = ['Jeux-Video-et-Consoles_Cartes-de-jeux', 'Jeux-Video-et-Consoles_Jeux-Video', 'Jeux-Video-et-Consoles_Figurines', 'Jeux-Video-et-Consoles_Accessoires-Jeux-Video', 'Jeux-Video-et-Consoles_lot-console-et-jeux-video', 'Jeux-Video-et-Consoles_Jeux-de-role-et-jeux-de-figurines', 'Jeux-Video-et-Consoles_Consoles', 'Jeux-Video-et-Consoles_Jeux-video-en-pre-commande']
		

	print "\nself_call_ws_fr_shopping_rakuten\n"
	request=ws_fr_shopping_rakuten_productsSearch( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global product_list
	product_list = []
	try:
		result = json.loads(response.get('data')).get('products')
		for item in result:
			a = {}
			a['id'] = item.get('id')
			a['newBestPrice'] = item.get('newBestPrice')
			a['usedBestPrice'] = item.get('usedBestPrice')
			product_list.append(a)

		if not product_list:
			product_list = [{'id': 66325584, 'usedBestPrice': 7.9, 'newBestPrice': 19.9}, {'id': 126692669, 'usedBestPrice': 15, 'newBestPrice': 19.97}, {'id': 298977853, 'usedBestPrice': 79.9, 'newBestPrice': 248.96}, {'id': 5894131, 'usedBestPrice': 27.97, 'newBestPrice': 27.97}, {'id': 139394879, 'usedBestPrice': 0, 'newBestPrice': 43.47}, {'id': 150329558, 'usedBestPrice': 0, 'newBestPrice': 26.05}, {'id': 76003538, 'usedBestPrice': 2.6, 'newBestPrice': 0}, {'id': 126690619, 'usedBestPrice': 0, 'newBestPrice': 27.33}, {'id': 1957993, 'usedBestPrice': 0, 'newBestPrice': 25.88}, {'id': 61457733, 'usedBestPrice': 0, 'newBestPrice': 23.97}, {'id': 112305272, 'usedBestPrice': 5, 'newBestPrice': 10.58}, {'id': 156943082, 'usedBestPrice': 0, 'newBestPrice': 23}, {'id': 51519648, 'usedBestPrice': 0, 'newBestPrice': 39.1}, {'id': 51518571, 'usedBestPrice': 30.35, 'newBestPrice': 32.97}, {'id': 153997451, 'usedBestPrice': 0, 'newBestPrice': 24.34}, {'id': 753040879, 'usedBestPrice': 4.4, 'newBestPrice': 20.24}, {'id': 3963207296L, 'usedBestPrice': 0.9, 'newBestPrice': 0}, {'id': 4259379, 'usedBestPrice': 0, 'newBestPrice': 32.51}, {'id': 124140929, 'usedBestPrice': 0, 'newBestPrice': 9.99}, {'id': 2841774704L, 'usedBestPrice': 0, 'newBestPrice': 32.31}]

	except:
		product_list = [{'id': 66325584, 'usedBestPrice': 7.9, 'newBestPrice': 19.9}, {'id': 126692669, 'usedBestPrice': 15, 'newBestPrice': 19.97}, {'id': 298977853, 'usedBestPrice': 79.9, 'newBestPrice': 248.96}, {'id': 5894131, 'usedBestPrice': 27.97, 'newBestPrice': 27.97}, {'id': 139394879, 'usedBestPrice': 0, 'newBestPrice': 43.47}, {'id': 150329558, 'usedBestPrice': 0, 'newBestPrice': 26.05}, {'id': 76003538, 'usedBestPrice': 2.6, 'newBestPrice': 0}, {'id': 126690619, 'usedBestPrice': 0, 'newBestPrice': 27.33}, {'id': 1957993, 'usedBestPrice': 0, 'newBestPrice': 25.88}, {'id': 61457733, 'usedBestPrice': 0, 'newBestPrice': 23.97}, {'id': 112305272, 'usedBestPrice': 5, 'newBestPrice': 10.58}, {'id': 156943082, 'usedBestPrice': 0, 'newBestPrice': 23}, {'id': 51519648, 'usedBestPrice': 0, 'newBestPrice': 39.1}, {'id': 51518571, 'usedBestPrice': 30.35, 'newBestPrice': 32.97}, {'id': 153997451, 'usedBestPrice': 0, 'newBestPrice': 24.34}, {'id': 753040879, 'usedBestPrice': 4.4, 'newBestPrice': 20.24}, {'id': 3963207296L, 'usedBestPrice': 0.9, 'newBestPrice': 0}, {'id': 4259379, 'usedBestPrice': 0, 'newBestPrice': 32.51}, {'id': 124140929, 'usedBestPrice': 0, 'newBestPrice': 9.99}, {'id': 2841774704L, 'usedBestPrice': 0, 'newBestPrice': 32.31}]

	print '\n'+'Appsflyer : Stats____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)
	
	time.sleep(random.randint(20,40))
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)

	for _ in range(random.randint(1,3)):

		if random.randint(1,100)<=40 and not app_data.get('user_id'):
			
			time.sleep(random.randint(30,45))
			print '\nRegister selfcall'
			req = ws_fr_shopping_rakuten_login( campaign_data, device_data, app_data )
			resp = util.execute_request(**req)
			try:
				userAppId=json.loads(resp.get('data')).get('users')[0].get('id')
			except:
				userAppId='111441251'

			app_data['user_id']=userAppId	

			time.sleep(random.randint(2,5))
			af_login(campaign_data, app_data, device_data)

		if random.randint(1,100)<=80:
			time.sleep(random.randint(5,10))
			af_content_list(campaign_data, app_data, device_data)	

		if random.randint(1,100)<=90:	
			time.sleep(random.randint(10,15))
			af_content_view(campaign_data, app_data, device_data)

			print "\nself_call_ws_fr_shopping_rakuten\n"
			request=ws_fr_shopping_rakuten_products( campaign_data, device_data, app_data )
			response = util.execute_request(**request)
			global advertId,isoCountryId,shippingAmount,item_list
			item_list=[]
			try:
				result = json.loads(response.get('data')).get('adverts')[0]
				advertId = result.get('advertId')
				item=result.get('availableShippingTypes')
				for i in range(len(item)):
					item_list.append(item[i].get('id'))
				app_data['itmSourceCode'] =random.choice(item_list)
				isoCountryId = result.get('seller').get('isoCountryId')
				shippingAmount = result.get('shippingAmount')
			except:
				advertId = random.randint(5018526002,5018526002)
				app_data['itmSourceCode'] = random.randint(30,80)
				isoCountryId = random.randint(200,300)
				shippingAmount = random.randint(100,900)/100.0


			if random.randint(1,100)<=50:	
				time.sleep(random.randint(30,45))
				af_add_to_cart(campaign_data, app_data, device_data)

				print "\nself_call_ws_fr_shopping_rakuten\n"
				request=ws_fr_shopping_rakuten_cart_add( campaign_data, device_data, app_data )
				response = util.execute_request(**request)
				global cartId
				try:
					app_data['count']=0
					while(json.loads(response.get('data')).get('errors')[0].get('message')):
						app_data['count']+=1
						app_data['itmSourceCode'] =random.choice(item_list)

						print "\nself_call_ws_fr_shopping_rakuten\n"
						request=ws_fr_shopping_rakuten_cart_add( campaign_data, device_data, app_data )
						response = util.execute_request(**request)
						try:
							result = json.loads(response.get('data')).get('cart').get('cartId')
							cartId = result
						except:
							cartId = random.randint(300000000,399999999)	

						if app_data.get('count')==5:
							break

					result = json.loads(response.get('data')).get('cart').get('cartId')
					cartId = result
					
				except:
					cartId = random.randint(300000000,399999999)

				num = random.randint(20,70)/100.0	
				price = prod.get('usedBestPrice') if prod.get('usedBestPrice')>0 else prod.get('newBestPrice')
				price += shippingAmount
				revenue1 += price-num
				revenue2 += num
				a = {}
				a['af_price'] = num
				a['af_content_id'] = str(prod.get('id'))
				a['af_quantity'] = 1
				cart.append(a)
						
		if random.randint(1,100)<=70:
			time.sleep(random.randint(1,4))	
			print '\n'+'Appsflyer : Stats____________________________________'
			request  = appsflyer_stats(campaign_data, app_data, device_data)
			util.execute_request(**request)
			
			time.sleep(random.randint(30,60))
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)

	if app_data.get('user_id') and len(cart)>0:
		if purchase.isPurchase(app_data,day=1,advertiser_demand=10):	
			time.sleep(random.randint(30,45))
			af_purchase(campaign_data, app_data, device_data)
			time.sleep(random.randint(1,5))
			com_auth(campaign_data, app_data, device_data)		
			
	time.sleep(random.randint(1,4))
	print '\n'+'Appsflyer : Stats____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)
	
	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android',min_sleep=20)

	def_sec(app_data,device_data)
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	def_firstLaunchDate(app_data,device_data)
	app_data['time_in_app']=int(time.time())
	register_user(app_data)
	global userAppId,cart,revenue1,revenue2
	userAppId = ""
	cart = list()
	revenue1 = 0.0
	revenue2 = 0.0

	###########		CALLS		############
	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)
	time.sleep(random.randint(30,60))

	if random.randint(1,100)<=5 and app_data.get('user_id'):
		time.sleep(random.randint(30,45))
		af_login(campaign_data, app_data, device_data)

	if not app_data.get('categories'):
		app_data['categories']=[]

		print "\nself_call_ws_fr_shopping_rakuten\n"
		request=ws_fr_shopping_rakuten_category( campaign_data, device_data, app_data )
		response = util.execute_request(**request)
		# global categories
		# categories = list()
		try:
			result = json.loads(response.get('data')).get('categories')
			for item in result:
				app_data.get('categories').append(item.get('name'))

			if len(app_data.get('categories'))<1:
				app_data['categories'] = ['Jeux-Video-et-Consoles_Cartes-de-jeux', 'Jeux-Video-et-Consoles_Jeux-Video', 'Jeux-Video-et-Consoles_Figurines', 'Jeux-Video-et-Consoles_Accessoires-Jeux-Video', 'Jeux-Video-et-Consoles_lot-console-et-jeux-video', 'Jeux-Video-et-Consoles_Jeux-de-role-et-jeux-de-figurines', 'Jeux-Video-et-Consoles_Consoles', 'Jeux-Video-et-Consoles_Jeux-video-en-pre-commande']	
		except:
			app_data['categories'] = ['Jeux-Video-et-Consoles_Cartes-de-jeux', 'Jeux-Video-et-Consoles_Jeux-Video', 'Jeux-Video-et-Consoles_Figurines', 'Jeux-Video-et-Consoles_Accessoires-Jeux-Video', 'Jeux-Video-et-Consoles_lot-console-et-jeux-video', 'Jeux-Video-et-Consoles_Jeux-de-role-et-jeux-de-figurines', 'Jeux-Video-et-Consoles_Consoles', 'Jeux-Video-et-Consoles_Jeux-video-en-pre-commande']
		

	print "\nself_call_ws_fr_shopping_rakuten\n"
	request=ws_fr_shopping_rakuten_productsSearch( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global product_list
	product_list = []
	try:
		result = json.loads(response.get('data')).get('products')
		for item in result:
			a = {}
			a['id'] = item.get('id')
			a['newBestPrice'] = item.get('newBestPrice')
			a['usedBestPrice'] = item.get('usedBestPrice')
			product_list.append(a)

		if not product_list:
			product_list = [{'id': 66325584, 'usedBestPrice': 7.9, 'newBestPrice': 19.9}, {'id': 126692669, 'usedBestPrice': 15, 'newBestPrice': 19.97}, {'id': 298977853, 'usedBestPrice': 79.9, 'newBestPrice': 248.96}, {'id': 5894131, 'usedBestPrice': 27.97, 'newBestPrice': 27.97}, {'id': 139394879, 'usedBestPrice': 0, 'newBestPrice': 43.47}, {'id': 150329558, 'usedBestPrice': 0, 'newBestPrice': 26.05}, {'id': 76003538, 'usedBestPrice': 2.6, 'newBestPrice': 0}, {'id': 126690619, 'usedBestPrice': 0, 'newBestPrice': 27.33}, {'id': 1957993, 'usedBestPrice': 0, 'newBestPrice': 25.88}, {'id': 61457733, 'usedBestPrice': 0, 'newBestPrice': 23.97}, {'id': 112305272, 'usedBestPrice': 5, 'newBestPrice': 10.58}, {'id': 156943082, 'usedBestPrice': 0, 'newBestPrice': 23}, {'id': 51519648, 'usedBestPrice': 0, 'newBestPrice': 39.1}, {'id': 51518571, 'usedBestPrice': 30.35, 'newBestPrice': 32.97}, {'id': 153997451, 'usedBestPrice': 0, 'newBestPrice': 24.34}, {'id': 753040879, 'usedBestPrice': 4.4, 'newBestPrice': 20.24}, {'id': 3963207296L, 'usedBestPrice': 0.9, 'newBestPrice': 0}, {'id': 4259379, 'usedBestPrice': 0, 'newBestPrice': 32.51}, {'id': 124140929, 'usedBestPrice': 0, 'newBestPrice': 9.99}, {'id': 2841774704L, 'usedBestPrice': 0, 'newBestPrice': 32.31}]

	except:
		product_list = [{'id': 66325584, 'usedBestPrice': 7.9, 'newBestPrice': 19.9}, {'id': 126692669, 'usedBestPrice': 15, 'newBestPrice': 19.97}, {'id': 298977853, 'usedBestPrice': 79.9, 'newBestPrice': 248.96}, {'id': 5894131, 'usedBestPrice': 27.97, 'newBestPrice': 27.97}, {'id': 139394879, 'usedBestPrice': 0, 'newBestPrice': 43.47}, {'id': 150329558, 'usedBestPrice': 0, 'newBestPrice': 26.05}, {'id': 76003538, 'usedBestPrice': 2.6, 'newBestPrice': 0}, {'id': 126690619, 'usedBestPrice': 0, 'newBestPrice': 27.33}, {'id': 1957993, 'usedBestPrice': 0, 'newBestPrice': 25.88}, {'id': 61457733, 'usedBestPrice': 0, 'newBestPrice': 23.97}, {'id': 112305272, 'usedBestPrice': 5, 'newBestPrice': 10.58}, {'id': 156943082, 'usedBestPrice': 0, 'newBestPrice': 23}, {'id': 51519648, 'usedBestPrice': 0, 'newBestPrice': 39.1}, {'id': 51518571, 'usedBestPrice': 30.35, 'newBestPrice': 32.97}, {'id': 153997451, 'usedBestPrice': 0, 'newBestPrice': 24.34}, {'id': 753040879, 'usedBestPrice': 4.4, 'newBestPrice': 20.24}, {'id': 3963207296L, 'usedBestPrice': 0.9, 'newBestPrice': 0}, {'id': 4259379, 'usedBestPrice': 0, 'newBestPrice': 32.51}, {'id': 124140929, 'usedBestPrice': 0, 'newBestPrice': 9.99}, {'id': 2841774704L, 'usedBestPrice': 0, 'newBestPrice': 32.31}]

	print '\n'+'Appsflyer : Stats____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)
	
	time.sleep(random.randint(20,40))
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)

	for _ in range(random.randint(1,3)):

		if random.randint(1,100)<=40 and not app_data.get('user_id'):
			
			time.sleep(random.randint(30,60))
			print '\nRegister selfcall'
			req = ws_fr_shopping_rakuten_login( campaign_data, device_data, app_data )
			resp = util.execute_request(**req)
			try:
				userAppId=json.loads(resp.get('data')).get('users')[0].get('id')
			except:
				userAppId='111441251'

			app_data['user_id']=userAppId	

			time.sleep(random.randint(2,5))
			af_login(campaign_data, app_data, device_data)

		if random.randint(1,100)<=70:
			time.sleep(random.randint(5,10))
			af_content_list(campaign_data, app_data, device_data)	

		if random.randint(1,100)<=80:	
			time.sleep(random.randint(10,15))
			af_content_view(campaign_data, app_data, device_data)

			print "\nself_call_ws_fr_shopping_rakuten\n"
			request=ws_fr_shopping_rakuten_products( campaign_data, device_data, app_data )
			response = util.execute_request(**request)
			global advertId,isoCountryId,shippingAmount,item_list
			item_list=[]
			try:
				result = json.loads(response.get('data')).get('adverts')[0]
				advertId = result.get('advertId')
				item=result.get('availableShippingTypes')
				for i in range(len(item)):
					item_list.append(item[i].get('id'))
				app_data['itmSourceCode'] =random.choice(item_list)
				isoCountryId = result.get('seller').get('isoCountryId')
				shippingAmount = result.get('shippingAmount')
			except:
				advertId = random.randint(5018526002,5018526002)
				app_data['itmSourceCode'] = random.randint(30,80)
				isoCountryId = random.randint(200,300)
				shippingAmount = random.randint(100,900)/100.0


			if random.randint(1,100)<=50:	
				time.sleep(random.randint(30,45))
				af_add_to_cart(campaign_data, app_data, device_data)

				print "\nself_call_ws_fr_shopping_rakuten\n"
				request=ws_fr_shopping_rakuten_cart_add( campaign_data, device_data, app_data )
				response = util.execute_request(**request)
				global cartId
				try:
					app_data['count']=0
					while(json.loads(response.get('data')).get('errors')[0].get('message')):
						app_data['count']+=1
						app_data['itmSourceCode'] =random.choice(item_list)

						print "\nself_call_ws_fr_shopping_rakuten\n"
						request=ws_fr_shopping_rakuten_cart_add( campaign_data, device_data, app_data )
						response = util.execute_request(**request)
						try:
							result = json.loads(response.get('data')).get('cart').get('cartId')
							cartId = result
						except:
							cartId = random.randint(300000000,399999999)	

						if app_data.get('count')==5:
							break

					result = json.loads(response.get('data')).get('cart').get('cartId')
					cartId = result
					
				except:
					cartId = random.randint(300000000,399999999)

				num = random.randint(20,70)/100.0	
				price = prod.get('usedBestPrice') if prod.get('usedBestPrice')>0 else prod.get('newBestPrice')
				price += shippingAmount
				revenue1 += price-num
				revenue2 += num
				a = {}
				a['af_price'] = num
				a['af_content_id'] = str(prod.get('id'))
				a['af_quantity'] = 1
				cart.append(a)
						
		if random.randint(1,100)<=70:
			time.sleep(random.randint(1,4))	
			print '\n'+'Appsflyer : Stats____________________________________'
			request  = appsflyer_stats(campaign_data, app_data, device_data)
			util.execute_request(**request)
			
			time.sleep(random.randint(30,60))
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)

	if app_data.get('user_id') and len(cart)>0:
		if purchase.isPurchase(app_data,day,advertiser_demand=10):	
			time.sleep(random.randint(30,45))
			af_purchase(campaign_data, app_data, device_data)
			time.sleep(random.randint(1,5))
			com_auth(campaign_data, app_data, device_data)		
			
	time.sleep(random.randint(1,4))
	print '\n'+'Appsflyer : Stats____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)

	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def af_login(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_login'
	eventName			= 'af_login'
	eventValue			= json.dumps({})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_content_view(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_content_view'
	global prod
	try:
		prod = random.choice(product_list)
	except:
		prod = {'id': 66325584, 'usedBestPrice': 7.9, 'newBestPrice': 19.9}	
	eventName			= 'af_content_view'
	eventValue			= json.dumps({"af_content_id":str(prod.get('id')),"af_currency":"USD"})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_add_to_cart(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_add_to_cart'
	eventName			= 'af_add_to_cart'
	eventValue			= json.dumps({"product":[{"af_price":prod.get('usedBestPrice') if prod.get('usedBestPrice')>0 else prod.get('newBestPrice'),"af_content_id":prod.get('id'),"af_quantity":1}],"af_currency":"USD"})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_content_list(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_content_list'
	product = []
	for _ in range(3):
		product.append(str(random.choice(product_list).get('id')))
	eventName			= 'af_content_list'
	eventValue			= json.dumps({"af_currency":"USD","product":product})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def af_purchase(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_purchase'
	eventName			= 'af_purchase'
	eventValue			= json.dumps({"af_revenue":str(revenue1),"product":cart,"af_currency":"USD","af_receipt_id":str(cartId)})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def com_auth(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________com_auth'
	eventName			= 'com_auth'
	eventValue			= json.dumps({"af_revenue":revenue2,"product":cart,"af_currency":"USD","af_receipt_id":str(cartId)})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)	



###################################################################
#						selfcalls
###################################################################

def ws_fr_shopping_rakuten_login( campaign_data, device_data, app_data ):
	url= "http://ws.fr.shopping.rakuten.com/restpublic/buy-apps/user"
	method= "put"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json; charset=UTF-8",
        "User-Agent": device_data.get('User-Agent')
        }

	params= {       
		"channel": "buyerapp",
        "channelType": "BUYER_SMARTPHONE_APP",
        "deviceId": device_data.get('android_id'),
        "deviceName": device_data.get('manufacturer')+"+"+device_data.get('model'),
        "version": "1"
        }

	data= {       
		"accept_terms": "true",
        "dob": app_data.get('user').get('dob')[8:]+'/'+app_data.get('user').get('dob')[5:7]+'/'+app_data.get('user').get('dob')[0:4],
        "email": app_data.get('user').get('email'),
        "email_confirmation": app_data.get('user').get('email'),
        "first_name": app_data.get('user').get('firstname'),
        "last_name": app_data.get('user').get('lastname'),
        "password": util.get_random_string('char_all',8)+'@'+util.get_random_string('decimal',2),
        "salutation": "30",
        "subscribe_to_news_letters": "true"
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def ws_fr_shopping_rakuten_category( campaign_data, device_data, app_data ):
	url= "http://ws.fr.shopping.rakuten.com/restpublic/vis-apps/productsSearch"
	method= "get"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "User-Agent": device_data.get('User-Agent'),
        }

	params= {       
		"category": random.choice(["Tel-PDA","Informatique","Maison","Jeux-Video-et-Consoles","Mode","jardin","Vin-Saveur"]),
        "channel": "buyerapp",
        "channelType": "BUYER_SMARTPHONE_APP",
        "loadProducts": "false",
        "userId": userAppId,
        "version": "1"
        }

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

	
def ws_fr_shopping_rakuten_productsSearch( campaign_data, device_data, app_data ):
	url= "http://ws.fr.shopping.rakuten.com/restpublic/vis-apps/productsSearch"
	method= "get"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "User-Agent": device_data.get('User-Agent'),
        }

	params= {       
		"advertType": "ALL",
        "category": random.choice(app_data.get('categories')),
        "channel": "buyerapp",
        "channelType": "BUYER_SMARTPHONE_APP",
        "disableAlternativeResults": "false",
        "loadMemos": "true",
        "loadProductDetails": "true",
        "pageNumber": "1",
        "sortTypeIndex": "0",
        "userId": userAppId,
        "version": "1",
        "withoutStock": "false"
        }

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

	
def ws_fr_shopping_rakuten_products( campaign_data, device_data, app_data ):
	url= "http://ws.fr.shopping.rakuten.com/restpublic/vis-apps/products/"+str(prod.get('id'))
	method= "get"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Authorization": device_data.get('android_id')+"|null",
        "User-Agent": device_data.get('User-Agent'),
        }

	params= {       
		"advertType": "ALL",
        "channel": "buyerapp",
        "channelType": "BUYER_SMARTPHONE_APP",
        "cheapestAdvertInBuybox": "false",
        "loadBuybox": "true",
        "loadProductDetails": "true",
        "loadRakuponDetails": "true",
        "version": "2"
        }

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

	
def ws_fr_shopping_rakuten_cart_add( campaign_data, device_data, app_data ):
	url= "http://ws.fr.shopping.rakuten.com/restpublic/buy-apps/cart/add"
	method= "post"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json; charset=UTF-8",
        "User-Agent": device_data.get('User-Agent'),
        }

	params= {       
				"channel": "buyerapp", 
				"channelType": "BUYER_SMARTPHONE_APP", 
				"version": "2"
			}

	data= {       
		"advertId": advertId,
        "buyerCountryId": isoCountryId,
        "channel": "buyerapp",
        "itmSourceCode": app_data.get('itmSourceCode')
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}	

	
###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	# app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_sdks" : "0000000000",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"currency" : 'USD',
		"date1" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime('%Y-%m-%d_%H%M')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime('%Y-%m-%d_%H%M')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceFingerPrintId" : app_data.get('deviceFingerPrintId'),
		"deviceType" : "user",
		"dkh" : campaign_data.get('appsflyer').get('dkh'),
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime('%Y-%m-%d_%H%M')+device_data.get('timezone'),
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),

		}			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')
		data['user_emails'] = '{"md5_el_arr":"'+util.md5(urllib.quote(app_data.get('user').get('email'))+",")+'"}'

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		pass
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_stats()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Appsflyer's behaviour whenever user leaves app's context
# example : User drags notification bar,
#			Facebook SignUp page opens in app, etc
###################################################################
def appsflyer_stats(campaign_data, app_data, device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	inc_(app_data,'launchCounter')
	def_(app_data,'counter')
	method = "post"
	url = "https://stats.appsflyer.com/stats"
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"app_id" : campaign_data.get('package_name'),
		"deviceFingerPrintId" : app_data.get('deviceFingerPrintId'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"gcd_conversion_data_timing" : 0,
		"launch_counter" : str(app_data.get('counter')),
		"platform" : "Android",
		"statType" : "user_closed_app",
		"time_in_app" : str(int(random.randint(0,2))),
		"uid" : app_data.get('uid'),

		}

	
	if app_data.get('time_in_app'):
		data['time_in_app']=str(int(time.time())-app_data['time_in_app'])
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"currency" : 'USD',
		"date1" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime('%Y-%m-%d_%H%M')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime('%Y-%m-%d_%H%M')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceFingerPrintId" : app_data.get('deviceFingerPrintId'),
		"deviceType" : "user",
		"dkh" : campaign_data.get('appsflyer').get('dkh'),
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" :datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime('%Y-%m-%d_%H%M')+device_data.get('timezone'),
		"installer_package" : "com.android.vending",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),
		# "user_emails" : '{"md5_el_arr":"'+util.md5(urllib.quote(app_data.get('user').get('email'))+",")+'"}',

		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')
		data['user_emails'] = '{"md5_el_arr":"'+util.md5(urllib.quote(app_data.get('user').get('email'))+",")+'"}'

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date+app_data.get('sec')).strftime("%Y-%m-%d_%H%M")+device_data.get('timezone')

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def ref_time():
	time.sleep(random.randint(10,15))
	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"