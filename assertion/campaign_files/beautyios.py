# -*- coding: UTF-8 -*-
import random,time,datetime,uuid,urllib
from sdk import getsleep
from sdk import util,installtimenew
import json
import clicker,Config

campaign_data = {
	'package_name' : 'jp.co.recruit.mtl.beauty.salon',
	'ctr'		: 6,
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'app_name' : 'Beauty',
	'app_id':'385724144',
	'app_version_code': '15144',#'14962',#'14856',#'14832',#14533, 14453, '14428',#'14209',#'14166',#'5.18.0',#'5.17.0',#'5.16.0',#'5.15.0',#5.14.0,'5.13.0',#5.12.0,'5.10.0',#'5.9.0',#'5.8.0',5.11.0,
	'app_version_name': '5.23.1',#'5.23.0',#'5.22.0',#'5.21.1',#5.21.0, 5.20.2, '5.20.1',#'5.20.0',#'5.19.0',#'5.18.0',#'5.17.0',#'5.16.0',#'5.15.0',#5.14.0,'5.13.0',#5.12.0,'5.10.0',#'5.9.0',#'5.8.0',5.11.0,
	'supported_os':'10.0',
	'ctr':6,
	'app_size':104.1,#99.9,#98.5, 100.0,#98.2,
	'no_referrer': True,
	'supported_countries': 'WW',
	'device_targeting':True,
	'sdk' : 'ios',
	'tracker':'adjust',
	'adjust':{
				'app_token': 'md8gsodo9beo',
				'sdk':'ios4.18.0',#ios4.15.0, ios4.14.1,'ios4.11.4',
				'app_updated_at': '2019-10-01T09:50:51.000Z',#'2019-09-27T13:51:32.000Z',#'2019-08-27T14:03:00.000Z',#'2019-08-08T12:56:44.000Z',#2019-07-31T11:03:40.000Z, 2019-07-10T16:23:58.000Z, '2019-07-01T12:15:56.000Z',#'2019-06-24T14:34:56.000Z',#'2019-05-24T17:39:58.000Z',#'2019-05-08T18:04:08.000Z',#'2019-03-28T12:10:44.000Z',#'2019-02-26T17:45:30.000Z',#'2019-02-04T11:38:58.000Z',#2018-12-25T13:34:54.000Z, '2018-11-29T18:54:38.000Z',#2018-10-26T12:57:58.000Z,'2018-08-31T11:34:48.000Z',#'2018-07-30T12:34:18.000Z',#'2018-06-29T18:28:14.000Z',
				'secret_key' : '38203586611180600511193418285899821201',
				'secret_id'  : '3',

			},
	'retention':{
					1:70,
					2:68,
					3:66,
					4:64,
					5:61,
					6:59,
					7:57,
					8:52,
					9:50,
					10:47,
					11:45,
					12:43,
					13:40,
					14:37,
					15:35,
					16:31,
					17:30,
					18:28,
					19:26,
					20:20,
					21:19,
					22:18,
					23:17,
					24:16,
					25:15,
					26:14,
					27:13,
					28:12,
					29:11,
					30:10,
					31:9,
					32:8,
					33:7,
					34:6,
					35:5,
	}
}


def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')

def sc_vid(app_data):
	if not app_data.get('sc_vid'):
		app_data['sc_vid']=util.get_random_string('hex',32).upper() 
	return app_data.get('sc_vid')

def get_date_ts(app_data,device_data,get_ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(get_ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date
		
###########################################################
#						INSTALL							  #
###########################################################
def install(app_data, device_data):
	print 'plz wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)
	get_ts=app_data.get('times').get('install_complete_time')
	app_data['installed_at'] = get_date_ts(app_data,device_data,get_ts)	

	print '*************Install**********************'

	###########		Initialize		############
	global receipt_id
	receipt_id='MIISnQYJKoZIhvcNAQcCoIISjjCCEooCAQExCzAJBgUrDgMCGgUAMIICPgYJKoZIhvcNAQcBoIICLwSCAisxggInMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMAwCAQsCAQEEBAICVdswDAIBDgIBAQQEAgIAjTANAgENAgEBBAUCAwHViDAOAgEBAgEBBAYCBBb9rvAwDgIBCQIBAQQGAgRQMjUzMA4CARACAQEEBgIEMaXRqzAPAgEDAgEBBAcMBTE0OTYyMBACAQ8CAQEECAIGSbf1FnvEMBACARMCAQEECAwGNS4xNC4wMBQCAQACAQEEDAwKUHJvZHVjdGlvbjAYAgEEAgECBBBaAVV7f4+DzUVASPFyo3FdMBwCAQUCAQEEFGasIyGtAcTrPkwv9IIxSd0aztfyMB4CAQgCAQEEFhYUMjAxOS0wOS0zMFQwOTo0MzowM1owHgIBDAIBAQQWFhQyMDE5LTA5LTMwVDA5OjQzOjAzWjAeAgESAgEBBBYWFDIwMTktMDEtMTdUMTI6NDI6NTdaMCgCAQICAQEEIAweanAuY28ucmVjcnVpdC5tdGwuYmVhdXR5LnNhbG9uMEcCAQYCAQEEP/4sq2E5/hvi8a0yWSMF2V6OYFH+tQShi2OI6C833nlW1g+HODnedoWiuRpmabyhYHaHtSxq5C48itoEw2ofETBNAgEHAgEBBEWcYHHi9e0U4xKFTvLlFZxacZw5WjS4ixDk9Gj94V1QnOEYrI+0OONpuq6vCddO/Xwhgly32Nlplnd9nmo/IGQaDSRI4TKggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAITWPL//WuQTFrdxm4aVQnJiXnCqsOWPxcpxOD52JDw7K7Endhjs89JDgBXPRaDy3wsG68zuF6LK009Zb9usajkQr7A5IMmq/popmfFGeAkhks9Qtpq7HSDUyESydb/tTSCWKfBuJRKjTZYbmni4d7HxgtWDxjPWrT/p22mmz2HCV99Mad2CnM95txGU4Qz8SWw9UPuT3tU7dgPi0lRtad/hKtEcpmMfdyS/LgvxTs+hiYl8upwjZI0a2vyOJFOqRD1oC3KipbiDIo0UVcTYh5Q/J214w/uJ1PRDybdZIBdX/HNqaz70jOmkQw45eaBGoOF6K8iXagQw49kjF6FZrYE='
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')

	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')	
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('registration'):
		app_data['registration']=False

	app_data['completeHair1st']=False

	app_data['completeNail1st']=False

	app_data['completeRelax1st']=False

	app_data['completeEyelash1st']=False

	app_data['completeBeauty1st']=False
		
	###########			Calls		############  
	set_sessionLength(app_data,forced=True,length=0) 

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	interval 		= random.randint(1,3)
	set_sessionLength(app_data,length=interval)
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)
	
	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,initiated_by='backend')
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,initiated_by='sdk')
	util.execute_request(**adjustSession)

	if random.randint(1,100)<=100:

		# Response of these selfcalls is 302 in original logs also

		print "\n\nlogincall--------------------------"
		r=login(app_data,campaign_data,device_data)
		output=util.execute_request(**r)

		print "\n\nregistercall--------------------------"
		r=memberregister(app_data,campaign_data,device_data)
		util.execute_request(**r)

		print "\n\nregistercall--1--------------------------"
		r=memberforward(app_data,campaign_data,device_data)
		util.execute_request(**r)
		app_data['registration']=True

	if app_data.get('registration')==True:
		time.sleep(random.randint(15,20))

		print "service_area--------------------------"
		r=area(app_data,campaign_data,device_data,area='service')
		output=util.execute_request(**r)

		try :
			if json.loads(output.get('data')).get('results').get('service_area'):
				scode=[]
				info2= json.loads(output.get('data')).get('results').get('service_area')
				if len(info2)==0:
						scode=['SA', 'SB', 'SC', 'SD', 'SE', 'SH', 'SF', 'SI', 'SG']	
						app_data['sarea'] =random.choice(scode)
				else:
					for i in range(0,len(info2)):				
						areacode=info2[i].get('code')
						scode.append(areacode)
					
					app_data['sarea'] = random.choice(scode)
					print scode
					print app_data['sarea']

			else:
				scode=['SA', 'SB', 'SC', 'SD', 'SE', 'SH', 'SF', 'SI', 'SG']	
				app_data['sarea'] =random.choice(scode)
		except :
			scode=['SA', 'SB', 'SC', 'SD', 'SE', 'SH', 'SF', 'SI', 'SG']	
			app_data['sarea'] =random.choice(scode)

		print "middle_area--------------------------"
		r=area(app_data,campaign_data,device_data,area='middle')
		output=util.execute_request(**r)

		try :
			if json.loads(output.get('data')).get('results').get('middle_area'):
				mcode=[]
				info2= json.loads(output.get('data')).get('results').get('middle_area')
				
				if len(info2)==0:
						mcode=['FA', 'FB', 'FC', 'FE', 'FF', 'FG']	
						app_data['marea'] =random.choice(mcode)
				else:
					for i in range(0,len(info2)):				
						areacode=info2[i].get('code')
						mcode.append(areacode)
				
					app_data['marea'] = random.choice(mcode)
					print mcode
					print app_data['marea']

			else:
				mcode=['FA', 'FB', 'FC', 'FE', 'FF', 'FG']	
				app_data['marea'] =random.choice(mcode)
		except :
			mcode=['FA', 'FB', 'FC', 'FE', 'FF', 'FG']	
			app_data['marea'] =random.choice(mcode)

		print "small_area--------------------------"
		r=area(app_data,campaign_data,device_data,area='small')
		output=util.execute_request(**r)

		try :
			if json.loads(output.get('data')).get('results').get('small_area'):
				smcode=[]
				info2= json.loads(output.get('data')).get('results').get('small_area')
				if len(info2)==0:
					smcode=['X462', 'X539', 'X558', 'X519', 'X559', 'X463', 'X560', 'X561', 'X464', 'X520']
			 		app_data['smarea'] =random.choice(smcode)
				else:
					for i in range(0,len(info2)):			
						areacode=info2[i].get('code')
						smcode.append(areacode)
				
					app_data['smarea'] = random.choice(smcode)
					print smcode
					print app_data['smarea']

			else:
				smcode=['X462', 'X539', 'X558', 'X519', 'X559', 'X463', 'X560', 'X561', 'X464', 'X520']
			 	app_data['smarea'] =random.choice(smcode)

		except :
			smcode=['X462', 'X539', 'X558', 'X519', 'X559', 'X463', 'X560', 'X561', 'X464', 'X520']
			app_data['smarea'] =random.choice(smcode)

		print "testsaloon--------------------------"
		r=testsaloon(app_data,campaign_data,device_data)
		output=util.execute_request(**r)
		try :
			if json.loads(output.get('data')).get('results').get('salon'):
				codeids=[]
				info2= json.loads(output.get('data')).get('results').get('salon')
				if len(info2)==0:
					codeids=['H000235624', 'H000420820', 'H000422002', 'H000171064', 'H000337960', 'H000108187', 'H000268632', 'H000313648', 'H000313866']
			 		app_data['codeid'] =random.choice(codeids)
				else:
					for i in range(0,len(info2)):			
						idcode=info2[i].get('id')
						codeids.append(idcode)
				
					app_data['codeid'] = random.choice(codeids)
					print codeids
					print app_data['codeid']

			else:
				codeids=['H000235624', 'H000420820', 'H000422002', 'H000171064', 'H000337960', 'H000108187', 'H000268632', 'H000313648', 'H000313866']
			 	app_data['codeid'] =random.choice(codeids)

		except :
			codeids=['H000235624', 'H000420820', 'H000422002', 'H000171064', 'H000337960', 'H000108187', 'H000268632', 'H000313648', 'H000313866']
			app_data['codeid']=random.choice(codeids)

		print "\n\ntesting--------------------------"
		r=reservecall(app_data,campaign_data,device_data)
		util.execute_request(**r)

		call_pattern(campaign_data,app_data,device_data,call_type='install')

	###########		Finalize	############
	set_appCloseTime(app_data)

	return {'status':True}

###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)
		get_ts=app_data.get('times').get('install_complete_time')
		app_data['installed_at'] = get_date_ts(app_data,device_data,get_ts)	
		time.sleep(random.randint(60,100))
	
	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')

	if not app_data.get('idfv_id'):
		if not device_data.get('idfv_id'):
			app_data['idfv_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfv_id'] = device_data.get('idfv_id')

	global receipt_id
	receipt_id='MIISnQYJKoZIhvcNAQcCoIISjjCCEooCAQExCzAJBgUrDgMCGgUAMIICPgYJKoZIhvcNAQcBoIICLwSCAisxggInMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMAwCAQsCAQEEBAICVdswDAIBDgIBAQQEAgIAjTANAgENAgEBBAUCAwHViDAOAgEBAgEBBAYCBBb9rvAwDgIBCQIBAQQGAgRQMjUzMA4CARACAQEEBgIEMaXRqzAPAgEDAgEBBAcMBTE0OTYyMBACAQ8CAQEECAIGSbf1FnvEMBACARMCAQEECAwGNS4xNC4wMBQCAQACAQEEDAwKUHJvZHVjdGlvbjAYAgEEAgECBBBaAVV7f4+DzUVASPFyo3FdMBwCAQUCAQEEFGasIyGtAcTrPkwv9IIxSd0aztfyMB4CAQgCAQEEFhYUMjAxOS0wOS0zMFQwOTo0MzowM1owHgIBDAIBAQQWFhQyMDE5LTA5LTMwVDA5OjQzOjAzWjAeAgESAgEBBBYWFDIwMTktMDEtMTdUMTI6NDI6NTdaMCgCAQICAQEEIAweanAuY28ucmVjcnVpdC5tdGwuYmVhdXR5LnNhbG9uMEcCAQYCAQEEP/4sq2E5/hvi8a0yWSMF2V6OYFH+tQShi2OI6C833nlW1g+HODnedoWiuRpmabyhYHaHtSxq5C48itoEw2ofETBNAgEHAgEBBEWcYHHi9e0U4xKFTvLlFZxacZw5WjS4ixDk9Gj94V1QnOEYrI+0OONpuq6vCddO/Xwhgly32Nlplnd9nmo/IGQaDSRI4TKggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAITWPL//WuQTFrdxm4aVQnJiXnCqsOWPxcpxOD52JDw7K7Endhjs89JDgBXPRaDy3wsG68zuF6LK009Zb9usajkQr7A5IMmq/popmfFGeAkhks9Qtpq7HSDUyESydb/tTSCWKfBuJRKjTZYbmni4d7HxgtWDxjPWrT/p22mmz2HCV99Mad2CnM95txGU4Qz8SWw9UPuT3tU7dgPi0lRtad/hKtEcpmMfdyS/LgvxTs+hiYl8upwjZI0a2vyOJFOqRD1oC3KipbiDIo0UVcTYh5Q/J214w/uJ1PRDybdZIBdX/HNqaz70jOmkQw45eaBGoOF6K8iXagQw49kjF6FZrYE='
	
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
	
	###########		Initialize		############

	if not app_data.get('registration'):
		app_data['registration']=False

	if not app_data.get('completeHair1st'):
		app_data['completeHair1st']=False

	if not app_data.get('completeNail1st'):
		app_data['completeNail1st']=False

	if not app_data.get('completeRelax1st'):
		app_data['completeRelax1st']=False

	if not app_data.get('completeEyelash1st'):
		app_data['completeEyelash1st']=False

	if not app_data.get('completeBeauty1st'):
		app_data['completeBeauty1st']=False

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,type='open')
	util.execute_request(**adjustSession)
	set_sessionLength(app_data,forced=True,length=0)

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,initiated_by='backend')
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,initiated_by='sdk')
	util.execute_request(**adjustSession)

	if app_data.get('registration')==False and random.randint(1,100)<=30:
		print "\n\nlogincall--------------------------"
		r=login(app_data,campaign_data,device_data)
		output=util.execute_request(**r)
		
		print "\n\nrefgistercall--------------------------"
		r=memberregister(app_data,campaign_data,device_data)
		util.execute_request(**r)

		print "\n\nrefgistercall--1--------------------------"
		r=memberforward(app_data,campaign_data,device_data)
		util.execute_request(**r)
		app_data['registration']=True

	if app_data.get('registration')==True:

		if not app_data.get('sarea'):
			print "service_area--------------------------"
			r=area(app_data,campaign_data,device_data,area='service')
			output=util.execute_request(**r)

			try :
				if json.loads(output.get('data')).get('results').get('service_area'):
					scode=[]
					info2= json.loads(output.get('data')).get('results').get('service_area')
					if len(info2)==0:
							scode=['SA', 'SB', 'SC', 'SD', 'SE', 'SH', 'SF', 'SI', 'SG']	
							app_data['sarea'] =random.choice(scode)
					else:
						for i in range(0,len(info2)):				
							areacode=info2[i].get('code')
							scode.append(areacode)
						
						app_data['sarea'] = random.choice(scode)
						print scode
						print app_data['sarea']

				else:
					scode=['SA', 'SB', 'SC', 'SD', 'SE', 'SH', 'SF', 'SI', 'SG']	
					app_data['sarea'] =random.choice(scode)
			except :
				scode=['SA', 'SB', 'SC', 'SD', 'SE', 'SH', 'SF', 'SI', 'SG']	
				app_data['sarea'] =random.choice(scode)

		if not app_data.get('marea'):
			print "middle_area--------------------------"
			r=area(app_data,campaign_data,device_data,area='middle')
			output=util.execute_request(**r)

			try :
				if json.loads(output.get('data')).get('results').get('middle_area'):
					mcode=[]
					info2= json.loads(output.get('data')).get('results').get('middle_area')
					
					if len(info2)==0:
							mcode=['FA', 'FB', 'FC', 'FE', 'FF', 'FG']	
							app_data['marea'] =random.choice(mcode)
					else:
						for i in range(0,len(info2)):				
							areacode=info2[i].get('code')
							mcode.append(areacode)
					
						app_data['marea'] = random.choice(mcode)
						print mcode
						print app_data['marea']

				else:
					mcode=['FA', 'FB', 'FC', 'FE', 'FF', 'FG']	
					app_data['marea'] =random.choice(mcode)
			except :
				mcode=['FA', 'FB', 'FC', 'FE', 'FF', 'FG']	
				app_data['marea'] =random.choice(mcode)

		if not app_data.get('smarea'):
			print "small_area--------------------------"
			r=area(app_data,campaign_data,device_data,area='small')
			output=util.execute_request(**r)

			try :
				if json.loads(output.get('data')).get('results').get('small_area'):
					smcode=[]
					info2= json.loads(output.get('data')).get('results').get('small_area')
					if len(info2)==0:
						smcode=['X462', 'X539', 'X558', 'X519', 'X559', 'X463', 'X560', 'X561', 'X464', 'X520']
				 		app_data['smarea'] =random.choice(smcode)
					else:
						for i in range(0,len(info2)):			
							areacode=info2[i].get('code')
							smcode.append(areacode)
					
						app_data['smarea'] = random.choice(smcode)
						print smcode
						print app_data['smarea']

				else:
					smcode=['X462', 'X539', 'X558', 'X519', 'X559', 'X463', 'X560', 'X561', 'X464', 'X520']
				 	app_data['smarea'] =random.choice(smcode)

			except :
				smcode=['X462', 'X539', 'X558', 'X519', 'X559', 'X463', 'X560', 'X561', 'X464', 'X520']
				app_data['smarea'] =random.choice(smcode)

		if not app_data.get('codeid'):
			print "testsaloon--------------------------"
			r=testsaloon(app_data,campaign_data,device_data)
			output=util.execute_request(**r)
			try :
				if json.loads(output.get('data')).get('results').get('salon'):
					codeids=[]
					info2= json.loads(output.get('data')).get('results').get('salon')
					if len(info2)==0:
						codeids=['H000235624', 'H000420820', 'H000422002', 'H000171064', 'H000337960', 'H000108187', 'H000268632', 'H000313648', 'H000313866']
				 		app_data['codeid'] =random.choice(codeids)
					else:
						for i in range(0,len(info2)):			
							idcode=info2[i].get('id')
							codeids.append(idcode)
					
						app_data['codeid'] = random.choice(codeids)
						print codeids
						print app_data['codeid']

				else:
					codeids=['H000235624', 'H000420820', 'H000422002', 'H000171064', 'H000337960', 'H000108187', 'H000268632', 'H000313648', 'H000313866']
				 	app_data['codeid'] =random.choice(codeids)

			except :
				codeids=['H000235624', 'H000420820', 'H000422002', 'H000171064', 'H000337960', 'H000108187', 'H000268632', 'H000313648', 'H000313866']
				app_data['codeid']=random.choice(codeids)

		call_pattern(campaign_data,app_data,device_data,call_type='open')

	return {'status':True}


def call_pattern(campaign_data, app_data, device_data,call_type='install'):

	event_list=['hair','nail','eyelash','relax','beauty']
	ch1=random.choice(event_list)
	ch2=random.choice(event_list)

	if ch1==ch2:
		while ch1==ch2:
			ch2=random.choice(event_list)
	
	event_list.remove(ch1)
	event_list.remove(ch2)
	ch3=random.choice(event_list)

	if call_type=='install':
		r1=15
		r2=25
	else:
		r1=25
		r2=45

	if random.randint(1,100)<=r1:			
		
		if ch1=='hair':
			if app_data.get('completeHair1st')==False:
				print '\n\n-----------------------Adjust Event Hair Ist----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'l6k9rv')
				util.execute_request(**adjustEvent)
				app_data['completeHair1st']=True
			
			else:
				print '\n\n-----------------------Adjust Event Hair After Ist----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'tuq8fb')
				util.execute_request(**adjustEvent)

		elif ch1=='nail':
			if app_data.get('completeNail1st')==False:
				print '\n\n-----------------------Adjust Complete Nail 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

				app_data['completeNail1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Nail After 1st----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

		elif ch1=='eyelash':
			if app_data.get('completeEyelash1st')==False:
				print '\n\n-----------------------Adjust Complete Eyelashes 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'xchlrm')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

				app_data['completeEyelash1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Eyelashes After 1st----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

		elif ch1=='relax':
			if app_data.get('completeRelax1st')==False:
				print '\n\n-----------------------Adjust Complete Relax 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'orp6bm')
				util.execute_request(**adjustEvent)

				app_data['completeRelax1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Relax After 1st----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

		else:
			if app_data.get('completeBeauty1st')==False:
				print '\n\n-----------------------Adjust Complete Beauty 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'5jrj0o')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'wtvh4h')
				util.execute_request(**adjustEvent)

				app_data['completeBeauty1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Beauty 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'5jrj0o')
				util.execute_request(**adjustEvent)


		if ch2=='hair':
			if app_data.get('completeHair1st')==False:
				print '\n\n-----------------------Adjust Event Hair Ist----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'l6k9rv')
				util.execute_request(**adjustEvent)
				app_data['completeHair1st']=True
			
			else:
				print '\n\n-----------------------Adjust Event Hair After Ist----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'tuq8fb')
				util.execute_request(**adjustEvent)

		elif ch2=='nail':
			if app_data.get('completeNail1st')==False:
				print '\n\n-----------------------Adjust Complete Nail 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

				app_data['completeNail1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Nail After 1st----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

		elif ch2=='eyelash':
			if app_data.get('completeEyelash1st')==False:
				print '\n\n-----------------------Adjust Complete Eyelashes 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'xchlrm')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

				app_data['completeEyelash1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Eyelashes After 1st----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

		elif ch2=='relax':
			if app_data.get('completeRelax1st')==False:
				print '\n\n-----------------------Adjust Complete Relax 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'orp6bm')
				util.execute_request(**adjustEvent)

				app_data['completeRelax1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Relax After 1st----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

		else:
			if app_data.get('completeBeauty1st')==False:
				print '\n\n-----------------------Adjust Complete Beauty 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'5jrj0o')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'wtvh4h')
				util.execute_request(**adjustEvent)

				app_data['completeBeauty1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Beauty 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'5jrj0o')
				util.execute_request(**adjustEvent)

	if random.randint(1,100)<=r2:

		if ch3=='hair':
			if app_data.get('completeHair1st')==False:
				print '\n\n-----------------------Adjust Event Hair Ist----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'l6k9rv')
				util.execute_request(**adjustEvent)
				app_data['completeHair1st']=True
			
			else:
				print '\n\n-----------------------Adjust Event Hair After Ist----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'tuq8fb')
				util.execute_request(**adjustEvent)

		elif ch3=='nail':
			if app_data.get('completeNail1st')==False:
				print '\n\n-----------------------Adjust Complete Nail 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

				app_data['completeNail1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Nail After 1st----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

		elif ch3=='eyelash':
			if app_data.get('completeEyelash1st')==False:
				print '\n\n-----------------------Adjust Complete Eyelashes 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'xchlrm')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

				app_data['completeEyelash1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Eyelashes After 1st----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

		elif ch3=='relax':
			if app_data.get('completeRelax1st')==False:
				print '\n\n-----------------------Adjust Complete Relax 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'orp6bm')
				util.execute_request(**adjustEvent)

				app_data['completeRelax1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Relax After 1st----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'mte2lo')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ufzlxk')
				util.execute_request(**adjustEvent)

		else:
			if app_data.get('completeBeauty1st')==False:
				print '\n\n-----------------------Adjust Complete Beauty 1st----------------------------'
				interval 		= random.randint(120,150)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'5jrj0o')
				util.execute_request(**adjustEvent)

				adjustEvent = adjust_event(campaign_data, app_data, device_data,'wtvh4h')
				util.execute_request(**adjustEvent)

				app_data['completeBeauty1st']=True

			else:
				print '\n\n-----------------------Adjust Complete Beauty 1st----------------------------'
				interval 		= random.randint(90,120)
				time.sleep(interval)
				set_sessionLength(app_data,length=interval)
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'5jrj0o')
				util.execute_request(**adjustEvent)


def memberregister(app_data,campaign_data,device_data):
	sc_vid(app_data)
	s='ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	
	sval2=''.join(random.choice(s) for i in range(32))
	url = 'https://beauty.hotpepper.jp/CSP/my/memberRegist/fromApp/'
	method = 'head'
	headers = {
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
		
		'User-Agent':device_data.get('User-Agent'),
		'Accept-Encoding':'gzip,deflate',
		
		'Cookie':'GalileoCookie='+util.get_random_string('all',24)+'; JSESSIONID='+sval2+'; HPB_SESSION_ID=a1600fe0bdad49329c89479732727c03;  BEAUTY_WEB_1='+util.get_random_string('hex',656),

	}

	params={
				'os_app':'ios_hair' ,
				 'ap':2 ,
				 'rtnapp':1, 
				 'sc_ap':1 ,
				 'sc_vid':app_data.get('sc_vid') ,
				 'fromLogin':1,
	}

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def area(app_data,campaign_data,device_data,area=''):
	s='ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	
	sval2=''.join(random.choice(s) for i in range(32))
	if area=='service':
		url = 'http://beauty.sda.hotpepper.jp/beauty/serviceArea/'
	elif area=='middle':
		url = 'http://beauty.sda.hotpepper.jp/beauty/middleArea/'
	else:
		url='http://beauty.sda.hotpepper.jp/beauty/smallArea/'
	method = 'get'
	headers = {
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
		
		'User-Agent':'ios_hair/<5.8.0>(OS 10.0.1;Model iPhone6,2;Build production)',#device_data.get('User-Agent'),
		'Accept-Encoding':'gzip,deflate',
		'gzip': 'Accept-Encoding',
		'Cookie':'GalileoCookie='+util.get_random_string('all',24)+'; JSESSIONID='+sval2,

	}

	params={
				'format':'json' ,
				 'key':'9ab2e68e5d6b0095' ,
				 
	}

	if area=='middle':
		params['service_area']=app_data.get('sarea')
	if area=='small':
		params['middle_area']=app_data.get('marea')

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def testsaloon(app_data,campaign_data,device_data,area=''):
	s='ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	
	sval2=''.join(random.choice(s) for i in range(32))
	
	url='http://beauty.sda.hotpepper.jp/beauty/abTestSalon/'
	method = 'get'
	headers = {
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
		
		'User-Agent':'ios_hair/<5.8.0>(OS 10.0.1;Model iPhone6,2;Build production)',#device_data.get('User-Agent'),
		'Accept-Encoding':'gzip,deflate',
		'gzip': 'Accept-Encoding',
		'Cookie':'GalileoCookie='+util.get_random_string('all',24)+'; JSESSIONID='+sval2,

	}
	stralpha='ABCDEFG'

	params={
				'format':'json', 
				 'seed':'40', 
				 'response_type':'1', 
				 'key':'9ab2e68e5d6b0095', 
				 'ab_test_pattern':random.choice(stralpha), 
				 'salon_search_sort_type':'1', 
				 'count':random.randint(1,15), 
				 'small_area':app_data.get('smarea'), 
				 'start':'1', 
				 'type':'tel'
				 
	}

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def login(app_data,campaign_data,device_data):
	sc_vid(app_data)

	url = 'https://beauty.hotpepper.jp/CSP/login/fromApp/'
	method = 'head'
	headers = {
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
		
		'User-Agent':device_data.get('User-Agent'),
		'Accept-Encoding':'gzip,deflate',
		
			}

	params={
				'os_app':'ios_hair' ,
				 'ap':2 ,
				 'rtnapp':1, 
				 'sc_ap':1 ,
				 'sc_vid':app_data.get('sc_vid'),
				 'appState':util.get_random_string('all',32),
				 'deviceId':str(uuid.uuid4()),
	}

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


def memberforward(app_data,campaign_data,device_data):
	sc_vid(app_data)
	s='ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	
	sval2=''.join(random.choice(s) for i in range(32))
	url = 'https://beauty.hotpepper.jp/CSP/capMemberForward/doAuth'
	method = 'head'
	headers = {
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
		
		'User-Agent':device_data.get('User-Agent'),
		'Accept-Encoding':'gzip,deflate',
		
		'Cookie':'GalileoCookie='+util.get_random_string('all',24)+'; JSESSIONID='+sval2+'; HPB_SESSION_ID=a1600fe0bdad49329c89479732727c03;  BEAUTY_WEB_1='+util.get_random_string('hex',656),

	}

	params={
				'os_app':'ios_hair' ,
				 'ap':2 ,
				 'sc_ap':1 ,
				 'sc_vid':app_data.get('sc_vid'),
				 'state':util.get_random_string('hex',40),
				 'code':util.get_random_string('hex',64),
	}

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def reservecall(app_data,campaign_data,device_data):
	set_sessionLength(app_data)
	def_sessionLength(app_data)
	s='ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	sval=''.join(random.choice(s) for i in range(16))
	sval2=''.join(random.choice(s) for i in range(32))
	url = 'https://beauty.hotpepper.jp/CSP/bt/reserve/doReserveValidate'
	method = 'post'
	headers = {
		
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		# 'X-Requested-With':'jp.hotpepper.android.beauty.hair',
		# 'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent':device_data.get('User-Agent'),
		'Accept-Encoding':'gzip,deflate',
		'Referer': 'https://beauty.hotpepper.jp/CSP/bt/reserve/confirm?storeId='+str(app_data.get('codeid')),
		'Origin': 'https://beauty.hotpepper.jp',
		# 'Upgrade-Insecure-Requests': '1',
		'Cookie':'GalileoCookie='+util.get_random_string('all',24)+'; HPB_GENRE_COMMON=beauty; s_fr=2018%3A07%3A20; s_cc=true; s_fid='+sval+'-'+sval+'; s_rsid=rcrthpbap1prd; sc_vid_ap=3c563a8ca8264f1eb9dcb801136a95b2; __utmc=6573601; __utmz=6573601.1532076170.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utma=6573601.92418845.1532076170.1532076170.1532084191.2; HPD_SP_ML_ID='+util.get_random_string('hex',80)+'; HPD_SP_AL_ID='+util.get_random_string('hex',80)+'; HPB_USER_INFO=%7B%22MEMBER_SEX_AGE_KBN%22%3A%22M1%22%7D; JSESSIONID='+sval2+'.ap214_003; HPB_RANDOM='+util.get_random_string('all',20)+'; HPB_SESSION_ID=33e7cd48b5314b2391b9a56f25d5c9a4; cap_id=4ffbc6ec7a19d846; s_cm=1; __utmt=1; BEAUTY_WEB_1='+util.get_random_string('hex',656)+'; s_nr_beauty=1532086458441-Repeat; __utmb=6573601.6.10.1532084191; s_sq=rcrthpbap1prd%3D%2526pid%253Dbt%25253Areserve%25253Aaw%25253Areserveinfoconf%2526pidt%253D1%2526oid%253D%2525E4%2525B8%25258A%2525E8%2525A8%252598%2525E3%252581%2525AB%2525E5%252590%25258C%2525E6%252584%25258F%2525E3%252581%2525AE%2525E4%2525B8%25258A%2525E4%2525BA%252588%2525E7%2525B4%252584%2525E3%252582%252592%2525E7%2525A2%2525BA%2525E5%2525AE%25259A%2525E3%252581%252599%2525E3%252582%25258B%2526oidt%253D3%2526ot%253DSUBMIT',

	}

	params={
		'storeId':str(app_data.get('codeid')),
	}

	data={

	'org.apache.struts.taglib.html.TOKEN':'fc46ba0e2a48c1798704e3a88b2bce91',
	'storeId':str(app_data.get('codeid')),
	'doReserveValidate':u'上記に同意の上予約を確定する'.encode('utf-8'),

	}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,type='install'):
	app_data['subsession_count'] = 1
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	params={}

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'connectivity_type':	'2',
			'created_at': get_date(app_data,device_data,early=random.uniform(1,3)),
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			# 'ios_uuid':	app_data.get('ios_uuid'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'sent_at': get_date(app_data,device_data),
			'session_count': make_session_count(app_data,device_data),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':app_data.get('installed_at'),
			'install_receipt':receipt_id,
			'os_build':	device_data.get('build'),
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'mcc': device_data.get('mcc'),
			'mnc': device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyLTE',
			# 'queue_size':'2',
			# 'tce':	1,
			}
	
	if type == "open":
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = app_data.get('sessionLength')
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = app_data.get('sessionLength')
	
	headers['Authorization']= get_auth(app_data,created_at=data['created_at'],idfa=data['idfa'],activity_kind='session')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adjust_sdkclick(campaign_data, app_data, device_data):
	set_sessionLength(app_data,forced=False,length=0)
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.pathname2url(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	params={}

	data = {
			'details': '{"Version3.1":{"iad-attribution":"false"}}',
			'source': 'iad3',
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': get_date(app_data,device_data,early=random.uniform(1,3)),
			'connectivity_type':	'2',
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			# 'ios_uuid':	app_data.get('ios_uuid'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'sent_at': get_date(app_data,device_data),
			'session_count':app_data.get('session_count'),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':app_data.get('installed_at'),
			'install_receipt': receipt_id,
			'os_build':	device_data.get('build'),
			# 'tce':0,
			'last_interval': 6,
			'session_length': app_data.get('sessionLength'),
			'subsession_count': 13,
			'time_spent' : app_data.get('sessionLength'),
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'mcc': device_data.get('mcc'),
			'mnc': device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyLTE',
			}

	headers['Authorization']= get_auth(app_data,created_at=data['created_at'],idfa=data['idfa'],activity_kind='click')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
def adjust_attribution(campaign_data, app_data, device_data,initiated_by=''):
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'User-Agent':urllib.pathname2url(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	data={}

	params = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink' : 1,
			# 'created_at': datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'created_at': get_date(app_data,device_data,early=random.uniform(4,4.5)),
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'needs_response_details': 1,
			# 'sent_at': datetime.datetime.utcfromtimestamp(time.time()+random.uniform(0,1)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'sent_at': get_date(app_data,device_data),
			'initiated_by':	initiated_by,
			'app_version_short':campaign_data.get('app_version_name'),
			'app_version': campaign_data.get('app_version_code'),
			'bundle_id':campaign_data.get('package_name'),
			'device_type':device_data.get('device_type'),
			'device_name':device_data.get('device_platform'),
			'os_name':'ios',
			'os_version':device_data.get('os_version'),
			'os_build':	device_data.get('build'),
			'persistent_ios_uuid':app_data.get('ios_uuid'),

			}
	
	headers['Authorization']= get_auth(app_data,created_at=params['created_at'],idfa=params['idfa'],activity_kind='attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	
def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=None,partner_params=None,purchase=False):
	
	def_(app_data,'subsession_count')	
		
	url = 'http://app.adjust.com/event'
	method = 'post'
	header = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}
			
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	data = {
			# 'queue_size':'2',
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			#"currency":	"USD",
			'hardware_name':device_data.get('hardware'),	
			'event_buffering_enabled':	0,
			'cpu_type':device_data.get('cpu_type'),
			'attribution_deeplink': '1',
			# 'ios_uuid':app_data.get('ios_uuid'),
			'os_name':'ios',
			'environment':'production',
			'needs_response_details':1,
			'event_count':app_data.get('event_count'),
			'time_spent':app_data.get('sessionLength')-random.randint(5,10),
			'session_count':str(app_data.get('session_count')),
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			# 'created_at': datetime.datetime.utcfromtimestamp((time.time()-0.04)-app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'created_at': get_date(app_data,device_data,early=random.uniform(1,3)),
			'connectivity_type':	'2',
			'event_token':event_token,
			'bundle_id':campaign_data.get('package_name'),
			'subsession_count':8,
			'os_version':device_data.get('os_version'),
			'app_version': campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country'),
			'language':	device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'session_length':app_data.get('sessionLength'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': '1',
			'device_name':device_data.get('device_platform'),
			# 'sent_at': datetime.datetime.utcfromtimestamp((time.time())-app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'sent_at': get_date(app_data,device_data),
			'os_build':	device_data.get('build'),
			# 'tce': '1',
			'install_receipt':receipt_id,
			'mcc': device_data.get('mcc'),
			'mnc': device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyLTE',
			}
			
	if callback_params:
		data['callback_params']	= callback_params
	if partner_params:
		data['partner_params']	= partner_params
	
	header['Authorization']= get_auth(app_data,created_at=data['created_at'],idfa=data['idfa'],activity_kind='event')
	
	return {'url': url, 'httpmethod': method, 'headers': header, 'params':None, 'data': data}	

	
###########################################################
#						UTIL							  #
###########################################################

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)
	
def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_auth(app_data,created_at,idfa,activity_kind):
	final_str=campaign_data.get('adjust').get('secret_key')+str(activity_kind)+str(idfa)+str(created_at)
	sign=util.sha256(final_str)
	auth='Signature secret_id="'+str(campaign_data.get('adjust').get('secret_id'))+'",signature="'+str(sign)+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'
	return auth

