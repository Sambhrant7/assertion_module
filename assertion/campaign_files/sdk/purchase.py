import random
import math

def isPurchase(app_data,day,advertiser_demand=5):
	isPurchaseDone = getFlag(app_data,"flag_isPurchaseDone")
	purchasePercentage = round(random.uniform(1,100),1)
	isOkToPurchase = checkPercentage(day,purchasePercentage,int(advertiser_demand))
	if not isPurchaseDone and isOkToPurchase:
		app_data['flag_isPurchaseDone'] = True
		return True
	return False
		
def initializeFlag(app_data,flagName):
	if not app_data.get(flagName):
		app_data[flagName]=False

def getFlag(app_data,flagName):
	initializeFlag(app_data,flagName)
	return app_data[flagName]

def checkPercentage(day,purchasePercentage,advertiser_demand):
	arr = getPurchasePercent(advertiser_demand)
	if day==1 and 0<purchasePercentage<=arr['day1']:
		return True
	if day==2 and 0<purchasePercentage<=arr['day2']:
		return True
	if day==3 and 0<purchasePercentage<=arr['day3']:
		return True
	if day==4 and 0<purchasePercentage<=arr['day4']:
		return True
	if day==5 and 0<purchasePercentage<=arr['day5']:
		return True
	if day==6 and 0<purchasePercentage<=arr['day6']:
		return True
	if day==7 and 0<purchasePercentage<=arr['day7']:
		return True
	if day>7 and 0<purchasePercentage<=5.0:
		return True
	return False

def getPurchasePercent(advertiser_demand):
	arr = {}
	if not advertiser_demand<5:
		arr['day1'] = (int(advertiser_demand))
		arr['day2'] = (int(advertiser_demand/2) - float(int(advertiser_demand/2))/10)
		arr['day3'] = (float(advertiser_demand)/2 - math.ceil(float(advertiser_demand)/2)/6)
		arr['day4'] = (math.ceil(float(advertiser_demand)/2))
		arr['day5'] = (int(advertiser_demand/2) + math.ceil(float(advertiser_demand)/2)/4)
		arr['day6'] = (math.ceil(float(advertiser_demand)/2)-float(advertiser_demand)/10 + float(advertiser_demand/2)/10 )
		arr['day7'] = (math.ceil(float(advertiser_demand)/2)+math.ceil(float(advertiser_demand)/4))
	else:
		arr['day1'] = 1.2
		arr['day2'] = 1.5
		arr['day3'] = 2
		arr['day4'] = 3
		arr['day5'] = 1.5
		arr['day6'] = 2
		arr['day7'] = 3
	return arr