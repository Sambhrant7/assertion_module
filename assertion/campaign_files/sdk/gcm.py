'''
Created on 14-May-2015

@author: root
'''

import random, string


def generate_gcm_id():
    return 'APA' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(201))