import sys,os
import random	
import util
import NameLists
from contextlib import closing
import requests
import traceback

# Disable
def block():
	sys.stdout = open(os.devnull, 'w')

# Restore
def enable():
	sys.stdout = sys.__stdout__

def timestamp():
	import time
	return time.time()

def registerUser(datastore,country='united states',emailPattern=1):
	gender 		= random.choice(['male','female'])
	first_names = NameLists.NAMES[country][gender]
	first_name 	= random.choice(first_names)
	last_names 	= NameLists.NAMES[country]['lastnames']
	last_name 	= random.choice(last_names)
	datastore['user'] = {}
	datastore['user']['firstname'] 	= first_name
	datastore['user']['lastname']  	= last_name
	datastore['user']['gender'] 	= gender

	if emailPattern==1:
		datastore['user']['email']		= first_name.lower()+random.choice(['_','.'])+last_name.lower()+str(random.randint(0,1000))+"@gmail.com"
	if emailPattern==2:
		datastore['user']['email']		= first_name.lower()+random.choice(['_','.'])+last_name.lower()+"@gmail.com"

def pingEmailID(email):	
	url 	= 'http://appamplify.com/hasoffer/email_verified/getemailrequest.php'
	method 	= 'post'
	header 	= {"Content-type":"application/x-www-form-urlencoded"}
	params 	= {}
	send_data = [('email_id',email)]
	request   = {'url':url, 'httpmethod':method, 'headers':header, 'params':None, 'data':send_data}
	return execute_request(**request)

def checkEmailIDStatus(email):	
	url 	= 'http://appamplify.com/hasoffer/email_verified/requestforresult.php'
	method 	= 'post'
	header 	= {"Content-type":"application/x-www-form-urlencoded"}	
	params 	= {}	
	send_data = [('email_id',email)]
	request   = {'url':url, 'httpmethod':method, 'headers':header, 'params':None, 'data':send_data}
	return util.execute_request(**request)

def createAndValidateEmailID(datastore,country='united states',emailPattern=1):
	registerUser(datastore,country,emailPattern)
	email = datastore['user']['email']
	enable()
	print "Checking for : "+email
	block()
	response = pingEmailID(email)
	responseStatus = int(response.get('status'))
	try :
		responseText = response.get('data')
	except:
		responseText = None
	
	if responseStatus==1 and (responseText == "Checking in another system for email in valid or not"):
		import time
		time.sleep(random.randint(10,20))
		response = checkEmailIDStatus(email)
		try : 
			responseCode = response.get('data')
		except:
			responseCode = None
		
		if(responseCode == '1'):
			return True
		if(responseCode == '2'):
			return False
		else:
			return False
	elif (responseText == "Email already in our database"):
		return True
	else:
		return False

def getAttemptsCount(totalAttempts):
	if totalAttempts<=5:
		return round(0.5*totalAttempts)
	else:
		return int(0.5*totalAttempts)

def getValidUser(country='united states',attempts=1):
	attemptsForEmailPattern2 = getAttemptsCount(attempts)
	while attempts:
		datastore = {}

		if attempts > attemptsForEmailPattern2:
			isValidMailID = createAndValidateEmailID(datastore,country,emailPattern=1)
		else:
			isValidMailID = createAndValidateEmailID(datastore,country,emailPattern=2)
		enable()
		if isValidMailID:
			print "\nValid email-id found :: "+datastore['user']['email']
			return datastore['user']
			break
		attempts -= 1
	print "\nAll attempts made, No valid email found"
	return datastore['user']

def execute_request(url, httpmethod, headers, params, data, log_response=True, auth=None, files=None):
	log_data = {'status':0, 'error':None}
	print ">>>%s %s\nParams:%s\nHeaders:%s\nData:%s" % (httpmethod, url, str(params), str(headers), str(data))
	r = None
	if httpmethod == 'post':
		try:
			with closing(requests.post(url, headers=headers, params=params, data=data, timeout=120, verify=False, auth=auth, files=files)) as r:
				# Do things with the response here.
				print 'done'
			log_data['data'] = r.text
			log_data['status'] = 1
		except:
			traceback.print_exc(file=sys.stdout)
			tb = traceback.format_exc().split('\n')
			log_data['error'] = tb[len(tb) - 2]

	if log_data['status'] == 1 and log_response:
		print "<<<%s Data:%s" % (str(r.status_code), log_data['data'].encode("utf-8"))
	elif log_data['status'] == 0:
		print "<<<Error:%s" % str((log_data['error']))
	log_data['res']=r
	return log_data