# -*- coding: utf8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


import random,time,datetime,uuid, json
from sdk import getsleep
from sdk import NameLists
from sdk import util,purchase,installtimenew
import urllib
import clicker,Config

campaign_data = {
	'package_name' : 'sa.com.easytaxi.Easy',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'ctr':6,
	'app_size':122.5,#121.3,
	'app_name' : 'Jeeny',
	'device_targeting' : True,
	'app_id':'1178701124',
	'app_version_code': '468',#'467',#'465',#'462',#'460',#'455',#'453',#'450',
	'app_version_name': '846',#'845',#'843',#'840',#'837',#'829',#'825',#'821',
	'supported_os':'9.0',
	'supported_countries': 'WW',   
	'sdk' : 'ios',
	'tracker': 'adjust',
	'adjust':{
				'app_token': 'b8cm285xz9j4',
				#'secret_key': '',
				'sdk': 'ios4.17.1',#'ios4.11.5',
				'app_updated_at': '2019-08-02T19:58:04.000Z',#'2019-07-09T01:58:50.000Z',#'2019-05-15T00:07:51.000Z',#'2019-03-19T15:56:30.000Z',#'2019-02-11T21:49:46.000Z',#'2019-01-11T22:35:25.000Z',#'2018-12-13T17:40:41.000Z',# '2018-10-24T21:40:10.000Z',
				#'secret_id':'',
			},
	'moengage' : {
				'X-MOENGAGE-APP-KEY': '34RYGOT9UGEZ1SIMHGU87S3H',
				'sdk_ver' : '4.3.1',
	},
	'mixpanel':{
					'token' : 'b0d4d7dfe3323922d10c4444e983f086',
					'version' : '1',
					"lib_version":"3.3.7",
					
			},
	'retention':{
					1:71,
					2:69,
					3:67,
					4:65,
					5:63,
					6:61,
					7:57,
					8:55,
					9:53,
					10:51,
					11:48,
					12:45,
					13:42,
					14:39,
					15:37,
					16:35,
					17:33,
					18:32,
					19:31,
					20:30,
					21:30,
					22:29,
					23:28,
					24:27,
					25:26,
					26:25,
					27:25,
					28:24,
					29:23,
					30:22,
					31:18,
					32:12,
					33:10,
					34:8,
					35:5,
	}
}


def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	
	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')
		
def make_subsession_count(app_data,device_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 1
	else:
		app_data['subsession_count'] += 1
		
	return app_data.get('subsession_count')

def get_install_reciept(app_data):

	return 'MIISpgYJKoZIhvcNAQcCoIISlzCCEpMCAQExCzAJBgUrDgMCGgUAMIICRwYJKoZIhvcNAQcBoIICOASCAjQxggIwMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMAwCAQ4CAQEEBAICAI0wDQIBAwIBAQQFDAM0NjcwDQIBDQIBAQQFAgMB1YgwDQIBEwIBAQQFDAM4MzcwDgIBAQIBAQQGAgRGQY1EMA4CAQkCAQEEBgIEUDI1MzAOAgELAgEBBAYCBAcUevQwDgIBEAIBAQQGAgQxlmGDMBACAQ8CAQEECAIGSbdSm5FfMBQCAQACAQEEDAwKUHJvZHVjdGlvbjAYAgEEAgECBBBumK4AyF4hEwuIdn3bRQSDMBwCAQUCAQEEFDac2V+JfGL+SE5zPH3f4eig6fIKMB4CAQICAQEEFgwUc2EuY29tLmVhc3l0YXhpLkVhc3kwHgIBCAIBAQQWFhQyMDE5LTA3LTI1VDA5OjQ2OjUxWjAeAgEMAgEBBBYWFDIwMTktMDctMjVUMDk6NDY6NTFaMB4CARICAQEEFhYUMjAxOS0wMi0xOVQwNToxODo1OFowTAIBBwIBAQRE+RYojCwimssWbWK9WxME3Swtl0MX/k6tB2tfHDdP+hsfqLJYG6TQSwNBqEoqkJeXZGDkcn2YmVHcLB69uPx+o/Ka48gwXgIBBgIBAQRWSGVn2lKGL5/9SIeV1dcIJb9oS1coyQYN5402mExoMQbn7Bkh3AbNTmpldcJa9TZODA7zma8bomNm0aztyYeSSbm+8E2b2hTV3C3K1BDy2PG630sL0eSggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAC+6SOTqmDEHE1CSMDcwWi2J/BGsK7r9nih+d+/Kp25M71TIwxL+8XRhEGVS44xI52THO0za24bpPofXg27XMfEQtCdMWeaTISUmMkbqbIG/ktY2nCpwRVVj18hG7A2uoVcOh2J4DG5r2hpkrY1gRNgGIZ2oeSYsN7HPXtXYfWzS90WnCvXlkwx61tRAug423tI/5puJMwSkdbFWIa5B02YEEYGbFA0sB3yc5norucHOZSdE3h+xVxxiCqsclu/EqPXa7ympK041lbpdrMKbCnKRmSfa37lP/+jWcYQhpLKFB9aufAhn3hMtnRv+MAUxB4+rZhDmSOGA8PWwHjDKEL4='
	

def install(app_data, device_data):

	print 'plz wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")

	app_data['account_create']=False
	app_data['first']=False
	app_data['Order_completed']=False
	app_data['Sign_out']=False
	app_data['log_in']=False

	time.sleep(random.randint(60,100))

	get_install_reciept(app_data)
	print '*************Install**********************'
	
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')

		
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')


	app_data['moengage_unique_id'] = app_data.get('idfv_id') + '-' + str(int(time.time()))

		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())


	register_user(app_data,country='united states')

	print 'lat and lon generator'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	try:
		json_lat_lon_result=json.loads(lat_lon_result.get('data')).get('geo')
		app_data['lat']=str(json_lat_lon_result.get('latitude'))
		app_data['lon']=str(json_lat_lon_result.get('longitude'))
		print app_data['lat']
		print app_data['lon']
	except:
		print "Exception"
		app_data['lat']= '77.318566'
		app_data['lon']= '28.5814742'

	print '\n----------------------- amazonaws   -----------------------------'
	request = amazonaws(campaign_data, app_data, device_data,url='https://s3.amazonaws.com/jeenyassets/live/api/url')  
	result=util.execute_request(**request)
	try:
		app_data['url']=result['data']
		print result['data']
		# time.sleep(5)
	except:
		app_data['url']='d20j8jpuehbhu5.cloudfront.net'


	print '\n----------------------- CLOUDFRONT   -----------------------------'
	request = cloudfront_application(campaign_data, app_data, device_data)  
	util.execute_request(**request)

	print '\n----------------------- amazonaws   -----------------------------'
	request = amazonaws(campaign_data, app_data, device_data,url='https://s3.amazonaws.com/jeenyassets/live/emaps/url')  
	util.execute_request(**request)


	
	print '\n----------------------- Moengage Device -----------------------------'
	request = moengage_device(campaign_data, app_data, device_data)  
	util.execute_request(**request)

	print '\n----------------------- Moengage Sdk Config -----------------------------'
	request = moengage_sdkconfig(campaign_data, app_data, device_data)  
	util.execute_request(**request)


	print '\n----------------------- Moengage Campaign -----------------------------'
	request = moengage_campaign(campaign_data, app_data, device_data)  
	util.execute_request(**request)


	print '\n----------------------- Moengage Categories -----------------------------'
	request = moengage_categories(campaign_data, app_data, device_data)  
	util.execute_request(**request)


	
	app_data['session_start_timestamp'] = int(time.time())
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data, t1 = 0, t2 = 2)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)


	time.sleep(random.randint(2,5))


	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data, t1 = 0, t2 = 0)
	util.execute_request(**adjustSession)

	print '\n-----------------------ADJUST Event------------- Dashboard_Presented ----------------'
	callback_params = json.dumps({"Dashboard_Presented":"bxwh61"})
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'bxwh61', callback_params = callback_params)
	util.execute_request(**adjust_event_req)

	print "device mixpanel"	
	device_mixpanel_2 = device_mixpanel(campaign_data,app_data,device_data)	
	util.execute_request(**device_mixpanel_2)
	time.sleep(random.randint(5,7))

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,t1 = 15, t2 = 20)
	util.execute_request(**adjustSession)

	time.sleep(random.randint(5,7))

	print "device mixpanel"	
	device_mixpanel_2 = device_mixpanel(campaign_data,app_data,device_data)	
	util.execute_request(**device_mixpanel_2)

	print '\n----------------------- Moengage Report -----------------------------'
	request = moengage_report(campaign_data, app_data, device_data)  
	util.execute_request(**request)

	print '\n----------------------- CLOUDFRONT  GEOLOCATION -----------------------------'
	request = cloudfront_geolocation(campaign_data, app_data, device_data)  
	response = util.execute_request(**request)

	try:
		app_data['city'] = json.loads(response.get('data')).get('name').encode('utf-8')
		app_data['code'] = json.loads(response.get('data')).get('code').encode('utf-8')
		print app_data['city']
		print app_data['code']
		# time.sleep(5)
	except:
		app_data['city'] = 'Riyadh'
		app_data['code']='RUH'

	time.sleep(random.randint(2,4))

	if random.randint(1,100)<=10:
		return {'status':True}


	print '\n-----------------------ADJUST Event-------------Log_in page----------------'
	callback_params = json.dumps({"device_imei": app_data.get('idfv_id',''),"locale": device_data.get('locale').get('language') ,"app_version": campaign_data.get('app_version_name') ,"deviceOS": device_data.get('os_version') ,"city": app_data.get('city') ,"device_type": device_data.get('device_platform')})
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'pjyq5k', callback_params = callback_params)
	util.execute_request(**adjust_event_req)


	time.sleep(random.randint(20,40))


	print '\n----------------------- Cloudfront Customer -----------------------------'
	request = cloudfront_customer(campaign_data, app_data, device_data)
	response = util.execute_request(**request)

	try:
		app_data['user_id'] = json.loads(response.get('data')).get('customer_id')
		print app_data['user_id']
	except:
		app_data['user_id'] = '5c4188b2c6385ddc67014759'


	time.sleep(random.randint(10,20))

	if random.randint(1,100)<=20:
		for i in range(0,random.randint(1,3)):
			time.sleep(random.randint(2,4))
			print '\n-----------------------ADJUST Event------------Make_an_order-----------------'

			callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"Initiate_Order":"mt9pd2","device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'mt9pd2', callback_params = callback_params)
			util.execute_request(**adjust_event_req)

			time.sleep(random.randint(1,3))

			print '\n-----------------------ADJUST Event---------------------Log_in page--------'
			callback_params = json.dumps({"device_imei":app_data.get('idfv_id',''),"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"device_type":device_data.get('device_platform')})
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'pjyq5k', callback_params = callback_params)
			util.execute_request(**adjust_event_req)

	if random.randint(1,100)<=70:
		time.sleep(random.randint(18,20))
		print '\n-----------------------ADJUST Event------------Create_Account-----------------'
		callback_params = json.dumps({"locale": device_data.get('locale').get('language') ,"app_version": campaign_data.get('app_version_name') ,"deviceOS": device_data.get('os_version') ,"city": app_data.get('city') ,"shop_country": device_data.get('locale').get('country') ,"user_id": app_data.get('user_id','') ,"device_type": device_data.get('device_platform') ,"device_imei": app_data.get('idfv_id','') })
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '83jdxr', callback_params = callback_params)
		util.execute_request(**adjust_event_req)
		time.sleep(random.randint(15,20))
		print '\n-----------------------ADJUST Event---------------------Mobile_number_verified--------'
		callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id',''),"facebook_import":"true"})
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '6glawa', callback_params = callback_params)
		util.execute_request(**adjust_event_req)
		app_data['account_create']=True


		if random.randint(1,100)<=50:
			dest=["المملكة العربية السعودية","العليا، الرياض","المملكة العربية ","المملكة العربية السعودية","الورود، الرياض ","الملك فهد،","طريق الأمير نايف بن عبدالعزيز","هشام بن عبدالملك","شارع هشام بن عبدالملك","ابي منصور البلوي، الملك فهد","طريق الملك عبدالله، الفرعي،، الرياض‎","شارع التخصصي، المحمدية، الرياض‎","طريق الملك فهد الفرعي، النخيل، الرياض‎","طريق الملك فهد الفرعي، الرياض‎","طريق الملك عبدالعزيز، الرياض‎"]

			time.sleep(random.randint(10,15))
			print '\n-----------------------ADJUST Event---------------------Ride_request--------'
			callback_params = json.dumps({"shop_country":device_data.get('locale').get('country'),"device_imei":app_data.get('idfv_id',''),"app_version":campaign_data.get('app_version_name'),"user_id":app_data.get('user_id',''),"locale":device_data.get('locale').get('language'),"device_type":device_data.get('device_platform'),"location":json.dumps({'latitude':app_data.get('lat'),'longitude':app_data.get('lon')}),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"payment_option":"money","destination":random.choice(dest).encode('utf-8'),"Ride_request":"cx0fac"},ensure_ascii=False)
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'cx0fac', callback_params = callback_params)
			util.execute_request(**adjust_event_req)


		if random.randint(1,100)<=50:
			for i in range(0,random.randint(1,3)):
				time.sleep(random.randint(4,8))
				print '\n-----------------------ADJUST Event------------Make_an_order-----------------'
				callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"Initiate_Order":"mt9pd2","device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
				adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'mt9pd2', callback_params = callback_params)
				util.execute_request(**adjust_event_req)

				time.sleep(random.randint(1,3))

				print '\n-----------------------ADJUST Event---------------------Order_page--------'
				callback_params = json.dumps({"Start_Order":"s1jh7n"})
				adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 's1jh7n', callback_params = callback_params)
				util.execute_request(**adjust_event_req)

	if random.randint(1,100)<=10 and app_data.get('account_create')==True:
		time.sleep(random.randint(4,6))
		print '\n-----------------------ADJUST Event---------------------Sign_out--------'
		callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'pjyq5k', callback_params = callback_params)
		util.execute_request(**adjust_event_req)
		app_data['Sign_out']==True

		if random.randint(1,100)<=50:
			time.sleep(random.randint(18,20))
			print '\n-----------------------ADJUST Event---------------------log_in--------'
			callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id',''),"facebook_import":"false"})
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '6glawa', callback_params = callback_params)
			util.execute_request(**adjust_event_req)
			app_data['log_in']==True


	
	app_data['session_end_timestamp'] = int(time.time())

		
	return {'status':True}


###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):

	if not app_data.get('times'):
		print 'plz wait...'
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")

	if not app_data.get('account_create'):
		app_data['account_create']=False

	if not app_data.get('first'):
		app_data['first']=False

	if not app_data.get('Order_completed'):
		app_data['Order_completed']=False

	if not app_data.get('Sign_out'):
		app_data['Sign_out']=False

	if not app_data.get('log_in'):
		app_data['log_in']=False

	time.sleep(random.randint(60,100))

	get_install_reciept(app_data)
	print '*************Install**********************'
	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')

	if not app_data.get('idfv_id'):	
		if not device_data.get('idfv_id'):
			app_data['idfv_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfv_id'] = device_data.get('idfv_id')

	if not app_data.get('moengage_unique_id'):	
		app_data['moengage_unique_id'] = app_data.get('idfv_id') + '-' + str(int(time.time()))

		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())


	register_user(app_data,country='united states')


	if not app_data.get('lat'):
		print 'lat and lon generator'
		lat_lon=current_location(device_data)
		lat_lon_result = util.execute_request(**lat_lon)
		try:
			json_lat_lon_result=json.loads(lat_lon_result.get('data')).get('geo')
			app_data['lat']=str(json_lat_lon_result.get('latitude'))
			app_data['lon']=str(json_lat_lon_result.get('longitude'))
			print app_data['lat']
			print app_data['lon']
		except:
			print "Exception"
			app_data['lat']= '77.318566'
			app_data['lon']= '28.5814742'

	if not app_data.get('url'):
		print '\n----------------------- amazonaws   -----------------------------'
		request = amazonaws(campaign_data, app_data, device_data,url='https://s3.amazonaws.com/jeenyassets/live/api/url')  
		result=util.execute_request(**request)
		try:
			app_data['url']=result['data']
			print result['data']
			# time.sleep(5)
		except:
			app_data['url']='d20j8jpuehbhu5.cloudfront.net'


	if not app_data.get('city'):
		print '\n----------------------- CLOUDFRONT  GEOLOCATION -----------------------------'
		request = cloudfront_geolocation(campaign_data, app_data, device_data)  
		response = util.execute_request(**request)

		try:
			app_data['city'] = json.loads(response.get('data')).get('name').encode('utf-8')
			app_data['code'] = json.loads(response.get('data')).get('code').encode('utf-8')
			print app_data['city']
			print app_data['code']
			# time.sleep(5)
		except:
			app_data['city'] = 'Riyadh'
			app_data['code']='RUH'

	if not app_data.get('user_id'):
		print '\n----------------------- Cloudfront Customer -----------------------------'
		request = cloudfront_customer(campaign_data, app_data, device_data)
		response = util.execute_request(**request)

		try:
			app_data['user_id'] = json.loads(response.get('data')).get('customer_id')
			print app_data['user_id']
		except:
			app_data['user_id'] = '5c4188b2c6385ddc67014759'

	if not app_data.get('session_start_timestamp'):
		app_data['session_start_timestamp'] = int(time.time())

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data, t1 = 0, t2 = 2,isopen=True)
	util.execute_request(**adjustSession)
	app_data['session_start_timestamp'] = int(time.time())

	print '\n-----------------------ADJUST Event------------- Dashboard_Presented ----------------'
	callback_params = json.dumps({"Dashboard_Presented":"bxwh61"})
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'bxwh61', callback_params = callback_params)
	util.execute_request(**adjust_event_req)

	if app_data.get('account_create')==False:
		time.sleep(random.randint(18,20))
		print '\n-----------------------ADJUST Event------------Create_Account-----------------'
		callback_params = json.dumps({"locale": device_data.get('locale').get('language') ,"app_version": campaign_data.get('app_version_name') ,"deviceOS": device_data.get('os_version') ,"city": app_data.get('city') ,"shop_country": device_data.get('locale').get('country') ,"user_id": app_data.get('user_id','') ,"device_type": device_data.get('device_platform') ,"device_imei": app_data.get('idfv_id','') })
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '83jdxr', callback_params = callback_params)
		util.execute_request(**adjust_event_req)
		
		time.sleep(random.randint(15,20))
		print '\n-----------------------ADJUST Event---------------------Mobile_number_verified--------'
		callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id',''),"facebook_import":"true"})
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '6glawa', callback_params = callback_params)
		util.execute_request(**adjust_event_req)
		app_data['account_create']=True

	if random.randint(1,100)<=20 and app_data.get('account_create')==True:
		dest=["المملكة العربية السعودية","العليا، الرياض","المملكة العربية ","المملكة العربية السعودية","الورود، الرياض ","الملك فهد،","طريق الأمير نايف بن عبدالعزيز","هشام بن عبدالملك","شارع هشام بن عبدالملك","ابي منصور البلوي، الملك فهد","طريق الملك عبدالله، الفرعي،، الرياض‎","شارع التخصصي، المحمدية، الرياض‎","طريق الملك فهد الفرعي، النخيل، الرياض‎","طريق الملك فهد الفرعي، الرياض‎","طريق الملك عبدالعزيز، الرياض‎"]

		time.sleep(random.randint(10,15))
		print '\n-----------------------ADJUST Event---------------------Ride_request--------'
		callback_params = json.dumps({"shop_country":device_data.get('locale').get('country'),"device_imei":app_data.get('idfv_id',''),"app_version":campaign_data.get('app_version_name'),"user_id":app_data.get('user_id',''),"locale":device_data.get('locale').get('language'),"device_type":device_data.get('device_platform'),"location":json.dumps({'latitude':app_data.get('lat'),'longitude':app_data.get('lon')}),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"payment_option":"money","destination":random.choice(dest).encode('utf-8'),"Ride_request":"cx0fac"},ensure_ascii=False)
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'cx0fac', callback_params = callback_params)
		util.execute_request(**adjust_event_req)


	if random.randint(1,100)<=50:
		for i in range(0,random.randint(1,3)):
			time.sleep(random.randint(4,8))
			print '\n-----------------------ADJUST Event------------Make_an_order-----------------'
			callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"Initiate_Order":"mt9pd2","device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'mt9pd2', callback_params = callback_params)
			util.execute_request(**adjust_event_req)

			time.sleep(random.randint(1,3))

			print '\n-----------------------ADJUST Event---------------------Order_page--------'
			callback_params = json.dumps({"Start_Order":"s1jh7n"})
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 's1jh7n', callback_params = callback_params)
			util.execute_request(**adjust_event_req)


			if random.randint(1,100)<=40:
				time.sleep(random.randint(50,60))
				print '\n-----------------------ADJUST Event---------------------Order_Submitted--------'
				callback_params = json.dumps({"Order_Submitted":"dm80f9"})
				adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'dm80f9', callback_params = callback_params)
				util.execute_request(**adjust_event_req)

				if app_data.get('first')==False:
					print '\n-----------------------ADJUST Event---------------------First_order--------'
					callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"First_order":"24q62t","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
					adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '24q62t', callback_params = callback_params)
					util.execute_request(**adjust_event_req)
					app_data['first']=True

				time.sleep(random.randint(18,20))

				print '\n-----------------------ADJUST Event---------------------Bids_Placed--------'
				callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"Bids_Placed":"ra04u5","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
				adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'ra04u5', callback_params = callback_params)
				util.execute_request(**adjust_event_req)

				time.sleep(random.randint(600,900))

				print '\n-----------------------ADJUST Event---------------------Order_PickedUp--------'
				callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"Order_PickedUp":"fp37bz","device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
				adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'fp37bz', callback_params = callback_params)
				util.execute_request(**adjust_event_req)

				time.sleep(random.randint(60,300))

				print '\n-----------------------ADJUST Event---------------------Order_completed--------'
				callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"Order_completed":"8nyqcc","device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
				adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '8nyqcc', callback_params = callback_params)
				util.execute_request(**adjust_event_req)
				app_data['Order_completed']=True

		if purchase.isPurchase(app_data,day,advertiser_demand=30) and app_data.get('Order_completed')==True:
			time.sleep(random.randint(200,250))
			print '\n-----------------------ADJUST Event---------------------Purchase_Completed--------'
			callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id',''),"Purchase_Completed":"stdre5"})
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'stdre5', callback_params = callback_params)
			util.execute_request(**adjust_event_req)

			time.sleep(random.randint(50,60))

			print '\n-----------------------ADJUST Event---------------------Completed_OrderNoDis--------'
			callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"Completed_OrderNoDis":"ayw1iu","deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'ayw1iu', callback_params = callback_params)
			util.execute_request(**adjust_event_req)

			time.sleep(random.randint(50,60))

			print '\n-----------------------ADJUST Event---------------------Completed_OrderDis--------'
			callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"Completed_OrderDis":"1zp0r7","shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '1zp0r7', callback_params = callback_params)
			util.execute_request(**adjust_event_req)


	if random.randint(1,100)<=10 and app_data.get('Sign_out')==True  and app_data.get('log_in')==False:
		time.sleep(random.randint(4,6))
		print '\n-----------------------ADJUST Event---------------------Sign_out--------'
		callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id','')})
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'pjyq5k', callback_params = callback_params)
		util.execute_request(**adjust_event_req)
		app_data['Sign_out']=True

	if random.randint(1,100)<=10 and app_data.get('Sign_out')==True:
		time.sleep(random.randint(18,20))
		print '\n-----------------------ADJUST Event---------------------log_in--------'
		callback_params = json.dumps({"locale":device_data.get('locale').get('language'),"app_version":campaign_data.get('app_version_name'),"deviceOS":device_data.get('os_version'),"city":app_data.get('city'),"shop_country":device_data.get('locale').get('country'),"user_id":app_data.get('user_id',''),"device_type":device_data.get('device_platform'),"device_imei":app_data.get('idfv_id',''),"facebook_import":"false"})
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '6glawa', callback_params = callback_params)
		util.execute_request(**adjust_event_req)

		app_data['log_in']=True






	app_data['session_end_timestamp'] = int(time.time())
	return {'status':True}



def cloudfront_geolocation(campaign_data, app_data, device_data):

	url = 'https://'+app_data.get('url')+'/area/by-geohash/ttnfx7e7xkzt/v2/US'

	method = 'get'

	headers = {
					'x-vendor' : 'Apple',
					'x-location' : app_data.get('lat')+','+app_data.get('lon'),
					'x-os' : device_data.get('os_version'),
					'x-model' : device_data.get('device_platform',''),
					'Accept-Language' : device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country') + ';q=1, ja-US;q=0.9, ru-US;q=0.8, ar-US;q=0.7, fr-US;q=0.6, pt-BR;q=0.5',
					'Accept-Encoding' : 'br, gzip, deflate',
					'x-user-agent' : 'ECIM-'+campaign_data.get('app_version_name'),
					'User-Agent' : device_data.get('User-Agent',''),
					'x-gps' :  app_data.get('lat')+','+app_data.get('lon'),
					'x-imei' : app_data.get('idfv_id'),
					'x-lang' : device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'),
					'x-geohash' : 'ttnfx7e7xkzt',
	}


	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data': None}

def current_location(device_data):
	url='http://lumtest.com/myip.json'
		
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}



def cloudfront_customer(campaign_data, app_data, device_data):

	register_user(app_data,country='united states')

	url = 'https://'+app_data.get('url')+'/customer'

	method = 'post'

	headers = {
					'x-vendor' : 'Apple',
					'x-area': app_data.get('code'),
					'x-location' : app_data.get('lat')+','+app_data.get('lon'),
					'x-os' : device_data.get('os_version'),
					'x-model' : device_data.get('device_platform',''),
					'Accept-Language' : device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country') + ';q=1, ja-US;q=0.9, ru-US;q=0.8, ar-US;q=0.7, fr-US;q=0.6, pt-BR;q=0.5',
					'Accept-Encoding' : 'br, gzip, deflate',
					'x-user-agent' : 'ECIM-'+campaign_data.get('app_version_name'),
					'User-Agent' : device_data.get('User-Agent',''),
					'x-gps' : app_data.get('lat')+','+app_data.get('lon'),
					'x-imei' : app_data.get('idfv_id'),
					'x-lang' : device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'),
					'x-geohash' : '7zzzzzzzzzzz',
					'Content-Type': 'application/json'
	}

	data = {
				"birthday": "",
				"country": device_data.get('locale').get('country'),
				"email": app_data.get('user').get('email'),
				"gender": "",
				"hash":  util.get_random_string('hex', 32),
				"imei":  app_data.get('idfv_id'),
				"is_pv": "1",
				"language": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'),
				"name": app_data.get('user').get('firstname') + ' ' + app_data.get('user').get('lastname'),
				"password": app_data.get('user').get('password'),
				"phone": app_data.get('user').get('phone'),
				"promo_code": "",
				"push_id": app_data.get('push_id'),
				"verify_phone": True,
			}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data': json.dumps(data)}



def cloudfront_application(campaign_data, app_data, device_data):

	url = 'https://'+app_data.get('url')+'/application/global'

	method = 'get'

	headers = {
					'x-vendor' : 'Apple',
					'x-location' : '0,0',
					'x-os' : device_data.get('os_version'),
					'x-model' : device_data.get('device_platform',''),
					'Accept-Language' : device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country') + ';q=1, ja-US;q=0.9, ru-US;q=0.8, ar-US;q=0.7, fr-US;q=0.6, pt-BR;q=0.5',
					'Accept-Encoding' : 'br, gzip, deflate',
					'x-user-agent' : 'ECIM-'+campaign_data.get('app_version_name'),
					'User-Agent' : device_data.get('User-Agent',''),
					'x-gps' : '0,0',
					'x-imei' : app_data.get('idfv_id'),
					'x-lang' : device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'),
					'x-geohash' : '7zzzzzzzzzzz',
	}

	params = {
					'imei' : app_data.get('idfv_id'),
					'push_id' : '',
	}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data': None}


def moengage_report(campaign_data, app_data, device_data):

	url = 'https://apiv2.moengage.com:443/v2/report/add/' + campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY')
	method = 'post'

	headers = {
					'X-MOENGAGE-APP-KEY' : campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
					'Accept-Language'  : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
					'Accept-Encoding'  : 'br, gzip, deflate',
					'User-Agent'  :  urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
					'Content-Type'  : 'application/json',
	}


	data = {
				"meta": {
							"bid": str(int(time.time()*1000)) + '-' + str(uuid.uuid4()).upper(),
						},
				"query_params": {
									"app_id": campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
									"device_tz_offset": str(app_data.get('sec','19800')) + '000',
									"device_unique_id": "P-" + app_data.get('moengage_unique_id'),
									"model_name": device_data.get('device'),
									"os": "iOS",
									"unique_id": app_data.get('moengage_unique_id'),
									"device_ts": str(int(time.time())),
									"os_ver":  device_data.get('os_version'),
									"app_ver": campaign_data.get('app_version_name'),
									"device_tz": device_data.get('local_tz_name'),
									"model": device_data.get('device_type'),
									"sdk_ver": campaign_data.get('moengage').get('sdk_ver'),
									"push_id" : app_data.get('push_id'),
								},
				"viewsCount"	: 6,
				"viewsInfo": 	[
									{
										"EVENT_G_TIME": str(int(time.time())),
										"EVENT_ACTION": "REINSTALL",
										"EVENT_L_TIME": datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									},
									{
										"EVENT_ACTION": "EVENT_ACTION_USER_ATTRIBUTE",
										"EVENT_L_TIME": datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
										"EVENT_ATTRS": {
											"ADVERTISING_IDENTIFIER": app_data.get('idfa_id'),
										},
										"EVENT_G_TIME": str(int(time.time())),
									},
									{
										"EVENT_ACTION": "EVENT_ACTION_DEVICE_ATTRIBUTE",
										"EVENT_L_TIME": datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
										"EVENT_ATTRS": {
											"ADVERTISING_IDENTIFIER": app_data.get('idfa_id'),
										},
										"EVENT_G_TIME": str(int(time.time())),
									},
									{
										"EVENT_G_TIME": str(int(time.time())),
										"EVENT_ACTION": "EVENT_ACTION_IOS_SESSION_START",
										"EVENT_L_TIME": datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									},
									{
										"EVENT_ACTION": "EVENT_ACTION_USER_ATTRIBUTE",
										"EVENT_L_TIME": datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
										"EVENT_ATTRS": {
											"PUSH_PREFERENCE": False,
										},
										"EVENT_G_TIME": str(int(time.time())),
									},
									{
										"EVENT_G_TIME": str(int(time.time())),
										"EVENT_ACTION": "EVENT_ACTION_IOS_SESSION_END",
										"EVENT_L_TIME": datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									},
									
									{
									"EVENT_ACTION":"EVENT_ACTION_USER_ATTRIBUTE",
									"EVENT_L_TIME":datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									"EVENT_ATTRS":{"PUSH_PREFERENCE":True},
									"EVENT_G_TIME":str(int(time.time()))},
									{
									"EVENT_G_TIME":str(int(time.time())),
									"EVENT_ACTION":"EVENT_ACTION_IOS_SESSION_START",
									"EVENT_L_TIME":datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									},
									{
									"EVENT_ACTION":"EVENT_ACTION_USER_ATTRIBUTE",
									"EVENT_L_TIME":datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									"EVENT_ATTRS":{"PUSH_PREFERENCE":False	},
									"EVENT_G_TIME":str(int(time.time()))
									},
									{"EVENT_ACTION":"EVENT_ACTION_DEVICE_ATTRIBUTE",
									"EVENT_L_TIME":datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									"EVENT_ATTRS":{"device_model":device_data.get('device_type')},
									"EVENT_G_TIME":str(int(time.time()))
									},
									{
									"EVENT_ACTION":"EVENT_ACTION_DEVICE_ATTRIBUTE",
									"EVENT_L_TIME":datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									"EVENT_ATTRS":{"device_model_name":device_data.get('device_type')},
									"EVENT_G_TIME":str(int(time.time()))
									},
									{
									"EVENT_ACTION":"EVENT_ACTION_DEVICE_ATTRIBUTE",
									"EVENT_L_TIME":datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									"EVENT_ATTRS":{"device_IDFV":app_data.get('idfv_id')},
									"EVENT_G_TIME":str(int(time.time()))
									},
									{
									"EVENT_ACTION":"EVENT_ACTION_DEVICE_ATTRIBUTE",
									"EVENT_L_TIME":datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									"EVENT_ATTRS":{"os_version":device_data.get('os_version')},
									"EVENT_G_TIME":str(int(time.time()))
									},
									{
									"EVENT_ACTION":"Dashboard_Presented",
									"EVENT_L_TIME":datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%d:%m:%Y:%H:%M:%S"),
									"EVENT_ATTRS":{"device_imei":app_data.get('idfv_id'),
									"locale":device_data.get('locale').get('language'),
									"app_version":campaign_data.get('app_version_name'),
									"deviceOS":device_data.get('os_version'),
									"device_type":device_data.get('device_platform')},
									"EVENT_G_TIME":str(int(time.time()))
									},
								]
			}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data': json.dumps(data)}



def moengage_device(campaign_data, app_data, device_data):

	url = 'https://apiv2.moengage.com:443/v2/device/add/' + campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY')

	method = 'post'

	headers = {
					'X-MOENGAGE-APP-KEY' : campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
					'Accept-Language'  : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
					'Accept-Encoding'  : 'br, gzip, deflate',
					'User-Agent'  :  urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
					'Content-Type'  : 'application/json',
	}


	app_data['push_id'] = util.get_random_string('hex',64)


	data = {
				"ADVERTISING_IDENTIFIER" : app_data.get('idfa_id'),
				"IDFV" : app_data.get('idfv_id'),
				"meta": {
							"dev_pref" : {
											"e_t_p" : True,
											"in_app_p" : True,
											"push_p" : True,
							}
						},
				"query_params": {
									"app_id": campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
									"device_tz_offset": str(app_data.get('sec','19800')) + '000',
									"device_unique_id": "P-" + app_data.get('moengage_unique_id'),
									"model_name": device_data.get('device'),
									"os": "iOS",
									"unique_id": app_data.get('moengage_unique_id'),
									"device_ts": str(int(time.time())),
									"os_ver":  device_data.get('os_version'),
									"app_ver": campaign_data.get('app_version_name'),
									"device_tz": device_data.get('local_tz_name'),
									"model": device_data.get('device_type'),
									"sdk_ver": campaign_data.get('moengage').get('sdk_ver'),
									"push_id" : app_data.get('push_id'),
								},
			}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data': json.dumps(data)}



def moengage_campaign(campaign_data, app_data, device_data):

	url = 'https://apiv2.moengage.com:443/campaigns/inappcampaigns/fetch'

	method = 'post'

	headers = {
					'X-MOENGAGE-APP-KEY' : campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
					'Accept-Language'  : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
					'Accept-Encoding'  : 'br, gzip, deflate',
					'User-Agent'  :  urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
					'Content-Type'  : 'application/json',
	}


	data = {
				"campaign_ids" : [],
				"query_params": {
									"app_id": campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
									"device_tz_offset": str(app_data.get('sec','19800')) + '000',
									"device_unique_id": "P-" + app_data.get('moengage_unique_id'),
									"model_name": device_data.get('device'),
									"last_updated":"0000",
									"os": "iOS",
									"unique_id": app_data.get('moengage_unique_id'),
									"device_ts": str(int(time.time())),
									"os_ver":  device_data.get('os_version'),
									"app_ver": campaign_data.get('app_version_name'),
									"device_tz": device_data.get('local_tz_name'),
									"model": device_data.get('device_type'),
									"sdk_ver": campaign_data.get('moengage').get('sdk_ver'),
								},
			}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data': json.dumps(data)}



def moengage_categories(campaign_data, app_data, device_data):

	url = 'https://apiv2.moengage.com:443/v2/categories/add'

	method = 'post'

	headers = {
					'X-MOENGAGE-APP-KEY' : campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
					'Accept-Language'  : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
					'Accept-Encoding'  : 'br, gzip, deflate',
					'User-Agent'  :  urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
					'Content-Type'  : 'application/json',
	}


	data = {
				"data" : [{
								"actions" : ["Dismiss"],
								"actionsIdentifier" : ["MOE_DISMISS_ACTION"],
								"name"  :  "MOE_DISMISS_CATEGORY",
				}],
				"query_params": {
									"app_id": campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
									"device_tz_offset": str(app_data.get('sec','19800')) + '000',
									"device_unique_id": "P-" + app_data.get('moengage_unique_id'),
									"model_name": device_data.get('device'),
									#"last_updated":"0000",
									"os": "iOS",
									"unique_id": app_data.get('moengage_unique_id'),
									"device_ts": str(int(time.time())),
									"os_ver":  device_data.get('os_version'),
									"app_ver": campaign_data.get('app_version_name'),
									"device_tz": device_data.get('local_tz_name'),
									"model": device_data.get('device_type'),
									"sdk_ver": campaign_data.get('moengage').get('sdk_ver'),
								},
			}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data': json.dumps(data)}



def moengage_sdkconfig(campaign_data, app_data, device_data):

	url = 'https://apiv2.moengage.com:443/v3/sdkconfig/ios/' + campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY')

	method = 'post'

	headers = {
					'X-MOENGAGE-APP-KEY' : campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
					'Accept-Language'  : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
					'Accept-Encoding'  : 'br, gzip, deflate',
					'User-Agent'  :  urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
					'Content-Type'  : 'application/json',
	}


	data = {
				"query_params": {
									"app_id": campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
									"device_tz_offset": str(app_data.get('sec','19800')) + '000',
									"device_unique_id": "P-" + app_data.get('moengage_unique_id'),
									"model_name": device_data.get('device'),
									#"last_updated":"0000",
									"os": "iOS",
									"unique_id": app_data.get('moengage_unique_id'),
									"device_ts": str(int(time.time())),
									"os_ver":  device_data.get('os_version'),
									"app_ver": campaign_data.get('app_version_name'),
									"device_tz": device_data.get('local_tz_name'),
									"model": device_data.get('device_type'),
									"sdk_ver": campaign_data.get('moengage').get('sdk_ver'),
									# "push_id" : app_data.get('push_id'),
								},
			}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data': json.dumps(data)}

def amazonaws(campaign_data, app_data, device_data,url=''):

	url = url

	method = 'get'

	headers = {
					# 'X-MOENGAGE-APP-KEY' : campaign_data.get('moengage').get('X-MOENGAGE-APP-KEY'),
					'Accept-Language'  : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
					'Accept-Encoding'  : 'br, gzip, deflate',
					'User-Agent'  :  urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
					# 'Content-Type'  : 'application/json',
					'Accept': '*/*'
	}


	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data': None}


###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,t1 = 0, t2 = 0,isopen=False):


	app_data['created_at'] = get_date(app_data,device_data)
	time.sleep(random.randint(t1, t2))
	sent_at = get_date(app_data,device_data)
	
	
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}

	data = {
			 'app_token': campaign_data.get('adjust').get('app_token'),
			 'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			 'app_version':	campaign_data.get('app_version_code'),
			 'app_version_short': campaign_data.get('app_version_name'),
			 'attribution_deeplink':	1,
			 'bundle_id': campaign_data.get('package_name'),
			 'country':device_data.get('locale').get('country').upper(),
			 'cpu_type':	device_data.get('cpu_type'),
			 'created_at': app_data.get('created_at'),
			 'device_name': device_data.get('device_platform'),
			 'device_type': device_data.get('device_type'),
			 'environment': 'production',
			 'event_buffering_enabled': 0,
			 'hardware_name': device_data.get('hardware'),
			 'idfa': app_data.get('idfa_id'),
			 'idfv':	app_data.get('idfv_id'),
			 'install_receipt':get_install_reciept(app_data),
			 'installed_at' : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
			 'language':	device_data.get('locale').get('language'),
			 'needs_response_details': 1,
			 'os_build':	device_data.get('build'),
			 'os_name': 'ios',
			 'os_version': device_data.get('os_version'),
			 'persistent_ios_uuid': app_data.get('ios_uuid'),
			 'sent_at': sent_at,
			 'session_count': make_session_count(app_data,device_data),
			 'tracking_enabled':	1,
			}

	if isopen:
		if app_data.get('session_end_timestamp'):
			data['session_length'] 	= app_data.get('session_end_timestamp')-app_data.get('session_start_timestamp')
			data['time_spent'] 		= app_data.get('session_end_timestamp')-app_data.get('session_start_timestamp')
			data['last_interval'] 	= int(time.time())-app_data.get('session_end_timestamp')
		else:
			data['session_length'] 	= 0
			data['time_spent'] 		=0
			data['last_interval']=0

		def_(app_data,'subsession_count')
		data['created_at'] 		=app_data.get('created_at')
		
		
		data['subsession_count']= app_data.get('subsession_count')

		
	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	



def adjust_sdkclick(campaign_data, app_data, device_data,t1 = 0, t2 = 0,callback_params=False,partner_params=False):

	created_at = get_date(app_data,device_data)
	time.sleep(random.randint(t1, t2))
	sent_at = get_date(app_data,device_data)
	
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	
	params={}

	data = {
			  'app_token': campaign_data.get('adjust').get('app_token'),
			  'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			  'app_version':	campaign_data.get('app_version_code'),
			  'app_version_short': campaign_data.get('app_version_name'),
			  'attribution_deeplink':	1,
			  'bundle_id': campaign_data.get('package_name'),
			 # 'connectivity_type' : 2,
			  'country':device_data.get('locale').get('country').upper(),
			  'cpu_type':	device_data.get('cpu_type'),
			  'created_at': created_at,
			  'details': json.dumps({"Version3.1":{"iad-attribution":"false"}}),
			  'device_name': device_data.get('device_platform'),
			  'device_type': device_data.get('device_type'),
			  'environment': 'production',
			  'event_buffering_enabled': 0,
			  'hardware_name': device_data.get('hardware'),
			  'idfa': app_data.get('idfa_id'),
			  'idfv':	app_data.get('idfv_id'),
			  'install_receipt':get_install_reciept(app_data),
			  'installed_at' : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
			  'language':	device_data.get('locale').get('language'),
			  'last_interval' : 2,
			  'needs_response_details': 1,
			  'os_build':	device_data.get('build'),
			  'os_name': 'ios',
			  'os_version': device_data.get('os_version'),
			  'persistent_ios_uuid': app_data.get('ios_uuid'),
			  'sent_at': sent_at,
			  'session_count':str(app_data.get('session_count')),
			  'source': 'iad3',
			  'tracking_enabled': '1',
			  'session_length' : int(time.time()) - app_data.get('session_start_timestamp'),
			  'time_spent' : int(time.time()) - app_data.get('session_start_timestamp'),
			  'subsession_count'  :  random.randint(2,3),
			}

	
	
	#headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'click')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	

def adjust_attribution(campaign_data, app_data, device_data,t1 = 0, t2 = 0):

	created_at = get_date(app_data,device_data)
	time.sleep(random.randint(t1, t2))
	sent_at = get_date(app_data,device_data)

	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	
	data={}

	params = {       "app_token": campaign_data.get('adjust').get('app_token'),
        "app_version": campaign_data.get('app_version_code'),
        "app_version_short": campaign_data.get('app_version_name'),
        "attribution_deeplink": "1",
        "bundle_id": campaign_data.get('package_name'),
        "created_at": created_at,
        "device_name": device_data.get('device_platform'),
        "device_type": "iPhone",
        "environment": "production",
        "event_buffering_enabled": "0",
        "idfa": app_data.get('idfa_id'),
        "idfv": app_data.get('idfv_id'),
        "initiated_by": "sdk",
        "needs_response_details": "1",
        "os_build": device_data.get('build'),
        "os_name": "ios",
        "os_version": device_data.get('os_version'),
        "persistent_ios_uuid": app_data.get('ios_uuid'),
        "sent_at": sent_at,
        }

	#headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('idfa'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	


def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=False, partner_params=False,t1=0,t2=0):
	
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	inc_(app_data, 'event_count')	
		
	url = 'http://app.adjust.com/event'
	
	headers = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}

	data = {
			  'app_token':campaign_data.get('adjust').get('app_token'),
			  'app_version':	campaign_data.get('app_version_code'),
			  'app_version_short':campaign_data.get('app_version_name'),
			  'attribution_deeplink': 1,
			  'bundle_id':campaign_data.get('package_name'),
			 # 'connectivity_type' : 2,
			  'country':device_data.get('locale').get('country'),
			  'cpu_type':device_data.get('cpu_type'),
			  'created_at': created_at,
			  'device_name':device_data.get('device_platform'),
			  'device_type':device_data.get('device_type'),
			  'environment':'production',
			  'event_buffering_enabled':	0,
			  'event_count':app_data.get('event_count'),
			  'event_token':event_token,
			  'hardware_name': device_data.get('hardware'),	
			  'idfa':app_data.get('idfa_id'),
			  'idfv':app_data.get('idfv_id'),
			  'install_receipt':get_install_reciept(app_data),
			  'language':	device_data.get('locale').get('language'),
			  'needs_response_details':1,
			  'os_build':	device_data.get('build'),
			  'os_name':'ios',
			  'os_version':device_data.get('os_version'),
			  'persistent_ios_uuid':app_data.get('ios_uuid'),
			  'sent_at': sent_at,
			  'session_count':str(app_data.get('session_count')),
			  'tracking_enabled': '1',
			  'subsession_count': random.randint(3,6),
			}

	data['session_length']= int(time.time()) - app_data.get('session_start_timestamp')
	data['time_spent'] = int(time.time()) - app_data.get('session_start_timestamp')

	
	if partner_params:
		data['partner_params']=partner_params	

	if callback_params:
		data['callback_params']=callback_params	
		
	#headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'event')

	return {'url': url, 'httpmethod': 'post', 'headers': headers, 'params':None, 'data': data}	


def adjust_sdkinfo(campaign_data, app_data, device_data):
	pushtoken(app_data)
	url = 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',

# 'Accept': '*/*',
		'User-Agent':urllib.pathname2url(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}

	params={}

	data = {

		'app_token': campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink' : 1,
# 'created_at': datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		'created_at': get_date(app_data,device_data),
		'environment' : 'production',
		'event_buffering_enabled': 0,
		'idfa': app_data.get('idfa_id'),
		'idfv':	app_data.get('idfv_id'),
		#'initiated_by':'backend',
		'needs_response_details': 1,
# 'sent_at': datetime.datetime.utcfromtimestamp(time.time()+random.uniform(0,1)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		'sent_at': get_date(app_data,device_data),
		'persistent_ios_uuid': app_data.get('ios_uuid'),
		'push_token':app_data.get('pushtoken'),
		'source' :'push'
	}
	headers['Authorization']=headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


def device_mixpanel(campaign_data,app_data,device_data):
	url = 'http://decide.mixpanel.com/decide'
	method = 'head'
	headers = {
		'User-Agent' : urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Encoding': 'gzip',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept': '*/*'
	}
	params = {
		'distinct_id': app_data.get('idfa_id') ,
		'lib': 'iphone',
		'token': campaign_data.get('mixpanel').get('token'),
		'version': '1',
		'properties':json.dumps({"$ios_version":device_data.get('os_version'),"$ios_lib_version":campaign_data.get('mixpanel').get('lib_version'),"$ios_app_version":campaign_data.get('app_version_name'),"$ios_app_release":campaign_data.get('app_version_code'),"$ios_device_model":device_data.get('device_platform'),"$ios_ifa":app_data.get('idfa_id')})
	}
	data = {}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}


###########################################################
#						UTIL							  #
###########################################################

def make_distinct_id(app_data):
	app_data['distinct_id'] = str(uuid.uuid4())
	return app_data.get('distinct_id')

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	# st = str(int(time.time()*1000))
	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

	
def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)

def get_date_by_ts(app_data,device_data,ts=timestamp()):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('hex',64)

def device_id(app_data):
	if not app_data.get('x-device-id'):
		app_data['x-device-id']=str(uuid.uuid4()).upper()

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		# app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		# app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['phone'] 		="+971"+'-50-'+util.get_random_string('decimal',7)
		app_data['user']['password'] 	= app_data['user']['lastname']+random.choice(["@","!"])+app_data['user']['dob'].replace("-","") 

def get_auth(device_data,app_data,created_at,idfa,activity_type):
	data1= str(campaign_data['adjust']['app_secret']+activity_type+idfa+created_at)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('signature_id')+'",signature="'+sign+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'	
