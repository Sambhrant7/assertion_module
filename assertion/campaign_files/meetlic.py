import random,time,datetime,uuid,urlparse
from sdk import util,purchase,installtimenew
from sdk import getsleep
import urllib
import clicker,Config

campaign_data = {
	'package_name' : 'net.ilius.android.meetic',
	'app_name'	   : 'Meetic',
	'app_version_name' : '5.8.2',#'5.7.2',#5.7.0, '5.6.2',#5.4.2, 5.3.1, '5.2.1',#'5.2.0',#5.1.5, '5.0.5',#5.0.3, '5.0.2',#'4.3.9',#'4.3.3',#'4.3.1',#'4.2.0',#'4.1.1',#4.1.0, '4.0.2',#'3.34.1',#'3.33.5',#'3.33.4',#'3.32.4',#'3.31.5',# '3.30.1',#'3.25.3',4.0.1
	'no_referrer': True,
	'app_size' : 27.0, #30.0, 28.0
	'ctr'		 : 6,
	'device_targeting':True,
	'tracker':'adjust',
	'adjust':
				{
					'app_token': 'pcyjyrqbppbe',
					'sdk': 'android4.17.0',#'android4.12.1',#'android4.11.0',
				},
	'supported_countries': 'WW',
	'device_targeting':True,
	'supported_os': '5.0',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention':{
					1:65,
					2:63,
					3:60,
					4:58,
					5:55,
					6:53,
					7:50,
					8:48,
					9:45,
					10:43,
					11:40,
					12:38,
					13:35,
					14:32,
					15:30,
					16:28,
					17:26,
					18:23,
					19:21,
					20:20,
					21:19,
					22:18,
					23:17,
					24:16,
					25:15,
					26:14,
					27:13,
					28:12,
					29:11,
					30:10,
					31:9,
					32:8,
					33:7,
					34:6,
					35:5,
	}
}


###########################################################
#														  #
#						INSTALL							  #
#														  #
###########################################################
def install(app_data, device_data):

	print '*************Install**********************'
	###########		Initialize		############
	print 'please wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

	app_data['pattern_flag']=False
	app_data['Registration_flag']=False
	app_data['upload_picture_flag']=False
	app_data['main_screen_flag']=False

	set_sessionLength(app_data,forced=True,length=0)
	
	###########			Calls		############
	app_data['adjust_call_time']=int(time.time())
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)
	time.sleep(random.randint(1,3))

	app_data['adjust_call_time']=int(time.time())
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdk_click(campaign_data, app_data, device_data,'reftag')
	util.execute_request(**adjustSession)

	print "RELAX WAIT !!!! SLEEP IS RUNNING"
	time.sleep(random.randint(2,7))
	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data, initiated_by = "backend")
	util.execute_request(**adjustSession)

	time.sleep(random.randint(2,7))
	app_data['adjust_call_time']=int(time.time())
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdk_click(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	if random.randint(1,100)<=90:
		call_pattern(campaign_data, app_data, device_data)

	set_appCloseTime(app_data)
	return {'status':True}

###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):
	
	print '*************OPEN**********************'

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

	if not app_data.get('pattern_flag'):
		app_data['pattern_flag']=False

	if not app_data.get('Registration_flag'):
		app_data['Registration_flag']=False

	if not app_data.get('upload_picture_flag'):
		app_data['upload_picture_flag']=False

	if not app_data.get('main_screen_flag'):
		app_data['main_screen_flag']=False

	###########			Calls		############
	set_sessionLength(app_data,forced=True,length=random.randint(350,1000))
	app_data['adjust_call_time']=int(time.time())
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,'open')
	util.execute_request(**adjustSession)

	set_sessionLength(app_data,forced=True,length=0)

	call_pattern(campaign_data, app_data, device_data)
	
	if purchase.isPurchase(app_data,day) and app_data.get('main_screen_flag')==True:

		time.sleep(random.randint(10,30))
		print "\n----------------------------ADJUST PURCHASE------------------------------------"
		adjustevent = adjust_event(campaign_data, app_data, device_data,"hvq872",True)
		util.execute_request(**adjustevent)
		time.sleep(random.randint(2,5))
		
		for i in range(2):
			time.sleep(random.randint(2,5))
			print "\n----------------------------ADJUST PURCHASE------------------------------------"
			adjustevent = adjust_event(campaign_data, app_data, device_data,"hvq872")
			util.execute_request(**adjustevent)
	
	###########		Finalize		############
	set_appCloseTime(app_data)
	
	return {'status':True}


def call_pattern(campaign_data, app_data, device_data):
	if random.randint(1,100)<=90 and app_data.get('Registration_flag')==False:
		interval = random.randint(10,20)
		set_sessionLength(app_data,length=interval)
		print "RELAX WAIT !!!! SLEEP IS RUNNING"
		time.sleep(random.randint(2,7))
		print "\n----------------------------ADJUST Event Registration------------------------------------"
		adjustevent = adjust_event(campaign_data, app_data, device_data,"2ipb3l")
		util.execute_request(**adjustevent)

		if random.randint(1,100)<=80:
			interval = random.randint(10,20)
			set_sessionLength(app_data,length=interval)
			print "RELAX WAIT !!!! SLEEP IS RUNNING"
			time.sleep(random.randint(2,7))
			print "\n----------------------------ADJUST Event ------------------------------------"
			adjustevent = adjust_event(campaign_data, app_data, device_data,"tqwn1w")
			util.execute_request(**adjustevent)
			
		interval = random.randint(10,20)
		set_sessionLength(app_data,length=interval)
		print "RELAX WAIT !!!! SLEEP IS RUNNING"
		time.sleep(random.randint(2,7))
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustevent = adjust_event(campaign_data, app_data, device_data,"dypud7")
		util.execute_request(**adjustevent)
		app_data['Registration_flag']=True
	else:
		if random.randint(1,100)<=60 and app_data.get('upload_picture_flag')==False:
			interval = random.randint(10,20)
			set_sessionLength(app_data,length=interval)
			print "RELAX WAIT !!!! SLEEP IS RUNNING"
			time.sleep(random.randint(2,7))
			print "\n----------------------------ADJUST Event upload a picture------------------------------------"
			adjustevent = adjust_event(campaign_data, app_data, device_data,"41841e")
			util.execute_request(**adjustevent)
			app_data['upload_picture_flag']=True
		
		if random.randint(1,100)<=70 and app_data.get('main_screen_flag')==False:
			interval = random.randint(10,20)
			set_sessionLength(app_data,length=interval)
			print "RELAX WAIT !!!! SLEEP IS RUNNING"
			time.sleep(random.randint(2,7))
			print "\n----------------------------ADJUST Event main screen------------------------------------"
			adjustevent = adjust_event(campaign_data, app_data, device_data,"bgdh3j")
			util.execute_request(**adjustevent)
			app_data['main_screen_flag']=True

	
###########################################################
#						ADJUST							  #
###########################################################
def adjust_session(campaign_data, app_data, device_data,event='install'):
	def_sessionLength(app_data)
	set_installedAT(app_data,device_data)
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
				'Client-SDK':campaign_data.get('adjust').get('sdk'),
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Connection':'Keep-Alive',
				'Accept-Encoding':'gzip',	
				'Content-Type': 'application/x-www-form-urlencoded',
				#'ADRUM': 'isAjax:true',
				#'ADRUM_1':'isMobile:true'
			}

	params={}

	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1

	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())

	app_data['keep_time1']=time.time()
	
	data = {
		'attribution_deeplink':1,
		'cpu_type': device_data.get('cpu_abi'),
		'event_buffering_enabled': 0,
		'hardware_name':device_data.get('hardware'),		
		'android_uuid':app_data.get('android_uuid'),
		'api_level':device_data.get('sdk'),
		'app_token':campaign_data.get('adjust').get('app_token'),
		'app_version': campaign_data.get('app_version_name'),
		'country':device_data.get('locale').get('country').upper(),
		'created_at':datetime.datetime.fromtimestamp(app_data.get('keep_time1')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'connectivity_type'     :'1',
		'device_manufacturer': device_data.get('brand').upper(),
		'device_name':device_data.get('model'),
		'device_type':device_data.get('device_type'),
		'display_height':device_data.get('resolution').split('x')[0],
		'display_width':device_data.get('resolution').split('x')[1],
		'environment': 'production',
		'gps_adid':device_data.get('adid'),
		'gps_adid_src' : 'service',
		'installed_at':datetime.datetime.fromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'language':device_data.get('locale').get('language'),
		'mcc':device_data.get('mcc'),
		'mnc':device_data.get('mnc'),
		'needs_response_details':  '1',
		'network_type'          :'0',
		'os_name': 'android',
		'os_version': device_data.get('os_version'),
		'package_name': campaign_data.get('package_name'),
		'screen_density':cal_density(device_data),
		'screen_format':cal_screen_formet(device_data),
		'screen_size': cal_screen_size(device_data),
		'sent_at':datetime.datetime.fromtimestamp(app_data.get('keep_time1')+1).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'session_count':app_data.get('session_count'),
		'tracking_enabled':1,
		'os_build':device_data.get('build'),
		'updated_at':datetime.datetime.fromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		# 'vm_isa': 'arm64',
		}

	if event == 'open':
		timeSpent = int(time.time())-app_data.get('adjust_call_time')
		app_data['timeSpent'] = timeSpent+ random.randint(600,800)
		
		data['time_spent']=app_data.get('sessionLength')
		data['subsession_count']=3
		data['session_length']=app_data.get('sessionLength')
		data['last_interval']= get_lastInterval(app_data)
		# app_data['last_interval_1'] = int(time.time())

	# if not app_data.get('last_interval_1'):
	# 	app_data['last_interval_1'] = int(time.time())

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_attribution(campaign_data, app_data, device_data, initiated_by = "sdk"):

	url = 'http://app.adjust.com/attribution'
	method='head'
	headers = {
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'User-Agent' : 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding' : 'gzip',
		#'ADRUM': 'isAjax:true',
		#'ADRUM_1':'isMobile:true'
		}
	params = {
		'app_token':campaign_data.get('adjust').get('app_token'),
		'created_at': datetime.datetime.fromtimestamp(app_data.get('adjust_call_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'environment':'production',
		'gps_adid':device_data.get('adid'),
		'gps_adid_src' : 'service',
		'needs_response_details':'1',
		'tracking_enabled':'1',
		'event_buffering_enabled':'0',
		'sent_at':datetime.datetime.fromtimestamp(app_data.get('adjust_call_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'attribution_deeplink':1,
		'android_uuid':app_data.get('android_uuid'),
		'api_level':device_data.get('sdk'),
		'app_version': campaign_data.get('app_version_name'),
		'device_name':device_data.get('model'),
		'device_type':device_data.get('device_type'),
		'initiated_by' : initiated_by,
		'os_name': 'android',
		'os_version': device_data.get('os_version'),
		'package_name': campaign_data.get('package_name'),

		}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

def adjust_sdk_click(campaign_data, app_data, device_data,source='install_referrer'):
	
	click1 = datetime.datetime.fromtimestamp(app_data.get('times').get('click_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
	set_installedAT(app_data,device_data)
	
	url = 'http://app.adjust.com/sdk_click'
	method='post'
	headers={
		'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Client-SDK': campaign_data.get('adjust').get('sdk'),
		'Content-Type':'application/x-www-form-urlencoded',
		'Accept-Encoding':'gzip',
		#'ADRUM': 'isAjax:true',
		#'ADRUM_1':'isMobile:true'
	}
	
	params={}
	data={
		'android_uuid':app_data.get('android_uuid'),
		'app_token' : campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink':'1',
		#'click_time' :click1,
		'created_at' :datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'environment' : 'production',
		'event_buffering_enabled':'0',
		'gps_adid' : device_data.get('adid'),
		'gps_adid_src' : 'service',
		'needs_response_details':'1',
		'referrer':app_data.get('referrer') if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)',
		'sent_at': datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'source' : source,
		'tracking_enabled' : '1',
		'cpu_type': device_data.get('cpu_abi'),
		'hardware_name':device_data.get('hardware'),		
		'api_level':device_data.get('sdk'),
		'app_version': campaign_data.get('app_version_name'),
		'country':device_data.get('locale').get('country').upper(),
		'connectivity_type'     :'1',
		'device_manufacturer': device_data.get('brand').upper(),
		'device_name':device_data.get('model'),
		'device_type':device_data.get('device_type'),
		'display_height':device_data.get('resolution').split('x')[0],
		'display_width':device_data.get('resolution').split('x')[1],
		'installed_at':datetime.datetime.fromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'install_begin_time': datetime.datetime.fromtimestamp(app_data.get('times').get('download_end_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'language':device_data.get('locale').get('language'),
		'mcc':device_data.get('mcc'),
		'mnc':device_data.get('mnc'),
		'last_interval':get_lastInterval(app_data),
		'network_type'          :'0',
		'os_name': 'android',
		'os_version': device_data.get('os_version'),
		'package_name': campaign_data.get('package_name'),
		'screen_density':cal_density(device_data),
		'screen_format':cal_screen_formet(device_data),
		'screen_size': cal_screen_size(device_data),
		'os_build':device_data.get('build'),
		'updated_at':datetime.datetime.fromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		# 'vm_isa': 'arm64',
		'session_count':app_data.get('session_count'),
		'time_spent':app_data.get('sessionLength'),
		'subsession_count':'1',
		'session_length':app_data.get('sessionLength')
		
	}
	data['click_time']=click1
	

	if source=='reftag':
		# del data['session_count']
		# del data['time_spent']
		# del data['subsession_count']
		# del data['session_length']
		# del data['android_uuid']
		del data['install_begin_time']
		# data['last_interval']=get_lastInterval(app_data)
		data ['raw_referrer']= urllib.quote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not%20set)&utm_medium=(not%20set)' 
		# app_data['last_interval_1'] = int(time.time())

	# if not app_data.get('last_interval_1'):
	# 	app_data['last_interval_1'] = int(time.time())

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}	

def get_reftag(referrer):
	reftag=""
	temp_b = urlparse.parse_qs(referrer)
	if temp_b.get('adjust_reftag'):
		reftag = temp_b.get('adjust_reftag')[0]
	return reftag

def adjust_event(campaign_data, app_data, device_data,event_token,revenue=False):
	def_subsession_count(app_data)
	def_sessionLength(app_data)
	
	url = 'http://app.adjust.com/event'
	method = 'post'
	header = {
				'Client-SDK':campaign_data.get('adjust').get('sdk'),
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Connection':'Keep-Alive',
				'Accept-Encoding':'gzip',	
				'Content-Type': 'application/x-www-form-urlencoded',
				#'ADRUM': 'isAjax:true',
				#'ADRUM_1':'isMobile:true'
			}

	params={}

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	timeSpent = int(time.time())-app_data.get('adjust_call_time')
	app_data['timeSpent'] = timeSpent
	app_data['keep_time1']=time.time()

	data ={
		'api_level':device_data.get('sdk'),
		'device_type':device_data.get('device_type'),
		'device_manufacturer': device_data.get('brand').upper(),
		'gps_adid':device_data.get('adid'),
		'gps_adid_src' : 'service',
		'environment': 'production',
		'session_count':app_data.get('session_count'),
		'time_spent':  app_data.get('sessionLength'),	
		'android_uuid':app_data.get('android_uuid'),
		'needs_response_details':  '1',
		'os_name': 'android',
		'display_width':device_data.get('resolution').split('x')[1],
		'subsession_count':app_data.get('subsession_count'),
		'screen_format':cal_screen_formet(device_data),
		'screen_density':cal_density(device_data),
		'tracking_enabled':1,
		'session_length':  app_data.get('sessionLength'),			
		'package_name': campaign_data.get('package_name'),
		'event_count': app_data.get('event_count'),
		'app_version': campaign_data.get('app_version_name'),
		'country':device_data.get('locale').get('country').upper(),
		'screen_size': cal_screen_size(device_data),
		'app_token':campaign_data.get('adjust').get('app_token'),
		'device_name':device_data.get('model'),
		'created_at':datetime.datetime.fromtimestamp(app_data.get('keep_time1')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'display_height': device_data.get('resolution').split('x')[0],
		'language':device_data.get('locale').get('language'),
		'mcc':device_data.get('mcc'),
		'mnc':device_data.get('mnc'),
		'os_version': device_data.get('os_version'),
		'event_token':event_token,
		'sent_at':datetime.datetime.fromtimestamp(app_data.get('keep_time1')+1).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z' + device_data.get('timezone'),
		'attribution_deeplink':1,
		'cpu_type': device_data.get('cpu_abi'),
		'event_buffering_enabled': 0,
		'hardware_name':device_data.get('hardware'),
		'os_build':device_data.get('build'),
		# 'vm_isa': 'arm64',
		'connectivity_type'     :'1',
		'network_type'          :'0',
	}
	# if event_token=='2ipb3l':
	# 	data['queue_size']="1"
	if revenue==True:
		data['currency'] = 'EUR'
		data['revenue'] = random.choice(['1.99','1.99','1.99','1.99','7.95'])
	
			
	return {'url': url, 'httpmethod': method, 'headers': header, 'params':params, 'data': data}


###########################################################
#						UTIL							  #
###########################################################
def cal_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def cal_screen_formet(device_data):
	screen_res=device_data.get('resolution')
	screen_formet='0'
	scr=screen_res.split("x")
	if float(scr[0]) / float(scr[1]) <= 1.7:
		screen_formet = "normal"
	else:
		screen_formet = "long"

	return screen_formet

def cal_density(device_data):
	density=''
	if device_data.get('dpi')>'160':
		density='high'
	elif device_data.get('dpi')>'120' and device_data.get('dpi')<='160':
		density='medium'
	else:
		density='low'

	return density

def def_sec(app_data, device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;

# def get_screen_format(app_data, device_data):
# 	resolution = device_data.get('resolution')
# 	b = resolution.split('x')
# 	c = float(b[1])/float(b[0])
# 	if c >= 1.77:
# 		return 'long'
# 	else:
# 		return 'normal'

# def get_screen_density(app_data, device_data):
# 	dpi = int(device_data.get('dpi'))
# 	if dpi >= 320:
# 		return 'high'
# 	elif dpi >= 180:
# 		return 'medium'
# 	else:
# 		return 'low'

def get_date(app_data,device_data,early=0):
	def_sec(app_data, device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

# def def_ua(app_data,device_data):
# 	if not app_data.get('ua'):
# 		if int(device_data.get("sdk")) >19:
# 			app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
# 		else:
# 			app_data['ua'] = 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_android_uuid(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())

def def_session_count(app_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 0

def inc_session_count(app_data):
	def_session_count(app_data)
	app_data['session_count'] += 1

def def_event_count(app_data):
	if not app_data.get('event_count'):
		app_data['event_count'] = 0

def inc_event_count(app_data):
	def_event_count(app_data)
	app_data['event_count']+=1

# def def_time_spent(app_data,forced=False):
# 	if not app_data.get('time_spent') or forced:
# 		app_data['time_spent']=0

# def inc_time_spent(app_data,val,forced=False):
# 	def_time_spent(app_data,forced)
# 	app_data['time_spent']+=val

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length		
	
def def_subsession_count(app_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 0

def inc_subsession_count(app_data):
	def_subsession_count(app_data)
	app_data['subsession_count']+=1

def set_installedAT(app_data,device_data):
	if not app_data.get('installed_at'):
		app_data['installed_at'] =  get_date(app_data,device_data,early=random.uniform(5,10))

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')