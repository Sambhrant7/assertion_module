# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util,purchase
from sdk import NameLists
import time
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.connected2.ozzy.c2m',
	'app_size'			 : 35.4,#27.6,#34.68,#47.26,
	'app_name' 			 :'Connected2.me Chat Anonymously',
	'app_version_name' 	 : '3.134',#'3.133',#'3.132',#'3.131',#'3.111',
	'app_version_code' 	 : '1819287174',#'1819280175',#'1819273175',#'1819266176',#'1819154381',
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 : 'uqra3ejzxy4g',
		'sdk'		 : 'android4.18.0',#'android4.15.1',
		'secret_key' : '174179255617068148851199241700610404100',
		'secret_id'	 : '7',
		},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	pushtoken(app_data)
	set_androidUUID(app_data)

	################generating_realtime_differences##################
	s1=20
	s2=24
	a1=22
	a2=25
	ir1=19
	ir2=23
	click_time2=True
	in1=22
	in2=25
	e1=0
	e2=0

	temp1,temp2,temp3=False,False,False

	###########		CALLS		############

	print "gcm token call"
	req=android_clients_google( campaign_data, device_data, app_data )
	res=util.execute_request(**req)
	try:
		gcm_token=res.get('data').split('token=')[1]
	except:
		gcm_token='ddvvcoVpmP8:APA91bFL_YE5MASSAGq4hAvywTGNTdUyPIeXkwWnAcmNOTlJtDz3psDZwEOQnxch9vWiI7XuNchU7SBTrhVwHjeGLweMxhLvlecRPsbIgzBT-X3WA2_Z23K03jBEkgJQKDhmAYu2_JqP'
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
		
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='install_referrer',t1=ir1,t2=ir2)
	util.execute_request(**request)
		
	app_data['gcm_token']=gcm_token

	print '\n'+'Adjust : SDK INFO____________________________________'
	request=adjust_sdkinfo(campaign_data, app_data, device_data,t1=in1,t2=in2)
	util.execute_request(**request)

	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='reftag',t1=ir1,t2=ir2)
	util.execute_request(**request)
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)

	if random.randint(1,100)<=60:

		time.sleep(random.randint(20,30))
		adjust_token_lxsonu(campaign_data, app_data, device_data,e1,e2)

		adjust_token_x95h24(campaign_data, app_data, device_data,e1,e2)

		app_data['register'] = True

	if random.randint(1,100)<=40 and app_data.get('register'):

		time.sleep(random.randint(8,10))
		adjust_token_888eee(campaign_data, app_data, device_data,e1,e2)

	for _ in range(random.randint(2,4)):
		
		if random.randint(1,100)<=85:

			time.sleep(random.randint(25,35))
			adjust_token_7hcrqq(campaign_data, app_data, device_data,e1,e2)

			if not temp1:
				adjust_token_w39jb8(campaign_data, app_data, device_data,e1,e2)

				temp1=True

			if not temp2:
				time.sleep(random.randint(1,2))
				adjust_token_sla1ds(campaign_data, app_data, device_data,e1,e2)

				temp2 = True

			if random.randint(1,100)<=75 and not temp3:

				time.sleep(random.randint(10,120))
				adjust_token_2g4270(campaign_data, app_data, device_data,e1,e2)

				temp3 = True

	set_appCloseTime(app_data)

	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	pushtoken(app_data)
	set_androidUUID(app_data)

	################generating_realtime_differences##################
	s1=20
	s2=24
	a1=22
	a2=25
	ir1=19
	ir2=23
	click_time2=True
	in1=22
	in2=25
	e1=0
	e2=0

	temp1,temp2,temp3=False,False,False

	###########		CALLS		############

	if not app_data.get('gcm_token'):
		print "gcm token call"
		req=android_clients_google( campaign_data, device_data, app_data )
		res=util.execute_request(**req)
		try:
			gcm_token=res.get('data').split('token=')[1]
		except:
			gcm_token='ddvvcoVpmP8:APA91bFL_YE5MASSAGq4hAvywTGNTdUyPIeXkwWnAcmNOTlJtDz3psDZwEOQnxch9vWiI7XuNchU7SBTrhVwHjeGLweMxhLvlecRPsbIgzBT-X3WA2_Z23K03jBEkgJQKDhmAYu2_JqP'

		app_data['gcm_token']=gcm_token	
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2,isOpen=True)
	util.execute_request(**request)

	if not app_data.get('register'):

		time.sleep(random.randint(20,30))
		adjust_token_lxsonu(campaign_data, app_data, device_data,e1,e2)

		adjust_token_x95h24(campaign_data, app_data, device_data,e1,e2)

		app_data['register'] = True

	if random.randint(1,100)<=35 and app_data.get('register'):

		time.sleep(random.randint(8,10))
		adjust_token_888eee(campaign_data, app_data, device_data,e1,e2)

	if day <=4:		
		number_of_days = random.randint(2,4)
	elif day <=7:
		number_of_days = random.randint(1,2)
	elif day>7:
		number_of_days = random.randint(0,1)	

	for _ in range(number_of_days):
		
		if random.randint(1,100)<=80:

			time.sleep(random.randint(25,35))
			adjust_token_7hcrqq(campaign_data, app_data, device_data,e1,e2)

			if not temp1:
				adjust_token_w39jb8(campaign_data, app_data, device_data,e1,e2)

				temp1=True

			if not temp2:
				time.sleep(random.randint(1,2))
				adjust_token_sla1ds(campaign_data, app_data, device_data,e1,e2)

				temp2 = True

			if random.randint(1,100)<=60 and not temp3:

				time.sleep(random.randint(10,120))
				adjust_token_2g4270(campaign_data, app_data, device_data,e1,e2)

				temp3 = True

	if random.randint(1,100)<=70 and not app_data.get('subscribe'):
		time.sleep(random.randint(5,10))
		adjust_token_u6g8ol(campaign_data, app_data, device_data,e1,e2)
		app_data['subscribe'] = True

	if purchase.isPurchase(app_data,day,advertiser_demand=10):	

		num = random.choice([1,1,1,2,2,3,4])

		if num==1:

			time.sleep(random.randint(5,10))
			adjust_token_53sqrq(campaign_data, app_data, device_data,e1,e2,revenue='6.99000')

			time.sleep(random.randint(1,2))
			adjust_token_o7aqbj(campaign_data, app_data, device_data,e1,e2)

			time.sleep(random.randint(1,2))
			adjust_token_gc9t2c(campaign_data, app_data, device_data,e1,e2)
	
			time.sleep(random.randint(1,2))
			adjust_token_cdb41c(campaign_data, app_data, device_data,e1,e2)

		elif num==2:

			time.sleep(random.randint(5,10))
			adjust_token_53sqrq(campaign_data, app_data, device_data,e1,e2,revenue='14.99000')

			time.sleep(random.randint(1,2))
			adjust_token_o7aqbj(campaign_data, app_data, device_data,e1,e2)

			time.sleep(random.randint(1,2))
			adjust_token_7k3yxk(campaign_data, app_data, device_data,e1,e2)
	
			time.sleep(random.randint(1,2))
			adjust_token_pzsize(campaign_data, app_data, device_data,e1,e2)	

		elif num==3:

			time.sleep(random.randint(5,10))
			adjust_token_53sqrq(campaign_data, app_data, device_data,e1,e2,revenue='22.99000')

			time.sleep(random.randint(1,2))
			adjust_token_o7aqbj(campaign_data, app_data, device_data,e1,e2)

			time.sleep(random.randint(1,2))
			adjust_token_b7hb2x(campaign_data, app_data, device_data,e1,e2)
	
			time.sleep(random.randint(1,2))
			adjust_token_9thmeo(campaign_data, app_data, device_data,e1,e2)	

		elif num==4:

			time.sleep(random.randint(5,10))
			adjust_token_djf7k2(campaign_data, app_data, device_data,e1,e2)

			time.sleep(random.randint(1,2))
			adjust_token_42cxkn(campaign_data, app_data, device_data,e1,e2)			
				
	set_appCloseTime(app_data)

	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def adjust_token_7hcrqq(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________7hcrqq'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='7hcrqq',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_w39jb8(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________w39jb8'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='w39jb8',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_sla1ds(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________sla1ds'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='sla1ds',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_lxsonu(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________lxsonu'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='lxsonu',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_x95h24(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________x95h24'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='x95h24',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_888eee(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________888eee'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='888eee',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_2g4270(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________2g4270'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='2g4270',t1=t1,t2=t2)
	util.execute_request(**request)	

def adjust_token_u6g8ol(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________u6g8ol'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='u6g8ol',t1=t1,t2=t2)
	util.execute_request(**request)	



def adjust_token_53sqrq(campaign_data, app_data, device_data,t1=0,t2=0,revenue=''):
	transactionID = 'GPA'+util.get_random_string(type='decimal',size=4)+'-'+util.get_random_string(type='decimal',size=4)+'-'+util.get_random_string(type='decimal',size=4)+'-'+util.get_random_string(type='decimal',size=4)
	print 'Adjust : EVENT______________________________53sqrq'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=json.dumps({"transactionID":transactionID}),event_token='53sqrq',t1=t1,t2=t2,revenue=revenue)
	util.execute_request(**request)	

def adjust_token_o7aqbj(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________o7aqbj'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='o7aqbj',t1=t1,t2=t2)
	util.execute_request(**request)	

def adjust_token_gc9t2c(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________gc9t2c'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='gc9t2c',t1=t1,t2=t2)
	util.execute_request(**request)		

def adjust_token_cdb41c(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________cdb41c'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='cdb41c',t1=t1,t2=t2)
	util.execute_request(**request)	

def adjust_token_7k3yxk(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________7k3yxk'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='7k3yxk',t1=t1,t2=t2)
	util.execute_request(**request)	

def adjust_token_pzsize(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________pzsize'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='pzsize',t1=t1,t2=t2)
	util.execute_request(**request)	

def adjust_token_b7hb2x(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________b7hb2x'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='b7hb2x',t1=t1,t2=t2)
	util.execute_request(**request)	

def adjust_token_9thmeo(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________9thmeo'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='9thmeo',t1=t1,t2=t2)
	util.execute_request(**request)	

def adjust_token_djf7k2(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________djf7k2'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='djf7k2',t1=t1,t2=t2)
	util.execute_request(**request)	

def adjust_token_42cxkn(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________42cxkn'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='42cxkn',t1=t1,t2=t2)
	util.execute_request(**request)													


###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	pushtoken(app_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())

	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src":	"service",
		"mcc":device_data.get('mcc'),
		"mnc":device_data.get('mnc'),
		"hardware_name" : device_data.get('hardware'),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if isOpen:
		def_(app_data,'subsession_count')
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('appCloseTime')-app_data.get('sess_len')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('appCloseTime')-app_data.get('sess_len')
		app_data['sess_len']=int(time.time())


	if app_data.get('gcm_token'):
		data['push_token']=app_data.get('gcm_token')


	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'session')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,t3=0,t4=0,callback_params=None,partner_params=None):
	pushtoken(app_data)
	reftag = ''
	if app_data.get('referrer'):
		temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
		if temp_b.get('adjust_reftag'):
			reftag = temp_b.get('adjust_reftag')[0]
	sdk_clk_time=get_date(app_data,device_data)
	time.sleep(random.randint(t3,t4))
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src":	"service",
		"mcc":device_data.get('mcc'),
		"mnc":device_data.get('mnc'),
		"hardware_name" : device_data.get('hardware'),
		"install_begin_time" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time")),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"referrer" : "utm_source=(not%20set)&utm_medium=(not%20set)",
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : 1,
		"session_length" : session_length,
		"source" : source,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params



	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		data['last_interval']='0'
		if data.get('install_begin_time'):
			del data['install_begin_time']
		if click_time:
			data['click_time']=sdk_clk_time
		elif data.get("click_time"):
			del data['click_time']
		# del data['session_length']
		# del data['subsession_count']
		# del data['time_spent']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['last_interval']='0'
		if click_time:
			data['click_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("click_time"))
		elif data.get("click_time"):
			del data['click_time']
		
		if data.get('raw_referrer'):
			del data['raw_referrer']
		
		data['install_begin_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time"))
		# del data['last_interval']

	if app_data.get('gcm_token'):
		data['push_token']=app_data.get('gcm_token')

	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'click')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_sdkinfo(campaign_data, app_data,device_data,t1=0,t2=0):
	pushtoken(app_data)
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	method = "post"
	url = 'https://app.adjust.com/sdk_info'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid":app_data.get('android_uuid'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"attribution_deeplink" : "1",
		"created_at" : created_at,
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src":	'service',
		"needs_response_details" : "1",
		"push_token" : app_data.get('pushtoken'),
		"sent_at" : sent_at,
		"source" : "push",
		"tracking_enabled" : "1",

		}

	if app_data.get('gcm_token'):
		data['push_token']=app_data.get('gcm_token')

	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'info')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,t1=0,t2=0):
	inc_(app_data,'subsession_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"android_uuid":app_data.get('android_uuid'),
		"api_level":device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version":campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"device_name":device_data.get('model'),
		"device_type":device_data.get('device_type'),
		"created_at" : created_at,
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src":"service",
		"package_name":campaign_data.get('package_name'),
		"initiated_by" : "backend",
		"needs_response_details" : "1",
		"os_name": "android",
		"os_version":device_data.get('os_version'),
		"sent_at" : sent_at,
		"tracking_enabled" : "1",

		}

	if app_data.get('gcm_token'):
		params['push_token']=app_data.get('gcm_token')


	data = {

		}

	
	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('gps_adid'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None,revenue=''):
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"event_count" : app_data.get('event_count'),
		"event_token" : event_token,
		"gps_adid" : device_data.get('adid'),
		"gps_adid_src":	"service",
		"mcc":device_data.get('mcc'),
		"mnc":device_data.get('mnc'),
		"hardware_name" : device_data.get('hardware'),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : "1",
		"network_type" : "13",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"push_token" : app_data.get('pushtoken'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"session_length" : session_length,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if app_data.get('gcm_token'):
		data['push_token']=app_data.get('gcm_token')

	if event_token=='53sqrq':
		data['currency'] = 'USD'
		data['revenue'] = revenue	

	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}




def android_clients_google( campaign_data, device_data, app_data ):
	url= "https://android.clients.google.com/c2dm/register3"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Authorization": "AidLogin 3488683844547025388:744292537329313395",
        'User-Agent'	 : 'Android-GCM/1.5 ('+device_data.get('product')+' '+device_data.get('build')+')',
        'app'	 : campaign_data.get('package_name'),
        
        "content-type": "application/x-www-form-urlencoded",
        "gcm_ver": "17455008"}

	params= None

	data= {       'X-app_ver':campaign_data.get('app_version_code'),
        "X-app_ver_name": campaign_data.get('app_version_name'),
        "X-appid": util.get_random_string('char_all',11),
        "X-cliv": 'fiid-'+util.get_random_string('decimal',8),
        "X-gmp_app_id": '1:'+'14581598012'+':android:'+util.get_random_string('hex',16),
        "X-gmsv": "17455008",
        "X-osv":device_data.get('sdk'),
        "X-scope": "*",
        "X-subtype": "14581598012",
        "app":campaign_data.get('package_name'),
        "app_ver": campaign_data.get('app_version_code'),
        "cert": util.get_random_string('hex',40),
        "device": "3488683844547025388",
        "gcm_ver": "17455008",
        "info": "cw1XKmVGuToaUEfGgBpd_731eAHzrhY",
        "plat": "0",
        "sender": "14581598012",
        "target_ver": "28"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}




###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	return app_data.get('android_uuid')


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']='f0L4WFS5AEk:'+util.get_random_string('google_token',140)
	return app_data.get('pushtoken')

def get_auth(device_data,app_data,created_at,gps_adid,activity_type):
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activity_type)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'




#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"