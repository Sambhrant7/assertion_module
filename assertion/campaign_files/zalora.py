# -*- coding: utf8 -*-
import uuid,time,datetime,random,json,urllib,urlparse,string
from sdk import getsleep
from sdk import util,purchase,installtimenew
import clicker, Config


campaign_data = {
			'package_name':'com.zalora.android',
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'app_name':'ZALORA Fashion Shopping',
			'app_version_name': '8.6.0',#'8.5.0',#'8.4.0',#'8.2.1',#'8.2.0',##'8.1.0',#'8.0.1',#'7.5.0',#'7.4.0',#'7.3.2',#'7.2.0',#'7.1.4',#'7.1.3',#'7.1.2',#'7.1.1',#'7.0.3',#6.13.4, '6.13.2',#'6.12.0',#'6.11.2',#'6.9.0', #'6.8.2',#'6.7.7',#'6.7.6','6.6.0',#'6.5.2',#'6.5.1',#'6.3.0',#'6.1.0',#'6.0.0',#'5.6.1', 6.2.0, 6.8.1, 6.10.0
			'app_version_code': '277',#'276',#'275',#'273',#'271',#'270',#'269',#'265',#'264',#'263',#'260',#'259',#'258',#'257',#'256',#'254',#247, '245',#'242',#'241',#'235',#'233',#'230',#'229','217',#'216',#'214',#'203',#'201', 208, 232, 237
			'ctr':6,
			'no_referrer': True,
			'supported_countries': 'WW',
			'device_targeting':True,
			'supported_os':'5.0',#'4.4', 
			'tracker'	 :'adjust',
			'app_size' : 10.0,#11.0,#10.0,#11.0,
			'adjust':{
					'app_token': 's4qpu9zf8f98',
					'sdk': 'android4.14.0',#'android4.13.0',#'android4.11.4',
					'app_secret'	: '701093614107610673616921591230719402',#'8422933121525584704328943788821906064',#'14838973472087150079491520861095341176',#'2038645809298963876154540821593661155',#'1818462201948733626177654699615231871',#'125259246267731984217657440031841080847',#'99301159412979811199332525282052503199',#'21470598654994432371728199511647838989',#'627130136910372697579956321886608686',#'5663690455026950687971364422081779509',#'580239088111664365612887249401525181134',#'787742918961197298975048148587593926',#'1206337027165376051021452585541509063132',#'3980392562284552058202934401657410538',#'1807605032458006129314271501653730882',#'13480863476529651925656873454478736',#18358385031916546689288420380605760330, '17728998566255590771313211912836082321',#'209007873899792980612383903511649118866',#'106742836264228381820059210212024112651',#1277271498248308305697944869581930993, '252445791093636650878115741518653471', #'185146179523508104818165885801093022485',#'1445471879603516971955430672573595837',#'11166564541897458879138564962499503462','12586425858427017401651109801790434111',#'19276832011083688336445514311791435987',#'1655438354439260511925185282158212106',#'1701959811595392856511682669905913851',#'60282851819011519798074634511906333052', 16362601910770837534292559961644703025
					'Signature_secret_id'	: '89',#'88',#'87',#'84',#'83',#'82',#'80',#'78',#'77',#'76',#'73',#'72',#'70',#'69',#'64',#'61',#54, '49',#'44',#'43',#38, '37', #'35',#'29',#'28','17',#'16',#'14',#'12',#'3',31  , 
					},
			'retention':{
							1:50,
							2:48,
							3:46,
							4:44,
							5:40,
							6:38,
							7:35,
							8:32,
							9:31,
							10:30,
							11:29,
							12:28,
							13:27,
							14:26,
							15:25,
							16:24,
							17:23,
							18:22,
							19:21,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
						},
		}
		
def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	if not gender_n:
		app_data['gender'] = random.choice(['male', 'female'])
		gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	if not user_info:
		user_data = util.generate_name(gender=gender_n)
		user_info = {}
		username = user_data.get('username').lower()
		user_info['email'] = username.lower() + '@gmail.com'
		# user_info['f_name'] = user_data.get('firstname')
		# user_info['l_name'] = user_data.get('lastname')
		# user_info['sex'] = gender_n
		# user_info['gender'] = str(1) if gender_n == 'female' else str(2)
		# user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
		# user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
		user_info['password'] = util.get_random_string(type='all',size=16)
		user_info['age'] = str(random.randint(18,35))
		app_data['user_info'] = user_info		
		
countryData_arr = {
		'BN':{ 'key':'zySHsP6cOEJMf3anffw8Ton0Zdx5Bmq','male':'men','female':'women' },
		'HK':{ 'key':'KMjnIcDfDgiXMJFiiJw4VaJ10K6j2jZ','male':'men','female':'women' },
		'ID':{ 'key':'w6nJpr78eAypUl8bXOn52V8zm40RoIZ','male':'pria','female':'wanita' },
		'MY':{ 'key':'zySHsP6cOEJMf3anffw8Ton0Zdx5Bmq','male':'men','female':'women' },
		'PH':{ 'key':'10G2St1EIp8lFtM6fmBqk7cC4xJxoR0','male':'men','female':'women' },
		'SG':{ 'key':'EPchje2Mv0EbETzpLDCO4V2gGpgidE8','male':'men','female':'women' },
		'TW':{ 'key':'scJSRncTe7jsUzGTrMCKoZ6I0EB1zo3','male':u'男裝'.encode('utf-8'),'female':u'男裝'.encode('utf-8')},
		'TH':{ 'key':'IfiP8a5CVnCtjDiUYaxTf6C3azEUZve','male':u'ผู้ชาย'.encode('utf-8'),'female':u'ผู้หญิง'.encode('utf-8')},
		'VN':{ 'key':'RdGJHUg4Ot98yoVxkQrsb3smXmKfrdz','male':'nam','female':'nam' },
		}

###########################################################
#														  #
#						INSTALL							  #
#														  #
###########################################################
def install(app_data, device_data, day=1):
	print 'please wait installing...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)
	global get_ts
	get_ts=app_data.get('times').get('download_end_time')
	app_data['install_begin_time']=get_date_ts(app_data,device_data,get_ts)
	get_ts=app_data.get('times').get('install_complete_time')
	app_data['ins_at']=get_date_ts(app_data,device_data,get_ts)
	print '---------------------------Install---------------------------------'
	###########		Initialize		############
	def_sec(app_data,device_data)
	def_android_uuid(app_data)
	def_session_count(app_data)
	set_sessionLength(app_data,forced=True,length=0)
	get_gcmToken(app_data)
	if not app_data.get('user_id'):
		app_data['user_id']='10'+str(util.get_random_string('decimal',15))

	app_data['time_passes'] = int(time.time())
	app_data['register']=False
	app_data['login']=False
	app_data['in_cart']=0
	app_data['in_wishlist']=0
	app_data['view_list']=False
	app_data['purchase_count']=0
	global search_query,category,category_id,tree
	search_query=['shoes','socks','jeans','shirt','jockey','casual shoes','watch','adidas','t shirt','bag','wallet','belt','jacket','sweater','pullover']
	category_id=['164','5614','18']
	category=['Shoes','Socks','Jeans']
	tree=['men,Shoes,','men,Clothing,Socks,','men,Clothing,Jeans,']

	###########			Calls		############
		
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)

	print "ADJUST sdk"
	a1 = adjust_sdkclick(campaign_data, app_data, device_data)
	util.execute_request(**a1)

	print "ADJUST sdk"
	a1 = adjust_sdkclick(campaign_data, app_data, device_data, source='install_referrer')
	util.execute_request(**a1)

	print "\n----------------------------ADJUST Sdk_info ----------------------------------------"
	adjustSession=adjust_sdk_info(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)
	
	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data, initiated_by='sdk')
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)
	
	# print "zalora features"
	# zf = zalora_features(campaign_data, app_data, device_data)
	# util.execute_request(**zf)
	
	# print "zalora homescreen"
	# zm = zalora_homescreen(campaign_data, app_data, device_data)
	# util.execute_request(**zm)

	print "zalora checkout cart"
	zcc = zalora_checkout_cart(campaign_data, app_data, device_data)
	zcc_output = util.execute_request(**zcc)
	global cookie_1
	cookie_1=False
	try:
		cookie_1=str(zcc_output.get('res').headers.get('Set-Cookie')).split(';')[0]
	except:
		cookie_1 = ''
		print "exception"

	# print "zalora tutorials"
	# zm = zalora_tutorials(campaign_data, app_data, device_data)
	# util.execute_request(**zm)

	
	
	if random.randint(1,100)<=90:
		time.sleep(random.randint(15,20))	
		inc_subsession_count(app_data)
		inc=random.randint(125,150)
		inc_time_spent(app_data,inc)
		print "\n----------------------------ADJUST Event(On choosing country) ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'rwzx75')
		util.execute_request(**adjustEvent)

		print "\n----------------------------ADJUST Event(On choosing gender) ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'rwzx75')
		util.execute_request(**adjustEvent)
	
	 				########SIGNUP########
		if random.randint(1,100)<=45 and app_data['register']==False:
			print "zalora forced login"
			zm = zalora_forced_login(campaign_data, app_data, device_data)
			util.execute_request(**zm)
	
			time.sleep(random.randint(30,60))
			print "\n----------------------------ADJUST Event(SIGNUP) ------------------------------------"
			adjustEvent = adjust_event(campaign_data, app_data, device_data,'si5ud1')
			util.execute_request(**adjustEvent)			
			app_data['register']=True
			app_data['login']=True
	
	 					########___HOMEPAGE___########
		if random.randint(1,100)<=95:
			time.sleep(random.randint(5,10))
			inc_time_spent(app_data,1)
			print "\n----------------------------ADJUST Event ------------------------------------"
			adjustEvent = adjust_event(campaign_data, app_data, device_data,'xuhzlr')
			util.execute_request(**adjustEvent)

			print "\n----------------------------ADJUST Event ------------------------------------"
			adjustEvent = adjust_event(campaign_data, app_data, device_data,'pffoac')
			util.execute_request(**adjustEvent)

			print "\n----------------------------ADJUST Event ------------------------------------"
			adjustEvent = adjust_event2(campaign_data, app_data, device_data,'mwzf5s')
			util.execute_request(**adjustEvent)

	 						########___View List___########
			if random.randint(1,100)<=60 and app_data['view_list']==False:
				time.sleep(random.randint(10,60))
				inc=random.randint(35,45)
				inc_time_spent(app_data,inc)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'jzux93')
				util.execute_request(**adjustEvent)

				time.sleep(random.randint(1,2))
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'xvw4qk')
				util.execute_request(**adjustEvent)

				time.sleep(random.randint(2,3))
				inc_time_spent(app_data,1)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event2(campaign_data, app_data, device_data,'d2arxn')
				util.execute_request(**adjustEvent)
				app_data['view_list']=True

	 							########___Search Item___########
			if random.randint(1,100)<=60 and app_data['view_list']==False:
				time.sleep(random.randint(30,60))
				inc=random.randint(35,45)
				inc_time_spent(app_data,inc)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'jzux93',event_type='search_item')
				util.execute_request(**adjustEvent)

				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'xvw4qk',event_type='search_item')
				util.execute_request(**adjustEvent)

				time.sleep(random.randint(1,2))
				inc_time_spent(app_data,1)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event2(campaign_data, app_data, device_data,'d2arxn',event_type='search_item')
				util.execute_request(**adjustEvent)

				inc_time_spent(app_data,1)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event2(campaign_data, app_data, device_data,'96tlbs')
				util.execute_request(**adjustEvent)

				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event2(campaign_data, app_data, device_data,'bz0p2p')
				util.execute_request(**adjustEvent)
				app_data['view_list']=True

	 				########___View Product___########
			if random.randint(1,100)<=80 and app_data['view_list']==True:
				time.sleep(random.randint(10,60))
				inc=random.randint(35,45)
				inc_time_spent(app_data,inc)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'1kas4v')
				util.execute_request(**adjustEvent)

				time.sleep(random.randint(1,2))
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'guxfii')
				util.execute_request(**adjustEvent)

				time.sleep(random.randint(1,2))
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'guxfii')
				util.execute_request(**adjustEvent)

				time.sleep(random.randint(1,2))
				inc_time_spent(app_data,1)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event2(campaign_data, app_data, device_data,'o3lkor')
				util.execute_request(**adjustEvent)

				time.sleep(random.randint(10,30))
				choice=random.choice([1,2,2,2,2,3,3,3])
				inc=random.randint(35,45)
				inc_time_spent(app_data,inc)
				if choice==1:
						 				########___Share___#######		
					print "\n----------------------------ADJUST Event ------------------------------------"
					adjustEvent = adjust_event(campaign_data, app_data, device_data,'xsjg95')
					util.execute_request(**adjustEvent)
				elif choice==2:
						 				########___Add to Cart___########
					print "\n----------------------------ADJUST Event ------------------------------------"
					adjustEvent = adjust_event(campaign_data, app_data, device_data,'qv9q1v')
					util.execute_request(**adjustEvent)
					app_data['in_cart']+=1
				elif choice==3:
	 									########___Add to Wishlist___########
	 				adjustEvent = adjust_event2(campaign_data, app_data, device_data,event_token='bhzrc5')
					util.execute_request(**adjustEvent)
					app_data['in_wishlist']+=1

			 				########___Remove from Wishlist___########
			if random.randint(1,100)<=10 and app_data.get('in_wishlist')>0:
				time.sleep(random.randint(5,20))
				adjustEvent = adjust_event2(campaign_data, app_data, device_data,event_token='7bib2b')
				util.execute_request(**adjustEvent)
				app_data['in_wishlist']-=1

			 				########___View Wishlist___########
			if random.randint(1,100)<=10:
				time.sleep(random.randint(10,60))
				adjustEvent = adjust_event2(campaign_data, app_data, device_data,event_token='z7q1gc')
				util.execute_request(**adjustEvent)

			 				########___View Cart___########
			if random.randint(1,100)<=35:
				time.sleep(random.randint(10,60))
				inc=random.randint(35,45)
				inc_time_spent(app_data,inc)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ybci1u')
				util.execute_request(**adjustEvent)

				# print "\n----------------------------ADJUST Event ------------------------------------"
				# adjustEvent = adjust_event2(campaign_data, app_data, device_data,'bilmfq')
				# util.execute_request(**adjustEvent)

				inc_time_spent(app_data,1)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'6pau48')
				util.execute_request(**adjustEvent)

			 				########___Remove from Cart___########
			if random.randint(1,100)<=10 and app_data.get('in_cart')>0:
				time.sleep(random.randint(5,30))
				inc=random.randint(35,45)
				inc_time_spent(app_data,inc)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'wmriow')
				util.execute_request(**adjustEvent)
				app_data['in_cart']-=1

			 				########___Log out___########
			if random.randint(1,100)<=10 and app_data.get('login')==True:
				time.sleep(random.randint(10,60))
				inc_time_spent(app_data,1)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'235ywq')
				util.execute_request(**adjustEvent)	
				app_data['login']=False	

			 				########___Log in___########
			if random.randint(1,100)<=10 and app_data.get('login')==False:
				inc=random.randint(35,45)
				time.sleep(random.randint(30,60))
				inc_time_spent(app_data,inc)
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'ho196m')
				util.execute_request(**adjustEvent)
				app_data['login']=True

			if app_data.get('in_cart')>0 and app_data.get('login')==True:
				if purchase.isPurchase(app_data,day,advertiser_demand=4):
					time.sleep(random.randint(10,20))
					inc=random.randint(35,45)
					inc_time_spent(app_data,inc)
					print "\n----------------------------ADJUST Event ------------------------------------"
					adjustEvent = adjust_event(campaign_data, app_data, device_data,'5que71')
					util.execute_request(**adjustEvent)

					inc_time_spent(app_data,1)
					print "\n----------------------------ADJUST Event ------------------------------------"
					adjustEvent = adjust_event(campaign_data, app_data, device_data,'6m0dcb')
					util.execute_request(**adjustEvent)	

					inc_time_spent(app_data,1)
					print "\n----------------------------ADJUST Event ------------------------------------"
					adjustEvent = adjust_event(campaign_data, app_data, device_data,'3w2g5z')
					util.execute_request(**adjustEvent)

					if app_data['purchase_count']==0:
						print "\n----------------------------ADJUST Event ------------------------------------"
						adjustEvent = adjust_event(campaign_data, app_data, device_data,'bu6h1q')
						util.execute_request(**adjustEvent)

					print "\n----------------------------ADJUST Event ------------------------------------"
					adjustEvent = adjust_event2(campaign_data, app_data, device_data,'uxn22v')
					util.execute_request(**adjustEvent)
					app_data['purchase_count']+=1

		###########		Finalize		############
		
	set_appCloseTime(app_data)
	return {'status':True}

###########################################################
#														  #
#						OPEN							  #
#														  #
###########################################################
def open(app_data, device_data,day=1):
	print '---------------------------Open---------------------------------'
	###########		Initialize		############
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)
		global get_ts
		get_ts=app_data.get('times').get('install_complete_time')
		app_data['ins_at']=get_date_ts(app_data,device_data,get_ts)
	def_sec(app_data,device_data)
	def_android_uuid(app_data)
	def_session_count(app_data)
	get_gcmToken(app_data)

	if not app_data.get('user_id'):
		app_data['user_id']='10'+str(util.get_random_string('decimal',15))

	set_sessionLength(app_data,forced=True,length=4000)
	if not app_data.get('login'):
		app_data['login']=False
	if not app_data.get('register'):
		app_data['register']=False
	if not app_data.get('in_cart'):
		app_data['in_cart']=0
	if not app_data.get('in_wishlist'):
		app_data['in_wishlist']=0

	app_data['view_list']=False
	if not app_data.get('purchase_count'):
		app_data['purchase_count']=0
	global search_query,category,category_id,tree
	search_query=['shoes','socks','jeans','shirt','jockey','casual shoes','watch','adidas','t shirt','bag','wallet','belt','jacket','sweater','pullover']
	category_id=['164','5614','18']
	category=['Shoes','Socks','Jeans']
	tree=['men,Shoes,','men,Clothing,Socks,','men,Clothing,Jeans,']

	###########			Calls		############

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,True)
	util.execute_request(**adjustSession)

	app_data['time_passes'] = int(time.time())
	
	time.sleep(random.randint(1,2))
	inc_subsession_count(app_data)
	inc=random.randint(125,150)
	inc_time_spent(app_data,inc)
	print "\n----------------------------ADJUST Event ------------------------------------"
	adjustEvent = adjust_event(campaign_data, app_data, device_data,'rwzx75')
	util.execute_request(**adjustEvent)

	time.sleep(random.randint(3,4))
	print "\n----------------------------ADJUST Event ------------------------------------"
	adjustEvent = adjust_event(campaign_data, app_data, device_data,'rwzx75')
	util.execute_request(**adjustEvent)

	 				########SIGNUP########
	if random.randint(1,100)<=30 and app_data['register']==False:
		time.sleep(random.randint(10,60))
		print "\n----------------------------ADJUST Event(SIGNUP) ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'si5ud1')
		util.execute_request(**adjustEvent)	
		app_data['register']=True		
		app_data['login']=True

	 					########___HOMEPAGE___########
	time.sleep(random.randint(1,2))
	inc_time_spent(app_data,1)
	print "\n----------------------------ADJUST Event ------------------------------------"
	adjustEvent = adjust_event(campaign_data, app_data, device_data,'xuhzlr')
	util.execute_request(**adjustEvent)

	time.sleep(random.randint(1,2))
	print "\n----------------------------ADJUST Event ------------------------------------"
	adjustEvent = adjust_event(campaign_data, app_data, device_data,'pffoac')
	util.execute_request(**adjustEvent)

	time.sleep(random.randint(1,2))
	print "\n----------------------------ADJUST Event ------------------------------------"
	adjustEvent = adjust_event2(campaign_data, app_data, device_data,'mwzf5s')
	util.execute_request(**adjustEvent)

		 				########___View List___########
	if random.randint(1,100)<=40 and app_data['view_list']==False:
		time.sleep(random.randint(10,60))
		inc=random.randint(35,45)
		inc_time_spent(app_data,inc)
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'jzux93')
		util.execute_request(**adjustEvent)

		time.sleep(random.randint(1,2))
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'xvw4qk')
		util.execute_request(**adjustEvent)

		time.sleep(random.randint(2,3))
		inc_time_spent(app_data,1)
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event2(campaign_data, app_data, device_data,'d2arxn')
		util.execute_request(**adjustEvent)
		app_data['view_list']=True

							########___Search Item___########
	if random.randint(1,100)<=60 and app_data['view_list']==False:
		time.sleep(random.randint(30,60))
		inc=random.randint(35,45)
		inc_time_spent(app_data,inc)
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'jzux93',event_type='search_item')
		util.execute_request(**adjustEvent)

		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'xvw4qk',event_type='search_item')
		util.execute_request(**adjustEvent)

		time.sleep(random.randint(1,2))
		inc_time_spent(app_data,1)
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event2(campaign_data, app_data, device_data,'d2arxn',event_type='search_item')
		util.execute_request(**adjustEvent)

		inc_time_spent(app_data,1)
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event2(campaign_data, app_data, device_data,'96tlbs')
		util.execute_request(**adjustEvent)

		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event2(campaign_data, app_data, device_data,'bz0p2p')
		util.execute_request(**adjustEvent)
		app_data['view_list']=True

	 				########___View Product___########
	if random.randint(1,100)<=60 and app_data['view_list']==True:
		time.sleep(random.randint(5,30))
		inc=random.randint(35,45)
		inc_time_spent(app_data,inc)
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'1kas4v')
		util.execute_request(**adjustEvent)

		time.sleep(random.randint(1,2))
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'guxfii')
		util.execute_request(**adjustEvent)

		time.sleep(random.randint(1,2))
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'guxfii')
		util.execute_request(**adjustEvent)

		time.sleep(random.randint(1,2))
		inc_time_spent(app_data,1)
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event2(campaign_data, app_data, device_data,'o3lkor')
		util.execute_request(**adjustEvent)

		time.sleep(random.randint(10,30))
		choice=random.choice([1,2,2,2,2,3,3,3])
		inc=random.randint(35,45)
		inc_time_spent(app_data,inc)
		if choice==1:
				 				########___Share___#######		
			print "\n----------------------------ADJUST Event ------------------------------------"
			adjustEvent = adjust_event(campaign_data, app_data, device_data,'xsjg95')
			util.execute_request(**adjustEvent)
		elif choice==2:
				 				########___Add to Cart___########
			print "\n----------------------------ADJUST Event ------------------------------------"
			adjustEvent = adjust_event(campaign_data, app_data, device_data,'qv9q1v')
			util.execute_request(**adjustEvent)
			app_data['in_cart']+=1
		elif choice==3:
									########___Add to Wishlist___########
			adjustEvent = adjust_event2(campaign_data, app_data, device_data,event_token='bhzrc5')
			util.execute_request(**adjustEvent)
			app_data['in_wishlist']+=1

	 				########___Remove from Wishlist___########
	if random.randint(1,100)<=5 and app_data.get('in_wishlist')>0:
		time.sleep(random.randint(5,20))
		adjustEvent = adjust_event2(campaign_data, app_data, device_data,event_token='7bib2b')
		util.execute_request(**adjustEvent)
		app_data['in_wishlist']-=1

	 				########___View Wishlist___########
	if random.randint(1,100)<=5:
		time.sleep(random.randint(10,60))
		adjustEvent = adjust_event2(campaign_data, app_data, device_data,event_token='z7q1gc')
		util.execute_request(**adjustEvent)

	 				########___View Cart___########
	if random.randint(1,100)<=25:
		time.sleep(random.randint(10,60))
		inc=random.randint(35,45)
		inc_time_spent(app_data,inc)
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'ybci1u')
		util.execute_request(**adjustEvent)

		# print "\n----------------------------ADJUST Event ------------------------------------"
		# adjustEvent = adjust_event2(campaign_data, app_data, device_data,'bilmfq')
		# util.execute_request(**adjustEvent)

		inc_time_spent(app_data,1)
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'6pau48')
		util.execute_request(**adjustEvent)

	 				########___Remove from Cart___########
	if random.randint(1,100)<=5 and app_data.get('in_cart')>0:
		time.sleep(random.randint(5,30))
		inc=random.randint(35,45)
		inc_time_spent(app_data,inc)
		print "\n----------------------------ADJUST Event ------------------------------------"
		adjustEvent = adjust_event(campaign_data, app_data, device_data,'wmriow')
		util.execute_request(**adjustEvent)
		app_data['in_cart']-=1

	if app_data.get('in_cart')>0 and app_data.get('login')==True:
		if purchase.isPurchase(app_data,day,advertiser_demand=26):
			time.sleep(random.randint(10,20))
			inc=random.randint(35,45)
			inc_time_spent(app_data,inc)
			print "\n----------------------------ADJUST Event ------------------------------------"
			adjustEvent = adjust_event(campaign_data, app_data, device_data,'5que71')
			util.execute_request(**adjustEvent)

			inc_time_spent(app_data,1)
			print "\n----------------------------ADJUST Event ------------------------------------"
			adjustEvent = adjust_event(campaign_data, app_data, device_data,'6m0dcb')
			util.execute_request(**adjustEvent)	

			inc_time_spent(app_data,1)
			print "\n----------------------------ADJUST Event ------------------------------------"
			adjustEvent = adjust_event(campaign_data, app_data, device_data,'3w2g5z')
			util.execute_request(**adjustEvent)

			if app_data['purchase_count']==0:
				print "\n----------------------------ADJUST Event ------------------------------------"
				adjustEvent = adjust_event(campaign_data, app_data, device_data,'bu6h1q')
				util.execute_request(**adjustEvent)

			print "\n----------------------------ADJUST Event ------------------------------------"
			adjustEvent = adjust_event2(campaign_data, app_data, device_data,'uxn22v')
			util.execute_request(**adjustEvent)
			app_data['purchase_count']+=1

	###########		Finalize		############
	set_appCloseTime(app_data)
	
	return {'status':True}
	
def zalora_features(campaign_data, app_data, device_data):
	url = 'https://api.zalora.sg/v1/init'
	method='get'
	headers = {
		'User-Agent': 'Android/'+device_data.get('sdk')+'/'+device_data.get('brand')+' '+device_data.get('model')+'/'+campaign_data.get('app_version_code')+'/0f607264fc6318a92b9e13c65db7cd3c/'+device_data.get('resolution').split('x')[1]+'/'+device_data.get('resolution').split('x')[0]+'/'+campaign_data.get('app_version_name'),
		'Zalora-Lang': device_data.get('locale').get('language'),
		'Zalora-Country': device_data.get('locale').get('country'),
		'Accept-Encoding':'gzip',
	}

	
	params = {
				'contentVersion': json.dumps({"shop_catalogs":"2","shopbys":"9"}),
				'd' : 'a',
				'features':	'campaign_pin,campaign_qr,native_mobile_thumbor,native_mobile_webp,app_proactively_fetch_cart,app_proactively_fetch_wishlist,app_proactively_fetch_wallet,forced_login,source_catalog_attribution,app_quicksilver_checkout_enabled,app_akamai_enabled',
				'setLang':	device_data.get('locale').get('language'),
				'shop': 'm',
	}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}	

def zalora_homescreen(campaign_data, app_data, device_data):
	url = 'https://api.zalora.sg/v1/homescreen'
	method='get'
	headers = {
		'User-Agent': 'Android/'+device_data.get('sdk')+'/'+device_data.get('brand')+' '+device_data.get('model')+'/'+campaign_data.get('app_version_code')+'/0f607264fc6318a92b9e13c65db7cd3c/'+device_data.get('resolution').split('x')[1]+'/'+device_data.get('resolution').split('x')[0]+'/'+campaign_data.get('app_version_name'),
		'Zalora-Lang': device_data.get('locale').get('language'),
		'Zalora-Country': device_data.get('locale').get('country'),
		'Accept-Encoding':'gzip',
	}
	
	params = {
				'd' : 'a',
				'formFactor': 'phone',
				'segment': random.choice(['men','women']),	
				'setLang':	device_data.get('locale').get('language'),
				'shop': 'm',
	}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}	
	
def zalora_checkout_cart(campaign_data, app_data, device_data):
	url = 'https://api.zalora.sg/v1/checkout/cart/'
	method='get'
	headers = {
		'User-Agent': 'Android/'+device_data.get('sdk')+'/'+device_data.get('brand')+' '+device_data.get('model')+'/'+campaign_data.get('app_version_code')+'/0f607264fc6318a92b9e13c65db7cd3c/'+device_data.get('resolution').split('x')[1]+'/'+device_data.get('resolution').split('x')[0]+'/'+campaign_data.get('app_version_name'),
		'Zalora-Lang': device_data.get('locale').get('language'),
		'Zalora-Country': device_data.get('locale').get('country'),
		'Accept-Encoding':'gzip',
	}
	
	params = {
				'd' : 'a',
				'setLang':	device_data.get('locale').get('language'),
	}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}		
	
def zalora_tutorials(campaign_data, app_data, device_data):
	url = 'https://api.zalora.sg/v1/tutorials'
	method='get'
	headers = {
		'User-Agent': 'Android/'+device_data.get('sdk')+'/'+device_data.get('brand')+' '+device_data.get('model')+'/'+campaign_data.get('app_version_code')+'/0f607264fc6318a92b9e13c65db7cd3c/'+device_data.get('resolution').split('x')[1]+'/'+device_data.get('resolution').split('x')[0]+'/'+campaign_data.get('app_version_name'),
		'Zalora-Lang': device_data.get('locale').get('language'),
		'Zalora-Country': device_data.get('locale').get('country'),
		'Accept-Encoding':'gzip',
	}
	
	if cookie_1:
		headers['Cookie'] = cookie_1


	
	params = {
				'd' : 'a',
				'deviceType':'android',
				'fromVersion':'',
				'setLang':	device_data.get('locale').get('language'),
				'toVersion':campaign_data.get('app_version_name'),
	}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}	
	
def zalora_forced_login(campaign_data, app_data, device_data):
	url = 'https://api.zalora.sg/v1/forms/forcedlogin'
	method='head'
	headers = {
		'User-Agent': 'Android/'+device_data.get('sdk')+'/'+device_data.get('brand')+' '+device_data.get('model')+'/'+campaign_data.get('app_version_code')+'/0f607264fc6318a92b9e13c65db7cd3c/'+device_data.get('resolution').split('x')[1]+'/'+device_data.get('resolution').split('x')[0]+'/'+campaign_data.get('app_version_name'),
		'Zalora-Lang': device_data.get('locale').get('language'),
		'Zalora-Country': device_data.get('locale').get('country'),
		'Accept-Encoding':'gzip',
	}
	
	if cookie_1:
		headers['Cookie'] = cookie_1

	
	params = {
				'd' : 'a',
				'setLang':	device_data.get('locale').get('language'),
				'version':2,
	}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}		
	
def zalora_customer_forced_login(campaign_data, app_data, device_data):

	return_userinfo(app_data)
	url = 'https://api.zalora.sg/v1/customers/forcedLogin'
	method='post'
	headers = {
		'User-Agent': 'Android/'+device_data.get('sdk')+'/'+device_data.get('brand')+' '+device_data.get('model')+'/'+campaign_data.get('app_version_code')+'/0f607264fc6318a92b9e13c65db7cd3c/'+device_data.get('resolution').split('x')[1]+'/'+device_data.get('resolution').split('x')[0]+'/'+campaign_data.get('app_version_name'),
		'Zalora-Lang': device_data.get('locale').get('language'),
		'Zalora-Country': device_data.get('locale').get('country'),
		'Accept-Encoding':'gzip',
		'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
	}
	
	if cookie_1:
		headers['Cookie'] = cookie_1


	data = {
				'':'',
				'email':app_data.get('user_info').get('email'),
				'password':app_data.get('user_info').get('password'),
	}
	
	params = {
				'd' : 'a',
				'setLang':	device_data.get('locale').get('language'),
	}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def zalora_checkout_sync(campaign_data, app_data, device_data):

	url = 'https://api.zalora.sg/v1/checkout/sync'
	method='post'
	headers = {
		'User-Agent': 'Android/'+device_data.get('sdk')+'/'+device_data.get('brand')+' '+device_data.get('model')+'/'+campaign_data.get('app_version_code')+'/0f607264fc6318a92b9e13c65db7cd3c/'+device_data.get('resolution').split('x')[1]+'/'+device_data.get('resolution').split('x')[0]+'/'+campaign_data.get('app_version_name'),
		'Zalora-Lang': device_data.get('locale').get('language'),
		'Zalora-Country': device_data.get('locale').get('country'),
		'Accept-Encoding':'gzip',
		'Content-Type': 'application/x-www-form-urlencoded'
	}
	
	if cookie_1:
		headers['Cookie'] = cookie_1

	
	params = {
				'd' : 'a',
				'setLang':	device_data.get('locale').get('language'),
	}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}	
	
def zalora_wishlist(campaign_data, app_data, device_data):

	url = 'https://api.zalora.sg/v1/wishLists/sync'
	method='post'
	headers = {
		'User-Agent': 'Android/'+device_data.get('sdk')+'/'+device_data.get('brand')+' '+device_data.get('model')+'/'+campaign_data.get('app_version_code')+'/0f607264fc6318a92b9e13c65db7cd3c/'+device_data.get('resolution').split('x')[1]+'/'+device_data.get('resolution').split('x')[0]+'/'+campaign_data.get('app_version_name'),
		'Zalora-Lang': device_data.get('locale').get('language'),
		'Zalora-Country': device_data.get('locale').get('country'),
		'Accept-Encoding':'gzip',
		'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
	}
	
	if cookie_1:
		headers['Cookie'] = cookie_1


	data = {
				'':'',
				'wishLists':json.dumps({}),
	}
	
	params = {
				'd' : 'a',
				'setLang':	device_data.get('locale').get('language'),
	}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
###########################################################
#														  #
#						ADJUST							  #
#														  #
###########################################################
def adjust_session(campaign_data, app_data, device_data,isOpen=False):
	def_android_uuid(app_data)
	inc_session_count(app_data)
	def_time_spent(app_data)

	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'User-Agent':def_ua(app_data,device_data),
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'Accept-Encoding':'gzip',
		 }
	params=None
	data = {
		'android_uuid':app_data.get('android_uuid'),
		'api_level':device_data.get('sdk'),
		'app_token':campaign_data.get('adjust').get('app_token'),
		'app_version':campaign_data.get('app_version_name'),
		'attribution_deeplink':	'1',
		'connectivity_type':'1',
		'cpu_type': device_data.get('cpu_abi'),
		'country':device_data.get('locale').get('country'),
		'created_at': get_date(app_data,device_data,early=random.uniform(2,2.5)),
		'device_manufacturer':device_data.get('manufacturer'),
		'device_name':device_data.get('model'),
		'device_type':device_data.get('device_type'),
		'display_height':device_data.get('resolution').split('x')[0],
		'display_width':device_data.get('resolution').split('x')[1],
		'environment':'production',
		'event_buffering_enabled':0,
		'gps_adid':device_data.get('adid'),
		'hardware_name':device_data.get('hardware'),
		'language':device_data.get('locale').get('language'),
		'needs_response_details':1,
		'os_name': 'android',
		'os_version':device_data.get('os_version'),
		'package_name':campaign_data.get('package_name'),
		# 'queue_size':0,
		'screen_density': get_screen_density(device_data),
		'screen_format': get_screen_format(device_data),
		'screen_size': 'normal',
		'network_type':'13',#'0',
		'sent_at': get_date(app_data,device_data),
		'session_count':app_data.get('session_count'),
		'tracking_enabled': 1,
		'installed_at': app_data.get('ins_at'),
		'updated_at':app_data.get('ins_at'),
		'vm_isa':'arm64',
		'os_build':device_data.get('build'),
		'mnc':device_data.get('mnc'),
		'mcc':device_data.get('mcc'),
		}

	
	if isOpen:
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['created_at'] 		= get_date(app_data,device_data,early=random.uniform(1,3))
		data['last_interval'] 	= get_lastInterval(app_data)
		
		data['subsession_count']= app_data.get('subsession_count')
		

		data['session_length'] 	= app_data.get('time_spent')
		data['time_spent'] 		= app_data.get('time_spent') - random.randint(2,3)

	headers['Authorization']=forSignature(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'session')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

# def def_insat(app_data,device_data):
# 	if not app_data.get('ins_at'):
# 		app_data['ins_at']=get_date(app_data,device_data,early=random.randint(2300,2500))

def adjust_attribution(campaign_data, app_data, device_data, initiated_by='backend'):
	url = 'http://app.adjust.com/attribution'
	method='head'
	headers = {
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'User-Agent' :def_ua(app_data,device_data),
		'Accept-Encoding' : 'gzip'
		}
	params = {
		'app_token':campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink':	'1',
		'created_at': get_date(app_data,device_data,early=random.uniform(2,2.5)),
		'environment':'production',
		'event_buffering_enabled':0,
		'initiated_by':initiated_by,
		'gps_adid':device_data.get('adid'),
		'needs_response_details':1,
		'sent_at': get_date(app_data,device_data),
		'tracking_enabled':1,
		}
	headers['Authorization']=forSignature(device_data,app_data,params.get('created_at'),params.get('gps_adid'),'attribution')	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

def adjust_sdk_info(campaign_data, app_data, device_data):
	url = 'http://app.adjust.com/sdk_info'
	method='post'
	headers = {
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'User-Agent' :def_ua(app_data,device_data),
		'Content-Type': 'application/x-www-form-urlencoded',
		'Accept-Encoding' : 'gzip'
		}
	params = None

	data ={
		'app_token':campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink':	'1',
		'created_at': get_date(app_data,device_data,early=random.uniform(2,2.5)),
		'environment':'production',
		'event_buffering_enabled':0,
		#'initiated_by':'backend',
		'gps_adid':device_data.get('adid'),
		'needs_response_details':1,
		'push_token':app_data.get('push_token'),
		'sent_at': get_date(app_data,device_data),
		'source':	'push',
		'tracking_enabled':1,
		}
	headers['Authorization']=forSignature(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'info')	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_sdkclick(campaign_data, app_data, device_data, source='reftag'):
	url = 'http://app.adjust.com/sdk_click'
	method='post'
	headers = {
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding':'gzip',
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent':def_ua(app_data,device_data),
		 }
	temp_b = urlparse.parse_qs(app_data.get('referrer'))
	if temp_b.get('adjust_reftag'):
		reftag = temp_b.get('adjust_reftag')[0]
	else:
		reftag=""

	data = {
		'app_token':campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink':	'1',
		'click_time':get_date(app_data,device_data,early=random.uniform(1,2)),
		'created_at':get_date(app_data,device_data,early=random.uniform(2,2.5)),
		'connectivity_type':'1',
		'environment':'production',
		#'needs_response_details':0,
		# 'referrer':app_data.get('referrer'),
		'reftag':reftag,
		'source':source,
		'android_uuid':app_data.get('android_uuid'),
		'api_level':device_data.get('sdk'),
		'app_version':campaign_data.get('app_version_name'),
		'cpu_type': device_data.get('cpu_abi'),
		'device_manufacturer':device_data.get('manufacturer'),
		'device_name':device_data.get('model'),
		'device_type':device_data.get('device_type'),
		'display_height':device_data.get('resolution').split('x')[0],
		'display_width':device_data.get('resolution').split('x')[1],
		'event_buffering_enabled':0,
		'gps_adid':device_data.get('adid'),
		'hardware_name':device_data.get('hardware'),
		'language':device_data.get('locale').get('language'),
		'needs_response_details':1,
		'network_type':'13',#'0',
		'os_name': 'android',
		'os_version':device_data.get('os_version'),
		'package_name':campaign_data.get('package_name'),
		# 'queue_size':0,
		'screen_density': get_screen_density(device_data),
		'screen_format': get_screen_format(device_data),
		'screen_size': 'normal',
		'sent_at': get_date(app_data,device_data),
		'session_count':1,
		'tracking_enabled': 1,
		'installed_at': app_data.get('ins_at'),
		'updated_at':app_data.get('ins_at'),
		'vm_isa':'arm64',
		'os_build':device_data.get('build'),	
		'country':device_data.get('locale').get('country'),	
		'mnc':device_data.get('mnc'),
		'mcc':device_data.get('mcc'),
		}

	if app_data.get('referrer'):
		data['referrer']=urllib.unquote(app_data.get('referrer'))
		data['raw_referrer']=urllib.quote(app_data.get('referrer'))	
	else:
		data['referrer']='utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'	
			
	def_(app_data,'subsession_count')
	def_sessionLength(app_data)
	data['created_at'] 		= get_date(app_data,device_data,early=random.uniform(1,3))
	data['last_interval'] 	= get_lastInterval(app_data)
	data['session_length'] 	= int(time.time()) - app_data.get('time_passes')
	data['subsession_count']= app_data.get('subsession_count')
	data['time_spent'] 		= int(time.time()) - app_data.get('time_passes')

	if source == 'install_referrer':
		data['install_begin_time'] = app_data.get('install_begin_time')
		del data['last_interval']
		del data['raw_referrer']
		del data['reftag']

	headers['Authorization']=forSignature(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'click')	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}

def adjust_event(campaign_data, app_data, device_data,event_token,event_type=''):
	def_android_uuid(app_data)
	def_session_count(app_data)
	def_time_spent(app_data)
	def_subsession_count(app_data)
	inc_event_count(app_data)
	def_sessionLength(app_data)
	url = 'http://app.adjust.com/event'
	headers = {
		'User-Agent':def_ua(app_data,device_data),
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'Accept-Encoding':'gzip',
		 }
	params=None
	data = {
			'android_uuid':app_data.get('android_uuid'),
			'api_level':device_data.get('sdk'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'app_version':campaign_data.get('app_version_name'),
			'attribution_deeplink':	'1',
			'cpu_type': device_data.get('cpu_abi'),
			'connectivity_type':'1',
			'created_at': get_date(app_data,device_data,early=random.uniform(2,2.5)),
			'device_manufacturer':device_data.get('manufacturer'),
			'device_name':device_data.get('model'),
			'device_type':device_data.get('device_type'),
			'network_type':'13',#'0',
			'display_height':device_data.get('resolution').split('x')[0],
			'display_width':device_data.get('resolution').split('x')[1],
			'environment':'production',
			'event_buffering_enabled':0,
			'event_count':app_data.get('event_count'),
			'event_token':event_token,
			'gps_adid':device_data.get('adid'),
			'hardware_name':device_data.get('hardware'),
			'language':device_data.get('locale').get('language'),
			'needs_response_details':1,
			'os_name': 'android',
			'os_version':device_data.get('os_version'),
			'package_name':campaign_data.get('package_name'),
			# 'queue_size':0,
			'screen_density': get_screen_density(device_data),
			'screen_format': get_screen_format(device_data),
			'screen_size': 'normal',
			'sent_at': get_date(app_data,device_data),
			'session_count':app_data.get('session_count'),
			'session_length': app_data.get('time_spent')+2,
			'subsession_count':app_data.get('subsession_count'),
			'time_spent':app_data.get('time_spent'),
			'tracking_enabled': 1,
			# 'installed_at': app_data.get('ins_at'),
			# 'updated_at':app_data.get('ins_at'),
			'vm_isa':'arm64',
			'os_build':device_data.get('build'),
			'country':device_data.get('locale').get('country'),
			'mnc':device_data.get('mnc'),
			'mcc':device_data.get('mcc'),
			'push_token' : app_data.get('push_token'),
		}

	callback_params = get_callback_params(campaign_data, app_data, device_data,event_token)
	data['callback_params']=callback_params
	data['partner_params']=callback_params

	if event_token=='si5ud1':
		data['queue_size']=4
	# if event_token=='rwzx75':
	# 	data['queue_size']=1
	if event_token=='pffoac':
		data['queue_size']=1
	if event_token=='xvw4qk':
		data['queue_size']=1
	if event_token=='xuhzlr':
		data['queue_size']=2

	if event_token == '6m0dcb':
		data['revenue'] = app_data.get('price_list').get(app_data.get('product'))

	if event_token == '3w2g5z':
		data['revenue'] = app_data.get('price_list').get(app_data.get('product'))

	if event_token == 'xvw4qk':
		partner_params_data = json.loads(callback_params)
		del partner_params_data['device_manufacturer'], partner_params_data['display_size'], partner_params_data['device'], partner_params_data['device_model']
		data['partner_params']=json.dumps(partner_params_data)
		
	if event_token == 'qv9q1v':
		partner_params_data = json.loads(callback_params)
		del partner_params_data['currency_code']
		partner_params_data['fb_currency']='MYR'
		partner_params_data['fb_content_type']='product'
		partner_params_data['fb_content_id']=app_data.get('product')
		data['partner_params']=json.dumps(partner_params_data)
		
	if event_token=='guxfii':
		data['revenue'] = app_data.get('price_list').get(app_data.get('product'))
		data['currency'] = util.country_list.get(app_data.get('shop_country')).get('currency')
		partner_params_data = json.loads(callback_params)
		del partner_params_data['device_manufacturer'], partner_params_data['display_size'], partner_params_data['device'], partner_params_data['device_model'],partner_params_data['amount_sessions'],partner_params_data['currency_code'],partner_params_data['discount']
		partner_params_data['fb_currency']='MYR'
		partner_params_data['fb_content_type']='product'
		partner_params_data['fb_content_id']=app_data.get('product')
		partner_params_data['product']=app_data.get('product')
		partner_params_data['sku']=app_data.get('product')
		partner_params_data['amount_transactions']='0'
		data['partner_params']=json.dumps(partner_params_data)
		data['queue_size']=2

	if event_token=='6pau48':
		partner_params=get_callback_params(campaign_data, app_data, device_data,event_token)
		data['partner_params']=partner_params
		callback_params_data=json.loads(partner_params)
		del callback_params_data['app_version'],callback_params_data['shop_country'],callback_params_data['segment'],callback_params_data['fb_currency'],callback_params_data['user_segment'],callback_params_data['total_cart']
		callback_params_data['size']=random.choice(['S','M','L','XL'])
		data['callback_params']=json.dumps(callback_params_data)

	if event_type=='search_item':
		if event_token=='jzux93':
			callback_params=get_callback_params(campaign_data,app_data,device_data,event_token,event_type)
			data['callback_params']=callback_params
			data['partner_params']=callback_params
		if event_token=='xvw4qk':
			callback_params=get_callback_params(campaign_data,app_data,device_data,event_token,event_type)
			data['callback_params']=callback_params
			data['partner_params']=callback_params

	if event_token=='6m0dcb':
		partner_params_data = json.loads(callback_params)
		del partner_params_data['device_manufacturer'], partner_params_data['display_size'], partner_params_data['device'], partner_params_data['device_model'],partner_params_data['amount_sessions'],partner_params_data['currency_code'],partner_params_data['discount'],partner_params_data['age'],partner_params_data['user_segment'],partner_params_data['amount_transactions'],partner_params_data['quantity'],partner_params_data['size']
		partner_params_data['fb_content_id']=app_data.get('product')
		partner_params_data['fb_currency']='MYR'
		partner_params_data['transaction_id']=app_data.get('t_id')
		data['partner_params']=json.dumps(partner_params_data)

	if app_data.get('time_passes'):
		data['time_spent'] = int(time.time()) - app_data.get('time_passes')
		app_data['time_spent']=data['time_spent']
		data['session_length'] = data.get('time_spent')
	else:
		data['time_spent'] = '0'
		data['session_length'] = '0'

	headers['Authorization']=forSignature(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')	

	return {'url': url, 'httpmethod': 'post', 'headers': headers, 'params':params, 'data': data}

def adjust_event2(campaign_data, app_data, device_data,event_token,event_type=''):
	def_android_uuid(app_data)
	def_session_count(app_data)
	def_time_spent(app_data)
	def_subsession_count(app_data)
	inc_event_count(app_data)

	url = 'http://app.adjust.com/event'
	headers = {
		'User-Agent':def_ua(app_data,device_data),
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'Accept-Encoding':'gzip',
		 }
	params=None
	data = {
			'android_uuid':app_data.get('android_uuid'),
			'api_level':device_data.get('sdk'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'app_version':campaign_data.get('app_version_name'),
			'cpu_type': device_data.get('cpu_abi'),
			'created_at': get_date(app_data,device_data,early=random.uniform(2,2.5)),
			'device_manufacturer':device_data.get('manufacturer'),
			'device_name':device_data.get('model'),
			'device_type':device_data.get('device_type'),
			'display_height':device_data.get('resolution').split('x')[0],
			'display_width':device_data.get('resolution').split('x')[1],
			'environment':'production',
			'event_buffering_enabled':0,
			'event_count':app_data.get('event_count'),
			'event_token':event_token,
			'gps_adid':device_data.get('adid'),
			'hardware_name':device_data.get('hardware'),
			'language':device_data.get('locale').get('language'),
			'needs_response_details':1,
			'os_name': 'android',
			'os_version':device_data.get('os_version'),
			'package_name':campaign_data.get('package_name'),
			# 'queue_size':0,
			'screen_density': get_screen_density(device_data),
			'screen_format': get_screen_format(device_data),
			'screen_size': 'normal',
			'sent_at': get_date(app_data,device_data),
			'session_count':app_data.get('session_count'),
			'session_length': app_data.get('time_spent')+2,
			'subsession_count':app_data.get('subsession_count'),
			'time_spent':app_data.get('time_spent'),
			'tracking_enabled': 1,
			'installed_at': app_data.get('ins_at'),
			'updated_at':app_data.get('ins_at'),
			'vm_isa':'arm64',
			'os_build':device_data.get('build'),	
			'country':device_data.get('locale').get('country'),	
			'mnc':device_data.get('mnc'),
			'mcc':device_data.get('mcc'),
			'push_token' : app_data.get('push_token'),	
			}

	if event_token=='z7q1gc':
		partner_params = get_callback_params(campaign_data, app_data, device_data,event_token)
		data['partner_params']=partner_params
		callback_params_data=json.loads(partner_params)
		del callback_params_data['app_version'],callback_params_data['user_id'],callback_params_data['shop_country'],callback_params_data['gender'],callback_params_data['segment'],callback_params_data['age'],callback_params_data['fb_currency'],callback_params_data['user_segment']
		callback_params_data['discount']="TRUE"
		data['callback_params']=json.dumps(callback_params_data)

	if event_token=='7bib2b':
		callback_params = get_callback_params(campaign_data, app_data, device_data,event_token)
		data['callback_params']=callback_params
		data['partner_params']=callback_params

	if event_token=='o3lkor':
		callback_params = get_callback_params(campaign_data, app_data, device_data,event_token)
		data['callback_params']=callback_params
		partner_params=get_callback_params(campaign_data, app_data, device_data,"partner_params2")
		data['partner_params']=partner_params

	if event_token=='96tlbs':
		callback_params = get_callback_params(campaign_data, app_data, device_data,event_token)
		data['callback_params']=callback_params
		data['partner_params']=callback_params

	if event_token=='bz0p2p':
		global choice
		choice=random.randint(0,len(search_query)-1)
		callback_params = get_callback_params(campaign_data, app_data, device_data,event_token)
		partner_params_data=json.loads(callback_params)
		if choice==0 or choice==1 or choice==2:
			del partner_params_data['device'],partner_params_data['device_manufacturer'],partner_params_data['display_size'],partner_params_data['device_model'],partner_params_data['query'],partner_params_data['tree']
		else:
			del partner_params_data['device'],partner_params_data['device_manufacturer'],partner_params_data['display_size'],partner_params_data['device_model'],partner_params_data['query']
		data['callback_params']=callback_params
		data['partner_params']=json.dumps(partner_params_data)
		
	if event_token == 'd2arxn':
		callback_params = get_callback_params(campaign_data, app_data, device_data,event_token)
		data['callback_params']=callback_params
		partner_params = get_callback_params(campaign_data, app_data, device_data,"partner_params1")
		data['partner_params']=partner_params
		if event_type=='search_item':
			callback_params = get_callback_params(campaign_data, app_data, device_data,event_token,event_type)
			data['callback_params']=callback_params
			partner_params=get_callback_params(campaign_data, app_data, device_data,"partner_params1",event_type)
			data['partner_params']=partner_params

		del data['installed_at']
		del data['updated_at']

	if event_token == 'bhzrc5':
		data['attribution_deeplink']=1
		data['connectivity_type']=1
		data['network_type']='13',#'0'
		callback_params=get_callback_params(campaign_data, app_data, device_data,event_token=event_token)
		data['callback_params']=callback_params
		data['partner_params']=callback_params
		del data['installed_at']
		del data['updated_at']

	if event_token == 'mwzf5s':
		data['attribution_deeplink']=1
		data['connectivity_type']=1
		data['network_type']='13',#'0'
		del data['installed_at']
		del data['updated_at']

	if event_token=='uxn22v':
		partner_params=get_callback_params(campaign_data, app_data, device_data,event_token=event_token)
		data['partner_params']=partner_params
		data['callback_params']=json.dumps({"age":app_data.get('user_info').get('age'),"user_segment":"4"})

	if app_data.get('time_passes'):
		data['time_spent'] = int(time.time()) - app_data.get('time_passes')
		app_data['time_spent']=data['time_spent']
		data['session_length'] = data.get('time_spent')
	else:
		data['time_spent'] = '0'
		data['session_length'] = '0'

	headers['Authorization']=forSignature(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')
	return {'url': url, 'httpmethod': 'post', 'headers': headers, 'params':params, 'data': data}

def get_callback_params(campaign_data, app_data, device_data,event_token,event_type=""):
	def_display_size(app_data)
	get_shopCountry(app_data,device_data)
	get_gender(app_data)
	return_userinfo(app_data)
	t_id(app_data)
	e_hash(app_data)
	bestseller_product(campaign_data, app_data, device_data)
	select_product(app_data,app_data.get('products'))
	
	
	if not app_data.get('brand_list'):
		app_data['brand_list']={'063A6SH544BDE6GS': 'ZALORA', 'NI126AA0SFC4MY': 'Nike', '098EESHBF89131GS': 'ZALORA', 'CO302AC0RZZHMY': 'Converse', 'NI126SH0RJW6MY': 'Nike', '5204DSHDEB6E21GS': 'ZALORA', '6FF80SH84E853BGS': 'ZALORA', 'A044ASH992D251GS': 'ZALORA', 'NI126AC0SFCLMY': 'Nike', 'AE46FZZEAF4D28GS': 'ZALORA'}

	if not app_data.get('products'):
		app_data['products']=["NI126AC0SFCLMY", "A044ASH992D251GS", "NI126AA0SFC4MY", "063A6SH544BDE6GS", "098EESHBF89131GS", "CO302AC0RZZHMY", "NI126SH0RJW6MY", "5204DSHDEB6E21GS", "6FF80SH84E853BGS", "AE46FZZEAF4D28GS"]

	if not app_data.get('price_list'):
		app_data['price_list']={'063A6SH544BDE6GS': '79.0', 'NI126AA0SFC4MY': '89.0', '098EESHBF89131GS': '89.0', 'CO302AC0RZZHMY': '69.9', 'NI126SH0RJW6MY': '239.0', '5204DSHDEB6E21GS': '129.0', '6FF80SH84E853BGS': '99.0', 'A044ASH992D251GS': '69.0', 'NI126AC0SFCLMY': '99.0', 'AE46FZZEAF4D28GS': '89.0'}


	callback_params = {
		    "app_version":campaign_data.get('app_version_name'),
		    "device":"Phone",
		    "device_manufacturer":device_data.get('manufacturer'),
		    "device_model":device_data.get('model'),
		    "display_size":app_data.get('display_size'),
		    "segment":'men' if app_data.get('gender')=='male' else 'women',
		    "shop_country":app_data.get('shop_country'),
		    "user_id":app_data.get('user_id'),
		}

	if event_token=='z7q1gc':
		app_data['price']=app_data.get('price_list').get(app_data.get('product'))
		del callback_params['device_model'],callback_params['device'],callback_params['device_manufacturer'],callback_params['display_size']
		callback_params['fb_currency']='MYR'
		callback_params["gender"]='male' if app_data.get('gender')=='male' else 'female'
		callback_params['user_segment']='4'
		callback_params['age']=app_data.get('user_info').get('age')
		callback_params['price']=app_data.get('price_list').get(app_data.get('product'))
		callback_params['quantity']=str(random.choice([1,1,1,2]))
		callback_params['sku']=app_data.get('product')
		callback_params['total_wishlist']=app_data.get('price')

	if event_token=='7bib2b':
		del callback_params['device'],callback_params['segment']
		callback_params['sku']=app_data.get('product')
		callback_params['currency_code']=util.country_list.get(app_data.get('shop_country')).get('currency')
		callback_params['price']=app_data.get('price_list').get(app_data.get('product'))

	if event_token=='xsjg95':
		del callback_params['device'],callback_params['segment']
		callback_params['sku']=app_data.get('product')

	if event_token=='si5ud1':
		del callback_params['device'],callback_params['segment']

	if event_token=='96tlbs':
		del callback_params['device'],callback_params['segment']
		callback_params['keywords']='shoes'

	if event_token=='bz0p2p':
		query=search_query[choice]
		if choice==0 or choice==1 or choice==2:
			callback_params['category']=category[choice]
			callback_params['category_id']=category_id[choice]
			callback_params['tree']=tree[choice]
		callback_params['fb_currency']='MYR'
		callback_params["gender"]='male' if app_data.get('gender')=='male' else 'female'
		callback_params['user_segment']='4'
		callback_params['query']=query
		callback_params['fb_content_id']=str(app_data.get('products'))
		callback_params['age']=app_data.get('user_info').get('age')
		callback_params['fb_content_type']='product'

	if event_token=='bhzrc5':
		select_product(app_data,app_data.get('products'))
		if callback_params.get('device'):
			del callback_params['device']
		if callback_params.get('segment'):
			del callback_params['segment']
		if callback_params.get('user_id'):
			del callback_params['user_id']
		callback_params['sku']=app_data.get('product')
		callback_params['currency_code']=util.country_list.get(app_data.get('shop_country')).get('currency')
		callback_params['price']=app_data.get('price_list').get(app_data.get('product'))

	if event_token=='5que71':
		quantity={"sku":app_data.get('product'),"currency_code":util.country_list.get(app_data.get('shop_country')).get('currency'),"quantity":str(random.choice([1,1,1,2]))}
		callback_params["transaction_id"]=str(app_data.get('t_id'))
		callback_params["new_customer"]="False"
		callback_params["amount_transactions"]="0"
		callback_params["gender"]='male' if app_data.get('gender')=='male' else 'female'
		callback_params["age"]=app_data.get('user_info').get('age')
		callback_params['user_segment']='4'
		callback_params['quantity']=json.dumps(quantity)

	if event_token=='uxn22v':
		callback_params = {"age":app_data.get('user_info').get('age'),"transaction_id":str(app_data.get('t_id')),"criteo_p":urllib.quote(str(app_data.get('products'))),"new_customer":app_data.get('user_id'),"user_segment":"4","customer_id":app_data.get('user_id')}

	if event_token=='3w2g5z':
		del callback_params['segment'],callback_params['device']
		callback_params["transaction_id"]= app_data.get('t_id')
		callback_params["skus"]= str(app_data.get('products'))
		callback_params["amount_transactions"]= "0"

	if event_token=='6m0dcb':
		app_data['price']=app_data.get('price_list').get(app_data.get('product'))
		callback_params["amount_sessions"]= "12"
		callback_params["brand"]= app_data.get('brand_list').get(app_data.get('product'))
		callback_params["currency_code"]= util.country_list.get(app_data.get('shop_country')).get('currency')
		callback_params["discount"]= "true"
		callback_params["new_customer"]= "false"
		callback_params["gender"]='male' if app_data.get('gender')=='male' else 'female'
		callback_params["amount_transactions"]= "0"
		callback_params["total_transaction"]= app_data.get('price')
		callback_params["price"]=app_data.get('price')
		callback_params["sku"]= app_data.get('product')
		callback_params["size"]= random.choice(['S','M','L','XL'])
		callback_params['quantity']=str(random.choice([1,1,1,2]))
		callback_params['fb_order_id']='SG'+app_data.get('t_id')
		callback_params["age"]=app_data.get('user_info').get('age')
		callback_params['user_segment']='4'

	if event_token=='rwzx75':
		del callback_params['device'],callback_params['segment'],callback_params['user_id']
		callback_params["duration"] = str(random.randint(148000,148100))

	if event_token=='pffoac':
		del callback_params['device_model'],callback_params['user_id'],callback_params['device'],callback_params['device_manufacturer'],callback_params['display_size']
		callback_params['fb_currency']='MYR'
		callback_params['user_segment']='4'

	if event_token=='jzux93':
		callback_params["products"]= str(app_data.get('products'))
		del callback_params['user_id'],callback_params['device'],callback_params['segment']
		if event_type=='search_item':
			callback_params['user_id']=app_data.get('user_id')

	if event_token=='xvw4qk':
		callback_params["skus"]= str(app_data.get('products'))
		callback_params['user_segment']='4'
		del callback_params['user_id']
		if event_type=='search_item':
			callback_params['user_id']=app_data.get('user_id')
			callback_params['gender']='male' if app_data.get('gender')=='male' else 'female'
			callback_params['age']=app_data.get('user_info').get('age')

	if event_token=='d2arxn':
		callback_params = {'user_segment':'4'}
		if event_type=='search_item':
			callback_params['criteo_email_hash']=app_data.get('e_hash')
			callback_params['age']=app_data.get('user_info').get('age')

	if event_token=='o3lkor':		
		callback_params = {"user_segment":"4"}

	if event_token=='1kas4v':
		select_product(app_data,app_data.get('products'))
		callback_params["product"]= app_data.get('product')
		del callback_params['user_id'],callback_params['device'],callback_params['segment']

	if event_token=='guxfii':
		callback_params["amount_sessions"]= "1"
		callback_params["brand"]= app_data.get('brand_list').get(app_data.get('product'))
		callback_params["currency_code"]= util.country_list.get(app_data.get('shop_country')).get('currency')
		callback_params["discount"]= "true"
		callback_params["price"]=app_data.get('price_list').get(app_data.get('product'))
		callback_params["sku"]= app_data.get('product')
		callback_params['user_segment']='4'
		del callback_params["user_id"]

	if event_token=='6pau48':
		app_data['price']=app_data.get('price_list').get(app_data.get('product'))
		del callback_params['device_model'],callback_params['device'],callback_params['device_manufacturer'],callback_params['display_size'],callback_params['user_id']
		callback_params['fb_currency']='MYR'
		callback_params['user_segment']='4'
		callback_params["brand"]= app_data.get('brand_list').get(app_data.get('product'))
		callback_params["discount"]= "FALSE"
		callback_params["price"]=app_data.get('price_list').get(app_data.get('product'))
		callback_params["sku"]= app_data.get('product')
		callback_params["total_cart"]=app_data.get('price')
		callback_params['quantity']=str(random.choice([1,1,1,2]))
		
	if event_token=='ybci1u':
		callback_params["product"]='{'+'sku:'+str(app_data.get('product'))+','+'price:'+str(app_data.get('price_list').get(app_data.get('product')))+','+'currency_code:'+str(util.country_list.get(app_data.get('shop_country')).get('currency'))+','+'quantity:'+str(random.choice([1,1,1,2]))+'}'
		del callback_params['user_id']
		callback_params['user_segment']='4'

	if event_token=='xuhzlr':
		callback_params['user_segment']='4'
		del callback_params['user_id']

	if event_token=='qv9q1v':
		del callback_params['segment'], callback_params['device'], callback_params['user_id']
		callback_params["currency_code"]= util.country_list.get(app_data.get('shop_country')).get('currency')
		callback_params["price"]=app_data.get('price_list').get(app_data.get('product'))
		callback_params["sku"]= app_data.get('product')

	if event_token=='ho196m':
		del callback_params['segment'], callback_params['device']

	if event_token=='wmriow':
		del callback_params['device'],callback_params['segment']
		callback_params['sku']=app_data.get('product')
		callback_params['currency_code']=util.country_list.get(app_data.get('shop_country')).get('currency')
		callback_params['price']=app_data.get('price_list').get(app_data.get('product'))

	if event_token=='bu6h1q':
		del callback_params['device'],callback_params['segment']

	if event_token=='235ywq':
		del callback_params['segment'],callback_params['device']

	if event_token=='partner_params1':
		callback_params = {'user_segment':'4','criteo_p':urllib.quote(str(app_data.get('products')))}
		if event_type=='search_item':
			callback_params['age']=app_data.get('user_info').get('age')
			callback_params['criteo_email_hash']=app_data.get('e_hash')
			callback_params['customer_id']=app_data.get('user_id')

	if event_token=='partner_params2':
		callback_params={"criteo_p":app_data.get('product'),"user_segment":"4"}

	return str(json.dumps(callback_params))

def t_id(app_data):
	if not app_data.get('t_id'):
		app_data['t_id']='208'+str(util.get_random_string('decimal',6))

def e_hash(app_data):
	if not app_data.get('e_hash'):
		app_data['e_hash']=str(util.get_random_string('hex',32))


def get_shopCountry(app_data,device_data):
	if not app_data.get('shop_country'):
		country_arr = get_countries()
		shop_country = device_data.get('country')
		if not shop_country in country_arr:
			shop_country = random.choice(country_arr)
		app_data['shop_country']=shop_country

def get_countries():
	return ['BN','HK','ID','MY','PH','SG','TW','TH','VN']

def get_gender(app_data,forced=False):
	if not app_data.get('gender') or forced:
		app_data['gender'] = random.choice(['male','female'])

def select_product(app_data,products):
	try: 
		app_data['product'] = str(random.choice(json.loads(products)))
	except:
		print "Exception"
		app_data['product'] = "098EESHBF89131GS"
		


###########################################################
#														  #
#						Zalora							  #
#														  #
###########################################################
def bestseller_product(campaign_data, app_data, device_data):
	get_shopCountry(app_data,device_data)
	get_gender(app_data)
	shop_country = app_data.get('shop_country')
	gender = app_data.get('gender')
	
	url = 'http://feed.apse.datajet.io/1.1/bestsellers'
	method='get'
	headers = {
		'User-Agent' :def_ua(app_data,device_data),
		'Accept-Encoding' : 'gzip'
		}
	params = {
		'from':'',
		'key':get_shopCountryKey(shop_country),
		'category':get_category(shop_country,gender),
		'page':'1',
		}
	run = {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}
	response = util.execute_request(**run)
	catch(response,app_data,'products_id')
	catch(response,app_data,'price_list')
	catch(response,app_data,'brand_list')

def get_shopCountryKey(shop_country):
	global countryData_arr
	countryKey = countryData_arr.get(shop_country).get('key')
	return countryKey

def get_category(shop_country,gender):
	global countryData_arr
	category = countryData_arr.get(shop_country).get(gender)
	return category


###########################################################
#														  #
#						UTIL							  #
#														  #
###########################################################
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	#st = str(int(time.time()*1000)) 
	st=device_data.get("device_id", str(int(time.time()*1000)))


	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid = device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_ts(app_data,device_data,get_ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(get_ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;

def def_ua(app_data,device_data):
	if int(device_data.get("sdk")) >19:
		ua = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		ua = 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	return ua

def def_android_uuid(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())

def def_session_count(app_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 0

def inc_session_count(app_data):
	def_session_count(app_data)
	app_data['session_count']+=1

def def_subsession_count(app_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 0

def inc_subsession_count(app_data):
	def_subsession_count(app_data)
	app_data['subsession_count']+=1

def def_event_count(app_data):
	if not app_data.get('event_count'):
		app_data['event_count'] = 0

def inc_event_count(app_data):
	def_event_count(app_data)
	app_data['event_count']+=1

def def_time_spent(app_data,forced=False):
	if not app_data.get('time_spent') or forced:
		app_data['time_spent']=0

def inc_time_spent(app_data,val,forced=False):
	def_time_spent(app_data,forced)
	app_data['time_spent']+=val

def def_display_size(app_data):
	if not app_data.get('display_size'):
		app_data['display_size'] = str(format(random.uniform(4,6), '.15f'))

def tester(app_data,device_data):
	bestseller_product(campaign_data, app_data, device_data)
	print '#####$$$$########$#$#'
	print app_data.get('brand_list')
	print "\n\n"
	print app_data.get('products')
	print "\n\n"
	print app_data.get('price_list')


def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="products_id":
			app_data['products'] = get_productIds(jsonData.get('items'))
		if paramName=='price_list':
			app_data['price_list'] = get_price_list(jsonData.get('items'))
		if paramName=='brand_list':
			app_data['brand_list'] = get_brand_list(jsonData.get('items'))
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def get_productIds(items):
	products_ids = []
	skus = []
	for item in items:
		skus += [str(item.get('skus')[1])]
	
	i=0
	while i<3:
		products_id = random.choice(skus)
		# indx = skus.index(products_id)
		# del skus[indx]
		products_ids += [str(products_id)]
		i+=1
	
	return json.dumps(products_ids)

def get_price_list(items):
	price_list = {}
	for item in items:
		product_id = str(item.get('skus')[1])
		price_list[product_id] = str(item.get('price').get('current'))
	
	return price_list

def get_brand_list(items):
	brand_list = {}
	for item in items:
		product_id = str(item.get('skus')[1])
		brand_list[product_id] = item.get('brand').get('name')
	return brand_list

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')		

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0		

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)	

def get_gcmToken(app_data):
	if not app_data.get('push_token'):
		app_data['push_token']= util.get_random_string('char_all',11) + ':APA91b'+''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))		
def forSignature(device_data,app_data,created_at,adid,activity_kind):
	data1=str(created_at)+str(campaign_data.get('adjust').get('app_secret'))+str(adid)+str(activity_kind)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('Signature_secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'		

#Authorization: Signature secret_id="29",signature="bf3aa0690781cf5d3aeb4cf5defce8d8bb306db9917f6a7abb11946a794c2157",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"