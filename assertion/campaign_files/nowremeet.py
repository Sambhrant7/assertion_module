#-*- coding:UTF-8 -*-
import sys
from sdk import getsleep
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import util,purchase, installtimenew
import time,hashlib,urllib,random,base64,string,datetime
import clicker
import Config

campaign_data = {
				'package_name' : 'com.voocle.now',
				'sdk' : 'Android',
				'app_version_name' : '1.2.21.527',#'1.2.21.526',#'1.2.21.522',#'1.2.21.521',#1.2.21.520, '1.2.21.510',#'1.2.21.504',#'1.2.21.490',#'1.2.21.488',#1.2.21.486, '1.2.21.485',#'1.2.21.484',#'1.2.21.481',#'1.2.21.480',#1.2.21.479, '1.2.21.477',#1.2.21.474, '1.2.21.470',#'1.2.21.469',#'1.2.21.468',#'1.2.21.467',#'1.2.21.464',#'1.2.21.463',#'1.2.8.390',#'1.2.5.380',#'1.2.4.379',#'3.0.8',#'3.0.5',3.0.6
				'app_name' : 'NOW',
				'supported_countries': 'WW',
				'device_targeting':True,
				'no_referrer': True,
				'tracker':'apsalar',
				'ctr':6,
				'app_size':81.0,#82.0,#101.0,#68.0,
				'supported_os':'5.0',
				'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
				'apsalar':{
					'version': 'Singular/v9.2.5-Unity/1.3.3',#'Singular/v7.4.0.PROD',
					'key':'voocle_c1de9c66',
					'secret':'417562fb2dd3acdf621fd058efe681e0',
				},
				'retention':{
							1:50,
							2:48,
							3:46,
							4:44,
							5:40,
							6:38,
							7:35,
							8:32,
							9:31,
							10:30,
							11:29,
							12:28,
							13:27,
							14:26,
							15:25,
							16:24,
							17:23,
							18:22,
							19:21,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
				}
}


def install(app_data, device_data):
	print 'please wait..........'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)
	
	user_agent(campaign_data,app_data,device_data)

	app_data['CharCreate']=False

	app_data["PID"]=util.get_random_string(type='decimal',size=5)
	app_data['custom_user_id'] = str(random.randint(30000,39999))
	app_data["ID"]=util.get_random_string(type='all',size=28)

	################################calls#####################################

	print '----------------------------- APSALAR Resolve ---------------------------'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**apsalar_Resolve)

	print '----------------------------- APSALAR START -----------------------------'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data,lag='0.0'+str(random.randint(80,99)))
	util.execute_request(**apsalar_start)

	print '----------------------------- APSALAR EVENT -------------------------__InstallReferrer----'
	value='{"referrer":"'+urllib.quote(app_data.get('referrer'))+'","referrer_source":"service","clickTimestampSeconds":'+str(int(str(int(app_data.get('times').get('install_complete_time')*1000)))-random.randint(100,120))+',"installBeginTimestampSeconds":'+str(int(app_data.get('times').get('install_complete_time')*1000))+',"current_device_time":'+str(int(time.time()))+'}'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='__InstallReferrer',value=value,t='0.'+str(random.randint(400,500)),lag='0.0'+str(random.randint(30,50)))
	util.execute_request(**apsalar_event)

	time.sleep(random.randint(2,4))

	print '----------------------------- APSALAR START -----------------------------'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data,lag='0.059000000000000004',flag=True)
	util.execute_request(**apsalar_start)

	

	if random.randint(1,100)<=95:
		time.sleep(random.randint(660,900))
		print '----------------------------- APSALAR EVENT --------------------------Login---'
		value='{"TIME":"'+datetime.datetime.now().strftime("%Y\/%m\/%d %H:%M:%S")+'","ID":'+app_data.get("ID")+'}'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='Login',value=value,custom_user_id=True,t='15.703000000000001',lag='0.03600000000000000'+str(random.randint(1,8)))
		util.execute_request(**apsalar_event)

		time.sleep(random.randint(55,60))

		print '----------------------------- APSALAR EVENT ---------------------------CharCreate--'
		value='{"TIME":"'+datetime.datetime.now().strftime("%Y\/%m\/%d %H:%M:%S")+'","ID":'+app_data.get("ID")+',"PID":'+app_data.get("PID")+',"NICKNAME":"'+util.get_random_string(type='char_small',size=5)+'"}'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='CharCreate',value=value,custom_user_id=True,t='256.'+str(random.randint(100,200)),lag='0.0'+str(random.randint(30,50)),seq='2')
		util.execute_request(**apsalar_event)
		app_data['CharCreate']=True

		print '----------------------------- APSALAR EVENT ---------------------------CharLogin--'
		value='{"TIME":"'+datetime.datetime.now().strftime("%Y\/%m\/%d %H:%M:%S")+'","ID":'+app_data.get("ID")+',"PID":'+app_data.get("PID")+'}'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='CharLogin',value=value,custom_user_id=True,t='257.'+str(random.randint(700,900)),lag='0.'+str(random.randint(500,800)),seq='3')
		util.execute_request(**apsalar_event)


		call_pattern(campaign_data,app_data,device_data,type1='install')
	
	return {'status':True}
	
	
def open(app_data, device_data,day):

	print "---------------------open--------------------------------------------"

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)
	
	user_agent(campaign_data,app_data,device_data)

	if not app_data.get('CharCreate'):
		app_data['CharCreate']=False
	if not app_data.get('PID'):
		app_data['PID']=util.get_random_string(type='decimal',size=5)
	if not app_data.get('custom_user_id'):
		app_data['custom_user_id'] = str(random.randint(30000,39999))
	if not app_data.get('ID'):
		app_data['ID']=util.get_random_string(type='all',size=28)


	print '----------------------------- APSALAR Resolve ---------------------------'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_Resolve)

	print '----------------------------- APSALAR START -----------------------------'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data,lag='0.0'+str(random.randint(80,99)))
	util.execute_request(**apsalar_start)


	if random.randint(1,100)<=95:
		time.sleep(random.randint(55,60))
		print '----------------------------- APSALAR EVENT ---------------------------Login--'
		value='{"TIME":"'+datetime.datetime.now().strftime("%Y\/%m\/%d %H:%M:%S")+'","ID":'+app_data.get("ID")+'}'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='Login',value=value,custom_user_id=True,t='15.703000000000001',lag='0.03600000000000000'+str(random.randint(1,8)))
		util.execute_request(**apsalar_event)

		

		if app_data.get('CharCreate')==False:
			time.sleep(random.randint(55,60))
			print '----------------------------- APSALAR EVENT ---------------------------CharCreate--'
			value='{"TIME":"'+datetime.datetime.now().strftime("%Y\/%m\/%d %H:%M:%S")+'","ID":"'+app_data.get("ID")+'","PID":'+app_data.get("PID")+',"NICKNAME":"'+util.get_random_string(type='char_small',size=5)+'"}'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='CharCreate',value=value,custom_user_id=True,t='256.'+str(random.randint(100,200)),lag='0.0'+str(random.randint(30,50)),seq='2')
			util.execute_request(**apsalar_event)
			app_data['CharCreate']=True


		
		if app_data.get('CharCreate')==True:
			time.sleep(random.randint(8,10))
			print '----------------------------- APSALAR EVENT ----------------------------CharLogin-'
			value='{"TIME":"'+datetime.datetime.now().strftime("%Y\/%m\/%d %H:%M:%S")+'","ID":"'+app_data.get("ID")+'","PID":'+app_data.get("PID")+'}'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='CharLogin',value=value,custom_user_id=True,t='257.'+str(random.randint(700,900)),lag='0.'+str(random.randint(500,800)),seq='3')
			util.execute_request(**apsalar_event)


			call_pattern(campaign_data,app_data,device_data,type1='open',day=day)


			if  purchase.isPurchase(app_data,day,advertiser_demand=10):

				if random.randint(1,100)<=70:			
					product_name=u'신규 유저 패키지'.encode('UTF-8')
					price='5500'
				elif random.randint(1,100)>70 and  random.randint(1,100)<=90:
					
					product_name=u'레드 다이아 묶음'.encode('UTF-8')
					price='9900'
				else:
					
					product_name=u'첫 구매 패키지'.encode('UTF-8')
					price='11000'

				if not app_data.get('purchase_token'):
					app_data['purchase_token']=util.get_random_string('hex',24)+"."+util.get_random_string('google_token',140)

				print '----------------------------- APSALAR EVENT ----------------------------purchase-'
				value='{"pcc":"KRW","r":'+price+',"pk":"'+app_data.get('purchase_token')+'","pn":"'+product_name+'","pc":"cash_item","pq":1,"pp":'+price+'}'	
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='__iap__',value=value,custom_user_id=True,t='257.'+str(random.randint(700,900)),lag='0.'+str(random.randint(500,800)),seq='3')
				util.execute_request(**apsalar_event)

				transaction="GPA."+str(util.get_random_string('decimal',4))+"-"+str(util.get_random_string('decimal',4))+"-"+str(util.get_random_string('decimal',4))+"-"+str(util.get_random_string('decimal',5))
				print '----------------------------- APSALAR EVENT ----------------------------store_result'
				value='{"TIME":"'+datetime.datetime.now().strftime("%Y\/%m\/%d %H:%M:%S")+'","ID":"'+app_data.get("ID")+'","PID":'+app_data.get("PID")+',"TRAN_ID":"'+str(transaction)+'","GOODS":"'+str(random.randint(1,9))+'"}'	
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='STORE_RESULT',value=value,custom_user_id=True,t='257.'+str(random.randint(700,900)),lag='0.'+str(random.randint(500,800)),seq='3')
				util.execute_request(**apsalar_event)



	
	return {'status':True}


def call_pattern(campaign_data,app_data,device_data,type1='install',day=0):
	# def_level(app_data)
	if not app_data.get('quest'):
		app_data['quest']=1

	if not app_data.get('level'):
		app_data['level']=0

	if not app_data.get('skill'):
		app_data['skill']=False

	if not app_data.get('counter'):
		app_data['counter']=0
	
	if type1== 'install':	
		level_clear= random.choice([12,12,12,13,13,13,14,14,15,15])
		
	else:
		if day==1:
			level_clear= random.choice([10,10,10,11,11,11,12,12,12,13])
		else:
			level_clear= random.choice([8,8,8,8,9,9,9,10,10,10])


	level_list=[2,2,3,3,4,12,5,6,6,5,5,5,6,4,12,7,7,10,8,10,14]
	

	for i in range(0,level_clear):
		print "----------------------------------------------------------------------------"
		print level_clear
		
		if random.randint(1,100)<95:
			time.sleep(random.choice([1,1,1,15,15,30,30,60,60,60,120]))
			print '----------------------------- APSALAR EVENT -----------------------------"NUM":'+str(app_data.get("quest"))
			value='{"TIME":"'+datetime.datetime.now().strftime("%Y\/%m\/%d %H:%M:%S")+'","ID":"'+app_data.get("ID")+'","PID":'+app_data.get("PID")+',"NUM":'+str(app_data.get("quest"))+'}'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='Quest',value=value,custom_user_id=True,t='15.703000000000001',lag='0.03600000000000000'+str(random.randint(1,8)))
			util.execute_request(**apsalar_event)
			app_data['counter']+=1
			print app_data['counter']

			app_data['quest']+=1

			val=random.choice([2,2,2,2,2,3,3,3,4,4])

			if app_data.get('counter')==val:
				print "level---start----counter----------"+str(app_data.get('counter'))
				print val
				app_data['counter']=0
				print "---counter---------"+str(app_data.get('counter'))
				if app_data.get('level')<=len(level_list)-1:
					time.sleep(random.randint(60,200))
					print '----------------------------- APSALAR EVENT -----------------------------LEVEL:'+str(level_list[app_data.get("level")])
					value='{"TIME":"'+datetime.datetime.now().strftime("%Y\/%m\/%d %H:%M:%S")+'","ID":"'+app_data.get("ID")+'","PID":'+app_data.get("PID")+',"LEVEL":'+str(level_list[app_data.get("level")])+'}'	
					apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='LevelUp',value=value,custom_user_id=True,t='15.703000000000001',lag='0.03600000000000000'+str(random.randint(1,8)))
					util.execute_request(**apsalar_event)
					app_data['level']+=1

				else:
					print "chill'...level in progress"
					break	

			if app_data.get('counter')>=4:
				app_data['counter']=0	
			
			if app_data.get('level')>3 and app_data.get('skill')==False:
				time.sleep(random.randint(100,120))
				print '----------------------------- APSALAR EVENT -----------------------------SKILL:'+str(level_list[app_data.get("level")])
				value='{"TIME":"'+datetime.datetime.now().strftime("%Y\/%m\/%d %H:%M:%S")+'","ID":"'+app_data.get("ID")+'","PID":'+app_data.get("PID")+',"SKILL":4}'	
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,event_name='SkillUp',value=value,custom_user_id=True,t='15.703000000000001',lag='0.03600000000000000'+str(random.randint(1,8)))
				util.execute_request(**apsalar_event)
				app_data['skill']=True

		
		
	print "i:"+str(i)
	print str(app_data.get('level'))
	print "maxlevel:"+str(level_clear)

#########################################
#										#
# 			AppSalar funcation 			#
#										#
#########################################

def apsalarResolve(campaign_data, app_data, device_data):
	url = 'http://e-ssl.apsalar.com/api/v1/resolve'
	method = 'get'
	headers = {
		'User-Agent': 'Singular/SDK-v9.2.5.PROD',
		'Accept-Encoding': 'gzip'
	}
	
	params ='a='+campaign_data.get('apsalar').get('key')+'&c=wifi&i='+campaign_data.get('package_name')+'&k=AIFA&lag=0.00'+str(random.randint(1,9))+'&p='+campaign_data.get('sdk')+'&pi=1&rt=json&u='+device_data.get('adid')+'&v='+device_data.get('os_version')

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarStart(campaign_data, app_data, device_data,ri=False,lag='',flag=False):
	#generate_gcm_id(app_data)

	url = 'http://e-ssl.apsalar.com/api/v1/start'
	method = 'get'
	headers = {
		'User-Agent': 'Singular/SDK-v9.2.5.PROD', 
		'Accept-Encoding': 'gzip'
	}
	
	if not app_data.get('s'):
		app_data['s'] = str(int(time.time()*1000)-random.randint(5,10))

	params ='a='+campaign_data.get('apsalar').get('key')+'&ab='+device_data.get('cpu_abi')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&br='+device_data.get('brand')+'&c=wifi'+'&current_device_time='+str(int(time.time()*1000))+'&ddl_enabled=true&ddl_to=60&de='+device_data.get('device')+'&device_type='+device_data.get('device_type')+'&dnt=0&i='+campaign_data.get('package_name')+'&install_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&is=true&k=AIFA&lag='+lag+'&lc='+device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')+'&ma='+device_data.get('brand').upper()+'&mo='+device_data.get('model')+'&n='+campaign_data.get('app_name')+'&p='+campaign_data.get('sdk')+'&pr='+device_data.get('product')+'&rt=json&s='+app_data.get('s')+'&sdk='+campaign_data.get('apsalar').get('version')+'&src=com.android.vending&u='+device_data.get('adid')+'&update_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&v='+device_data.get('os_version')

	if flag:
		params ='a='+campaign_data.get('apsalar').get('key')+'&ab='+device_data.get('cpu_abi')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&br='+device_data.get('brand')+'&c=wifi'+'&current_device_time='+str(int(time.time()*1000))+'&ddl_enabled=true&ddl_to=60&de='+device_data.get('device')+'&device_type='+device_data.get('device_type')+'&dnt=0&i='+campaign_data.get('package_name')+'&install_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&k=AIFA&lag='+lag+'&lc='+device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')+'&ma='+device_data.get('brand').upper()+'&mo='+device_data.get('model')+'&n='+campaign_data.get('app_name')+'&p='+campaign_data.get('sdk')+'&pr='+device_data.get('product')+'&rt=json&s='+app_data.get('s')+'&sdk='+campaign_data.get('apsalar').get('version')+'&src=com.android.vending&u='+device_data.get('adid')+'&update_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&v='+device_data.get('os_version')

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarEvent(campaign_data, app_data, device_data,event_name,value='',custom_user_id=False,t='',lag='',seq='1'):
	# custom_user_id(app_data)
	url = 'http://e-ssl.apsalar.com/api/v1/event'

	method = 'get'
	headers = {
		'User-Agent':'Singular/SDK-v9.2.5.PROD',
		'Accept-Encoding': 'gzip'
	}

	s=str(int(time.time()*1000))

	if custom_user_id:
		params='a='+campaign_data.get('apsalar').get('key')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&c=wifi&custom_user_id='+app_data.get('custom_user_id')+'&e='+value+'&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+lag+'&n='+event_name+'&p='+campaign_data.get('sdk')+'&rt=json&s='+s+'&sdk='+campaign_data.get('apsalar').get('version')+'&seq='+seq+'&t='+t+'&u='+device_data.get('adid')
	else:
	    params='a='+campaign_data.get('apsalar').get('key')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&c=wifi&e='+value+'&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+lag+'&n='+event_name+'&p='+campaign_data.get('sdk')+'&rt=json&s='+s+'&sdk='+campaign_data.get('apsalar').get('version')+'&seq='+seq+'&t='+t+'&u='+device_data.get('adid')


	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }



#########################################
#										#
# 			Extra funcation 			#
#										#
#########################################

def cal_seq(app_data):
	if not app_data.get('seq'):
		app_data['seq'] = 1
	else:
		app_data['seq'] = app_data.get('seq')+1
	return app_data.get('seq')

def user_agent(campaign_data,app_data,device_data):
	if int(device_data.get("sdk")) >19:
		app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'	
	return app_data.get('ua')

# def cal_screen_size(device_data):
# 	dots = device_data.get('dpi')
# 	if (dots <= 120):
# 		screen_size = "small"
# 	elif (dots <= 160):
# 		screen_size = "normal"
# 	elif (dots <= 240):
# 		screen_size = "large"
# 	else:

# 		screen_size ="xlarge"

# 	return screen_size

def current_location():
	
	url='https://api.letgo.com/api/iplookup.json'
	header={
			'User-Agent': 'okhttp/2.3.0'
			}
	params={}
	data={}
	
	return {'url': url, 'httpmethod': 'get', 'headers': header, 'params': params, 'data': data}

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def generateLag():
	return (random.randint(10,90)*0.001)

def generate_t():
	return "%.3f" %(random.uniform(4500,5200))

def generate_t1():
	return random.randint(300,1000)

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())
#########################################
#										#
# 			Util funcation 				#
#										#
#########################################

def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)


def custom_user_id(app_data):
	if not app_data.get('custom_user_id'):
		app_data['custom_user_id']= '12035'+ str(random.randint(1000,9999))
	return app_data.get('custom_user_id')

def get_gcmToken(app_data):
	if not app_data.get('push_token'):
		app_data['push_token']='APA91b'+''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(198))
	return app_data.get('push_token') 
	