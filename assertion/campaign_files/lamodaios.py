
# -*- coding: UTF-8 -*-
from sdk import getsleep
import random,time,datetime,uuid,json,string,urllib, hashlib
from sdk import util,installtimenew,purchase
import clicker,Config



campaign_data = {
	'package_name' : 'com.lamoda.ios',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'ctr':6,
	'app_name' : 'Lamoda',
	'app_id':'777645417',
	'app_version_code': '3570001',#'3560002',#3550001, '3540001',#'3530101',#'3520101',#'3520001',#'3510101',#'3510001',#'3500101',#'3490001',#'3480001',#'3470101',#'3460001',#'3440001',#'3420001',#'3380001',#3340202,'3340101',#'3340003','3330001',#'3240001',3300101
	'app_version_name': '3.57.0',#'3.56.0',#3.55.0, '3.54.0',#'3.53.1',#'3.52.1',#'3.52.0',#'3.51.1',#'3.51.0',#'3.50.1',#'3.49.0',#'3.48.0',#'3.47.1',#'3.46.0',#'3.44.0',#'3.42.0',#'3.38.0',#3.34.2,'3.34.1',#'3.34.0','3.33.0',#'3.24.0',3.31.0
	'supported_countries': 'WW',
	'app_size': 142.1,#132.5, 136.1,#135.8,#97.9,#96.2,#96.1,#96.0,
	'device_targeting':True,
	'X-NewRelic-ID': 'UwYOVl5aGwAHVlNQDgk=',
	'tracker': 'adjust',
	'sdk' : 'ios',
	'supported_os': '10.0',#'7.0',
	'app_updated_at': '2019-09-21T00:44:32.000Z',#'2019-09-17T22:01:35.000Z',#'2019-09-06T16:00:25.000Z, 2019-08-22T18:35:13.000Z',#'2019-08-15T17:47:28.000Z',#'2019-07-29T21:14:22.000Z',#'2019-07-24T15:16:53.000Z',#'2019-07-20T18:55:05.000Z',#'2019-07-19T15:50:22.000Z',#'2019-06-24T21:56:49.000Z',#'2019-06-07T14:24:27.000Z',#'2019-05-24T20:54:00.000Z',#'2019-05-16T20:00:41.000Z',#'2019-04-18T22:50:44.000Z',#'2019-03-26T23:06:14.000Z',#'2019-03-11T18:38:51.000Z',#'2018-12-27T17:13:21.000Z',#2018-10-31T20:42:43.000Z,'2018-10-25T15:45:56.000Z',#'2018-10-19T20:26:00.000Z','2018-10-05T21:49:04.000Z',
	'adjust':{
				'app_token': 'en25k6dvf3d9',
				'sdk':'ios4.18.2',#'ios4.14.1',#'ios4.12.1',ios4.17.1
				'secret_key': '143345831210858244821301197599746770270',#'58304179980683031413468283491438283543',
				'secret_id'	: '2',#'1',
			},
	'mobile_collector':{
							'mobile_collector_version':'6.3.0',

	},
	'retention':{
					1:25,
					2:24,
					3:24,
					4:23,
					5:22,
					6:22,
					7:21,
					8:20,
					9:20,
					10:20,
					11:19,
					12:19,
					13:18,
					14:17,
					15:17,
					16:17,
					17:16,
					18:15,
					19:14,
					20:13,
					21:12,
					22:11,
					23:10,
					24:9,
					25:8,
					26:7,
					27:6,
					28:6,
					29:5,
					30:4,
					31:3,
					32:2,
					33:2,
					34:1,
					35:1,
	}
}

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	
	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')
		
def make_subsession_count(app_data,device_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 1
	else:
		app_data['subsession_count'] += 1
		
	return app_data.get('subsession_count')



###########################################################
#						INSTALL							  #
###########################################################
def install(app_data, device_data):

	print 'please wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os='ios')
	make_session_count(app_data,device_data)
	make_subsession_count(app_data,device_data)

	if not app_data.get('sec'):
		make_sec(app_data,device_data)	

	if not app_data.get('lid'):
		app_data['lid'] = util.get_random_string('all',23) + '='

	if not app_data.get('gender'):
		app_data['gender']=random.choice(['male','female'])

	if not app_data.get('cart_list'):
		app_data['cart_list']=[]

	###########		Initialize		############

	if not app_data.get('trg_feed'):
		app_data['trg_feed']=str(util.get_random_string('decimal',7))+'_'+str(util.get_random_string('decimal',1))


	contentView_call = False
	app_data['logout']=False
	app_data['signup']=False

	global product_list

	product_list=[{'sku': 'SA032AUOKU30', 'price': '359.00', 'brand': 'Salton Professional'}, {'sku': 'KA009LUASG26', 'price': '599.00', 'brand': 'Kativa'}, {'sku': 'SA032AUDBWY6', 'price': '239.00', 'brand': 'Salton Professional'}, {'sku': 'GE633LUAXQH8', 'price': '590.00', 'brand': 'Gezatone'}, {'sku': 'IC647CMDLOW4', 'price': '475.00', 'brand': 'Icepeak'}, {'sku': 'SA049AUCPC50', 'price': '385.00', 'brand': 'Salamander Professional'}, {'sku': 'BE099AMACAL4', 'price': '485.00', 'brand': 'Beppi'}, {'sku': 'MP002XM050R4', 'price': '199.00', 'brand': 'Befree'}, {'sku': 'MP002XM1ZIHT', 'price': '599.00', 'brand': 'Befree'}, {'sku': 'SA032AUBV386', 'price': '296.00', 'brand': 'Salton Professional'}, {'sku': 'RO049LUEMMF6', 'price': '480.00', 'brand': 'R.O.C.S.'}, {'sku': 'SA033AUDBWE2', 'price': '269.00', 'brand': 'Salton Feet Comfort'}, {'sku': 'SA049AUCYCW5', 'price': '770.00', 'brand': 'Salamander Professional'}, {'sku': 'LE034LWOWE64', 'price': '590.00', 'brand': 'Levrana'}, {'sku': 'SA032AUDBWY1', 'price': '395.00', 'brand': 'Salton Professional'}, {'sku': 'ON013EMDJWH3', 'price': '490.00', 'brand': 'Only & Sons'}, {'sku': 'SA032AUDBWY4', 'price': '239.00', 'brand': 'Salton Professional'}, {'sku': 'SA049AUBMBH1', 'price': '319.00', 'brand': 'Salamander Professional'}, {'sku': 'PR030EMECNK1', 'price': '590.00', 'brand': 'Produkt'}, {'sku': 'SA032AUDBWY5', 'price': '84.00', 'brand': 'Salton Professional'}, {'sku': 'SA033AUDBWE5', 'price': '395.00', 'brand': 'Salton Feet Comfort'}, {'sku': 'LO006LMFJU61', 'price': '500.00', 'brand': "L'Oreal Paris"}, {'sku': 'SA032AUOKU29', 'price': '239.00', 'brand': 'Salton Professional'}, {'sku': 'SA032AUDKQ07', 'price': '339.00', 'brand': 'Salton Professional'}, {'sku': 'PI022GMCURH3', 'price': '595.00', 'brand': 'Piazza Italia'}, {'sku': 'MP002XM20LZ4', 'price': '199.00', 'brand': 'Befree'}, {'sku': 'MA191BUDKZY1', 'price': '420.00', 'brand': 'MakeCase'}, {'sku': 'DI042DMXIK12', 'price': '590.00', 'brand': 'Dimanche'}, {'sku': 'SA049AUBB345', 'price': '269.00', 'brand': 'Salamander Professional'}, {'sku': 'KA042DWDPFM5', 'price': '11020.00', 'brand': 'Kate Spade'},{'sku': 'MO044DMCSVO7', 'price': '199.00', 'brand': 'Modis'}, {'sku': 'AT006CUDFCK8', 'price': '445.00', 'brand': 'Atributika & Club™'}, {'sku': 'MP002XW15EA8', 'price': '149.00', 'brand': 'diva'}, {'sku': 'MP002XW0YJ61', 'price': '239.00', 'brand': 'diva'}, {'sku': 'MP002XW0TUAH', 'price': '259.00', 'brand': 'diva'}, {'sku': 'VE003DWMXC93', 'price': '224.00', 'brand': 'Venera'}, {'sku': 'MP002XW14T7N', 'price': '210.00', 'brand': 'Forte St.Petersburg'}, {'sku': 'MP002XW0TUAG', 'price': '259.00', 'brand': 'diva'}, {'sku': 'DI006DWILM14', 'price': '395.00', 'brand': 'diva'}, {'sku': 'MP002XU02EI4', 'price': '270.00', 'brand': 'Forte St.Petersburg'}, {'sku': 'MP002XW0R5NE', 'price': '319.00', 'brand': 'Homsu'}, {'sku': 'MP002XU0107F', 'price': '277.00', 'brand': 'Franchesco Mariscotti'}, {'sku': 'IN020LWDT030', 'price': '190.00', 'brand': 'invisibobble'}, {'sku': 'AT006CUDFCM7', 'price': '490.00', 'brand': 'Atributika & Club™'}, {'sku': 'MP002XW1716E', 'price': '194.00', 'brand': 'diva'}, {'sku': 'MP002XU02GUE', 'price': '400.00', 'brand': 'Duffy'}, {'sku': 'MP002XW0ITWF', 'price': '250.00', 'brand': 'Befree'}, {'sku': 'MO044DWBKJI5', 'price': '229.00', 'brand': 'Modis'}, {'sku': 'MP002XU0E00T', 'price': '449.00', 'brand': 'With Love. Moscow'}, {'sku': 'MP002XU0E987', 'price': '249.00', 'brand': 'D.Angeny'}, {'sku': '00000BUACLM1', 'price': '149.00', 'brand': 'ALLBrands'}, {'sku': 'MP002XW1918D', 'price': '399.00', 'brand': 'Aiyony Macie'}, {'sku': 'MO044DMBASC4', 'price': '249.00', 'brand': 'Modis'}, {'sku': 'MP002XM240GP', 'price': '290.00', 'brand': 'Zasport'}]

	
	session_createdAt=get_date(app_data,device_data)
	attribution_createdAt=get_date(app_data,device_data)
	time.sleep(random.randint(2,5))
	click_createdAt=get_date(app_data,device_data)
	click_sentAt=get_date(app_data,device_data)

	time.sleep(random.randint(2,5))

	session_sentAt=get_date(app_data,device_data)

	time.sleep(random.randint(8,12))	
	
	attribution_sentAt=get_date(app_data,device_data)


	app_data['time_passes'] = int(time.time())	
	
	app_data['adjust_call_time']=int(time.time())
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	global M_C_V
	M_C_V=str(uuid.uuid4()).upper()
	

	return_userinfo(app_data)



	print '\nAdjust : selfcall___________________________app_site'
	request=app_site(campaign_data, app_data, device_data,c='by')
	util.execute_request(**request)

	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data, click_createdAt,click_sentAt)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)
	
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,session_createdAt, session_sentAt)
	util.execute_request(**adjustSession)

	time.sleep(random.randint(1,4))
	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,attribution_createdAt, attribution_sentAt,initiated_by='sdk')
	util.execute_request(**request)

	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,attribution_createdAt, attribution_sentAt,initiated_by='backend')
	util.execute_request(**request)

	time.sleep(random.randint(10,15))
	if random.randint(1,100)<=90:
		print '\nAdjust : sdk_info____________________________________'
		request=adjust_sdkInfo(campaign_data, app_data, device_data,t1=1,t2=2)
		util.execute_request(**request)	

	if random.randint(1,100)<=95:

		time.sleep(random.randint(1,5))
		push_notification(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		choose_country(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		push_notification(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		homepage(campaign_data, app_data, device_data)


		if random.randint(1,100)<=50:
			time.sleep(random.randint(60,90))
			choose=random.choice(['signup','signup','signup','signup','signup','login','login','login'])
			if choose=='signup':
				signup_Event(campaign_data, app_data, device_data)
			else:
				login(campaign_data, app_data, device_data)
			app_data['signup']=True

		for _ in range(0,random.randint(1,3)):

			app_data['selected_product']=random.choice(product_list)

			
			if random.randint(1,100)<=60:
				time.sleep(random.randint(10,15))
				click_category(campaign_data, app_data, device_data)
				contentView_call=True

			if random.randint(1,100)<=30:
				time.sleep(random.randint(5,10))
				search_Item(campaign_data, app_data, device_data)
				
				contentView_call=True

			if contentView_call ==True and random.randint(1,100)<=50: ## can be 95
				time.sleep(random.randint(5,15))
				contentList_View(campaign_data, app_data, device_data)
				if random.randint(1,100)<=95:
					
					time.sleep(random.randint(3,6))
					content_View(campaign_data, app_data, device_data)
			
					if random.randint(1,100)<=53:
						time.sleep(random.randint(1,5))
						addTo_Wishlist(campaign_data, app_data, device_data)

						if random.randint(1,100)<=80:
							time.sleep(random.randint(1,5))
							view_Wishlist(campaign_data, app_data, device_data)


						if random.randint(1,100)<=10:
							time.sleep(random.randint(1,5))
							removeFrom_Wishlist(campaign_data, app_data, device_data)

					if random.randint(1,100)<=50:
						time.sleep(random.randint(5,10))
						addTo_Cart(campaign_data, app_data, device_data)				

						if random.randint(1,100)<=85:
							time.sleep(random.randint(5,10))
							view_Cart(campaign_data, app_data, device_data)

							if random.randint(1,100)<=15:
								time.sleep(random.randint(1,5))
								removefrom_cart(campaign_data, app_data, device_data)

					if random.randint(1,100)<=30:
						time.sleep(random.randint(60,65))
						review_Product(campaign_data, app_data, device_data)

		if app_data.get('signup')==True and len(app_data.get('cart_list'))>0:
			if purchase.isPurchase(app_data,1,advertiser_demand=12):       
				var=random.randint(1,3)

				while(len(app_data.get('cart_list'))>var):
					removefrom_cart(campaign_data, app_data, device_data)
				time.sleep(random.randint(5,10))
				orderInitiated(campaign_data, app_data, device_data)
				app_data['cart_list']=[]

					

		if app_data.get('signup')==True and random.randint(1,100)<=10: 
			time.sleep(random.randint(8,13))
			logout(campaign_data, app_data, device_data)
			app_data['logout']=True



	app_data['app_close_time']=int(time.time())

	return {'status':True}

###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os='ios')
		

	session_createdAt=get_date(app_data,device_data)
	attribution_createdAt=get_date(app_data,device_data)
	session_sentAt=get_date(app_data,device_data)
	attribution_sentAt=get_date(app_data,device_data)
	
	if not app_data.get('session_count'):
		make_session_count(app_data,device_data)

	if not app_data.get('subsession_count'):
		make_subsession_count(app_data,device_data)

	if not app_data.get('trg_feed'):
		app_data['trg_feed']=str(util.get_random_string('decimal',7))+'_'+str(util.get_random_string('decimal',1))
	
	if not app_data.get('time_passes'):
		app_data['time_passes'] = int(time.time()*1000)

	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())

	if not app_data.get('app_close_time'):
		app_data['app_close_time']=int(time.time())

	if not app_data.get('idfa_id'):
		if device_data.get('idfa_id'):
			app_data['idfa_id'] =device_data.get('idfa_id')			
		else:
			app_data['idfa_id'] = str(uuid.uuid4()).upper()

	if not app_data.get('idfv_id'):
		if device_data.get('idfv_id'):
			app_data['idfv_id'] =device_data.get('idfv_id')			
		else:
			app_data['idfv_id'] = str(uuid.uuid4()).upper()

	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('sec'):
		make_sec(app_data,device_data)
		
	

	if not app_data.get('cart_list'):
		app_data['cart_list']=[]

	

	if not app_data.get('gender'):
		app_data['gender']=random.choice(['male','female'])

	

	if not app_data.get('email'):
		return_userinfo(app_data)

	contentView_call = False
	if not app_data.get('logout'):
		app_data['logout']=False
	if not app_data.get('signup'):
		app_data['signup']=False

	global product_list

	product_list=[{'sku': 'SA032AUOKU30', 'price': '359.00', 'brand': 'Salton Professional'}, {'sku': 'KA009LUASG26', 'price': '599.00', 'brand': 'Kativa'}, {'sku': 'SA032AUDBWY6', 'price': '239.00', 'brand': 'Salton Professional'}, {'sku': 'GE633LUAXQH8', 'price': '590.00', 'brand': 'Gezatone'}, {'sku': 'IC647CMDLOW4', 'price': '475.00', 'brand': 'Icepeak'}, {'sku': 'SA049AUCPC50', 'price': '385.00', 'brand': 'Salamander Professional'}, {'sku': 'BE099AMACAL4', 'price': '485.00', 'brand': 'Beppi'}, {'sku': 'MP002XM050R4', 'price': '199.00', 'brand': 'Befree'}, {'sku': 'MP002XM1ZIHT', 'price': '599.00', 'brand': 'Befree'}, {'sku': 'SA032AUBV386', 'price': '296.00', 'brand': 'Salton Professional'}, {'sku': 'RO049LUEMMF6', 'price': '480.00', 'brand': 'R.O.C.S.'}, {'sku': 'SA033AUDBWE2', 'price': '269.00', 'brand': 'Salton Feet Comfort'}, {'sku': 'SA049AUCYCW5', 'price': '770.00', 'brand': 'Salamander Professional'}, {'sku': 'LE034LWOWE64', 'price': '590.00', 'brand': 'Levrana'}, {'sku': 'SA032AUDBWY1', 'price': '395.00', 'brand': 'Salton Professional'}, {'sku': 'ON013EMDJWH3', 'price': '490.00', 'brand': 'Only & Sons'}, {'sku': 'SA032AUDBWY4', 'price': '239.00', 'brand': 'Salton Professional'}, {'sku': 'SA049AUBMBH1', 'price': '319.00', 'brand': 'Salamander Professional'}, {'sku': 'PR030EMECNK1', 'price': '590.00', 'brand': 'Produkt'}, {'sku': 'SA032AUDBWY5', 'price': '84.00', 'brand': 'Salton Professional'}, {'sku': 'SA033AUDBWE5', 'price': '395.00', 'brand': 'Salton Feet Comfort'}, {'sku': 'LO006LMFJU61', 'price': '500.00', 'brand': "L'Oreal Paris"}, {'sku': 'SA032AUOKU29', 'price': '239.00', 'brand': 'Salton Professional'}, {'sku': 'SA032AUDKQ07', 'price': '339.00', 'brand': 'Salton Professional'}, {'sku': 'PI022GMCURH3', 'price': '595.00', 'brand': 'Piazza Italia'}, {'sku': 'MP002XM20LZ4', 'price': '199.00', 'brand': 'Befree'}, {'sku': 'MA191BUDKZY1', 'price': '420.00', 'brand': 'MakeCase'}, {'sku': 'DI042DMXIK12', 'price': '590.00', 'brand': 'Dimanche'}, {'sku': 'SA049AUBB345', 'price': '269.00', 'brand': 'Salamander Professional'}, {'sku': 'KA042DWDPFM5', 'price': '11020.00', 'brand': 'Kate Spade'},{'sku': 'MO044DMCSVO7', 'price': '199.00', 'brand': 'Modis'}, {'sku': 'AT006CUDFCK8', 'price': '445.00', 'brand': 'Atributika & Club™'}, {'sku': 'MP002XW15EA8', 'price': '149.00', 'brand': 'diva'}, {'sku': 'MP002XW0YJ61', 'price': '239.00', 'brand': 'diva'}, {'sku': 'MP002XW0TUAH', 'price': '259.00', 'brand': 'diva'}, {'sku': 'VE003DWMXC93', 'price': '224.00', 'brand': 'Venera'}, {'sku': 'MP002XW14T7N', 'price': '210.00', 'brand': 'Forte St.Petersburg'}, {'sku': 'MP002XW0TUAG', 'price': '259.00', 'brand': 'diva'}, {'sku': 'DI006DWILM14', 'price': '395.00', 'brand': 'diva'}, {'sku': 'MP002XU02EI4', 'price': '270.00', 'brand': 'Forte St.Petersburg'}, {'sku': 'MP002XW0R5NE', 'price': '319.00', 'brand': 'Homsu'}, {'sku': 'MP002XU0107F', 'price': '277.00', 'brand': 'Franchesco Mariscotti'}, {'sku': 'IN020LWDT030', 'price': '190.00', 'brand': 'invisibobble'}, {'sku': 'AT006CUDFCM7', 'price': '490.00', 'brand': 'Atributika & Club™'}, {'sku': 'MP002XW1716E', 'price': '194.00', 'brand': 'diva'}, {'sku': 'MP002XU02GUE', 'price': '400.00', 'brand': 'Duffy'}, {'sku': 'MP002XW0ITWF', 'price': '250.00', 'brand': 'Befree'}, {'sku': 'MO044DWBKJI5', 'price': '229.00', 'brand': 'Modis'}, {'sku': 'MP002XU0E00T', 'price': '449.00', 'brand': 'With Love. Moscow'}, {'sku': 'MP002XU0E987', 'price': '249.00', 'brand': 'D.Angeny'}, {'sku': '00000BUACLM1', 'price': '149.00', 'brand': 'ALLBrands'}, {'sku': 'MP002XW1918D', 'price': '399.00', 'brand': 'Aiyony Macie'}, {'sku': 'MO044DMBASC4', 'price': '249.00', 'brand': 'Modis'}, {'sku': 'MP002XM240GP', 'price': '290.00', 'brand': 'Zasport'}]

	# ################### CALLS #####################

	
	if not app_data.get('lid'):
		selfcall1(campaign_data, app_data, device_data)

	

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,session_createdAt, session_sentAt,typee='open')
	util.execute_request(**adjustSession)

	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,attribution_createdAt, attribution_sentAt)
	util.execute_request(**request)
	# #requests.head(request.get('url'),headers = request.get('headers'), params = request.get('params'), data = request.get('data'), verify=False)

	

	if random.randint(1,100)<=90:

		time.sleep(random.randint(1,5))
		push_notification(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		choose_country(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		push_notification(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		homepage(campaign_data, app_data, device_data)


		if random.randint(1,100)<=50 and app_data.get('signup')==False:
			time.sleep(random.randint(60,90))
			choose=random.choice(['signup','signup','signup','signup','signup','login','login','login'])
			if choose=='signup':
				signup_Event(campaign_data, app_data, device_data)
			else:
				login(campaign_data, app_data, device_data)
			app_data['signup']=True

		for _ in range(0,random.randint(1,3)):

			app_data['selected_product']=random.choice(product_list)

			
			if random.randint(1,100)<=55:
				time.sleep(random.randint(10,15))
				click_category(campaign_data, app_data, device_data)
				contentView_call=True

			if random.randint(1,100)<=25:
				time.sleep(random.randint(5,10))
				search_Item(campaign_data, app_data, device_data)
				
				contentView_call=True

			if contentView_call ==True and random.randint(1,100)<=80: ## can be 95
				time.sleep(random.randint(5,15))
				contentList_View(campaign_data, app_data, device_data)
				if random.randint(1,100)<=95:
					
					time.sleep(random.randint(1,5))
					content_View(campaign_data, app_data, device_data)
			
					if random.randint(1,100)<=53:
						time.sleep(random.randint(5,10))
						addTo_Wishlist(campaign_data, app_data, device_data)

						if random.randint(1,100)<=80:
							time.sleep(random.randint(1,5))
							view_Wishlist(campaign_data, app_data, device_data)


						if random.randint(1,100)<=10:
							time.sleep(random.randint(1,5))
							removeFrom_Wishlist(campaign_data, app_data, device_data)

					if random.randint(1,100)<=45:
						time.sleep(random.randint(5,10))
						addTo_Cart(campaign_data, app_data, device_data)				

						if random.randint(1,100)<=85:
							time.sleep(random.randint(5,10))
							view_Cart(campaign_data, app_data, device_data)

							if random.randint(1,100)<=15:
								time.sleep(random.randint(1,5))
								removefrom_cart(campaign_data, app_data, device_data)

						# if random.randint(1,100)<=70 and app_data.get('signup')==True:
						# 	time.sleep(random.randint(1,3))
						# 	orderInitiated(campaign_data, app_data, device_data)

					if random.randint(1,100)<=20:
						time.sleep(random.randint(100,120))
						review_Product(campaign_data, app_data, device_data)

		if app_data.get('signup')==True and len(app_data.get('cart_list'))>0:
			if purchase.isPurchase(app_data,day,advertiser_demand=17):  
				var=random.randint(1,3)
				while(len(app_data.get('cart_list'))>var):
					removefrom_cart(campaign_data, app_data, device_data)
				

				time.sleep(random.randint(60,65))
				orderInitiated(campaign_data, app_data, device_data)
				app_data['cart_list']=[]


		if app_data.get('signup')==True and random.randint(1,100)<=10 and app_data.get('logout')==False:
			time.sleep(random.randint(5,5))
			logout(campaign_data, app_data, device_data)
			app_data['logout']=True

		


	app_data['app_close_time']=int(time.time())

	
	
	return {'status':True}


###########################################################
#						ADJUST							  #
###########################################################

def push_notification(campaign_data, app_data, device_data):

	print '\nAdjust Event - Push Notification'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	app_data['shop_country'] = 'ru' #country[choose]
	app_data['currency'] = 'RUB' #currency1[choose]

	callback_params={"app_version":campaign_data.get('app_version_code'),"lid":app_data.get('lid'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}

	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')
		partner_params={"name":"uio_push_token","value":json.dumps({"push_token":app_data.get('pushToken_Event')})}	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'bb90w5',partner_params=str(json.dumps(partner_params)),callback_params=str(json.dumps(callback_params)),t1=1,t2=2)
	util.execute_request(**request)


def choose_country(campaign_data, app_data, device_data):

	print '\nAdjust Event - choose country'
	interval 		= random.randint(1,3)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}

	partner_params={"app_version":campaign_data.get('app_version_code'),"shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform'),"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
		partner_params['gender'] = app_data.get('gender')
	
	request=adjust_event(campaign_data, app_data, device_data,'y0zy1e',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=3,t2=7)
	util.execute_request(**request)



def homepage(campaign_data, app_data, device_data):

	print '\nAdjust : HOMEPAGE EVENT____________________________1'
	interval 		= random.randint(1,3)
	set_sessionLength(app_data,length=interval)
	duration=str(random.randint(1000,99999))+'.0'

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"duration":duration,"device_type":device_data.get('device_platform')}
	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')		

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
	
	request=adjust_event(campaign_data, app_data, device_data,'a0d43g',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=7,t2=12)
	util.execute_request(**request)

def removefrom_cart(campaign_data, app_data, device_data):

	print '\nAdjust : removefrom_cart___________________________1'
	interval 		= random.randint(1,3)
	set_sessionLength(app_data,length=interval)
	duration=str(random.randint(1000,99999))+'.0'

	if len(app_data.get('cart_list'))!=0:
		print len(app_data.get('cart_list'))
		index=random.randint(0,len(app_data.get('cart_list'))-1)
		product=app_data.get('cart_list')[index].get('product')
		price=app_data.get('cart_list')[index].get('price')
		app_data.get('cart_list').pop(index)
		print len(app_data.get('cart_list'))

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"sku":product,"device_group":"Phone","shop_country":app_data.get('shop_country'),"currency":app_data.get('currency'),"device_type":app_data.get('device_platform')}
	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')		

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
	
	request=adjust_event(campaign_data, app_data, device_data,'twkelz',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=7,t2=12)
	util.execute_request(**request)


def signup_Event(campaign_data, app_data, device_data):


	print '\nAdjust : signup_Event___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	app_data['user_id']='102088'+ str(util.get_random_string('decimal',4))

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}
	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	
		
	request=adjust_event(campaign_data, app_data, device_data,'tzsel8',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)
	

	


def click_category(campaign_data, app_data, device_data):
	print '\nAdjust : click_category__________________________3'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	var=random.choice(['"https:\/\/m.lamoda.ru\/c\/4153\/default-women\/?bs=1&display_locations=all&labels=29819&pc=%D0%96%D0%90%D0%A0%D0%90&zbs_content=apps_1_ru_0208_app_mob_w_sale_80_50"','"https://m.lamoda.by/c/357/clothes-verkhnyaya-odezhda/?labels=32371&zbs_content=apps_2_def_by_2108_app_w_outwear"','"https:\/\/m.lamoda.ru\/a\/35487\/summer_makeup\/?sv=mob&limit=20&platform=ios_phone&section=woman&country=ru&stories_id=140&is_webview=1"'])

	callback_params={"app_version":campaign_data.get('app_version_code'),"lid":app_data.get('lid'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}

	partner_params={"criteo_deeplink":var,"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'), "criteo_partner_id" : campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')		

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
		partner_params['customer_id'] = app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'c7uqvy',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

def search_Item(campaign_data, app_data, device_data):

	print '\nAdjust :SEARCH ITEM EVENT___________________________1'
	keywords_List=['shoes','wallet','jeans','shirts','handbag','slippers','belt','t-shirt','socks', 'gloves']
	keyword=random.choice(keywords_List)
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","keywords":keyword,"shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}
	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'bri274',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

	###############

	time.sleep(random.randint(1,3))
	print '\nAdjust :SEARCH ITEM EVENT___________________________2'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}
	partner_params={"criteo_partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"keywords":keyword,"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}
	
	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')		

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
		partner_params['customer_id']=app_data.get('user_id')
		partner_params['user_id']=app_data.get('user_id')
	
	request=adjust_event(campaign_data, app_data, device_data,'wc9d4l',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=2,t2=4)
	util.execute_request(**request)

	################

	time.sleep(random.randint(1,3))
	print '\nAdjust :SEARCH ITEM EVENT___________________________3'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","query":keyword,"shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}
	partner_params={"gender":app_data.get('gender'),"app_version":campaign_data.get('app_version_code'),"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"query":keyword,"shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')		
		callback_params['gender']=app_data.get('gender')
		partner_params['user_id']=app_data.get('user_id')
		partner_params['gender']=app_data.get('gender')

	request=adjust_event(campaign_data, app_data, device_data,'wcbsvp',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=5,t2=8)
	util.execute_request(**request)

def contentList_View(campaign_data, app_data, device_data):
	content_list=[]
	temp=[]
	app_data['selected_product']=random.choice(product_list)

	for i in range(0,3):
		var=random.randint(0,len(product_list)-1)
		temp.append(product_list[var])
		content_list.append(product_list[var].get('sku'))
	app_data['selected_product']=random.choice(temp)

	print '\nAdjust :CONTENT LIST VIEW EVENT___________________________1'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"products":json.dumps(content_list),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}	


	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
	
	request=adjust_event(campaign_data, app_data, device_data,'ae8z35',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

	time.sleep(random.randint(1,3))
	print '\nAdjust :CONTENT LIST VIEW EVENT___________________________2'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"app_version":campaign_data.get('app_version_code'),"lid":app_data.get('lid'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}



	partner_params={"criteo_partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"criteo_p":urllib.quote(str(content_list))}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
		partner_params['customer_id']=app_data.get('user_id')
		partner_params['criteo_email_hash']=app_data.get('user_id')
	
	request=adjust_event(campaign_data, app_data, device_data,'qt765c',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=2,t2=4)
	util.execute_request(**request)

	time.sleep(random.randint(1,3))
	print '\nAdjust :CONTENT LIST VIEW EVENT___________________________3'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"app_version":campaign_data.get('app_version_code'),"lid":app_data.get('lid'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"skus":json.dumps(content_list),"device_type":device_data.get('device_platform')}



	partner_params={"app_version":campaign_data.get('app_version_code'),"shop_country":app_data.get('shop_country'),"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"skus":json.dumps(content_list),"device_type":device_data.get('device_platform')}


	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		partner_params['user_id']=app_data.get('user_id')
		partner_params['gender']=app_data.get('gender')
		

	request=adjust_event(campaign_data, app_data, device_data,'g26swb',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=2,t2=4)
	util.execute_request(**request)

def content_View(campaign_data, app_data, device_data):
	
	product=app_data.get('selected_product').get('sku')
	

	print '\nAdjust :CONTENT VIEW EVENT___________________________1'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"product":app_data.get('selected_product').get('sku'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}
	
	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
	
	request=adjust_event(campaign_data, app_data, device_data,'p7k7a4',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

	time.sleep(random.randint(1,3))
	print '\nAdjust :CONTENT VIEW EVENT___________________________2'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"app_version":campaign_data.get('app_version_code'),"lid":app_data.get('lid'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}

	
	partner_params={"criteo_partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"criteo_p":app_data.get('selected_product').get('sku')}




	

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
		partner_params['customer_id']=app_data.get('user_id')
		partner_params['criteo_email_hash']=app_data.get('user_id')
	
	request=adjust_event(campaign_data, app_data, device_data,'t9fltd',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=2,t2=4)
	util.execute_request(**request)

	time.sleep(random.randint(1,3))
	print '\nAdjust :CONTENT VIEW EVENT___________________________3'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"fb_content_id":app_data.get('selected_product').get('sku'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"fb_currency":app_data.get('currency'),"device_type":device_data.get('device_platform'),"fb_content_type":"PRODUCT"}

	
	partner_params={"trg_feed":app_data.get('trg_feed'),"fb_currency":app_data.get('currency'),"fb_content_type":"PRODUCT","fb_content_id":app_data.get('selected_product').get('sku'),"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}



	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')	

	request=adjust_event(campaign_data, app_data, device_data,'l7ota5',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=2,t2=4)
	util.execute_request(**request)

def addTo_Wishlist(campaign_data, app_data, device_data):

	if not app_data.get('wish_list'):
		app_data['wish_list']=[]


	print '\nAdjust : addTo_Wishlist___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	product=app_data.get('selected_product').get('sku')
	price=app_data.get('selected_product').get('price')
	brand=app_data.get('selected_product').get('brand')
	temp={'product':product,'price':price,'brand':brand}
	app_data.get('wish_list').append(temp)


	print "-------added in cart----------------------"
	print app_data.get('wish_list')

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","sku":product,"price":price,"shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform'),"currency":app_data.get('currency')}

	

	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'14xp6d',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

def view_Wishlist(campaign_data, app_data, device_data):
	print '\nAdjust : addTo_Wishlist___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)


	fav_list=[]

	if len(app_data.get('wish_list'))!=0:
		for i in range(len(app_data.get('wish_list'))):			
			fav_list.append(app_data.get('wish_list')[i].get('product')) 

	else:
		
		fav_list=[]

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"currency_code":app_data.get('currency'),"device_type":device_data.get('device_platform'),"skus":json.dumps(fav_list)}

	

	partner_params={"app_version":campaign_data.get('app_version_code'),"shop_country":app_data.get('shop_country'),"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"currency_code":app_data.get('currency'),"skus":json.dumps(fav_list),"device_type":device_data.get('device_platform')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'4dfgk2',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

def removeFrom_Wishlist(campaign_data, app_data, device_data):
	print '\nAdjust : removeFrom_Wishlist___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	if len(app_data.get('wish_list'))!=0:
		print len(app_data.get('wish_list'))
		index=random.randint(0,len(app_data.get('wish_list'))-1)
		product=app_data.get('wish_list')[index].get('product')
		price=app_data.get('wish_list')[index].get('price')
		app_data.get('wish_list').pop(index)
		print len(app_data.get('wish_list'))


	

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","price":price,"sku":product,"shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform'),"currency":app_data.get('currency')}

	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')		

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'wz0plz',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=3,t2=7)
	util.execute_request(**request)

def addTo_Cart(campaign_data, app_data, device_data):

	if not app_data.get('cart_list'):
		app_data['cart_list']=[]

	print '\nAdjust : ADD TO CART EVENT 1___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)
	
	product=app_data.get('selected_product').get('sku')
	price=app_data.get('selected_product').get('price')
	brand=app_data.get('selected_product').get('brand')
	temp={'product':product,'price':price,'brand':brand}
	app_data.get('cart_list').append(temp)


	print "-------added in cart----------------------"
	print app_data.get('cart_list')
	# app_data.get('cart_priceList').append(price)
	# app_data.get('cart_brandList').append(brand_list_view[index])
	# app_data.get('cart_sizeList').append(size_list_view[index])
	# app_data.get('cart_colourList').append(colour_list_view[index])


	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"sku":product,"device_group":"Phone","price":price,"shop_country":app_data.get('shop_country'),"currency":app_data.get('currency'),"device_type":device_data.get('device_platform')}		

	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}
		
	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'72mosm',partner_params=str(json.dumps(partner_params)),callback_params=str(json.dumps(callback_params)),t1=0,t2=0)
	util.execute_request(**request)

	time.sleep(random.randint(1,3))
	print '\nAdjust : ADD TO CART EVENT 2___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"fb_content_id":product,"device_group":"Phone","shop_country":app_data.get('shop_country'),"fb_currency":app_data.get('currency'),"device_type":device_data.get('device_platform'),"fb_content_type":"PRODUCT","_valueToSum":price.split('.')[0]}



	partner_params={"trg_feed":app_data.get('trg_feed'),"fb_currency":app_data.get('currency'),"fb_content_type":"PRODUCT","fb_content_id":product,"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"_valueToSum":price.split('.')[0]}


	
	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')	

	request=adjust_event(campaign_data, app_data, device_data,'822hhx',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=3,t2=6)
	util.execute_request(**request)

def view_Cart(campaign_data, app_data, device_data):
	app_data['adding_val2']='['

	print '\nAdjust : VIEW CART EVENT 1___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	
	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}	

	print app_data.get('cart_list')

	if len(app_data.get('cart_list'))!=0:
		print len(app_data.get('cart_list'))
		for i in range(0,len(app_data.get('cart_list'))):
			print "view_Cart"
			print i
			if i==0:
				callback_params['product']=str(json.dumps({'sku':app_data.get('cart_list')[i].get('product'),'currency':app_data.get('currency'),'price':app_data.get('cart_list')[i].get('price'),'quantity':'1'}))
			else:
				callback_params['product'+str(i)]=str(json.dumps({'sku':app_data.get('cart_list')[i].get('product'),'currency':app_data.get('currency'),'price':app_data.get('cart_list')[i].get('price'),'quantity':'1'}))

	else:
		print "len 0 "

	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}
		
	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'31mrml',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

	time.sleep(random.randint(1,3))
	print '\nAdjust : VIEW CART EVENT 2___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	if len(app_data.get('cart_list'))!=0:
		for i in range(0,len(app_data.get('cart_list'))):
			check=json.dumps({'i':app_data.get('cart_list')[i].get('product'),'pr':app_data.get('cart_list')[i].get('price'),'q':'1'})
			comma=''
			if not i==0:
				comma=','
			app_data['adding_val2']+=comma+ check

		app_data['adding_val2']+=']'

	else:
		app_data['adding_val2']='[]'

	


	callback_params={"app_version":campaign_data.get('app_version_code'),"lid":app_data.get('lid'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}



	partner_params={"criteo_partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"criteo_p":urllib.quote(app_data.get('adding_val2'))}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
		partner_params['customer_id']=app_data.get('user_id')
		partner_params['criteo_email_hash']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'khfymt',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=2,t2=5)
	util.execute_request(**request)
	


	time.sleep(random.randint(1,3))
	
	print '\nAdjust : VIEW CART EVENT 3___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)	

	brand_list=[]
	sku_list=[]

	if len(app_data.get('cart_list'))!=0:
		for i in range(len(app_data.get('cart_list'))):

			brand_list.append(app_data.get('cart_list')[i].get('brand')) 
			sku_list.append(app_data.get('cart_list')[i].get('product')) 

	else:
		brand_list=[]
		sku_list=[]
	

	

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"gender":app_data.get('gender'),"device_group":"Phone","brands":json.dumps(brand_list),"shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform'),"skus":json.dumps(sku_list)}


	partner_params={"gender":app_data.get('gender'),"app_version":campaign_data.get('app_version_code'),"brands":json.dumps(brand_list),"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform'),"skus":json.dumps(sku_list)}


	

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')
		partner_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'ujq0yl',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=2,t2=5)
	util.execute_request(**request)

def review_Product(campaign_data, app_data, device_data):
	print '\nAdjust :review_Product___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","sku":app_data.get('selected_product').get('sku'),"shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}

	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'5iieq6',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

def logout(campaign_data, app_data, device_data):
	print '\nAdjust : logout___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}

	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'1lx7jb',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

def login(campaign_data, app_data, device_data):
	print '\nAdjust : login___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}



	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'ycbh27',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

def orderInitiated(campaign_data, app_data, device_data):
	if not app_data.get('user_id'):
		signup_Event(campaign_data, app_data, device_data)

	date=str(time.strftime('%Y%m%d')[2:] )

	transaction_id=app_data.get('shop_country').upper()+date+'-'+str(util.get_random_string('decimal',6))

	print '\nAdjust :  ORDER INITIATED EVENT 1___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	sku_list='['

	for i in range(0,len(app_data.get('cart_list'))):
			check=str(json.dumps(app_data.get('cart_list')[i].get('product')))
			comma=''
			if not i==0:
				comma=','
			sku_list+=comma+ check

	sku_list+=']'

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform'),"skus":sku_list}

	

	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'4yiaue',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=0,t2=0)
	util.execute_request(**request)

	print '\nAdjust :  ORDER INITIATED EVENT 2___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"transaction_id":transaction_id,"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}



	for i in range(0,len(app_data.get('cart_list'))):
		if i==0:
			callback_params['product']=str(json.dumps({'sku':app_data.get('cart_list')[i].get('product'),'currency':app_data.get('currency'),'price':app_data.get('cart_list')[i].get('price'),'quantity':'1'}))
		else:
			callback_params['product'+str(i)]=str({'sku':app_data.get('cart_list')[i].get('product'),'currency':app_data.get('currency'),'price':app_data.get('cart_list')[i].get('price'),'quantity':'1'})

	partner_params={"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}

	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')		

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'y527pj',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=3,t2=8)
	util.execute_request(**request)

	print '\nAdjust :  ORDER INITIATED EVENT 3___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	callback_params={"app_version":campaign_data.get('app_version_code'),"lid":app_data.get('lid'),"device_group":"Phone","shop_country":app_data.get('shop_country'),"device_type":device_data.get('device_platform')}



	if not app_data.get('adding_val2'):	
		app_data['adding_val2']='['
		for i in range(0,len(app_data.get('cart_list'))):
			check=json.dumps({'i':app_data.get('cart_list')[i].get('product'),'pr':str(app_data.get('cart_list')[i].get('price'))+'0000','q':'1'})
			comma=''
			if not i==0:
				comma=','
			app_data['adding_val2']+=comma+ check

		app_data['adding_val2']+=']'

	partner_params={"criteo_partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"transaction_id":transaction_id,"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country'),"criteo_p":urllib.quote(app_data.get('adding_val2'))}

	

	if app_data.get('pushToken_Event'):
		callback_params['pushToken_Event']=app_data.get('pushToken_Event')	

	request=adjust_event(campaign_data, app_data, device_data,'9ir2q9',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=9,t2=15)
	util.execute_request(**request)

	print '\nAdjust :  ORDER INITIATED EVENT 4___________________________'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)

	amount=0.0

	for i in range(0,len(app_data.get('cart_list'))):
		amount=amount+float(app_data.get('cart_list')[i].get('price'))


	callback_params={"lid":app_data.get('lid'),"app_version":campaign_data.get('app_version_code'),"fb_content_id":sku_list,"device_group":"Phone","shop_country":app_data.get('shop_country'),"fb_currency":app_data.get('currency'),"device_type":device_data.get('device_platform'),"fb_content_type":"PRODUCT","_valueToSum":str(amount)}
	

	partner_params={"trg_feed":app_data.get('trg_feed'),"fb_currency":app_data.get('currency'),"fb_content_type":"PRODUCT","fb_content_id":sku_list,"_valueToSum":str(amount),"partner_id":campaign_data.get('app_name').lower()+app_data.get('shop_country')}



	if app_data.get('pushToken_Event'):
		callback_params['push_token']=app_data.get('pushToken_Event')	

	if app_data.get('user_id'):
		callback_params['user_id']=app_data.get('user_id')

	request=adjust_event(campaign_data, app_data, device_data,'1xkndr',partner_params=json.dumps(partner_params),callback_params=json.dumps(callback_params),t1=10,t2=16,revenue=str(amount))
	util.execute_request(**request)

################### SELF CALLS#################

def selfcall1(campaign_data, app_data, device_data):
	print '\nAdjust : selfcall1___________________________getCustomerBySesssion'
	request=adjust_selfcall1(campaign_data, app_data, device_data)
	output=util.execute_request(**request)
	try:
		d = output.get('res').headers.get('Set-Cookie')
		v=d.split(';')[0].split('lid=')[1]
		app_data['lid']=v
	except:
		print "Exception"
		app_data['lid']=util.get_random_string('char_all',23)+'='



def selfcall2(campaign_data, app_data, device_data):
	print '\nAdjust : selfcall2___________________________config'
	request=adjust_selfcall2(campaign_data, app_data, device_data)
	util.execute_request(**request)        

def selfcall3(campaign_data, app_data, device_data):
	print '\nAdjust : selfcall3___________________________checkin'
	request=adjust_selfcall3(campaign_data, app_data, device_data)
	util.execute_request(**request)	


def get_auth(app_data,created_at,idfa,activity_kind):
	final_str=campaign_data.get('adjust').get('secret_key')+str(activity_kind)+str(idfa)+str(created_at)
	sign=util.sha256(final_str)
	auth='Signature secret_id="'+str(campaign_data.get('adjust').get('secret_id'))+'",signature="'+str(sign)+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'
	return auth


	

def adjust_session(campaign_data, app_data, device_data,created_at,sent_at,typee='install'):
	
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'X-NewRelic-ID' : campaign_data.get('X-NewRelic-ID'),
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		}
	
	params={}

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_updated_at': campaign_data.get('app_updated_at')+device_data.get('timezone'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'connectivity_type': '2',
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': created_at,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'install_receipt': apple_receipt(),
			'installed_at': datetime.datetime.fromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_build':device_data.get('build'),
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'sent_at':	sent_at,
			'session_count':app_data.get('session_count'),
			'tracking_enabled':	1,
			'mcc':device_data.get('mcc'),
			'mnc':device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyLTE'
			
			}

	data["callback_params"]=json.dumps({"device_type":device_data.get('device_platform'),"device_group":"Phone","app_version":campaign_data.get('app_version_code')})
	
	if typee=='open':
		data['last_interval'] = int(time.time())-app_data.get('app_close_time')
		data['session_length'] = app_data.get('app_close_time')-app_data.get('adjust_call_time')
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = app_data.get('app_close_time')-app_data.get('adjust_call_time')
		app_data['adjust_call_time']=int(time.time())
	
	headers['Authorization']= get_auth(app_data,created_at=data['created_at'],idfa=data['idfa'],activity_kind='session')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adjust_sdkclick(campaign_data, app_data, device_data, created_at, sent_at):

	set_sessionLength(app_data,forced=False,length=0)
	def_sessionLength(app_data)
	# set_installedAT(app_data,device_data)
	
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'X-NewRelic-ID' : campaign_data.get('X-NewRelic-ID'),
	}
	
	params={}

	if app_data.get('adjust_call_time'):
		timeSpent=int(time.time())-app_data.get('adjust_call_time')
		app_data['timeSpent']=timeSpent
	else:
		app_data['timeSpent']=0

	data = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_updated_at':campaign_data.get('app_updated_at')+device_data.get('timezone') ,
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'connectivity_type':'2',
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at':created_at,
			'details': json.dumps({"Version3.1":{"iad-attribution":"false"}}),
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment':'production' ,			
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'last_interval': int(time.time())-app_data.get('time_passes'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_build':device_data.get('build'),
			'bundle_id': campaign_data.get('package_name'),
			'os_version': device_data.get('os_version'),
			'sent_at':	sent_at,
			'source':'iad3',
			'session_count': app_data.get('session_count'),
			"session_length":app_data.get('timeSpent'), 
			'subsession_count': app_data.get('subsession_count'),
			'time_spent': app_data.get('timeSpent'),
			'tracking_enabled':	1,
			'install_receipt': apple_receipt(),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'installed_at':datetime.datetime.fromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'mcc':device_data.get('mcc'),
			'mnc':device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyLTE'
			}



	

	data["callback_params"]=json.dumps({"device_type":device_data.get('device_platform'),"device_group":"Phone","app_version":campaign_data.get('app_version_code')})
	
	
	headers['Authorization']= get_auth(app_data,created_at=data['created_at'],idfa=data['idfa'],activity_kind='click')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
def adjust_attribution(campaign_data, app_data, device_data,created_at,sent_at,initiated_by=''):
	
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'X-NewRelic-ID' : campaign_data.get('X-NewRelic-ID'),
		}
	
	data={}

	params = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink' : 1,
			'created_at': created_at,
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'needs_response_details': 1,
			'sent_at': sent_at,
			'initiated_by':initiated_by,
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'app_version'			:campaign_data.get('app_version_code'),
			'device_name'			:device_data.get('device_platform'),
			'device_type'			:device_data.get('device_type'),
			'app_version_short':campaign_data.get('app_version_name'),
			'os_name'				:'ios',
			'os_build':	device_data.get('build'),
			'os_version'			:device_data.get('os_version'),
			'bundle_id'			:campaign_data.get('package_name'),
			}
	
	
	headers['Authorization']= get_auth(app_data,created_at=params['created_at'],idfa=params['idfa'],activity_kind='attribution')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_sdkInfo(campaign_data, app_data, device_data,t1=0,t2=0):

	created_at=datetime.datetime.fromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')	
	time.sleep(random.randint(t1,t2))
	sent_at=datetime.datetime.fromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	url = 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'X-NewRelic-ID' : campaign_data.get('X-NewRelic-ID'),
	}

	pushToken1(app_data)
	pushToken_event1(app_data)
	params={}

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink':	1,
			'created_at':created_at,
			'environment':'production' ,			
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'needs_response_details': 1,
			'sent_at':	sent_at,
			'source':'push',
			'queue_size':3,
			'push_token':app_data.get('push_token'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			}

	headers['Authorization']= get_auth(app_data,created_at=data['created_at'],idfa=data['idfa'],activity_kind='info')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


def adjust_event(campaign_data, app_data, device_data,event_token,partner_params, callback_params,t1=None,t2=None,revenue=""):

	created_at=datetime.datetime.fromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')	
	time.sleep(random.randint(t1,t2))
	sent_at=datetime.datetime.fromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	uid(app_data)		
	url = 'http://app.adjust.com/event'
	method = 'post'
	header = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept' : '*/*',
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'X-NewRelic-ID' : campaign_data.get('X-NewRelic-ID'),
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
				}

	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	timeSpent = int(time.time())-app_data.get('adjust_call_time')
	
	app_data['timeSpent'] = timeSpent
	
	data = {
			'hardware_name':device_data.get('hardware'),	
			'event_buffering_enabled':	0,
			'cpu_type':device_data.get('cpu_type'),
			'attribution_deeplink': '1',
			'os_name':'ios',
			'environment':'production',
			'needs_response_details':1,
			'event_count':app_data.get('event_count'),
			'time_spent':app_data.get('timeSpent'),
			'session_count':str(app_data.get('session_count')),
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			'created_at': created_at,
			'event_token':event_token,
			'bundle_id':campaign_data.get('package_name'),
			'subsession_count':str(app_data.get('session_count')),
			'os_version':device_data.get('os_version'),
			'app_version': campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country'),
			'language':	device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'session_length':app_data.get('timeSpent'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': '1',
			'device_name':device_data.get('device_platform'),
			'sent_at': sent_at,
			'os_build':	device_data.get('build'),			
			'install_receipt': apple_receipt(),
			'callback_params': callback_params,
			'partner_params':partner_params,
			'connectivity_type': 2,
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'mcc':device_data.get('mcc'),
			'mnc':device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyLTE'
			}
			
	if event_token== '1xkndr':
		data['revenue']= revenue
		data['currency']= 'RUB'
		app_data['revenue_purchase'] = revenue

	# if event_token=='a0d43g':
		# del data['partner_params']

	header['Authorization']= get_auth(app_data,created_at=data['created_at'],idfa=data['idfa'],activity_kind='event')
	
		
	return {'url': url, 'httpmethod': method, 'headers': header, 'params':None, 'data': data}		
	

def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	if not gender_n:
		app_data['gender'] = random.choice(['male', 'female'])
		gender_n = app_data['gender']
		
	# user_info = app_data.get('user_info')
	if not app_data.get('email'):
		user_data = util.generate_name(gender=gender_n)
		# user_info = {}
		# user_info['username'] = user_data.get('username').lower()
		app_data['email'] = user_data.get('username').lower() + '@gmail.com'
		# user_info['f_name'] = user_data.get('firstname')
		# user_info['l_name'] = user_data.get('lastname')
		# user_info['sex'] = gender_n
		# user_info['gender'] = str(1) if gender_n == 'female' else str(2)
		# user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
		# user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
		# user_info['password'] = util.get_random_string(type='all',size=16)
		# app_data['user_info'] = user_info
	return app_data['email']
	
###########################################################
#						Self Calls						  #
###########################################################	

def adjust_selfcall1(campaign_data, app_data, device_data):
	url = 'http://apigw.lamoda.by/json/get_customer_by_session'
	method = 'get'
	headers = {
		'Accept': 'application/json',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br,deflate, gzip',
		'X-NewRelic-ID' : campaign_data.get('X-NewRelic-ID'),
		'X-LM-SupportedFeatures': 'marketplace;applepay',
		'User-Agent': campaign_data.get('app_name')+'AppIos/'+campaign_data.get('app_version_code')+'.'+campaign_data.get('app_version_name')+' (DT/phone; PN/iOS; PV/'+device_data.get('os_version')+'; DI/'+device_data.get('device_platform')+'; SC/'+device_data.get('resolution')+'@'+device_data.get('screen_size')+')'

	}
	params={}
	data = {}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def app_site(campaign_data, app_data, device_data,c=''):
	url = 'https://www.lamoda.'+c+'/apple-app-site-association'
	method = 'head'
	headers = {
		'Accept': 'application/json',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br,deflate, gzip',
		
		'User-Agent': 'swcd (unknown version)'+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	params={}
	data = {}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


def adjust_selfcall2(campaign_data, app_data, device_data):
	url = 'http://app-measurement.com/config/app/1:3322758512:ios:426360e7ed2bcbc3'
	method = 'get'
	headers = {
		'Accept': 'application/json',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br,deflate, gzip',
		'X-NewRelic-ID' : campaign_data.get('X-NewRelic-ID'),
		'X-LM-SupportedFeatures': 'marketplace',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	params={
			'app_instance_id':'E7B3C8FDF0724433A2F58DBE210652EC',
			'gmp_version':'3600',
			'platform':'ios'
	}
	data = {}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_selfcall3(campaign_data, app_data, device_data):
	url = 'http://device-provisioning.googleapis.com/checkin'
	method = 'post'
	headers = {
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br,deflate, gzip',
		'X-NewRelic-ID' : campaign_data.get('X-NewRelic-ID'),
		'Content-Type': 'application/json',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	params=None			
	data = {"locale":device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
			"digest":"",
			"checkin":{"iosbuild":{"model":device_data.get('device_platform'),
									"os_version":"IOS_"+device_data.get('os_version')},
						"last_checkin_msec":0,
						"user_number":0,
						"type":2},
			"user_serial_number":0,
			"id":0,
			"timezone":"Asia/Kolkata",
			"logging_id":2562692969,
			"version":2,
			"security_token":0,
			"fragment":0}
	json_str=json.dumps(data)
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json_str}


def adjust_selfcall4(campaign_data, app_data, device_data):
	url = 'https://api.lamoda.ru/mobile/v1/catalog/products'
	method = 'get'
	headers = {
		'Accept': 'application/json',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'X-NewRelic-ID' : campaign_data.get('X-NewRelic-ID'),
		'X-LM-SupportedFeatures': 'marketplace;applepay;sessionsign2',
		'User-Agent': campaign_data.get('app_name')+'AppIos/'+campaign_data.get('app_version_name')+'.'+campaign_data.get('app_version_code')+' (DT/phone; PN/iOS; PV/'+device_data.get('os_version')+'; DI/'+device_data.get('device_platform')+'; SC/'+device_data.get('resolution')+'@'+device_data.get('screen_size')+')',
		'X-LM-Country':'ru',
		'X-LM-LID':app_data.get('lid'),
		'X-LM-Platform': 'ios_phone',
		'X-LM-SessionKey':'MTc4YmIwOTgyNDkyZWFlNGMwNTNjZWEwNGI1M2I0YWM=|1547445959|13bb7c0f1164048e07ff805ed523ae970d511ad8',
		'X-LM-SessionSignature': 'c51b8460cb0e5b02f7018aa1a3c6e4c5347ee6a1',
	}
	labels_list=['30779', '32128', '29819', '31926']
	params={
		'country':'ru',
		'filter':'{"labels":["'+random.choice(labels_list)+'"],"categories":"'+app_data.get('category_id')+'","display_locations":["all"]}',
		'limit':'20',
		'offset':0
	}
	data = {}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_selfcall5(campaign_data, app_data, device_data):
	url='https://api.lamoda.ru/mobile/v1/catalog/menu'
	method='get'
	headers={
	'Accept': 'application/json',
	'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
	'Accept-Encoding': 'br, gzip, deflate',
	'X-NewRelic-ID' : campaign_data.get('X-NewRelic-ID'),
	'X-LM-SupportedFeatures': 'marketplace;applepay;sessionsign2',
	'User-Agent': campaign_data.get('app_name')+'AppIos/'+campaign_data.get('app_version_name')+'.'+campaign_data.get('app_version_code')+' (DT/phone; PN/iOS; PV/'+device_data.get('os_version')+'; DI/'+device_data.get('device_platform')+'; SC/'+device_data.get('resolution')+'@'+device_data.get('screen_size')+')',
	'X-LM-Country':'by',
	'X-LM-LID':app_data.get('lid'),
	'X-LM-Platform': 'ios_phone',
	'X-LM-SessionKey':'MTc4YmIwOTgyNDkyZWFlNGMwNTNjZWEwNGI1M2I0YWM=|1547445952|b20e81b91a53f12d8f7feef50d7d34a416f01d3a',#'MzI4ODk0Mjc4ODdmZWIzY2U4MGU5MDUzYmQxM2YwNmU=|'+str(int(time.time()))+'|3e0fda7bb9c38f40c542acddfef70f5b90bb6d0b',1539147832
	'X-LM-SessionSignature': 'c51b8460cb0e5b02f7018aa1a3c6e4c5347ee6a1',#'f699f1e0ce76c53db9f5294865ddbcdd12c09d72',#f699f1e0ce76c53db9f5294865ddbcdd12c09d72
	}
	params={
		'country':'by',
		'depth':3,
		'platform':'ios_phone',
		'version' :'16.10.19.1'

		}

	data={}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def uid(app_data):
	if not app_data.get('uid'):
		app_data['uid']='1206'+str(util.get_random_string('decimal',13))


		
###########################################################
#						UTIL							  #
###########################################################

def def_sessionLength(app_data,forced=False):
	if not app_data.get('session_length') or forced:
		app_data['session_length'] = 0	
	return app_data.get('session_length')	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['session_length'] += length


def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def pushToken1(app_data):
	if not app_data.get('push_token'):
		app_data['push_token']=util.get_random_string('hex',64)

def pushToken_event1(app_data):
	if not app_data.get('pushToken_Event'):
		app_data['pushToken_Event']=util.get_random_string('all',43)+'='

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date
def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec


def set_installedAT(app_data,device_data):
	if not app_data.get('installed_at'):
		app_data['installed_at'] =  get_date(app_data,device_data,early=random.uniform(245,250))

def hash_digest(app_data):
	if not app_data.get('email_hash'):
		string=str(app_data.get('email'))
		result = hashlib.md5(string.encode())
		app_data['email_hash']=result.hexdigest()

	return app_data.get('email_hash')

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def apple_receipt():
		return 'MIISngYJKoZIhvcNAQcCoIISjzCCEosCAQExCzAJBgUrDgMCGgUAMIICPwYJKoZIhvcNAQcBoIICMASCAiwxggIoMAoCARQCAQEEAgwAMAsCAQ4CAQEEAwIBWjALAgEZAgEBBAMCAQMwDAIBCgIBAQQEFgI0KzANAgENAgEBBAUCAwHVJjAOAgEBAgEBBAYCBC5Z7WkwDgIBCQIBAQQGAgRQMjUzMA4CAQsCAQEEBgIEATwwiDAOAgEQAgEBBAYCBDGi0hMwEAIBDwIBAQQIAgZJt9aQQiQwEQIBAwIBAQQJDAczNTYwMDAyMBECARMCAQEECQwHMzUxMDEwMTAUAgEAAgEBBAwMClByb2R1Y3Rpb24wGAIBAgIBAQQQDA5jb20ubGFtb2RhLmlvczAYAgEEAgECBBBIsqBELBBGxwB7JXT6UB+PMBwCAQUCAQEEFEW581qlwMaQLEP2UZLGP8nWLIpVMB4CAQgCAQEEFhYUMjAxOS0wOS0yMFQwOTo1OToxMFowHgIBDAIBAQQWFhQyMDE5LTA5LTIwVDA5OjU5OjEwWjAeAgESAgEBBBYWFDIwMTktMDctMjNUMDY6MDA6MjBaMEQCAQcCAQEEPC6M68XwZrE7WWoD9iJv/1moHh2hE0YlrSmjgP5VXbt2QhEwlRnnTKSHZP0elM/oI1Je1mLkjv0xRWHvTjBdAgEGAgEBBFXLKvGKiyj0JaoUJbDZC0gnK1Xx22s+gbSrZQk7jmFbGZ78OraiHtPZWHJCZYOwW3rpOYFd861gHgx1h8lV9ftoG8h7RSgcDUhpo2K0iC2rdK4iJRjmoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQBjeRVilf9k27/Ovld2Yn2Nbv/S0mp32ZtMecAfB3qqAnBMUlXTCvzBBR0rJBahdVwcPDUj89la5yK6mTovmXa2RKIGShebAmn3Uk2JT8E6pYW2CCr/ZRXm9Nxrg8jUzEhbToAGDKbT4sg5xo0JXYAkBcl9m3rQE85CLwem0h7jYM+ya/lsXqmB1hE9Sgvctq6hkLCBqJYZ/3LEGst3YsN/ZkWlETp6lUVY4DRbDofOfqzH5dkCbDceSKT0MIb9tlTZ3mCgPKHPAiJXT0LlMnKx0xZwnsVFNEfDsg5xvpjddUrEQXV6DlQL0ACYZEa0lNnWTD7JNDfaPd1UL/NPNCbZ'
		