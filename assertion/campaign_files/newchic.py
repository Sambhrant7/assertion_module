# -*- coding: utf8 -*-
import sys
from sdk import getsleep
reload(sys)
sys.setdefaultencoding('utf-8')

from sdk import installtimenew
from sdk import util, purchase
from sdk import NameLists
import time
import urllib
import random
import json
import datetime
import clicker
import Config
import uuid


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'app_size'			 : 15.0,#14.0,
	'package_name'		 :'com.newchic.client',
	'app_name' 			 :'Newchic',
	'app_version_name' 	 :'4.17.0',#'4.16.0',#'4.15.0',#'4.14.0',#'4.13.0',#'4.12.0',#'4.11.0',#'4.9.0',#'4.8.0',#'4.4.0',
	'app_version_code' 	 :'620',#'611',#'602',#'592',#'584',#'575',#'561',#'540',#'531',#'492',
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'RLrfhteHDbuNhCPYXMyhw3',
		'dkh'		 : 'RLrfhteH',
		'buildnumber': '4.9.0',#'4.8.15',
		'version'	 : 'v4',
	},
	'branch':{
					'key':'key_live_pol2F8dlZTYwmpb1goyPJoimwzfXPBzm',
					'sdk': 'android2.18.1',
					},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=20)

	app_data['clicked_referrer_ts'] = int(app_data.get('times').get('click_time'))
	app_data['install_begin_time_ts'] = int(app_data.get('times').get('download_begin_time')*1000)
	app_data['first_install_time'] = int(app_data.get('times').get('install_complete_time')*1000)	

	################generating_realtime_differences##################
	time.sleep(random.randint(10,20))
	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	get_batteryLevel(app_data)
	register_user(app_data,country='united states')

	app_data['registeredUninstall']=False

	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])	

	###########		CALLS		############

	print '________BRANCH INSTALL_____________________________'
	request = branch_install(campaign_data, app_data, device_data)
	r = util.execute_request(**request)

	try:
		response=json.loads(r.get('data'))
		app_data['session_id'] = response.get('session_id')
		app_data['identity_id'] = response.get('identity_id')
		app_data['device_fingerprint_id'] = response.get('device_fingerprint_id')
	except:
		app_data['session_id'] = '596200596772794493'
		app_data['identity_id'] = '596200596764491744'
		app_data['device_fingerprint_id'] ='595559518134453935'	

	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)

	print '________BRANCH Eevent_____________________________'
	request = branch_event(campaign_data, app_data, device_data)
	util.execute_request(**request)


	print "gcm token call"
	req=gcmToken_call(campaign_data, app_data, device_data)
	res=util.execute_request(**req)
	try:
		app_data['af_gcm_token']=res.get('data').split('=')[1]
		print "\n\n"
		print app_data['af_gcm_token']
	except:
		app_data['af_gcm_token']='BTIykIwgEZX:APA91bFU8YIfbcTmwzJ9fXU9nSTQe2m2M4VWuyTmNFRqx1FN_Qd5nq2hUBYWiEGALGWXOT7OPWRybwyvPHupXxsJabobVJZm2swN6JlHn8MrF8FIOFmX_AiTDVbwWjg3lDz1XJPuODv2'

	time.sleep(random.randint(6,10))
		
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)

	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	response = util.execute_request(**request)

	time.sleep(random.randint(60,100))

	print '________BRANCH CLOSE_____________________________'
	request = branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)

	branch_open=api_branch_open(campaign_data, app_data, device_data)
	util.execute_request(**branch_open)

	print "\nself_call_dc_banggood\n"
	request=dc_banggood( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	try:
		result = json.loads(response.get('data'))
		app_data['bgqueue'] = result.get('__bgqueue')
		app_data['bguser'] = result.get('__bguser')
		app_data['bgvisit'] = result.get('__bgvisit')
	except:
		app_data['bgqueue'] = '1551336213|appier_int|0jmi13hgg|-|agent|0|1||media channels|'
		app_data['bguser'] = '1558012198|2505046626|2257995807|1551336213'
		app_data['bgvisit'] = '1558012198|direct|none|-|-|0|0|0|||en-GB||'	


	for count in range(1, random.randint(2,4)):

		globals()['category'] = random.choice(['3973', '3990', '3580', '4565']) 	# accessories, beauty, shoes, kids and moms
		globals()['af_currency'] = 'USD'
		get_product_list(campaign_data, device_data, app_data)  # globals()['af_content_list']

		# if count == 1:
		# 	newchic_registration( campaign_data, device_data, app_data )

		n = random.randint(0,len(globals()['af_content_list'])-1)
		globals()['af_product_id'] = globals()['af_content_list'][n]

		# print globals()['extra']
		try:
			globals()['image_url'] = globals()['extra'][n][0]
			globals()['url'] = globals()['extra'][n][1]
			globals()['products_name'] = globals()['extra'][n][2]
		except:	
			globals()['image_url'] = 'http://img.chiccdn.com/thumb/view/oaupload/newchic/images/1F/A0/ea6adbe8-ca00-4370-b858-2f21521bcf77.jpg'
			globals()['url'] = 'http://m.newchic.com/hats-and-caps-4192/p-1029255.html'
			globals()['products_name'] = "Men's Cotton Beret Cap "

		get_product_data(campaign_data, device_data, app_data)  # globals()['af_product_data']
		add_product_data(campaign_data, device_data, app_data)  # globals()['af_add_to_cart_data']


		##### Branch call goes here

		if random.randint(1,100) <= 85:

			time.sleep(random.randint(14,30))
			af_content_list_call(campaign_data, app_data, device_data)


		if random.randint(1,100) <= 85:

			time.sleep(random.randint(15,45))
			af_content_view(campaign_data, app_data, device_data)

			prod = globals()['af_product_data'].get('af_content_id')

			print "\nself_call_android_newchic_in\n"
			request=android_newchic_in( campaign_data, device_data, app_data, prod=prod )
			util.execute_request(**request)

			print "\nself_call_api_branch_i\n"
			request=api_branch_i( campaign_data, device_data, app_data, prod=prod )
			util.execute_request(**request)

			print "\nself_call_api_branch_i\n"
			request=api_branch_url( campaign_data, device_data, app_data, prod=prod )
			util.execute_request(**request)


			if random.randint(1,100) <= 82:

				time.sleep(random.randint(9,15))
				af_add_to_cart(campaign_data, app_data, device_data)
				app_data['add_to_cart'] = True


		if random.randint(1,100) <= 65:

			time.sleep(random.randint(7,12))
			af_add_to_wishlist(campaign_data, app_data, device_data)


		if random.randint(1,100) <= 55:

			time.sleep(random.randint(12,20))
			# both "af_view_wishlist" comes together
			af_view_wishlist(campaign_data, app_data, device_data)
			af_view_wishlist(campaign_data, app_data, device_data)


		if random.randint(1,100) <= 60:

			time.sleep(random.randint(8,40))
			# both "af_view_basket" comes together
			af_view_basket(campaign_data, app_data, device_data)
			af_view_basket(campaign_data, app_data, device_data)


		if random.randint(1,100) <= 20 and not app_data.get('registered'):

			time.sleep(random.randint(30,50))

			# if registration is done using email-id
			newchic_registration( campaign_data, device_data, app_data )
			extra_registration_calls(campaign_data, device_data, app_data)
			af_register(campaign_data, app_data, device_data)
			app_data['registered'] = True


		if random.randint(1,100) <= 45 and not app_data.get('registered'):

			time.sleep(random.randint(30,50))

			# if registration is done using google or facebook. No token in this method.
			newchic_registration( campaign_data, device_data, app_data )
			extra_registration_calls(campaign_data, device_data, app_data)
			app_data['registered'] = True


		if app_data.get('registered') and app_data.get('add_to_cart') and random.randint(1,100) <= 50:#50

			time.sleep(random.randint(60,90))
			checkout_product_data(campaign_data, device_data, app_data) # globals()['af_checkout_data'] and globals().get('purchase_af_revenue') and globals().get('purchase_value') and globals().get('purchase_af_quantity')
			af_initiated_checkout(campaign_data, app_data, device_data)

			if purchase.isPurchase(app_data,day=1,advertiser_demand=8):#8

				time.sleep(random.randint(60,80))

				af_criteo_purchase(campaign_data, app_data, device_data)
				af_nonfee_purchase(campaign_data, app_data, device_data)
				af_purchase(campaign_data, app_data, device_data)

	if app_data.get('registered'):
		print "\nClear Cart\n"
		request=newchic_adjust_cart( campaign_data, device_data, app_data, call_type = 'clearcart' )
		util.execute_request(**request)


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):

	###########		INITIALIZE		############	

	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=20)

	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	get_batteryLevel(app_data)
	register_user(app_data,country='united states')

	if not app_data.get('registeredUninstall'):
		app_data['registeredUninstall']=False

	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])


	###########		CALLS		############


	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	util.execute_request(**request)


	for _ in range(1, random.randint(2,4)):

		globals()['category'] = random.choice(['3973', '3990', '3580', '4565']) 	# accessories, beauty, shoes, kids and moms
		globals()['af_currency'] = 'USD'
		get_product_list(campaign_data, device_data, app_data)  # globals()['af_content_list']

		# if not app_data.get('dc_cid'):
		# 	newchic_registration( campaign_data, device_data, app_data )
		# else:
		# 	globals()['dc_cid'] = app_data['dc_cid']
		# 	globals()['_uh_email'] = app_data['_uh_email']

		globals()['af_product_id'] = random.choice(globals()['af_content_list'])
		
		get_product_data(campaign_data, device_data, app_data)  # globals()['af_product_data']
		add_product_data(campaign_data, device_data, app_data)  # globals()['af_add_to_cart_data']

		if random.randint(1,100) <= 80:

			time.sleep(random.randint(14,30))
			af_content_list_call(campaign_data, app_data, device_data)


		if random.randint(1,100) <= 80:

			time.sleep(random.randint(15,45))
			af_content_view(campaign_data, app_data, device_data)


			if random.randint(1,100) <= 82:

				time.sleep(random.randint(9,15))
				af_add_to_cart(campaign_data, app_data, device_data)
				app_data['add_to_cart'] = True


		if random.randint(1,100) <= 60:

			time.sleep(random.randint(7,12))
			af_add_to_wishlist(campaign_data, app_data, device_data)


		if random.randint(1,100) <= 50:

			time.sleep(random.randint(12,20))
			# both "af_view_wishlist" comes together
			af_view_wishlist(campaign_data, app_data, device_data)
			af_view_wishlist(campaign_data, app_data, device_data)


		if random.randint(1,100) <= 55:

			time.sleep(random.randint(8,40))
			# both "af_view_basket" comes together
			af_view_basket(campaign_data, app_data, device_data)
			af_view_basket(campaign_data, app_data, device_data)


		if random.randint(1,100) <= 15 and not app_data.get('registered'):

			time.sleep(random.randint(30,50))

			# if registration is done using email-id
			newchic_registration( campaign_data, device_data, app_data )
			extra_registration_calls(campaign_data, device_data, app_data)
			af_register(campaign_data, app_data, device_data)
			app_data['registered'] = True


		if random.randint(1,100) <= 40 and not app_data.get('registered'):

			time.sleep(random.randint(30,50))

			# if registration is done using google or facebook. No token in this method.
			newchic_registration( campaign_data, device_data, app_data )
			extra_registration_calls(campaign_data, device_data, app_data)
			app_data['registered'] = True
			

		if app_data.get('dc_cid'):
		
			globals()['dc_cid'] = app_data['dc_cid']
			globals()['_uh_email'] = app_data['_uh_email']


		if app_data.get('registered') and app_data.get('add_to_cart') and random.randint(1,100) <= 40:

			time.sleep(random.randint(60,90))
			checkout_product_data(campaign_data, device_data, app_data) # globals()['af_checkout_data'] and globals().get('purchase_af_revenue') and globals().get('purchase_value') and globals().get('purchase_af_quantity')
			af_initiated_checkout(campaign_data, app_data, device_data)

			if purchase.isPurchase(app_data,day=day,advertiser_demand=26):

				time.sleep(random.randint(60,80))

				af_criteo_purchase(campaign_data, app_data, device_data)
				af_nonfee_purchase(campaign_data, app_data, device_data)
				af_purchase(campaign_data, app_data, device_data)
				

	if app_data.get('registered'):
		print "\nClear Cart\n"
		request=newchic_adjust_cart( campaign_data, device_data, app_data, call_type = 'clearcart' )
		util.execute_request(**request)


	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def af_register(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_register'
	eventName		  = 'af_register'
	eventValue		  = str(json.dumps({}))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def af_content_list_call(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_content_list'
	eventName		  = 'af_content_list'
	eventValue		  = str(json.dumps({"af_content_list": globals().get('af_content_list') ,"af_currency":"USD"}, ensure_ascii=False))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)	

def af_content_view(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_content_view'
	eventName		  = 'af_content_view'
	eventValue		  = str(json.dumps(globals()['af_product_data'], ensure_ascii=False))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_add_to_wishlist(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_add_to_wishlist'
	eventName		  = 'af_add_to_wishlist'
	eventValue		  = str(json.dumps({}))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def af_view_wishlist(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_view_wishlist'
	eventName		  = 'af_view_wishlist'
	eventValue		  = str(json.dumps({}))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def af_add_to_cart(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_add_to_cart'
	eventName		  = 'af_add_to_cart'
	eventValue		  = str(json.dumps(globals()['af_add_to_cart_data'], ensure_ascii=False))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)	

def af_view_basket(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_view_basket'
	eventName		  = 'af_view_basket'
	eventValue		  = str(json.dumps({}))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)	


def af_initiated_checkout(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_initiated_checkout'
	eventName		  = 'af_initiated_checkout'
	eventValue		  = str(json.dumps(globals()['af_checkout_data'], ensure_ascii=False))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)	


def af_criteo_purchase(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_criteo_purchase'
	eventName		  = 'af_criteo_purchase'
	d = {"af_price":[globals()['purchase_value']],"af_content_id":[globals()['af_product_id']],"af_quantity":[globals()['purchase_af_quantity']],"source":"test","campaign":"swimwear1","medium":"trends","af_content_type":"product","af_currency":"USD","af_revenue":globals()['purchase_value'],"af_receipt_id":globals()['order_id']}
	eventValue		  = str(json.dumps(d, ensure_ascii=False))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)	


def af_nonfee_purchase(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_nonfee_purchase'
	eventName		  = 'af_nonfee_purchase'
	d = {"af_price":[globals()['purchase_value']],"af_content_id":[globals()['af_product_id']],"af_quantity":str(globals()['purchase_af_quantity']),"source":"test","campaign":"swimwear1","medium":"trends","af_content_type":"product","af_currency":"USD","af_revenue":globals()['purchase_value'],"af_receipt_id":globals()['order_id']}
	eventValue		  = str(json.dumps(d, ensure_ascii=False))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)	


def af_purchase(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT___________________________af_purchase'
	eventName		  = 'af_purchase'
	d = {"af_price":[globals()['purchase_value']],"af_content_id":[globals()['af_product_id']],"af_quantity":str(globals()['purchase_af_quantity']),"source":"test","campaign":"swimwear1","medium":"trends","af_content_type":"product","af_currency":"USD","af_revenue":globals()['purchase_af_revenue'],"af_receipt_id":globals()['order_id']}
	eventValue		  = str(json.dumps(d, ensure_ascii=False))
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)
		


def newchic_registration( campaign_data, device_data, app_data ):

	print '\nNewChic : registration ............................'
	request = newchic_registration_call( campaign_data, device_data, app_data )
	result = util.execute_request(**request)
	try:
		globals()['dc_cid'] = result.get('res').headers.get('Set-Cookie').split('dc_cid=')[1].split(';')[0]
		globals()['_uh_email'] = result.get('res').headers.get('Set-Cookie').split('_uh_email=')[1].split(';')[0]
		print "\nOk response for _uh_email"
		print globals()['dc_cid']
		print globals()['_uh_email']
		app_data['dc_cid'] = globals()['dc_cid']
		app_data['_uh_email'] = globals()['_uh_email']
	except:
		print "\nException for _uh_email"
		globals()['dc_cid'] = "e451ElSgPqGxzw%2Bbf%2B6Ao3vykgEC07UDJuqTDo1fYpaUG05olQ"
		globals()['_uh_email'] = "75fda9e7c6da4ddd0df46dfb6915038410bf4dca2a5a981846d6e0033b47c292"
		app_data['dc_cid'] = globals()['dc_cid']
		app_data['_uh_email'] = globals()['_uh_email']

def get_product_list(campaign_data, device_data, app_data):

	print '\nNewChic : category display ............................'
	req = newchic_category_display( campaign_data, device_data, app_data )
	result = util.execute_request(**req)
	try:
		res = json.loads(result.get('data'), encoding = "utf-8")
		extra_value = list()
		globals()['extra'] = []
		if not app_data.get('newchic_SID'):
			globals()['newchic_SID']=result.get('res').headers.get('Set-Cookie').split('newchic_SID=')[1].split(';')[0]
			globals()['route']=result.get('res').headers.get('Set-Cookie').split('route=')[1].split(',')[0]
			globals()['default_rule_country']=result.get('res').headers.get('Set-Cookie').split('default_rule_country=')[1].split(';')[0]
			globals()['generalAbTest']=result.get('res').headers.get('Set-Cookie').split('generalAbTest=')[1].split(';')[0]
			globals()['_abtest']=result.get('res').headers.get('Set-Cookie').split('_abtest=')[1].split(';')[0]
		else:
			globals()['newchic_SID']=app_data['newchic_SID']
			globals()['route']=app_data['route']
			globals()['default_rule_country']=app_data['default_rule_country']
			globals()['generalAbTest']=app_data['generalAbTest']
			globals()['_abtest']=app_data['_abtest']

		if res.get('result'):
			if res.get('result').get('list'):	
				for n in res.get('result').get('list'):
					if n.get('products_name'):
						extra_value.append(str(n.get('image_url')))
						extra_value.append(str(n.get('url')))
						extra_value.append(str(n.get('products_name')))
					globals()['extra'].append(extra_value)		
					extra_value = []

		if len(globals().get('extra')) > 3:
			random.shuffle(globals()['extra'])
			globals()['extra'] = globals()['extra'][:3]			

		if not globals().get('extra'):
			l = [['http://img.chiccdn.com/thumb/view/oaupload/newchic/images/1F/A0/ea6adbe8-ca00-4370-b858-2f21521bcf77.jpg', 'http://m.newchic.com/hats-and-caps-4192/p-1029255.html', "Men's Cotton Beret Cap "], ['http://img.chiccdn.com/thumb/view/oaupload/newchic/images/2A/77/f81154c2-53e8-4de4-9af6-715f29826c58.jpg', 'http://m.newchic.com/womens-hats-4124/p-1130732.html', 'Women Summer Gardening Anti-UV Foldable Hat'], ['http://img.chiccdn.com/thumb/view/oaupload/newchic/images/F3/EA/4d853616-33b8-4120-8164-d59a475644df.JPG', 'http://m.newchic.com/hats-and-caps-4192/p-1404952.html', 'Adjustable  Cotton Beret Cap']]
			random.shuffle(l)
			globals()['extra'] = l
			print '\n Default Data'	

		if len(globals().get('extra')) == 0:
			l = [['http://img.chiccdn.com/thumb/view/oaupload/newchic/images/1F/A0/ea6adbe8-ca00-4370-b858-2f21521bcf77.jpg', 'http://m.newchic.com/hats-and-caps-4192/p-1029255.html', "Men's Cotton Beret Cap "], ['http://img.chiccdn.com/thumb/view/oaupload/newchic/images/2A/77/f81154c2-53e8-4de4-9af6-715f29826c58.jpg', 'http://m.newchic.com/womens-hats-4124/p-1130732.html', 'Women Summer Gardening Anti-UV Foldable Hat'], ['http://img.chiccdn.com/thumb/view/oaupload/newchic/images/F3/EA/4d853616-33b8-4120-8164-d59a475644df.JPG', 'http://m.newchic.com/hats-and-caps-4192/p-1404952.html', 'Adjustable  Cotton Beret Cap']]
			random.shuffle(l)
			globals()['extra'] = l
			print '\n Default Data'		


		app_data['newchic_SID'] = globals()['newchic_SID']
		app_data['route'] = globals()['route']
		app_data['default_rule_country'] = globals()['default_rule_country']
		app_data['generalAbTest'] = globals()['generalAbTest']
		app_data['_abtest'] = globals()['_abtest']


		print '\nOk Response for cookies'
		print globals()['newchic_SID']
		print globals()['route']
		print globals()['default_rule_country']
		print globals()['generalAbTest']
		print globals()['_abtest']

	except:
		globals()['newchic_SID'] = "18f4f377602b77406480c08e1952e650"
		globals()['route'] = "99f9338137232b0a5a40cb82e4058c8f"
		globals()['default_rule_country'] = "99"
		globals()['generalAbTest'] = "95"
		globals()['_abtest'] = "1"
		globals()['extra'] = [['http://img.chiccdn.com/thumb/view/oaupload/newchic/images/1F/A0/ea6adbe8-ca00-4370-b858-2f21521bcf77.jpg', 'http://m.newchic.com/hats-and-caps-4192/p-1029255.html', "Men's Cotton Beret Cap "], ['http://img.chiccdn.com/thumb/view/oaupload/newchic/images/2A/77/f81154c2-53e8-4de4-9af6-715f29826c58.jpg', 'http://m.newchic.com/womens-hats-4124/p-1130732.html', 'Women Summer Gardening Anti-UV Foldable Hat'], ['http://img.chiccdn.com/thumb/view/oaupload/newchic/images/F3/EA/4d853616-33b8-4120-8164-d59a475644df.JPG', 'http://m.newchic.com/hats-and-caps-4192/p-1404952.html', 'Adjustable  Cotton Beret Cap']]


	random_number = random.randint(1,100)
	if random_number <= 80:
		lower_limit = 0
		upper_limit = 20
	elif random_number <= 90:
		lower_limit = 0
		upper_limit = 20
	elif random_number <= 98:
		lower_limit = 20
		upper_limit = 30
	else:
		lower_limit = 20
		upper_limit = 30


	print '\nNewChic : category display - sort + filter ..............................'
	req = newchic_category_display( campaign_data, device_data, app_data, call_type = 'filter', lower_limit = lower_limit, upper_limit = upper_limit )
	result = util.execute_request(**req)
	try:
		globals()['af_content_list'] = []
		res = json.loads(result.get('data'), encoding = "utf-8")
		if res.get('result'):
			if res.get('result').get('list'):	
				for n in res.get('result').get('list'):
					if n.get('products_id'):
						globals()['af_content_list'].append(str(n.get('products_id')))


		if not globals().get('af_content_list'):
			l = ["1256460", "1269731", "1203632", "1222373", "1223384"]
			random.shuffle(l)
			globals()['af_content_list'] = l[:3]
			print '\n Default Data'

		if len(globals().get('af_content_list')) > 3:
			random.shuffle(globals()['af_content_list'])
			globals()['af_content_list'] = globals()['af_content_list'][:3]

		print '\n Ok Response for "af_content_list"'
		print '"af_content_list" = ',
		print globals()['af_content_list']
	except:
		print '\n Exception for "af_content_list"'
		l = ["1137556", "1269731", "1203632", "1222373", "1223384"]
		random.shuffle(l)
		globals()['af_content_list'] = l[:3]


def get_product_data(campaign_data, device_data, app_data):

	print '\nNewChic : product ............................'
	request=newchic_product( campaign_data, device_data, app_data )
	result = util.execute_request(**request)
	try:
		product_data = {}
		dic = {}
		res = json.loads(result.get('data'))

		if res.get('result'):
			if res.get('result').get('productInfo'):
				if res.get('result').get('productInfo').get('attributes'):
					if res.get('result').get('productInfo').get('attributes').get('selAttr'):
						dic = res.get('result').get('productInfo').get('attributes').get('selAttr')

		if not dic:
			dic = {'379' : '16217'}

		for k,v in dic.items():
			globals()['option_id'] = k
			globals()['option_value_id'] = v
			break

		if res.get('result'):
			if res.get('result').get('statistics'):
				product_data = res.get('result').get('statistics')

		if not product_data:
			product_data = {"content_category":"Accessories > Women Accessories > Socks","value":0.99,"fb_content_id":"1137556-27749US","fb_content_type":"product","fb_currency":"USD","af_currency":"USD","af_content_id":1137556,"Color":"Black,Khaki,Navy,Orange,Yellow","Material":"Cotton,Glass Silk ","Style":"Ankle Sock","Gender":"Women","Season":"Autumn,Spring,Summer"}
			print "\n Default value for 'product_data'"

		for k,v in product_data.items():
			product_data[k] = str(v)

		product_data['source'] = "direct"
		product_data['campaign'] = "-"
		product_data['medium'] = "none"

		if product_data.get('fb_currency'):
			product_data['fb_currency'] = 'USD'

		globals()["af_product_data"] = product_data
		print "\n Ok Response ..........."
		print "af_product_data = ",
		print globals()['af_product_data']

	except:
		print "\nException for 'af_product_data'"

		globals()['option_id'] = '379'
		globals()['option_value_id'] = '16242'

		product_data = {"content_category":"Accessories > Women Accessories > Socks","value":0.99,"fb_content_id":"1137556-27749US","fb_content_type":"product","fb_currency":"USD","af_currency":"USD","af_content_id":1137556,"Color":"Black,Khaki,Navy,Orange,Yellow","Material":"Cotton,Glass Silk ","Style":"Ankle Sock","Gender":"Women","Season":"Autumn,Spring,Summer"}
		for k,v in product_data.items():
			product_data[k] = str(v)

		if product_data.get('fb_currency'):
			product_data['fb_currency'] = 'USD'

		product_data['source'] = "direct"
		product_data['campaign'] = "-"
		product_data['medium'] = "none"
		globals()["af_product_data"] = product_data
		print "af_product_data = ",
		print globals()['af_product_data']
		

def add_product_data(campaign_data, device_data, app_data):

	if not app_data.get('total_products_in_cart'):
		app_data['total_products_in_cart'] = str(globals()['af_product_id']) + '{' + str(globals()['option_id']) + '}' + str(globals()['option_value_id'])
	else:
		app_data['total_products_in_cart'] += ',' + str(globals()['af_product_id']) + '{' + str(globals()['option_id']) + '}' + str(globals()['option_value_id'])

	print '\nNewChic : add_to_cart ............................'
	request=newchic_add_product( campaign_data, device_data, app_data )
	result = util.execute_request(**request)
	try:
		d = {}
		res = json.loads(result.get('data'))
		if res.get('result'):
			if res.get('result').get('statistics'):
				d = res.get('result').get('statistics')

		if not d:
			d = {"content_category":"Accessories > Women Accessories > Socks","value":0.99,"fb_content_id":"1137556-16242US","fb_content_type":"product","fb_currency":"USD","af_currency":"USD","af_content_id":1137556,"Color":"Black,Khaki,Navy,Orange,Yellow","Material":"Cotton,Glass Silk ","Style":"Ankle Sock","Gender":"Women","Season":"Autumn,Spring,Summer"}
			print "\n Default value for 'add_to_cart'"

		for k,v in d.items():
			d[k] = str(v)

		d['source'] = "direct"
		d['campaign'] = "-"
		d['medium'] = "none"

		if d.get('fb_currency'):
			d['fb_currency'] = 'USD'

		globals()['af_add_to_cart_data'] = d
		print "\n Ok Response ..........."
		print "af_add_to_cart_data = ",
		print globals()['af_add_to_cart_data']

	except:
		print '\nException for "add_to_cart_data"'
		d = {"content_category":"Accessories > Women Accessories > Socks","value":0.99,"fb_content_id":"1137556-16242US","fb_content_type":"product","fb_currency":"USD","af_currency":"USD","af_content_id":1137556,"Color":"Black,Khaki,Navy,Orange,Yellow","Material":"Cotton,Glass Silk ","Style":"Ankle Sock","Gender":"Women","Season":"Autumn,Spring,Summer"}

		for k,v in d.items():
			d[k] = str(v)

		d['source'] = "direct"
		d['campaign'] = "-"
		d['medium'] = "none"

		if d.get('fb_currency'):
			d['fb_currency'] = 'USD'

		globals()['af_add_to_cart_data'] = d
		print "af_add_to_cart_data = ",
		print globals()['af_add_to_cart_data']


def checkout_product_data(campaign_data, device_data, app_data):


	print "\nOnly One Item in checkout\n"
	request=newchic_adjust_cart( campaign_data, device_data, app_data, call_type = 'remove' )
	util.execute_request(**request)

	print "\nOnly One Item in checkout\n"
	request=newchic_adjust_cart( campaign_data, device_data, app_data, call_type = 'select' )
	util.execute_request(**request)

	print '\nNewChic : checkout ............................'
	request=newchic_checkout( campaign_data, device_data, app_data )
	result = util.execute_request(**request)
	try:
		d = {}
		res = json.loads(result.get('data'))
		if res.get('result'):
			if res.get('result').get('statistics'):
				d = res.get('result').get('statistics')

		if not d:
			d_list = [{"fb_content_type":"product","fb_currency":"USD","content_value":1,"num_items":1,"fb_content_id":"1353926-16221US","value":3.39,"content_category":"Beauty > Nail Art > Nail Tips"},
			{"fb_content_type":"product","fb_currency":"USD","content_value":3.74,"num_items":1,"fb_content_id":"1222373-16217US","value":5.66,"content_category":"Kids & Mom > Kid's Accessories > Hats","Color":"Blue-Gold-Pink-Purple-Red-White","Type":"Non-adjustable","Gender":"Men-Unisex-Women","Season":"Winter"},
			{"fb_content_type":"product","fb_currency":"USD","content_value":0.99,"num_items":1,"fb_content_id":"1457093-16220US","value":4.16,"content_category":"Accessories > Men's Accessories > Socks","Color":"Black-Coffee-Grey-Red-Yellow","Material":"Cotton","Style":"Ankle Sock","Gender":"Unisex","Season":"Autumn-Spring-Summer-Winter"},
			{"fb_content_type":"product","fb_currency":"USD","content_value":3.99,"num_items":1,"fb_content_id":"1223384-16223US","value":7.16,"content_category":"Kids & Mom > Kid's Accessories > Hats","Color":"Beige-Black-Blue-Grey-Navy-Red","Material":"Cotton","Season":"Fall-Spring-Winter","Pattern_Type":"Solid color","Age":"3-4 Years-4-5 Years-5-6 Years","Style":"Cute"},
			{"fb_content_type":"product","fb_currency":"USD","content_value":0.99,"num_items":1,"fb_content_id":"1177559-16223US","value":3.9,"content_category":"Accessories > Women Accessories > Socks","Color":"Beige-Black-Green-Navy-Red","Material":"Cotton","Style":"Stocking","Gender":"Women","Season":"Autumn-Spring-Summer"}]
			d = random.choice(d_list)
			print "\n Default value for 'checkout_initiate'"

		for k,v in d.items():
			d[k] = str(v)

		globals()['purchase_af_revenue'] = float(d.get('value'))
		globals()['purchase_value'] 	= str(d.get('content_value'))
		globals()['order_id'] = util.get_random_string('decimal', 8)
		globals()['purchase_af_quantity'] = int(d.get('num_items'))
		d['source'] = "direct"
		d['campaign'] = "-"
		d['medium'] = "none"

		if d.get('fb_currency'):
			d['fb_currency'] = 'USD'

		globals()['af_checkout_data'] = d
		print "\n Ok Response ..........."
		print "af_checkout_data = ",
		print globals()['af_checkout_data']

	except:
		print '\nException for "af_checkout_data"'
		d_list = [{"fb_content_type":"product","fb_currency":"USD","content_value":1,"num_items":1,"fb_content_id":"1353926-16221US","value":3.39,"content_category":"Beauty > Nail Art > Nail Tips"},
{"fb_content_type":"product","fb_currency":"USD","content_value":3.74,"num_items":1,"fb_content_id":"1222373-16217US","value":5.66,"content_category":"Kids & Mom > Kid's Accessories > Hats","Color":"Blue-Gold-Pink-Purple-Red-White","Type":"Non-adjustable","Gender":"Men-Unisex-Women","Season":"Winter"},
{"fb_content_type":"product","fb_currency":"USD","content_value":0.99,"num_items":1,"fb_content_id":"1457093-16220US","value":4.16,"content_category":"Accessories > Men's Accessories > Socks","Color":"Black-Coffee-Grey-Red-Yellow","Material":"Cotton","Style":"Ankle Sock","Gender":"Unisex","Season":"Autumn-Spring-Summer-Winter"},
{"fb_content_type":"product","fb_currency":"USD","content_value":3.99,"num_items":1,"fb_content_id":"1223384-16223US","value":7.16,"content_category":"Kids & Mom > Kid's Accessories > Hats","Color":"Beige-Black-Blue-Grey-Navy-Red","Material":"Cotton","Season":"Fall-Spring-Winter","Pattern_Type":"Solid color","Age":"3-4 Years-4-5 Years-5-6 Years","Style":"Cute"},
{"fb_content_type":"product","fb_currency":"USD","content_value":0.99,"num_items":1,"fb_content_id":"1177559-16223US","value":3.9,"content_category":"Accessories > Women Accessories > Socks","Color":"Beige-Black-Green-Navy-Red","Material":"Cotton","Style":"Stocking","Gender":"Women","Season":"Autumn-Spring-Summer"}]
		d = random.choice(d_list)

		for k,v in d.items():
			d[k] = str(v)

		globals()['purchase_af_revenue'] = float(d.get('value'))
		globals()['purchase_value'] = float(d.get('content_value'))
		globals()['order_id'] = util.get_random_string('decimal', 8)
		globals()['purchase_af_quantity'] = int(d.get('num_items'))
		d['source'] = "direct"
		d['campaign'] = "-"
		d['medium'] = "none"

		if d.get('fb_currency'):
			d['fb_currency'] = 'USD'

		globals()['af_checkout_data'] = d
		print "af_checkout_data = ",
		print globals()['af_checkout_data']




#####################################################################
##
##	SELF CALLS
##
#####################################################################

def newchic_adjust_cart( campaign_data, device_data, app_data, call_type = "" ):
	url= "http://android.newchic.com/index.php"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "NCA/"+ campaign_data.get('app_version_name') +"("+ device_data.get('model') +";Android;"+ device_data.get('sdk') +";"+device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')+")",
        "Cookie": "newchic_SID=" + globals()['newchic_SID'] + "; " + "route=" + globals()['route'] + "; " + "default_rule_country=" + globals()['default_rule_country'] + "; " + "generalAbTest=" + globals()['generalAbTest'] + "; " + "_abtest=" + globals()['_abtest'] + "; " + "dc_cid=" + globals()["dc_cid"] + "; " + "_uh_email=" + globals()['_uh_email'],
        "api-version": "30",
        "app-version": campaign_data.get('app_version_name')}

	params= {       "com": "shopcart", "t": "changeActionCartSelected"}

	data= {       "cart_ids": str(globals()['af_product_id']) + '{' + str(globals()['option_id']) + '}' + str(globals()['option_value_id']), "lang": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'), "selected": "1"}

	if call_type == 'remove' or call_type == 'clearcart':
		data['cart_ids'] = app_data['total_products_in_cart']
		data['selected'] = 0

	if call_type == 'clearcart':
		params['t'] = 'removeActionCartItem'

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def newchic_checkout( campaign_data, device_data, app_data ):
        url= "http://android.newchic.com/index.php"
        method= "post"
        headers= {       "Accept-Encoding": "gzip",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "NCA/"+ campaign_data.get('app_version_name') +"("+ device_data.get('model') +";Android;"+ device_data.get('sdk') +";"+device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')+")",
        "Cookie": "newchic_SID=" + globals()['newchic_SID'] + "; " + "route=" + globals()['route'] + "; " + "default_rule_country=" + globals()['default_rule_country'] + "; " + "generalAbTest=" + globals()['generalAbTest'] + "; " + "_abtest=" + globals()['_abtest'] + "; " + "dc_cid=" + globals()["dc_cid"] + "; " + "_uh_email=" + globals()['_uh_email'], 
        "api-version": "30",
        "app-version": campaign_data.get('app_version_name')}

        params= {       "com": "shopcart", "t": "appCheckOrder"}

        data= {       "lang": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'), "showSDK": "1"}

        return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def newchic_add_product( campaign_data, device_data, app_data ):
        url= "http://android.newchic.com/index.php"
        method= "post"
        headers= {       "Accept-Encoding": "gzip",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "NCA/"+ campaign_data.get('app_version_name') +"("+ device_data.get('model') +";Android;"+ device_data.get('sdk') +";"+device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')+")",
        "api-version": "30",
        "Cookie": "newchic_SID=" + globals()['newchic_SID'] + "; " + "route=" + globals()['route'] + "; " + "default_rule_country=" + globals()['default_rule_country'] + "; " + "generalAbTest=" + globals()['generalAbTest'] + "; " + "_abtest=" + globals()['_abtest'],
        "app-version": campaign_data.get('app_version_name')}

        params= {       "com": "shopcart", "t": "addProduct"}

        data= {
        "is_gift": "0",
        "lang": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'),
        "products_id": globals()['af_product_id'],
        "qty": "1",
        "attrs[" + str(globals()['option_id']) + ']' : globals()['option_value_id'],
        }

        return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def newchic_registration_call( campaign_data, device_data, app_data ):

        url= "http://android.newchic.com/index.php"
        method= "post"
        headers= {       "Accept-Encoding": "gzip",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "NCA/"+ campaign_data.get('app_version_name') +"("+ device_data.get('model') +";Android;"+ device_data.get('sdk') +";"+device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')+")",
        "api-version": "30",
        "Cookie": "newchic_SID=" + globals()['newchic_SID'] + "; " + "route=" + globals()['route'] + "; " + "default_rule_country=" + globals()['default_rule_country'] + "; " + "generalAbTest=" + globals()['generalAbTest'] + "; " + "_abtest=" + globals()['_abtest'],
        "app-version": campaign_data.get('app_version_name')}

        params= {       "com": "account", "t": "register"}

        data= {       "email": app_data['user']['email'],
        "from_share": "0",
        "lang":  device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'),
        "pwd": app_data['user']['password']}

        return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def newchic_product( campaign_data, device_data, app_data ):
	url= "http://android.newchic.com/api/product/appIndex"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent":  "NCA/"+ campaign_data.get('app_version_name') +"("+ device_data.get('model') +";Android;"+ device_data.get('sdk') +";"+device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')+")",
        "api-version": "30",
        "Cookie": "newchic_SID=" + globals()['newchic_SID'] + "; " + "route=" + globals()['route'] + "; " + "default_rule_country=" + globals()['default_rule_country'] + "; " + "generalAbTest=" + globals()['generalAbTest'] + "; " + "_abtest=" + globals()['_abtest'],
        "app-version": campaign_data.get('app_version_name')}

	params= {       "currency_cdn": "USD",
        "default_rule_country_cdn": "99",
        "is_new": "1",
        "lang": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'),
        "products_id": globals()['af_product_id']}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def newchic_category_display( campaign_data, device_data, app_data, call_type = 'normal', lower_limit = 0, upper_limit = 0 ):
	url= "http://android.newchic.com/api/category/display"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent": "NCA/"+ campaign_data.get('app_version_name') +"("+ device_data.get('model') +";Android;"+ device_data.get('sdk') +";"+device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')+")",
        "api-version": "30",
        "app-version": campaign_data.get('app_version_name')}

	if call_type == 'filter':
		headers["Cookie"] = "newchic_SID=" + globals()['newchic_SID'] + "; " + "route=" + globals()['route'] + "; " + "default_rule_country=" + globals()['default_rule_country'] + "; " + "generalAbTest=" + globals()['generalAbTest'] + "; " + "_abtest=" + globals()['_abtest']

	params= {       "cat_id": globals()['category'],
        "currency_cdn": "USD",
        "default_rule_country_cdn": "99",
        "filterProduct": "0",
        "lang": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'), #"en-IN",
        "page": "1",
        "pageSize": random.randint(20,40),
        "position_id": "0",
        "searchtag": "",
        "sort": "0"}

	if call_type == "filter":
		params['pfrom'] = lower_limit
		params['pto'] = upper_limit
		params['sort'] = "6"

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}



###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)

 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		"Content-Type" : "application/json",

		}
	params = {
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),
		"app_id" : campaign_data.get('package_name'),

		}
	data = {
		"registeredUninstall" : app_data.get('registeredUninstall'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"product" : device_data.get('product'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"advertiserId" : device_data.get('adid'),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"network" : device_data.get('network').upper(),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"af_events_api" : "1",
		"brand" : device_data.get('brand'),
		# "af_sdks" : "0001000000",
		"deviceType" : "user",
		"app_version_code" : campaign_data.get('app_version_code'),
		"batteryLevel" : get_batteryLevel(app_data),
		# "deviceRm" : device_data.get('build'),
		"af_preinstalled" : "false",
		"isGaidWithGps" : "true",
		"app_version_name" : campaign_data.get('app_version_name'),
		"advertiserIdEnabled" : "true",
		"device" : device_data.get('device'),
		"lang_code" : device_data.get('locale').get('language'),
		"uid" : app_data.get('uid'),
		"sdk" : device_data.get('sdk'),
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"installer_package" : "com.android.vending",
		"deviceData" : {
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"build_display_id" : device_data.get('build'),
						"btch" : "no",
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"btl" : get_batteryLevel(app_data),
						"arch" : "",
		},
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"country" : device_data.get('locale').get('country'),
		"counter" : str(app_data.get('counter')),
		"cksm_v1" : util.get_random_string('hex',34),
		"carrier" : device_data.get('carrier'),
		"isFirstCall" : isFirstCall,
		"model" : device_data.get('model'),
		"af_timestamp" : timestamp(),
		"iaecounter" : str(app_data.get('iaecounter')),
		"ivc" : False,
		'open_referrer' : "android-app://com.android.vending",
		'tokenRefreshConfigured' : False,
		'af_preinstall_name':''
		}			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)

	if app_data.get('af_gcm_token'):
		data['af_gcm_token']=app_data.get('af_gcm_token')

	data["deviceData"]["sensors"] = [{"sT":2,"sV":"MTK","sVE":[-16.38,128.52,-349.44],"sVS":[-25.14,105.96,-327.18],"sN":"MAGNETOMETER"},{"sT":1,"sV":"MTK","sVE":[-0.143,-0.143,9.787],"sVS":[-0.114,-0.047,9.749],"sN":"ACCELEROMETER"}]	

	data['cksm_v1']=checksum(data.get('installDate'), data.get('af_timestamp'))

	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],sdk=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('sdk'), device_data.get('brand'), data["af_timestamp"])
	data["kef"+kefKey] = kefVal	

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"device_id" : app_data.get('uid'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data):
	def_appsflyerUID(app_data)
	# get_google_gcmToken(app_data)
 	def_(app_data,'counter')

 	app_data['registeredUninstall']=True

	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		"Content-Type" : "application/json",

		}
	params = {
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),
		"app_id" : campaign_data.get('package_name'),

		}
	data = {
		"app_name" : campaign_data.get('app_name'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"uid" : app_data.get('uid'),
		"brand" : device_data.get('brand').upper(),
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"advertiserId" : device_data.get('adid'),
		"network" : device_data.get('network').upper(),
		"carrier" : device_data.get('carrier'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"launch_counter" : str(app_data.get('counter')),
		"operator" : device_data.get('carrier'),
		"model" : device_data.get('model'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"sdk" : device_data.get('sdk'),

		}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		"Content-Type" : "application/json",

		}
	params = {
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),
		"app_id" : campaign_data.get('package_name'),

		}
	data = {
		"registeredUninstall" : app_data.get('registeredUninstall'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"product" : device_data.get('product'),
		"installer_package" : "com.android.vending",
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"network" : device_data.get('network').upper(),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"af_events_api" : "1",
		"brand" : device_data.get('brand'),
		"deviceType" : "user",
		"app_version_code" : campaign_data.get('app_version_code'),
		# "deviceRm" : device_data.get('build'),
		"af_preinstalled" : "false",
		"isGaidWithGps" : "true",
		"app_version_name" : campaign_data.get('app_version_name'),
		"advertiserIdEnabled" : "true",
		"device" : device_data.get('device'),
		"lang_code" : device_data.get('locale').get('language'),
		"uid" : app_data.get('uid'),
		"sdk" : device_data.get('sdk'),
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"advertiserId" : device_data.get('adid'),
		"deviceData" : {
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"arch" : "",
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"build_display_id" : device_data.get('build'),
		},
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"country" : device_data.get('locale').get('country'),
		"counter" : str(app_data.get('counter')),
		"carrier" : device_data.get('carrier'),
		"isFirstCall" : "false",
		"model" : device_data.get('model'),
		"af_timestamp" : timestamp(),
		"iaecounter" : str(app_data.get('iaecounter')),
		"ivc" : False,
		'tokenRefreshConfigured' : False,
		'af_preinstall_name':''
		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('af_gcm_token'):
		data['af_gcm_token']=app_data.get('af_gcm_token')


	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)

	data['cksm_v1']=checksum(data.get('installDate'), data.get('af_timestamp'))
	
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],sdk=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('sdk'), device_data.get('brand'), data["af_timestamp"])
	data["kef"+kefKey] = kefVal	
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

###########################################################################################################################################
def gcmToken_call(campaign_data, app_data, device_data):
	url = 'http://android.clients.google.com/c2dm/register3'
	method = 'post'
	headers = {
	'Accept-Encoding' : 'gzip',
	'User-Agent'	 : 'Android-GCM/1.5 ('+device_data.get('product')+' '+device_data.get('build')+')',
	'Content-Type'	 : 'application/x-www-form-urlencoded',
	'app'	 : campaign_data.get('package_name'),
	'gcm_ver'	 : '15090016',
	'Authorization'	 : 'AidLogin 4567366446032139707:868807887254987862',
	}
	params = None
	data = {
	'X-subtype':'1041733753436',
	'sender':'1041733753436',
	'X-app_ver':campaign_data.get('app_version_code'),
	'X-osv':device_data.get('sdk'),
	'X-cliv':'fiid-'+util.get_random_string('decimal',8),
	'X-gmsv':'15090016',
	'X-appid':util.get_random_string('char_all',11),
	'X-scope':'*',
	'X-gmp_app_id':'1:'+util.get_random_string('decimal',12)+':android:'+util.get_random_string('hex',16),
	'X-app_ver_name':campaign_data.get('app_version_name'),
	'app':campaign_data.get('package_name'),
	'device':'4567366446032139707',
	'cert':util.get_random_string('hex',40),
	'app_ver':campaign_data.get('app_version_code'),
	'info':'I_'+util.get_random_string('all',18)+'_'+util.get_random_string('all',10),
	'gcm_ver':'15090016',
	'plat':'0',
	}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

######################################################################################################################
#
#														Branch calls
#
#####################################################################################################################

def branch_install(campaign_data, app_data, device_data):

	url = 'http://api.branch.io/v1/install'
	method = 'post'
	headers = {
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'Accept-Encoding':'gzip',
		'User-Agent': get_ua(device_data),
		}
	data_value = {
		'screen_dpi': int(device_data.get('dpi')),
		'branch_key': campaign_data.get('branch').get('key'),
		'is_hardware_id_real': True,
		# # 'external_intent_extra':'{"profile":0}',
		'hardware_id': device_data.get('android_id'),
		'os_version': int(device_data.get('sdk')),
		'app_version': campaign_data.get('app_version_name'),
		'brand': device_data.get('brand'),
		'screen_width': int(device_data.get('resolution').split('x')[1]),
		'is_referrable': 1,
		'update': 0,
		'sdk': campaign_data.get('branch').get('sdk'),
		'wifi': True,
		'retryNumber': 0,
		'lat_val': 0,
		'debug': False,
		'metadata':{},
		'model': device_data.get('model'),
		'os': 'Android',
		'screen_height': int(device_data.get('resolution').split('x')[0]),
		'google_advertising_id': device_data.get('adid'),
		'cd':{'mv':-1,'pn':campaign_data.get('package_name')},
		'country':device_data.get('locale').get('country'),
		'facebook_app_link_checked':False,
		'instrumentation':{'v1/install-qwt':'0'},
		'language':device_data.get('locale').get('language'),
		'local_ip':device_data.get('private_ip'),
		'clicked_referrer_ts':app_data.get('clicked_referrer_ts'),
		'environment':'FULL_APP',
		'first_install_time':app_data.get('first_install_time'),
		'install_begin_ts':app_data.get('install_begin_time_ts'),
		'latest_install_time':app_data.get('first_install_time'),
		'latest_update_time':app_data.get('first_install_time'),
		'previous_update_time':0,
		'ui_mode':'UI_MODE_TYPE_NORMAL'


		}
	data = json.dumps(data_value)
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params': None, 'data': data}


def branch_event(campaign_data, app_data, device_data):


	url = 'http://api.branch.io/v1/event'
	method = 'post'
	headers = {
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'Accept-Encoding':'gzip',
		'User-Agent':  get_ua(device_data),
		}


	data_value = {
					"identity_id": app_data.get('identity_id'),
					"device_fingerprint_id":app_data.get('device_fingerprint_id'),
					"session_id": app_data.get('session_id'),
					"event": "started",
					"metadata": {
						"time": int(time.time())
					},
					"environment": "FULL_APP",
					"hardware_id": device_data.get('android_id'),
					"is_hardware_id_real": True,
					"brand": device_data.get('brand'),
					"model": device_data.get('model'),
					"screen_dpi": device_data.get('dpi'),
					"screen_height": device_data.get('resolution').split('x')[0],
					"screen_width": device_data.get('resolution').split('x')[1],
					"wifi": True,
					"ui_mode": "UI_MODE_TYPE_NORMAL",
					"os": "Android",
					"os_version": device_data.get('sdk'),
					"country": device_data.get('locale').get('country'),
					"language": device_data.get('locale').get('language'),
					"local_ip":device_data.get('private_ip'),
					"instrumentation": {
						"v1/event-qwt": "3",
						"v1/install-brtt": "4686"
					},
					"sdk": campaign_data.get('branch').get('sdk'),
					"branch_key": campaign_data.get('branch').get('key'),
					"retryNumber": 0
				}
	data = json.dumps(data_value)
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params': None, 'data': data}

def api_branch_open(campaign_data, app_data, device_data):	
	
	url='http://api.branch.io/v1/open'
	header={
		# 'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={"app_version":campaign_data.get('app_version_name'),
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id'),
			# "session_id":app_data.get('session_id'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand').upper(),
			"model":device_data.get('model'),
			"screen_dpi": device_data.get('dpi'),
			"screen_height": device_data.get('resolution').split('x')[0],
			"screen_width": device_data.get('resolution').split('x')[1],
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": 'Android',
			"os_version": device_data.get('sdk'),		
			"country":device_data.get('locale').get('country'),
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/open-qwt":str(0)},
			"sdk": campaign_data.get('branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('branch').get('key'),	
			"cd":{'mv':'-1','pn':campaign_data.get('package_name')},
			"debug" :False,
			"environment" :'FULL_APP',
			"facebook_app_link_checked":False,
			"google_advertising_id":device_data.get('adid'),
			"is_referrable":1,
			"lat_val":0,
			"update":1,
			'latest_install_time':app_data.get('first_install_time'),
			'latest_update_time':app_data.get('first_install_time'),
			'first_install_time':app_data.get('first_install_time'),
			'previous_update_time':app_data.get('first_install_time')
	}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}


def branch_close(campaign_data, app_data, device_data):

	url = 'http://api.branch.io/v1/close'
	method = 'post'
	headers = {
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'Accept-Encoding':'gzip',
		'User-Agent': get_ua(device_data),
		}
	data_value = {
					"device_fingerprint_id": app_data.get('device_fingerprint_id'),
					"identity_id": app_data.get('identity_id'),
					"session_id": app_data.get('session_id'),
					"app_version": campaign_data.get('app_version_name'),
					"hardware_id": device_data.get('android_id'),
					"is_hardware_id_real": True,
					"brand": device_data.get('brand'),
					"model": device_data.get('model'),
					"screen_dpi": device_data.get('dpi'),
					"screen_height": device_data.get('resolution').split('x')[0],
					"screen_width": device_data.get('resolution').split('x')[1],
					"wifi": True,
					"ui_mode": "UI_MODE_TYPE_NORMAL",
					"os": "Android",
					"os_version": device_data.get('sdk'),
					"country": device_data.get('locale').get('country'),
					"language": device_data.get('locale').get('language'),
					"local_ip": device_data.get('private_ip'),
					"metadata": {},
					"instrumentation": {
						"v1/event-brtt": "367",
						"v1/close-qwt": "0"
					},
					"sdk": campaign_data.get('branch').get('sdk'),
					"branch_key": campaign_data.get('branch').get('key'),
					"retryNumber": 0
				}
	data = json.dumps(data_value)
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params': None, 'data': data}

################################################################################################

def api_branch_i( campaign_data, device_data, app_data, prod='' ):
	url= "http://api.branch.io/v2/event/standard"
	method= "post"
	headers= {       
		'Accept': 'application/json',
        'Accept-Encoding': 'gzip',
        'Content-Type': 'application/json',
        'User-Agent': get_ua(device_data),
        }

	params= None

	data= {       
		'branch_key': campaign_data.get('branch').get('key'),
        'content_items': [       
        						{       '$canonical_identifier': 'item/'+prod,
                                         '$creation_timestamp': int(time.time()*1000),
                                         '$locally_indexable': True,
                                         '$og_image_url': globals()['image_url'],
                                         '$og_title':globals()['products_name'],
                                         '$publicly_indexable': True
                                }
                            ],
        'instrumentation': {       
        							'v1/install-brtt': '7256',
                                   'v2/event/standard-qwt': '1'
                            },
        'metadata': {       },
        'name': 'VIEW_ITEM',
        'retryNumber': 0,
        'user_data': {       
        					'aaid': device_data.get('adid'),
                             'android_id': device_data.get('android_id'),
                             'app_version': campaign_data.get('app_version_name'),
                             'brand': device_data.get('brand'),
                             'country': device_data.get('locale').get('country'),
                             'developer_identity': 'bnc_no_value',
                             'device_fingerprint_id': app_data.get('device_fingerprint_id'),
                             'environment': 'FULL_APP',
                             'language': device_data.get('locale').get('language'),
                             'limit_ad_tracking': 0,
                             'local_ip': device_data.get('private_ip'),
                             'model': device_data.get('model'),
                             'os': 'Android',
                             'os_version': device_data.get('sdk'),
                             "screen_dpi": device_data.get('dpi'),
							"screen_height": device_data.get('resolution').split('x')[0],
							"screen_width": device_data.get('resolution').split('x')[1],
                             'sdk': 'android',
                             'sdk_version': campaign_data.get('branch').get('sdk'),
                             'user_agent': device_data.get('User-Agent')
                             }
                        }

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}	


def api_branch_url( campaign_data, device_data, app_data, prod='' ):
	url= "http://api.branch.io/v1/url"
	method= "post"
	headers= {       
		'Accept': 'application/json',
        'Accept-Encoding': 'gzip',
        'Content-Type': 'application/json',
        'User-Agent': get_ua(device_data),
        }

	params= None

	data= {       
		'alias': '',
        'branch_key': campaign_data.get('branch').get('key'),
        'brand':  device_data.get('brand'),
        'campaign': '',
        'channel': 'sysshare',
        'country': device_data.get('locale').get('country'),
        'data': {       
        				'$android_url': globals()['url']+'?pm=fed9dbe9c6dceef19aac1c69d3ecf256&prod_id='+prod+'&site=21',
                        '$canonical_identifier': 'item/'+prod+'',
                        '$deeplink_path': 'prod-'+prod+'?pm=fed9dbe9c6dceef19aac1c69d3ecf256&prod_id='+prod+'&site=21',
                        '$desktop_url': globals()['url']+'?pm=fed9dbe9c6dceef19aac1c69d3ecf256&prod_id='+prod+'&site=21',
                        '$ios_url': globals()['url']+'?pm=fed9dbe9c6dceef19aac1c69d3ecf256&prod_id='+prod+'&site=21',
                        '$og_description': 'Your friend shared a fashion website for you and give you $60 coupons! Claim it now.',
                        '$og_image_height': '361',
                        '$og_image_url': globals()['image_url'],
                        '$og_image_width': '361',
                        '$og_title': 'Your friend give you $60 coupons -- Newchic',
                        '$publicly_indexable': 'true',
                        'og_image_url': globals()['image_url'],
                        'og_redirect': globals()['url']+'?pm=fed9dbe9c6dceef19aac1c69d3ecf256&prod_id='+prod+'&site=21',
                        'og_title': 'Your friend give you $60 coupons -- Newchic',
                        'og_type': 'product',
                        'og_url': globals()['url']+'?pm=fed9dbe9c6dceef19aac1c69d3ecf256&prod_id='+prod+'&site=21',
                        'share_source': 'Newchic Android',
                        'source': 'android',
                        'twitter_image_url': globals()['image_url'],
                        'twitter_title': 'Your friend give you $60 coupons -- Newchic'
                        },
        'device_fingerprint_id': app_data.get('device_fingerprint_id'),
        'feature': 'sharing',
        'hardware_id': device_data.get('android_id'),
        'identity_id': app_data.get('identity_id'),
        'instrumentation': {       
        							'v1/url-qwt': '2573',
                                   'v2/event/standard-brtt': '831'
                                 },
        'is_hardware_id_real': True,
        'language': device_data.get('locale').get('language'),
        'local_ip': device_data.get('private_ip'),
        'metadata': {       },
        'model': device_data.get('model'),
        'os': 'Android',
        'os_version': device_data.get('sdk'),
        'retryNumber': 0,
        "screen_dpi": device_data.get('dpi'),
		"screen_height": device_data.get('resolution').split('x')[0],
		"screen_width": device_data.get('resolution').split('x')[1],
        'sdk': campaign_data.get('branch').get('sdk'),
        'session_id': app_data.get('session_id'),
        'stage': '',
        'tags': [],
        'ui_mode': 'UI_MODE_TYPE_NORMAL',
        'wifi': True
        }

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

########################################################################################################


def dc_banggood( campaign_data, device_data, app_data ):
	url= "http://dc.banggood.com/index.php"
	method= "post"
	headers= {       
		'Accept-Encoding': 'gzip',
        'Content-Type': 'application/x-www-form-urlencoded',
        'User-Agent': 'NCA/'+campaign_data.get('app_version_name')+'('+device_data.get('model')+';Android;'+device_data.get('os_version')+';'+device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')+')'
        }

	params= {       
				'com': 'record', 
				't': 'visit'
				}

	data= {       
		'af_device_id': app_data.get('uid'),
        'alias_page': 'SplashPage',
        'alias_prev_page': '',
        'app_channel': 'GooglePlay',
        'app_version': campaign_data.get('app_version_name'),
        'bglang': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
        'browser': 'app',
        'customers_id': '',
        'dcApp': 'newchicAndroid',
        'device_number': device_data.get('android_id'),
        'net_name': 'WIFI',
        'operating_system': device_data.get('os_version'),
        'prev_page': '',
        'site': 'android.newchic.com',
        'visit_page': 'SplashPage',
        'win_size': device_data.get('resolution').replace('x','*')
        }

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


def android_newchic_in( campaign_data, device_data, app_data, prod='' ):
	url= "http://android.newchic.com/index.php"
	method= "post"
	headers= {       
		'Accept-Encoding': 'gzip',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cookie': 'device_number='+device_data.get('android_id')+'; rec_sid=1937060832|1558012200; _bgLang='+device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+'; _abtest=1; __productDetailNewCoupon=1; recommend_SID=huf876l3vnin0n31fgdm6nqb85; app_sys=android; _uh_email=b3d2da935eb473014c79c735c231aed618c5e5a69c0647cd6be0a681a5be02e8; __ACCESS=1; currency=INR; __bgvisit='+urllib.quote(app_data.get('bgvisit'))+'; default_rule_country=99; site=android.newchic.in; gdpr_status=1|1|1|1|1; __bguser='+urllib.quote(app_data.get('bguser'))+'; loadCartTime='+str(int(time.time()))+'; generalAbTest=36; route=0dd669f57e1e1665b95b36008c61fe2e; rec_uid=808158205|1558012200|1937060832|1558012200; dc_cid=70d1py6IaxX6zXfuIUenHAPF46cHXgrhZAsIWJjksXoa5KsvjA; gdpr_is_agree=1; __bgqueue='+urllib.quote(app_data.get('bgqueue'))+'; newchic_SID=2b749f605b7c14b09ded1583f974ab8d; gdpr_is_eu=0; app_version='+campaign_data.get('app_version_name')+'; gdpr_is_show_privacypolicy=0; setCurrency=1; ncim-bypass=false',
        'User-Agent': 'NCA/'+campaign_data.get('app_version_name')+'('+device_data.get('model')+';Android;'+device_data.get('os_version')+';'+device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')+')',
        'api-version': '30',
        'app-version': campaign_data.get('app_version_name'),
        }

	params= {       
				'com': 'product', 
				't': 'getActivity'
				}

	data= {       
				'device_id':device_data.get('android_id'),
				'lang': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
				'products_id': prod
				}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################################################	

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')




###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	if not app_data.get('prev_event_name'):
		app_data['prev_event_name']  = ""
		app_data['prev_event_value'] = ""
		app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))


###########################################################
# Utility methods : MISC
#
###########################################################

def timestamp():
	return str(int(time.time()*1000))


#######################################################
#
#		KEF Value
#
#######################################################

def generateValue1get(ts, firstLaunch, sdk_version):
    gethash = sha256(ts+firstLaunch+sdk_version)
    return gethash[:16]

def generateValue(b,x,s,p,ts,fl,sdk):
    part1=generateValue1get(ts,fl,sdk)
    print part1
    str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, "utf-8")
    for i in range(len(str_)):
        str_[i] = int((str_[i] ^ ((i % 2) + 42)))

    stringBuilder = ""
    for toHexString in str_:
        toHexString2 = str(hex(int(toHexString))).strip("0x")
        if 1 == len(toHexString2):
            toHexString2 = "0"+str(toHexString2)
        stringBuilder+=toHexString2
    part2 = stringBuilder.__str__()
    return part1+part2

#######################################################
#
#			KEF Key
#
#######################################################
global temp, counter

temp = 0
counter = 1


def get_key_half(sdk, brand, af_ts):
    c = '\u0000'
    obj2 = af_ts[::-1]
    if obj2 == None:
        obj2 = util(8, 17, '㟳')
        i = globals()['counter'] + 119
        globals()['temp'] = i % 128
        i %= 2
    # try:
    # brand = brand[::-1]
    out = calculate(sdk, obj2, brand)
    i = len(out)
    if i > 4:
        i2 = globals()['counter']+65
        globals()['temp'] = i2 % 128
        if (1 if i2 % 2 != 0 else None) != 1:
            out.delete(4, i)
        else:
            out.delete(5, i)
    else:
        while i < 4:
            i3 = globals()['counter'] + 119
            globals()['temp'] = i3 % 128
            if (16 if i3 % 2 != 0 else 93) != 16:
                i += 1
                out+='1'
            else:
                i += 66
                out+='H'
            i3 = globals()['counter'] + 109
            globals()['temp'] = i3 % 128
            i3 %= 2
    # out=util(3, 25, 'ⶖ')+str(out)
    return out.__str__()
    # except Exception as e:
    #     stringBuilder.append(util(42, 28, c))
    #     stringBuilder.append(e)
    #     return util(7, 70, '达')


def calculate(sdk, obj, brand):
    allList = [sdk, obj, brand]
    arrayList = []
    i = 0
    while True:
        flag = 1
        if i >= 3:
            break
        i2 = globals()['temp'] + 63
        globals()['counter'] = i2 % 128
        if i2 % 2 != 0:
            flag = None
        if flag != None:
            arrayList.append(len(allList[i]))
            i += 31
        else:
            arrayList.append(len(allList[i]))
            i += 1
    sorted(arrayList)
    intValue = int(arrayList[0])
    out = ""
    i3 = 0
    while i3 < intValue:
        i4 = globals()['counter']+99
        globals()['temp'] = i4 % 128
        i5 =  globals()['temp']+67
        globals()['counter'] = i5 % 128
        i5 %= 2
        number = None
        if i4 % 2 != 0:
            a = 57
        else:
            a = 83
        if a != 83:
            i4 = 1
        else:
            i4 = 0
        while i4 < 3:
            i5 = globals()['temp'] + 87
            globals()['counter'] = i5 % 128
            i5 %= 2
            i5 = ord(allList[i4][i3])
            if (92 if number == None else 39) != 39:
                i6 = globals()['temp'] + 55
                globals()['counter'] = i6 % 128
                i6 %= 2
                i6 = globals()['counter'] + 39
                globals()['temp'] = i6 % 128
                i6 %= 2
            else:
                i5 ^= int(number)
            number = i5
            i4 += 1
        out+=str(hex(int(number)))[2:]
        i3 += 1
    return out

# print get_key_half("23", "vivo", "1556085610061")

#####################################################################################
#
#					CHECKSUM GENERATE
#
#####################################################################################

def checksum(date1,af_time):

	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+date1+af_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	sb = md5_result

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')


	n = af_time
	n4 = 0
	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
	 		
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100
		
	sb = insert_char(sb,23,str(n3))
	print sb

	return sb

################################################################
#
#			Methods
#
################################################################
def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()

def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())	

#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

	
def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		#app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		#app_data['user']['firstname'] 	= first_name
		#app_data['user']['lastname'] 	= last_name
		#app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['password']	= username+ util.get_random_string('all',4)




##############################################
#	more registration self calls
##############################################


def extra_registration_calls(campaign_data, device_data, app_data):

	print "\registration self call : logapi.banggood.gelf\n"
	request=logapi_banggood_gelf( campaign_data, device_data, app_data )
	util.execute_request(**request)


	print "\registration self call : newchic.appGeneralInfo\n"
	request=newchic_appgeneralinfo( campaign_data, device_data, app_data )
	res = util.execute_request(**request)
	try:
		result = json.loads(res.get('data'))
		if result.get('result'):
			if result.get('result').get('customers_id'):
				app_data['newchic_customers_id'] = result.get('result').get('customers_id')

		if not app_data.get('newchic_customers_id'):
			app_data['newchic_customers_id'] = "33482282"
	except:
		app_data['newchic_customers_id'] = "33482282"


	print "\registration self call : newchic.insertToken\n"
	request=newchic_insert_token( campaign_data, device_data, app_data )
	util.execute_request(**request)


	print "\registration self call : newchic.getPushSet\n"
	request=newchic_getpushset( campaign_data, device_data, app_data )
	util.execute_request(**request)


	print "\registration self call : banggood.SignUpPage\n"
	request=banggood_signuppage( campaign_data, device_data, app_data )
	util.execute_request(**request)


##############################################
#	more registration self call functions
##############################################


def logapi_banggood_gelf( campaign_data, device_data, app_data ):
	url= "https://logapi.banggood.com/gelf"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": "okhttp/3.11.0"}

	params= None

	if not app_data.get('NCA_deviceUUID'):
		app_data['NCA_deviceUUID'] = str(uuid.uuid4())

	data= {       "NCA_apiVersion": "30",
        "NCA_appLang": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'), #"en-IN",
        "NCA_currency": "USD",
        "NCA_device": device_data.get('device'),
        "NCA_deviceId": device_data.get('android_id'),
        "NCA_deviceUUID": app_data.get('NCA_deviceUUID'),
        "NCA_eventName": "SignUpSuccess",
        "NCA_model": device_data.get('model'),
        "NCA_release": device_data.get('os_version'),
        "NCA_type": "newchic",
        "NCA_versionCode": campaign_data.get('app_version_code'),
        "build_type": "release",
        "gl_type": "NCA",
        "table_name": "LoginEvent",
        "version": campaign_data.get('app_version_name')}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


def newchic_appgeneralinfo( campaign_data, device_data, app_data ):
    url= "http://android.newchic.com/index.php"
    method= "post"
    headers= {       "Accept-Encoding": "gzip",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "NCA/"+ campaign_data.get('app_version_name') +"("+ device_data.get('model') +";Android;"+ device_data.get('sdk') +";"+device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')+")",
        "Cookie": "newchic_SID=" + globals()['newchic_SID'] + "; " + "route=" + globals()['route'] + "; " + "default_rule_country=" + globals()['default_rule_country'] + "; " + "generalAbTest=" + globals()['generalAbTest'] + "; " + "_abtest=" + globals()['_abtest'] + "; " + "dc_cid=" + globals()["dc_cid"] + "; " + "_uh_email=" + globals()['_uh_email'],
        "api-version": "30",
        "app-version": campaign_data.get('app_version_name')}

    params= {       "com": "account", "t": "appGeneralInfo"}

    data= {       "lang": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')}

    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def newchic_insert_token( campaign_data, device_data, app_data ):
	url= "http://android.newchic.com/index.php"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "NCA/"+ campaign_data.get('app_version_name') +"("+ device_data.get('model') +";Android;"+ device_data.get('sdk') +";"+device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')+")",
        "api-version": "30",
        "app-version": campaign_data.get('app_version_name')}

	params= {       "com": "ajax", "t": "insertToken"}

	data= {       "adwords": json.dumps({"rdid":device_data.get('adid'),"lat":"0","locale":device_data.get('locale').get('language') + '_' + device_data.get('locale').get('country'),"build_id":device_data.get('build'),"ad_name":campaign_data.get('package_name'),"os_version":device_data.get('os_version'),"device_model":device_data.get('model')}),
        "customers_id": app_data.get('newchic_customers_id'),
        "device_id": device_data.get('android_id'),
        "email": app_data['user']['email'],
        "lang": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'),
        "one_signal_id": str(uuid.uuid4()),
        "versions": campaign_data.get('app_version_name')}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def newchic_getpushset( campaign_data, device_data, app_data ):
	url= "http://android.newchic.com/index.php"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "NCA/"+ campaign_data.get('app_version_name') +"("+ device_data.get('model') +";Android;"+ device_data.get('sdk') +";"+device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')+")",
        "api-version": "30",
        "app-version": campaign_data.get('app_version_name')}

	params= {       "com": "ajax", "t": "getPushSet"}

	data= {       "device_id": device_data.get('android_id'),
        "email": app_data['user']['email'],
        "lang": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def banggood_signuppage( campaign_data, device_data, app_data ):
	url= "http://dc.banggood.com/index.php"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "NCA/"+ campaign_data.get('app_version_name') +"("+ device_data.get('model') +";Android;"+ device_data.get('sdk') +";"+device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country')+")"}

	params= {       "com": "record", "t": "visit"}

	data= {       "alias_page": "BrandHomePage",
        "alias_prev_page": "SignUpPage",
        "app_channel": "GooglePlay",
        "app_version": campaign_data.get('app_version_name'),
        "bglang": device_data.get('locale').get('language') + '-' + device_data.get('locale').get('country'),
        "browser": "app",
        "customers_id": app_data.get('newchic_customers_id'),
        "dcApp": "newchicAndroidIn",
        "device_number": device_data.get('android_id'),
        "net_name": "WIFI",
        "operating_system": device_data.get('os_version'),
        "prev_page": "SignUpPage",
        "site": "android.newchic.com",
        "visit_page": "BrandHomePage?act=back",
        "win_size": device_data.get('resolution').split('x')[1] + "*" + device_data.get('resolution').split('x')[0]}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}