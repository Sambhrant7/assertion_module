# -*- coding: utf8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from sdk import installtimenew
from sdk import getsleep
from sdk import util
from sdk import NameLists,purchase
import uuid
import time
import random
import json
import string
import datetime
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.photobook.android',
	'app_size'			 : 8.5,
	'app_name' 			 :'Photobook',
	'app_version_name' 	 :'2.12.0', #2.11.0
	'app_version_code' 	 :'2783', #2783
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'PuP2tJMANgJxJsss9Q7zxD',
		'dkh'		 : 'PuP2tJMA',
		'buildnumber': '4.10.0',
		'version'	 : 'v4',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	def_firstLaunchDate(app_data,device_data)

	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)

	app_data['user_id']='ANONYMOUS'

	global cart
	cart = list()

 	###########	 CALLS		 ############

	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	time.sleep(random.randint(8,10))		
	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	response=util.execute_request(**request)

	time.sleep(random.randint(2,3))			
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)

	print "\nself_call_api-mob_photobookworldwide\n"
	request=api_mob_photobookworldwide_GetCountries( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	app_data['country_data'] = dict()
	try:
		result = json.loads(response.get('data')).get("data").get("getCountries")
		for item in result:	
			app_data['country_data'][item.get("cca2")] = list()
			app_data['country_data'][item.get("cca2")].append(item.get("store").get('code'))
			app_data['country_data'][item.get("cca2")].append(item.get("store").get('currency').get('code'))

		if not app_data.get('country_data'):
			app_data['country_data'] = {'BD': ['PBWW', 'USD'], 'BE': ['PBE', 'EUR'], 'BG': ['PBE', 'EUR'], 'BB': ['PBWW', 'USD'], 'BN': ['PBBN', 'SGD'], 'BO': ['PBWW', 'USD'], 'JP': ['PBJP', 'JPY'], 'JM': ['PBWW', 'USD'], 'JO': ['PBWW', 'USD'], 'BR': ['PBWW', 'USD'], 'BS': ['PBWW', 'USD'], 'JE': ['PBE', 'EUR'], 'BY': ['PBE', 'EUR'], 'R': ['PBWW', 'USD'], 'RS': ['PBE', 'EUR'], 'RO': ['PBE', 'EUR'], 'GT': ['PBWW', 'USD'], 'GR': ['PBE', 'EUR'], 'GY': ['PBWW', 'USD'], 'GE': ['PBE', 'EUR'], 'GB': ['PBUK', 'GBP'], 'GH': ['PBWW', 'USD'], 'OM': ['PBWW', 'USD'], 'HR': ['PBE', 'EUR'], 'H': ['PBE', 'EUR'], 'HK': ['PBHK', 'HKD'], 'PT': ['PBE', 'EUR'], 'PY': ['PBWW', 'USD'], 'PA': ['PBWW', 'USD'], 'UY': ['PBWW', 'USD'], 'PE': ['PBWW', 'USD'], 'PK': ['PBWW', 'USD'], 'PH': ['PBPH', 'PHP'], 'PL': ['PBE', 'EUR'], 'EE': ['PBE', 'EUR'], 'EG': ['PBWW', 'USD'], 'ZA': ['PBWW', 'USD'], 'EC': ['PBWW', 'USD'], 'VN': ['PBVN', 'VND'], 'ET': ['PBWW', 'USD'], 'ES': ['PBE', 'EUR'], 'MA': ['PBWW', 'USD'], 'ML': ['PBWW', 'USD'], 'MO': ['PBHK', 'HKD'], 'MN': ['PBWW', 'USD'], 'US': ['PBUS', 'USD'], 'MV': ['PBWW', 'USD'], 'MY': ['PBMY', 'MYR'], 'MX': ['PBWW', 'USD'], 'IL': ['PBWW', 'USD'], 'FR': ['PBE', 'EUR'], 'NL': ['PBE', 'EUR'], 'NG': ['PBWW', 'USD'], 'NZ': ['PBNZ', 'NZD'], 'NP': ['PBWW', 'USD'], 'CO': ['PBWW', 'USD'], 'CN': ['PBCN', 'USD'], 'CM': ['PBWW', 'USD'], 'CL': ['PBWW', 'USD'], 'CA': ['PBCA', 'CAD'], 'CG': ['PBWW', 'USD'], 'CZ': ['PBE', 'EUR'], 'CY': ['PBE', 'EUR'], 'CR': ['PBWW', 'USD'], 'C': ['PBWW', 'USD'], 'SY': ['PBWW', 'USD'], 'KE': ['PBWW', 'USD'], 'SK': ['PBE', 'EUR'], 'KR': ['PBKR', 'USD'], 'SI': ['PBE', 'EUR'], 'SA': ['PBWW', 'USD'], 'SG': ['PBSG', 'SGD'], 'DO': ['PBWW', 'USD'], 'DK': ['PBE', 'EUR'], 'DE': ['PBE', 'EUR'], 'DZ': ['PBWW', 'USD'], 'TZ': ['PBWW', 'USD'], 'TW': ['PBTW', 'TWD'], 'TR': ['PBWW', 'USD'], 'LK': ['PBWW', 'USD'], 'TN': ['PBWW', 'USD'], 'L': ['PBE', 'EUR'], 'TH': ['PBTH', 'THB'], 'LY': ['PBWW', 'USD'], 'AE': ['PBAE', 'USD'], 'VE': ['PBWW', 'USD'], 'IQ': ['PBWW', 'USD'], 'IS': ['PBE', 'EUR'], 'IR': ['PBWW', 'USD'], 'IT': ['PBE', 'EUR'], 'AO': ['PBWW', 'USD'], 'AR': ['PBWW', 'USD'], 'A': ['PBA', 'AUD'], 'AT': ['PBE', 'EUR'], 'IN': ['PBIN', 'USD'], 'IE': ['PBE', 'EUR'], 'ID': ['PBID', 'IDR'], 'UA': ['PBE', 'EUR'], 'QA': ['PBWW', 'USD']}	
	except:
		app_data['country_data'] = {'BD': ['PBWW', 'USD'], 'BE': ['PBE', 'EUR'], 'BG': ['PBE', 'EUR'], 'BB': ['PBWW', 'USD'], 'BN': ['PBBN', 'SGD'], 'BO': ['PBWW', 'USD'], 'JP': ['PBJP', 'JPY'], 'JM': ['PBWW', 'USD'], 'JO': ['PBWW', 'USD'], 'BR': ['PBWW', 'USD'], 'BS': ['PBWW', 'USD'], 'JE': ['PBE', 'EUR'], 'BY': ['PBE', 'EUR'], 'R': ['PBWW', 'USD'], 'RS': ['PBE', 'EUR'], 'RO': ['PBE', 'EUR'], 'GT': ['PBWW', 'USD'], 'GR': ['PBE', 'EUR'], 'GY': ['PBWW', 'USD'], 'GE': ['PBE', 'EUR'], 'GB': ['PBUK', 'GBP'], 'GH': ['PBWW', 'USD'], 'OM': ['PBWW', 'USD'], 'HR': ['PBE', 'EUR'], 'H': ['PBE', 'EUR'], 'HK': ['PBHK', 'HKD'], 'PT': ['PBE', 'EUR'], 'PY': ['PBWW', 'USD'], 'PA': ['PBWW', 'USD'], 'UY': ['PBWW', 'USD'], 'PE': ['PBWW', 'USD'], 'PK': ['PBWW', 'USD'], 'PH': ['PBPH', 'PHP'], 'PL': ['PBE', 'EUR'], 'EE': ['PBE', 'EUR'], 'EG': ['PBWW', 'USD'], 'ZA': ['PBWW', 'USD'], 'EC': ['PBWW', 'USD'], 'VN': ['PBVN', 'VND'], 'ET': ['PBWW', 'USD'], 'ES': ['PBE', 'EUR'], 'MA': ['PBWW', 'USD'], 'ML': ['PBWW', 'USD'], 'MO': ['PBHK', 'HKD'], 'MN': ['PBWW', 'USD'], 'US': ['PBUS', 'USD'], 'MV': ['PBWW', 'USD'], 'MY': ['PBMY', 'MYR'], 'MX': ['PBWW', 'USD'], 'IL': ['PBWW', 'USD'], 'FR': ['PBE', 'EUR'], 'NL': ['PBE', 'EUR'], 'NG': ['PBWW', 'USD'], 'NZ': ['PBNZ', 'NZD'], 'NP': ['PBWW', 'USD'], 'CO': ['PBWW', 'USD'], 'CN': ['PBCN', 'USD'], 'CM': ['PBWW', 'USD'], 'CL': ['PBWW', 'USD'], 'CA': ['PBCA', 'CAD'], 'CG': ['PBWW', 'USD'], 'CZ': ['PBE', 'EUR'], 'CY': ['PBE', 'EUR'], 'CR': ['PBWW', 'USD'], 'C': ['PBWW', 'USD'], 'SY': ['PBWW', 'USD'], 'KE': ['PBWW', 'USD'], 'SK': ['PBE', 'EUR'], 'KR': ['PBKR', 'USD'], 'SI': ['PBE', 'EUR'], 'SA': ['PBWW', 'USD'], 'SG': ['PBSG', 'SGD'], 'DO': ['PBWW', 'USD'], 'DK': ['PBE', 'EUR'], 'DE': ['PBE', 'EUR'], 'DZ': ['PBWW', 'USD'], 'TZ': ['PBWW', 'USD'], 'TW': ['PBTW', 'TWD'], 'TR': ['PBWW', 'USD'], 'LK': ['PBWW', 'USD'], 'TN': ['PBWW', 'USD'], 'L': ['PBE', 'EUR'], 'TH': ['PBTH', 'THB'], 'LY': ['PBWW', 'USD'], 'AE': ['PBAE', 'USD'], 'VE': ['PBWW', 'USD'], 'IQ': ['PBWW', 'USD'], 'IS': ['PBE', 'EUR'], 'IR': ['PBWW', 'USD'], 'IT': ['PBE', 'EUR'], 'AO': ['PBWW', 'USD'], 'AR': ['PBWW', 'USD'], 'A': ['PBA', 'AUD'], 'AT': ['PBE', 'EUR'], 'IN': ['PBIN', 'USD'], 'IE': ['PBE', 'EUR'], 'ID': ['PBID', 'IDR'], 'UA': ['PBE', 'EUR'], 'QA': ['PBWW', 'USD']}


	print "\nself_call_api-mob_photobookworldwide\n"
	request=api_mob_photobookworldwide_GetProductMenus( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global product
	product  = dict()
	try:
		result = json.loads(response.get('data')).get('data').get('productMenu')
		for i in result:
			for item in i.get('product_families'):
				product[json.loads(item).get('productFamilyId')] = list()
				product[json.loads(item).get('productFamilyId')].append(json.loads(item).get('displayName'))
				for k in json.loads(item).get("productSizes"):
					a = dict()
					a['attributeValueId'] 	= k.get("attributeValueId")
					a['productId'] 			= k.get("productId")
					a['displayName'] 		= k.get("displayName")
					a['unitPrice'] 			= k.get("unitPrice")
					product[json.loads(item).get('productFamilyId')].append(a)

		if not product:
			product = {101: ["Lil' Photobook", {'unitPrice': 10, 'displayName': '4" x 4" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TSS0404', 'productId': '3472'}, {'unitPrice': 15, 'displayName': '5" x 5" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TSS0505', 'productId': '3473'}, {'unitPrice': 15, 'displayName': '6" x 4" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TLS0604', 'productId': '3475'}, {'unitPrice': 25, 'displayName': '7" x 5" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TLS0705', 'productId': '3474'}], 110: ['Photo Tiles', {'unitPrice': 99, 'displayName': '8" x 8" Photo Tiles  - Front Mount', 'attributeValueId': 'MOB_PTS88_FM', 'productId': '3727'}, {'unitPrice': 99, 'displayName': '8" x 8" Photo Tiles - Back Mount', 'attributeValueId': 'MOB_PTS88_BM', 'productId': '3726'}], 111: ['Canvas', {'unitPrice': 79, 'displayName': '8" x 8" Standard Canvas', 'attributeValueId': 'MOB_CVS88', 'productId': '3747'}, {'unitPrice': 169, 'displayName': '12" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVS1212', 'productId': '3773'}, {'unitPrice': 269, 'displayName': '16" x 16" Standard Canvas', 'attributeValueId': 'MOB_CVS1616', 'productId': '3772'}, {'unitPrice': 369, 'displayName': '24" x 24" Standard Canvas', 'attributeValueId': 'MOB_CVS2424', 'productId': '3771'}, {'unitPrice': 129, 'displayName': '12" x 8" Standard Canvas', 'attributeValueId': 'MOB_CVL128', 'productId': '3768'}, {'unitPrice': 229, 'displayName': '18" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL1812', 'productId': '3767'}, {'unitPrice': 279, 'displayName': '20" x 16" Standard Canvas', 'attributeValueId': 'MOB_CVL2016', 'productId': '3766'}, {'unitPrice': 369, 'displayName': '30" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVL3020', 'productId': '3765'}, {'unitPrice': 499, 'displayName': '36" x 24" Standard Canvas', 'attributeValueId': 'MOB_CVL3624', 'productId': '3764'}, {'unitPrice': 129, 'displayName': '8" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVP812', 'productId': '3760'}, {'unitPrice': 229, 'displayName': '12" x 18" Standard Canvas', 'attributeValueId': 'MOB_CVP1218', 'productId': '3759'}, {'unitPrice': 279, 'displayName': '16" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVP1620', 'productId': '3758'}, {'unitPrice': 369, 'displayName': '20" x 30" Standard Canvas', 'attributeValueId': 'MOB_CVP2030', 'productId': '3757'}, {'unitPrice': 499, 'displayName': '24" x 36" Standard Canvas', 'attributeValueId': 'MOB_CVP2436', 'productId': '3756'}, {'unitPrice': 279, 'displayName': '24" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL2412', 'productId': '3753'}, {'unitPrice': 339, 'displayName': '40" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL4012', 'productId': '3752'}, {'unitPrice': 479, 'displayName': '40" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVL4020', 'productId': '3751'}], 86: ['Canvas Air', {'unitPrice': 79, 'displayName': '8" x 8" Canvas Air', 'attributeValueId': 'MOB_CVAS88', 'productId': '2614'}, {'unitPrice': 169, 'displayName': '12" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAS1212', 'productId': '2615'}, {'unitPrice': 269, 'displayName': '16" x 16" Canvas Air', 'attributeValueId': 'MOB_CVAS1616', 'productId': '2616'}, {'unitPrice': 129, 'displayName': '12" x 8" Canvas Air', 'attributeValueId': 'MOB_CVAL128', 'productId': '2617'}, {'unitPrice': 229, 'displayName': '18" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAL1812', 'productId': '2618'}, {'unitPrice': 279, 'displayName': '20" x 16" Canvas Air', 'attributeValueId': 'MOB_CVAL2016', 'productId': '2619'}, {'unitPrice': 129, 'displayName': '8" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAP812', 'productId': '2620'}, {'unitPrice': 229, 'displayName': '12" x 18" Canvas Air', 'attributeValueId': 'MOB_CVAP1218', 'productId': '2621'}, {'unitPrice': 279, 'displayName': '16" x 20" Canvas Air', 'attributeValueId': 'MOB_CVAP1620', 'productId': '2622'}]}			
	except:
		product = {101: ["Lil' Photobook", {'unitPrice': 10, 'displayName': '4" x 4" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TSS0404', 'productId': '3472'}, {'unitPrice': 15, 'displayName': '5" x 5" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TSS0505', 'productId': '3473'}, {'unitPrice': 15, 'displayName': '6" x 4" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TLS0604', 'productId': '3475'}, {'unitPrice': 25, 'displayName': '7" x 5" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TLS0705', 'productId': '3474'}], 110: ['Photo Tiles', {'unitPrice': 99, 'displayName': '8" x 8" Photo Tiles  - Front Mount', 'attributeValueId': 'MOB_PTS88_FM', 'productId': '3727'}, {'unitPrice': 99, 'displayName': '8" x 8" Photo Tiles - Back Mount', 'attributeValueId': 'MOB_PTS88_BM', 'productId': '3726'}], 111: ['Canvas', {'unitPrice': 79, 'displayName': '8" x 8" Standard Canvas', 'attributeValueId': 'MOB_CVS88', 'productId': '3747'}, {'unitPrice': 169, 'displayName': '12" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVS1212', 'productId': '3773'}, {'unitPrice': 269, 'displayName': '16" x 16" Standard Canvas', 'attributeValueId': 'MOB_CVS1616', 'productId': '3772'}, {'unitPrice': 369, 'displayName': '24" x 24" Standard Canvas', 'attributeValueId': 'MOB_CVS2424', 'productId': '3771'}, {'unitPrice': 129, 'displayName': '12" x 8" Standard Canvas', 'attributeValueId': 'MOB_CVL128', 'productId': '3768'}, {'unitPrice': 229, 'displayName': '18" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL1812', 'productId': '3767'}, {'unitPrice': 279, 'displayName': '20" x 16" Standard Canvas', 'attributeValueId': 'MOB_CVL2016', 'productId': '3766'}, {'unitPrice': 369, 'displayName': '30" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVL3020', 'productId': '3765'}, {'unitPrice': 499, 'displayName': '36" x 24" Standard Canvas', 'attributeValueId': 'MOB_CVL3624', 'productId': '3764'}, {'unitPrice': 129, 'displayName': '8" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVP812', 'productId': '3760'}, {'unitPrice': 229, 'displayName': '12" x 18" Standard Canvas', 'attributeValueId': 'MOB_CVP1218', 'productId': '3759'}, {'unitPrice': 279, 'displayName': '16" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVP1620', 'productId': '3758'}, {'unitPrice': 369, 'displayName': '20" x 30" Standard Canvas', 'attributeValueId': 'MOB_CVP2030', 'productId': '3757'}, {'unitPrice': 499, 'displayName': '24" x 36" Standard Canvas', 'attributeValueId': 'MOB_CVP2436', 'productId': '3756'}, {'unitPrice': 279, 'displayName': '24" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL2412', 'productId': '3753'}, {'unitPrice': 339, 'displayName': '40" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL4012', 'productId': '3752'}, {'unitPrice': 479, 'displayName': '40" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVL4020', 'productId': '3751'}], 86: ['Canvas Air', {'unitPrice': 79, 'displayName': '8" x 8" Canvas Air', 'attributeValueId': 'MOB_CVAS88', 'productId': '2614'}, {'unitPrice': 169, 'displayName': '12" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAS1212', 'productId': '2615'}, {'unitPrice': 269, 'displayName': '16" x 16" Canvas Air', 'attributeValueId': 'MOB_CVAS1616', 'productId': '2616'}, {'unitPrice': 129, 'displayName': '12" x 8" Canvas Air', 'attributeValueId': 'MOB_CVAL128', 'productId': '2617'}, {'unitPrice': 229, 'displayName': '18" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAL1812', 'productId': '2618'}, {'unitPrice': 279, 'displayName': '20" x 16" Canvas Air', 'attributeValueId': 'MOB_CVAL2016', 'productId': '2619'}, {'unitPrice': 129, 'displayName': '8" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAP812', 'productId': '2620'}, {'unitPrice': 229, 'displayName': '12" x 18" Canvas Air', 'attributeValueId': 'MOB_CVAP1218', 'productId': '2621'}, {'unitPrice': 279, 'displayName': '16" x 20" Canvas Air', 'attributeValueId': 'MOB_CVAP1620', 'productId': '2622'}]}
				


	if not app_data.get('register') and random.randint(1,100)<=60:	
		time.sleep(random.randint(30,60))	
		print "\nself_call_api-mob_photobookworldwide\n"
		request=api_mob_photobookworldwide_SignUp( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			result=json.loads(response.get('data')).get('data').get('register').get('user').get('id')
			app_data['token'] = json.loads(response.get('data')).get('data').get('register').get('token')
			app_data['user_id'] = result
		except:
			app_data['user_id'] = random.randint(4800000,4899999)	

		app_data['register'] = True	

	for _ in range(random.randint(1,3)):

		global family_id,family_name,prod
		f_ids = product.keys()
		family_id = random.choice(f_ids)
		family_name = product.get(family_id)[0]
		prod = random.choice(product.get(family_id)[1:])			

		if random.randint(1,100)<=90:
			time.sleep(random.randint(30,60))	
			af_content_view(campaign_data, app_data, device_data)

			if random.randint(1,100)<=77:
				time.sleep(random.randint(30,60))	
				cart.append({"id":prod.get("productId"),"sku":prod.get("attributeValueId"),"quantity":1,"item_price":prod.get("unitPrice")})
				af_add_to_cart(campaign_data, app_data, device_data)

				if random.randint(1,100)<=30 and app_data.get('register'):	
					time.sleep(random.randint(30,60))	
					af_initiated_checkout(campaign_data, app_data, device_data)

					if random.randint(1,100)<=50:
						time.sleep(random.randint(30,60))
						order(campaign_data, app_data, device_data)

						if purchase.isPurchase(app_data,day=1,advertiser_demand=10):	
							time.sleep(random.randint(30,60))
							af_purchase(campaign_data, app_data, device_data)

		if random.randint(1,100)<=70:
			time.sleep(random.randint(1,2))		
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	def_firstLaunchDate(app_data,device_data)

	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)

	if not app_data.get('user_id'):
		app_data['user_id']='ANONYMOUS'

	global cart
	cart = list()

 	###########	 CALLS		 ############

	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)

	if not app_data.get('country_data'):
		print "\nself_call_api-mob_photobookworldwide\n"
		request=api_mob_photobookworldwide_GetCountries( campaign_data, device_data, app_data )
		response = util.execute_request(**request)
		app_data['country_data'] = dict()
		try:
			result = json.loads(response.get('data')).get("data").get("getCountries")
			for item in result:	
				app_data['country_data'][item.get("cca2")] = list()
				app_data['country_data'][item.get("cca2")].append(item.get("store").get('code'))
				app_data['country_data'][item.get("cca2")].append(item.get("store").get('currency').get('code'))

			if not app_data.get('country_data'):
				app_data['country_data'] = {'BD': ['PBWW', 'USD'], 'BE': ['PBE', 'EUR'], 'BG': ['PBE', 'EUR'], 'BB': ['PBWW', 'USD'], 'BN': ['PBBN', 'SGD'], 'BO': ['PBWW', 'USD'], 'JP': ['PBJP', 'JPY'], 'JM': ['PBWW', 'USD'], 'JO': ['PBWW', 'USD'], 'BR': ['PBWW', 'USD'], 'BS': ['PBWW', 'USD'], 'JE': ['PBE', 'EUR'], 'BY': ['PBE', 'EUR'], 'R': ['PBWW', 'USD'], 'RS': ['PBE', 'EUR'], 'RO': ['PBE', 'EUR'], 'GT': ['PBWW', 'USD'], 'GR': ['PBE', 'EUR'], 'GY': ['PBWW', 'USD'], 'GE': ['PBE', 'EUR'], 'GB': ['PBUK', 'GBP'], 'GH': ['PBWW', 'USD'], 'OM': ['PBWW', 'USD'], 'HR': ['PBE', 'EUR'], 'H': ['PBE', 'EUR'], 'HK': ['PBHK', 'HKD'], 'PT': ['PBE', 'EUR'], 'PY': ['PBWW', 'USD'], 'PA': ['PBWW', 'USD'], 'UY': ['PBWW', 'USD'], 'PE': ['PBWW', 'USD'], 'PK': ['PBWW', 'USD'], 'PH': ['PBPH', 'PHP'], 'PL': ['PBE', 'EUR'], 'EE': ['PBE', 'EUR'], 'EG': ['PBWW', 'USD'], 'ZA': ['PBWW', 'USD'], 'EC': ['PBWW', 'USD'], 'VN': ['PBVN', 'VND'], 'ET': ['PBWW', 'USD'], 'ES': ['PBE', 'EUR'], 'MA': ['PBWW', 'USD'], 'ML': ['PBWW', 'USD'], 'MO': ['PBHK', 'HKD'], 'MN': ['PBWW', 'USD'], 'US': ['PBUS', 'USD'], 'MV': ['PBWW', 'USD'], 'MY': ['PBMY', 'MYR'], 'MX': ['PBWW', 'USD'], 'IL': ['PBWW', 'USD'], 'FR': ['PBE', 'EUR'], 'NL': ['PBE', 'EUR'], 'NG': ['PBWW', 'USD'], 'NZ': ['PBNZ', 'NZD'], 'NP': ['PBWW', 'USD'], 'CO': ['PBWW', 'USD'], 'CN': ['PBCN', 'USD'], 'CM': ['PBWW', 'USD'], 'CL': ['PBWW', 'USD'], 'CA': ['PBCA', 'CAD'], 'CG': ['PBWW', 'USD'], 'CZ': ['PBE', 'EUR'], 'CY': ['PBE', 'EUR'], 'CR': ['PBWW', 'USD'], 'C': ['PBWW', 'USD'], 'SY': ['PBWW', 'USD'], 'KE': ['PBWW', 'USD'], 'SK': ['PBE', 'EUR'], 'KR': ['PBKR', 'USD'], 'SI': ['PBE', 'EUR'], 'SA': ['PBWW', 'USD'], 'SG': ['PBSG', 'SGD'], 'DO': ['PBWW', 'USD'], 'DK': ['PBE', 'EUR'], 'DE': ['PBE', 'EUR'], 'DZ': ['PBWW', 'USD'], 'TZ': ['PBWW', 'USD'], 'TW': ['PBTW', 'TWD'], 'TR': ['PBWW', 'USD'], 'LK': ['PBWW', 'USD'], 'TN': ['PBWW', 'USD'], 'L': ['PBE', 'EUR'], 'TH': ['PBTH', 'THB'], 'LY': ['PBWW', 'USD'], 'AE': ['PBAE', 'USD'], 'VE': ['PBWW', 'USD'], 'IQ': ['PBWW', 'USD'], 'IS': ['PBE', 'EUR'], 'IR': ['PBWW', 'USD'], 'IT': ['PBE', 'EUR'], 'AO': ['PBWW', 'USD'], 'AR': ['PBWW', 'USD'], 'A': ['PBA', 'AUD'], 'AT': ['PBE', 'EUR'], 'IN': ['PBIN', 'USD'], 'IE': ['PBE', 'EUR'], 'ID': ['PBID', 'IDR'], 'UA': ['PBE', 'EUR'], 'QA': ['PBWW', 'USD']}	
		except:
			app_data['country_data'] = {'BD': ['PBWW', 'USD'], 'BE': ['PBE', 'EUR'], 'BG': ['PBE', 'EUR'], 'BB': ['PBWW', 'USD'], 'BN': ['PBBN', 'SGD'], 'BO': ['PBWW', 'USD'], 'JP': ['PBJP', 'JPY'], 'JM': ['PBWW', 'USD'], 'JO': ['PBWW', 'USD'], 'BR': ['PBWW', 'USD'], 'BS': ['PBWW', 'USD'], 'JE': ['PBE', 'EUR'], 'BY': ['PBE', 'EUR'], 'R': ['PBWW', 'USD'], 'RS': ['PBE', 'EUR'], 'RO': ['PBE', 'EUR'], 'GT': ['PBWW', 'USD'], 'GR': ['PBE', 'EUR'], 'GY': ['PBWW', 'USD'], 'GE': ['PBE', 'EUR'], 'GB': ['PBUK', 'GBP'], 'GH': ['PBWW', 'USD'], 'OM': ['PBWW', 'USD'], 'HR': ['PBE', 'EUR'], 'H': ['PBE', 'EUR'], 'HK': ['PBHK', 'HKD'], 'PT': ['PBE', 'EUR'], 'PY': ['PBWW', 'USD'], 'PA': ['PBWW', 'USD'], 'UY': ['PBWW', 'USD'], 'PE': ['PBWW', 'USD'], 'PK': ['PBWW', 'USD'], 'PH': ['PBPH', 'PHP'], 'PL': ['PBE', 'EUR'], 'EE': ['PBE', 'EUR'], 'EG': ['PBWW', 'USD'], 'ZA': ['PBWW', 'USD'], 'EC': ['PBWW', 'USD'], 'VN': ['PBVN', 'VND'], 'ET': ['PBWW', 'USD'], 'ES': ['PBE', 'EUR'], 'MA': ['PBWW', 'USD'], 'ML': ['PBWW', 'USD'], 'MO': ['PBHK', 'HKD'], 'MN': ['PBWW', 'USD'], 'US': ['PBUS', 'USD'], 'MV': ['PBWW', 'USD'], 'MY': ['PBMY', 'MYR'], 'MX': ['PBWW', 'USD'], 'IL': ['PBWW', 'USD'], 'FR': ['PBE', 'EUR'], 'NL': ['PBE', 'EUR'], 'NG': ['PBWW', 'USD'], 'NZ': ['PBNZ', 'NZD'], 'NP': ['PBWW', 'USD'], 'CO': ['PBWW', 'USD'], 'CN': ['PBCN', 'USD'], 'CM': ['PBWW', 'USD'], 'CL': ['PBWW', 'USD'], 'CA': ['PBCA', 'CAD'], 'CG': ['PBWW', 'USD'], 'CZ': ['PBE', 'EUR'], 'CY': ['PBE', 'EUR'], 'CR': ['PBWW', 'USD'], 'C': ['PBWW', 'USD'], 'SY': ['PBWW', 'USD'], 'KE': ['PBWW', 'USD'], 'SK': ['PBE', 'EUR'], 'KR': ['PBKR', 'USD'], 'SI': ['PBE', 'EUR'], 'SA': ['PBWW', 'USD'], 'SG': ['PBSG', 'SGD'], 'DO': ['PBWW', 'USD'], 'DK': ['PBE', 'EUR'], 'DE': ['PBE', 'EUR'], 'DZ': ['PBWW', 'USD'], 'TZ': ['PBWW', 'USD'], 'TW': ['PBTW', 'TWD'], 'TR': ['PBWW', 'USD'], 'LK': ['PBWW', 'USD'], 'TN': ['PBWW', 'USD'], 'L': ['PBE', 'EUR'], 'TH': ['PBTH', 'THB'], 'LY': ['PBWW', 'USD'], 'AE': ['PBAE', 'USD'], 'VE': ['PBWW', 'USD'], 'IQ': ['PBWW', 'USD'], 'IS': ['PBE', 'EUR'], 'IR': ['PBWW', 'USD'], 'IT': ['PBE', 'EUR'], 'AO': ['PBWW', 'USD'], 'AR': ['PBWW', 'USD'], 'A': ['PBA', 'AUD'], 'AT': ['PBE', 'EUR'], 'IN': ['PBIN', 'USD'], 'IE': ['PBE', 'EUR'], 'ID': ['PBID', 'IDR'], 'UA': ['PBE', 'EUR'], 'QA': ['PBWW', 'USD']}


	print "\nself_call_api-mob_photobookworldwide\n"
	request=api_mob_photobookworldwide_GetProductMenus( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global product
	product  = dict()
	try:
		result = json.loads(response.get('data')).get('data').get('productMenu')
		for i in result:
			for item in i.get('product_families'):
				product[json.loads(item).get('productFamilyId')] = list()
				product[json.loads(item).get('productFamilyId')].append(json.loads(item).get('displayName'))
				for k in json.loads(item).get("productSizes"):
					a = dict()
					a['attributeValueId'] 	= k.get("attributeValueId")
					a['productId'] 			= k.get("productId")
					a['displayName'] 		= k.get("displayName")
					a['unitPrice'] 			= k.get("unitPrice")
					product[json.loads(item).get('productFamilyId')].append(a)

		if not product:
			product = {101: ["Lil' Photobook", {'unitPrice': 10, 'displayName': '4" x 4" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TSS0404', 'productId': '3472'}, {'unitPrice': 15, 'displayName': '5" x 5" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TSS0505', 'productId': '3473'}, {'unitPrice': 15, 'displayName': '6" x 4" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TLS0604', 'productId': '3475'}, {'unitPrice': 25, 'displayName': '7" x 5" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TLS0705', 'productId': '3474'}], 110: ['Photo Tiles', {'unitPrice': 99, 'displayName': '8" x 8" Photo Tiles  - Front Mount', 'attributeValueId': 'MOB_PTS88_FM', 'productId': '3727'}, {'unitPrice': 99, 'displayName': '8" x 8" Photo Tiles - Back Mount', 'attributeValueId': 'MOB_PTS88_BM', 'productId': '3726'}], 111: ['Canvas', {'unitPrice': 79, 'displayName': '8" x 8" Standard Canvas', 'attributeValueId': 'MOB_CVS88', 'productId': '3747'}, {'unitPrice': 169, 'displayName': '12" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVS1212', 'productId': '3773'}, {'unitPrice': 269, 'displayName': '16" x 16" Standard Canvas', 'attributeValueId': 'MOB_CVS1616', 'productId': '3772'}, {'unitPrice': 369, 'displayName': '24" x 24" Standard Canvas', 'attributeValueId': 'MOB_CVS2424', 'productId': '3771'}, {'unitPrice': 129, 'displayName': '12" x 8" Standard Canvas', 'attributeValueId': 'MOB_CVL128', 'productId': '3768'}, {'unitPrice': 229, 'displayName': '18" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL1812', 'productId': '3767'}, {'unitPrice': 279, 'displayName': '20" x 16" Standard Canvas', 'attributeValueId': 'MOB_CVL2016', 'productId': '3766'}, {'unitPrice': 369, 'displayName': '30" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVL3020', 'productId': '3765'}, {'unitPrice': 499, 'displayName': '36" x 24" Standard Canvas', 'attributeValueId': 'MOB_CVL3624', 'productId': '3764'}, {'unitPrice': 129, 'displayName': '8" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVP812', 'productId': '3760'}, {'unitPrice': 229, 'displayName': '12" x 18" Standard Canvas', 'attributeValueId': 'MOB_CVP1218', 'productId': '3759'}, {'unitPrice': 279, 'displayName': '16" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVP1620', 'productId': '3758'}, {'unitPrice': 369, 'displayName': '20" x 30" Standard Canvas', 'attributeValueId': 'MOB_CVP2030', 'productId': '3757'}, {'unitPrice': 499, 'displayName': '24" x 36" Standard Canvas', 'attributeValueId': 'MOB_CVP2436', 'productId': '3756'}, {'unitPrice': 279, 'displayName': '24" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL2412', 'productId': '3753'}, {'unitPrice': 339, 'displayName': '40" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL4012', 'productId': '3752'}, {'unitPrice': 479, 'displayName': '40" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVL4020', 'productId': '3751'}], 86: ['Canvas Air', {'unitPrice': 79, 'displayName': '8" x 8" Canvas Air', 'attributeValueId': 'MOB_CVAS88', 'productId': '2614'}, {'unitPrice': 169, 'displayName': '12" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAS1212', 'productId': '2615'}, {'unitPrice': 269, 'displayName': '16" x 16" Canvas Air', 'attributeValueId': 'MOB_CVAS1616', 'productId': '2616'}, {'unitPrice': 129, 'displayName': '12" x 8" Canvas Air', 'attributeValueId': 'MOB_CVAL128', 'productId': '2617'}, {'unitPrice': 229, 'displayName': '18" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAL1812', 'productId': '2618'}, {'unitPrice': 279, 'displayName': '20" x 16" Canvas Air', 'attributeValueId': 'MOB_CVAL2016', 'productId': '2619'}, {'unitPrice': 129, 'displayName': '8" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAP812', 'productId': '2620'}, {'unitPrice': 229, 'displayName': '12" x 18" Canvas Air', 'attributeValueId': 'MOB_CVAP1218', 'productId': '2621'}, {'unitPrice': 279, 'displayName': '16" x 20" Canvas Air', 'attributeValueId': 'MOB_CVAP1620', 'productId': '2622'}]}			
	except:
		product = {101: ["Lil' Photobook", {'unitPrice': 10, 'displayName': '4" x 4" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TSS0404', 'productId': '3472'}, {'unitPrice': 15, 'displayName': '5" x 5" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TSS0505', 'productId': '3473'}, {'unitPrice': 15, 'displayName': '6" x 4" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TLS0604', 'productId': '3475'}, {'unitPrice': 25, 'displayName': '7" x 5" Layflat Softcover Lil\' Photobook', 'attributeValueId': 'MOB_LF-TLS0705', 'productId': '3474'}], 110: ['Photo Tiles', {'unitPrice': 99, 'displayName': '8" x 8" Photo Tiles  - Front Mount', 'attributeValueId': 'MOB_PTS88_FM', 'productId': '3727'}, {'unitPrice': 99, 'displayName': '8" x 8" Photo Tiles - Back Mount', 'attributeValueId': 'MOB_PTS88_BM', 'productId': '3726'}], 111: ['Canvas', {'unitPrice': 79, 'displayName': '8" x 8" Standard Canvas', 'attributeValueId': 'MOB_CVS88', 'productId': '3747'}, {'unitPrice': 169, 'displayName': '12" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVS1212', 'productId': '3773'}, {'unitPrice': 269, 'displayName': '16" x 16" Standard Canvas', 'attributeValueId': 'MOB_CVS1616', 'productId': '3772'}, {'unitPrice': 369, 'displayName': '24" x 24" Standard Canvas', 'attributeValueId': 'MOB_CVS2424', 'productId': '3771'}, {'unitPrice': 129, 'displayName': '12" x 8" Standard Canvas', 'attributeValueId': 'MOB_CVL128', 'productId': '3768'}, {'unitPrice': 229, 'displayName': '18" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL1812', 'productId': '3767'}, {'unitPrice': 279, 'displayName': '20" x 16" Standard Canvas', 'attributeValueId': 'MOB_CVL2016', 'productId': '3766'}, {'unitPrice': 369, 'displayName': '30" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVL3020', 'productId': '3765'}, {'unitPrice': 499, 'displayName': '36" x 24" Standard Canvas', 'attributeValueId': 'MOB_CVL3624', 'productId': '3764'}, {'unitPrice': 129, 'displayName': '8" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVP812', 'productId': '3760'}, {'unitPrice': 229, 'displayName': '12" x 18" Standard Canvas', 'attributeValueId': 'MOB_CVP1218', 'productId': '3759'}, {'unitPrice': 279, 'displayName': '16" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVP1620', 'productId': '3758'}, {'unitPrice': 369, 'displayName': '20" x 30" Standard Canvas', 'attributeValueId': 'MOB_CVP2030', 'productId': '3757'}, {'unitPrice': 499, 'displayName': '24" x 36" Standard Canvas', 'attributeValueId': 'MOB_CVP2436', 'productId': '3756'}, {'unitPrice': 279, 'displayName': '24" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL2412', 'productId': '3753'}, {'unitPrice': 339, 'displayName': '40" x 12" Standard Canvas', 'attributeValueId': 'MOB_CVL4012', 'productId': '3752'}, {'unitPrice': 479, 'displayName': '40" x 20" Standard Canvas', 'attributeValueId': 'MOB_CVL4020', 'productId': '3751'}], 86: ['Canvas Air', {'unitPrice': 79, 'displayName': '8" x 8" Canvas Air', 'attributeValueId': 'MOB_CVAS88', 'productId': '2614'}, {'unitPrice': 169, 'displayName': '12" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAS1212', 'productId': '2615'}, {'unitPrice': 269, 'displayName': '16" x 16" Canvas Air', 'attributeValueId': 'MOB_CVAS1616', 'productId': '2616'}, {'unitPrice': 129, 'displayName': '12" x 8" Canvas Air', 'attributeValueId': 'MOB_CVAL128', 'productId': '2617'}, {'unitPrice': 229, 'displayName': '18" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAL1812', 'productId': '2618'}, {'unitPrice': 279, 'displayName': '20" x 16" Canvas Air', 'attributeValueId': 'MOB_CVAL2016', 'productId': '2619'}, {'unitPrice': 129, 'displayName': '8" x 12" Canvas Air', 'attributeValueId': 'MOB_CVAP812', 'productId': '2620'}, {'unitPrice': 229, 'displayName': '12" x 18" Canvas Air', 'attributeValueId': 'MOB_CVAP1218', 'productId': '2621'}, {'unitPrice': 279, 'displayName': '16" x 20" Canvas Air', 'attributeValueId': 'MOB_CVAP1620', 'productId': '2622'}]}
				


	if not app_data.get('register') and random.randint(1,100)<=60:	
		time.sleep(random.randint(30,60))	
		print "\nself_call_api-mob_photobookworldwide\n"
		request=api_mob_photobookworldwide_SignUp( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			result=json.loads(response.get('data')).get('data').get('register').get('user').get('id')
			app_data['token'] = json.loads(response.get('data')).get('data').get('register').get('token')
			app_data['user_id'] = result
		except:
			app_data['user_id'] = random.randint(4800000,4899999)	

		app_data['register'] = True	

	if day<=4:
		num = random.randint(1,3)
	elif day<=7:
		num = random.randint(1,2)
	else:
		num = random.randint(0,1)			

	for _ in range(num):

		global family_id,family_name,prod
		f_ids = product.keys()
		family_id = random.choice(f_ids)
		family_name = product.get(family_id)[0]
		prod = random.choice(product.get(family_id)[1:])			

		if random.randint(1,100)<=90:
			time.sleep(random.randint(30,60))	
			af_content_view(campaign_data, app_data, device_data)

			if random.randint(1,100)<=77:
				time.sleep(random.randint(30,60))	
				cart.append({"id":prod.get("productId"),"sku":prod.get("attributeValueId"),"quantity":1,"item_price":prod.get("unitPrice")})
				af_add_to_cart(campaign_data, app_data, device_data)

				if random.randint(1,100)<=30 and app_data.get('register'):	
					time.sleep(random.randint(30,60))	
					af_initiated_checkout(campaign_data, app_data, device_data)

					if random.randint(1,100)<=50:
						time.sleep(random.randint(30,60))
						order(campaign_data, app_data, device_data)

						if purchase.isPurchase(app_data,day,advertiser_demand=20):	
							time.sleep(random.randint(30,60))
							af_purchase(campaign_data, app_data, device_data)

		if random.randint(1,100)<=70:
			time.sleep(random.randint(1,2))		
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)

	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def af_content_view(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_content_view'
	eventName			= 'af_content_view'
	eventValue			= json.dumps({"af_price":prod.get('unitPrice'),"af_content_id":prod.get("productId"),"sku":prod.get("attributeValueId"),"af_content":prod.get("displayName"),"af_content_type":"product","af_currency":app_data.get('country_data').get(device_data.get('locale').get('country'))[1],"family_id":family_id,"family_name":family_name},ensure_ascii=False)
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_add_to_cart(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_add_to_cart'
	eventName			= 'af_add_to_cart'
	eventValue			= json.dumps({"af_price":prod.get('unitPrice'),"af_content_id":prod.get("productId"),"sku":prod.get("attributeValueId"),"af_content":[{"id":prod.get("productId"),"sku":prod.get("attributeValueId"),"quantity":1,"item_price":prod.get("unitPrice")}],"af_content_type":"product","af_currency":app_data.get('country_data').get(device_data.get('locale').get('country'))[1],"family_id":family_id,"family_name":family_name},ensure_ascii=False)
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_initiated_checkout(campaign_data, app_data, device_data):
	total_sum = 0
	af_content_id = list()
	for item in cart:
		total_sum += eval(str(item.get('item_price')))
		af_content_id.append(str(item.get('id'))) 
	print 'Appsflyer : EVENT___________________________af_initiated_checkout'
	eventName			= 'af_initiated_checkout'
	eventValue			= json.dumps({"af_price":total_sum,"af_content_id":",".join(af_content_id),"af_quantity":len(cart),"af_currency":app_data.get('country_data').get(device_data.get('locale').get('country'))[1],"af_content":cart},ensure_ascii=False)
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def order(campaign_data, app_data, device_data):
	total_sum = 0
	af_content_id = list()
	for item in cart:
		total_sum += eval(str(item.get('item_price')))
		af_content_id.append(str(item.get('id'))) 
	print 'Appsflyer : EVENT___________________________order'
	eventName			= 'order'
	eventValue			= json.dumps({"af_price":total_sum,"af_content_id":",".join(af_content_id),"af_quantity":len(cart),"payment_method":"paypal_express","shipping_method":"GLOBALMAIL","af_content":cart,"af_content_type":"product","af_currency":app_data.get('country_data').get(device_data.get('locale').get('country'))[1]},ensure_ascii=False)
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def af_purchase(campaign_data, app_data, device_data):
	total_sum = 0
	af_content_id = list()
	for item in cart:
		total_sum += eval(str(item.get('item_price')))
		af_content_id.append(str(item.get('id'))) 
	af_revenue = total_sum+random.choice([10.95,12.95,2.95,2.95,9.95,9.95])  	
	# shipping charges have been randomly generated based on shipping address of australia , as shipping chrges could not be fetched from self calls
	print 'Appsflyer : EVENT___________________________af_purchase'
	eventName			= 'af_purchase'
	eventValue			= json.dumps({"af_content_id":",".join(af_content_id),"af_quantity":len(cart),"payment_method":"paypal_express","shipping_method":"GLOBALMAIL","af_content":cart,"af_content_type":"product","af_currency":app_data.get('country_data').get(device_data.get('locale').get('country'))[1],"af_revenue":af_revenue},ensure_ascii=False)
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)		

###################################################################


def api_mob_photobookworldwide_GetCountries( campaign_data, device_data, app_data ):
	url= "https://api-mob.photobookworldwide.com/graphql"
	method= "post"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "CONTENT_TYPE": "application/json",
        "Content-Type": "application/json; charset=utf-8",
        "Platform": "mobile",
        "User-Agent": "Photobook/Android/VersionCode:"+campaign_data.get('app_version_code')+"/VersionName:"+campaign_data.get('app_version_name')+"/PhoneModel:"+device_data.get('model')+"/OS:MMB29M release-keys_"+device_data.get('os_version')+"/AndroidSDK:"+device_data.get('sdk')+"/BuildFlavor:productionrelease",
        "X-AP-LangCode": "en",
        "X-APOLLO-CACHE-FETCH-STRATEGY": "NETWORK_ONLY",
        "X-APOLLO-CACHE-KEY": util.get_random_string(type='hex',size=32),
        "X-APOLLO-EXPIRE-AFTER-READ": "false",
        "X-APOLLO-EXPIRE-TIMEOUT": "0",
        "X-APOLLO-OPERATION-ID": util.get_random_string(type='hex',size=64),
        "X-APOLLO-OPERATION-NAME": "GetCountries",
        "X-APOLLO-PREFETCH": "false",
        "x-phone-id": str(uuid.uuid4())
        }

	params= {       
		"operation": "GetCountries",
        "platform": "android",
        "store": "",
        "version": campaign_data.get('app_version_name'),
        }

	data= {       
		"query": "query GetCountries {  getCountries {    __typename    name    cca2    store {      __typename      name      code      url      timezone      logo      logo_white      flag      currency {        __typename        id        code        name        symbol        decimal_places      }    }  }}",
        "variables": {       }}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}
	
def api_mob_photobookworldwide_GetProductMenus( campaign_data, device_data, app_data ):
	url= "https://api-mob.photobookworldwide.com/graphql"
	method= "post"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "CONTENT_TYPE": "application/json",
        "Content-Type": "application/json; charset=utf-8",
        "Platform": "mobile",
        "User-Agent": "Photobook/Android/VersionCode:"+campaign_data.get('app_version_code')+"/VersionName:"+campaign_data.get('app_version_name')+"/PhoneModel:"+device_data.get('model')+"/OS:MMB29M release-keys_"+device_data.get('os_version')+"/AndroidSDK:"+device_data.get('sdk')+"/BuildFlavor:productionrelease",
        "X-AP-LangCode": "en",
        "X-APOLLO-CACHE-FETCH-STRATEGY": "NETWORK_ONLY",
        "X-APOLLO-CACHE-KEY": util.get_random_string(type='hex',size=32),
        "X-APOLLO-EXPIRE-AFTER-READ": "false",
        "X-APOLLO-EXPIRE-TIMEOUT": "0",
        "X-APOLLO-OPERATION-ID": util.get_random_string(type='hex',size=64),
        "X-APOLLO-OPERATION-NAME": "GetProductMenus",
        "X-APOLLO-PREFETCH": "false",
        "x-phone-id": str(uuid.uuid4())
        }

	params= {       
		"operation": "GetProductMenus",
        "platform": "android",
        "store": app_data.get('country_data').get(device_data.get('locale').get('country'))[0],
        "version": campaign_data.get('app_version_name'),
        }

	data= {       
		"query": "query GetProductMenus($store: String!, $currency: String!, $language: String!) {  productMenu(store: $store, language: $language, currency: $currency) {    __typename    menu_id    display_name    image    position    product_families  }}",
        "variables": {       
        				"currency": app_data.get('country_data').get(device_data.get('locale').get('country'))[1],
        				"language": device_data.get('locale').get('language'),
        				"store": app_data.get('country_data').get(device_data.get('locale').get('country'))[0],
        		}
        	}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def api_mob_photobookworldwide_SignUp( campaign_data, device_data, app_data ):
	register_user(app_data)
	url= "https://api-mob.photobookworldwide.com/graphql"
	method= "post"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "CONTENT_TYPE": "application/json",
        "Content-Type": "application/json; charset=utf-8",
        "Platform": "mobile",
        "User-Agent": "Photobook/Android/VersionCode:"+campaign_data.get('app_version_code')+"/VersionName:"+campaign_data.get('app_version_name')+"/PhoneModel:"+device_data.get('model')+"/OS:MMB29M release-keys_"+device_data.get('os_version')+"/AndroidSDK:"+device_data.get('sdk')+"/BuildFlavor:productionrelease",
        "X-AP-LangCode": "en",
        "X-APOLLO-OPERATION-ID": util.get_random_string(type='hex',size=64),
        "X-APOLLO-OPERATION-NAME": "SignUp",
        "x-phone-id": str(uuid.uuid4())
        }

	params= {       
		"operation": "SignUp",
        "platform": "android",
        "store": app_data.get('country_data').get(device_data.get('locale').get('country'))[0],
        "version": campaign_data.get('app_version_name')
       }

	data= {       
		"query": "mutation SignUp($firstName: String!, $lastName: String!, $email: String!, $password: String!, $countryCode: String!, $store: String!) {  register(email: $email, first_name: $firstName, last_name: $lastName, password: $password, country_code: $countryCode, store: $store) {    __typename    token    printbox_session {      __typename      session_id      expiry_time    }    user {      __typename      id      email      first_name      last_name      country_code      country_display_name      store      store_display_name      store_currency {        __typename        id        code        name        symbol        decimal_places      }    }  }}",
        "variables": {       
        					"countryCode": device_data.get('locale').get('country'),
                            "email": app_data.get('user').get('email'),
                            "firstName": app_data.get('user').get('firstname'),
                            "lastName": app_data.get('user').get('lastname'),
                            "password": app_data.get('user').get('username'),
                            "store": app_data.get('country_data').get(device_data.get('locale').get('country'))[0],
                    }
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

	# print "\nself_call_api-mob_photobookworldwide\n"
	# request=api_mob_photobookworldwide_address( campaign_data, device_data, app_data )
	# util.execute_request(**request)

	# print "\nself_call_api-mob_photobookworldwide\n"
	# request=api_mob_photobookworldwide_address_order( campaign_data, device_data, app_data )
	# util.execute_request(**request)	

	# print "\nself_call_api-mob_photobookworldwide\n"
	# request=api_mob_photobookworldwide_getCart( campaign_data, device_data, app_data )
	# util.execute_request(**request)

	# print "\nself_call_api-mob_photobookworldwide\n"
	# request=api_mob_photobookworldwide_checkout( campaign_data, device_data, app_data )
	# util.execute_request(**request)

# def api_mob_photobookworldwide_address( campaign_data, device_data, app_data ):
# 	url= "https://api-mob.photobookworldwide.com/api/v1/"+country_data.get(device_data.get('locale').get('country'))[0]+"/address/store"
# 	method= "post"
# 	headers= {       
# 		"Accept-Encoding": "gzip",
#         "Authorization": "Bearer "+app_data.get('token'),
#         "Cache-Control": "no-cache",
#         "Content-Type": "application/json",
#         "Platform": "mobile",
#         "User-Agent": "Photobook/Android/VersionCode:"+campaign_data.get('app_version_code')+"/VersionName:"+campaign_data.get('app_version_name')+"/PhoneModel:"+device_data.get('model')+"/OS:MMB29M release-keys_"+device_data.get('os_version')+"/AndroidSDK:"+device_data.get('sdk')+"/BuildFlavor:productionrelease",
#         "X-AP-LangCode": "en",
#         "x-phone-id": str(uuid.uuid4())
#         }

# 	params= None

# 	data= {       
# 		"abn": None,
#         "city": "Cordelia",
#         "company": None,
#         "country_code": "AU",
#         "country_name": "Australia",
#         "first_name": "Happu",
#         "id": None,
#         "is_default_billing": True,
#         "is_default_shipping": True,
#         "last_name": "Singh",
#         "postcode": "4850",
#         "state": "Queensland",
#         "street1": "97 Cedar Street",
#         "street2": None,
#         "street3": None,
#         "street4": None,
#         "telephone_number": "0740603045",
#         "user_id": "4814212"
#         }

# 	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

	
# def api_mob_photobookworldwide_address_order( campaign_data, device_data, app_data ):
# 	url= "https://api-mob.photobookworldwide.com/api/v1/PBAU/en/order/list"
# 	method= "get"
# 	headers= {       
# 		"Accept-Encoding": "gzip",
#         "Authorization": "Bearer "+app_data.get('token'),
#         "Cache-Control": "no-cache",
#         "Platform": "mobile",
#         "User-Agent": "Photobook/Android/VersionCode:"+campaign_data.get('app_version_code')+"/VersionName:"+campaign_data.get('app_version_name')+"/PhoneModel:"+device_data.get('model')+"/OS:MMB29M release-keys_"+device_data.get('os_version')+"/AndroidSDK:"+device_data.get('sdk')+"/BuildFlavor:productionrelease",
#         "X-AP-LangCode": "en",
#         "x-phone-id": str(uuid.uuid4())
#         }

# 	params= None

# 	data= None
# 	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

	
# def api_mob_photobookworldwide_checkout( campaign_data, device_data, app_data ):
# 	url= "https://api-mob.photobookworldwide.com/api/v1/PBAU/cart/mobile-checkout"
# 	method= "get"
# 	headers= {       
# 		"Accept-Encoding": "gzip",
#         "Authorization": "Bearer "+app_data.get('token'),
#         "Cache-Control": "no-cache",
#         "Platform": "mobile",
#         "User-Agent": "Photobook/Android/VersionCode:"+campaign_data.get('app_version_code')+"/VersionName:"+campaign_data.get('app_version_name')+"/PhoneModel:"+device_data.get('model')+"/OS:MMB29M release-keys_"+device_data.get('os_version')+"/AndroidSDK:"+device_data.get('sdk')+"/BuildFlavor:productionrelease",
#         "X-AP-LangCode": "en",
#         "x-phone-id": str(uuid.uuid4())
#         }

# 	params= None

# 	data= None
# 	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

	
# def api_mob_photobookworldwide_getCart( campaign_data, device_data, app_data ):
# 	url= "https://api-mob.photobookworldwide.com/graphql"
# 	method= "post"
# 	headers= {       
# 		"Accept": "application/json",
#         "Accept-Encoding": "gzip",
#         "CONTENT_TYPE": "application/json",
#         "Content-Type": "application/json; charset=utf-8",
#         "Platform": "mobile",
#         "User-Agent": "Photobook/Android/VersionCode:"+campaign_data.get('app_version_code')+"/VersionName:"+campaign_data.get('app_version_name')+"/PhoneModel:"+device_data.get('model')+"/OS:MMB29M release-keys_"+device_data.get('os_version')+"/AndroidSDK:"+device_data.get('sdk')+"/BuildFlavor:productionrelease",
#         "X-AP-LangCode": "en",
#         "X-APOLLO-CACHE-FETCH-STRATEGY": "NETWORK_ONLY",
#         "X-APOLLO-CACHE-KEY": "bc2e8666230fed058457f1812c5c8ff7",
#         "X-APOLLO-EXPIRE-AFTER-READ": "false",
#         "X-APOLLO-EXPIRE-TIMEOUT": "0",
#         "X-APOLLO-OPERATION-ID": util.get_random_string(type='hex',size=64),
#         "X-APOLLO-OPERATION-NAME": "getCart",
#         "X-APOLLO-PREFETCH": "false",
#         "x-phone-id": str(uuid.uuid4())
#        }

# 	params= {       
# 		"operation": "getCart",
#         "platform": "android",
#         "store": country_data.get(device_data.get('locale').get('country'))[0],
#         "version": campaign_data.get('app_version_name')
#         }

# 	data= {       
# 		"query": "query getCart($token: String!, $store: String!) {  cartShow(token: $token, store: $store) {    __typename    currency_code    total_tax    total_upgrade    total_discount    subtotal    subtotal_with_discount    subtotal_with_tax    cart_items {      __typename      hash_key      quantity      unit_price      custom_price      discounts {        __typename        id        voucher_code        discount_amount      }      tax_amount      total_discount      subtotal      subtotal_without_upgrade      subtotal_with_discount      subtotal_with_tax      cart_itemable    }  }}",
#         "variables": {       
#         					"store": country_data.get(device_data.get('locale').get('country'))[0],
#                             "token": app_data.get('token')
#                     }
#             }

# 	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}	


###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"appUserId" : "ANONYMOUS",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "ac",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"sensors" : [{u'sN': u'ACCELEROMETER', u'sVE': [-0.354, -0.124, 9.72], u'sV': u'MTK', u'sVS': [-0.191, -0.124, 10.056], u'sT': 1}],
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"open_referrer" : 'android-app://com.android.vending',
		"operator" : device_data.get('carrier'),
		"p_receipt" : util.get_random_string('google_token',637).replace('-','+').replace('_','/')+'=',
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"rfr" : {
								"clk" : str(int(app_data.get("times").get("click_time"))),
								"code" : "0",
								"install" : str(int(app_data.get("times").get("download_begin_time"))),
								"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",
		},
		"sc_o" : "p",
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),

		}			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('counter')>2:
		if data.get('rfr'):
			del data['rfr']
		if data.get('deviceData').get('sensors'):
			del data['deviceData']['sensors']
		if data.get("p_receipt"):
			del data["p_receipt"]

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if app_data.get('counter')==2:
		data['gcd_timing']='1800'

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))	
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"device_id" : app_data.get('uid'),
		"devkey" : campaign_data.get('appsflyer').get('key'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
 	def_(app_data,'counter')
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"app_name" : campaign_data.get('app_name'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand').upper(),
		"carrier" : device_data.get('carrier'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),
		'appUserId': app_data.get('user_id')

		}

	app_data['af_gcm_token']=str(int(time.time()*1000))

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"appUserId" : "ANONYMOUS",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : True,
		"sc_o" : "p",
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################
def change_char(s, p, r):
	return s[:p]+r+s[p+1:]
		
def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100

	if n3<10:
		n3 = '0'+str(n3)
		
	sb = insert_char(sb,23,str(n3))
	return sb



###################################################################
# appsflyer_kef_key and value generator
# parameter 			: brand, sdk, lang, af_ts, buildnumber, etc
#
# Generates kef key and value for appsflyer.
###################################################################
global temp, counter
temp = 0
counter = 1

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
	if buildnumber=="4.8.18":
		return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
	else:
		c = '\u0000'
		obj2 = af_ts[::-1]
		out = calculate(sdk, obj2, brand)
		i = len(out)
		if i > 4:
			i2 = globals()['counter']+65
			globals()['temp'] = i2 % 128
			if (1 if i2 % 2 != 0 else None) != 1:
				out.delete(4, i)
			else:
				out.delete(5, i)
		else:
			while i < 4:
				i3 = globals()['counter'] + 119
				globals()['temp'] = i3 % 128
				if (16 if i3 % 2 != 0 else 93) != 16:
					i += 1
					out+='1'
				else:
					i += 66
					out+='H'
				i3 = globals()['counter'] + 109
				globals()['temp'] = i3 % 128
				i3 %= 2
		return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
	ts = ts[::-1]
	appends = [sdk,lang,ts]
	lengths = [len(sdk),len(lang),len(ts)]
	l = sorted(lengths)
	least = l[0]
	out = ""
	for x in range(least):
		numb = None
		for y in range(len(appends)):
			charAt = ord(appends[y][x])
			if numb:
				charAt = int((charAt ^ int(numb)))

			numb = charAt
			
		out+=str(hex(numb))[2:]

	if len(out)>4:
		out = out[:4]
	elif len(out)<4:
		while len(out)<4:
			out+="1"
	
	return out

"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""

def generateValue(b,x,s,p,ts,fl,buildnumber):
	part1=generateValue1get(ts,fl,buildnumber)
	str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, "utf-8")
	for i in range(len(str_)):
		str_[i] = int((str_[i] ^ ((i % 2) + 42)))

	stringBuilder = ""
	for toHexString in str_:
		toHexString2 = str(hex(int(toHexString)))[2:]
		if 1 == len(toHexString2):
			toHexString2 = "0"+str(toHexString2)
		stringBuilder+=toHexString2
	part2 = stringBuilder.__str__()
	return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
	gethash = sha256(ts+firstLaunch+buildnumber)
	return gethash[:16]

"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""

def calculate(sdk, obj, brand):
	allList = [sdk, obj, brand]
	arrayList = []
	i = 0
	while True:
		flag = 1
		if i >= 3:
			break
		i2 = globals()['temp'] + 63
		globals()['counter'] = i2 % 128
		if i2 % 2 != 0:
			flag = None
		if flag != None:
			arrayList.append(len(allList[i]))
			i += 31
		else:
			arrayList.append(len(allList[i]))
			i += 1
	sorted(arrayList)
	intValue = int(arrayList[0])
	out = ""
	i3 = 0
	while i3 < intValue:
		i4 = globals()['counter']+99
		globals()['temp'] = i4 % 128
		i5 =  globals()['temp']+67
		globals()['counter'] = i5 % 128
		i5 %= 2
		number = None
		if i4 % 2 != 0:
			a = 57
		else:
			a = 83
		if a != 83:
			i4 = 1
		else:
			i4 = 0
		while i4 < 3:
			i5 = globals()['temp'] + 87
			globals()['counter'] = i5 % 128
			i5 %= 2
			i5 = ord(allList[i4][i3])
			if (92 if number == None else 39) != 39:
				i6 = globals()['temp'] + 55
				globals()['counter'] = i6 % 128
				i6 %= 2
				i6 = globals()['counter'] + 39
				globals()['temp'] = i6 % 128
				i6 %= 2
			else:
				i5 ^= int(number)
			number = i5
			i4 += 1
		out+=str(hex(int(number)))[2:]
		i3 += 1
	return out



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def ref_time():
	time.sleep(random.randint(10,15))
	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"


def test(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	################generating_realtime_differences##################
	def_firstLaunchDate(app_data,device_data)

	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)

	app_data['user_id']='ANONYMOUS'

	###########	 CALLS		 ############
	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	time.sleep(random.randint(8,10))		
	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	# catch(response,app_data,"appsflyer")

	time.sleep(random.randint(2,3))			
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)

	time.sleep(random.randint(11,14))	
	af_content_view(campaign_data, app_data, device_data)

	time.sleep(random.randint(1,2))		
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(25,30))	
	af_content_view(campaign_data, app_data, device_data)

	time.sleep(random.randint(67,74))	
	af_add_to_cart(campaign_data, app_data, device_data)

	time.sleep(random.randint(2,4))	
	af_initiated_checkout(campaign_data, app_data, device_data)


	return {'status':'true'}		