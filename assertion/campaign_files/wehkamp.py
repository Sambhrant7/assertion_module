# -*- coding: utf-8 -*-
from sdk import util,installtimenew
from sdk import getsleep
import time
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'app_size'           : 14.0,#13.0,#12.0,
	'package_name'		 :'nl.wehkamp.shop',
	'app_name' 			 :'wehkamp - shopping & service',
	'app_version_name' 	 : '43902',#'43502',#'43301',#'43001',#42805, '42804',#42803, 42601, '42500',#'42304',#'42303',#'42200',#'42101',#'42002',
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '5.0',
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 : '4u6e7pzq1rsw',
		'sdk'		 : 'android4.14.0',
		'secret_key' : '12137072101437594316341921108761641540',
		'secret_id'	 : '1',
		},
	'branch':{
				'key':'key_live_pnxPRJqVXFL5kl7BGqoR4onpCFfdBxw6',
				'sdk': 'android2.19.5',
			},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:24,
				21:21,
				22:20,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	# time.sleep(random.randint(15,30))######PLEASE ADJUST INSTALL SLEEP AS PER APP SIZE IN MB.
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=20)
	def_sec(app_data,device_data)
	set_sessionLength(app_data,forced=True,length=0)

	set_pushtoken(app_data)
	
	################generating_realtime_differences##################
	click_time2=datetime.datetime.utcfromtimestamp((app_data.get('times').get('click_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	global clk
 	clk = int(app_data.get('times').get('click_time'))
	# time.sleep(random.randint(22,34))
	global install_begin_time,install_
	install_begin_time=datetime.datetime.utcfromtimestamp((app_data.get('times').get('download_end_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	install_ = int(app_data.get('times').get('download_end_time'))
	# time.sleep(random.randint(29,40))
	# set_installedAT(app_data,device_data)
	global install_date
	install_date = int(app_data.get('times').get('install_complete_time')*1000)
	time.sleep(random.randint(72,85))
	created_at_attribution=get_date(app_data,device_data)
	time.sleep(random.randint(0,2))
	created_at_sdk2=get_date(app_data,device_data)
	created_at_session=get_date(app_data,device_data)
	time.sleep(random.randint(2,4))
	sent_at_sdk2=get_date(app_data,device_data)
	sent_at_session=get_date(app_data,device_data)
	time.sleep(random.randint(3,5))
	sent_at_attribution=get_date(app_data,device_data)
	
	if not app_data.get('flag'):
		app_data['flag']=False

	###########		CALLS		############

	return_userinfo(app_data)
	if not app_data.get('email_hash'):
		app_data['email_hash'] = util.md5(app_data.get('user_info').get('email'))

	if not app_data.get('customer_id'):
		app_data['customer_id'] = '7'+util.get_random_string('decimal',7)

	global product_id,price_list,banner_list,discover,search_list
	product_id =['834438','674752','507077','16134461','16071171','16023760','16134472','16022373','315166','16191874','16191919','16191915','16191918','16191940']
	price_list = ['2.020000','24.950001','34.950001','34.950001','10.950000','89.949997','43.950001','14.950000','9.950000','6.950000','23.950001','29.950001','9.950000','9.950000']
	banner_list = ['wehkamp://campaigncarousel','wehkamp://pdp/16175274']
	discover = ['wehkamp://wanderwall','wehkamp://wanderwallonboarding']
	search_list = ['jeans','shirt','trouser','shoes','gown','sweat-shirt','t-shirt','goggles','bermuda','coat','jacket','shocks']


	print 'branch Install'
	request=branch_install(campaign_data, app_data, device_data)
	util.execute_request(**request)

	print 'branch cdn'
	request=branch_cdn(campaign_data, app_data, device_data)
	util.execute_request(**request)
		
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,created_at=created_at_sdk2,sent_at=sent_at_sdk2,source='install_referrer')
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session)
	util.execute_request(**request)
	time.sleep(random.randint(2,60))

	send_home_view(campaign_data, app_data, device_data,t1=0,t2=0)

	print '\n'+'Adjust : adjust_sdkinfo____________________________________'
	request=adjust_sdkinfo(campaign_data,app_data,device_data,token=True,t1=0,t2=2)
	util.execute_request(**request)

		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,created_at=created_at_attribution,sent_at=sent_at_attribution)
	util.execute_request(**request)
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,created_at=created_at_attribution,sent_at=sent_at_attribution,call=2)
	util.execute_request(**request)

	for j in range(0,random.randint(1,3)):
		shuffle=[0,1,2,3,4,5]
		random.shuffle(shuffle)
		for i in range(0,len(shuffle)):

			if i==shuffle[0] and random.randint(1,100)<=70:
				time.sleep(random.randint(15,17))

				click_banner(campaign_data, app_data, device_data,t1=0,t2=0)

			if i==shuffle[1] and random.randint(1,100)<=90:
				time.sleep(random.randint(1,60))

				product_detail(campaign_data, app_data, device_data,t1=0,t2=0)
				
				if random.randint(1,100)<=70:#65
					time.sleep(random.randint(1,7))

					add_item_to_wishlist(campaign_data, app_data, device_data)

				if random.randint(1,100)<=65:#60
					time.sleep(random.randint(1,7))

					visit_to_wishlist(campaign_data, app_data, device_data)

					if random.randint(1,100)<=50:

						send_home_view(campaign_data, app_data, device_data,t1=0,t2=0)

				if random.randint(1,100)<=70:
					time.sleep(random.randint(1,100))

					add_item_to_cart(campaign_data, app_data, device_data)

				if random.randint(1,100)<=60:
					time.sleep(random.randint(1,100))

					visit_to_cart(campaign_data, app_data, device_data)

					if random.randint(1,100)<=50:
						
						send_home_view(campaign_data, app_data, device_data,t1=0,t2=0)

			if i==shuffle[2] and random.randint(1,100)<=85:
				time.sleep(random.randint(7,50))

				account_page(campaign_data, app_data, device_data,t1=0,t2=2)

				if random.randint(1,100)<=50:
						
					send_home_view(campaign_data, app_data, device_data,t1=0,t2=0)

				if random.randint(1,100)<=80:
					time.sleep(random.randint(10,60))

					my_order(campaign_data, app_data, device_data,t1=0,t2=2)
					app_data['flag']=True


			if i==shuffle[3] and random.randint(1,100)<=30:
				time.sleep(random.randint(2,40))

				to_discover(campaign_data, app_data, device_data,t1=0,t2=0)

				to_discover(campaign_data, app_data, device_data,t1=0,t2=0)
				

			if i==shuffle[4] and random.randint(1,100)<=20:
				time.sleep(random.randint(30,100))

				relaunch_app(campaign_data, app_data, device_data,t1=0,t2=0)

				if random.randint(1,100)<=50:
						
					send_home_view(campaign_data, app_data, device_data,t1=0,t2=0)

			if i==shuffle[5] and random.randint(1,100)<=50:
				time.sleep(random.randint(10,50))

				search(campaign_data, app_data, device_data,t1=0,t2=0)
				
	set_appCloseTime(app_data)

	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	# def_appsflyerUID(app_data)
	# def_eventsRecords(app_data)
	# generate_realtime_differences(device_data,app_data)

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=20)

	def_sec(app_data,device_data)

	set_pushtoken(app_data)

	click_time2=datetime.datetime.utcfromtimestamp((app_data.get('times').get('click_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	global install_begin_time
	install_begin_time=datetime.datetime.utcfromtimestamp((app_data.get('times').get('download_end_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	# set_installedAT(app_data,device_data)

	created_at_attribution=get_date(app_data,device_data)
	time.sleep(random.randint(0,2))
	created_at_sdk2=get_date(app_data,device_data)
	created_at_session=get_date(app_data,device_data)
	time.sleep(random.randint(2,4))
	sent_at_sdk2=get_date(app_data,device_data)
	sent_at_session=get_date(app_data,device_data)
	time.sleep(random.randint(3,5))
	sent_at_attribution=get_date(app_data,device_data)

	if not app_data.get('flag'):
		app_data['flag']=False

	if not app_data.get('user_info'):
		return_userinfo(app_data)
	if not app_data.get('email_hash'):
		app_data['email_hash'] = util.md5(app_data.get('user_info').get('email'))

	if not app_data.get('customer_id'):
		app_data['customer_id'] = '7'+util.get_random_string('decimal',7)

	global product_id,price_list,banner_list,discover,search_list
	product_id =['834438','674752','507077','16134461','16071171','16023760','16134472','16022373','315166','16191874','16191919','16191915','16191918','16191940']
	price_list = ['2.020000','24.950001','34.950001','34.950001','10.950000','89.949997','43.950001','14.950000','9.950000','6.950000','23.950001','29.950001','9.950000','9.950000']
	banner_list = ['wehkamp://campaigncarousel','wehkamp://pdp/16175274']
	discover = ['wehkamp://wanderwall','wehkamp://wanderwallonboarding']
	search_list = ['jeans','shirt','trouser','shoes','gown','sweat-shirt','t-shirt','goggles','bermuda','coat','jacket','shocks']

	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session,isOpen=True)
	util.execute_request(**request)

	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,created_at=created_at_sdk2,sent_at=sent_at_sdk2,source='install_referrer')
	util.execute_request(**request)
	time.sleep(random.randint(4,60))

	send_home_view(campaign_data, app_data, device_data,t1=0,t2=0)
	time.sleep(random.randint(2,5))

	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,created_at=created_at_attribution,sent_at=sent_at_attribution)
	util.execute_request(**request)

	for j in range(0,random.randint(1,3)):
		shuffle=[0,1,2,3,4,5]
		random.shuffle(shuffle)
		for i in range(0,len(shuffle)):

			if i==shuffle[0] and random.randint(1,100)<=60:
				time.sleep(random.randint(8,10))

				click_banner(campaign_data, app_data, device_data,t1=0,t2=0)


			if i==shuffle[1] and random.randint(1,100)<=85:
				time.sleep(random.randint(2,33))

				product_detail(campaign_data, app_data, device_data,t1=0,t2=0)
				
				if random.randint(1,100)<=65:#65
					time.sleep(random.randint(1,10))

					add_item_to_wishlist(campaign_data, app_data, device_data)

				if random.randint(1,100)<=60:#60
					time.sleep(random.randint(1,10))

					visit_to_wishlist(campaign_data, app_data, device_data)

					if random.randint(1,100)<=50:
						
						send_home_view(campaign_data, app_data, device_data,t1=0,t2=0)

				if random.randint(1,100)<=70:
					time.sleep(random.randint(1,100))

					add_item_to_cart(campaign_data, app_data, device_data)
					

				if random.randint(1,100)<=55:
					time.sleep(random.randint(1,100))

					visit_to_cart(campaign_data, app_data, device_data)

					if random.randint(1,100)<=50:
						
						send_home_view(campaign_data, app_data, device_data,t1=0,t2=0)

			if i==shuffle[2] and random.randint(1,100)<=75:
				time.sleep(random.randint(2,6))

				account_page(campaign_data, app_data, device_data,t1=0,t2=2)

				if random.randint(1,100)<=50:
						
					send_home_view(campaign_data, app_data, device_data,t1=0,t2=0)

				if random.randint(1,100)<=80:
					time.sleep(random.randint(18,21))

					my_order(campaign_data, app_data, device_data,t1=0,t2=2)
					app_data['flag']=True


			if i==shuffle[3] and random.randint(1,100)<=30:
				time.sleep(random.randint(4,6))

				to_discover(campaign_data, app_data, device_data,t1=0,t2=0)

				to_discover(campaign_data, app_data, device_data,t1=0,t2=0)
				

			if i==shuffle[4] and random.randint(1,100)<=25:
				time.sleep(random.randint(3,21))

				relaunch_app(campaign_data, app_data, device_data,t1=0,t2=0)

				if random.randint(1,100)<=50:
						
					send_home_view(campaign_data, app_data, device_data,t1=0,t2=0)


			if i==shuffle[5] and random.randint(1,100)<=45:
				time.sleep(random.randint(15,60))

				search(campaign_data, app_data, device_data,t1=0,t2=0)

	if random.randint(1,100)<=25:
		print 'adjust Event......'
		partner_params=str(json.dumps({"criteo_deeplink":'wehkamp://kassa?url=https://kassa.wehkamp.nl/bedankt/bevestiging',"criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))

		request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params,event_token='8l5p4t',t1=0,t2=0)
		util.execute_request(**request)

	return {'status':'true'}





################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def send_home_view(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________send_home_view'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,event_token='wxjclg',t1=t1,t2=t2)
	util.execute_request(**request)

def click_banner(campaign_data, app_data, device_data,t1=0,t2=0):
	banner = random.choice(banner_list)
	if app_data['flag'] == False:
		partner_params = str(json.dumps({"criteo_deeplink":banner}))
	else:
		partner_params = str(json.dumps({"criteo_deeplink":banner,"criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))

	print 'Adjust : EVENT______________________________click_banner'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params,event_token='8l5p4t',t1=t1,t2=t2)
	util.execute_request(**request)

def add_item_to_wishlist(campaign_data, app_data, device_data):
	app_data['product_list']=[]
	for i in range(0,random.randint(1,3)):
		r = random.randint(0,len(product_id)-1)
		app_data['product_list'].append(product_id[r])
	if app_data['flag'] == False:
		partner_params = str(json.dumps({"criteo_p":urllib.quote(str(app_data.get('product_list')))}))
	else:
		partner_params = str(json.dumps({"criteo_p":urllib.quote(str(app_data.get('product_list'))),"criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))
	print 'Adjust : EVENT______________________________ProductImpression'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params,event_token='34wpf1',t1=0,t2=2)
	util.execute_request(**request)

def visit_to_wishlist(campaign_data, app_data, device_data):
	if app_data.get('product_list'):
		product=random.choice(app_data.get('product_list'))
	else:
		product=''

	if app_data['flag'] == False:
		partner_params1 = str(json.dumps({"criteo_deeplink":"wehkamp://wishlist"}))
		partner_params2 = str(json.dumps({"criteo_p":urllib.quote(product)}))
	else:
		partner_params1 = str(json.dumps({"criteo_deeplink":"wehkamp://wishlist","criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))
		partner_params2 = str(json.dumps({"criteo_p":urllib.quote(product),"criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))

	print 'Adjust : EVENT______________________________productImpressions'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params1,event_token='8l5p4t',t1=0,t2=0)
	util.execute_request(**request)

	print 'Adjust : EVENT______________________________ProductImpression'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params2,event_token='34wpf1',t1=0,t2=2)
	util.execute_request(**request)

def add_item_to_cart(campaign_data, app_data, device_data):
	app_data['item']=[]
	app_data['price']=[]
	list1=[]
	for i in range(0,random.randint(1,3)):
		r = random.randint(0,len(product_id)-1)
		app_data['item'].append(product_id[r])
		app_data['price'].append(price_list[r])

	
	for i in range(0,len(app_data['item'])-1):
		list1.append({'i':app_data['item'][i],'pr':app_data['price'][i],'q':random.randint(1,3)})


	if app_data.get('flag')==False:
		partner_params=str(json.dumps({"currency":"EUR","criteo_p":urllib.quote(str(list1))}))
	else:
		partner_params=str(json.dumps({"currency":"EUR","criteo_p":urllib.quote(str(list1)),"criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))
	
	print 'Adjust : EVENT______________________________BasketViews'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params,event_token='spk185',t1=0,t2=2)
	util.execute_request(**request)

def visit_to_cart(campaign_data, app_data, device_data):
	list1=[]
	if app_data.get('item'):
		for i in range(0,len(app_data['item'])-1):
			list1.append({'i':app_data['item'][i],'pr':app_data['price'][i],'q':random.randint(1,3)})
	else:
		list1=[]

	if app_data.get('flag')==False:
		partner_params1 = str(json.dumps({"criteo_deeplink":"wehkamp://cart"}))
		partner_params2=str(json.dumps({"currency":"EUR","criteo_p":urllib.quote(str(list1))}))
	else:
		partner_params1 = str(json.dumps({"criteo_deeplink":"wehkamp://cart","criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))
		partner_params2=str(json.dumps({"currency":"EUR","criteo_p":urllib.quote(str(list1)),"criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))


	print 'Adjust : EVENT______________________________BasketViews'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params2,event_token='spk185',t1=0,t2=0)
	util.execute_request(**request)

	print 'Adjust : EVENT______________________________BasketView'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params1,event_token='8l5p4t',t1=0,t2=2)
	util.execute_request(**request)



def account_page(campaign_data, app_data, device_data,t1=0,t2=0):

	if app_data['flag'] == False:
		partner_params = str(json.dumps({"criteo_deeplink":"wehkamp://selfservice"}))
	else:
		partner_params = str(json.dumps({"criteo_deeplink":"wehkamp://selfservice","criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))

	print 'Adjust : EVENT______________________________account_page'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params,event_token='8l5p4t',t1=t1,t2=t2)
	util.execute_request(**request)

def my_order(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________my_order'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"criteo_deeplink":"wehkamp://selfservice/2"+util.get_random_string('decimal',9),"criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')})),event_token='8l5p4t',t1=t1,t2=t2)
	util.execute_request(**request)


def to_discover(campaign_data, app_data, device_data,t1=0,t2=0):
	to_discover = random.choice(discover)
	if app_data['flag'] == False:
		partner_params = str(json.dumps({"criteo_deeplink":to_discover}))
	else:
		partner_params = str(json.dumps({"criteo_deeplink":to_discover,"criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))

	print 'Adjust : EVENT______________________________to_discover'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params,event_token='8l5p4t',t1=t1,t2=t2)
	util.execute_request(**request)

def product_detail(campaign_data, app_data, device_data,t1=0,t2=0):
	if app_data['flag'] == False:
		partner_params = str(json.dumps({"criteo_p":random.choice(product_id)}))
	else:
		partner_params = str(json.dumps({"criteo_p":random.choice(product_id),"criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))
	print 'Adjust : EVENT______________________________product_detail'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params,event_token='wpjs8f',t1=t1,t2=t2)
	util.execute_request(**request)

def relaunch_app(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________relaunch_app'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='lp0olb',t1=t1,t2=t2)
	util.execute_request(**request)

def search(campaign_data, app_data, device_data,t1=0,t2=0):
	if app_data['flag'] == False:
		partner_params = str(json.dumps({"criteo_deeplink":"wehkamp://sop?keyword="+random.choice(search_list)+"&category=null"}))
	else:
		partner_params = str(json.dumps({"criteo_deeplink":"wehkamp://sop?keyword="+random.choice(search_list)+"&category=null","criteo_email_hash":app_data.get('email_hash'),"customer_id":app_data.get('customer_id')}))
	print 'Adjust : EVENT______________________________search'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=partner_params,event_token='8l5p4t',t1=t1,t2=t2)
	util.execute_request(**request)


##################################
##################################
##
##		BRANCH INSTALL
##
##################################
##################################


def branch_install(campaign_data, app_data, device_data):

 	

	url = 'http://api.branch.io/v1/install'
	method = 'post'
	headers = {
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'Accept-Encoding':'gzip',
		'User-Agent': get_ua(device_data),
		}
	data_value = {
		'app_version': campaign_data.get('app_version_name'),
		'branch_key': campaign_data.get('branch').get('key'),
		'brand': device_data.get('brand'),
		'cd':{'mv':-1,'pn':campaign_data.get('package_name')},
		'clicked_referrer_ts':clk,
		#'country':device_data.get('locale').get('country'),
		'debug': False,
		'environment':'FULL_APP',
		#'external_intent_extra':'{"profile":0}',
		'facebook_app_link_checked':False,
		'first_install_time':install_date,
		'google_advertising_id': device_data.get('adid'),
		'hardware_id': device_data.get('android_id'),
		'install_begin_ts':install_,
		'instrumentation':{'v1/install-qwt':'0'},
		'is_hardware_id_real': True,
		'is_referrable': 1,
		'language':device_data.get('locale').get('language'),
		'lat_val': 0,
		'latest_install_time':install_date,
		'latest_update_time':install_date,
		'local_ip':device_data.get('private_ip'),
		'metadata':{},
		'model': device_data.get('model'),
		'os': 'Android',
		'os_version': int(device_data.get('sdk')),
		'previous_update_time':0,
		'retryNumber': 0,
		'screen_dpi': int(device_data.get('dpi')),
		'screen_height': int(device_data.get('resolution').split('x')[0]),
		'screen_width': int(device_data.get('resolution').split('x')[1]),
		'sdk': campaign_data.get('branch').get('sdk'),
		'ui_mode':'UI_MODE_TYPE_NORMAL',
		'update': 0,
		'wifi': True,
		}

	data = json.dumps(data_value)

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params': None, 'data': data}

def branch_cdn(campaign_data, app_data, device_data):


	url = 'https://cdn.branch.io/sdk/uriskiplist_v1.json'
	method = 'get'
	headers = {
		
		'Accept-Encoding':'gzip',
		'User-Agent': get_ua(device_data),
		}
	

	data = None

	params = None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params': params, 'data': data}


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time,created_at,sent_at,source='',callback_params=None,partner_params=None,isOpen=False):
	set_androidUUID(app_data)
	inc_(app_data,'subsession_count')
	reftag = ''
	temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
	if temp_b.get('adjust_reftag'):
		reftag = temp_b.get('adjust_reftag')[0]
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded",
		# "Authorization" : CHECK AUTH,
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	if app_data.get('sess_len'):
		time_spent=int(time.time())-app_data.get('sess_len')
	else:
		time_spent=0

	session_length=time_spent

	data = {
		"event_buffering_enabled" : "0",
		"updated_at" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		"needs_response_details" : "1",
		"time_spent" : time_spent,
		"os_name" : "android",
		"device_type" : device_data.get('device_type'),
		"device_manufacturer" : device_data.get('manufacturer'),
		"connectivity_type" : 1,
		"package_name" : campaign_data.get('package_name'),
		"click_time" : click_time,
		"os_version" : device_data.get('os_version'),
		"installed_at" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		"vm_isa" : 'arm64',#"arm",
		"environment" : "production",
		"source" : source,
		"android_uuid" : app_data.get('android_uuid'),
		"display_width" : device_data.get('resolution').split('x')[1],
		"screen_density" : get_screen_density(device_data),
		"subsession_count" : app_data.get('subsession_count'),
		"session_length" : session_length,
		"gps_adid" : device_data.get('adid'),
		"screen_size" : get_screen_size(device_data),
		"tracking_enabled" : "1",
		"screen_format" : get_screen_format(device_data),
		"display_height" : device_data.get('resolution').split('x')[0],
		"hardware_name" : device_data.get('hardware'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"sent_at" : sent_at,
		"attribution_deeplink" : "1",
		"app_version" : campaign_data.get('app_version_name'),
		"language" : device_data.get('locale').get('language'),
		"referrer" : "utm_source=(not%20set)&utm_medium=(not%20set)",
		"created_at" : created_at,
		"session_count" : 1,
		"device_name" : device_data.get('model'),
		"install_begin_time" : install_begin_time,
		"cpu_type" : device_data.get('cpu_abi'),
		"country" : device_data.get('locale').get('country'),
		"network_type" : "13",
		"api_level" : device_data.get('sdk'),
		"os_build" : device_data.get('build'),
		'mcc' : device_data.get('mcc'),
		'mnc' : device_data.get('mnc')

		}

	if app_data.get('push_token'):
		data['push_token']=app_data.get('push_token')

	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		# del data['session_length']
		# del data['subsession_count']
		# del data['time_spent']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		# del data['last_interval']

	if isOpen:
		if data.get('click_time'):
			del data['click_time']
		if data.get('install_begin_time'):
			del data['install_begin_time']

	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'click')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_sdkinfo(campaign_data,app_data,device_data,token=False,t1=0,t2=0):
	inc_(app_data,'subsession_count')
	
	url 	= 'http://app.adjust.com/sdk_info'
	method 	= 'post'
	headers = {
		'Client-SDK'		: campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/x-www-form-urlencoded',
		'User-Agent'		: get_ua(device_data),
		 }
	created_at = get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at = get_date(app_data,device_data)
	params=None
	set_pushtoken(app_data)
	data = {
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		
		'created_at'			:created_at,
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'needs_response_details':'1',
		#'queue_size'            :1,
		'sent_at'				:sent_at,
		'source'                :'push',
		'tracking_enabled'		:'1',
		}
	if token:
		data['push_token']=app_data.get('push_token')

	
	headers['Authorization']=get_auth(device_data,app_data,created_at=data['created_at'],gps_adid=data['gps_adid'],activity_type='info')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,created_at,sent_at,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	# set_installedAT(app_data,device_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded",
		# "Authorization" : CHECK AUTH,
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}

	data = {
		"package_name" : campaign_data.get('package_name'),
		"updated_at" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		"needs_response_details" : "1",
		"os_name" : "android",
		"device_type" : device_data.get('device_type'),
		"device_manufacturer" : device_data.get('manufacturer'),
		"tracking_enabled" : "1",
		"connectivity_type" : "1",
		"event_buffering_enabled" : "0",
		"installed_at" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		"vm_isa" : 'arm64',#"arm",
		"environment" : "production",
		"os_version" : device_data.get('os_version'),
		"android_uuid" : app_data.get('android_uuid'),
		"display_width" : device_data.get('resolution').split('x')[1],
		"app_version" : campaign_data.get('app_version_name'),
		"gps_adid" : device_data.get('adid'),
		"screen_size" : get_screen_size(device_data),
		"hardware_name" : device_data.get('hardware'),
		#"queue_size" : 1,
		"screen_format" : get_screen_format(device_data),
		"display_height" : device_data.get('resolution').split('x')[0],
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"sent_at" : sent_at,
		"attribution_deeplink" : "1",
		"screen_density" : get_screen_density(device_data),
		"language" : device_data.get('locale').get('language'),
		"country" : device_data.get('locale').get('country'),
		"created_at" : created_at,
		"session_count" : app_data.get('session_count'),
		"device_name" : device_data.get('model'),
		"cpu_type" : device_data.get('cpu_abi'),
		"network_type" : "13",
		"api_level" : device_data.get('sdk'),
		"os_build" : device_data.get('build'),
		'mcc' : device_data.get('mcc'),
		'mnc' : device_data.get('mnc')

		}
	if isOpen:
		def_(app_data,'subsession_count')
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= int(time.time())-app_data.get('sess_len')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= int(time.time())-app_data.get('sess_len')
		app_data['sess_len']=int(time.time())
		
	if app_data.get('push_token'):
		data['push_token']=app_data.get('push_token')


	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'session')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	# set_installedAT(app_data,device_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	def_sessionLength(app_data)
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded",
		# "Authorization" : CHECK AUTH,
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}


	data = {
		"package_name" : campaign_data.get('package_name'),
		"event_token" : event_token,
		"tracking_enabled" : "1",
		"needs_response_details" : "1",
		"time_spent" : time_spent,
		"os_name" : "android",
		"device_type" : device_data.get('device_type'),
		"device_manufacturer" : device_data.get('manufacturer'),
		"connectivity_type" : "1",
		"event_buffering_enabled" : "0",
		"vm_isa" : 'arm64',#"arm",
		"environment" : "production",
		"os_version" : device_data.get('os_version'),
		"session_length" : session_length,
		"display_width" : device_data.get('resolution').split('x')[1],
		"screen_density" : get_screen_density(device_data),
		"subsession_count" : app_data.get('subsession_count'),
		"android_uuid" : app_data.get('android_uuid'),
		"partner_params" : partner_params,
		"gps_adid" : device_data.get('adid'),
		"screen_size" : get_screen_size(device_data),
		"hardware_name" : device_data.get('hardware'),
		#"queue_size" : 1,
		"screen_format" : get_screen_format(device_data),
		"display_height" : device_data.get('resolution').split('x')[0],
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"sent_at" : sent_at,
		"attribution_deeplink" : "1",
		"app_version" : campaign_data.get('app_version_name'),
		"language" : device_data.get('locale').get('language'),
		"event_count" : app_data.get('event_count'),
		"country" : device_data.get('locale').get('country'),
		"created_at" : created_at,
		"session_count" : app_data.get('session_count'),
		"device_name" : device_data.get('model'),
		"cpu_type" : device_data.get('cpu_abi'),
		"network_type" : "13",
		"api_level" : device_data.get('sdk'),
		"os_build" : device_data.get('build'),
		'mcc' : device_data.get('mcc'),
		'mnc' : device_data.get('mnc')

		}

	if app_data.get('event_count')>1:
		if data.get('queue_size'):
			del data['queue_size']

	if app_data.get('push_token'):
		data['push_token']=app_data.get('push_token')
	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,created_at,sent_at,call=1):
	inc_(app_data,'subsession_count')
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		# "Authorization" : CHECK AUTH,

		}
	params = {
		"attribution_deeplink" : "1",
		"gps_adid" : device_data.get('adid'),
		"sent_at" : sent_at,
		"event_buffering_enabled" : "0",
		"initiated_by" : "backend",
		"created_at" : created_at,
		"tracking_enabled" : "1",
		"needs_response_details" : "1",
		"environment" : "production",
		"app_token" : campaign_data.get('adjust').get('app_token'),

		}
	if call==2:
		params['initiated_by']='sdk'

	# if app_data.get('push_token'):
	# 	params['push_token']=app_data.get('push_token')

	data = None

	
	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('gps_adid'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}




###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())


def set_pushtoken(app_data):
	if not app_data.get('push_token'):
		app_data['push_token'] = util.get_random_string('google_token',11)+':APA91b'+util.get_random_string('google_token',134)


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	

def set_installedAT(app_data,device_data):
	if not app_data.get('installed_at'):
		app_data['installed_at'] =  get_date(app_data,device_data)

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']='f0L4WFS5AEk:'+util.get_random_string('google_token',140)

def get_auth(device_data,app_data,created_at,gps_adid,activity_type):
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activity_type)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'


def get_install_reciept(app_data):
	install_receipt='MIISiAYJKoZIhvcNAQ'+util.get_random_string('google_token',6314)
	if not app_data.get('install_receipt'):
		app_data['install_receipt']=install_receipt

def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	if not gender_n:
		app_data['gender'] = random.choice(['male', 'female'])
		gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	if not user_info:
		user_data = util.generate_name(gender=gender_n)
		user_info = {}
		user_info['username'] = user_data.get('username').lower()
		user_info['email'] = user_data.get('username').lower() + '@gmail.com'
		# user_info['f_name'] = user_data.get('firstname')
		# user_info['l_name'] = user_data.get('lastname')
		# user_info['sex'] = gender_n
		# user_info['gender'] = str(1) if gender_n == 'female' else str(2)
		# user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
		# user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
		# user_info['password'] = util.get_random_string(type='all',size=16)
		app_data['user_info'] = user_info

#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	#st 	 = str(int(time.time()*1000))
	st=device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"