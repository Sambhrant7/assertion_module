import uuid,time,random,json,urllib,datetime
from sdk import util, installtimenew,getsleep
from Crypto.Cipher import AES
import clicker,Config


campaign_data = {
            'package_name':'com.opodo.reisen',
            'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
            'app_name' : 'Opodo',
            'app_version_code': '412301',#'412201',#'412200',#'412100',#'411901',#411701, '411400',#"411301",#'411200',#'47200',#'47000',#'46800',#'46601',#'46100', 46300
            'app_version_name': '4.123.1',#'4.122.1',#'4.122.0',#'4.121.0',#'4.119.1',#4.117.1, '4.114.0',#"4.113.1",#'4.112.0',#'4.72.0',#'4.70.0',#'4.68.0',#'4.66.1',#'4.61.0', 4.63.0
            'supported_countries': 'WW',
            'supported_os':'4.4',
            'tracker' :'mat',
            'no_referrer':True,
            'device_targeting':True,
            'ctr':6,
            'app_size': 45.0,#35.0,
            'mat' :{
                        'version' : '6.0.3',#'4.10.1',
                        'sdk' : 'android',
                        'advertiser_id' : '188712',
                        'key' : '74e6a7e154ac762ed0d235be82072e18',
                        'iv' : 'heF9BATUfWuISyO8',
                        'gmp_version':'19056',#'12874',#'12872',#'12685'
                    },
            'measurement_api':{
                    'publisher_id':'169736' ,
                    'site_id_android':'103308',
                    'site_id_ios':'103300',
                    'site_id_web':'119563'
                              },

            'retention':{
                            1:50,
                            2:48,
                            3:46,
                            4:44,
                            5:40,
                            6:38,
                            7:35,
                            8:32,
                            9:31,
                            10:30,
                            11:29,
                            12:28,
                            13:27,
                            14:26,
                            15:25,
                            16:24,
                            17:23,
                            18:22,
                            19:21,
                            20:20,
                            21:19,
                            22:18,
                            23:17,
                            24:16,
                            25:15,
                            26:14,
                            27:13,
                            28:12,
                            29:11,
                            30:10,
                            31:9,
                            32:8,
                            33:7,
                            34:6,
                            35:5,
                        }
        }

    
  

    
def install(app_data, device_data):
    print 'Please wait installing...'
    installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")

    make_sec(app_data,device_data)
    # if not app_data.get('insdate'):
    #     app_data['insdate'] = str(int(time.time())-100)

    app_data['state']=0


    print"CURRENT LOCATION"
    mat1 = current_location(app_data)
    latlong = util.execute_request(**mat1)
    try:
        app_data['latlong'] = json.loads(latlong.get('data'))
        print app_data['latlong']
    except:
        print 'exception'
        app_data['latlong'] ={"latitude":'28.57',"longitude":'77.319999999999993'}
    
    print"MAT 1----------------------------------------------------------------------------------"
    mat1 = mat_serve(campaign_data, app_data, device_data,'session')
    app_data['api_hit_time'] = time.time()
    mat1_output=util.execute_request(**mat1)
    try:
        mat1_decode = json.loads(mat1_output.get('data'))
        app_data['open_log_id'] = mat1_decode.get('log_id')
        app_data['last_open_log_id'] = app_data.get('open_log_id')
    except:
        print "Exception"
        app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
        app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')    


    # print "measurement api_______________________________________"
    # api=measurement_api(campaign_data, app_data, device_data)
    # util.execute_request(**api)

    # time.sleep(random.randint(1,4))
    # print "opodo call_______________________________________"
    # opo=opodo(campaign_data, app_data, device_data)
    # util.execute_request(**opo)

    print"MAT 2--------------------------------------------------------------------------------"
    mat1 = mat_serve(campaign_data, app_data, device_data,'session',log=True)
    mat1_output=util.execute_request(**mat1)
    try:
        mat1_decode = json.loads(mat1_output.get('data'))
        app_data['open_log_id'] = mat1_decode.get('log_id')
        app_data['last_open_log_id'] = app_data.get('open_log_id')
    except:
        print "Exception"
        app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
        app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')    


    print "measurement config_______________________________________"
    config=measurement_config(campaign_data, app_data, device_data)
    util.execute_request(**config)

    print "measurement config_______________________________________"
    config=measurement_config(campaign_data, app_data, device_data)
    util.execute_request(**config)

    print"MAT 3---------------------------------------------------------------------------------"
    mat1 = mat_serve(campaign_data, app_data, device_data,'session',log=True,delay=True)
    mat1_output=util.execute_request(**mat1)
    try:
        mat1_decode = json.loads(mat1_output.get('data'))
        app_data['open_log_id'] = mat1_decode.get('log_id')
        app_data['last_open_log_id'] = app_data.get('open_log_id')
    except:
        print "Exception"
        app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
        app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')   

    print "\nself_call_sl_odige\n"
    request=sl_odige_nearestLocations( campaign_data, device_data, app_data )
    response = util.execute_request(**request)     
    global iataCode_list
    iataCode_list = list()
    try:
        result = json.loads(response.get('data')).get('cities')
        for item in result:
            a = {}
            a['cityName'] = item.get('cityName')
            a['iataCode'] = item.get('iataCode')
            iataCode_list.append(a)

        if not iataCode_list:
            iataCode_list = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]      

    except:
        iataCode_list = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]

    print "\nself_call_sl_odige\n"
    request=sl_odige_locations( campaign_data, device_data, app_data )
    response = util.execute_request(**request)
    global iataCode_list_2
    iataCode_list_2 = list()
    try:
        result = json.loads(response.get('data')).get('locations')
        for item in result:
            a = {}
            a['cityName'] = item.get('cityName')
            a['iataCode'] = item.get('iataCode')
            iataCode_list_2.append(a)

        if not iataCode_list_2:
            iataCode_list_2 = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]      

    except:
        iataCode_list_2 = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]         

    global message,price_list
    message = [1]
    counter=0
    price_list = list()
    while counter<5:
        print "\nself_call_sl_odige\n"
        request=sl_odige_search( campaign_data, device_data, app_data )
        response = util.execute_request(**request)
        counter += 1
        try:
            result = json.loads(response.get('data'))
            message = result.get('messages')
            itineraryResults = result.get('itineraryResultsPage').get('itineraryResults')
            for item in itineraryResults:
                price_list.append(item.get('price'))
            if message==[]:
                break        
        except:
            price_list = [{'sortPrice': 2846.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.66}}, 'markup': 23.47, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.12, 'tax': 275.74, 'taxDetails': []}}}, {'sortPrice': 2916.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.32}}, 'markup': 23.49, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.13, 'tax': 346.06, 'taxDetails': []}}}, {'sortPrice': 2924.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.19}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 397.8, 'taxDetails': []}}}, {'sortPrice': 2934.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.78}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 407.21, 'taxDetails': []}}}, {'sortPrice': 2942.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.48}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2474.0, 'tax': 401.51, 'taxDetails': []}}}, {'sortPrice': 3153.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.01}}, 'markup': 5.63, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2734.13, 'tax': 358.22, 'taxDetails': []}}}, {'sortPrice': 4306.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.74}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3370.0, 'tax': 880.25, 'taxDetails': []}}}, {'sortPrice': 4911.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.23}}, 'markup': 133.55, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3883.29, 'tax': 838.92, 'taxDetails': []}}}, {'sortPrice': 5930.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.43}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 4832.0, 'tax': 1031.56, 'taxDetails': []}}}, {'sortPrice': 6171.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.97}}, 'markup': 38.69, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5042.1, 'tax': 1035.23, 'taxDetails': []}}}, {'sortPrice': 6377.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.2}}, 'markup': 23.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5239.65, 'tax': 1058.94, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6550.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.54}}, 'markup': 10.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5405.42, 'tax': 1078.83, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}]   

    if not price_list:
        price_list = [{'sortPrice': 2846.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.66}}, 'markup': 23.47, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.12, 'tax': 275.74, 'taxDetails': []}}}, {'sortPrice': 2916.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.32}}, 'markup': 23.49, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.13, 'tax': 346.06, 'taxDetails': []}}}, {'sortPrice': 2924.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.19}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 397.8, 'taxDetails': []}}}, {'sortPrice': 2934.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.78}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 407.21, 'taxDetails': []}}}, {'sortPrice': 2942.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.48}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2474.0, 'tax': 401.51, 'taxDetails': []}}}, {'sortPrice': 3153.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.01}}, 'markup': 5.63, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2734.13, 'tax': 358.22, 'taxDetails': []}}}, {'sortPrice': 4306.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.74}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3370.0, 'tax': 880.25, 'taxDetails': []}}}, {'sortPrice': 4911.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.23}}, 'markup': 133.55, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3883.29, 'tax': 838.92, 'taxDetails': []}}}, {'sortPrice': 5930.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.43}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 4832.0, 'tax': 1031.56, 'taxDetails': []}}}, {'sortPrice': 6171.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.97}}, 'markup': 38.69, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5042.1, 'tax': 1035.23, 'taxDetails': []}}}, {'sortPrice': 6377.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.2}}, 'markup': 23.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5239.65, 'tax': 1058.94, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6550.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.54}}, 'markup': 10.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5405.42, 'tax': 1078.83, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}]        

    print "\nself_call_sl_odige\n"
    request=sl_odige_priceBreakdown( campaign_data, device_data, app_data )
    response = util.execute_request(**request)
    global price
    try:
        result = json.loads(response.get('data')).get('pricingBreakdownSteps')[0].get('pricingBreakdownItems')[0].get('priceItemAmount')    
        price = result
    except:
        price = random.choice([1441.09,6717.31,2576.2])    


    if random.randint(1,100)<=30:  #30

        print"MAT 1______________________Session"
        mat1 = mat_serve(campaign_data, app_data, device_data,'session',True,include=True,delay=True)
        mat1_output = util.execute_request(**mat1)
   
        try:
            mat1_decode = json.loads(mat1_output.get('data'))
            app_data['open_log_id'] = mat1_decode.get('log_id')
            app_data['last_open_log_id'] = app_data.get('open_log_id')
        except:
            print "Exception"
            app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
            app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')


        print"MAT 4------------------------------------------------------------------------------"
        mat5 = mat_serve(campaign_data, app_data, device_data,'conversion:search',True)
        util.execute_request(**mat5)
        

        if random.randint(1,100)<=80:  #25
            print"MAT 5------------------------------------------------------------------------------"
            mat5 = mat_serve(campaign_data, app_data, device_data,'conversion:ViewProduct',True)
            util.execute_request(**mat5)

            print"MAT 1______________________Session"
            mat1 = mat_serve(campaign_data, app_data, device_data,'session',True,include=True,delay=True)
            mat1_output = util.execute_request(**mat1)
   
            try:
                mat1_decode = json.loads(mat1_output.get('data'))
                app_data['open_log_id'] = mat1_decode.get('log_id')
                app_data['last_open_log_id'] = app_data.get('open_log_id')
            except:
                print "Exception"
                app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
                app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')

            
            app_data['status']=1

    time.sleep(random.randint(5,10))
    if random.randint(1,100)<=50:
        print"MAT 1______________________Session"
        mat1 = mat_serve(campaign_data, app_data, device_data,'session',True)
        mat1_output = util.execute_request(**mat1)
   
        try:
            mat1_decode = json.loads(mat1_output.get('data'))
            app_data['open_log_id'] = mat1_decode.get('log_id')
            app_data['last_open_log_id'] = app_data.get('open_log_id')
        except:
            print "Exception"
            app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
            app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')


    if random.randint(1,100)<=10 and  app_data.get('status')==1 :  #10
        print"MAT 5------------------------------------------------------------------------------"
        mat5 = mat_serve(campaign_data, app_data, device_data,'conversion:ViewBasket',True)
        util.execute_request(**mat5)


    if random.randint(1,100)<=50:
        print"MAT 1______________________Session"
        mat1 = mat_serve(campaign_data, app_data, device_data,'session',True)

        mat1_output = util.execute_request(**mat1)
        try:
            mat1_decode = json.loads(mat1_output.get('data'))
            app_data['open_log_id'] = mat1_decode.get('log_id')
            app_data['last_open_log_id'] = app_data.get('open_log_id')
        except:
            print "Exception"
            app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
            app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')


    return {'status':True}  
 
    
def open(app_data,device_data,day):

    if not app_data.get('times'):
        installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")

    make_sec(app_data,device_data)
    # if not app_data.get('insdate'):
    #     app_data['insdate'] = str(int(time.time())-100)

    if not app_data.get('latlong'):
        print"CURRENT LOCATION"
        mat1 = current_location(app_data)
        latlong = util.execute_request(**mat1)
        try:
            app_data['latlong'] = json.loads(latlong.get('data'))
            print app_data['latlong']
        except:
            print 'exception'
            app_data['latlong'] ={"latitude":'28.57',"longitude":'77.319999999999993'}


    app_data['state']=0

    
    print"MAT 1----------------------------------------------------------------------------------"
    mat1 = mat_serve(campaign_data, app_data, device_data,'session',True, isopen=True)
    mat1_output=util.execute_request(**mat1)
    try:
        mat1_decode = json.loads(mat1_output.get('data'))
        app_data['open_log_id'] = mat1_decode.get('log_id')
        app_data['last_open_log_id'] = app_data.get('open_log_id')
    except:
        print "Exception"
        app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
        app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')    


   

    print"MAT 2--------------------------------------------------------------------------------"
    mat1 = mat_serve(campaign_data, app_data, device_data,'session',True, isopen=True)
    mat1_output=util.execute_request(**mat1)
    try:
        mat1_decode = json.loads(mat1_output.get('data'))
        app_data['open_log_id'] = mat1_decode.get('log_id')
        app_data['last_open_log_id'] = app_data.get('open_log_id')
    except:
        print "Exception"
        app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
        app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')    


    
    print"MAT 3---------------------------------------------------------------------------------"
    mat1 = mat_serve(campaign_data, app_data, device_data,'session',True, isopen=True)
    mat1_output=util.execute_request(**mat1)
    try:
        mat1_decode = json.loads(mat1_output.get('data'))
        app_data['open_log_id'] = mat1_decode.get('log_id')
        app_data['last_open_log_id'] = app_data.get('open_log_id')
    except:
        print "Exception"
        app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
        app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')    


    print "\nself_call_sl_odige\n"
    request=sl_odige_nearestLocations( campaign_data, device_data, app_data )
    response = util.execute_request(**request)     
    global iataCode_list
    iataCode_list = list()
    try:
        result = json.loads(response.get('data')).get('cities')
        for item in result:
            a = {}
            a['cityName'] = item.get('cityName')
            a['iataCode'] = item.get('iataCode')
            iataCode_list.append(a)

        if not iataCode_list:
            iataCode_list = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]      

    except:
        iataCode_list = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]

    print "\nself_call_sl_odige\n"
    request=sl_odige_locations( campaign_data, device_data, app_data )
    response = util.execute_request(**request)
    global iataCode_list_2
    iataCode_list_2 = list()
    try:
        result = json.loads(response.get('data')).get('locations')
        for item in result:
            a = {}
            a['cityName'] = item.get('cityName')
            a['iataCode'] = item.get('iataCode')
            iataCode_list_2.append(a)

        if not iataCode_list_2:
            iataCode_list_2 = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]      

    except:
        iataCode_list_2 = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]         

    global message,price_list
    message = [1]
    counter=0
    price_list = list()
    while counter<5:
        print "\nself_call_sl_odige\n"
        request=sl_odige_search( campaign_data, device_data, app_data )
        response = util.execute_request(**request)
        counter += 1
        try:
            result = json.loads(response.get('data'))
            message = result.get('messages')
            itineraryResults = result.get('itineraryResultsPage').get('itineraryResults')
            for item in itineraryResults:
                price_list.append(item.get('price'))
            if message==[]:
                break        
        except:
            price_list = [{'sortPrice': 2846.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.66}}, 'markup': 23.47, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.12, 'tax': 275.74, 'taxDetails': []}}}, {'sortPrice': 2916.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.32}}, 'markup': 23.49, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.13, 'tax': 346.06, 'taxDetails': []}}}, {'sortPrice': 2924.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.19}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 397.8, 'taxDetails': []}}}, {'sortPrice': 2934.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.78}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 407.21, 'taxDetails': []}}}, {'sortPrice': 2942.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.48}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2474.0, 'tax': 401.51, 'taxDetails': []}}}, {'sortPrice': 3153.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.01}}, 'markup': 5.63, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2734.13, 'tax': 358.22, 'taxDetails': []}}}, {'sortPrice': 4306.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.74}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3370.0, 'tax': 880.25, 'taxDetails': []}}}, {'sortPrice': 4911.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.23}}, 'markup': 133.55, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3883.29, 'tax': 838.92, 'taxDetails': []}}}, {'sortPrice': 5930.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.43}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 4832.0, 'tax': 1031.56, 'taxDetails': []}}}, {'sortPrice': 6171.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.97}}, 'markup': 38.69, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5042.1, 'tax': 1035.23, 'taxDetails': []}}}, {'sortPrice': 6377.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.2}}, 'markup': 23.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5239.65, 'tax': 1058.94, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6550.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.54}}, 'markup': 10.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5405.42, 'tax': 1078.83, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}]   

    if not price_list:
        price_list = [{'sortPrice': 2846.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.66}}, 'markup': 23.47, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.12, 'tax': 275.74, 'taxDetails': []}}}, {'sortPrice': 2916.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.32}}, 'markup': 23.49, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.13, 'tax': 346.06, 'taxDetails': []}}}, {'sortPrice': 2924.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.19}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 397.8, 'taxDetails': []}}}, {'sortPrice': 2934.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.78}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 407.21, 'taxDetails': []}}}, {'sortPrice': 2942.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.48}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2474.0, 'tax': 401.51, 'taxDetails': []}}}, {'sortPrice': 3153.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.01}}, 'markup': 5.63, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2734.13, 'tax': 358.22, 'taxDetails': []}}}, {'sortPrice': 4306.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.74}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3370.0, 'tax': 880.25, 'taxDetails': []}}}, {'sortPrice': 4911.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.23}}, 'markup': 133.55, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3883.29, 'tax': 838.92, 'taxDetails': []}}}, {'sortPrice': 5930.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.43}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 4832.0, 'tax': 1031.56, 'taxDetails': []}}}, {'sortPrice': 6171.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.97}}, 'markup': 38.69, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5042.1, 'tax': 1035.23, 'taxDetails': []}}}, {'sortPrice': 6377.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.2}}, 'markup': 23.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5239.65, 'tax': 1058.94, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6550.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.54}}, 'markup': 10.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5405.42, 'tax': 1078.83, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}]        

    print "\nself_call_sl_odige\n"
    request=sl_odige_priceBreakdown( campaign_data, device_data, app_data )
    response = util.execute_request(**request)
    global price
    try:
        result = json.loads(response.get('data')).get('pricingBreakdownSteps')[0].get('pricingBreakdownItems')[0].get('priceItemAmount')    
        price = result
    except:
        price = random.choice([1441.09,6717.31,2576.2]) 

    if random.randint(1,100)<=20:
        print"MAT 1______________________Session"
        mat1 = mat_serve(campaign_data, app_data, device_data,'session',True,include=True,delay=True)
        mat1_output = util.execute_request(**mat1)
   
        try:
            mat1_decode = json.loads(mat1_output.get('data'))
            app_data['open_log_id'] = mat1_decode.get('log_id')
            app_data['last_open_log_id'] = app_data.get('open_log_id')
        except:
            print "Exception"
            app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
            app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')

        print"MAT 5------------------------------------------------------------------------------"
        mat5 = mat_serve(campaign_data, app_data, device_data,'conversion:search',True)
        util.execute_request(**mat5)
        
        
        if random.randint(1,100)<=90 :
            print"MAT 5------------------------------------------------------------------------------"
            mat5 = mat_serve(campaign_data, app_data, device_data,'conversion:ViewProduct',True)
            util.execute_request(**mat5)
            app_data['status']=1

        print"MAT 1______________________Session"
        mat1 = mat_serve(campaign_data, app_data, device_data,'session',True,include=True,delay=True)
        mat1_output = util.execute_request(**mat1)
   
        try:
            mat1_decode = json.loads(mat1_output.get('data'))
            app_data['open_log_id'] = mat1_decode.get('log_id')
            app_data['last_open_log_id'] = app_data.get('open_log_id')
        except:
            print "Exception"
            app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
            app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')


    time.sleep(random.randint(5,10))
    if random.randint(1,100)<=50:
        print"MAT 1______________________Session"
        mat1 = mat_serve(campaign_data, app_data, device_data,'session',True, isopen=True)
        mat1_output = util.execute_request(**mat1)

        try:
            mat1_decode = json.loads(mat1_output.get('data'))
            app_data['open_log_id'] = mat1_decode.get('log_id')
            app_data['last_open_log_id'] = app_data.get('open_log_id')
        except:
            print "Exception"
            app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
            app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')


    if random.randint(1,100)<=5 and  app_data.get('status')==1 :
        print"MAT 5------------------------------------------------------------------------------"
        mat5 = mat_serve(campaign_data, app_data, device_data,'conversion:ViewBasket',True)
        
        util.execute_request(**mat5)

    if random.randint(1,100)<=50:
        print"MAT 1______________________Session"
        mat1 = mat_serve(campaign_data, app_data, device_data,'session',True, isopen=True)
        mat1_output = util.execute_request(**mat1)

    return {'status':True}   

class AESCipher:
 def __init__(self, key):
        self.key = key

 def pad(self, raw):
        l = len(raw) % 16
        l = 16 - l
        for x in range(l):
            raw += ' '
        return raw

 def encrypt(self, iv, raw):
        raw = self.pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')

 def decrypt(self, enc , iv):
        enc = enc.decode('hex')#base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return cipher.decrypt(enc)   

#####################################################################
#
#                      Self Calls
#
#####################################################################


def sl_odige_locations( campaign_data, device_data, app_data ):
    url= "https://msl.odigeo.com/mobile-api/msl/locations"
    method= "get"
    headers= {       
        "Accept": "application/vnd.com.odigeo.msl.v4+json;charset=utf-8",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "Device-ID": "ANDROID;"+device_data.get('model')+";"+device_data.get('os_version')+";O;OPGB;c8ff196a-b7aa-4bb7-bdd8-089a525c8510;en;en;"+campaign_data.get('app_version_name')+";"+campaign_data.get('app_version_code')+";XL",
        "User-Agent": "okhttp/3.10.0"
        }

    params= {       
        "departureOrArrival": "DEPARTURE",
        "distanceUnit": "KM",
        "productType": "FLIGHT",
        "searchKey": util.country_list.get(random.choice(util.country_list.keys())).get('apolitical_name'),
        }

    data= None
    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

    
def sl_odige_nearestLocations( campaign_data, device_data, app_data ):
    url= "https://msl.odigeo.com/mobile-api/msl/nearestLocations"
    method= "get"
    headers= {       
        "Accept": "application/vnd.com.odigeo.msl.v3+json;charset=utf-8",
        "Accept-Encoding": "gzip,deflate,sdch",
        "If-None-Match": "",
        "User-Agent": 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
        }

    params= {       
        "latitude": app_data.get('latlong').get('latitude'),
        "locale": device_data.get('locale').get('language'),
        "longitude": app_data.get('latlong').get('longitude'),
        "productType": "FLIGHT",
        "radiusInKm": "100"
        }

    data= None
    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}    


def sl_odige_search( campaign_data, device_data, app_data ):
    url= "https://msl.odigeo.com/mobile-api/msl/search"
    method= "post"
    headers= {       
        "Accept": "application/vnd.com.odigeo.msl.v3+json;charset=utf-8",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json; charset=UTF-8",
        "Device-ID": "ANDROID;"+device_data.get('model')+";"+device_data.get('os_version')+";O;OPGB;c8ff196a-b7aa-4bb7-bdd8-089a525c8510;en;en;"+campaign_data.get('app_version_name')+";"+campaign_data.get('app_version_code')+";XL",
        "User-Agent": "okhttp/3.10.0"
        }

    params= None

    global choice_1,choice_2,adults,num1,num2
    num1 = int(time.time()+(random.randint(2,5)*24*60*60))
    num2 = int(num1+(random.randint(1,3)*24*60*60))
    choice_1 = random.choice(iataCode_list)
    choice_2 = random.choice(iataCode_list_2)
    adults = random.randint(1,2)

    data= {       
            "itinerarySearchRequest": {       
            "cabinClass": "TOURIST",
            "isMember": False,
            "numAdults": adults,
            "numChildren": 0,
            "numInfants": 0,
            "resident": False,
            "searchMainProductType": "FLIGHT",
            "segmentRequests": [       
                                    {       
                                        "dateString": str(datetime.datetime.utcfromtimestamp(num1).strftime('%d-%m-%Y')),
                                        "departure": {       
                                                        "iataCode": choice_1.get('iataCode'),
                                                        "name": choice_1.get('cityName')
                                                    },
                                         "destination": {       
                                                            "iataCode": choice_2.get('iataCode'),
                                                            "name": choice_2.get('cityName')
                                                        }
                                    },
                                    {       
                                        "dateString": str(datetime.datetime.utcfromtimestamp(num2).strftime('%d-%m-%Y')),
                                        "departure": {       
                                                        "iataCode": choice_2.get('iataCode'),
                                                        "name": choice_2.get('cityName')
                                                    },
                                         "destination": {       
                                                            "iataCode": choice_1.get('iataCode'),
                                                            "name": choice_1.get('cityName')
                                                        }
                                    }
                                ]
                            }
                        }

    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}    

   
def sl_odige_priceBreakdown( campaign_data, device_data, app_data ):
    url= "https://msl.odigeo.com/mobile-api/msl/priceBreakdown"
    method= "post"
    headers= {       
        "Accept": "application/vnd.com.odigeo.msl.v5+json;charset=utf-8",
        "Accept-Encoding": "gzip,deflate,sdch",
        "Content-Type": "application/json",
        "Device-ID": "ANDROID;"+device_data.get('model')+";"+device_data.get('os_version')+";O;OPGB;c8ff196a-b7aa-4bb7-bdd8-089a525c8510;en;en;"+campaign_data.get('app_version_name')+";"+campaign_data.get('app_version_code')+";XL",
        "User-Agent": 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
        }

    params= None

    data= {       
        "numAdults": adults,
        "numChildren": 0,
        "numInfants": 0,
        "price": random.choice(price_list),
        "sortCriteria": "MINIMUM_PURCHASABLE_PRICE"
        }

    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}    

#######################################################################

def mat_serve(campaign_data, app_data, device_data,action,log=False,data=False, isopen=False, include=False, delay=False):
    param = action.split(':')
    action = param[0]


    url = 'http://%s.engine.mobileapptracking.com/serve' % campaign_data.get('mat').get('advertiser_id')
    headers ={
                'Content-Type': 'application/json',
                'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
                'Accept': 'application/json',
                'Accept-Encoding':'gzip',

                }
    method = 'post'

    params = {
        'action':action,
        'advertiser_id':campaign_data.get('mat').get('advertiser_id'),
        'package_name':campaign_data.get('package_name'),
        'sdk':campaign_data.get('mat').get('sdk'),
        'sdk_retry_attempt':'0',
        'transaction_id':str(uuid.uuid4()),
        'ver':campaign_data.get('mat').get('version'),
    }

    if not app_data.get('mat_id'):
        app_data['mat_id'] = str(uuid.uuid4())
        
    if not app_data.get('referrer'):
        make_another_referrer(app_data,device_data)
        
    
        

    data_value = {
            #'altitude':'0.0',
            'connection_type':device_data.get('network').lower(),
            #'app_ad_tracking' :'0',
            'build' :device_data.get('build'),
           
            'app_name':campaign_data.get('app_name'),
            'app_version':campaign_data.get('app_version_code'),
            'app_version_name':campaign_data.get('app_version_name'),
            #'country_code':device_data.get('locale').get('country'),
            'device_brand':device_data.get('manufacturer'),
            'device_carrier':device_data.get('carrier'),
            'device_cpu_type':device_data.get('cpu_abi'),
            'device_model':device_data.get('model'),
            'insdate':int(app_data.get('times').get('install_complete_time')),
            'existing_user':'1',
            'installer':'com.android.vending',
            'language':device_data.get('locale').get('language'),
            'mat_id':app_data.get('mat_id'),
            'os_version':device_data.get('os_version'),
            'screen_density':util.getdensity(device_data.get('dpi')),
            'screen_layout_size':device_data.get('resolution').split('x')[1]+'x'+device_data.get('resolution').split('x')[0],
            'sdk_version':campaign_data.get('mat').get('version'),
            'conversion_user_agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
            'currency_code':'USD',
            'revenue':'0.0',
            'is_coppa' :'0',
            'click_timestamp':int(app_data.get('times').get('click_time')),
            'download_date':int(app_data.get('times').get('click_time')),
            'fire_ad_tracking_disabled':0,
            'platform_ad_tracking_disabled':0,
            'platform_aid':device_data.get('adid'),
            #'sdk_plugin':'unity',
            #'mobile_country_code':device_data.get('mcc'),
            #'mobile_network_code':device_data.get('mnc'),
            'locale':device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
            'google_aid':device_data.get('adid'),
            'google_ad_tracking_disabled':'0',
            'install_referrer':app_data.get('referrer') if app_data.get('referrer') else make_another_referrer(app_data,device_data),
            'system_date':str(int(time.time())),
            #'user_id':app_data.get('u_id'),
            }

    if log:
        if app_data.get('open_log_id'):
            data_value['open_log_id'] = str(app_data.get('open_log_id'))
        if app_data.get('last_open_log_id'):
            data_value['last_open_log_id'] = str(app_data.get('last_open_log_id'))
            
    if action=='session':
        # if isopen:
        # data_value['altitude']=random.uniform(149,150)
       # data_value['latitude']=app_data.get('latlong').get('latitude')
       # data_value['longitude']=app_data.get('latlong').get('longitude')
        if include==True:
            params['referral_source'] = campaign_data.get('package_name')
            data_value['referral_source']=campaign_data.get('package_name')
            params['referral_url']='op-app://results/result'
            data_value['referral_url']='op-app://results/result'
        if delay==True:
            data_value['referrer_delay']=random.randint(1000,1300)

 
    if action == 'conversion':
        params['site_event_name'] = param[1]
        params['referral_source'] = campaign_data.get('package_name')
        data_value['altitude']='0.0'
        data_value['date1']=str(num1)
        data_value['date2']=str(num2)
        data_value['latitude']=app_data.get('latlong').get('latitude')
        data_value['longitude']=app_data.get('latlong').get('longitude')
        data_value['referral_source']=campaign_data.get('package_name')
        data_value['referrer_delay']=random.randint(1300,1400)

        if param[1]=='search':
            data = {"data":[{"quantity":str(adults),"item":str(choice_1.get('iataCode'))+"-"+str(choice_2.get('iataCode'))}]}
        elif param[1]=='ViewProduct' or param[1]=='ViewBasket': 
            data = {"data":[{"unit_price":str(price),"quantity":str(adults),"item":str(choice_1.get('iataCode'))+"-"+str(choice_2.get('iataCode'))}]}  
  
    da_str = urllib.urlencode(data_value)

    key = campaign_data.get('mat').get('key')
    iv = campaign_data.get('mat').get('iv')
    aes = AESCipher(key)
    params['data'] = aes.encrypt(iv, da_str)
    
    return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}


############################ MEASUREMENT API CALL ################################
def measurement_api(campaign_data,app_data,device_data):
    method='get'
    url='http://%s.measurementapi.com/serve' % campaign_data.get('measurement_api').get('publisher_id')
    headers={
            'Accept-Encoding':'gzip',
            'User-Agent':get_ua(device_data),
            'Referer':'http://www.google.com'
    }
    params={
           'action':'click',
           'my_campaign':'SHARE_APP',
           'my_publisher':'Internal',
           'my_site':'OP',
           'publisher_id':campaign_data.get('measurement_api').get('publisher_id'),
           'site_id_android':campaign_data.get('measurement_api').get('site_id_android'),
           'site_id_ios':campaign_data.get('measurement_api').get('site_id_ios'),
           'site_id_web':campaign_data.get('measurement_api').get('site_id_web')

    }
    data=None
    return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

################### MEASUREMENT CONFIG ##########################

def measurement_config(campaign_data,app_data,device_data):
    url='http://app-measurement.com/config/app/1%3A859795194611%3Aandroid%3A4ff5456e62f5ca09'
    method='get'
    header={
        'User-Agent': app_data.get('ua') ,
        #'Content-Type':'application/json; charset=UTF-8',
        #'X-Unity-Version': campaign_data.get('kochava').get('x_unity_version'),
        'Accept-Encoding': 'gzip',
        }
    params={
           'app_instance_id' :random_string(32),
           'gmp_version'     :campaign_data.get('mat').get('gmp_version'),
           'platform'        :device_data.get('manufacturer').lower()

    }
    data=None
    return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': data}

################# OPODO CALL ##################

def opodo(campaign_data,app_data,device_data):
    url='http://www.opodo.co.uk/app/'
    method='get'
    header={
    'Accept-Encoding': 'gzip',
    'User-Agent': 'Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6',
    'Cookie': '188712_119563_d41d8cd98f00b204e9800998ecf8427e=da1c3251bc36b62f73788905c93c3a8c; 119563_tracking_id=da1c3251bc36b62f73788905c93c3a8c',
    'Referer':'http://www.google.com'
    }
    params={'referrer':app_data.get('referrer')}
    data=None
    return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': data}

############### MANDATORY FUNCTIONS ###############################

def click(device_data=None, camp_type='market', camp_plat = 'android'):

    package_name = campaign_data.get('package_name');
    serial        = device_data.get('serial')
    agent_id      = Config.AGENTID
    random_number = random.randint(1,10)
    source_id     = ""
    if random_number < 5:
        source_id = "728"
    elif random_number < 8:
        source_id = "517"
    elif random_number < 9:
        source_id = "604"
    else:
        source_id = "386"
    st   = device_data.get("device_id", str(int(time.time()*1000)))

    link = campaign_data['link']
    link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
    return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def random_string(len,typ='hex'):
    if typ=='hex':
        return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
    if typ=='numb':
        return ''.join([random.choice("123456789") for _ in range(len)])

############### function called by calls ###########

def make_another_referrer(app_data,device_data):

    if not app_data.get('sec'):
        make_sec(app_data,device_data)
        
    app_data['referrer'] = 'mat_click_id='+util.get_random_string('hex',32)+'-'+datetime.datetime.utcfromtimestamp(int(time.time())+app_data.get('sec')).strftime("%Y%m%d")+'-'+util.get_random_string('decimal',4)
    
    return app_data.get('referrer')

# def def_sec(app_data,device_data):
#     timez = device_data.get('timezone')
#     sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
#     if int(timez) < 0:
#         sec = (-1) * sec
#     if not app_data.get('sec'):
#         app_data['sec'] = sec

def make_sec(app_data,device_data): 
    timez = device_data.get('timezone')
    sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
    if int(timez) < 0:
        sec = (-1) * sec
    if not app_data.get('sec'):
        app_data['sec'] = sec;
        
    return app_data.get('sec')


def current_location(app_data):
    url='https://api.letgo.com/api/iplookup.json'
    header={
    'User-Agent': 'okhttp/2.3.0'
    }
    return {'url': url, 'httpmethod': 'get', 'headers': header, 'params': None, 'data': None}

def get_ua(device_data):
    if int(device_data.get("sdk")) >=19:
        return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
    else:
        return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'



