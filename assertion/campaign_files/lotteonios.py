# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
import time
import random
import json
import datetime
import uuid, urllib
import clicker
import Config

from Crypto.Cipher import AES


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'kr.co.lohbs.app.ios',
	'app_size'		     : 46.3,
	'app_name' 			 :'Lohbs',
	'app_version_name' 	 :"3.07",#'3.04',
	'app_version_code' 	 :"20190813",#'20190522',
	'app_id' 			 : '1125919749',
	'ctr' 				 : 6,
	'sdk' 		 		 : 'ios',
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '9.0',
	'tracker'		 	 : 'ad-brix',

	'ad-brix': {
			'app_key': '380064148',
			'ver': '2.4.8in',
			'key_getreferral': 'efc914e52be34622efc914e52be34622',
			'iv_getreferral' : 'efc914e52be34622',
			'key_tracking': 'srkterowgawrsozerruly82nfij625w9',
			'iv_tracking' : 'srkterowgawrsoze'
			},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),
	
	
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	app_data['install_mdatetime']=get_timestamp(para1=0,para2=0)
	app_data['install_datetime']=get_date1(app_data,device_data,para1=0,para2=0)

	app_data['puid']=util.get_random_string('hex',96)
	app_data['odin']=util.get_random_string('hex',40)

	###########		CALLS		############	

	print "AD BRIX get Referral"
	get_referral = ad_brix_get_referral(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	get_referral_result =util.execute_request(**get_referral)
	try:
		if json.loads(get_referral_result.get('data')).get('ResultCode')==5600:
			print "AD BRIX get Referral   (Base  16)"
			get_referral = ad_brix_get_referral(campaign_data, app_data, device_data, base_change=True)
			get_referral_result =util.execute_request(**get_referral)
			try:
				get_referral_result_decode = json.loads(get_referral_result.get('data'))
				app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
				app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))
			except:
				app_data['user_number'] = "1137070546381692850"
				app_data['shard_number'] = "13"	
		get_referral_result_decode = json.loads(get_referral_result.get('data'))
		app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
		app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))

		if not app_data.get('user_number'):
			app_data['user_number'] = "1137070546381692850"

		if not app_data.get('shard_number'):
			app_data['shard_number'] = "13"

	except:
		app_data['user_number'] = "1137070546381692850"
		app_data['shard_number'] = "13"

	print "Ad Brix Get Schedule"
	ad_brix_schedule = ad_brix_get_schedule(campaign_data, app_data, device_data)	
	util.execute_request(**ad_brix_schedule)

	time.sleep(random.randint(1,2))

	print "Ad Brix Tracking Start"
	request=ad_brix_tracking_start(campaign_data, app_data, device_data)
	util.execute_request(**request)

	request=measurement(campaign_data, app_data, device_data)
	util.execute_request(**request)


	time.sleep(random.randint(0,5*60))


	if random.randint(1,100) <= 10:

		time.sleep(random.randint(5,10))

		print "Ad Brix Tracking event"
		request = ad_brix_tracking_event(campaign_data, app_data, device_data, "end", 'session')
		util.execute_request(**request)

		print "Ad Brix Tracking event"
		request = ad_brix_tracking_event(campaign_data, app_data, device_data, "start", 'session')
		util.execute_request(**request)


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	

	###########		INITIALIZE		############	
	print "Please wait ....."

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),
	
	
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')

	if not app_data.get('idfv_id'):
		if not device_data.get('idfv_id'):
			app_data['idfv_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfv_id'] = device_data.get('idfv_id')


	if not app_data.get('install_mdatetime'):
		app_data['install_mdatetime']=get_timestamp(para1=0,para2=0)

	if not app_data.get('install_datetime'):
		app_data['install_datetime']=get_date1(app_data,device_data,para1=0,para2=0)

	if not app_data.get('puid'):
		app_data['puid']=util.get_random_string('hex',96)

	if not app_data.get('odin'):
		app_data['odin']=util.get_random_string('hex',40)

	###########		CALLS		############	


	if not app_data.get('user_number') or not app_data.get('shard_number'):

		print "AD BRIX get Referral"
		get_referral = ad_brix_get_referral(campaign_data, app_data, device_data)
		get_referral_result =util.execute_request(**get_referral)
		try:
			if json.loads(get_referral_result.get('data')).get('ResultCode')==5600:
				print "AD BRIX get Referral   (Base  16)"
				get_referral = ad_brix_get_referral(campaign_data, app_data, device_data, base_change=True)
				get_referral_result =util.execute_request(**get_referral)
				try:
					get_referral_result_decode = json.loads(get_referral_result.get('data'))
					app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
					app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))
				except:
					app_data['user_number'] = "1137070546381692850"
					app_data['shard_number'] = "13"	
			get_referral_result_decode = json.loads(get_referral_result.get('data'))
			app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
			app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))

			if not app_data.get('user_number'):
				app_data['user_number'] = "1137070546381692850"

			if not app_data.get('shard_number'):
				app_data['shard_number'] = "13"

		except:
			app_data['user_number'] = "1137070546381692850"
			app_data['shard_number'] = "13"


	print "Ad Brix Get Schedule"
	ad_brix_schedule = ad_brix_get_schedule(campaign_data, app_data, device_data)	
	util.execute_request(**ad_brix_schedule)


	time.sleep(random.randint(1,2))

	print "Ad Brix Tracking Start"
	request=ad_brix_tracking_start(campaign_data, app_data, device_data, is_open = True)
	util.execute_request(**request)


	time.sleep(random.randint(0,5*60))

	if random.randint(1,100) <= 5:

		time.sleep(random.randint(5,10))

		print "Ad Brix Tracking event"
		request = ad_brix_tracking_event(campaign_data, app_data, device_data, "end", 'session')
		util.execute_request(**request)

		print "Ad Brix Tracking event"
		request = ad_brix_tracking_event(campaign_data, app_data, device_data, "start", 'session')
		util.execute_request(**request)


	return {'status':'true'}



def measurement(campaign_data, app_data, device_data):
	url= "http://app-measurement.com/config/app/1:190022801909:ios:5a61cd1448fb589c"
	method= "get"
	headers= {       
        "Accept-Encoding": "br, gzip, deflate",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "User-Agent": urllib.quote('롭스') +'/'+campaign_data.get('app_version_code')+'.1 CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],}

	params= {       "app_instance_id": "AFDFE09DA08D47FF9FF08B506E80DA5E",
        "gmp_version": "40200",
        "platform": "ios"}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

#################################################
#		AD-BRIX 								#
#################################################


def ad_brix_get_schedule(campaign_data, app_data, device_data):

	method = 'post'
	
	url = 'http://campaign.ad-brix.com/v1/CampaignVer2/GetSchedule'
	
	header={
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1, ja-US;q=0.9, ru-US;q=0.8, ar-US;q=0.7, fr-US;q=0.6, pt-BR;q=0.5',
				'Accept-Encoding': 'br, gzip, deflate',
				'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+'.1 (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)'
		}
		
	data = {
				'checksum': 'init',				
				'k': campaign_data.get('ad-brix').get('app_key'),
				'language': device_data.get('locale').get('language'),
				
	}
	
	
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}			
	
def ad_brix_get_referral(campaign_data, app_data, device_data, base_change=False):

	url = 'http://cvr.ad-brix.com/v1/conversion/GetReferral'
	headers = {	
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1, ja-US;q=0.9, ru-US;q=0.8, ar-US;q=0.7, fr-US;q=0.6, pt-BR;q=0.5',
				'Accept-Encoding': 'gzip',
				'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+'.1 (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)'
			}
				
	method = 'post'	
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	event_date = get_date1(app_data,device_data,para1=0,para2=0)

	data_value=json.dumps({"user_info":{"initial_ad_id":app_data.get('idfa_id'),
		"openudid_sha1":"",
		"mac_sha1_low":"",
		"ios_ifa_sha1":util.sha1(app_data.get('idfa_id')),
		"language":device_data.get('locale').get('language'),
		"country":device_data.get('locale').get('country'),
		"openudid_md5":"",
		"odin":app_data.get('odin'),
		"device_id_md5":"",
		"mac_sha1":"",
		"mac_md5_low":"",
		"ios_ifa_tracking_enabled":"false",
		"android_id_sha1":"",
		"android_id_md5":"",
		"ios_ifv_md5":util.md5(app_data.get('idfv_id')),
		"openudid":"","mac_md5":"",
		"ios_ifa":app_data.get('idfa_id'),
		"ios_ifa_md5":util.md5(app_data.get('idfa_id')),
		"carrier":device_data.get('carrier'),
		"ios_ifv":app_data.get('idfv_id'),
		"ios_ifv_sha1":util.sha1(app_data.get('idfv_id')),
		"device_id_sha1":"",
		"puid":app_data.get('puid')},
		"demographics":[],
		"version":campaign_data.get('ad-brix').get('ver'),
		"app_version_name":campaign_data.get('app_version_name'),
		"cohort_info":"","device_info":{"vendor":"appstore",
		"os":"i_"+device_data.get('os_version'),
		"height":device_data.get('resolution').split('x')[1],
		"is_wifi_only":"false","build_id":device_data.get('build'),
		"model":device_data.get('device_platform'),
		"width":device_data.get('resolution').split('x')[0],"ptype":"ios","network":"wifi","is_portrait":"true"},
		"package_name":campaign_data.get('package_name'),
		"conversion_cache":[],
		"adbrix_user_info":{"install_mdatetime":-1,
		"shard_no":-1,
		"referral_data":"",
		"reengagement_datetime":0,
		"referral_key":-1,
		"last_referral_key":-1,
		"reengagement_data":"",
		"life_hour":0,
		"reengagement_conversion_key":-1,
		"last_referral_data":"",
		"last_referral_datetime":"",
		"app_launch_count":1,
		"install_datetime":"",
		"adbrix_user_no":-1},
		"appkey":campaign_data.get('ad-brix').get('app_key'),
		"activity_info":[{"group":"session","activity":"start","param":"","prev_group":"","prev_activity":"","created_at":event_date}],
		"app_version_code":int(campaign_data.get('app_version_code')),
		"referral_info":{"conversion_key":-1,
		"session_no":-1},"searchads_response":""})
			
	if base_change:
		key = campaign_data.get('ad-brix').get('key_getreferral')
		iv = campaign_data.get('ad-brix').get('iv_getreferral')
		aes = AESCipher(key,bs=16)
		data['j'] = aes.encrypt(iv,data_value)	
	else:			
		key = campaign_data.get('ad-brix').get('key_getreferral')
		iv = campaign_data.get('ad-brix').get('iv_getreferral')
		aes = AESCipher(key)
		data['j'] = aes.encrypt(iv,data_value)

	update_activity(app_data,device_data,'session','start')
	
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}	
		
def ad_brix_tracking_start(campaign_data, app_data, device_data, is_open = False):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	headers = {	
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1, ja-US;q=0.9, ru-US;q=0.8, ar-US;q=0.7, fr-US;q=0.6, pt-BR;q=0.5',
				'Accept-Encoding': 'gzip',
				'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+'.1 (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)'
			}
				
	method = 'post'	
	
	event_date = get_date1(app_data,device_data,para1=60,para2=100)

	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}


	data_value={
				"user_info": {
					"initial_ad_id": app_data.get('idfa_id'),
					"openudid_sha1": "",
					"mac_sha1_low": "",
					"ios_ifa_sha1": util.sha1(app_data.get('idfa_id')),
					"language": device_data.get('locale').get('language'),
					"country": device_data.get('locale').get('country'),
					"openudid_md5": "",
					"odin": app_data.get('odin'),
					"device_id_md5": "",
					"mac_sha1": "",
					"mac_md5_low": "",
					"ios_ifa_tracking_enabled": "false",
					"android_id_sha1": "",
					"android_id_md5": "",
					"ios_ifv_md5": util.md5(app_data.get('idfv_id')),
					"openudid": "",
					"mac_md5": "",
					"ios_ifa": app_data.get('idfa_id'),
					"ios_ifa_md5": util.md5(app_data.get('idfa_id')),
					"carrier": device_data.get('carrier'),
					"ios_ifv": app_data.get('idfa_id'),
					"ios_ifv_sha1": util.sha1(app_data.get('idfv_id')),
					"device_id_sha1": "",
					"puid": app_data.get('puid')
				},
				"demographics": [],
				"version": campaign_data.get('ad-brix').get('ver'),
				"app_version_name": campaign_data.get('app_version_name'),
				"cohort_info": "",
				"device_info": {
					"vendor": "appstore",
					"os": "i_" + device_data.get('os_version'),
					"height": device_data.get('resolution').split('x')[1],
					"is_wifi_only": "false",
					"build_id": device_data.get('build'),
					"model": device_data.get('device_platform'),
					"width": device_data.get('resolution').split('x')[0],
					"ptype": "ios",
					"network": "wifi",
					"is_portrait": "true"
				},
				"impression_info": [],
				"package_name": campaign_data.get('package_name'),
				"conversion_cache": [],
				"adbrix_user_info": {
					"install_mdatetime": app_data.get('install_mdatetime'),
					"shard_no": int(app_data.get('shard_number')),
					"referral_data": "",
					"reengagement_datetime": 0,
					"referral_key": 0,
					"last_referral_key": -1,
					"reengagement_data": "",
					"life_hour": 0,
					"reengagement_conversion_key": -1,
					"last_referral_data": "",
					"last_referral_datetime": "",
					"app_launch_count": 1,
					"install_datetime": app_data.get('install_datetime'),
					"adbrix_user_no": int(app_data.get('user_number'))
				},
				"appkey": campaign_data.get('ad-brix').get('app_key'),
				"activity_info": [{"prev_activity":"","created_at":event_date,"activity":"start","param":"","group":"session","prev_group":"","event_id":"1B443A92-F062-4034-B939-F966453CBFF8"},{"prev_activity":"start","created_at":event_date,"activity":"retention","param":"","group":"session","prev_group":"session","event_id":"8D4D362B-2F2D-4DFC-98EC-253F42823E8E"}],
				"app_version_code": int(campaign_data.get('app_version_code')),
				"referral_info": {
					"conversion_key": -1,
					"session_no": -1
				}
			}

	if is_open:
		data_value['activity_info'] = [{"prev_activity":"","created_at":event_date,"activity":"start","param":"","group":"session","prev_group":"","event_id":str(uuid.uuid4())},{"prev_activity":"start","created_at":event_date,"activity":"retention","param":"","group":"session","prev_group":"session","event_id": str(uuid.uuid4())}]


	data_value = json.dumps(data_value)

	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)
	
	update_activity(app_data,device_data,'session','retention')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}	


def ad_brix_tracking_event(campaign_data, app_data, device_data, activity, group):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	headers = {	
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+';q=1, ja-US;q=0.9, ru-US;q=0.8, ar-US;q=0.7, fr-US;q=0.6, pt-BR;q=0.5',
				'Accept-Encoding': 'gzip',
				'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+'.1 (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)'
			}
				
	method = 'post'	
	
	event_date = get_date1(app_data,device_data,para1=0,para2=0)

	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}


	data_value=json.dumps({
				"user_info": {
					"initial_ad_id": app_data.get('idfa_id'),
					"openudid_sha1": "",
					"mac_sha1_low": "",
					"ios_ifa_sha1": util.sha1(app_data.get('idfa_id')),
					"language": device_data.get('locale').get('language'),
					"country": device_data.get('locale').get('country'),
					"openudid_md5": "",
					"odin": app_data.get('odin'),
					"device_id_md5": "",
					"mac_sha1": "",
					"mac_md5_low": "",
					"ios_ifa_tracking_enabled": "false",
					"android_id_sha1": "",
					"android_id_md5": "",
					"ios_ifv_md5": util.md5(app_data.get('idfv_id')),
					"openudid": "",
					"mac_md5": "",
					"ios_ifa": app_data.get('idfa_id'),
					"ios_ifa_md5": util.md5(app_data.get('idfa_id')),
					"carrier": device_data.get('carrier'),
					"ios_ifv": app_data.get('idfa_id'),
					"ios_ifv_sha1": util.sha1(app_data.get('idfv_id')),
					"device_id_sha1": "",
					"puid": app_data.get('puid')
				},
				"demographics": [],
				"version": campaign_data.get('ad-brix').get('ver'),
				"app_version_name": campaign_data.get('app_version_name'),
				"cohort_info": "",
				"device_info": {
					"vendor": "appstore",
					"os": "i_" + device_data.get('os_version'),
					"height": device_data.get('resolution').split('x')[1],
					"is_wifi_only": "false",
					"build_id": device_data.get('build'),
					"model": device_data.get('device_platform'),
					"width": device_data.get('resolution').split('x')[0],
					"ptype": "ios",
					"network": "wifi",
					"is_portrait": "true"
				},
				"impression_info": [],
				"package_name": campaign_data.get('package_name'),
				"conversion_cache": [],
				"adbrix_user_info": {
					"install_mdatetime": app_data.get('install_mdatetime'),
					"shard_no": int(app_data.get('shard_number')),
					"referral_data": "",
					"reengagement_datetime": 0,
					"referral_key": 0,
					"last_referral_key": -1,
					"reengagement_data": "",
					"life_hour": 0,
					"reengagement_conversion_key": -1,
					"last_referral_data": "",
					"last_referral_datetime": "",
					"app_launch_count": 1,
					"install_datetime": app_data.get('install_datetime'),
					"adbrix_user_no": int(app_data.get('user_number'))
				},
				"appkey": campaign_data.get('ad-brix').get('app_key'),
				"activity_info": [{"prev_activity":app_data.get('activity'),"created_at":event_date,"activity":activity,"param":"","group":group,"prev_group":app_data.get('group'),"event_id": str(uuid.uuid4())}],
				"app_version_code": int(campaign_data.get('app_version_code')),
				"referral_info": {
					"conversion_key": -1,
					"session_no": -1
				}
			})

	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)
	
	update_activity(app_data,device_data,group,activity)
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec


def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('hex',64)

def device_id(app_data):
	if not app_data.get('x-device-id'):
		app_data['x-device-id']=str(uuid.uuid4()).upper()


def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date1(app_data,device_data,para1=0,para2=0):
	time.sleep(random.randint(para1,para2))
	make_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y%m%d%H%M%S")
	return date
# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

class AESCipher(object):

    def __init__(self, key, bs=32): 
        self.bs = bs
        self.key = key.encode()
		
    def encrypt(self, iv,raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')
        
    def decrypt(self, enc):
        # enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

class AESCipherTracking(object):

    def __init__(self, key): 
        self.bs = 16
        self.key = key.encode()
		
    def encrypt(self, iv,raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')
        
    def decrypt(self, enc):
        # enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]	


def update_activity(app_data,device_data,group='',activity=''):
	app_data['group']=group
	app_data['activity']=activity

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')

def get_timestamp(para1=0,para2=0):
	time1 = int(time.time())
	return time1
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"

def install_receipt():
	return 'MIISeAYJKoZIhvcNAQcCoIISaTCCEmUCAQExCzAJBgUrDgMCGgUAMIICGQYJKoZIhvcNAQcBoIICCgSCAgYxggICMAoCARQCAQEEAgwAMAsCAQ4CAQEEAwIBWjALAgEZAgEBBAMCAQMwDQIBAwIBAQQFDAM3NDcwDQIBCgIBAQQFFgMxNyswDQIBDQIBAQQFAgMB1SYwDQIBEwIBAQQFDAM3NDcwDgIBAQIBAQQGAgQ9/dv3MA4CAQkCAQEEBgIEUDI1MjAOAgELAgEBBAYCBAH7KygwDgIBEAIBAQQGAgQxkAbZMBACAQ8CAQEECAIGHD5IjWlXMBQCAQACAQEEDAwKUHJvZHVjdGlvbjAYAgECAgEBBBAMDk15Y29vbi5TcG9vbk1lMBgCAQQCAQIEEIYkWD5oD+q9gv72qVPGX0EwHAIBBQIBAQQUe2qXYf7T+somBvdac99bUkzhge0wHgIBCAIBAQQWFhQyMDE5LTA2LTA3VDA4OjAyOjM1WjAeAgEMAgEBBBYWFDIwMTktMDYtMDdUMDg6MDI6MzZaMB4CARICAQEEFhYUMjAxOS0wNi0wN1QwODowMjozNlowPAIBBwIBAQQ05jlYK43IlimAhYE5oY8eAstemurG4YdyMG/SWH1pIMQCDD7ohoELhbeZ1RetRZAYqSS4uzBGAgEGAgEBBD6LR1R0+Kn+3nLVn1ruvapsD2DdBL9Bdoxo582spoQsmG49QG8KDVIkM6fwfFgc/fllF3O3dQNQ3Z8ZKiH/WKCCDmUwggV8MIIEZKADAgECAggO61eH554JjTANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xNTExMTMwMjE1MDlaFw0yMzAyMDcyMTQ4NDdaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClz4H9JaKBW9aH7SPaMxyO4iPApcQmyz3Gn+xKDVWG/6QC15fKOVRtfX+yVBidxCxScY5ke4LOibpJ1gjltIhxzz9bRi7GxB24A6lYogQ+IXjV27fQjhKNg0xbKmg3k8LyvR7E0qEMSlhSqxLj7d0fmBWQNS3CzBLKjUiB91h4VGvojDE2H0oGDEdU8zeQuLKSiX1fpIVK4cCc4Lqku4KXY/Qrk8H9Pm/KwfU8qY9SGsAlCnYO3v6Z/v/Ca/VbXqxzUUkIVonMQ5DMjoEC0KCXtlyxoWlph5AQaCYmObgdEHOwCl3Fc9DfdjvYLdmIHuPsB8/ijtDT+iZVge/iA0kjAgMBAAGjggHXMIIB0zA/BggrBgEFBQcBAQQzMDEwLwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcjA0MB0GA1UdDgQWBBSRpJz8xHa3n6CK9E31jzZd7SsEhTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHgYDVR0gBIIBFTCCAREwggENBgoqhkiG92NkBQYBMIH+MIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDYGCCsGAQUFBwIBFipodHRwOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQANphvTLj3jWysHbkKWbNPojEMwgl/gXNGNvr0PvRr8JZLbjIXDgFnf4+LXLgUUrA3btrj+/DUufMutF2uOfx/kd7mxZ5W0E16mGYZ2+FogledjjA9z/Ojtxh+umfhlSFyg4Cg6wBA3LbmgBDkfc7nIBf3y3n8aKipuKwH8oCBc2et9J6Yz+PWY4L5E27FMZ/xuCk/J4gao0pfzp45rUaJahHVl0RYEYuPBX/UIqc9o2ZIAycGMs/iNAGS6WGDAfK+PdcppuVsq1h1obphC9UynNxmbzDscehlD86Ntv0hgBgw2kivs3hi1EdotI9CO/KBpnBcbnoB7OUdFMGEvxxOoMIIEIjCCAwqgAwIBAgIIAd68xDltoBAwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTEzMDIwNzIxNDg0N1oXDTIzMDIwNzIxNDg0N1owgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjgaYwgaMwHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwDgYDVR0PAQH/BAQDAgGGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQBPz+9Zviz1smwvj+4ThzLoBTWobot9yWkMudkXvHcs1Gfi/ZptOllc34MBvbKuKmFysa/Nw0Uwj6ODDc4dR7Txk4qjdJukw5hyhzs+r0ULklS5MruQGFNrCk4QttkdUGwhgAqJTleMa1s8Pab93vcNIx0LSiaHP7qRkkykGRIZbVf1eliHe2iK5IaMSuviSRSqpd1VAKmuu0swruGgsbwpgOYJd+W+NKIByn/c4grmO7i77LpilfMFY0GCzQ87HUyVpNur+cmV6U/kTecmmYHpvPm0KdIBembhLoz2IYrF+Hjhga6/05Cdqa3zr/04GpZnMBxRpVzscYqCtGwPDBUfMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCA7rV4fnngmNMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEAmBXZyXyRMHSq0dzOcUALo0yG0ZfBEPSW09qhpC3zRGEaZmmRv0pLQTcgLdl8q0MXCZOaoYiWf4KtMNjatwlpMAOhWw5aUUV9wyg0fnCO3lGJ+m4hU8xw1xxKHq3Adq4OfhkspTcda+cLY1nFHbSkquHrvKMh++7MyZ6nrB2x2iTo6LDCSXoUW6vD4M4HHaVrAe41Y5JccUGXUa6UfecNLM5Ul0nxdRBhHKkjN0B49PNn4mz+SmOPQQDv5oX4407paTTBVDGW5zZh5Dbh6KTHLpile2TyyyIs5+DWFLM3KEWmCtYkTk1HJBM2GDQ/wLw1WSdf/3c1KnMhxQ2T3d3f3w=='
