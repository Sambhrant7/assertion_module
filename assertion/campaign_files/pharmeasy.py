# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
from sdk import NameLists
import time, urllib
import random
import uuid
import json
import string
import datetime
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.phonegap.rxpal',
	'app_size'			 : 11.5,#12.0,
	'app_name' 			 :'PharmEasy',
	'app_version_name' 	 : '4.8.24',#'4.8.21',#'4.8.20',#'4.8.14',#'4.8.11',
	'app_version_code' 	 : '40125',#'40121',#'40120',#'40114',#'40111',
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'c5jBuzzseAWbAn996YfanD',
		'dkh'		 : 'c5jBuzzs',
		'buildnumber': '4.10.3',#'4.10.2',#'4.10.0',#'4.8.20',
		'version'	 : 'v4',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:25,
				2:25,
				3:24,
				4:24,
				5:23,
				6:22,
				7:20,
				8:20,
				9:20,
				10:19,
				11:19,
				12:19,
				13:18,
				14:18,
				15:17,
				16:16,
				17:15,
				18:14,
				19:13,
				20:13,
				21:13,
				22:13,
				23:11,
				24:10,
				25:10,
				26:9,
				27:8,
				28:8,
				29:6,
				30:6,
				31:5,
				32:5,
				33:4,
				34:4,
				35:3
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	

	###########		INITIALIZE		############	

	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	def_sec(app_data,device_data)

	################generating_realtime_differences##################

	app_data['extra_referrer_time']=str(int(time.time()*1000))
	time.sleep(random.randint(2,4))
	def_firstLaunchDate(app_data,device_data)

	app_data['user_id'] = str(uuid.uuid4())
	
 	###########	 CALLS		 ############

 	global product_id_list, auth, variation_list, price_list, content_list

 	price_list=[]
 	content_list=[]
 	variation_list=[]
 	product_id_list=[]
 	auth=util.get_random_string('google_token',86)

 	print 'lat and lon generator'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	try:
		json_lat_lon_result=json.loads(lat_lon_result.get('data')).get('geo')
		app_data['city']=json_lat_lon_result.get('city').encode('utf-8')
		app_data['pincode']=json_lat_lon_result.get('postal_code')

		if not app_data.get('city'):
			app_data['city']='noida'

		if not app_data.get('pincode'):
			app_data['pincode'] = '201301'

	except:
		print 'Exception'
		app_data['city']='noida'
		app_data['pincode']='201301'

	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)

	time.sleep(random.randint(2,3))	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	time.sleep(random.randint(10,15))	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(25,40))
	request=search_selfCall( campaign_data, device_data, app_data )
	response=util.execute_request(**request)
	try:
		output=json.loads(response.get('data')).get('data').get('products')

		for i in range(len(output)):
			if output[i].get('productId'):
				product_id_list.append(output[i].get('productId'))

		if len(product_id_list)<1:
			product_id_list=[2321, 4859, 27367, 50342, 8803, 40094, 50343, 25870, 65425, 51126]

	except:
		print 'Exception-------------'
		product_id_list=[2321, 4859, 27367, 50342, 8803, 40094, 50343, 25870, 65425, 51126]

	time.sleep(random.randint(2,3))
	request=variant_selfCall(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	try:
		output=json.loads(response.get('data')).get('data').get('products')

		for i in range(len(output)):
			if output[i].get('productId') and output[i].get('name') and output[i].get('mrpDecimal'):
				variation_list.append(output[i].get('productId'))
				content_list.append(str(output[i].get('name').encode('utf-8')))
				price_list.append(str(output[i].get('mrpDecimal')))

		if len(variation_list)<1 or len(content_list)<1 or len(price_list)<1:
			variation_list=[6729, 219495, 34122]
			content_list=['AMANTREL 100MG CAP 15`S', "AMANTREL TAB 15'S", 'AMANTREL 100MG CAP 10`S']
			price_list=['183.70', '167.00', '55.00']

	except:
		print 'Exception-------------'
		variation_list=[6729, 219495, 34122]
		content_list=['AMANTREL 100MG CAP 15`S', "AMANTREL TAB 15'S", 'AMANTREL 100MG CAP 10`S']
		price_list=['183.70', '167.00', '55.00']


	for _ in range(1, random.randint(2,4)):

		if random.randint(1,100) <= 15:
			time.sleep(random.randint(4,6))
			af_add_to_cart(campaign_data, app_data, device_data)

		if random.randint(1,100) <= 10:
			time.sleep(random.randint(1,2))
			upload_rx(campaign_data, app_data, device_data)

		if random.randint(1,100) <= 60 and not app_data.get('registered'):
			app_data['app_user_id']='33'+util.get_random_string('decimal',5)
			time.sleep(random.randint(20,30))
			af_complete_registration(campaign_data, app_data, device_data)
			app_data['registered'] = True

		if random.randint(1,100) <= 10:
			time.sleep(random.randint(20,40))
			af_content_view_medicine(campaign_data, app_data, device_data)
		
		
		# Diagnostics block starts here

		if random.randint(1,100) <= 35:

			time.sleep(random.randint(20,40))
			af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_landing")
			af_d_landing_diagnostics(campaign_data, app_data, device_data)

			if random.randint(1,100) <= 23:

				time.sleep(random.randint(10,30))
				af_d_package_pdp(campaign_data, app_data, device_data)
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_package_pdp")

			if random.randint(1,100) <= 23:

				time.sleep(random.randint(10,30))
				af_d_test_pdp(campaign_data, app_data, device_data)
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_test_pdp")

			if random.randint(1,100) <= 14:

				time.sleep(random.randint(10,30))
				af_d_all_tests(campaign_data, app_data, device_data)
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_all_tests")

			if random.randint(1,100) <= 30:

				time.sleep(random.randint(15,40))
				print '\n'+'Appsflyer : Track____________________________________'
				request  = appsflyer_track(campaign_data, app_data, device_data, isOpen=False)
				util.execute_request(**request)

			if random.randint(1,100) <= 14:

				time.sleep(random.randint(10,30))
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_all_packages")
				af_d_all_packages(campaign_data, app_data, device_data)

			if random.randint(1,100) <= 23:

				time.sleep(random.randint(4*60,5*60))
				af_d_atc(campaign_data, app_data, device_data)
				af_add_to_cart_diagnostics(campaign_data, app_data, device_data, screen = "d_ATC")

			if random.randint(1,100) <= 23:

				time.sleep(random.randint(10,40))
				af_view_cart(campaign_data, app_data, device_data)
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_view_cart")

			if random.randint(1,100) <= 17:

				time.sleep(random.randint(20,40))
				af_d_all_labs(campaign_data, app_data, device_data)
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_all_labs")

				if random.randint(1,100) <= 83:

					time.sleep(random.randint(40,70))
					af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_patient_detail")

					if random.randint(1,100) <= 80:

						time.sleep(random.randint(40,70))
						af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_slot_select")
						af_d_slot_selection(campaign_data, app_data, device_data)

						if random.randint(1,100) <= 75:

							time.sleep(random.randint(40,70))
							af_d_payment(campaign_data, app_data, device_data)
							af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_payment")

							if random.randint(1,100) <= 66 and app_data.get('registered'):

								if not app_data.get('app_user_id'):
									app_data['app_user_id'] = '33'+util.get_random_string('decimal',5)

								time.sleep(random.randint(40,70))
								af_d_purchase(campaign_data, app_data, device_data)

			if random.randint(1,100) <= 6:
				time.sleep(random.randint(40,70))
				af_d_call_to_book(campaign_data, app_data, device_data)


		# Health care products starts here

		if random.randint(1,100) <= 15:

			time.sleep(random.randint(60,90))
			af_content_view_otc(campaign_data, app_data, device_data)

			if random.randint(1,100) <= 66:

				time.sleep(random.randint(30,60))
				af_add_to_cart_otc(campaign_data, app_data, device_data)



	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	
	###########		INITIALIZE		############	

	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_sec(app_data,device_data)

	if not app_data.get('prev_event_name'):
		def_eventsRecords(app_data)

	################generating_realtime_differences##################

	if not app_data.get('extra_referrer_time'):
		app_data['extra_referrer_time']=str(int(time.time()*1000))
	time.sleep(random.randint(2,4))
	def_firstLaunchDate(app_data,device_data)
	

	if not app_data.get('user_id'):
		app_data['user_id'] = str(uuid.uuid4())
	
 	###########	 CALLS		 ############

 	global product_id_list, auth, variation_list, price_list, content_list

 	price_list=[]
 	content_list=[]
 	variation_list=[]
 	product_id_list=[]
 	auth=util.get_random_string('google_token',86)

 	print 'lat and lon generator'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	try:
		json_lat_lon_result=json.loads(lat_lon_result.get('data')).get('geo')
		app_data['city']=json_lat_lon_result.get('city').encode('utf-8')
		app_data['pincode']=json_lat_lon_result.get('postal_code')

		if not app_data.get('city'):
			app_data['city']='noida'

		if not app_data.get('pincode'):
			app_data['pincode'] = '201301'

	except:
		print 'Exception'
		app_data['city']='noida'
		app_data['pincode']='201301'


	time.sleep(random.randint(2,3))	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	util.execute_request(**request)

	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	response = util.execute_request(**request)


	time.sleep(random.randint(25,40))
	request=search_selfCall( campaign_data, device_data, app_data )
	response=util.execute_request(**request)
	try:
		output=json.loads(response.get('data')).get('data').get('products')

		for i in range(len(output)):
			if output[i].get('productId'):
				product_id_list.append(output[i].get('productId'))

		if len(product_id_list)<1:
			product_id_list=[2321, 4859, 27367, 50342, 8803, 40094, 50343, 25870, 65425, 51126]

	except:
		print 'Exception-------------'
		product_id_list=[2321, 4859, 27367, 50342, 8803, 40094, 50343, 25870, 65425, 51126]

	time.sleep(random.randint(2,3))
	request=variant_selfCall(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	try:
		output=json.loads(response.get('data')).get('data').get('products')

		for i in range(len(output)):
			if output[i].get('productId') and output[i].get('name') and output[i].get('mrpDecimal'):
				variation_list.append(output[i].get('productId'))
				content_list.append(str(output[i].get('name').encode('utf-8')))
				price_list.append(str(output[i].get('mrpDecimal')))

		if len(variation_list)<1 or len(content_list)<1 or len(price_list)<1:
			variation_list=[6729, 219495, 34122]
			content_list=['AMANTREL 100MG CAP 15`S', "AMANTREL TAB 15'S", 'AMANTREL 100MG CAP 10`S']
			price_list=['183.70', '167.00', '55.00']

	except:
		print 'Exception-------------'
		variation_list=[6729, 219495, 34122]
		content_list=['AMANTREL 100MG CAP 15`S', "AMANTREL TAB 15'S", 'AMANTREL 100MG CAP 10`S']
		price_list=['183.70', '167.00', '55.00']


	for _ in range(1, random.randint(2,4)):

		if random.randint(1,100) <= 10:
			time.sleep(random.randint(4,6))
			af_add_to_cart(campaign_data, app_data, device_data)

		if random.randint(1,100) <= 8:
			time.sleep(random.randint(1,2))
			upload_rx(campaign_data, app_data, device_data)

		if random.randint(1,100) <= 60 and not app_data.get('registered'):
			app_data['app_user_id']='33'+util.get_random_string('decimal',5)
			time.sleep(random.randint(20,30))
			af_complete_registration(campaign_data, app_data, device_data)
			app_data['registered'] = True

		if random.randint(1,100) <= 8:
			time.sleep(random.randint(20,40))
			af_content_view_medicine(campaign_data, app_data, device_data)
		
		
		# Diagnostics block starts here

		if random.randint(1,100) <= 30:

			time.sleep(random.randint(20,40))
			af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_landing")
			af_d_landing_diagnostics(campaign_data, app_data, device_data)

			if random.randint(1,100) <= 16:

				time.sleep(random.randint(10,30))
				af_d_package_pdp(campaign_data, app_data, device_data)
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_package_pdp")

			if random.randint(1,100) <= 16:

				time.sleep(random.randint(10,30))
				af_d_test_pdp(campaign_data, app_data, device_data)
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_test_pdp")

			if random.randint(1,100) <= 10:

				time.sleep(random.randint(10,30))
				af_d_all_tests(campaign_data, app_data, device_data)
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_all_tests")


			if random.randint(1,100) <= 30:

				time.sleep(random.randint(15,40))
				print '\n'+'Appsflyer : Track____________________________________'
				request  = appsflyer_track(campaign_data, app_data, device_data, isOpen=False)
				util.execute_request(**request)


			if random.randint(1,100) <= 10:

				time.sleep(random.randint(10,30))
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_all_packages")
				af_d_all_packages(campaign_data, app_data, device_data)

			if random.randint(1,100) <= 16:

				time.sleep(random.randint(4*60,5*60))
				af_d_atc(campaign_data, app_data, device_data)
				af_add_to_cart_diagnostics(campaign_data, app_data, device_data, screen = "d_ATC")

			if random.randint(1,100) <= 16:

				time.sleep(random.randint(10,40))
				af_view_cart(campaign_data, app_data, device_data)
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_view_cart")

			if random.randint(1,100) <= 16:

				time.sleep(random.randint(20,40))
				af_d_all_labs(campaign_data, app_data, device_data)
				af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_all_labs")

				if random.randint(1,100) <= 83:

					time.sleep(random.randint(40,70))
					af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_patient_detail")

					if random.randint(1,100) <= 80:

						time.sleep(random.randint(40,70))
						af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_slot_select")
						af_d_slot_selection(campaign_data, app_data, device_data)

						if random.randint(1,100) <= 75:

							time.sleep(random.randint(40,70))
							af_d_payment(campaign_data, app_data, device_data)
							af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_payment")

							if random.randint(1,100) <= 66 and app_data.get('registered'):

								if not app_data.get('app_user_id'):
									app_data['app_user_id'] = '33'+util.get_random_string('decimal',5)

								time.sleep(random.randint(40,70))
								af_d_purchase(campaign_data, app_data, device_data)

			if random.randint(1,100) <= 4:
				time.sleep(random.randint(40,70))
				af_d_call_to_book(campaign_data, app_data, device_data)


		if random.randint(1,100) <= 30:

			time.sleep(random.randint(15,40))
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data, isOpen=False)
			util.execute_request(**request)


		# Health care products starts here

		if random.randint(1,100) <= 10:

			time.sleep(random.randint(60,90))
			af_content_view_otc(campaign_data, app_data, device_data)

			if random.randint(1,100) <= 50:

				time.sleep(random.randint(30,60))
				af_add_to_cart_otc(campaign_data, app_data, device_data)


	return {'status':'true'}

def measurement(campaign_data, app_data, device_data):
	url= "http://app-measurement.com/config/app/1%3A477737843689%3Aandroid%3A0cf0b42e812d7d86"
	method= "head"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent":  get_ua(device_data),}

	params= {       "app_instance_id": "3de05bbe4a713e3b0207398d4d364194",
        "gmp_version": "17785",
        "platform": "android"}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def gcmToken_call(campaign_data, app_data, device_data):
	url 	= 'http://android.clients.google.com/c2dm/register3'
	method 	= 'post'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent'	  : 'Android-GCM/1.5 ('+device_data.get('product')+' '+device_data.get('build')+')',
		'Content-Type'	  : 'application/x-www-form-urlencoded',
		'app'			  : campaign_data.get('package_name'),
		'gcm_ver'		  : '17785019',
		'Authorization'	  : 'AidLogin 4581808680700430854:4641510709115399361',
		}
	params 	= None
	data 	= {
			'X-subtype':'477737843689',
			'sender':'477737843689',
			'X-app_ver':campaign_data.get('app_version_code'),
			'X-osv':device_data.get('sdk'),
			'X-cliv':'fiid-'+util.get_random_string('decimal',8),
			'X-gmsv':'17785019',
			'X-appid':util.get_random_string('char_all',11),
			'X-scope':'*',
			'X-gmp_app_id':'1:477737843689:android:'+device_data.get('android_id'),
			'X-app_ver_name':campaign_data.get('app_version_name'),
			'app':campaign_data.get('package_name'),
			'device':'4581808680700430854',
			'cert':util.get_random_string('hex',40),
			'app_ver':campaign_data.get('app_version_code'),
			'info':util.get_random_string('all',31),
			'gcm_ver':'17785019',
			'plat':'0',
			'target_ver':27,
			'X-Firebase-Client':'fire-android/ fire-core/16.1.0'
			# 'X-google.message_id':'google.rpc1'
	}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def current_location(device_data):
	url='http://lumtest.com/myip.json'
	header={
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
			'Accept-Encoding': 'gzip, deflate',
			'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+','+device_data.get('locale').get('language')+';q=0.9',
			'Cache-Control': 'max-age=0',
			'Upgrade-Insecure-Requests': '1',
			'User-Agent': device_data.get('User-Agent')
	}
	params={}
	data={}
		
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}


def search_selfCall( campaign_data, device_data, app_data ):
	url= "http://api.pharmeasy.in/v3/ecommerce/omni-search/search-with-fulfilment"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent": "okhttp/3.10.0",
        "X-Advertising-Id": device_data.get('adid'),
        "X-Api-Auth": auth,
        "X-App-Version": campaign_data.get('app_version_name'),
        "X-Appsflyer-Id": app_data.get('uid'),
        "X-Default-City": str(random.randint(1,6)), #"6",
        "X-Device-Id": device_data.get('android_id'),
        "X-Os-Version": device_data.get('os_version'),
        "X-Phone-Manufacturer": device_data.get('manufacturer')+" "+device_data.get('model'),
        "X-Phone-Platform": "android",
        "X-Pincode": str(app_data.get('pincode'))}

	params= {       "q": util.get_random_string('char',2)}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def variant_selfCall(campaign_data, app_data, device_data):
	url= "http://api.pharmeasy.in/v3/ecommerce/omni-search/variants-with-fulfilment"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent": "okhttp/3.10.0",
        "X-Advertising-Id": device_data.get('adid'),
        "X-Api-Auth": auth,
        "X-App-Version": "4.8.11",
        "X-Appsflyer-Id": campaign_data.get('app_version_name'),
        "X-Default-City": "6",
        "X-Device-Id": device_data.get('android_id'),
        "X-Os-Version": device_data.get('os_version'),
        "X-Phone-Manufacturer": device_data.get('manufacturer')+" "+device_data.get('model'),
        "X-Phone-Platform": "android",
        "X-Pincode": str(app_data.get('pincode'))}

	params= {       "itemId": random.choice(product_id_list), "itemType": "1"}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def af_d_all_tests(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_all_tests'
	eventName			= 'af_d_all_tests'
	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+app_data.get('city').lower()+'","af_user_type":false,"af_screen":"d_all_tests"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_content_view(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_content_view'
	eventName			= 'af_content_view'
	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+app_data.get('city').lower()+'","af_user_type":false,"af_screen":"d_all_tests"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_complete_registration(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_complete_registration'
	eventName			= 'af_complete_registration'
	eventValue			= '{"af_city":"'+app_data.get('city').lower()+'","af_customer_user_id":"'+app_data.get('app_user_id')+'"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_add_to_cart(campaign_data, app_data, device_data):

	try:
		
		app_data['index']=random.randint(0, len(variation_list)-1)
		product=variation_list[app_data.get('index')]
		price=price_list[app_data.get('index')]
		content=content_list[app_data.get('index')]

	except:
		print 'Exception'
		product=6729
		price=183.70
		content='AMANTREL 100MG CAP 15`S'

	print 'Appsflyer : EVENT___________________________af_add_to_cart'
	eventName			= 'af_add_to_cart'
	eventValue			=  json.dumps({"af_content_type":"medicine","af_price":str(price),"af_content_id":int(product),"af_city":app_data.get('city').lower(),"af_content":str(content.lower())})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def af_content_view_medicine(campaign_data, app_data, device_data):

	try:
		
		app_data['index']=random.randint(0, len(variation_list)-1)
		product=variation_list[app_data.get('index')]
		price=price_list[app_data.get('index')]
		content=content_list[app_data.get('index')]

	except:
		print 'Exception'
		product=6729
		price=183.70
		content='AMANTREL 100MG CAP 15`S'

	print 'Appsflyer : EVENT___________________________af_content_view'
	eventName			= 'af_content_view'
	eventValue			=  json.dumps({"af_content_type":"medicine","af_price":str(price),"af_content_id":int(product),"af_city":app_data.get('city').lower(),"af_content":str(content.lower())})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)	


def af_content_view_otc(campaign_data, app_data, device_data):

	try:
		app_data['index']=random.randint(0, len(variation_list)-1)
		product=variation_list[app_data.get('index')]
		price=price_list[app_data.get('index')]
		content=content_list[app_data.get('index')]

	except:
		print 'Exception'
		product=6729
		price=183.70
		content='AMANTREL 100MG CAP 15`S'

	print 'Appsflyer : EVENT___________________________af_content_view'
	eventName			= 'af_content_view'
	eventValue			= '{"af_content_type":"otc","af_price":null,"af_content_id":'+ str(product) +',"af_city":"'+ app_data.get('city').lower() +'","af_content":"'+ str(content.lower()) +'"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_add_to_cart_otc(campaign_data, app_data, device_data):
	try:

		if not app_data.get('index'):
			app_data['index']=random.randint(0, len(variation_list)-1)
		product=variation_list[app_data.get('index')]
		price=price_list[app_data.get('index')]
		content=content_list[app_data.get('index')]

	except:
		print 'Exception'
		product=6729
		price=183.70
		content='AMANTREL 100MG CAP 15`S'

	print 'Appsflyer : EVENT___________________________af_add_to_cart'
	eventName			= 'af_add_to_cart'
	eventValue			= '{"af_content_type":"otc","af_price":null,"af_content_id":'+ str(product) +',"af_city":"'+ app_data.get('city').lower() +'","af_content":"'+ str(content.lower()) +'"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def upload_rx(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________upload_rx'
	eventName			= 'upload_rx'
	eventValue			= '{"city_name":"'+app_data.get('city').lower()+'"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_content_view_diagnostics(campaign_data, app_data, device_data, screen = "d_landing"):
	print 'Appsflyer : EVENT___________________________af_content_view'
	eventName			= 'af_content_view'
	eventValue			= {"af_content_type":"diagnostics","af_city":app_data.get('city').lower(),"af_user_type":False,"af_screen":screen}

	if screen == "d_package_pdp":

		eventValue["af_content"] = globals()["package_data"].get("af_content")
		eventValue["af_content_id"] = globals()["package_data"].get("af_content_id")
		eventValue["af_price"] = globals()["package_data"].get("af_price")

	elif screen == "d_test_pdp":

		eventValue["af_content"] = globals()["test_data"].get("af_content")
		eventValue["af_content_id"] = globals()["test_data"].get("af_content_id")
		eventValue["af_price"] = globals()["test_data"].get("af_price")

	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)

def af_add_to_cart_diagnostics(campaign_data, app_data, device_data, screen = "d_ATC"):
	print 'Appsflyer : EVENT___________________________af_add_to_cart'
	eventName			= 'af_add_to_cart'
	eventValue			= {"af_content_type":"diagnostics","af_city":app_data.get('city').lower(),"af_user_type":False,"af_screen":screen}
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)

def af_d_landing_diagnostics(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_landing'
	eventName			= 'af_d_landing'
	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+ app_data.get('city').lower() +'","af_user_type":false,"af_screen":"d_landing"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_d_package_pdp(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_package_pdp'
	eventName			= 'af_d_package_pdp'

	eventValue			= {"af_content_type":"diagnostics","af_city":app_data.get('city').lower(),"af_user_type":False,"af_screen":"d_package_pdp"}

	data = random.choice([	{"af_price":"600","af_content_id":389,"af_content":"Essential Vitamin Package"},
								{"af_price":"699","af_content_id":392,"af_content":"Basic Health Care (51 Parameters)"},
								{"af_price":"400","af_content_id":383,"af_content":"Diabetes Care"}		])

	eventValue["af_content"] = data.get("af_content")
	eventValue["af_content_id"] = data.get("af_content_id")
	eventValue["af_price"] = data.get("af_price")

	globals()["package_data"] = data

	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)


def af_d_test_pdp(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_test_pdp'
	eventName			= 'af_d_test_pdp'

	eventValue			= {"af_content_type":"diagnostics","af_city":app_data.get('city').lower(),"af_user_type":False,"af_screen":"d_test_pdp"}

	data = random.choice([	{"af_price":"280","af_content_id":86,"af_content":"Hba1C (Glycosylated hemoglobin)"},
								{"af_price":"270","af_content_id":2,"af_content":"Lipid Profile"},
								{"af_price":"220","af_content_id":5,"af_content":"Thyroid Profile (T3+T4+TSH)"},
								{"af_price":"120","af_content_id":6,"af_content":"Alkaline phosphatase (ALP)"},
								{"af_price":"150","af_content_id":44,"af_content":"CBC (Complete Blood Count)"}		])

	eventValue["af_content"] = data.get("af_content")
	eventValue["af_content_id"] = data.get("af_content_id")
	eventValue["af_price"] = data.get("af_price")

	globals()["test_data"] = data

	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=json.dumps(eventValue))
	util.execute_request(**request)


def af_d_all_packages(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_all_packages'
	eventName			= 'af_d_all_packages'
	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+ app_data.get('city').lower() +'","af_user_type":false,"af_screen":"d_all_packages"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_d_atc(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_atc'
	eventName			= 'af_d_atc'
	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+ app_data.get('city').lower() +'","af_user_type":false,"af_screen":"d_ATC"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_view_cart(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_view_cart'
	eventName			= 'af_view_cart'
	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+ app_data.get('city').lower() +'","af_user_type":false,"af_screen":"d_view_cart"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_d_all_labs(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_all_labs'
	eventName			= 'af_d_all_labs'
	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+ app_data.get('city').lower() +'","af_user_type":false,"af_screen":"d_all_labs"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_d_slot_selection(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_slot_selection'
	eventName			= 'af_d_slot_selection'
	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+ app_data.get('city').lower() +'","af_user_type":false,"af_screen":"d_slot_select"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_d_payment(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_payment'
	eventName			= 'af_d_payment'
	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+ app_data.get('city').lower() +'","af_user_type":false,"af_screen":"d_payment"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_d_purchase(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_purchase'
	eventName			= 'af_d_purchase'

	timing = random.choice(["09:00:00 10:00:00", "10:00:00 11:00:00", "11:00:00 12:00:00"])
	appointment_timestamp = datetime.datetime.utcfromtimestamp(time.time()+86400).strftime('%Y-%m-%d') + ' ' + timing

	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+ app_data.get('city').lower() +'","af_user_type":false,"af_customer_user_id":"'+app_data.get('app_user_id')+'","appointment_timestamp":"'+ appointment_timestamp +'","is_rx_upload":true,"af_order_id":"32'+ util.get_random_string('decimal',3) +'"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_d_call_to_book(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_d_call_to_book'
	eventName			= 'af_d_call_to_book'
	eventValue			= '{"af_content_type":"diagnostics","af_city":"'+ app_data.get('city').lower() +'","af_user_type":false,"af_screen":"d_call_to_book"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		'af_gcm_token' : app_data.get('af_gcm_token'),
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"appUserId" : "14dfd1ae-e709-4f92-998e-81bab31d3ad3",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "no",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"sensors" : [{u'sN': u'MAGNETOMETER', u'sVE': [-17.584, -2.778, -76.887], u'sV': u'MTK', u'sVS': [-19.684, -4.463, -77.762], u'sT': 2}, {u'sN': u'ACCELEROMETER', u'sVE': [-0.318, -0.451, 9.706], u'sV': u'MTK', u'sVS': [-0.342, -0.472, 9.72], u'sT': 1}, {u'sN': u'GYROSCOPE', u'sVE': [0, 0, 0], u'sV': u'MTK', u'sVS': [0, 0, 0], u'sT': 4}],
		},
		# "deviceRm" : device_data.get('build'),
		"deviceType" : "user",
		"extraReferrers" : json.dumps({urllib.unquote(app_data.get('referrer')):"["+app_data.get('extra_referrer_time')+"]"})  if app_data.get('referrer') else None ,
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"open_referrer" : 'android-app://com.android.vending',
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"rfr" : {
								"clk" : str(int(app_data.get("times").get("click_time"))),
								"code" : "0",
								"install" : str(int(app_data.get("times").get("download_begin_time"))),
								"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",
		},
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),
		"tokenRefreshConfigured": False,
		'p_receipt' : util.get_random_string(type='all',size=640)+"==",
		'sc_o':'p',
		}

		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

	if app_data.get('counter')>2:
		del data['rfr']
		del data['deviceData']['sensors']
		del data['p_receipt']

	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))	
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}



###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"device_id" : app_data.get('uid'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data, call=1):
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
 	def_(app_data,'counter')
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"app_name" : campaign_data.get('app_name'),
		"appUserId" : "14dfd1ae-e709-4f92-998e-81bab31d3ad3",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand').upper(),
		"carrier" : device_data.get('carrier'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),
		}

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_afGoogleInstanceID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"appUserId" : "14dfd1ae-e709-4f92-998e-81bab31d3ad3",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		# "deviceRm" : device_data.get('build'),
		"deviceType" : "user",
		"extraReferrers" : json.dumps({urllib.unquote(app_data.get('referrer')):"["+app_data.get('extra_referrer_time')+"]"})  if app_data.get('referrer') else None ,
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : True,
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),
		'sc_o':'p',
		"tokenRefreshConfigured": False,
		}

	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################
def change_char(s, p, r):
	return s[:p]+r+s[p+1:]
		
def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100

	if n3<10:
		n3 = '0'+str(n3)
		
	sb = insert_char(sb,23,str(n3))
	return sb



###################################################################
# appsflyer_kef_key and value generator
# parameter 			: brand, sdk, lang, af_ts, buildnumber, etc
#
# Generates kef key and value for appsflyer.
###################################################################
global temp, counter
temp = 0
counter = 1

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
	if buildnumber=="4.8.18":
		return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
	else:
		c = '\u0000'
		obj2 = af_ts[::-1]
		out = calculate(sdk, obj2, brand)
		i = len(out)
		if i > 4:
			i2 = globals()['counter']+65
			globals()['temp'] = i2 % 128
			if (1 if i2 % 2 != 0 else None) != 1:
				out.delete(4, i)
			else:
				out.delete(5, i)
		else:
			while i < 4:
				i3 = globals()['counter'] + 119
				globals()['temp'] = i3 % 128
				if (16 if i3 % 2 != 0 else 93) != 16:
					i += 1
					out+='1'
				else:
					i += 66
					out+='H'
				i3 = globals()['counter'] + 109
				globals()['temp'] = i3 % 128
				i3 %= 2
		return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
	ts = ts[::-1]
	appends = [sdk,lang,ts]
	lengths = [len(sdk),len(lang),len(ts)]
	l = sorted(lengths)
	least = l[0]
	out = ""
	for x in range(least):
		numb = None
		for y in range(len(appends)):
			charAt = ord(appends[y][x])
			if numb:
				charAt = int((charAt ^ int(numb)))

			numb = charAt
			
		out+=str(hex(numb))[2:]

	if len(out)>4:
		out = out[:4]
	elif len(out)<4:
		while len(out)<4:
			out+="1"
	
	return out

"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""

def generateValue(b,x,s,p,ts,fl,buildnumber):
	part1=generateValue1get(ts,fl,buildnumber)
	str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, 'utf-8')
	for i in range(len(str_)):
		str_[i] = int((str_[i] ^ ((i % 2) + 42)))

	stringBuilder = ""
	for toHexString in str_:
		toHexString2 = str(hex(int(toHexString)))[2:]
		if 1 == len(toHexString2):
			toHexString2 = "0"+str(toHexString2)
		stringBuilder+=toHexString2
	part2 = stringBuilder.__str__()
	return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
	gethash = sha256(ts+firstLaunch+buildnumber)
	return gethash[:16]

"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""

def calculate(sdk, obj, brand):
	allList = [sdk, obj, brand]
	arrayList = []
	i = 0
	while True:
		flag = 1
		if i >= 3:
			break
		i2 = globals()['temp'] + 63
		globals()['counter'] = i2 % 128
		if i2 % 2 != 0:
			flag = None
		if flag != None:
			arrayList.append(len(allList[i]))
			i += 31
		else:
			arrayList.append(len(allList[i]))
			i += 1
	sorted(arrayList)
	intValue = int(arrayList[0])
	out = ""
	i3 = 0
	while i3 < intValue:
		i4 = globals()['counter']+99
		globals()['temp'] = i4 % 128
		i5 =  globals()['temp']+67
		globals()['counter'] = i5 % 128
		i5 %= 2
		number = None
		if i4 % 2 != 0:
			a = 57
		else:
			a = 83
		if a != 83:
			i4 = 1
		else:
			i4 = 0
		while i4 < 3:
			i5 = globals()['temp'] + 87
			globals()['counter'] = i5 % 128
			i5 %= 2
			i5 = ord(allList[i4][i3])
			if (92 if number == None else 39) != 39:
				i6 = globals()['temp'] + 55
				globals()['counter'] = i6 % 128
				i6 %= 2
				i6 = globals()['counter'] + 39
				globals()['temp'] = i6 % 128
				i6 %= 2
			else:
				i5 ^= int(number)
			number = i5
			i4 += 1
		out+=str(hex(int(number)))[2:]
		i3 += 1
	return out



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def ref_time():
	time.sleep(random.randint(10,15))
	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"