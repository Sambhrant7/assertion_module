import random,time,datetime,uuid,json
from sdk import NameLists
from sdk import getsleep 
from sdk import installtimenew
from sdk import util
import urllib
import clicker,Config

campaign_data = {
	'package_name' : 'ru.tutu.TransportApp',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'ctr':6,
	'app_name' : 'Tutu.ru: flights, railway, bus',
	'app_id':'1296791829',
	'app_version_code': '13',#'1',#'449',
	'app_version_name': '3.13.0',#'2.7.0',#'1.12',
	'supported_os':'7.0',
	'app_size' : 215.3,
	'supported_countries': 'WW',
	'device_targeting':True,
	'sdk' : 'ios',
	'tracker':'adjust',
	'adjust':{
				'app_token': 'aunkeowcem0w',
				'app_secret': '96530831017190747913583574732104157358',
				'sdk': 'ios4.17.3',#'ios4.14.0',
				'app_updated_at': '2019-09-04T16:23:29.000Z',#'2018-09-08T22:36:04.000Z',
				'signature_id':'1'
			},
	'retention':{
					1:71,
					2:69,
					3:67,
					4:65,
					5:63,
					6:61,
					7:57,
					8:55,
					9:53,
					10:51,
					11:48,
					12:45,
					13:42,
					14:39,
					15:37,
					16:35,
					17:33,
					18:32,
					19:31,
					20:20,
					21:19,
					22:18,
					23:17,
					24:16,
					25:15,
					26:14,
					27:13,
					28:12,
					29:11,
					30:10,
					31:9,
					32:8,
					33:7,
					34:6,
					35:5,
	}
}


def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	
	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')
		
def make_subsession_count(app_data,device_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 1
	else:
		app_data['subsession_count'] += 1
		
	return app_data.get('subsession_count')

def get_install_reciept(app_data):
	install_receipt= "MIISgwYJKoZIhvcNAQcCoIISdDCCEnACAQExCzAJBgUrDgMCGgUAMIICJAYJKoZIhvcNAQcBoIICFQSCAhExggINMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEDAgEBBAQMAjEzMAwCAQoCAQEEBBYCNCswDAIBDgIBAQQEAgIAjTAMAgETAgEBBAQMAjEzMA0CAQsCAQEEBQIDCn20MA0CAQ0CAQEEBQIDAdWIMA4CAQECAQEEBgIETUt5FTAOAgEJAgEBBAYCBFAyNTMwDgIBEAIBAQQGAgQxn1e0MBACAQ8CAQEECAIGRhFAGbTMMBQCAQACAQEEDAwKUHJvZHVjdGlvbjAYAgEEAgECBBCFdfRnzINhU3ULAbB3530cMBwCAQUCAQEEFNiOOBJO2xNOXX5T+lOUoS9X+DLfMB4CAQICAQEEFgwUcnUudHV0dS5UcmFuc3BvcnRBcHAwHgIBCAIBAQQWFhQyMDE5LTA5LTE4VDA5OjU3OjA3WjAeAgEMAgEBBBYWFDIwMTktMDktMThUMDk6NTc6MDdaMB4CARICAQEEFhYUMjAxOS0wOS0xOFQwOTo1NzowN1owNQIBBwIBAQQtenANg1eSKr7Rv9qm8ZmPChFi71pqJIy0w/KYSAr/8ASVXVo+npoVDm3GgbjNMFUCAQYCAQEETR4v5uzJ8wyFW5Ik8Y7T6F5tH1QpS39iEEDsnVIMG/kTCx6IHTSAIxDeAXqzvxLGVkyDjq54L6o/YVyR57ZcUP4w3CwlbtaIpgX5aDNooIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQAAwyAubJZwIQwByZDyLozU7DmYDm/p2kDCZdvL38YcsK5BG8aILUgdMXuEoasIP8Kni7fgRZ1chC03d03rx4UVvX3gbxs14i/z//KPhHtrmWmdGwkvBrov6F8WFETfqZE6lHypgvxU6jOajKBEq6aVJt/IJjluSE8tqsShREQvrjiMnTe7Ew6ClwG9cmcDjjVnbREVBpju5vwLGiMx8U0jVCwyidP2MW1dYucqwZKtliPhp0DaBkKokAuaf8fGUiZ/TLeH6G2OHbCSU4jEAZBWgIH/zIPmB1djn3a6sHHKglzvT2hG9Vmoew+lE9NWskQDJR1Pbu2pJdR1C0R6ncFz"
	return install_receipt

def install(app_data, device_data):

	print 'plz wait installing ........'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')	
	
	get_install_reciept(app_data)

	print '*************Install**********************'

	###########		Initialize		############
	
	app_data['adjust_call_time']=int(time.time())
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)


	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time']=time.time()
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	time.sleep(random.randint(1,3))

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,t1=9,t2=11)
	util.execute_request(**adjustSession)
	
	#event_y55tbz(campaign_data, app_data, device_data)		// token present in new app but not in install complete and open complete logs and smali is not possible. Thats why it is commented.
	
	for _ in range(random.randint(1,3)):

		time.sleep(random.randint(0,5*60))

		if random.randint(1,100) <= 55:

			time.sleep(random.randint(15,20))
			event_25da8n(campaign_data, app_data, device_data,t1=0,t2=0)

			time.sleep(random.randint(10,15))
			event_owfor9(campaign_data, app_data, device_data,t1=0,t2=0)

		if random.randint(1,100) <= 50:

			time.sleep(random.randint(10,12))
			event_apaheb(campaign_data, app_data, device_data,t1=0,t2=0)

		time.sleep(random.randint(0,6*60))


	set_appCloseTime(app_data)

	return {'status':True}


###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):


	print 'plz wait ........'
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')	
	
	
	get_install_reciept(app_data)

	###########		Initialize		############


	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())
		
	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)


	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data, is_type = 'open')
	util.execute_request(**adjustSession)	

	app_data['adjust_call_time']=int(time.time())


	for _ in range(random.randint(1,3)):

		time.sleep(random.randint(0,5*60))

		if random.randint(1,100) <= 50:

			time.sleep(random.randint(15,20))
			event_25da8n(campaign_data, app_data, device_data,t1=0,t2=0)

			time.sleep(random.randint(10,15))
			event_owfor9(campaign_data, app_data, device_data,t1=0,t2=0)


		if random.randint(1,100) <= 45:

			time.sleep(random.randint(10,12))
			event_apaheb(campaign_data, app_data, device_data,t1=0,t2=0)

		time.sleep(random.randint(0,6*60))



	set_appCloseTime(app_data)

	return {'status':True}




def event_y55tbz(campaign_data, app_data, device_data,t1=0,t2=0):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'y55tbz',t1=t1,t2=t2)  
	util.execute_request(**adjust_event_req)

def event_owfor9(campaign_data, app_data, device_data,t1=0,t2=0):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'owfor9',t1=t1,t2=t2)  
	util.execute_request(**adjust_event_req)

def event_apaheb(campaign_data, app_data, device_data,t1=0,t2=0):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'apaheb',t1=t1,t2=t2)  
	util.execute_request(**adjust_event_req)

def event_25da8n(campaign_data, app_data, device_data,t1=0,t2=0):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'25da8n',t1=t1,t2=t2)  
	util.execute_request(**adjust_event_req)


###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0, is_type='install'):
	pushtoken(app_data)
	app_data['subsession_count'] = 0
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': '$'+urllib.quote('(PRODUCT_NAME)')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'connectivity_type':'2',
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': created_at,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'sent_at': sent_at,
			'session_count': make_session_count(app_data,device_data),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at': get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
			'os_build':	device_data.get('build'),
			'install_receipt':get_install_reciept(app_data),
			}

	if is_type == "open":		
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = app_data.get('appCloseTime') - app_data.get('adjust_call_time')
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = app_data.get('appCloseTime') - app_data.get('adjust_call_time')

	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')
		
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	



def adjust_sdkclick(campaign_data, app_data, device_data,t1=0,t2=0,callback_params=False,partner_params=False):
	pushtoken(app_data)
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent':'$'+urllib.quote('(PRODUCT_NAME)')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	
	params={}

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	data = {
			'details': '{"Version3.1":{"iad-attribution":"false"}}',
			'source': 'iad3',
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'connectivity_type':'2',
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': created_at,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'sent_at': sent_at,
			'session_count': app_data.get('session_count'),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at': get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
			'os_build':	device_data.get('build'),
			'last_interval': 0,
			'session_length': int(time.time()) - app_data.get('adjust_call_time'),
			'subsession_count': 1,
			'time_spent' : int(time.time()) - app_data.get('adjust_call_time'),
			'install_receipt': get_install_reciept(app_data),
			}

	if callback_params:
		data['callback_params']=callback_params
	if partner_params:
		data['partner_params']=partner_params
	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'click')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	

def adjust_attribution(campaign_data, app_data, device_data,t1=0,t2=0,):
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'User-Agent': '$'+urllib.quote('(PRODUCT_NAME)')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	
	data={}
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	params = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			"app_version": campaign_data.get('app_version_code'),
			"app_version_short": campaign_data.get('app_version_name'),
			'attribution_deeplink' : 1,
			"bundle_id": campaign_data.get('package_name'),
			'created_at': created_at,
			"device_name": device_data.get('device_platform'),
			"device_type": device_data.get('device_type'),
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'initiated_by':'backend',
			'needs_response_details': 1,
			"os_build": device_data.get('build'),
			"os_name": "ios",
			'os_version' : device_data.get('os_version'),
			"persistent_ios_uuid": app_data.get('ios_uuid'),
			'sent_at': sent_at,
			}

	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('idfa'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	


def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=False, partner_params=False, purchase=False,t1=0,t2=0):
	
	created_at_event=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at_event=get_date(app_data,device_data)

	def_sessionLength(app_data)
	def_(app_data,'subsession_count')
	if not app_data.get('device_token'):	
		app_data['device_token'] = util.get_random_string("all",22)	
		
	url = 'http://app.adjust.com/event'
	method = 'post'
	headers = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':'$'+urllib.quote('(PRODUCT_NAME)')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1


	data = {
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'hardware_name':'D101AP',	
			'event_buffering_enabled':	0,
			'cpu_type':device_data.get('cpu_type'),
			'attribution_deeplink': '1',
			'connectivity_type':'2',
			'os_name':'ios',
			'environment':'production',
			'needs_response_details':1,
			'event_count':app_data.get('event_count'),
			'time_spent':app_data.get('sessionLength'),
			'session_count':str(app_data.get('session_count')),
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			'created_at': created_at_event,
			'event_token':event_token,
			'bundle_id':campaign_data.get('package_name'),
			'subsession_count':app_data.get('subsession_count'),
			'os_version':device_data.get('os_version'),
			'app_version': campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country'),
			'language':	device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'session_length':app_data.get('sessionLength'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': '1',
			'device_name':device_data.get('device_platform'),
			'sent_at': sent_at_event,
			'os_build':	device_data.get('build'),
			'install_receipt': get_install_reciept(app_data),
			}

	if callback_params:
		data['callback_params']=callback_params
	if partner_params:
		data['partner_params']=partner_params	
	
	data['time_spent']= int(time.time()) - app_data.get('adjust_call_time')
	data['session_length']=data['time_spent']
	
	if data['event_count']==1:
		app_data['subsession_count']=1
		data['subsession_count']=app_data.get('subsession_count')
	elif data['event_count']==3:
		app_data['subsession_count']+=1
		data['subsession_count']=app_data.get('subsession_count')
		
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'event')

	return {'url': url, 'httpmethod': 'post', 'headers': headers, 'params':None, 'data': data}	



###########################################################
#						UTIL							  #
###########################################################

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)
	
def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('decimal',128)

def device_id(app_data):
	if not app_data.get('x-device-id'):
		app_data['x-device-id']=str(uuid.uuid4()).upper()

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['phone'] 		="+971"+'-50-'+util.get_random_string('decimal',7)
		app_data['user']['password'] 	= app_data['user']['lastname']+random.choice(["@","!"])+app_data['user']['dob'].replace("-","") 

def get_auth(device_data,app_data,created_at,idfa,activity_type):
	data1= str(campaign_data['adjust']['app_secret']+activity_type+idfa+created_at)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('signature_id')+'",signature="'+sign+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'	

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date