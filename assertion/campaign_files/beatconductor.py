# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
import urllib
import random
import time
import hashlib
import json
import base64
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
				'package_name' : 'co.taxibeat.driver',
				'sdk' : 'Android',
				'app_version_name' :'12.10',#'12.9.1',#'12.9',#'12.8',#'12.7',#12.6, '12.5',#'12.4', 
				'app_name' : 'Driver',
				'supported_countries': 'WW',
				'device_targeting':True,
				'no_referrer': False,
				'CREATE_DEVICE_MODE' : 3,
				'tracker':'singular',
				'ctr':6,
				'supported_os':'4.4',
				'app_size' : 38.0,#37.0,#38.0,
				'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
				'singular':{
					'version':'Singular/v9.0.5.PROD',
					'key':'taxibeat_3e4971d2',
					'secret':'525b7d53d078045f827c1956b8991687',
				},
				'api_branch':{
							    'branchkey'  :'key_live_ioFVP8JWdDz1h08wXEBuQpccFAm88oLC',
							    'sdk'        :'android3.1.0' 
							},	
				'retention':{
							1:70,
							2:68,
							3:66,
							4:64,
							5:61,
							6:59,
							7:57,
							8:52,
							9:50,
							10:47,
							11:45,
							12:43,
							13:40,
							14:37,
							15:35,
							16:31,
							17:30,
							18:28,
							19:26,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
				}
}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	
	###########		INITIALIZE		############	

	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android")

	#####################################

	global start_s
	start_s = str(int(app_data.get('times').get('install_complete_time')*1000))	

	app_data['seq'] = 1

 	###########	 	CALLS		 ########

 	time.sleep(random.randint(1,2))
 	print '\n APSALAR Resolve \n'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**apsalar_Resolve)


	time.sleep(random.randint(1,2))
	print '\n APSALAR START \n'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data,lag='0.0'+str(random.randint(91,99)))
	util.execute_request(**apsalar_start)


	time.sleep(random.randint(1,2))
	if app_data.get('referrer'):
		print '\n APSALAR EVENT - __InstallReferrer \n'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__InstallReferrer',custom_user_id=True,value=json.dumps({"referrer":urllib.quote(app_data.get('referrer')),"referrer_source":"service","clickTimestampSeconds":int(app_data.get('times').get('click_time')),"installBeginTimestampSeconds":int(app_data.get('times').get('download_begin_time')),"current_device_time":int(time.time()*1000),"is_revenue_event":False}),seq=app_data.get('seq'))
		util.execute_request(**apsalar_event)


 	print "Branch______install"
 	request=api_branch_install(campaign_data,app_data,device_data)
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='679914042672147267'
		app_data['identity_id']='684298298576010598'
		app_data['session_id']='684298298580538105'


	time.sleep(random.randint(1,2))
	print '\n APSALAR EVENT - __LicensingStatus \n'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__LicensingStatus',custom_user_id=True,value=json.dumps({"responseCode":"0","signedData":"0|619304650|co.taxibeat.driver|113|ANlOHQO5zVza9fKHU\/vAy6i3S74Q34\/M6A==|1572586088365:VT=9223372036854775807","signature":"Z9XuFsiX0hyws+41S+MXG2TYrnHNyYzpc1cMPf5XPfa+t5Ptf3MaRzAUEbc43EIe1NCFjfTgmMF9cOndIV241BGvqq1dT8vDUf51kN+87zkdfxAhjwgB9XgfzI1yMsZLoiQdnloVRzJQy4eceqlAO1BBTMsRo9bHliivmhjC\/td5uUd2jRyKqPYF75PmHYcseJ3z9dCgfxtB0SWSSaIhf8+jrdFgxZZHSV8ORz8PcfMFEqEMEi+jVY7lf+lxGEUDeWxVj\/2cj7UbYs4+zC0Kxn7txK3hCoHb34Ul39tN+43jfxEHOdtJ9EGHsyz\/dMuSnlf+SyaEr4q+7WYridR6dw==","is_revenue_event":False}),seq=app_data.get('seq'))
	util.execute_request(**apsalar_event)

	
	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)


	return {'status':'true'}


#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	
	###########		INITIALIZE		############	

	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android")

	#####################################

	global start_s
	start_s = str(int(time.time()*1000))

	if not app_data.get('seq'):
		app_data['seq'] = 1

 	###########	 	CALLS		 ########

 	time.sleep(random.randint(1,2))
 	print '\n APSALAR Resolve \n'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data, isOpen = True)
	util.execute_request(**apsalar_Resolve)


	time.sleep(random.randint(1,2))
	print '\n APSALAR START \n'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data,lag='0.0'+str(random.randint(81,99)), isOpen = True)
	util.execute_request(**apsalar_start)

	return {'status':'true'}


#########################################
#										#
# 			AppSalar funcation 			#
#										#
#########################################

def apsalarResolve(campaign_data, app_data, device_data, isOpen = False):
	url = 'http://sdk-api-v1.singular.net/api/v1/resolve' 
	method = 'get'
	headers = {
		'User-Agent': 'Singular/SDK-v9.0.5.PROD',
		'Accept-Encoding': 'gzip'
	}

	if not app_data.get('user_id'):
		app_data['user_id'] = util.get_random_string('decimal',40).upper()


	data_dict = {
		"a":campaign_data.get('singular').get('key'),
		"c":"wifi",
		"i":campaign_data.get('package_name'),
		"k":"AIFA",
		"lag":str(generateLag()),
		"p":"Android",
		"pi":"1",
		"rt":"json",
		"u":device_data.get('adid'),
		"v":device_data.get('os_version'),
	}

	if isOpen:
		data_dict['custom_user_id'] = app_data.get('user_id')


	params = get_params(data_dict)


	
	# params ='a='+campaign_data.get('singular').get('key')+'&c=wifi&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(str(generateLag()))+'&p='+'Android'+'&pi=1&rt=json&u='+device_data.get('adid')+'&v='+device_data.get('os_version')

	# if isOpen:
	# 	params ='a='+campaign_data.get('singular').get('key')+'&c=wifi&i='+campaign_data.get('package_name')+'&custom_user_id='+ app_data.get('user_id') +'&k=AIFA&lag='+str(str(generateLag()))+'&p='+'Android'+'&pi=1&rt=json&u='+device_data.get('adid')+'&v='+device_data.get('os_version')

	app_data['apsalar_time']=time.time()

	# params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('singular').get('secret'))
	params = params+'&h='+h


	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


def apsalarStart(campaign_data, app_data, device_data,lag='', isOpen = False):

	url = "http://sdk-api-v1.singular.net/api/v1/start"
	method = 'get'
	headers = {
		'User-Agent': 'Singular/SDK-v9.0.5.PROD', 
		'Accept-Encoding': 'gzip'
	}
	
	if not app_data.get('s'):
		app_data['s'] = str(int(time.time()*1000)-random.randint(5,10))

	if isOpen:
		check = 'false'
	else:
		check = 'true'

	if not app_data.get('user_id'):
		app_data['user_id'] = util.get_random_string('decimal',40).upper()

	data_dict = {
				'a':campaign_data.get('singular').get('key'),
				'ab':device_data.get('cpu_abi',"armeabi"),
				'aifa':device_data.get('adid'),
				'av':campaign_data.get('app_version_name'),
				'br':device_data.get('brand'),
				'c':'wifi',
				'current_device_time':str(int(time.time()*1000)),
				'ddl_enabled':'false',
				'de':device_data.get('device'),
				'device_type':device_data.get('device_type'),
				'device_user_agent': get_ua(device_data) ,
				'dnt':'0',
				'i':campaign_data.get('package_name'),
				'install_time':str(int(app_data.get('times').get('download_end_time')*1000)),
				'is': check ,
				'k':'AIFA',
				'lag':str(lag),
				'lc':device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
				'ma':device_data.get('manufacturer'),
				'mo':device_data.get('model'),
				'n': urllib.quote(campaign_data.get('app_name')) ,
				'p':'Android',
				'pr':device_data.get('product'),
				'rt':'json',
				's': start_s,
				'sdk':campaign_data.get('singular').get('version'),
				'u':device_data.get('adid'),
				'update_time':str(int(app_data.get('times').get('download_end_time')*1000)),
				'v':device_data.get('os_version'),
				'src':'com.android.vending',
	}

	# params = 'a='+campaign_data.get('singular').get('key')+'&ab='+device_data.get('cpu_abi',"armeabi")+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&br='+device_data.get('brand')+'&c=wifi&current_device_time='+str(int(time.time()*1000))+'&ddl_enabled=false&de='+device_data.get('device')+'&device_type='+device_data.get('device_type')+'&device_user_agent='+ urllib.quote(get_ua(device_data)) +'&dnt=0&i='+campaign_data.get('package_name')+'&install_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&is='+ check +'&k=AIFA&lag='+lag+'&lc='+device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')+'&ma='+device_data.get('manufacturer')+'&mo='+device_data.get('model')+'&n='+ urllib.quote(campaign_data.get('app_name')) +'&p=Android&pr='+device_data.get('product')+'&rt=json&s='+app_data.get('s')+'&sdk='+campaign_data.get('singular').get('version')+'&u='+device_data.get('adid')+'&update_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&v='+device_data.get('os_version')

	#&src=com.android.vending
	if isOpen:
		data_dict['custom_user_id'] = str(app_data.get('user_id'))
		# params = 'a='+campaign_data.get('singular').get('key')+'&ab='+device_data.get('cpu_abi',"armeabi")+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&br='+device_data.get('brand')+'&c=wifi&current_device_time='+str(int(time.time()*1000))+'&custom_user_id='+ app_data.get('user_id') +'&ddl_enabled=false&de='+device_data.get('device')+'&device_type='+device_data.get('device_type')+'&device_user_agent='+ urllib.quote(get_ua(device_data)) +'&dnt=0&i='+campaign_data.get('package_name')+'&install_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&is='+ check +'&k=AIFA&lag='+lag+'&lc='+device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country')+'&ma='+device_data.get('manufacturer')+'&mo='+device_data.get('model')+'&n='+ urllib.quote(campaign_data.get('app_name')) +'&p=Android&pr='+device_data.get('product')+'&rt=json&s='+app_data.get('s')+'&sdk='+campaign_data.get('singular').get('version')+'&u='+device_data.get('adid')+'&update_time='+str(int(app_data.get('times').get('install_complete_time')*1000))+'&v='+device_data.get('os_version')
	
	#&src=com.android.vending

	# params = params.replace(' ','+')

	params = get_params(data_dict)

	h = getHash('?'+params,campaign_data.get('singular').get('secret'))
	params = params+'&h='+h

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


def apsalarEvent(campaign_data, app_data, device_data,event_name,value='',custom_user_id=False,seq='1'):
	url = "http://sdk-api-v1.singular.net/api/v1/event"

	method = 'get'
	headers = {
		'User-Agent':'Singular/SDK-v9.0.5.PROD',
		'Accept-Encoding': 'gzip'
	}

	seq = str(app_data.get('seq'))	

	# s=str(int(time.time()*1000))

	t = str(time.time()-app_data.get('apsalar_time'))[:-8]

	if not app_data.get('user_id'):
		app_data['user_id'] = util.get_random_string('decimal',40).upper()


	data_dict = {
				'a':campaign_data.get('singular').get('key'),
				'aifa':device_data.get('adid'),
				'av':campaign_data.get('app_version_name'),
				'c':'wifi',
				'custom_user_id': app_data.get('user_id'),
				'i':campaign_data.get('package_name'),
				'k':'AIFA',
				'lag':str(generateLag()),
				'n':str(event_name),
				'p':campaign_data.get('sdk'),
				'rt':'json',
				's': start_s,
				'sdk':campaign_data.get('singular').get('version'),
				'seq':seq,
				't':t,
				'u':device_data.get('adid'),
	}

	if custom_user_id:
		data_dict['e'] = str(value)

	# if custom_user_id:
	# 	params='a='+campaign_data.get('singular').get('key')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&c=wifi&custom_user_id='+ app_data.get('user_id') +'&e='+value+'&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(generateLag())+'&n='+event_name+'&p='+campaign_data.get('sdk')+'&rt=json&s='+s+'&sdk='+campaign_data.get('singular').get('version')+'&seq='+seq+'&t='+t+'&u='+device_data.get('adid')
	# else:
	#     params='a='+campaign_data.get('singular').get('key')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&c=wifi&custom_user_id='+ app_data.get('user_id') +'&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(generateLag())+'&n='+event_name+'&p='+campaign_data.get('sdk')+'&rt=json&s='+s+'&sdk='+campaign_data.get('singular').get('version')+'&seq='+seq+'&t='+t+'&u='+device_data.get('adid')

	# params = params.replace(' ','+')
	params = get_params(data_dict)

	h = getHash('?'+params,campaign_data.get('singular').get('secret'))
	params = params+'&h='+h

	if app_data.get('seq'):
		app_data['seq']=app_data.get('seq')+1

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

################ BRANCH CALLS ###################

def branch_call(campaign_data, app_data, device_data, call=1):
	url='http://cdn.branch.io/sdk/uriskiplist_v1.json'
	method='get'
	headers={
		'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
	}

	if call==2:
		url='http://cdn.branch.io/sdk/uriskiplist_v2.json'

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': None}

def api_branch_install(campaign_data,app_data,device_data):
	method='post'
	url='http://api2.branch.io/v1/install'

	headers = {
				
				
				'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
				'Content-Type':'application/json',
				'Accept':'application/json',
			}

	data={"app_version":campaign_data.get('app_version_name'),
		"facebook_app_link_checked":False,
		"is_referrable":1,
		"update":0,
		"debug":False,
		"metadata":{},
		"hardware_id":device_data.get('android_id'),
		'clicked_referrer_ts':int(app_data.get('times').get('click_time')),
		'install_begin_ts':int(app_data.get('times').get('download_begin_time')),

		"is_hardware_id_real":True,
		"brand":device_data.get('brand'),
		"model":device_data.get('model'),
		"screen_dpi":int(device_data.get('dpi')),
		"screen_height":int(device_data.get('resolution').split('x')[0]),
		"screen_width":int(device_data.get('resolution').split('x')[1]),
		"wifi":True,
		"os":"Android",
		"os_version":int(device_data.get('sdk')),
		"country":device_data.get('locale').get('country'),
		"language":device_data.get('locale').get('language'),
		"local_ip":device_data.get('private_ip'),
		"cd":{"mv":"-1","pn":campaign_data.get('package_name')},
		"google_advertising_id":device_data.get('adid'),
		"lat_val":0,
		"instrumentation":{"v1/install-qwt":"0"},
		"sdk":campaign_data.get('api_branch').get('sdk'),
		"retryNumber":0,
		"branch_key":campaign_data.get('api_branch').get('branchkey'),
		'environment':'FULL_APP',
		'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
		'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'ui_mode':'UI_MODE_TYPE_NORMAL',
		'previous_update_time':0
		} 


	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}

#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"



######################################
#	Apsalar functions
######################################

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())

def generateLag():
	return (random.randint(40,300)*0.001)

def get_params(data_value):
	key = data_value.keys()
	a = ''
	for value in sorted(key):
		a += str(urllib.quote_plus(value).encode('utf-8'))+'='+str(urllib.quote_plus(data_value.get(value)).encode('utf-8'))+'&'
	a = a.rsplit("&",1)[0]	

	return a