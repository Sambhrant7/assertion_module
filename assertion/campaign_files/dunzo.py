# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
from sdk import NameLists
from sdk import purchase
import uuid
import time
import random
import json
import string
import datetime
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.dunzo.user',
	'app_size'			 : 26.0,#25.0,
	'app_name' 			 :'Dunzo | Delivery App for Food, Grocery &amp; more',
	'app_version_name' 	 :'3.5.0.2',#'3.4.0.0',#'3.1.0.0',
	'app_version_code' 	 :'916',#'877',#'832',
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '5.0',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'v7pDwccwZbZvtHVVrw2fpD',
		'dkh'		 : 'v7pDwccw',
		'buildnumber': '4.8.20',
		'version'	 : 'v4',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	

	###########		INITIALIZE		############

	print "Please wait installing....."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	# get_google_gcmToken(app_data)

	################generating_realtime_differences##################

	def_firstLaunchDate(app_data,device_data)
	register_user(app_data,country='united states')

	app_data['session_id'] = util.get_random_string(type='hex',size=40)
	app_data['launch_session_id'] = util.get_random_string(type='hex',size=40)

	app_data['server_user_id'] = "290" + util.get_random_string('decimal',4)
	app_data['server_hashKey'] = str(uuid.uuid4())

	app_data['widget_hashKey'] = str(uuid.uuid4())

	globals()['task_id'] = str(uuid.uuid4())
	app_data['dunzo_cash'] = 50

	

 	###########	 CALLS		 ############

 	time.sleep(random.randint(1,5))
	app_session_started(campaign_data, app_data, device_data)

	time.sleep(random.randint(3,4))
	splash_page_load(campaign_data, app_data, device_data)

	location_permission_modal_load(campaign_data, app_data, device_data)

	onb_tutorial_page_load(campaign_data, app_data, device_data)

	req = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**req)

	time.sleep(random.randint(1,2))	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	util.execute_request(**request)

	if random.randint(1,100) <= 20:

		time.sleep(random.randint(1,5))
		app_tnc_clicked(campaign_data, app_data, device_data)
		
	time.sleep(random.randint(1,5))
	app_session_background(campaign_data, app_data, device_data)

	time.sleep(random.randint(1,2))		
	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	catch(response,app_data,"appsflyer")

	time.sleep(random.randint(5,20))
	location_permission_modal_clicked(campaign_data, app_data, device_data)
	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)


	time.sleep(random.randint(5,20))
	fetch_lat_long(campaign_data, app_data, device_data)
	fetch_address(campaign_data, app_data, device_data, 'current')
	fetch_address(campaign_data, app_data, device_data, 'second')


	if random.randint(1,100) <= 50:

		time.sleep(random.randint(10,15))
		app_session_started(campaign_data, app_data, device_data)


	if not app_data.get('otp_entered') and random.randint(1,100) <= 95:

		time.sleep(random.randint(10,15))
		onb_mobile_enter_continue_clicked(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		onb_otp_page_load(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		init(campaign_data, app_data, device_data)


		if random.randint(1,100) <= 97:

			time.sleep(random.randint(5,10))
			timeforenteringotp(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			nummanualotp(campaign_data, app_data, device_data)


			if random.randint(1,100) <= 95:

				time.sleep(random.randint(1,5))
				onb_otp_continue_clicked(campaign_data, app_data, device_data)

				if not app_data.get('uuid'):
					app_data['uuid'] = str(uuid.uuid4())

				time.sleep(random.randint(1,5))
				synctask(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				generic_tele_issue_auth(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				config_call_from(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				onb_completion_page_load(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				generic_tele_issue_auth_response(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				generic_tele_issue_auth(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				onb_setting_up_sync_successful(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				gps_location_fetch(campaign_data, app_data, device_data) 

				time.sleep(random.randint(1,5))
				generic_tele_issue_auth_response(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				onb_setting_up_dunzo_loader(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				ref_code_enter_page_load(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				config_call_from(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				config_call_from(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				home_page_load(campaign_data, app_data, device_data) 

				time.sleep(random.randint(1,5))
				home_page_shimmer(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				banner_load(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				banner_scroll(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				banner_load(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				home_page_scrolled_down(campaign_data, app_data, device_data)

				app_data['otp_entered'] = True


				if random.randint(1,100) <= 94:

					if random.randint(1,100) <= 88:

						time.sleep(random.randint(5,10))
						ref_verification_loader(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						ref_code_enter_claim_clicked(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						ref_congrats_page_load(campaign_data, app_data, device_data)

					time.sleep(random.randint(1,5))
					ref_congrats_page_dismissed(campaign_data, app_data, device_data)


					for _ in range(random.randint(1,2)):

						globals()['task_id'] = str(uuid.uuid4())

						# Send Package items like keys,chargers & documents.
						if random.randint(1,100) <= 35:

							globals()['task_id'] = str(uuid.uuid4())

							fetch_address(campaign_data, app_data, device_data, 'second')

							time.sleep(random.randint(5,10))
							home_element_clicked(campaign_data, app_data, device_data, order_tag = "PICKUP", category = "PICK_UP_DROP", id_type = "banner_clicked")

							time.sleep(random.randint(1,5))
							banner_clicked(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							home_page_scrolled_down(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							pnd_ftue_page_load(campaign_data, app_data, device_data)


							if random.randint(1,100) <= 95:

								time.sleep(random.randint(1,5))
								pnd_ftue_page_load_second(campaign_data, app_data, device_data)


								if random.randint(1,100) <= 95:

									time.sleep(random.randint(1,5))
									pnd_page_load(campaign_data, app_data, device_data)

									time.sleep(random.randint(1,5))
									print '\n'+'Appsflyer : Track____________________________________'
									request  = appsflyer_track(campaign_data, app_data, device_data)
									util.execute_request(**request)


									if random.randint(1,100) <= 95:

										time.sleep(random.randint(5,10))
										pnd_pickup_address_clicked(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										location_search_page_load(campaign_data, app_data, device_data, source_page = "PND")

										time.sleep(random.randint(1,5))
										location_search_result_clicked(campaign_data, app_data, device_data, source_page = "PND")

										time.sleep(random.randint(1,5))
										location_pin_page_load(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										location_save_address_page_load(campaign_data, app_data, device_data, source_page = "PND") 

										time.sleep(random.randint(1,5))
										location_save_address_page_next_clicked(campaign_data, app_data, device_data) 


										if random.randint(1,100) <= 70:

											time.sleep(random.randint(1,5))
											print '\n'+'Appsflyer : Track____________________________________'
											request  = appsflyer_track(campaign_data, app_data, device_data)
											util.execute_request(**request)


										if random.randint(1,100) <= 95:

											time.sleep(random.randint(5,10))
											pnd_delivery_address_clicked(campaign_data, app_data, device_data)

											time.sleep(random.randint(1,5))
											location_search_page_load(campaign_data, app_data, device_data, source_page = "PND")

											time.sleep(random.randint(1,5))
											location_search_result_clicked(campaign_data, app_data, device_data, source_page = "PND") 

											time.sleep(random.randint(1,5))
											location_pin_page_load(campaign_data, app_data, device_data)

											time.sleep(random.randint(1,5))
											location_save_address_page_load(campaign_data, app_data, device_data, source_page = "PND") 

											time.sleep(random.randint(1,5))
											location_save_address_page_next_clicked(campaign_data, app_data, device_data) 


											if random.randint(1,100) <= 95:

												time.sleep(random.randint(1,5))
												pnd_choose_pickup_location(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												pnd_select_category_clicked(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												pnd_select_category_page_load(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												pnd_select_category_done_clicked(campaign_data, app_data, device_data)


												if random.randint(1,100) <= 95:

													time.sleep(random.randint(1,5))
													is_upfront(campaign_data, app_data, device_data, order_tag = "PICKUP", funnel_id = "BANNER_DEFAULT_PND", source_page = "pnd_ftue_page_load_second")

													time.sleep(random.randint(1,5))
													upfront_paynow_clicked(campaign_data, app_data, device_data, order_tag = "PND", funnel_id = "BANNER_DEFAULT_PND")


													if random.randint(1,100) <= 90:

														time.sleep(random.randint(1,5))
														payment_page_load(campaign_data, app_data, device_data, order_tag = "PND", funnel_id = "BANNER_DEFAULT_PND", source_page = "pnd_page_load")

														price = random.randint(20,200)

														time.sleep(random.randint(1,5))
														payment_page_element_clicked(campaign_data, app_data, device_data, order_tag = "PND", funnel_id = "BANNER_DEFAULT_PND", source_page = "pnd_page_load", order_amount = str(price) ) 

														time.sleep(random.randint(1,5))
														profile_add_card_page_load(campaign_data, app_data, device_data)


														if random.randint(1,100) <= 70:

															time.sleep(random.randint(1,5))
															payment_failure(campaign_data, app_data, device_data, order_amount = str(price)) #done

															time.sleep(random.randint(1,5))
															error_api_load(campaign_data, app_data, device_data)

															time.sleep(random.randint(1,5))
															is_upfront(campaign_data, app_data, device_data, order_tag = "PICKUP", funnel_id = "BANNER_DEFAULT_PND", source_page = "pnd_ftue_page_load_second")

						# Order from Store
						if random.randint(1,100) <= 35:

							globals()['task_id'] = str(uuid.uuid4())

							fetch_address(campaign_data, app_data, device_data, 'second')

							order_category_data = [
													{'category_name' : 'Documents | Books | Stationery', 'product_list' : ["Physics Book",  "Xerox Copies",  "Pen",  "Register",  "Pencil",  "Feviquick",  "Marker",  "Eraser",  "Color",  "Scale"]},
													{'category_name' : 'Grocery | Household items', 'product_list' : ["Fruits",  "Veggies",  "Cream",  "Cheese",  "Cold Drinks",  "Oil",  "Eggs",  "Chicken",  "Wheat ",  "Butter"]},
													{'category_name' : 'Food | Restaurant orders', 'product_list' : ["Pizaa",  "Pasta",  "Noodles",  "Manchurian",  "Dumplings",  "Wrap",  "Sides",  "Biryani",  "Tandoori",  "Rolls"]},
													{'category_name' : 'Electronic items', 'product_list' : ["iPhone 11",  "Asus Rog2",  "Reno X2",  "RealMe XT",  "Redmi K20",  "Laptop",  "Macbook Air",  "iPad Mini",  "PlayStation",  "Xbox"]},
													{'category_name' : 'Laundry', 'product_list' : ["Pants",  "Shirts",  "Jeans",  "Jackets",  "Socks",  "Chinos",  "T Shirts",  "Sweatshirts",  "Sweater",  "Hoodies"]},
													{'category_name' : 'Repair | Duplicate key', 'product_list' : ["Bike Key",  "Room Key",  "Bicycle Key",  "Office Key",  "Locker Key",  "Drawer Lock Key",  "Car Key",  "Almirah Key",  "Gate Key",  "Parking Key"]}
												  ]

							choose_item = random.choice(order_category_data)
							category = choose_item.get('category_name')
							product = random.choice(choose_item.get('product_list'))

							time.sleep(random.randint(1,5))
							banner_scroll(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							banner_load(campaign_data, app_data, device_data)


							if random.randint(1,100) <= 95:

								time.sleep(random.randint(5,10))
								home_element_clicked(campaign_data, app_data, device_data, order_tag = "OTHERS", category = "OTHERS", id_type = "banner_clicked")

								time.sleep(random.randint(1,5))
								banner_clicked(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								othr_others_page_load(campaign_data, app_data, device_data, funnel_id = "BANNER_DEFAULT_OTHERS")

								time.sleep(random.randint(1,5))
								home_page_scrolled_down(campaign_data, app_data, device_data)

								time.sleep(random.randint(5,10))
								othr_choose_category_clicked(campaign_data, app_data, device_data)

								time.sleep(random.randint(5,10))
								othr_pickup_location_clicked(campaign_data, app_data, device_data)

								time.sleep(random.randint(5,10))
								location_search_page_load(campaign_data, app_data, device_data, source_page = "othr_others_page_load")

								time.sleep(random.randint(5,10))
								location_search_result_clicked(campaign_data, app_data, device_data, source_page = "othr_others_page_load") 

								time.sleep(random.randint(1,5))
								othr_next_clicked(campaign_data, app_data, device_data, category = category, product = product) 

								time.sleep(random.randint(1,5))
								othrs_cp_page_load(campaign_data, app_data, device_data, category = category, product = product) 


								if random.randint(1,100) <= 70:

									time.sleep(random.randint(1,5))
									print '\n'+'Appsflyer : Track____________________________________'
									request  = appsflyer_track(campaign_data, app_data, device_data)
									util.execute_request(**request)


								if random.randint(1,100) <= 70:

									time.sleep(random.randint(5,10))
									cp_select_address_page_load(campaign_data, app_data, device_data) 

									time.sleep(random.randint(1,5))
									cp_eu_select_address_clicked(campaign_data, app_data, device_data) 


								if random.randint(1,100) <= 95:

									time.sleep(random.randint(1,5))
									is_upfront(campaign_data, app_data, device_data, order_tag = "OTHERS", funnel_id = "BANNER_DEFAULT_OTHERS", source_page = "othr_others_page_load")

									time.sleep(random.randint(1,5))
									is_upfront(campaign_data, app_data, device_data, order_tag = "OTHERS", funnel_id = "BANNER_DEFAULT_OTHERS", source_page = "othr_others_page_load")

									time.sleep(random.randint(1,5))
									othrs_cp_confirm_clicked(campaign_data, app_data, device_data, category = category, product = product)

									time.sleep(random.randint(1,5))
									is_upfront(campaign_data, app_data, device_data, order_tag = "OTHERS", funnel_id = "BANNER_DEFAULT_OTHERS", source_page = "othr_others_page_load")


									if random.randint(1,100) <= 90:

										time.sleep(random.randint(1,5))
										track_order_page_load(campaign_data, app_data, device_data, funnel_id = "BANNER_DEFAULT_OTHERS", source_page = "othr_checkout_page_load")

										time.sleep(random.randint(1,5))
										track_order_map_data_update(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										track_order_map_data_update(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										app_push_notif_recvd(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										track_order_map_data_update(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										app_push_notif_recvd(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										track_order_expand_order_status_clicked(campaign_data, app_data, device_data)


									if random.randint(1,100) <= 60:

										time.sleep(random.randint(5,10))
										track_chat_with_partner_clicked(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										track_order_chat_with_partner_load(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										track_order_chat_with_partner_message_sent(campaign_data, app_data, device_data)


									if random.randint(1,100) <= 60:

										time.sleep(random.randint(1,5))
										track_order_call_partner_clicked(campaign_data, app_data, device_data)


									if random.randint(1,100) <= 60:

										time.sleep(random.randint(1,5))
										track_order_support_page_load(campaign_data, app_data, device_data)


									if random.randint(1,100) <= 30:

										time.sleep(random.randint(1,5))
										order_listing_page_load(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										track_order_order_details_load(campaign_data, app_data, device_data)

						# Homepage 
						if random.randint(1,100) <= 85:

							globals()['task_id'] = str(uuid.uuid4())

							time.sleep(random.randint(1,5))
							banner_scroll(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							banner_load(campaign_data, app_data, device_data)


							# Home Tab
							if random.randint(1,100) <= 40:

								time.sleep(random.randint(1,5))
								home_tab_item_clicked(campaign_data, app_data, device_data, item_name = "Home", tab_position = "0")

								time.sleep(random.randint(1,5))
								home_page_scrolled_down(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								banner_scroll(campaign_data, app_data, device_data)


							# Search
							if random.randint(1,100) <= 70:

								time.sleep(random.randint(10,15))
								search_page_load(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								search_button_clicked(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								search_results_loader(campaign_data, app_data, device_data)


							# Orders
							if random.randint(1,100) <= 40:

								time.sleep(random.randint(1,5))
								home_tab_item_clicked(campaign_data, app_data, device_data, item_name = "Orders", tab_position = "2")

								time.sleep(random.randint(1,5))
								home_page_scrolled_down(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								order_listing_page_load(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								banner_scroll(campaign_data, app_data, device_data)


							if random.randint(1,100) <= 70:

								time.sleep(random.randint(1,5))
								print '\n'+'Appsflyer : Track____________________________________'
								request  = appsflyer_track(campaign_data, app_data, device_data)
								util.execute_request(**request)


							# Dunzo Cash
							if random.randint(1,100) <= 30:

								time.sleep(random.randint(1,5))
								home_tab_item_clicked(campaign_data, app_data, device_data, item_name = "Dunzo Cash", tab_position = "3")

								time.sleep(random.randint(1,5))
								home_dz_cash_icon_click(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								profile_dz_cash_page_load(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								profile_dz_cash_tnc_clicked(campaign_data, app_data, device_data)


							# Contact Dunzo Assistant 
							if random.randint(1,100) <= 15:

								time.sleep(random.randint(1,5))
								home_element_clicked(campaign_data, app_data, device_data, order_tag = "OTHERS", category = "", id_type = "ADVERTISEMENT_RIGHT_ICON")

								time.sleep(random.randint(1,5))
								home_page_scrolled_down(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								othr_others_page_load(campaign_data, app_data, device_data, funnel_id = "advertisement_r6")


							# View Profile
							if random.randint(1,100) <= 40:

								time.sleep(random.randint(1,5))
								home_profile_clicked(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								home_page_scrolled_down(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								init(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								profile_page_load(campaign_data, app_data, device_data)


								# Connect with Paytm
								if random.randint(1,100) <= 40:

									time.sleep(random.randint(1,5))
									profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "Paytm")

									time.sleep(random.randint(1,5))
									paytm_deactivated_page_load(campaign_data, app_data, device_data)


								# Connect With Simpl
								if random.randint(1,100) <= 30:

									time.sleep(random.randint(1,5))
									profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "Pay in a Click - Simpl")

									time.sleep(random.randint(1,5))
									simpl_deacivated_page_load(campaign_data, app_data, device_data)


								# View Saved Address
								if random.randint(1,100) <= 10:

									time.sleep(random.randint(1,5))
									profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "Saved Addresses")

									time.sleep(random.randint(1,5))
									numclicksoneditlocation(campaign_data, app_data, device_data)

									time.sleep(random.randint(1,5))
									profile_location_page_load(campaign_data, app_data, device_data) 


								# View Saved Cards
								if random.randint(1,100) <= 5:

									time.sleep(random.randint(1,5))
									profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "Saved Cards")

									time.sleep(random.randint(1,5))
									numclicksonsavedcard(campaign_data, app_data, device_data)


								# Refer to Friend
								if random.randint(1,100) <= 10:

									time.sleep(random.randint(1,5))
									profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "Refer a partner")

									time.sleep(random.randint(1,5))
									profile_refer_partner_page_load(campaign_data, app_data, device_data)


								# About
								if random.randint(1,100) <= 5:

									time.sleep(random.randint(1,5))
									profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "About")

									time.sleep(random.randint(1,5))
									profile_about_page_load(campaign_data, app_data, device_data)

						# Book Ride
						if random.randint(1,100) <= 30:

							globals()['task_id'] = str(uuid.uuid4())

							time.sleep(random.randint(5,10))
							fetch_address(campaign_data, app_data, device_data, 'second')

							time.sleep(random.randint(1,5))
							home_element_clicked(campaign_data, app_data, device_data, order_tag = "PILLION", category = "Pillion", id_type = "PILLION")

							time.sleep(random.randint(1,5))
							pillion_accessed(campaign_data, app_data, device_data) 

							time.sleep(random.randint(1,5))
							home_page_scrolled_down(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							pillion_loaded(campaign_data, app_data, device_data)


							if random.randint(1,100) <= 95:

								time.sleep(random.randint(1,5))
								location_selection_loaded(campaign_data, app_data, device_data) 

								time.sleep(random.randint(1,5))
								location_selection_loaded(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								location_selection_loaded(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								location_selection_loaded(campaign_data, app_data, device_data)


								if random.randint(1,100) <= 95:

									time.sleep(random.randint(1,5))
									to_selected(campaign_data, app_data, device_data)


									if random.randint(1,100) <= 70:

										time.sleep(random.randint(1,5))
										print '\n'+'Appsflyer : Track____________________________________'
										request  = appsflyer_track(campaign_data, app_data, device_data)
										util.execute_request(**request)


									if random.randint(1,100) <= 95:

										time.sleep(random.randint(1,5))
										book_ride_tapped(campaign_data, app_data, device_data) 

										time.sleep(random.randint(1,5))
										book_ride_tapped(campaign_data, app_data, device_data)


										if random.randint(1,100) <= 95:

											if random.randint(1,100) <= 20:

												time.sleep(random.randint(1,5))
												apifailure(campaign_data, app_data, device_data)

												numauthfailsonotp(campaign_data, app_data, device_data)

											else:

												time.sleep(random.randint(1,5))
												pcp_confirm_ride(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												pfpi_loaded(campaign_data, app_data, device_data) 

												time.sleep(random.randint(1,5))
												sc_createorder_confirmation_end(campaign_data, app_data, device_data)

						# Medical Store Clicked
						if random.randint(1,100) <= 25:

							globals()['task_id'] = str(uuid.uuid4())

							fetch_address(campaign_data, app_data, device_data, 'second')

							medical_item_data = [
													{"category_name" : "Baby Needs", "subcategory_list" : ["Cereals & Infant Formula", "Baby Diapers"]},
													{"category_name" : "Pharmacy - Vitamins", "subcategory_list" : ["Others"]},
													{"category_name" : "Pharmacy - Antibiotics", "subcategory_list" : ["Others"]},
													{"category_name" : "Pharmacy - Nonsteroidal Anti Inflammatory Drugs", "subcategory_list" : ["Others"]},
													{"category_name" : "Pharmacy - Drugs For Asthma and Copd", "subcategory_list" : ["Others"]},
													{"category_name" : "Pharmacy - Drugs For Diabetes", "subcategory_list" : ["Others"]},
													{"category_name" : "Pharmacy - Others", "subcategory_list" : ["Anabolic Steroids", "Anthelmintic Drugs"]},
													{"category_name" : "Personal Care - Sexual Wellness", "subcategory_list" : ["Others"]},
													{"category_name" : "Personal Care - Skin Care", "subcategory_list" : ["Others"]},
													{"category_name" : "Personal Care - Sanitary & Hygiene", "subcategory_list" : ["Others"]},
													{"category_name" : "Personal Care - Hair Care", "subcategory_list" : ["Others"]},
													{"category_name" : "Personal Care - Women's Hygiene & Wellness", "subcategory_list" : ["Others"]},
													{"category_name" : "Personal Care - Others", "subcategory_list" : ["Lip Care", "Hygiene", "Deodrants & Perfumes", "Shaving & Hair Removal"]},
													{"category_name" : "Health Needs", "subcategory_list" : ["Digestives & Laxatives", "Joint Care N Support", "Anti-Smoking Products"]},
													{"category_name" : "Health & Nutrition", "subcategory_list" : ["Health Drinks", "Nutritional Foods"]},
													{"category_name" : "Vitamin and Supplements", "subcategory_list" : ["Vitamins & Minerals"]},
													{"category_name" : "Health & Fitness", "subcategory_list" : ["Health Foods & Drinks"]},
													{"category_name" : "Healthcare Devices", "subcategory_list" : ["Thermometers & Monitors", "Foot Care"]}
												]

							choose_item = random.choice(medical_item_data)
							item_category = choose_item.get('category_name')
							item_subcategory = random.choice(choose_item.get('subcategory_list'))


							time.sleep(random.randint(5,10))
							home_element_clicked(campaign_data, app_data, device_data, order_tag = "BUY", category = "Medical", id_type = "BUY")

							time.sleep(random.randint(1,5))
							home_page_scrolled_down(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							slp_shimmer(campaign_data, app_data, device_data, funnel_id = "Medical")

							time.sleep(random.randint(1,5))
							slp_page_load(campaign_data, app_data, device_data, page_title = "Order Medicines", funnel_id = "Medical", order_subtag = "Medical")


							if random.randint(1,100) <= 95:

								time.sleep(random.randint(1,5))
								slp_store_clicked(campaign_data, app_data, device_data, funnel_id = "Medical", name = "Apollo Pharmacy")

								time.sleep(random.randint(1,5))
								sp_veg_non_veg_clicked(campaign_data, app_data, device_data, funnel_id = "medical")

								time.sleep(random.randint(1,5))
								sp_item_load(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								sp_shimmer(campaign_data, app_data, device_data, funnel_id = "medical")

								time.sleep(random.randint(1,5))
								sp_page_load(campaign_data, app_data, device_data, funnel_id = "medical", name = "Apollo Pharmacy")


								if random.randint(1,100) <= 70:

									time.sleep(random.randint(1,5))
									print '\n'+'Appsflyer : Track____________________________________'
									request  = appsflyer_track(campaign_data, app_data, device_data)
									util.execute_request(**request)


								if random.randint(1,100) <= 70:

									time.sleep(random.randint(1,5))
									item_add_clicked(campaign_data, app_data, device_data, funnel_id = "medical", item_description = "", item_subcategory = item_subcategory, item_price = "0", item_category = item_category)


								if random.randint(1,100) <= 50:

									time.sleep(random.randint(1,5))
									item_minus_clicked(campaign_data, app_data, device_data, funnel_id = "medical")

						# Buy From Grocery Shop
						if random.randint(1,100) <= 50:

							globals()['task_id'] = str(uuid.uuid4())

							fetch_address(campaign_data, app_data, device_data, 'second')

							grocery_data_list = [[ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Food Provisions", "Dry Fruits, Nuts & Seeds" ], [ "Food Provisions", "Cooking Oils & Ghee" ], [ "Food Provisions", "Cooking Oils & Ghee" ], [ "Food Provisions", "Cooking Oils & Ghee" ], [ "Food Provisions", "Cooking Oils & Ghee" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Breads & Buns" ], [ "Breakfast Foods & Drinks", "Breads & Buns" ], [ "Breakfast Foods & Drinks", "Breads & Buns" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Dairy & Eggs", "Milk" ], [ "Dairy & Eggs", "Milk" ], [ "Dairy & Eggs", "Milk" ], [ "Dairy & Eggs", "Curd & Yogurt" ], [ "Dairy & Eggs", "Curd & Yogurt" ], [ "Dairy & Eggs", "Flavoured Milk" ], [ "Dairy & Eggs", "Flavoured Milk" ], [ "Dairy & Eggs", "Flavoured Milk" ], [ "Dairy & Eggs", "Ice Cream & Popsicle" ], [ "Dairy & Eggs", "Ice Cream & Popsicle" ], [ "Dairy & Eggs", "Ice Cream & Popsicle" ], [ "Condiments", "Sauces, Spreads & Dips" ], [ "Condiments", "Sauces, Spreads & Dips" ], [ "Instant Food & Mixes", "Instant Noodles" ], [ "Instant Food & Mixes", "Instant Noodles" ], [ "Instant Food & Mixes", "Instant Noodles" ], [ "Instant Food & Mixes", "Instant Noodles" ], [ "Instant Food & Mixes", "Ready-To-Cook" ], [ "Instant Food & Mixes", "Ready-To-Eat" ], [ "Instant Food & Mixes", "Ready-To-Eat" ], [ "Instant Food & Mixes", "Ready-To-Cook" ], [ "Instant Food & Mixes", "Instant Noodles" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Beverages & Bottled Water", "Fruit Juices" ], [ "Beverages & Bottled Water", "Fruit Juices" ], [ "Beverages & Bottled Water", "Fruit Juices" ], [ "Beverages & Bottled Water", "Fruit Juices" ], [ "Snacks", "Biscuits & Cookies" ], [ "Snacks", "Puffs & Pastries" ], [ "Snacks", "Cupcakes, Muffins & Cakes" ], [ "Snacks", "Chips & Crisps" ], [ "Snacks", "Chips & Crisps" ], [ "Cold Storage", "Fries, Cutlets & Patties" ], [ "Cold Storage", "Fries, Cutlets & Patties" ], [ "Cold Storage", "Fries, Cutlets & Patties" ], [ "Cold Storage", "Cold Meats & Seafoods" ], [ "Personal Care", "Face Care" ], [ "Personal Care", "Face Care" ], [ "Personal Care", "Face Care" ], [ "Personal Care", "Oral Care" ], [ "Personal Care", "Shaving Essentials" ], [ "Personal Care", "Shaving Essentials" ], [ "Personal Care", "Shaving Essentials" ], [ "Personal Care", "Shaving Essentials" ], [ "Personal Care", "Shaving Essentials" ], [ "Personal Care", "Shaving Essentials" ], [ "Household & Cleaning Essentials", "Air Fresheners & Incense" ], [ "Household & Cleaning Essentials", "Air Fresheners & Incense" ], [ "Household & Cleaning Essentials", "Insect Repellents" ], [ "Others", "Others" ], [ "Others", "Others" ], [ "Others", "Others" ]]
							choose_item = random.choice(grocery_data_list)
							item_category = choose_item[0]
							item_subcategory = choose_item[1]
							price = random.randint(50,600)
							reduced_price = price - random.randint(0,30)


							time.sleep(random.randint(5,10))
							home_element_clicked(campaign_data, app_data, device_data, order_tag = "BUY", category = "Grocery", id_type = "BUY")

							time.sleep(random.randint(1,5))
							home_page_scrolled_down(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							slp_page_load(campaign_data, app_data, device_data, page_title = "Order Daily Grocery", funnel_id = "Grocery", order_subtag = "Grocery")

							time.sleep(random.randint(1,5))
							slp_shimmer(campaign_data, app_data, device_data, funnel_id = "Grocery")

							time.sleep(random.randint(1,5))
							slp_store_clicked(campaign_data, app_data, device_data, funnel_id = "Grocery", name = "Apollo Pharmacy")

							time.sleep(random.randint(1,5))
							sp_veg_non_veg_clicked(campaign_data, app_data, device_data, funnel_id = "grocery")

							time.sleep(random.randint(1,5))
							sp_item_load(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							sp_shimmer(campaign_data, app_data, device_data, funnel_id = "grocery")


							if random.randint(1,100) <= 70:

								time.sleep(random.randint(1,5))
								print '\n'+'Appsflyer : Track____________________________________'
								request  = appsflyer_track(campaign_data, app_data, device_data)
								util.execute_request(**request)


							if random.randint(1,100) <= 95:

								time.sleep(random.randint(1,5))
								category = random.choice([ "FOOD PROVISIONS", "CHOCOLATES & TREATS", "BEVERAGES & BOTTLED WATER", "SNACKS", "PERSONAL CARE" ])
								store_page_vertical_scroll_loader(campaign_data, app_data, device_data, category = category)


							if random.randint(1,100) <= 95:

								fromMenuName_list = ["BEST SELLERS","FOOD PROVISIONS","BREAKFAST FOODS & DRINKS","DAIRY & EGGS","CONDIMENTS","INSTANT FOOD & MIXES","CHOCOLATES & TREATS","BEVERAGES & BOTTLED WATER","SNACKS","PERSONAL CARE"]
								toMenuName_list = ["FOOD PROVISIONS","BREAKFAST FOODS & DRINKS","DAIRY & EGGS","CONDIMENTS","INSTANT FOOD & MIXES","CHOCOLATES & TREATS","BEVERAGES & BOTTLED WATER","SNACKS","PERSONAL CARE","HOUSEHOLD & CLEANING ESSENTIALS""OTHERS"]
								
								random_number = random.randint(0,len(fromMenuName_list)-1)

								fromMenuName = fromMenuName_list[random_number]
								toMenuName = toMenuName_list[random_number]

								time.sleep(random.randint(1,5))
								sp_category_right_swipe(campaign_data, app_data, device_data, fromMenuName = fromMenuName, toMenuName = toMenuName)

								time.sleep(random.randint(1,5))
								sp_category_clicked(campaign_data, app_data, device_data, fromMenuName = fromMenuName, toMenuName = toMenuName)	


							if random.randint(1,100) <= 80:

								time.sleep(random.randint(1,5))
								item_add_clicked(campaign_data, app_data, device_data, funnel_id = "grocery", item_description = "", item_subcategory = item_category, item_price = "0", item_category = item_subcategory)

								time.sleep(random.randint(1,5))
								sp_continue_clicked(campaign_data, app_data, device_data, sub_total = str(price)) 


								if random.randint(1,100) <= 90:

									time.sleep(random.randint(1,5))
									cp_page_load(campaign_data, app_data, device_data, estimated_price = str(price)) 

									time.sleep(random.randint(1,5))
									is_upfront(campaign_data, app_data, device_data, order_tag = "BUY", funnel_id = "SLP_grocery", source_page = "sp_page_load")

									time.sleep(random.randint(1,5))
									add_prescription_page_dismissed(campaign_data, app_data, device_data)

									time.sleep(random.randint(1,5))
									cp_nu_add_address_clicked(campaign_data, app_data, device_data) 

									time.sleep(random.randint(1,5))
									location_search_page_load(campaign_data, app_data, device_data, source_page = "cp_page_load")

									time.sleep(random.randint(1,5))
									location_google_search_no_results_found(campaign_data, app_data, device_data) 

									time.sleep(random.randint(1,5))
									location_search_map_pin_clicked(campaign_data, app_data, device_data)

									time.sleep(random.randint(1,5))
									location_pin_page_load(campaign_data, app_data, device_data)

									time.sleep(random.randint(1,5))
									location_save_address_page_load(campaign_data, app_data, device_data, source_page = "") 

									time.sleep(random.randint(1,5))
									location_save_address_page_next_clicked(campaign_data, app_data, device_data) 

									time.sleep(random.randint(1,5))
									config_call_from(campaign_data, app_data, device_data)

									time.sleep(random.randint(1,5))
									config_call_from(campaign_data, app_data, device_data)

									time.sleep(random.randint(1,5))
									config_call_from(campaign_data, app_data, device_data)
									
									time.sleep(random.randint(1,5))
									print '\n'+'Appsflyer : Track____________________________________'
									request  = appsflyer_track(campaign_data, app_data, device_data)
									util.execute_request(**request)

									time.sleep(random.randint(1,5))
									is_upfront(campaign_data, app_data, device_data, order_tag = "BUY", funnel_id = "SLP_grocery", source_page = "sp_page_load")


									if random.randint(1,100) <= 95:

										time.sleep(random.randint(1,5))
										add_prescription_page_dismissed(campaign_data, app_data, device_data)

										time.sleep(random.randint(1,5))
										upfront_paynow_clicked(campaign_data, app_data, device_data, order_tag = "BUY", funnel_id = "SLP_grocery")

										time.sleep(random.randint(1,5))
										cp_confirm_clicked(campaign_data, app_data, device_data) 

										time.sleep(random.randint(1,5))
										payment_page_load(campaign_data, app_data, device_data, order_tag = "BUY", funnel_id = "SLP_grocery", source_page = "cp_page_load")

										time.sleep(random.randint(1,5))
										payment_page_element_clicked(campaign_data, app_data, device_data, order_tag = "BUY", funnel_id = "SLP_grocery", source_page = "cp_page_load", order_amount = str(reduced_price)) #done


										if random.randint(1,100) <= 90:

											time.sleep(random.randint(1,5))
											profile_add_card_page_load(campaign_data, app_data, device_data)

											time.sleep(random.randint(1,5))
											profile_add_card_confirm_clicked(campaign_data, app_data, device_data)

											time.sleep(random.randint(1,5))
											profile_add_card_confirm_clicked(campaign_data, app_data, device_data)
											
											time.sleep(random.randint(1,5))
											print '\n'+'Appsflyer : Track____________________________________'
											request  = appsflyer_track(campaign_data, app_data, device_data)
											util.execute_request(**request)

											time.sleep(random.randint(1,5))
											synctask(campaign_data, app_data, device_data)

											time.sleep(random.randint(1,5))
											synctask(campaign_data, app_data, device_data)

											time.sleep(random.randint(1,5))
											synctask(campaign_data, app_data, device_data)

											time.sleep(random.randint(1,5))
											synctask(campaign_data, app_data, device_data)

											time.sleep(random.randint(1,5))
											synctask(campaign_data, app_data, device_data)

											time.sleep(random.randint(1,5))
											synctask(campaign_data, app_data, device_data)


											if purchase.isPurchase(app_data,day = 1,advertiser_demand=15): # to make overall 30%

												time.sleep(random.randint(1,5))
												payment_successful(campaign_data, app_data, device_data, order_amount = str(reduced_price)) #done

												time.sleep(random.randint(1,5))
												timepaynowtosuccess(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												track_order_received_animation_load(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												track_order_page_load(campaign_data, app_data, device_data, funnel_id = "SLP_grocery", source_page = "cp_page_load")
	
												time.sleep(random.randint(1,5))
												app_push_notif_recvd(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												track_order_map_data_update(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												app_push_notif_recvd(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												track_order_map_data_update(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												app_push_notif_recvd(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												app_push_notif_recvd(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												app_push_notif_recvd(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												order_listing_page_load(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												order_listing_email_invoice_clicked(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												onb_setup_account_page_load(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												setup_account_skip_clicked(campaign_data, app_data, device_data)

												time.sleep(random.randint(1,5))
												blocker_rate_order_page_load(campaign_data, app_data, device_data)
	
												time.sleep(random.randint(1,5))
												print '\n'+'Appsflyer : Track____________________________________'
												request  = appsflyer_track(campaign_data, app_data, device_data)
												util.execute_request(**request)

												time.sleep(random.randint(1,5))
												blocker_rate_order_done_clicked(campaign_data, app_data, device_data)




	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	
	###########		INITIALIZE		############

	print "Please wait ....."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	# get_google_gcmToken(app_data)

	if not app_data.get('prev_event_name'):
		def_eventsRecords(app_data)

	################generating_realtime_differences##################

	def_firstLaunchDate(app_data,device_data)
	register_user(app_data,country='united states')

	if not app_data.get('session_id'):
		app_data['session_id'] = util.get_random_string(type='hex',size=40)

	if not app_data.get('launch_session_id'):
		app_data['launch_session_id'] = util.get_random_string(type='hex',size=40)

	if not app_data.get('server_user_id'):
		app_data['server_user_id'] = "290" + util.get_random_string('decimal',4)

	if not app_data.get('server_hashKey'):
		app_data['server_hashKey'] = str(uuid.uuid4())

	if not app_data.get('widget_hashKey'):
		app_data['widget_hashKey'] = str(uuid.uuid4())

	globals()['task_id'] = str(uuid.uuid4())

	if not app_data.get('dunzo_cash'):
		app_data['dunzo_cash'] = 50


 	###########	 CALLS		 ############

 	time.sleep(random.randint(1,5))
 	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)


	time.sleep(random.randint(5,20))
	fetch_lat_long(campaign_data, app_data, device_data)
	fetch_address(campaign_data, app_data, device_data, 'current')
	fetch_address(campaign_data, app_data, device_data, 'second')


	if not app_data.get('otp_entered'):

		time.sleep(random.randint(10,15))
		onb_mobile_enter_continue_clicked(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		onb_otp_page_load(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		init(campaign_data, app_data, device_data)


		time.sleep(random.randint(5,10))
		timeforenteringotp(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		nummanualotp(campaign_data, app_data, device_data)


		time.sleep(random.randint(1,5))
		onb_otp_continue_clicked(campaign_data, app_data, device_data)

		if not app_data.get('uuid'):
			app_data['uuid'] = str(uuid.uuid4())

		time.sleep(random.randint(1,5))
		synctask(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		generic_tele_issue_auth(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		config_call_from(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		onb_completion_page_load(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		generic_tele_issue_auth_response(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		generic_tele_issue_auth(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		onb_setting_up_sync_successful(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		gps_location_fetch(campaign_data, app_data, device_data) 

		time.sleep(random.randint(1,5))
		generic_tele_issue_auth_response(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		onb_setting_up_dunzo_loader(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		ref_code_enter_page_load(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		config_call_from(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		config_call_from(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		home_page_load(campaign_data, app_data, device_data) 

		time.sleep(random.randint(1,5))
		home_page_shimmer(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		banner_load(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		banner_scroll(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		banner_load(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,5))
		home_page_scrolled_down(campaign_data, app_data, device_data)

		app_data['otp_entered'] = True


		if random.randint(1,100) <= 100:

			if random.randint(1,100) <= 88:

				time.sleep(random.randint(5,10))
				ref_verification_loader(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				ref_code_enter_claim_clicked(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				ref_congrats_page_load(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			ref_congrats_page_dismissed(campaign_data, app_data, device_data)

	time.sleep(random.randint(1,5))
	banner_scroll(campaign_data, app_data, device_data)

	time.sleep(random.randint(1,5))
	banner_load(campaign_data, app_data, device_data)


	for _ in range(random.randint(1,2)):

		globals()['task_id'] = str(uuid.uuid4())

		
		# Send Package items like keys,chargers & documents.
		if random.randint(1,100) <= 35:

			globals()['task_id'] = str(uuid.uuid4())

			fetch_address(campaign_data, app_data, device_data, 'second')

			time.sleep(random.randint(5,10))
			home_element_clicked(campaign_data, app_data, device_data, order_tag = "PICKUP", category = "PICK_UP_DROP", id_type = "banner_clicked")

			time.sleep(random.randint(1,5))
			banner_clicked(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			home_page_scrolled_down(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			pnd_ftue_page_load(campaign_data, app_data, device_data)


			if random.randint(1,100) <= 95:

				time.sleep(random.randint(1,5))
				pnd_ftue_page_load_second(campaign_data, app_data, device_data)


				if random.randint(1,100) <= 95:

					time.sleep(random.randint(1,5))
					pnd_page_load(campaign_data, app_data, device_data)

					time.sleep(random.randint(1,5))
					print '\n'+'Appsflyer : Track____________________________________'
					request  = appsflyer_track(campaign_data, app_data, device_data)
					util.execute_request(**request)


					if random.randint(1,100) <= 95:

						time.sleep(random.randint(5,10))
						pnd_pickup_address_clicked(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						location_search_page_load(campaign_data, app_data, device_data, source_page = "PND")

						time.sleep(random.randint(1,5))
						location_search_result_clicked(campaign_data, app_data, device_data, source_page = "PND")

						time.sleep(random.randint(1,5))
						location_pin_page_load(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						location_save_address_page_load(campaign_data, app_data, device_data, source_page = "PND") 

						time.sleep(random.randint(1,5))
						location_save_address_page_next_clicked(campaign_data, app_data, device_data) 


						if random.randint(1,100) <= 70:

							time.sleep(random.randint(1,5))
							print '\n'+'Appsflyer : Track____________________________________'
							request  = appsflyer_track(campaign_data, app_data, device_data)
							util.execute_request(**request)


						if random.randint(1,100) <= 95:

							time.sleep(random.randint(5,10))
							pnd_delivery_address_clicked(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							location_search_page_load(campaign_data, app_data, device_data, source_page = "PND")

							time.sleep(random.randint(1,5))
							location_search_result_clicked(campaign_data, app_data, device_data, source_page = "PND") 

							time.sleep(random.randint(1,5))
							location_pin_page_load(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							location_save_address_page_load(campaign_data, app_data, device_data, source_page = "PND") 

							time.sleep(random.randint(1,5))
							location_save_address_page_next_clicked(campaign_data, app_data, device_data) 


							if random.randint(1,100) <= 95:

								time.sleep(random.randint(1,5))
								pnd_choose_pickup_location(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								pnd_select_category_clicked(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								pnd_select_category_page_load(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								pnd_select_category_done_clicked(campaign_data, app_data, device_data)


								if random.randint(1,100) <= 95:

									time.sleep(random.randint(1,5))
									is_upfront(campaign_data, app_data, device_data, order_tag = "PICKUP", funnel_id = "BANNER_DEFAULT_PND", source_page = "pnd_ftue_page_load_second")

									time.sleep(random.randint(1,5))
									upfront_paynow_clicked(campaign_data, app_data, device_data, order_tag = "PND", funnel_id = "BANNER_DEFAULT_PND")


									if random.randint(1,100) <= 90:

										time.sleep(random.randint(1,5))
										payment_page_load(campaign_data, app_data, device_data, order_tag = "PND", funnel_id = "BANNER_DEFAULT_PND", source_page = "pnd_page_load")

										price = random.randint(20,200)

										time.sleep(random.randint(1,5))
										payment_page_element_clicked(campaign_data, app_data, device_data, order_tag = "PND", funnel_id = "BANNER_DEFAULT_PND", source_page = "pnd_page_load", order_amount = str(price) ) 

										time.sleep(random.randint(1,5))
										profile_add_card_page_load(campaign_data, app_data, device_data)


										if random.randint(1,100) <= 70:

											time.sleep(random.randint(1,5))
											payment_failure(campaign_data, app_data, device_data, order_amount = str(price)) #done

											time.sleep(random.randint(1,5))
											error_api_load(campaign_data, app_data, device_data)

											time.sleep(random.randint(1,5))
											is_upfront(campaign_data, app_data, device_data, order_tag = "PICKUP", funnel_id = "BANNER_DEFAULT_PND", source_page = "pnd_ftue_page_load_second")

		# Order from Store
		if random.randint(1,100) <= 35:

			globals()['task_id'] = str(uuid.uuid4())

			fetch_address(campaign_data, app_data, device_data, 'second')

			order_category_data = [
									{'category_name' : 'Documents | Books | Stationery', 'product_list' : ["Physics Book",  "Xerox Copies",  "Pen",  "Register",  "Pencil",  "Feviquick",  "Marker",  "Eraser",  "Color",  "Scale"]},
									{'category_name' : 'Grocery | Household items', 'product_list' : ["Fruits",  "Veggies",  "Cream",  "Cheese",  "Cold Drinks",  "Oil",  "Eggs",  "Chicken",  "Wheat ",  "Butter"]},
									{'category_name' : 'Food | Restaurant orders', 'product_list' : ["Pizaa",  "Pasta",  "Noodles",  "Manchurian",  "Dumplings",  "Wrap",  "Sides",  "Biryani",  "Tandoori",  "Rolls"]},
									{'category_name' : 'Electronic items', 'product_list' : ["iPhone 11",  "Asus Rog2",  "Reno X2",  "RealMe XT",  "Redmi K20",  "Laptop",  "Macbook Air",  "iPad Mini",  "PlayStation",  "Xbox"]},
									{'category_name' : 'Laundry', 'product_list' : ["Pants",  "Shirts",  "Jeans",  "Jackets",  "Socks",  "Chinos",  "T Shirts",  "Sweatshirts",  "Sweater",  "Hoodies"]},
									{'category_name' : 'Repair | Duplicate key', 'product_list' : ["Bike Key",  "Room Key",  "Bicycle Key",  "Office Key",  "Locker Key",  "Drawer Lock Key",  "Car Key",  "Almirah Key",  "Gate Key",  "Parking Key"]}
								  ]

			choose_item = random.choice(order_category_data)
			category = choose_item.get('category_name')
			product = random.choice(choose_item.get('product_list'))

			time.sleep(random.randint(1,5))
			banner_scroll(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			banner_load(campaign_data, app_data, device_data)


			if random.randint(1,100) <= 95:

				time.sleep(random.randint(5,10))
				home_element_clicked(campaign_data, app_data, device_data, order_tag = "OTHERS", category = "OTHERS", id_type = "banner_clicked")

				time.sleep(random.randint(1,5))
				banner_clicked(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				othr_others_page_load(campaign_data, app_data, device_data, funnel_id = "BANNER_DEFAULT_OTHERS")

				time.sleep(random.randint(1,5))
				home_page_scrolled_down(campaign_data, app_data, device_data)

				time.sleep(random.randint(5,10))
				othr_choose_category_clicked(campaign_data, app_data, device_data)

				time.sleep(random.randint(5,10))
				othr_pickup_location_clicked(campaign_data, app_data, device_data)

				time.sleep(random.randint(5,10))
				location_search_page_load(campaign_data, app_data, device_data, source_page = "othr_others_page_load")

				time.sleep(random.randint(5,10))
				location_search_result_clicked(campaign_data, app_data, device_data, source_page = "othr_others_page_load") 

				time.sleep(random.randint(1,5))
				othr_next_clicked(campaign_data, app_data, device_data, category = category, product = product) 

				time.sleep(random.randint(1,5))
				othrs_cp_page_load(campaign_data, app_data, device_data, category = category, product = product) 


				if random.randint(1,100) <= 70:

					time.sleep(random.randint(1,5))
					print '\n'+'Appsflyer : Track____________________________________'
					request  = appsflyer_track(campaign_data, app_data, device_data)
					util.execute_request(**request)


				if random.randint(1,100) <= 70:

					time.sleep(random.randint(5,10))
					cp_select_address_page_load(campaign_data, app_data, device_data) 

					time.sleep(random.randint(1,5))
					cp_eu_select_address_clicked(campaign_data, app_data, device_data) 


				if random.randint(1,100) <= 95:

					time.sleep(random.randint(1,5))
					is_upfront(campaign_data, app_data, device_data, order_tag = "OTHERS", funnel_id = "BANNER_DEFAULT_OTHERS", source_page = "othr_others_page_load")

					time.sleep(random.randint(1,5))
					is_upfront(campaign_data, app_data, device_data, order_tag = "OTHERS", funnel_id = "BANNER_DEFAULT_OTHERS", source_page = "othr_others_page_load")

					time.sleep(random.randint(1,5))
					othrs_cp_confirm_clicked(campaign_data, app_data, device_data, category = category, product = product)

					time.sleep(random.randint(1,5))
					is_upfront(campaign_data, app_data, device_data, order_tag = "OTHERS", funnel_id = "BANNER_DEFAULT_OTHERS", source_page = "othr_others_page_load")


					if random.randint(1,100) <= 90:

						time.sleep(random.randint(1,5))
						track_order_page_load(campaign_data, app_data, device_data, funnel_id = "BANNER_DEFAULT_OTHERS", source_page = "othr_checkout_page_load")

						time.sleep(random.randint(1,5))
						track_order_map_data_update(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						track_order_map_data_update(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						app_push_notif_recvd(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						track_order_map_data_update(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						app_push_notif_recvd(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						track_order_expand_order_status_clicked(campaign_data, app_data, device_data)


					if random.randint(1,100) <= 60:

						time.sleep(random.randint(5,10))
						track_chat_with_partner_clicked(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						track_order_chat_with_partner_load(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						track_order_chat_with_partner_message_sent(campaign_data, app_data, device_data)


					if random.randint(1,100) <= 60:

						time.sleep(random.randint(1,5))
						track_order_call_partner_clicked(campaign_data, app_data, device_data)


					if random.randint(1,100) <= 60:

						time.sleep(random.randint(1,5))
						track_order_support_page_load(campaign_data, app_data, device_data)


					if random.randint(1,100) <= 30:

						time.sleep(random.randint(1,5))
						order_listing_page_load(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						track_order_order_details_load(campaign_data, app_data, device_data)

		# Homepage 
		if random.randint(1,100) <= 85:

			globals()['task_id'] = str(uuid.uuid4())

			time.sleep(random.randint(1,5))
			banner_scroll(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			banner_load(campaign_data, app_data, device_data)


			# Home Tab
			if random.randint(1,100) <= 40:

				time.sleep(random.randint(1,5))
				home_tab_item_clicked(campaign_data, app_data, device_data, item_name = "Home", tab_position = "0")

				time.sleep(random.randint(1,5))
				home_page_scrolled_down(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				banner_scroll(campaign_data, app_data, device_data)


			# Search
			if random.randint(1,100) <= 70:

				time.sleep(random.randint(10,15))
				search_page_load(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				search_button_clicked(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				search_results_loader(campaign_data, app_data, device_data)


			# Orders
			if random.randint(1,100) <= 40:

				time.sleep(random.randint(1,5))
				home_tab_item_clicked(campaign_data, app_data, device_data, item_name = "Orders", tab_position = "2")

				time.sleep(random.randint(1,5))
				home_page_scrolled_down(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				order_listing_page_load(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				banner_scroll(campaign_data, app_data, device_data)


			if random.randint(1,100) <= 70:

				time.sleep(random.randint(1,5))
				print '\n'+'Appsflyer : Track____________________________________'
				request  = appsflyer_track(campaign_data, app_data, device_data)
				util.execute_request(**request)


			# Dunzo Cash
			if random.randint(1,100) <= 30:

				time.sleep(random.randint(1,5))
				home_tab_item_clicked(campaign_data, app_data, device_data, item_name = "Dunzo Cash", tab_position = "3")

				time.sleep(random.randint(1,5))
				home_dz_cash_icon_click(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				profile_dz_cash_page_load(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				profile_dz_cash_tnc_clicked(campaign_data, app_data, device_data)


			# Contact Dunzo Assistant 
			if random.randint(1,100) <= 15:

				time.sleep(random.randint(1,5))
				home_element_clicked(campaign_data, app_data, device_data, order_tag = "OTHERS", category = "", id_type = "ADVERTISEMENT_RIGHT_ICON")

				time.sleep(random.randint(1,5))
				home_page_scrolled_down(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				othr_others_page_load(campaign_data, app_data, device_data, funnel_id = "advertisement_r6")


			# View Profile
			if random.randint(1,100) <= 40:

				time.sleep(random.randint(1,5))
				home_profile_clicked(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				home_page_scrolled_down(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				init(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				profile_page_load(campaign_data, app_data, device_data)


				# Connect with Paytm
				if random.randint(1,100) <= 40:

					time.sleep(random.randint(1,5))
					profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "Paytm")

					time.sleep(random.randint(1,5))
					paytm_deactivated_page_load(campaign_data, app_data, device_data)


				# Connect With Simpl
				if random.randint(1,100) <= 30:

					time.sleep(random.randint(1,5))
					profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "Pay in a Click - Simpl")

					time.sleep(random.randint(1,5))
					simpl_deacivated_page_load(campaign_data, app_data, device_data)


				# View Saved Address
				if random.randint(1,100) <= 10:

					time.sleep(random.randint(1,5))
					profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "Saved Addresses")

					time.sleep(random.randint(1,5))
					numclicksoneditlocation(campaign_data, app_data, device_data)

					time.sleep(random.randint(1,5))
					profile_location_page_load(campaign_data, app_data, device_data) 


				# View Saved Cards
				if random.randint(1,100) <= 5:

					time.sleep(random.randint(1,5))
					profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "Saved Cards")

					time.sleep(random.randint(1,5))
					numclicksonsavedcard(campaign_data, app_data, device_data)


				# Refer to Friend
				if random.randint(1,100) <= 10:

					time.sleep(random.randint(1,5))
					profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "Refer a partner")

					time.sleep(random.randint(1,5))
					profile_refer_partner_page_load(campaign_data, app_data, device_data)


				# About
				if random.randint(1,100) <= 5:

					time.sleep(random.randint(1,5))
					profile_page_element_clicked(campaign_data, app_data, device_data, element_name = "About")

					time.sleep(random.randint(1,5))
					profile_about_page_load(campaign_data, app_data, device_data)

		# Book Ride
		if random.randint(1,100) <= 30:

			globals()['task_id'] = str(uuid.uuid4())

			time.sleep(random.randint(5,10))
			fetch_address(campaign_data, app_data, device_data, 'second')

			time.sleep(random.randint(1,5))
			home_element_clicked(campaign_data, app_data, device_data, order_tag = "PILLION", category = "Pillion", id_type = "PILLION")

			time.sleep(random.randint(1,5))
			pillion_accessed(campaign_data, app_data, device_data) 

			time.sleep(random.randint(1,5))
			home_page_scrolled_down(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			pillion_loaded(campaign_data, app_data, device_data)


			if random.randint(1,100) <= 95:

				time.sleep(random.randint(1,5))
				location_selection_loaded(campaign_data, app_data, device_data) 

				time.sleep(random.randint(1,5))
				location_selection_loaded(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				location_selection_loaded(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				location_selection_loaded(campaign_data, app_data, device_data)


				if random.randint(1,100) <= 95:

					time.sleep(random.randint(1,5))
					to_selected(campaign_data, app_data, device_data)


					if random.randint(1,100) <= 70:

						time.sleep(random.randint(1,5))
						print '\n'+'Appsflyer : Track____________________________________'
						request  = appsflyer_track(campaign_data, app_data, device_data)
						util.execute_request(**request)


					if random.randint(1,100) <= 95:

						time.sleep(random.randint(1,5))
						book_ride_tapped(campaign_data, app_data, device_data) 

						time.sleep(random.randint(1,5))
						book_ride_tapped(campaign_data, app_data, device_data)


						if random.randint(1,100) <= 95:

							if random.randint(1,100) <= 20:

								time.sleep(random.randint(1,5))
								apifailure(campaign_data, app_data, device_data)

								numauthfailsonotp(campaign_data, app_data, device_data)

							else:

								time.sleep(random.randint(1,5))
								pcp_confirm_ride(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								pfpi_loaded(campaign_data, app_data, device_data) 

								time.sleep(random.randint(1,5))
								sc_createorder_confirmation_end(campaign_data, app_data, device_data)

		# Medical Store Clicked
		if random.randint(1,100) <= 25:

			globals()['task_id'] = str(uuid.uuid4())

			fetch_address(campaign_data, app_data, device_data, 'second')

			medical_item_data = [
									{"category_name" : "Baby Needs", "subcategory_list" : ["Cereals & Infant Formula", "Baby Diapers"]},
									{"category_name" : "Pharmacy - Vitamins", "subcategory_list" : ["Others"]},
									{"category_name" : "Pharmacy - Antibiotics", "subcategory_list" : ["Others"]},
									{"category_name" : "Pharmacy - Nonsteroidal Anti Inflammatory Drugs", "subcategory_list" : ["Others"]},
									{"category_name" : "Pharmacy - Drugs For Asthma and Copd", "subcategory_list" : ["Others"]},
									{"category_name" : "Pharmacy - Drugs For Diabetes", "subcategory_list" : ["Others"]},
									{"category_name" : "Pharmacy - Others", "subcategory_list" : ["Anabolic Steroids", "Anthelmintic Drugs"]},
									{"category_name" : "Personal Care - Sexual Wellness", "subcategory_list" : ["Others"]},
									{"category_name" : "Personal Care - Skin Care", "subcategory_list" : ["Others"]},
									{"category_name" : "Personal Care - Sanitary & Hygiene", "subcategory_list" : ["Others"]},
									{"category_name" : "Personal Care - Hair Care", "subcategory_list" : ["Others"]},
									{"category_name" : "Personal Care - Women's Hygiene & Wellness", "subcategory_list" : ["Others"]},
									{"category_name" : "Personal Care - Others", "subcategory_list" : ["Lip Care", "Hygiene", "Deodrants & Perfumes", "Shaving & Hair Removal"]},
									{"category_name" : "Health Needs", "subcategory_list" : ["Digestives & Laxatives", "Joint Care N Support", "Anti-Smoking Products"]},
									{"category_name" : "Health & Nutrition", "subcategory_list" : ["Health Drinks", "Nutritional Foods"]},
									{"category_name" : "Vitamin and Supplements", "subcategory_list" : ["Vitamins & Minerals"]},
									{"category_name" : "Health & Fitness", "subcategory_list" : ["Health Foods & Drinks"]},
									{"category_name" : "Healthcare Devices", "subcategory_list" : ["Thermometers & Monitors", "Foot Care"]}
								]

			choose_item = random.choice(medical_item_data)
			item_category = choose_item.get('category_name')
			item_subcategory = random.choice(choose_item.get('subcategory_list'))


			time.sleep(random.randint(5,10))
			home_element_clicked(campaign_data, app_data, device_data, order_tag = "BUY", category = "Medical", id_type = "BUY")

			time.sleep(random.randint(1,5))
			home_page_scrolled_down(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			slp_shimmer(campaign_data, app_data, device_data, funnel_id = "Medical")

			time.sleep(random.randint(1,5))
			slp_page_load(campaign_data, app_data, device_data, page_title = "Order Medicines", funnel_id = "Medical", order_subtag = "Medical")


			if random.randint(1,100) <= 95:

				time.sleep(random.randint(1,5))
				slp_store_clicked(campaign_data, app_data, device_data, funnel_id = "Medical", name = "Apollo Pharmacy")

				time.sleep(random.randint(1,5))
				sp_veg_non_veg_clicked(campaign_data, app_data, device_data, funnel_id = "medical")

				time.sleep(random.randint(1,5))
				sp_item_load(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				sp_shimmer(campaign_data, app_data, device_data, funnel_id = "medical")

				time.sleep(random.randint(1,5))
				sp_page_load(campaign_data, app_data, device_data, funnel_id = "medical", name = "Apollo Pharmacy")


				if random.randint(1,100) <= 70:

					time.sleep(random.randint(1,5))
					print '\n'+'Appsflyer : Track____________________________________'
					request  = appsflyer_track(campaign_data, app_data, device_data)
					util.execute_request(**request)


				if random.randint(1,100) <= 70:

					time.sleep(random.randint(1,5))
					item_add_clicked(campaign_data, app_data, device_data, funnel_id = "medical", item_description = "", item_subcategory = item_subcategory, item_price = "0", item_category = item_category)


				if random.randint(1,100) <= 50:

					time.sleep(random.randint(1,5))
					item_minus_clicked(campaign_data, app_data, device_data, funnel_id = "medical")

		# Buy From Grocery Shop
		if random.randint(1,100) <= 45:

			globals()['task_id'] = str(uuid.uuid4())

			fetch_address(campaign_data, app_data, device_data, 'second')

			grocery_data_list = [[ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Offers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Best Sellers", "Others" ], [ "Food Provisions", "Dry Fruits, Nuts & Seeds" ], [ "Food Provisions", "Cooking Oils & Ghee" ], [ "Food Provisions", "Cooking Oils & Ghee" ], [ "Food Provisions", "Cooking Oils & Ghee" ], [ "Food Provisions", "Cooking Oils & Ghee" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Food Provisions", "Tea, Coffee & Milk Powders" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Breads & Buns" ], [ "Breakfast Foods & Drinks", "Breads & Buns" ], [ "Breakfast Foods & Drinks", "Breads & Buns" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Breakfast Foods & Drinks", "Cereals, Oats & Muesli" ], [ "Dairy & Eggs", "Milk" ], [ "Dairy & Eggs", "Milk" ], [ "Dairy & Eggs", "Milk" ], [ "Dairy & Eggs", "Curd & Yogurt" ], [ "Dairy & Eggs", "Curd & Yogurt" ], [ "Dairy & Eggs", "Flavoured Milk" ], [ "Dairy & Eggs", "Flavoured Milk" ], [ "Dairy & Eggs", "Flavoured Milk" ], [ "Dairy & Eggs", "Ice Cream & Popsicle" ], [ "Dairy & Eggs", "Ice Cream & Popsicle" ], [ "Dairy & Eggs", "Ice Cream & Popsicle" ], [ "Condiments", "Sauces, Spreads & Dips" ], [ "Condiments", "Sauces, Spreads & Dips" ], [ "Instant Food & Mixes", "Instant Noodles" ], [ "Instant Food & Mixes", "Instant Noodles" ], [ "Instant Food & Mixes", "Instant Noodles" ], [ "Instant Food & Mixes", "Instant Noodles" ], [ "Instant Food & Mixes", "Ready-To-Cook" ], [ "Instant Food & Mixes", "Ready-To-Eat" ], [ "Instant Food & Mixes", "Ready-To-Eat" ], [ "Instant Food & Mixes", "Ready-To-Cook" ], [ "Instant Food & Mixes", "Instant Noodles" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Chocolates & Treats", "Bar Chocolates" ], [ "Beverages & Bottled Water", "Fruit Juices" ], [ "Beverages & Bottled Water", "Fruit Juices" ], [ "Beverages & Bottled Water", "Fruit Juices" ], [ "Beverages & Bottled Water", "Fruit Juices" ], [ "Snacks", "Biscuits & Cookies" ], [ "Snacks", "Puffs & Pastries" ], [ "Snacks", "Cupcakes, Muffins & Cakes" ], [ "Snacks", "Chips & Crisps" ], [ "Snacks", "Chips & Crisps" ], [ "Cold Storage", "Fries, Cutlets & Patties" ], [ "Cold Storage", "Fries, Cutlets & Patties" ], [ "Cold Storage", "Fries, Cutlets & Patties" ], [ "Cold Storage", "Cold Meats & Seafoods" ], [ "Personal Care", "Face Care" ], [ "Personal Care", "Face Care" ], [ "Personal Care", "Face Care" ], [ "Personal Care", "Oral Care" ], [ "Personal Care", "Shaving Essentials" ], [ "Personal Care", "Shaving Essentials" ], [ "Personal Care", "Shaving Essentials" ], [ "Personal Care", "Shaving Essentials" ], [ "Personal Care", "Shaving Essentials" ], [ "Personal Care", "Shaving Essentials" ], [ "Household & Cleaning Essentials", "Air Fresheners & Incense" ], [ "Household & Cleaning Essentials", "Air Fresheners & Incense" ], [ "Household & Cleaning Essentials", "Insect Repellents" ], [ "Others", "Others" ], [ "Others", "Others" ], [ "Others", "Others" ]]
			choose_item = random.choice(grocery_data_list)
			item_category = choose_item[0]
			item_subcategory = choose_item[1]
			price = random.randint(50,600)
			reduced_price = price - random.randint(0,30)


			time.sleep(random.randint(5,10))
			home_element_clicked(campaign_data, app_data, device_data, order_tag = "BUY", category = "Grocery", id_type = "BUY")

			time.sleep(random.randint(1,5))
			home_page_scrolled_down(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			slp_page_load(campaign_data, app_data, device_data, page_title = "Order Daily Grocery", funnel_id = "Grocery", order_subtag = "Grocery")

			time.sleep(random.randint(1,5))
			slp_shimmer(campaign_data, app_data, device_data, funnel_id = "Grocery")

			time.sleep(random.randint(1,5))
			slp_store_clicked(campaign_data, app_data, device_data, funnel_id = "Grocery", name = "Apollo Pharmacy")

			time.sleep(random.randint(1,5))
			sp_veg_non_veg_clicked(campaign_data, app_data, device_data, funnel_id = "grocery")

			time.sleep(random.randint(1,5))
			sp_item_load(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			sp_shimmer(campaign_data, app_data, device_data, funnel_id = "grocery")


			if random.randint(1,100) <= 70:

				time.sleep(random.randint(1,5))
				print '\n'+'Appsflyer : Track____________________________________'
				request  = appsflyer_track(campaign_data, app_data, device_data)
				util.execute_request(**request)


			if random.randint(1,100) <= 95:

				time.sleep(random.randint(1,5))
				category = random.choice([ "FOOD PROVISIONS", "CHOCOLATES & TREATS", "BEVERAGES & BOTTLED WATER", "SNACKS", "PERSONAL CARE" ])
				store_page_vertical_scroll_loader(campaign_data, app_data, device_data, category = category)


			if random.randint(1,100) <= 95:

				fromMenuName_list = ["BEST SELLERS","FOOD PROVISIONS","BREAKFAST FOODS & DRINKS","DAIRY & EGGS","CONDIMENTS","INSTANT FOOD & MIXES","CHOCOLATES & TREATS","BEVERAGES & BOTTLED WATER","SNACKS","PERSONAL CARE"]
				toMenuName_list = ["FOOD PROVISIONS","BREAKFAST FOODS & DRINKS","DAIRY & EGGS","CONDIMENTS","INSTANT FOOD & MIXES","CHOCOLATES & TREATS","BEVERAGES & BOTTLED WATER","SNACKS","PERSONAL CARE","HOUSEHOLD & CLEANING ESSENTIALS""OTHERS"]
				
				random_number = random.randint(0,len(fromMenuName_list)-1)

				fromMenuName = fromMenuName_list[random_number]
				toMenuName = toMenuName_list[random_number]

				time.sleep(random.randint(1,5))
				sp_category_right_swipe(campaign_data, app_data, device_data, fromMenuName = fromMenuName, toMenuName = toMenuName)

				time.sleep(random.randint(1,5))
				sp_category_clicked(campaign_data, app_data, device_data, fromMenuName = fromMenuName, toMenuName = toMenuName)	


			if random.randint(1,100) <= 80:

				time.sleep(random.randint(1,5))
				item_add_clicked(campaign_data, app_data, device_data, funnel_id = "grocery", item_description = "", item_subcategory = item_category, item_price = "0", item_category = item_subcategory)

				time.sleep(random.randint(1,5))
				sp_continue_clicked(campaign_data, app_data, device_data, sub_total = str(price)) 


				if random.randint(1,100) <= 90:

					time.sleep(random.randint(1,5))
					cp_page_load(campaign_data, app_data, device_data, estimated_price = str(price)) 

					time.sleep(random.randint(1,5))
					is_upfront(campaign_data, app_data, device_data, order_tag = "BUY", funnel_id = "SLP_grocery", source_page = "sp_page_load")

					time.sleep(random.randint(1,5))
					add_prescription_page_dismissed(campaign_data, app_data, device_data)

					time.sleep(random.randint(1,5))
					cp_nu_add_address_clicked(campaign_data, app_data, device_data) 

					time.sleep(random.randint(1,5))
					location_search_page_load(campaign_data, app_data, device_data, source_page = "cp_page_load")

					time.sleep(random.randint(1,5))
					location_google_search_no_results_found(campaign_data, app_data, device_data) 

					time.sleep(random.randint(1,5))
					location_search_map_pin_clicked(campaign_data, app_data, device_data)

					time.sleep(random.randint(1,5))
					location_pin_page_load(campaign_data, app_data, device_data)

					time.sleep(random.randint(1,5))
					location_save_address_page_load(campaign_data, app_data, device_data, source_page = "") 

					time.sleep(random.randint(1,5))
					location_save_address_page_next_clicked(campaign_data, app_data, device_data) 

					time.sleep(random.randint(1,5))
					config_call_from(campaign_data, app_data, device_data)

					time.sleep(random.randint(1,5))
					config_call_from(campaign_data, app_data, device_data)

					time.sleep(random.randint(1,5))
					config_call_from(campaign_data, app_data, device_data)
					
					time.sleep(random.randint(1,5))
					print '\n'+'Appsflyer : Track____________________________________'
					request  = appsflyer_track(campaign_data, app_data, device_data)
					util.execute_request(**request)

					time.sleep(random.randint(1,5))
					is_upfront(campaign_data, app_data, device_data, order_tag = "BUY", funnel_id = "SLP_grocery", source_page = "sp_page_load")


					if random.randint(1,100) <= 95:

						time.sleep(random.randint(1,5))
						add_prescription_page_dismissed(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						upfront_paynow_clicked(campaign_data, app_data, device_data, order_tag = "BUY", funnel_id = "SLP_grocery")

						time.sleep(random.randint(1,5))
						cp_confirm_clicked(campaign_data, app_data, device_data) 

						time.sleep(random.randint(1,5))
						payment_page_load(campaign_data, app_data, device_data, order_tag = "BUY", funnel_id = "SLP_grocery", source_page = "cp_page_load")

						time.sleep(random.randint(1,5))
						payment_page_element_clicked(campaign_data, app_data, device_data, order_tag = "BUY", funnel_id = "SLP_grocery", source_page = "cp_page_load", order_amount = str(reduced_price)) #done


						if random.randint(1,100) <= 90:

							time.sleep(random.randint(1,5))
							profile_add_card_page_load(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							profile_add_card_confirm_clicked(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							profile_add_card_confirm_clicked(campaign_data, app_data, device_data)
							
							time.sleep(random.randint(1,5))
							print '\n'+'Appsflyer : Track____________________________________'
							request  = appsflyer_track(campaign_data, app_data, device_data)
							util.execute_request(**request)

							time.sleep(random.randint(1,5))
							synctask(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							synctask(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							synctask(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							synctask(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							synctask(campaign_data, app_data, device_data)

							time.sleep(random.randint(1,5))
							synctask(campaign_data, app_data, device_data)


							if purchase.isPurchase(app_data,day = day,advertiser_demand=30):	# to make overall 30%

								time.sleep(random.randint(1,5))
								payment_successful(campaign_data, app_data, device_data, order_amount = str(reduced_price)) #done

								time.sleep(random.randint(1,5))
								timepaynowtosuccess(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								track_order_received_animation_load(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								track_order_page_load(campaign_data, app_data, device_data, funnel_id = "SLP_grocery", source_page = "cp_page_load")

								time.sleep(random.randint(1,5))
								app_push_notif_recvd(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								track_order_map_data_update(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								app_push_notif_recvd(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								track_order_map_data_update(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								app_push_notif_recvd(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								app_push_notif_recvd(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								app_push_notif_recvd(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								order_listing_page_load(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								order_listing_email_invoice_clicked(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								onb_setup_account_page_load(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								setup_account_skip_clicked(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								blocker_rate_order_page_load(campaign_data, app_data, device_data)

								time.sleep(random.randint(1,5))
								print '\n'+'Appsflyer : Track____________________________________'
								request  = appsflyer_track(campaign_data, app_data, device_data)
								util.execute_request(**request)

								time.sleep(random.randint(1,5))
								blocker_rate_order_done_clicked(campaign_data, app_data, device_data)



	return {'status':'true'}



###################################################
#
#
#	SELF CALLS
#
###################################################

def fetch_address(campaign_data, app_data, device_data, is_type):

	print 'Fetching address ...............'
	request = locations_dunzo_in( campaign_data, device_data, app_data, is_type )
	response = util.execute_request(**request)
	try:
		res = json.loads(response.get('data'))
		if res.get('address_text'):

			if is_type == "current":
				globals()['current_address'] = res.get('address_text').encode('utf-8')
			else:
				globals()['second_address'] = res.get('address_text').encode('utf-8')

		if not globals().get('current_address'):
			globals()['current_address'] = 'Swami Pranavanand Marg, Block J, Block G, Sri Niwaspuri, New Delhi, Delhi 110048, India'

		if not globals().get('second_address'):
			globals()['second_address'] = 'Jama Masjid, Meena Bazar, Meena Bazaar, Jama Masjid, Chandni Chowk, New Delhi, Delhi 110006, India'

		print 'Ok response for current_address or second_address'

	except:
		print 'Exception in current_address or second_address'
		globals()['current_address'] = 'Swami Pranavanand Marg, Block J, Block G, Sri Niwaspuri, New Delhi, Delhi 110048, India'
		globals()['second_address'] = 'Jama Masjid, Meena Bazar, Meena Bazaar, Jama Masjid, Chandni Chowk, New Delhi, Delhi 110006, India'



def locations_dunzo_in( campaign_data, device_data, app_data, is_type ):
	url= "https://locations.dunzo.in/api/v1/locations/_address/"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        }

	params= {       "client": "Android", "lat": globals().get('latitude'), "lng": globals().get('longitude')}

	if is_type == 'second':
		globals()['second_lat'] = globals().get('latitude')[:4] + util.get_random_string('decimal',4)
		globals()['second_long'] = globals().get('longitude')[:4] + util.get_random_string('decimal',4)
		params['lat'] = globals().get('second_lat')
		params['lng'] = globals().get('second_long')

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def fetch_lat_long(campaign_data, app_data, device_data):

	print "Fetch lat and long............."
	request = current_location(device_data)
	res = util.execute_request(**request)
	try:
		js = json.loads(res.get('data'))
		if js.get('geo'):
			globals()['latitude'] = str(js.get('geo').get('latitude'))
			globals()['longitude'] = str(js.get('geo').get('longitude'))

		if not globals().get('latitude') or not globals().get('longitude'):
			globals()['latitude'] = '28.4601'
			globals()['longitude'] = '77.0263'

		print 'Ok response for lat and long'
	except:
		print 'Exception for lat and long'
		globals()['latitude'] = '28.4601'
		globals()['longitude'] = '77.0263'



def current_location(device_data):
	url='http://lumtest.com/myip.json'
	header={
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
			'Accept-Encoding': 'gzip, deflate',
			'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+','+device_data.get('locale').get('language')+';q=0.9',
			'Cache-Control': 'max-age=0',
			'Upgrade-Insecure-Requests': '1',
			'User-Agent': get_ua(device_data),
			}
	params={}
	data={}

	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def onb_completion_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________onb_completion_page_load'
	eventName			= 'onb_completion_page_load'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"status":"success","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def config_call_from(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________CONFIG_CALL_FROM'
	eventName			= 'CONFIG_CALL_FROM'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"Email":None,"uuid":app_data.get('uuid'),"flowVersion":None,"session_id":app_data.get("session_id"),"phone": app_data.get('user').get('phone',"8280053317"),"timestamp":int(time.time()*1000),"from":5,"launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def generic_tele_issue_auth(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________generic_tele_issue_auth'
	eventName			= 'generic_tele_issue_auth'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"key":"data","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def synctask(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________syncTask'
	eventName			= 'syncTask'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"Email":None,"en":"syncTask","uuid":app_data.get('uuid'),"flowVersion":None,"session_id":app_data.get("session_id"),"launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def onb_otp_continue_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________onb_otp_continue_clicked'
	eventName			= 'onb_otp_continue_clicked'
	eventValue			= json.dumps({"Phone":None,"uuid":None,"screenType":"","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"otp":util.get_random_string('decimal',4),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def nummanualotp(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________numManualOtp'
	eventName			= 'numManualOtp'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"Email":None,"en":"numManualOtp","uuid":None,"flowVersion":None,"session_id":app_data.get("session_id"),"launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def timeforenteringotp(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________timeForEnteringOtp'
	eventName			= 'timeForEnteringOtp'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"Email":None,"uuid":None,"flowVersion":None,"session_id":app_data.get("session_id"),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def init(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________INIT'
	eventName			= 'INIT'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"source":"OS","Email":None,"uuid":None,"flowVersion":None,"session_id":app_data.get("session_id"),"timestamp":int(time.time()*1000),"screenType":"OS","launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def onb_otp_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________onb_otp_page_load'
	eventName			= 'onb_otp_page_load'
	eventValue			= json.dumps({"Phone":None,"uuid":None,"screenType":"","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def onb_mobile_enter_continue_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________onb_mobile_enter_continue_clicked'
	eventName			= 'onb_mobile_enter_continue_clicked'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":None,"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def app_tnc_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________app_tnc_clicked'
	eventName			= 'app_tnc_clicked'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":None,"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def app_session_started(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________app_session_started'
	eventName			= 'app_session_started'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":None,"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"ip_address":device_data.get("private_ip"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"device_id":device_data.get("android_id"),"network_type":"WiFi","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"is_notification_enabled":True,"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)


def splash_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________splash_page_load'
	eventName			= 'splash_page_load'
	eventValue			= json.dumps({"Phone":None,"uuid":None,"screenType":"","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"timespent": util.get_random_string('decimal',4) ,"flowVersion":None,"session_id":app_data.get("session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def location_permission_modal_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________location_permission_modal_load'
	eventName			= 'location_permission_modal_load'
	eventValue			= json.dumps({"Phone":None,"source_page":"splash_page_load","time_stamp":str(int(time.time()*1000)),"uuid":None,"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def onb_tutorial_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________onb_tutorial_page_load'
	eventName			= 'onb_tutorial_page_load'
	eventValue			= json.dumps({"Phone":None,"uuid":None,"screenType":"","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def app_session_background(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________app_session_background'
	eventName			= 'app_session_background'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":None,"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def location_permission_modal_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________location_permission_modal_clicked'
	eventName			= 'location_permission_modal_clicked'
	eventValue			= json.dumps({"Phone":None,"source_page":"splash_page_load","time_stamp":str(int(time.time()*1000)),"uuid":None,"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"location_allowed":"yes","flowVersion":None,"session_id":app_data.get("session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)




##################################
##################################

def generic_tele_issue_auth_response(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________generic_tele_issue_auth_response'
	eventName			= 'generic_tele_issue_auth_response'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"key":"data","isAuthenticated":"True","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def onb_setting_up_sync_successful(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________onb_setting_up_sync_successful'
	eventName			= 'onb_setting_up_sync_successful'
	time_spent = util.get_random_string('decimal',4)
	eventValue			= json.dumps({"Phone":None,"time_spent_2":time_spent,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"time_spent":time_spent,"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"status":"success","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def gps_location_fetch(campaign_data, app_data, device_data):  
	print 'Appsflyer : EVENT___________________________gps_location_fetch'
	eventName			= 'gps_location_fetch'
	eventValue			= json.dumps({"Phone":None,"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"lng": globals().get('longitude') ,"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"status":"SUCCESS","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"lat":globals().get('latitude'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def onb_setting_up_dunzo_loader(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________onb_setting_up_dunzo_loader'
	eventName			= 'onb_setting_up_dunzo_loader'
	eventValue			= json.dumps({"Phone":None,"uuid":app_data.get('uuid'),"screenType":"","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"status":"success","Email":None,"timespent":util.get_random_string('decimal',3),"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def ref_code_enter_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ref_code_enter_page_load'
	eventName			= 'ref_code_enter_page_load'
	eventValue			= json.dumps({"Phone":None,"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def home_page_load(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________home_page_load'
	eventName			= 'home_page_load'
	eventValue			= json.dumps({"Phone":None,"city_id": util.get_random_string('decimal',1) ,"adv_id":device_data.get('adid'),"server_user_id": app_data.get('server_user_id') ,"source_page":"splash_page_load","server_hashKey": app_data.get('server_hashKey') ,"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"server_type":"HOME_SCREEN","isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","status":"SUCCESS","Email":None,"session_id":app_data.get("session_id"),"Name":"","bssid": device_data.get('mac') ,"time_stamp":str(int(time.time()*1000)),"delivery_lng": globals().get('longitude') ,"ip_address": device_data.get('private_ip') ,"area_id":util.get_random_string('decimal',3),"app_version":campaign_data.get('app_version_name'),"network_type":"WiFi","flowVersion":None,"delivery_lat": globals().get('latitude') ,"user_id":app_data.get('uuid')})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def home_page_shimmer(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________home_page_shimmer'
	eventName			= 'home_page_shimmer'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"timespent":util.get_random_string('decimal',4),"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def banner_load(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________banner_load'
	eventName			= 'banner_load'
	eventValue			= json.dumps({"Phone":None,"server_user_id":app_data.get('server_user_id'),"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"server_hashKey":app_data.get('server_hashKey'),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"server_type":"HOME_SCREEN","isProd":True,"deviceIdImei":device_data.get("android_id"),"widget_type":"BANNER_CAROUSEL","app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"widget_hashKey": app_data.get('widget_hashKey') ,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def banner_scroll(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________banner_scroll'
	eventName			= 'banner_scroll'
	eventValue			= json.dumps({"Phone":None,"server_user_id":app_data.get('server_user_id'),"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"server_hashKey":app_data.get('server_hashKey'),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"viewed_banner_indices":"[0, 1]","launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"server_type":"HOME_SCREEN","isProd":True,"deviceIdImei":device_data.get("android_id"),"widget_type":"BANNER_CAROUSEL","app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"widget_hashKey":app_data.get('widget_hashKey'),"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def home_page_scrolled_down(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________home_page_scrolled_down'
	eventName			= 'home_page_scrolled_down'
	eventValue			= json.dumps({"Phone":None,"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"section_seen":"[0, 1, 2, 3]","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def ref_verification_loader(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________ref_verification_loader'
	eventName			= 'ref_verification_loader'
	eventValue			= json.dumps({"Phone":None,"source":"home_page_load","source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"timespent": util.get_random_string('decimal',3) ,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def ref_code_enter_claim_clicked(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________ref_code_enter_claim_clicked'
	eventName			= 'ref_code_enter_claim_clicked'
	eventValue			= json.dumps({"Phone":None,"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"referral_code":"GET50","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def ref_congrats_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ref_congrats_page_load'
	eventName			= 'ref_congrats_page_load'
	eventValue			= json.dumps({"Phone":None,"source":"home_page_load","source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"expiry_date":"6d : 23h","launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"dunzo_cash_amount":"You got ₹50.0 Dunzo Cash!","flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def ref_congrats_page_dismissed(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ref_congrats_page_dismissed'
	eventName			= 'ref_congrats_page_dismissed'
	eventValue			= json.dumps({"Phone":None,"source":"home_page_load","source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"expiry_date":"6d : 23h","launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"dunzo_cash_amount":"You got ₹50.0 Dunzo Cash!","flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def home_element_clicked(campaign_data, app_data, device_data, order_tag, category, id_type):
	print 'Appsflyer : EVENT___________________________home_element_clicked'
	eventName			= 'home_element_clicked'
	eventValue			= json.dumps({"order_tag":order_tag,"banner_id":"0","Phone":None,"city_id": str(random.randint(1,9)) ,"funnelId":"BANNER_DEFAULT_PND","source_page":"","uuid":app_data.get('uuid'),"elementId":"BANNER_DEFAULT_PND","timestamp":int(time.time()*1000),"elementName":"DEFAULT_PND","position":"0","launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"banner_type":"BANNER_CAROUSEL","deviceIdImei":device_data.get("android_id"),"app_platform":"Android","category":category,"Email":None,"session_id":app_data.get("session_id"),"type":id_type,"Name":"","funnel_id":"BANNER_DEFAULT_PND","sectionId":"BANNER_0","elementPosition":"0","time_stamp":str(int(time.time()*1000)),"banner_name":"DEFAULT_PND","app_version":campaign_data.get('app_version_name'),"sectionPosition":"0","flowVersion":None,"sectionType":"BANNER_SCROLL","order_subtag":category,"sectionName":"BANNER_0","user_id":app_data.get('uuid')})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def banner_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________banner_clicked'
	eventName			= 'banner_clicked'
	eventValue			= json.dumps({"Phone":None,"server_user_id":app_data.get('server_user_id'),"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"server_hashKey":app_data.get('server_hashKey'),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"server_type":"HOME_SCREEN","isProd":True,"deviceIdImei":device_data.get("android_id"),"widget_type":"BANNER_CAROUSEL","app_platform":"Android","app_version":campaign_data.get('app_version_name'),"clicked_banner_index":str(random.randint(1,20)),"Email":None,"flowVersion":None,"widget_hashKey":app_data.get('widget_hashKey'),"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pnd_ftue_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________pnd_ftue_page_load'
	eventName			= 'pnd_ftue_page_load'
	eventValue			= json.dumps({"order_tag":"PICKUP","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Pickup","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pnd_ftue_page_load_second(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________pnd_ftue_page_load_second'
	eventName			= 'pnd_ftue_page_load_second'
	eventValue			= json.dumps({"order_tag":"PICKUP","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"pnd_ftue_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"first":True,"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Pickup","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pnd_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________pnd_page_load'
	eventName			= 'pnd_page_load'
	eventValue			= json.dumps({"order_tag":"PICKUP","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"pnd_ftue_page_load_second","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Pickup","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pnd_pickup_address_clicked(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________pnd_pickup_address_clicked'
	eventName			= 'pnd_pickup_address_clicked'
	eventValue			= json.dumps({"order_tag":"PICKUP","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"pnd_ftue_page_load_second","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Pickup","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def location_search_page_load(campaign_data, app_data, device_data, source_page): 
	print 'Appsflyer : EVENT___________________________location_search_page_load'
	eventName			= 'location_search_page_load'
	eventValue			= json.dumps({"Phone":None,"source_page":source_page,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"saved_address_JSON":"None","app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"recent_search_address_JSON":"[]","type":"PICKUP","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def location_search_result_clicked(campaign_data, app_data, device_data, source_page):
	print 'Appsflyer : EVENT___________________________location_search_result_clicked'
	eventName			= 'location_search_result_clicked'
	eventValue			= json.dumps({"address":globals().get('current_address'),"Phone":None,"source_page":source_page,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"position":"1","launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"area_id":"0","app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"search_text":"","session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def location_pin_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________location_pin_page_load'
	eventName			= 'location_pin_page_load'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def location_save_address_page_load(campaign_data, app_data, device_data, source_page): 
	print 'Appsflyer : EVENT___________________________location_save_address_page_load'
	eventName			= 'location_save_address_page_load'
	eventValue			= json.dumps({"address": globals().get('current_address') ,"Phone":None,"source_page":source_page,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"lng": globals().get('longitude') ,"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"lat":globals().get('latitude'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def location_save_address_page_next_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________location_save_address_page_next_clicked'
	eventName			= 'location_save_address_page_next_clicked'
	eventValue			= json.dumps({"order_tag":"","address": globals().get('current_address') ,"Phone":None,"landmark":"","contact_number": app_data.get('user').get('phone',"8280053317") ,"source_page":"PND","time_stamp":str(int(time.time()*1000)),"contact_name": app_data.get('user').get('firstname',"Peter") ,"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"lng":globals().get('longitude') ,"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"house": str(random.randint(10,50)) ,"lat": globals().get('latitude') ,"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pnd_choose_drop_location(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________pnd_choose_drop_location'
	eventName			= 'pnd_choose_drop_location'
	eventValue			= json.dumps({"order_tag":"PICKUP","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"pnd_ftue_page_load_second","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Pickup","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pnd_delivery_address_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________pnd_delivery_address_clicked'
	eventName			= 'pnd_delivery_address_clicked'
	eventValue			= json.dumps({"order_tag":"PICKUP","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"pnd_ftue_page_load_second","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Pickup","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pnd_choose_pickup_location(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________pnd_choose_pickup_location'
	eventName			= 'pnd_choose_pickup_location'
	eventValue			= json.dumps({"order_tag":"PICKUP","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"pnd_ftue_page_load_second","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Pickup","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pnd_select_category_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________pnd_select_category_clicked'
	eventName			= 'pnd_select_category_clicked'
	eventValue			= json.dumps({"order_tag":"PICKUP","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"pnd_ftue_page_load_second","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Pickup","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pnd_select_category_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________pnd_select_category_page_load'
	eventName			= 'pnd_select_category_page_load'
	eventValue			= json.dumps({"order_tag":"PICKUP","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"pnd_ftue_page_load_second","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Pickup","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pnd_select_category_done_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________pnd_select_category_done_clicked'
	eventName			= 'pnd_select_category_done_clicked'
	eventValue			= json.dumps({"order_tag":"PICKUP","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"pnd_ftue_page_load_second","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"category_list_JSON":"[\"Documents | Books\"]","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"status":"success","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Pickup","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def is_upfront(campaign_data, app_data, device_data, order_tag, funnel_id, source_page): 
	print 'Appsflyer : EVENT___________________________is_upfront'
	eventName			= 'is_upfront'
	eventValue			= json.dumps({"order_tag":order_tag,"Phone":None,"funnel_id":funnel_id,"source_page":source_page,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"is_upfront":"True","session_id":app_data.get("session_id"),"order_subtag":order_tag,"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def upfront_paynow_clicked(campaign_data, app_data, device_data, order_tag, funnel_id): 
	print 'Appsflyer : EVENT___________________________upfront_paynow_clicked'
	eventName			= 'upfront_paynow_clicked'
	eventValue			= json.dumps({"order_tag":order_tag,"Phone":None,"funnel_id":funnel_id,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def payment_page_load(campaign_data, app_data, device_data, order_tag, funnel_id, source_page):
	print 'Appsflyer : EVENT___________________________payment_page_load'
	eventName			= 'payment_page_load'
	eventValue			= json.dumps({"order_tag":"PND","Phone":None,"funnel_id":funnel_id,"source_page":source_page,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"preferred":"","session_id":app_data.get("session_id"),"payment_mode_JSON":"Simpl,PayTm,Amazon Pay,PayPal,Google Pay,UPI","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def payment_page_element_clicked(campaign_data, app_data, device_data, order_tag, funnel_id, source_page, order_amount): # order_amount
	print 'Appsflyer : EVENT___________________________payment_page_element_clicked'
	eventName			= 'payment_page_element_clicked'
	eventValue			= json.dumps({"order_tag":order_tag,"Phone":None,"funnel_id":funnel_id,"source_page":source_page,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"payment_type":"card","app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"task_id":globals().get('task_id'),"order_amount":order_amount+".0","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def profile_add_card_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________profile_add_card_page_load'
	eventName			= 'profile_add_card_page_load'
	eventValue			= json.dumps({"Phone":None,"source_page":"profile_saved_cards_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def payment_failure(campaign_data, app_data, device_data, order_amount): # order_amount
	print 'Appsflyer : EVENT___________________________payment_failure'
	eventName			= 'payment_failure'
	eventValue			= json.dumps({"order_tag":"PND","Phone":None,"funnel_id":"BANNER_DEFAULT_PND","source_page":"pnd_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"task_id":globals().get('task_id'),"order_amount":order_amount + ".0","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def error_api_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________error_api_load'
	eventName			= 'error_api_load'
	eventValue			= json.dumps({"Phone":None,"source":"pnd_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"error_presentation_type":"BOTTOM_SHEET","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"error_title":"Payment failed","Email":None,"flowVersion":None,"additional_info":"ServerError(type=TakeMeHomeError, title=Payment failed, subtitle=Transaction failed. If you have been charged, a refund shall be processed within 5-7 working days. Click to retry or try another payment method., imageUrl=None)","session_id":app_data.get("session_id"),"error_message":"Transaction failed. If you have been charged, a refund shall be processed within 5-7 working days. Click to retry or try another payment method.","error_type":"TakeMeHomeError","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def othr_others_page_load(campaign_data, app_data, device_data, funnel_id): 
	print 'Appsflyer : EVENT___________________________othr_others_page_load'
	eventName			= 'othr_others_page_load'
	eventValue			= json.dumps({"order_tag":"OTHERS","Phone":None,"funnel_id":funnel_id,"source_page":"HOME","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def othr_choose_category_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________othr_choose_category_clicked'
	eventName			= 'othr_choose_category_clicked'
	eventValue			= json.dumps({"order_tag":"OTHERS","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","source_page":"HOME","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def othr_pickup_location_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________othr_pickup_location_clicked'
	eventName			= 'othr_pickup_location_clicked'
	eventValue			= json.dumps({"order_tag":"OTHERS","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","source_page":"HOME","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def othr_next_clicked(campaign_data, app_data, device_data, category, product): 
	print 'Appsflyer : EVENT___________________________othr_next_clicked'
	eventName			= 'othr_next_clicked'
	eventValue			= json.dumps({"order_tag":"OTHERS","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","pickup_loc_JSON": globals().get('second_address') ,"source_page":"HOME","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"category_list_JSON": category ,"timestamp":int(time.time()*1000),"others_order_JSON":json.dumps([{"id": util.get_random_string('hex',40) ,"product":product,"quantity":1},{"id":util.get_random_string('hex',40),"product":"","quantity":1},{"product":"","quantity":1}]),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"insurance_value":"","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def othrs_cp_page_load(campaign_data, app_data, device_data, category, product): 
	print 'Appsflyer : EVENT___________________________othrs_cp_page_load'
	eventName			= 'othrs_cp_page_load'
	eventValue			= json.dumps({"order_tag":"OTHERS","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","pickup_loc_JSON":json.dumps({"address_line": globals().get('second_address') ,"apartment_address":"","buildingName":"","geo_address": globals().get('second_address') ,"is_save":False,"lat": globals().get('second_lat') ,"lng":globals().get('second_long'),"placeId":"","street_address":"","type":"PICKUP","user_edited_address":globals().get('second_address')}),"source_page":"othr_others_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"category_list_JSON":json.dumps([category]),"timestamp":int(time.time()*1000),"others_order_JSON":json.dumps([{"id": util.get_random_string('decimal',40) ,"product":"","quantity":1},{"id":util.get_random_string('decimal',40),"product":"","quantity":1}]),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"insurance_value":"0.0","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def cp_select_address_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________cp_select_address_page_load'
	eventName			= 'cp_select_address_page_load'
	eventValue			= json.dumps({"order_tag":"OTHERS","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","source_page":"othr_others_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"address_JSON":json.dumps([ util.get_random_string('decimal',8) , util.get_random_string('decimal',8) ]),"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def cp_eu_select_address_clicked(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________cp_eu_select_address_clicked'
	eventName			= 'cp_eu_select_address_clicked'
	eventValue			= json.dumps({"address": globals().get('current_address') ,"Phone":None,"funnel_id":"othr_others_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"locality": str(random.randint(5,50)) ,"launch_session_id":app_data.get("launch_session_id"),"lng": globals().get('longitude') ,"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"area_id":"OTHERS","app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"name":"","session_id":app_data.get("session_id"),"order_subtag":"BANNER_DEFAULT_OTHERS","address_id":"0","lat": globals().get('latitude') ,"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def othrs_cp_confirm_clicked(campaign_data, app_data, device_data, category, product):
	print 'Appsflyer : EVENT___________________________othrs_cp_confirm_clicked'
	eventName			= 'othrs_cp_confirm_clicked'
	eventValue			= json.dumps({"order_tag":"OTHERS","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","pickup_loc_JSON":json.dumps({"address_line": globals().get('current_address') ,"apartment_address":"","buildingName":"","geo_address": globals().get('current_address') ,"is_save":False,"lat": globals().get('latitude') ,"lng": globals().get('longitude') ,"placeId":"","street_address":"","type":"PICKUP","user_edited_address": globals().get('current_address') }),"source_page":"othr_others_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"category_list_JSON":json.dumps([category]),"timestamp":int(time.time()*1000),"others_order_JSON":json.dumps([{"id": util.get_random_string('hex', 40) ,"product":product,"quantity":1},{"id":util.get_random_string('hex', 40),"product":product,"quantity":1}]),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"insurance_value":"0.0","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def track_order_page_load(campaign_data, app_data, device_data, funnel_id, source_page): 
	print 'Appsflyer : EVENT___________________________track_order_page_load'
	eventName			= 'track_order_page_load'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":funnel_id,"runner_id":"","source_page":source_page,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"","task_id":globals().get('task_id'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def track_order_map_data_update(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________track_order_map_data_update'
	eventName			= 'track_order_map_data_update'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"task_state":"QUEUED","launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"event_time":int(time.time()*1000),"flowVersion":None,"session_id":app_data.get("session_id"),"task_id":globals().get('task_id'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def app_push_notif_recvd(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________app_push_notif_recvd'
	eventName			= 'app_push_notif_recvd'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"data":"{\"task_id\":" + str(globals().get('task_id')) +",\"body\":\"Partner will update the final bill amount once they reach the outlet. Payment would be needed for the purchase\",\"meta\":{\"notification_type\":\"TASK\",\"task_id\":" + str(globals().get('task_id')) + ",\"title\":\"Partner will update the final bill amount once they reach the outlet. Payment would be needed for the purchase\"},\"type\":\"EVENT\",\"title\":\"Partner is on the way to outlet\"}","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def track_order_expand_order_status_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________track_order_expand_order_status_clicked'
	eventName			= 'track_order_expand_order_status_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","runner_id":str(uuid.uuid4()),"source_page":"othr_checkout_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Food","task_id":globals().get('task_id'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def track_chat_with_partner_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________track_chat_with_partner_clicked'
	eventName			= 'track_chat_with_partner_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","runner_id":str(uuid.uuid4()),"source_page":"othr_checkout_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Food","task_id":globals().get('task_id'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def track_order_chat_with_partner_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________track_order_chat_with_partner_load'
	eventName			= 'track_order_chat_with_partner_load'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","runner_id":str(uuid.uuid4()),"source_page":"support_screen","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Food","task_id":globals().get('task_id'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def track_order_chat_with_partner_message_sent(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________track_order_chat_with_partner_message_sent'
	eventName			= 'track_order_chat_with_partner_message_sent'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","runner_id":str(uuid.uuid4()),"source_page":"support_screen","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Food","task_id":globals().get('task_id'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def track_order_call_partner_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________track_order_call_partner_clicked'
	eventName			= 'track_order_call_partner_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","runner_id":str(uuid.uuid4()),"source_page":"othr_checkout_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Food","task_id":globals().get('task_id'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def track_order_support_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________track_order_support_page_load'
	eventName			= 'track_order_support_page_load'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","runner_id":str(uuid.uuid4()),"source_page":"track_order_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"payment_status":"True","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Food","task_id":globals().get('task_id'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def order_listing_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________order_listing_page_load'
	eventName			= 'order_listing_page_load'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"email_invoice_visible":"false","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def track_order_order_details_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________track_order_order_details_load'
	eventName			= 'track_order_order_details_load'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"BANNER_DEFAULT_OTHERS","source_page":"track_order_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"state":"QUEUED","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Food","task_id":globals().get('task_id'),"type":"BUY","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def home_tab_item_clicked(campaign_data, app_data, device_data, item_name, tab_position): 
	print 'Appsflyer : EVENT___________________________home_tab_item_clicked'
	eventName			= 'home_tab_item_clicked'
	eventValue			= json.dumps({"Phone":None,"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"item_name":item_name,"Email":None,"flowVersion":None,"tab_position":tab_position,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def search_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________search_page_load'
	eventName			= 'search_page_load'
	eventValue			= json.dumps({"Phone":None,"funnel_id":"DEFAULT_GLOBAL_SEARCH","source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"recent_search_JSON":"[]","launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":"","trending_search_JSON":"[]"})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def search_button_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________search_button_clicked'
	eventName			= 'search_button_clicked'
	eventValue			= json.dumps({"Phone":None,"funnel_id":"DEFAULT_GLOBAL_SEARCH","source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def search_results_loader(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________search_results_loader'
	eventName			= 'search_results_loader'
	eventValue			= json.dumps({"Phone":None,"funnel_id":"DEFAULT_GLOBAL_SEARCH","end_time":str(int(time.time()*1000) + random.randint(1000,5000)),"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"start_time":str(int(time.time()*1000)),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id": app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def home_dz_cash_icon_click(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________home_dz_cash_icon_click'
	eventName			= 'home_dz_cash_icon_click'
	eventValue			= json.dumps({"Phone":None,"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def profile_dz_cash_page_load(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________profile_dz_cash_page_load'
	eventName			= 'profile_dz_cash_page_load'
	eventValue			= json.dumps({"Phone":None,"source_page":"profile_dz_cash_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"whatsapp_installed":False,"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"expiry":"7","isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"dunzo_cash_balance":"₹" + str(app_data.get('dunzo_cash')),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def profile_dz_cash_tnc_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________profile_dz_cash_tnc_clicked'
	eventName			= 'profile_dz_cash_tnc_clicked'
	eventValue			= json.dumps({"Phone":None,"source_page":"profile_dz_cash_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def home_profile_clicked(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________home_profile_clicked'
	eventName			= 'home_profile_clicked'
	eventValue			= json.dumps({"Phone":None,"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def profile_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________profile_page_load'
	eventName			= 'profile_page_load'
	eventValue			= json.dumps({"Phone":None,"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"phone":app_data.get('user').get('phone',"8280053317"),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"elements_JSON":"[{\"element_name\":\"Paytm\",\"element_position\":1},{\"element_name\":\"​​Pay in a Click - Simpl\",\"element_position\":2},{\"element_name\":\"Saved Addresses\",\"element_position\":3},{\"element_name\":\"Saved Cards\",\"element_position\":4},{\"element_name\":\"Refer a partner\",\"element_position\":5},{\"element_name\":\"Support\",\"element_position\":6},{\"element_name\":\"Dunzo Calling\",\"element_position\":7},{\"element_name\":\"About\",\"element_position\":8}]","Email":None,"flowVersion":None,"name":"","session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def profile_page_element_clicked(campaign_data, app_data, device_data, element_name): 
	print 'Appsflyer : EVENT___________________________profile_page_element_clicked'
	eventName			= 'profile_page_element_clicked'
	eventValue			= json.dumps({"Phone":None,"source_page":"PS","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"phone_number":app_data.get('user').get('phone',"8280053317"),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"element_name":element_name,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def paytm_deactivated_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________paytm_deactivated_page_load'
	eventName			= 'paytm_deactivated_page_load'
	eventValue			= json.dumps({"Phone":None,"source_page":"PS","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def simpl_deacivated_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________simpl_deacivated_page_load'
	eventName			= 'simpl_deacivated_page_load'
	eventValue			= json.dumps({"Phone":None,"source_page":"PS","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def numclicksoneditlocation(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________numClicksOnEditLocation'
	eventName			= 'numClicksOnEditLocation'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"Email":None,"en":"numClicksOnEditLocation","uuid":app_data.get('uuid'),"flowVersion":None,"session_id":app_data.get("session_id"),"launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def profile_location_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________profile_location_page_load'
	eventName			= 'profile_location_page_load'
	eventValue			= json.dumps({"Phone":None,"source_page":"profile_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"address_idlist":json.dumps([ util.get_random_string('decimal',8) , util.get_random_string('decimal',8) ]),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def numclicksonsavedcard(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________numClicksOnSavedCard'
	eventName			= 'numClicksOnSavedCard'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"Email":None,"en":"numClicksOnSavedCard","uuid":app_data.get('uuid'),"flowVersion":None,"session_id":app_data.get("session_id"),"launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def profile_refer_partner_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________profile_refer_partner_page_load'
	eventName			= 'profile_refer_partner_page_load'
	eventValue			= json.dumps({"Phone":None,"source_page":"PS","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def profile_about_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________profile_about_page_load'
	eventName			= 'profile_about_page_load'
	eventValue			= json.dumps({"Phone":None,"source_page":"profile_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pillion_accessed(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________PILLION_ACCESSED'
	eventName			= 'PILLION_ACCESSED'
	eventValue			= json.dumps({"Phone":None,"source":"some-analytics-identifier","Current_Long": globals().get('longitude') ,"Current_Lat":globals().get('latitude'),"uuid":app_data.get('uuid'),"screenType":"PLP","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"isProd":True,"deviceIdImei":device_data.get("android_id"),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"Current_Address":globals().get('current_address'),"type":"BUY","Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pillion_loaded(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________PILLION_LOADED'
	eventName			= 'PILLION_LOADED'
	eventValue			= json.dumps({"Phone":None,"uuid":app_data.get('uuid'),"screenType":"PLP","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"type":"BUY","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def location_selection_loaded(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________LOCATION_SELECTION_LOADED'
	eventName			= 'LOCATION_SELECTION_LOADED'
	eventValue			= json.dumps({"Phone":None,"uuid":app_data.get('uuid'),"To_Populated":"F","From_Long": globals().get('longitude') ,"screenType":"PLP","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"From_Populated":"T","type":"BUY","From_Address": globals().get('current_address') ,"user_id":app_data.get('uuid'),"From_Lat": globals().get('latitude') ,"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def to_selected(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________TO_SELECTED'
	eventName			= 'TO_SELECTED'
	eventValue			= json.dumps({"Phone":None,"uuid":app_data.get('uuid'),"screenType":"PLP","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"type":"BUY","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def book_ride_tapped(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________BOOK_RIDE_TAPPED'
	eventName			= 'BOOK_RIDE_TAPPED'
	eventValue			= json.dumps({"To_Address": globals().get('second_address') ,"Phone":None,"uuid":app_data.get('uuid'),"From_Long": globals().get('longitude') ,"screenType":"PLP","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"To_Lat": globals().get('second_lat') ,"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"To_Long": globals().get('second_long') ,"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"type":"BUY","From_Address": globals().get('current_address') ,"user_id":app_data.get('uuid'),"From_Lat": globals().get('latitude') ,"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def numauthfailsonotp(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________numAuthFailsOnOtp'
	eventName			= 'numAuthFailsOnOtp'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"Email":None,"uuid":app_data.get('uuid'),"flowVersion":None,"session_id":app_data.get("session_id"),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def apifailure(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________apiFailure'
	eventName			= 'apiFailure'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"Email":None,"uuid":app_data.get('uuid'),"flowVersion":None,"session_id":app_data.get("session_id"),"error":404,"launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pcp_confirm_ride(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________PCP_CONFIRM_RIDE'
	eventName			= 'PCP_CONFIRM_RIDE'
	eventValue			= json.dumps({"Phone":None,"uuid":app_data.get('uuid'),"screenType":"PCP","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"type":"BUY","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def pfpi_loaded(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________PFPI_LOADED'
	eventName			= 'PFPI_LOADED'
	eventValue			= json.dumps({"To_Address": globals().get('second_address') ,"Phone":None,"Price":"₹ " + util.get_random_string('decimal', 2),"uuid":app_data.get('uuid'),"Distance":str(random.randint(1,10))+'.' + str(random.randint(1,10)),"screenType":"PFPI","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"type":"BUY","From_Address": globals().get('current_address') ,"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sc_createorder_confirmation_end(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________SC_CREATEORDER_CONFIRMATION_END'
	eventName			= 'SC_CREATEORDER_CONFIRMATION_END'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"Email":None,"uuid":app_data.get('uuid'),"flowVersion":None,"session_id":app_data.get("session_id"),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def slp_shimmer(campaign_data, app_data, device_data, funnel_id): 
	print 'Appsflyer : EVENT___________________________slp_shimmer'
	eventName			= 'slp_shimmer'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"GRID_" + funnel_id,"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"shimmer_start_time":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":funnel_id,"shimmer_end_time":str(int(time.time()*1000) + random.randint(1000,5000)),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def slp_page_load(campaign_data, app_data, device_data, page_title, funnel_id, order_subtag):
	print 'Appsflyer : EVENT___________________________slp_page_load'
	eventName			= 'slp_page_load'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"deviceId":device_data.get("android_id"),"source_page":"home_page_load","server_hashKey":app_data.get('server_hashKey'),"uuid":app_data.get('uuid'),"page_title":page_title,"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"server_type":"SLP_SEARCH_RESULTS_CLICKED","isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","Email":None,"server_funnelId": str(uuid.uuid4()) ,"session_id":app_data.get("session_id"),"api_request_timestamp": str(int(time.time()*1000)) ,"Name":"","api_parse_timestamp":str(int(time.time()*1000)),"funnel_id":"GRID_" + funnel_id,"time_stamp":str(int(time.time()*1000)),"app_version":campaign_data.get('app_version_name'),"flowVersion":None,"order_subtag":order_subtag,"user_id":app_data.get('uuid')})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def slp_store_clicked(campaign_data, app_data, device_data, funnel_id, name): # name
	print 'Appsflyer : EVENT___________________________slp_store_clicked'
	eventName			= 'slp_store_clicked'
	eventValue			= json.dumps({"order_tag":"","Phone":None,"deviceId":device_data.get("android_id"),"funnel_id":"GRID_" + funnel_id,"source_page":"slp_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"position":"1","launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"name": name ,"session_id":app_data.get("session_id"),"order_subtag":"","type":"OPEN","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sp_veg_non_veg_clicked(campaign_data, app_data, device_data, funnel_id): 
	print 'Appsflyer : EVENT___________________________sp_veg_non_veg_clicked'
	eventName			= 'sp_veg_non_veg_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_" + funnel_id,"source_page":"slp_page_load","time_stamp":str(int(time.time()*1000)),"dzid":str(uuid.uuid4()),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":funnel_id,"type":"nonveg","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sp_item_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________sp_item_load'
	eventName			= 'sp_item_load'
	eventValue			= json.dumps({"api_parsing_timestamp":str(int(time.time()*1000)),"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"api_response_timestamp":str(int(time.time()*1000)),"flowVersion":None,"session_id":app_data.get("session_id"),"api_request_timestamp":str(int(time.time()*1000)),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sp_shimmer(campaign_data, app_data, device_data, funnel_id): 
	print 'Appsflyer : EVENT___________________________sp_shimmer'
	eventName			= 'sp_shimmer'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_" + funnel_id,"source_page":"slp_page_load","time_stamp":str(int(time.time()*1000)),"shimmer_start_time":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":funnel_id,"shimmer_end_time":str(int(time.time()*1000)),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sp_page_load(campaign_data, app_data, device_data, funnel_id, name): # name
	print 'Appsflyer : EVENT___________________________sp_page_load'
	eventName			= 'sp_page_load'
	eventValue			= json.dumps({"order_tag":"BUY","store_type":"skustore","Phone":None,"source_page":"slp_page_load","shimmer_start_time":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"demand_shape":"false","timestamp":int(time.time()*1000),"locality": globals().get('second_address') ,"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","Email":None,"eta": str(random.randint(5,60)) + " MINS","session_id":app_data.get("session_id"),"api_request_timestamp":str(int(time.time()*1000)),"shimmer_end_time":str(int(time.time()*1000) + random.randint(1000, 5000)),"Name":"","api_parsing_timestamp":str(int(time.time()*1000)),"funnel_id":"SLP_" + funnel_id,"time_stamp":str(int(time.time()*1000)),"dzid":str(uuid.uuid4()),"app_version":campaign_data.get('app_version_name'),"api_response_timestamp":str(int(time.time()*1000)),"flowVersion":None,"name":name,"order_subtag":funnel_id,"user_id":app_data.get('uuid')})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def item_add_clicked(campaign_data, app_data, device_data, funnel_id, item_description = "", item_subcategory = "", item_price = "0", item_category = ""): # item_description, item_subcategory, item_price, item_category
	print 'Appsflyer : EVENT___________________________item_add_clicked'
	eventName			= 'item_add_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"source_page":"sp_page_load","uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"item_description":"","item_subcategory":"Cereals & Infant Formula","launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"itemID":util.get_random_string('hex',24),"app_platform":"Android","Email":None,"session_id":app_data.get("session_id"),"item_price":"0","Name":"","item_category":"Baby Needs","funnel_id":"SLP_" + funnel_id,"time_stamp":str(int(time.time()*1000)),"dzid":str(uuid.uuid4()),"item_type":"variant","app_version":campaign_data.get('app_version_name'),"flowVersion":None,"order_subtag":funnel_id,"user_id":app_data.get('uuid')})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def item_minus_clicked(campaign_data, app_data, device_data, funnel_id):
	print 'Appsflyer : EVENT___________________________item_minus_clicked'
	eventName			= 'item_minus_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_" + funnel_id,"source_page":"sp_page_load","time_stamp":str(int(time.time()*1000)),"dzid": str(uuid.uuid4()) ,"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"itemID":util.get_random_string('hex',24),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":funnel_id,"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sp_category_right_swipe(campaign_data, app_data, device_data, fromMenuName, toMenuName): 
	print 'Appsflyer : EVENT___________________________sp_category_right_swipe'
	eventName			= 'sp_category_right_swipe'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_grocery","fromMenuName":fromMenuName,"source_page":"sp_page_load","time_stamp":str(int(time.time()*1000)),"dzid": str(uuid.uuid4()) ,"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"toMenuName":toMenuName,"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"grocery","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sp_category_clicked(campaign_data, app_data, device_data, fromMenuName, toMenuName): 
	print 'Appsflyer : EVENT___________________________sp_category_clicked'
	eventName			= 'sp_category_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_grocery","fromMenuName":fromMenuName,"source_page":"sp_page_load","time_stamp":str(int(time.time()*1000)),"dzid": str(uuid.uuid4()) ,"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"toMenuName":toMenuName,"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"grocery","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def store_page_vertical_scroll_loader(campaign_data, app_data, device_data, category): 
	print 'Appsflyer : EVENT___________________________store_page_vertical_scroll_loader'
	eventName			= 'store_page_vertical_scroll_loader'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_grocery","end_time":str(int(time.time()*1000) + random.randint(1000,5000)),"source_page":"sp_page_load","time_stamp":str(int(time.time()*1000)),"dzid":str(uuid.uuid4()),"uuid":app_data.get('uuid'),"start_time":str(int(time.time()*1000)),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"category":category,"status":"success","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"grocery","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def store_page_horizontal_scroll_loader(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________store_page_horizontal_scroll_loader'
	eventName			= 'store_page_horizontal_scroll_loader'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_grocery","end_time":str(int(time.time()*1000)),"source_page":"sp_page_load","time_stamp":str(int(time.time()*1000)),"dzid": str(uuid.uuid4()),"uuid":app_data.get('uuid'),"start_time":str(int(time.time()*1000)),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"status":"success","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"grocery","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sp_category_left_swipe(campaign_data, app_data, device_data, fromMenuName, toMenuName):
	print 'Appsflyer : EVENT___________________________sp_category_left_swipe'
	eventName			= 'sp_category_left_swipe'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_grocery","fromMenuName":fromMenuName,"source_page":"sp_page_load","time_stamp":str(int(time.time()*1000)),"dzid":str(uuid.uuid4()),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"toMenuName":toMenuName,"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"grocery","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sp_continue_clicked(campaign_data, app_data, device_data, sub_total): # sub_total
	print 'Appsflyer : EVENT___________________________sp_continue_clicked'
	eventName			= 'sp_continue_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","store_type":"sku","Phone":None,"funnel_id":"SLP_grocery","source_page":"slp_page_load","time_stamp":str(int(time.time()*1000)),"dzid":str(uuid.uuid4()),"uuid":app_data.get('uuid'),"demand_shape":"no","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"sub_total":sub_total,"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"total_quantity":"1","session_id":app_data.get("session_id"),"order_subtag":"grocery","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def cp_page_load(campaign_data, app_data, device_data, estimated_price): # estimated_price
	print 'Appsflyer : EVENT___________________________cp_page_load'
	eventName			= 'cp_page_load'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"source_page":"sp_page_load","server_hashKey":app_data.get('server_hashKey'),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"server_type":"CP_PAGE_LOAD","isProd":True,"deviceIdImei":device_data.get("android_id"),"estimated_price": estimated_price + ".0","app_platform":"Android","Email":None,"server_funnelId":str(uuid.uuid4()),"session_id":app_data.get("session_id"),"Name":"","has_freebie":False,"funnel_id":"SLP_grocery","time_stamp":str(int(time.time()*1000)),"dzid":str(uuid.uuid4()),"freebie_sku_id_JSON":"[]","app_version":campaign_data.get('app_version_name'),"flowVersion":None,"order_subtag":"grocery","drop_loc_JSON":json.dumps({"lat": globals().get('latitude') ,"lng": globals().get('longitude') ,"address_saved_name":""}),"user_id":app_data.get('uuid')})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def add_prescription_page_dismissed(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________add_prescription_page_dismissed'
	eventName			= 'add_prescription_page_dismissed'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_grocery","source_page":"sp_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"grocery","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def cp_nu_add_address_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________cp_nu_add_address_clicked'
	eventName			= 'cp_nu_add_address_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","address": globals().get('current_address') ,"Phone":None,"funnel_id":"SLP_grocery","source_page":"sp_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"locality":"","launch_session_id":app_data.get("launch_session_id"),"lng":globals().get('longitude'),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"area_id":"0","app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"grocery","lat": globals().get('latitude') ,"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def location_google_search_no_results_found(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________location_google_search_no_results_found'
	eventName			= 'location_google_search_no_results_found'
	eventValue			= json.dumps({"order_tag":"grocery","Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"search_string": util.get_random_string('all',3) ,"order_subtag":"SLP_grocery","error_message":"BUY","error_type":"Location Not Found","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def location_search_map_pin_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________location_search_map_pin_clicked'
	eventName			= 'location_search_map_pin_clicked'
	eventValue			= json.dumps({"Phone":None,"source_page":"cp_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def cp_confirm_clicked(campaign_data, app_data, device_data): 
	print 'Appsflyer : EVENT___________________________cp_confirm_clicked'
	eventName			= 'cp_confirm_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"source_page":"sp_page_load","server_hashKey":app_data.get('server_hashKey'),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"server_type":"CP_PAGE_LOAD","isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","Email":None,"server_funnelId":str(uuid.uuid4()),"session_id":app_data.get("session_id"),"Name":"","has_freebie":False,"funnel_id":"SLP_grocery","time_stamp":str(int(time.time()*1000)),"dzid":str(uuid.uuid4()),"freebie_sku_id_JSON":"[]","app_version":campaign_data.get('app_version_name'),"flowVersion":None,"order_subtag":"grocery","drop_loc_JSON":json.dumps({"lat": globals().get('latitude') ,"lng": globals().get('longitude') ,"address_saved_name":""}),"user_id":app_data.get('uuid')})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def profile_add_card_confirm_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________profile_add_card_confirm_clicked'
	eventName			= 'profile_add_card_confirm_clicked'
	eventValue			= json.dumps({"Phone":None,"source_page":"profile_saved_cards_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def payment_successful(campaign_data, app_data, device_data, order_amount): # order_amount
	print 'Appsflyer : EVENT___________________________payment_successful'
	eventName			= 'payment_successful'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_grocery","source_page":"cp_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"payment_type":"","app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"grocery","task_id":globals().get('task_id'),"order_amount":order_amount + ".0","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def timepaynowtosuccess(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________timePayNowToSuccess'
	eventName			= 'timePayNowToSuccess'
	eventValue			= json.dumps({"isProd":True,"Phone":None,"deviceIdImei":device_data.get("android_id"),"Email":None,"en":"timePayNowToSuccess","uuid":app_data.get('uuid'),"flowVersion":None,"session_id":app_data.get("session_id"),"launch_session_id":app_data.get("launch_session_id"),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def track_order_received_animation_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________track_order_received_animation_load'
	eventName			= 'track_order_received_animation_load'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_grocery","source_page":"cp_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"grocery","task_id":globals().get('task_id'),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def order_listing_email_invoice_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________order_listing_email_invoice_clicked'
	eventName			= 'order_listing_email_invoice_clicked'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def onb_setup_account_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________onb_setup_account_page_load'
	eventName			= 'onb_setup_account_page_load'
	eventValue			= json.dumps({"Phone":None,"source":"","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def setup_account_skip_clicked(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________setup_account_skip_clicked'
	eventName			= 'setup_account_skip_clicked'
	eventValue			= json.dumps({"Phone":None,"time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def blocker_rate_order_page_load(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________blocker_rate_order_page_load'
	eventName			= 'blocker_rate_order_page_load'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"funnel_id":"SLP_grocery","source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"state":"QUEUED","timestamp":int(time.time()*1000),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"category":"Grocery","Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Grocery","task_id":globals().get('task_id'),"type":"BUY","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def blocker_rate_order_done_clicked(campaign_data, app_data, device_data): # store_rating_JSON, partner_rating_JSON
	print 'Appsflyer : EVENT___________________________blocker_rate_order_done_clicked'
	eventName			= 'blocker_rate_order_done_clicked'
	eventValue			= json.dumps({"order_tag":"BUY","Phone":None,"source_page":"home_page_load","time_stamp":str(int(time.time()*1000)),"uuid":app_data.get('uuid'),"timestamp":int(time.time()*1000),"store_rating_JSON":json.dumps({"id": str(uuid.uuid4()) ,"name":"","stars": str(random.randint(1,5)) ,"reason":"[QUALITY, PRICE]"}),"launch_session_id":app_data.get("launch_session_id"),"app_timestamp":str(int(time.time()*1000)),"isProd":True,"partner_rating_JSON":json.dumps({"id":str(uuid.uuid4()),"name":"","stars": str(random.randint(1,5)) ,"reason":"[DELIVERY_TIME]"}),"deviceIdImei":device_data.get("android_id"),"app_platform":"Android","app_version":campaign_data.get('app_version_name'),"Email":None,"flowVersion":None,"session_id":app_data.get("session_id"),"order_subtag":"Grocery","user_id":app_data.get('uuid'),"Name":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)




###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data):
	def_appsflyerUID(app_data)
 	def_(app_data,'counter')
 	get_google_gcmToken(app_data)
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"app_name" : campaign_data.get('app_name'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand').upper(),
		"carrier" : device_data.get('carrier'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	register_user(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}

	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	if eventName=="numAuthFailsOnOtp":
		params['isCachedRequest']="true"
		params['timeincache']=10
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"currency" : 'USD',
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		"deviceRm" : device_data.get('build'),
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "true",
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"registeredUninstall" : False,
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('af_gcm_token'):
		data['af_gcm_token']=app_data.get('af_gcm_token')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"currency" : 'USD',
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "ac",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"sensors" : [{u'sN': u'ACCELEROMETER', u'sVE': [-0.296, -0.181, 9.95], u'sV': u'MTK', u'sVS': [-0.354, -0.239, 9.778], u'sT': 1}],
		},
		"deviceRm" : device_data.get('build'),
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"ol_ver" : None,
		"onelink_id" : "btui",
		"open_referrer" : 'android-app://com.android.vending',
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"registeredUninstall" : False,
		"rfr" : {
								"clk" : str(int(app_data.get("times").get("click_time"))),
								"code" : "0",
								"install" : str(int(app_data.get("times").get("download_begin_time"))),
								"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",
		},
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),

		}			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('counter')>2:
		if data.get('rfr'):
			del data['rfr']
		if data.get('deviceData').get('sensors'):
			del data['deviceData']['sensors']
		if data.get("p_receipt"):
			del data["p_receipt"]

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if app_data.get('af_gcm_token'):
		data['af_gcm_token']=app_data.get('af_gcm_token')

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))	
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"device_id" : app_data.get('uid'),
		"devkey" : campaign_data.get('appsflyer').get('key'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################
def change_char(s, p, r):
	return s[:p]+r+s[p+1:]
		
def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100

	if n3<10:
		n3 = '0'+str(n3)
		
	sb = insert_char(sb,23,str(n3))
	return sb



###################################################################
# appsflyer_kef_key and value generator
# parameter 			: brand, sdk, lang, af_ts, buildnumber, etc
#
# Generates kef key and value for appsflyer.
###################################################################
global temp, counter
temp = 0
counter = 1

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
	if buildnumber=="4.8.18":
		return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
	else:
		c = '\u0000'
		obj2 = af_ts[::-1]
		out = calculate(sdk, obj2, brand)
		i = len(out)
		if i > 4:
			i2 = globals()['counter']+65
			globals()['temp'] = i2 % 128
			if (1 if i2 % 2 != 0 else None) != 1:
				out.delete(4, i)
			else:
				out.delete(5, i)
		else:
			while i < 4:
				i3 = globals()['counter'] + 119
				globals()['temp'] = i3 % 128
				if (16 if i3 % 2 != 0 else 93) != 16:
					i += 1
					out+='1'
				else:
					i += 66
					out+='H'
				i3 = globals()['counter'] + 109
				globals()['temp'] = i3 % 128
				i3 %= 2
		return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
	ts = ts[::-1]
	appends = [sdk,lang,ts]
	lengths = [len(sdk),len(lang),len(ts)]
	l = sorted(lengths)
	least = l[0]
	out = ""
	for x in range(least):
		numb = None
		for y in range(len(appends)):
			charAt = ord(appends[y][x])
			if numb:
				charAt = int((charAt ^ int(numb)))

			numb = charAt
			
		out+=str(hex(numb))[2:]

	if len(out)>4:
		out = out[:4]
	elif len(out)<4:
		while len(out)<4:
			out+="1"
	
	return out

"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""

def generateValue(b,x,s,p,ts,fl,buildnumber):
	part1=generateValue1get(ts,fl,buildnumber)
	str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, "utf-8")
	for i in range(len(str_)):
		str_[i] = int((str_[i] ^ ((i % 2) + 42)))

	stringBuilder = ""
	for toHexString in str_:
		toHexString2 = str(hex(int(toHexString)))[2:]
		if 1 == len(toHexString2):
			toHexString2 = "0"+str(toHexString2)
		stringBuilder+=toHexString2
	part2 = stringBuilder.__str__()
	return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
	gethash = sha256(ts+firstLaunch+buildnumber)
	return gethash[:16]

"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""

def calculate(sdk, obj, brand):
	allList = [sdk, obj, brand]
	arrayList = []
	i = 0
	while True:
		flag = 1
		if i >= 3:
			break
		i2 = globals()['temp'] + 63
		globals()['counter'] = i2 % 128
		if i2 % 2 != 0:
			flag = None
		if flag != None:
			arrayList.append(len(allList[i]))
			i += 31
		else:
			arrayList.append(len(allList[i]))
			i += 1
	sorted(arrayList)
	intValue = int(arrayList[0])
	out = ""
	i3 = 0
	while i3 < intValue:
		i4 = globals()['counter']+99
		globals()['temp'] = i4 % 128
		i5 =  globals()['temp']+67
		globals()['counter'] = i5 % 128
		i5 %= 2
		number = None
		if i4 % 2 != 0:
			a = 57
		else:
			a = 83
		if a != 83:
			i4 = 1
		else:
			i4 = 0
		while i4 < 3:
			i5 = globals()['temp'] + 87
			globals()['counter'] = i5 % 128
			i5 %= 2
			i5 = ord(allList[i4][i3])
			if (92 if number == None else 39) != 39:
				i6 = globals()['temp'] + 55
				globals()['counter'] = i6 % 128
				i6 %= 2
				i6 = globals()['counter'] + 39
				globals()['temp'] = i6 % 128
				i6 %= 2
			else:
				i5 ^= int(number)
			number = i5
			i4 += 1
		out+=str(hex(int(number)))[2:]
		i3 += 1
	return out



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def ref_time():
	time.sleep(random.randint(10,15))
	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		#app_data['user']['sex'] 		= gender
		#app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		#app_data['user']['lastname'] 	= last_name
		#app_data['user']['username'] 	= username
		#app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['phone']		= str(random.randint(80,99)) + util.get_random_string('decimal', 8)