from sdk import util,purchase,installtimenew
from sdk import NameLists
from sdk import getsleep
import time
import random
import json
import datetime
import urllib
import uuid
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'com.voltage.g.hykoi',
	'app_name' 			 :'100 scenes love + all-round love affordable Ikki reading',
	'app_version_name'	 :'7.3',#'5.7',#'5.6',#'5.5',#'5.3',#'5.1',#'5.0',#'4.9',#'4.8',#'4.7',#'4.1',#4.6
	'no_referrer' 		 : True,
	'app_size'			 : 39.0,
	'supported_os'		 :'4.4',		
	'supported_countries':'WW',
	'device_targeting':True,
	'tracker'			 :'adjust',
	'ctr'				 :6,
	'adjust':{
		'app_token'	: '5dyfk28qvfgg',
		'sdk'		: 'unity4.11.0@android4.11.0',
				
	},
	'gmp_version':'14366',#'13280',
	'unity3D'   :{
                            'localProjectId'  :   'b70d3f4f398a441c5ba1df9affe63e72',
                            'platformid':'11',
                            "X-Unity-Version": "5.4.4f1"                   
    },  
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:20,
		21:19,
		22:18,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############
	print '---------------------------Install---------------------------------'
	installtimenew.main(app_data,device_data,app_size= campaign_data['app_size'] ,os="android")
	global unity_ID, unity_UUID
	unity_ID = util.get_random_string('hex', 32) 
	unity_UUID = util.get_random_string('hex', 32) 

	if not app_data.get('dlap_uid'):
		app_data['dlap_uid'] = str(random.randint(10,100))+str(util.get_random_string('char_all',8))

	if not app_data.get('contents_id'):
		app_data['contents_id'] = str(random.randint(100,999))

	if not app_data.get('platform_id'):
		app_data['platform_id'] = str(random.randint(1,9))

	if not app_data.get('vid2'):
		app_data['vid2'] = str(util.get_random_string('hex',8))+'_'+str(util.get_random_string('hex',16))

	if not app_data.get('vid1'):
		app_data['vid1'] = str(util.get_random_string('decimal',15))+'_'+str(util.get_random_string('decimal',15))+'_'+app_data.get('vid2')


	###########		CALLS		################
	
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

	print '\n\nUNITY_call------------------------------------------------------------------'
	unity3d = unityCall1(campaign_data, app_data, device_data)
	util.execute_request(**unity3d)

	print '\n\nUNITY_call------------------------------------------------------------------'
	unity3d = unityupdate(campaign_data, app_data, device_data)
	util.execute_request(**unity3d)


	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	time.sleep(random.randint(3,6))

	print '\nAdjust : selfcall1___________________________config'
	request=selfcall1(campaign_data, app_data, device_data)
	util.execute_request(**request)

	print '\nAdjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data , source='reftag')
	util.execute_request(**request)


	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)

	interval=random.randint(10,50)
	set_sessionLength(app_data,forced=False,length=interval)
	print '\nAdjust : Event____________________________________'
	callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"token":"ccefov"})
	request=adjust_event(campaign_data, app_data, device_data,event_token='ccefov',callback_params=callback_params)
	util.execute_request(**request)

	time.sleep(random.randint(6,8))
	interval=random.randint(10,50)
	set_sessionLength(app_data,forced=False,length=interval)
	print '\nAdjust : Event____________________________________'
	callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"token":"vkxeur"})
	request=adjust_event(campaign_data, app_data, device_data,event_token='vkxeur',callback_params=callback_params)
	util.execute_request(**request)

	time.sleep(random.randint(8,10))

	print '\n\nUNITY_call------------------------------------------------------------------'
	unity3d = unityupdate(campaign_data, app_data, device_data)
	util.execute_request(**unity3d)


	if random.randint(1,100)<=95:
		time.sleep(random.randint(10,15))
		interval=random.randint(10,50)
		set_sessionLength(app_data,forced=False,length=interval)
		print '\nAdjust : Event____________________________________'
		callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"vid1":app_data.get('vid1'),"vid2":app_data.get('vid2'),"token":"b7b217"})
		request=adjust_event(campaign_data, app_data, device_data,event_token='b7b217',callback_params=callback_params)
		util.execute_request(**request)
		app_data['tutorial_flag']=True

	if random.randint(1,100)<=60:
		time.sleep(random.randint(10,15))
		interval=random.randint(10,50)
		set_sessionLength(app_data,forced=False,length=interval)
		print '\nAdjust : Event____________________________________'
		callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"vid1":app_data.get('vid1'),"vid2":app_data.get('vid2'),"token":"vkxeur"})
		request=adjust_event(campaign_data, app_data, device_data,event_token='vkxeur',callback_params=callback_params)
		util.execute_request(**request)

	print '\n\nUNITY_call------------------------------------------------------------------'
	unity3d = unityupdate(campaign_data, app_data, device_data)
	util.execute_request(**unity3d)

	if random.randint(1,100)<=85:
		time.sleep(random.randint(10,15))
		interval=random.randint(10,50)
		set_sessionLength(app_data,forced=False,length=interval)
		print '\nAdjust : Event____________________________________'
		callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"vid1":app_data.get('vid1'),"vid2":app_data.get('vid2'),"token":"40383f"})
		request=adjust_event(campaign_data, app_data, device_data,event_token='40383f',callback_params=callback_params)
		util.execute_request(**request)
		app_data['story_start_flag']=True


	
	return {'status':True}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size= campaign_data['app_size'] ,os="android")

	if not app_data.get('dlap_uid'):
		app_data['dlap_uid'] = str(random.randint(10,100))+str(util.get_random_string('char_all',8))

	if not app_data.get('contents_id'):
		app_data['contents_id'] = str(random.randint(100,999))

	if not app_data.get('platform_id'):
		app_data['platform_id'] = str(random.randint(1,9))

	if not app_data.get('vid2'):
		app_data['vid2'] = str(util.get_random_string('hex',8))+'_'+str(util.get_random_string('hex',16))

	if not app_data.get('vid1'):
		app_data['vid1'] = str(util.get_random_string('decimal',15))+'_'+str(util.get_random_string('decimal',15))+'_'+app_data.get('vid2')

	if not app_data.get('tutorial_flag'):
		app_data['tutorial_flag']=False

	if not app_data.get('story_start_flag'):
		app_data['story_start_flag']=False
	###########		INITIALIZE		###########

	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data)
	util.execute_request(**request)

	interval=random.randint(10,50)
	set_sessionLength(app_data,forced=False,length=interval)
	print '\nAdjust : Event____________________________________'
	callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"vid1":app_data.get('vid1'),"vid2":app_data.get('vid2'),"token":"vkxeur"})
	request=adjust_event(campaign_data, app_data, device_data,event_token='vkxeur',callback_params=callback_params)
	util.execute_request(**request)

	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)

	if app_data.get('tutorial_flag')==False:
		time.sleep(random.randint(10,15))
		interval=random.randint(10,50)
		set_sessionLength(app_data,forced=False,length=interval)
		print '\nAdjust : Event____________________________________'
		callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"vid1":app_data.get('vid1'),"vid2":app_data.get('vid2'),"token":"b7b217"})
		request=adjust_event(campaign_data, app_data, device_data,event_token='b7b217',callback_params=callback_params)
		util.execute_request(**request)
		app_data['tutorial_flag']=True

	if random.randint(1,100)<=60:
		time.sleep(random.randint(10,15))
		interval=random.randint(10,50)
		set_sessionLength(app_data,forced=False,length=interval)
		print '\nAdjust : Event____________________________________'
		callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"vid1":app_data.get('vid1'),"vid2":app_data.get('vid2'),"token":"vkxeur"})
		request=adjust_event(campaign_data, app_data, device_data,event_token='vkxeur',callback_params=callback_params)
		util.execute_request(**request)

	if app_data.get('story_start_flag')==False:
		time.sleep(random.randint(10,15))
		interval=random.randint(10,50)
		set_sessionLength(app_data,forced=False,length=interval)
		print '\nAdjust : Event____________________________________'
		callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"vid1":app_data.get('vid1'),"vid2":app_data.get('vid2'),"token":"40383f"})
		request=adjust_event(campaign_data, app_data, device_data,event_token='40383f',callback_params=callback_params)
		util.execute_request(**request)
		app_data['story_start_flag']=True

	purchasee(campaign_data, app_data, device_data,day)
	
	return {'status':True}




def purchasee(campaign_data, app_data, device_data,day):
	interval=random.randint(10,50)
	set_sessionLength(app_data,forced=False,length=interval)
	print '\nMAT : PURCHASE____________________________________'
	if purchase.isPurchase(app_data,day):
		time.sleep(random.randint(20,30))
		print '\nAdjust : Event____________________________________'
		callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"vid1":app_data.get('vid1'),"vid2":app_data.get('vid2'),"token":"rxc5uv"})
		request=adjust_event(campaign_data, app_data, device_data,event_token='rxc5uv',callback_params=callback_params)
		util.execute_request(**request)

		print '\nAdjust : Event____________________________________'
		callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"vid1":app_data.get('vid1'),"vid2":app_data.get('vid2'),"token":"uc8d3k"})
		request=adjust_event(campaign_data, app_data, device_data,event_token='uc8d3k',callback_params=callback_params)
		util.execute_request(**request)

		print '\nAdjust : Event____________________________________'
		callback_params=json.dumps({"platform_id":app_data.get('platform_id'),"contents_id":app_data.get('contents_id'),"dlap_uid":app_data.get('dlap_uid'),"vid1":app_data.get('vid1'),"vid2":app_data.get('vid2'),"token":"cj4thm"})
		request=adjust_event(campaign_data, app_data, device_data,event_token='cj4thm',callback_params=callback_params)
		util.execute_request(**request)


def selfcall1(campaign_data, app_data, device_data):
	url = 'http://app-measurement.com/config/app/1%3A928625631129%3Aandroid%3A586e8854d9c71b96'
	method = 'head'
	headers = {
		'Accept-Encoding': 'gzip',
		'User-Agent': get_ua(device_data),
	}
	params={
			'app_instance_id':'af2bf2a84d847b9d2adfdf48883a23a8',
			'gmp_version':campaign_data.get('gmp_version'),
			'platform':'android'
	}
	data = {}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def unityCall1(campaign_data,app_data,device_data):
	url="http://stats.unity3d.com/HWStats.cgi"
	method="post"
	headers={
			"Accept-Encoding": "gzip",
			"User-Agent":get_ua(device_data),
			"Content-Type": "application/x-www-form-urlencoded",
			"X-Unity-Version": campaign_data.get('X-Unity-Version'),

	}
	params=None

	data={

				'os':device_data.get('manufacturer')+'OS'+'API'+'-'+device_data.get('sdk'),
				'cpu':device_data.get('processor').split(' ')[0]+' VFPv3'+' NEON',
				'gfxname':device_data.get('gpu').get('name'),
				'gfxapi': 11 ,
				'gfxvendor':device_data.get('gpu').get('vendor'),
				'gfxversion':device_data.get('gpu').get('version'),
				'gfxdriver':'V@100.0 AU@05.01.00.115.128 (GIT@I55c48cad9a)',
				'gfxflags':'27827',
				'gfxrt': '255',
				'gfxshader':'40',
				'gfxtex': '953',
				'cpucount':device_data.get('cpu_core'),
				'cpufreq':'1209',
				'debug':0,
				'lang':device_data.get('locale').get('language'),
				'localProjectId': campaign_data.get('unity3D').get('localProjectId'),
				'dpi': device_data.get('dpi'),
				'ram':device_data.get('ram').get('shown'),
				'vram':device_data.get('ram').get('available'),
				'screen':device_data.get('resolution'),
				'platform':campaign_data.get('unity3D').get('platformid'),
				'flags':'50333762',
				'sensors':55,
				'unityId':unity_ID,
				'hash':'2d76cec1275ca74c7f0f1e3096f86cb7',
				'appId':campaign_data.get('package_name'),
				'uuid':unity_UUID,
				'model':device_data.get('brand')+'/'+device_data.get('model')+'/'+device_data.get('device'),
				'unity':campaign_data.get('unity3D').get('X-Unity-Version'),
				'build':campaign_data.get('unity3D').get('X-Unity-Version'),
	}

	return{'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def unityupdate(campaign_data,app_data,device_data):
	url="http://stats.unity3d.com/HWStatsUpdate.cgi"
	method="post"
	headers={
			"Accept-Encoding": "gzip",
			"User-Agent":get_ua(device_data),
			"Content-Type": "application/x-www-form-urlencoded",
			"X-Unity-Version": campaign_data.get('X-Unity-Version'),

	}
	params=None

	data={
				'':'',
				'unityId':unity_ID,
				 'unity':campaign_data.get('unity3D').get('X-Unity-Version'),
				 'os':'Android OS '+device_data.get('os_version')+' / API-'+device_data.get('sdk')+' ('+device_data.get('build')+'/1456818039)',
				 'uuid':unity_UUID,
				 'platform':campaign_data.get('unity3D').get('platformid'),
				 'lang':device_data.get('locale').get('language'),
				 'appId':campaign_data.get('package_name'),
	}

	return{'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}






################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,isOpen=False):
	set_androidUUID(app_data)
	inc_(app_data,'session_count')
	set_sessionLength(app_data,forced=False,length=0)

	
	
	created_at=datetime.datetime.utcfromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')	
	time.sleep(random.randint(2,4))
	sent_at=datetime.datetime.utcfromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	
	url 	= 'http://app.adjust.com/session'
	method 	= 'post'
	headers = {
		'Client-SDK'		: campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/x-www-form-urlencoded',
		'User-Agent'		: get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		#'callback_params'		:str({"ldtrackid":random.choice(app_data.get('key')),}),
		# 'connectivity_type'		:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('brand'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		# 'installed_at'			:app_data.get("installed_at"),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		# 'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'tracking_enabled'		:'1',
		# 'updated_at'			:app_data.get("installed_at"),
		'vm_isa'				:'arm',
		}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################


def adjust_sdkclick(campaign_data, app_data, device_data , source=''):
	set_androidUUID(app_data)
	# set_installedAT(app_data,device_data)
	set_sessionLength(app_data,forced=False,length=0)
	def_(app_data,'subsession_count')

	
	click_time=datetime.datetime.utcfromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	time.sleep(random.randint(1,3))
	created_at=datetime.datetime.utcfromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')	
	time.sleep(random.randint(2,4))
	sent_at=datetime.datetime.utcfromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	url 	= 'http://app.adjust.com/sdk_click'
	method 	='post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None

	
	data = {
		'click_time'			:click_time,
		'source'				:source,
		# 'android_uuid'			:app_data.get('android_uuid'),
		# 'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		# 'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		# 'country'				:device_data.get('locale').get('country'),
		# 'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		# 'device_manufacturer'	:device_data.get('brand'),
		# 'device_name'			:device_data.get('model'),
		# 'device_type'			:device_data.get('device_type'),
		# 'display_height'		:device_data.get('resolution').split('x')[0],
		# 'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		# 'hardware_name'			:device_data.get('hardware'),
		# 'installed_at'			:app_data.get("installed_at"),
		# 'language'				:device_data.get('locale').get('language'),
		# 'last_interval'          :0 ,
		'needs_response_details':'1',
		# 'os_build'				:device_data.get('build'),
		# 'os_name'				:'android',
		# 'os_version'			:device_data.get('os_version'),
		# 'package_name'			:campaign_data.get('package_name'),
		# 'screen_density'		:get_screen_density(device_data),
		# 'screen_format'			:get_screen_format(device_data),
		# 'screen_size'			:'normal',
		'sent_at'				:sent_at,
		# 'session_count'			:1,
		'tracking_enabled'		:'1',
		# 'updated_at'			:app_data.get("installed_at"),
		# 'vm_isa'				:'arm',
		# 'connectivity_type'		: '1',
		# 'network_type'			: '0',
		# 'session_length'	: app_data.get('sessionLength'),
		# 'subsession_count': app_data.get('subsession_count'),
		# 'time_spent': app_data.get('sessionLength'),
		}

	if app_data.get('referrer'):
		data['referrer'] = urllib.unquote(app_data.get('referrer'))	
				
	else :
		data['referrer'] = "utm_source=google-play&utm_medium=organic"
		
	

	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data, device_data, initiated_by='backend'):

	
	created_at=datetime.datetime.utcfromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')	
	time.sleep(random.randint(5,8))
	sent_at=datetime.datetime.utcfromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	url 	= 'http://app.adjust.com/attribution'
	method 	='head'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	params = {
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:created_at,
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		# 'initiated_by'			:initiated_by,
		'needs_response_details':'1',
		'sent_at'				:sent_at,
		'tracking_enabled'		:'1'
		}

	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=''):
	set_androidUUID(app_data)
	# set_installedAT(app_data,device_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')

	
	created_at=datetime.datetime.utcfromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')	
	time.sleep(random.randint(1,3))
	sent_at=datetime.datetime.utcfromtimestamp((time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	
	url 	= 'http://app.adjust.com/event'
	method 	= 'post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'callback_params'		:callback_params,
		# 'connectivity_type'		:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			: created_at,
		'device_manufacturer'	:device_data.get('brand'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'event_count'			:app_data.get('event_count'),
		'event_token'			:event_token,
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		# 'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'session_length'		:app_data.get('sessionLength'),
		'subsession_count'		:app_data.get('subsession_count'),
		'time_spent'			:app_data.get('sessionLength'),
		'tracking_enabled'		:'1',
		'vm_isa'				:'arm',
		}

	if event_token=='uc8d3k':
		data['currency'] = 'JPY'
		data['revenue'] = '120.00000'

	if event_token=='ccefov':
		data['queue_size']='1'


	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}








#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = str(int(time.time()*1000))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0



def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	

###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1



def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)



def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def timestamp():
	return str(int(time.time()*1000))



