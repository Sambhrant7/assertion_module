# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
from sdk import NameLists
import time
import random
import json
import urllib
import hashlib
import base64
import datetime
import urlparse
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.netmarble.nanatsunotaizai',
	'app_size'			 : 245.5,
	'app_name' 			 :'nanatsunotaizai',
	'app_id' 			 : '1268959718',
	'app_version_name' 	 : '1.1.6',
	'app_version_code'   : '10106',
	'ctr' 				 : 6,
	'sdk' 		 		 : 'ios',
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '9.0',
	'tracker'		 	 : 'Apsalar',
	'apsalar'		 	    : {
		'version'		    : 'Singular/8.4.0',
		'key'				: 'netmarblekr_9b38e90b',
		'secret'		    : 'c52013bac93dfe2b0c9bf61a35d10452',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	

	###########		INITIALIZE		############

	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')

	install_receipt()
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')

	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	################generating_realtime_differences##################

 	###########	 CALLS		 ############

 	print '\n'+"-----------------------------APSALAR Resolve---------------------------------"
	call = apsalarResolve(campaign_data, app_data, device_data, ifu='APID')
	app_data['api_hit_time']=time.time()
	util.execute_request(**call)
		
	print '\n'+"-----------------------------APSALAR Resolve---------------------------------"
	call = apsalarResolve(campaign_data, app_data, device_data, ifu='IDFA')
	util.execute_request(**call)
		
	print '\n'+"-----------------------------APSALAR Resolve---------------------------------"
	call = apsalarResolve(campaign_data, app_data, device_data, ifu='IDFV')
	util.execute_request(**call)
		
	print '\n'+"-----------------------------APSALAR Start---------------------------------"
	call = apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**call)

	apsalar_event_heartbeat(campaign_data, app_data, device_data)

	time.sleep(random.randint(2*60,5*60))

	apsalar_event_registration(campaign_data, app_data, device_data)

	time.sleep(random.randint(10,15))

	apsalar_event_login(campaign_data, app_data, device_data)

	time.sleep(random.randint(2*60,5*60))

	apsalar_event_heartbeat(campaign_data, app_data, device_data)

	time.sleep(random.randint(4*60,6*60))

	apsalar_event_level_achieved(campaign_data, app_data, device_data)

	time.sleep(random.randint(2*60,3*60))

	apsalar_event_nickname_complete(campaign_data, app_data, device_data)

	time.sleep(random.randint(4*60,7*60))

	apsalar_event_tutorial_complete(campaign_data, app_data, device_data)


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	# def_appsflyerUID(app_data)
	# def_eventsRecords(app_data)
	# generate_realtime_differences(device_data,app_data)

	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def apsalar_event_heartbeat(campaign_data, app_data, device_data, custom_user_id=False):
	print 'Apsalr : EVENT______________________________heartbeat'
	request=apsalar_event(campaign_data, app_data, device_data,event_name='heartbeat',value=urllib.quote_plus(json.dumps({})),custom_user_id=custom_user_id)
	util.execute_request(**request)

def apsalar_event_registration(campaign_data, app_data, device_data, custom_user_id=False):
	print 'Apsalr : EVENT______________________________registration'
	request=apsalar_event(campaign_data, app_data, device_data,event_name='registration',value=urllib.quote_plus(json.dumps({})),custom_user_id=custom_user_id)
	util.execute_request(**request)

def apsalar_event_login(campaign_data, app_data, device_data, custom_user_id=False):
	print 'Apsalr : EVENT______________________________login'
	request=apsalar_event(campaign_data, app_data, device_data,event_name='login',value=urllib.quote_plus(json.dumps({})),custom_user_id=custom_user_id)
	util.execute_request(**request)

def apsalar_event_level_achieved(campaign_data, app_data, device_data, custom_user_id=False):
	print 'Apsalr : EVENT______________________________level_achieved'
	request=apsalar_event(campaign_data, app_data, device_data,event_name='level_achieved',value=urllib.quote_plus(json.dumps({"level":2})),custom_user_id=custom_user_id)
	util.execute_request(**request)

def apsalar_event_nickname_complete(campaign_data, app_data, device_data, custom_user_id=False):
	print 'Apsalr : EVENT______________________________nickname_complete'
	request=apsalar_event(campaign_data, app_data, device_data,event_name='nickname_complete',value=urllib.quote_plus(json.dumps({})),custom_user_id=custom_user_id)
	util.execute_request(**request)

def apsalar_event_tutorial_complete(campaign_data, app_data, device_data, custom_user_id=False):
	print 'Apsalr : EVENT______________________________tutorial_complete'
	request=apsalar_event(campaign_data, app_data, device_data,event_name='tutorial_complete',value=urllib.quote_plus(json.dumps({})),custom_user_id=custom_user_id)
	util.execute_request(**request)


#################################################
#												#
# 			AppSalar Resolve funcation 			#
#												#
#################################################

def apsalarResolve(campaign_data, app_data, device_data,ifu='IDFA',retries=False):
	custom_user_id_create(app_data)
	method = "get"
	url = "http://e-ssl.apsalar.com/api/v1/resolve"
	headers = {
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {
		"a" : campaign_data.get('apsalar').get('key'),
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"k" : ifu,
		"p" : "iOS",
		"rt" : "plist",
		#"tries" : ".",
		"u" : "542107A3-4BD3-4239-80EB-2852DB7B79D5",
		"v" : device_data.get('os_version'),

		}
	data = {

		}
	if ifu=='APID':
		params['u']=str(uuid.uuid4()).upper()
	elif ifu=='IDFV':
		params['u']=app_data.get('idfv_id')
	elif ifu=='IDFA':
		params['u']=app_data.get('idfa_id')	

	if retries==True:	
		params["tries"] = "."

	params = get_params(params)
	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


#################################################
#												#
# 			AppSalar Start funcation 			#
#												#
#################################################

def apsalarStart(campaign_data, app_data, device_data, ifu = "IDFV"):
	custom_user_id_create(app_data)

	if not app_data.get('s'):
		app_data['s'] = str(int(time.time())-random.randint(5,10))

	name = urllib.quote(campaign_data.get('app_name'))	

	method = "get"
	url = "http://e-ssl.apsalar.com/api/v1/start"
	headers = {
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {
		"a" : campaign_data.get('apsalar').get('key'),
		"av" : campaign_data.get('app_version_name'),
		"c" : "wifi",
		"cr" : "1",
		"d" : device_data.get('device_platform','iPhone5,2'),
		"ddl_enabled" : "true",
		"ddl_to" : "60",
		"device_type" : device_data.get('device_type'),
		"dnt" : "1",
		"i" : campaign_data.get('package_name'),
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"install_receipt" : install_receipt(),
		"install_time" : str(int(app_data.get('times').get('install_complete_time'))),
		"is" : "true",
		"k" : ifu,
		"lag" : str(generateLag()),
		"n" : name,
		"p" : "iOS",
		"rt" : "plist",
		"s" : app_data.get('s'),
		"sc" : "fb110555792951456",
		"scs" : "[\"nmnanatsunotaizai\",\"fb110555792951456\"]",
		"sdk" : campaign_data.get('apsalar').get('version'),
		"u" : app_data.get('idfv_id'),
		"update_time" : str(int(app_data.get('times').get('install_complete_time'))),
		"v" : device_data.get('os_version'),

		}
	data = {

		}

	

	if params.get('scs'):
		params['scs'] = urllib.quote(str(params.get('scs')))

	params = get_params(params)
	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h

	app_data['apsalar_time']=time.time()

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


#################################################
#												#
# 			AppSalar Event funcation 			#
#												#
#################################################

def apsalar_event(campaign_data, app_data, device_data,event_name='',value='',custom_user_id=False, ifu = "IDFV"):
	custom_user_id_create(app_data)

	if not app_data.get('seq'):
		app_data['seq'] = 1

	if not app_data.get('s'):
		app_data['s'] = str(int(time.time())-random.randint(5,10))	

	t = str(time.time()-app_data.get('apsalar_time'))[:-8]
	method = "get"
	url = "http://e-ssl.apsalar.com/api/v1/event"
	headers = {
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {
		"a" : campaign_data.get('apsalar').get('key'),
		"av" : campaign_data.get('app_version_name'),
		"device_type" : device_data.get('device_type'),
		"e" : value,
		"i" : campaign_data.get('package_name'),
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"k" : ifu,
		"lag" : str(generateLag()),
		"n" : event_name,
		"p" : "iOS",
		"rt" : "plist",
		"s" : app_data.get('s'),
		"sdk" : campaign_data.get('apsalar').get('version'),
		"seq" : app_data.get('seq'),
		"t" : t,
		"u" : app_data.get('idfv_id'),

		}
	data = {

		}				
	if urllib.unquote(value)=='':
		if params.get('e'):
			del params['e']

	if custom_user_id:
		data['custom_user_id'] = 	app_data.get('custom_user_id')	



		
	params = get_params(params)

	params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h

	if app_data.get('seq'):
		app_data['seq']=app_data.get('seq')+1

	app_data['apsalar_time']=time.time()

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

	
#####################################################
#													#
# 			Apsalar Utility function 				#
#													#
#####################################################

def get_params(data_value):
	key = data_value.keys()
	a = ''
	for value in sorted(key):
		a += str(value)+'='+str(data_value.get(value))+'&'
	a = a.rsplit("&",1)[0]	

	return a
			
def generateLag():
	return (random.randint(40,300)*0.001)

def custom_user_id_create(app_data):
	if not app_data.get('custom_user_id'):
		app_data['custom_user_id']= '12035'+ str(random.randint(1000,9999))

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())


# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')


# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"

def install_receipt():
	return 'MIISiAYJKoZIhvcNAQcCoIISeTCCEnUCAQExCzAJBgUrDgMCGgUAMIICKQYJKoZIhvcNAQcBoIICGgSCAhYxggISMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgCNMA0CAQoCAQEEBRYDMTIrMA0CAQ0CAQEEBQIDAdWIMA4CAQECAQEEBgIES6LJ5jAOAgEJAgEBBAYCBFAyNTMwDgIBCwIBAQQGAgQGj3SjMA4CARACAQEEBgIEMaKExTAPAgEDAgEBBAcMBTEwMTA2MA8CARMCAQEEBwwFMTAwNzIwEAIBDwIBAQQIAgZJt vf DgwFAIBAAIBAQQMDApQcm9kdWN0aW9uMBgCAQQCAQIEEONpVdv3p03cwRuW0 boq1IwHAIBBQIBAQQUy7q19mB4OYmkmTASPPCrSkYLP80wHgIBCAIBAQQWFhQyMDE5LTA5LTI3VDEwOjU5OjAzWjAeAgEMAgEBBBYWFDIwMTktMDktMjdUMTA6NTk6MDNaMB4CARICAQEEFhYUMjAxOS0wNi0xM1QwODowNDo1MFowJwIBAgIBAQQfDB1jb20ubmV0bWFyYmxlLm5hbmF0c3Vub3RhaXphaTA4AgEHAgEBBDCLV/RlFJdfGmZ4ZcFG5f0HJe5h XXdB28VMbiwfQRGTYrsBizjt3GVhWOqkNvJdsgwRgIBBgIBAQQ L9p0cya6mL5uZE/pTBxlaVFfIikpi5D4FWv7ba7DQeLiPtSdx5JdXY1wOSWqu6ZtZemj3w0Bq6SfB6/mSGWggg5lMIIFfDCCBGSgAwIBAgIIDutXh eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc B/SWigVvWh 0j2jMcjuIjwKXEJss9xp/sSg1Vhv kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4 3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7 mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5 givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3 Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn 9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN DAb2yriphcrGvzcNFMI jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz 6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn 9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn 9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t 2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti 9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU 12TZ/wYdV2aeZuTJC 9jVcZ5 oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAGiwvExnlaFuWnCIl5EZCpXIZ6 ythmWC6TqC2yU8y/bEHcEHvNTZkPQ43 ARcknrFnrODojo3SuXY6v9hy6AmRBtLG7YJ6vjKKuzHr 2CvKIxMeokeivyGy/RMf0J8nAZV8v6zjWwyhxHW1h9EXIQmM/o yZ 1o93yP7c4VCbFU7/sO7sViZ2TvZx9cB3q8QWUH5vTBCwQJdxU0FL2H9zgmPhNBMpuiBE0xJ plSKIN89WxoD/Gj6/n3/8S UEIApJQ3MwrfzFB3R4 vgizZfpIwpcjHCK4ooKd4E0p2nwZKXY9W0TNRsC/N6tV6vkEaxtJu02klEj1aYlFr/bLOPA='
