# -*- coding: utf-8 -*-
from sdk import util
from sdk import installtimenew
from sdk import getsleep
from sdk import NameLists
import time
import string
import random
import datetime
import urllib
import uuid
import clicker
import Config
import json

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'br.com.icarros.androidapp',
	'app_name' 			 :'iCarros - Buy Cars',
	'app_version_name'	 :'4.13.10',#'4.13.9',#'4.13.8',#'4.12.6',
	'no_referrer'		 : False,
	'ctr'				 : 6,
	'device_targeting'   : True,
	'supported_os'		 :'4.4',		
	'supported_countries':'WW',
	'app_size'			 : 12.5,#11.52,
	'tracker':'adjust',
	'adjust':{
		'app_token'	: '9tq6t7tpbz0g',
		'sdk'		: 'android4.13.0',	
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:20,
		21:19,
		22:18,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	

	###########		INITIALIZE		############	

	print 'please wait installing...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	get_gcmToken(app_data)
	register_user(app_data,country='united states')

	app_data['adjust_begin_time'] = int(time.time())

	###########		CALLS		################	

	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	if app_data.get('referrer'):
		print '\nAdjust : SDK CLICK____________________________________'
		request=adjust_sdkclick(campaign_data, app_data, device_data,source='install_referrer')
		util.execute_request(**request)

	time.sleep(random.randint(2,4))	

	print '\nAdjust : SDK Info____________________________________'
	request=adjust_sdk_info(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(1,2))

	if app_data.get('referrer'):
		print '\nAdjust : SDK CLICK____________________________________'
		request=adjust_sdkclick(campaign_data, app_data, device_data,source='reftag')
		util.execute_request(**request)

	time.sleep(random.randint(2,4))	

	print '\nAdjust : ATTRIBUTION____________________________________'
	request = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)


	for _ in range(random.randint(1,3)):

		if random.randint(1,100) <= 95:

			#home page
			time.sleep(random.randint(10,60))
			print '\nAdjust : EVENT____________________________________'
			request=adjust_event(campaign_data, app_data, device_data,event_token='hk84hb') 
			util.execute_request(**request)


			if not app_data.get('logged_in') and random.randint(1,100) <= 63:

				#signup/ login
				time.sleep(random.randint(30,120))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='exlnp0') 
				util.execute_request(**request)
				app_data['logged_in'] = True

		
			if random.randint(1,100) <= 26:

				if not app_data.get('auth_call'):

					print "\nself_call_accounts_icarros\n"
					request=accounts_icarros( campaign_data, device_data, app_data )
					util.execute_request(**request)
					app_data['auth_call'] = True


				print "\nself call for deal ids\n"
				request=deal_ids_list_call( campaign_data, device_data, app_data )
				result = util.execute_request(**request)
				try:
					res = json.loads(result.get('data')).get('deals')
					globals()['deal_id'] = []

					for item in res:
						if item.get('dealId'):
							globals()['deal_id'].append(int(item.get('dealId')))

					if not globals().get('deal_id'):
						globals()['deal_id'] = [26042846, 26041478, 26115942, 24305472, 24455804, 25273296, 25273239, 25273190, 25273210, 26285742, 25273189, 26041527, 26800568, 26585212, 26801483, 26043095, 24408355, 26215525, 26214955, 26041577]

					print 'ok response for deal id'

				except:
					print 'exception for deal id'
					globals()['deal_id'] = [26042846, 26041478, 26115942, 24305472, 24455804, 25273296, 25273239, 25273190, 25273210, 26285742, 25273189, 26041527, 26800568, 26585212, 26801483, 26043095, 24408355, 26215525, 26214955, 26041577]



				#DEALS_LIST
				partner_params = json.dumps({"ids": ",".join([str(n) for n in globals()['deal_id']])}) # list of deal ids
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='5kuyd8', partner_params = partner_params) 
				util.execute_request(**request)

				if random.randint(1,100) <= 88:

					choose_deal = random.choice(globals().get('deal_id'))

					# deal_details
					partner_params = json.dumps({"dealId": str(choose_deal)})
					time.sleep(random.randint(10,60))
					print '\nAdjust : EVENT____________________________________'
					request=adjust_event(campaign_data, app_data, device_data,event_token='ke8vqc', partner_params = partner_params) 
					util.execute_request(**request)


					print "\nself call to get lead id\n"
					request=lead_id_call( campaign_data, device_data, app_data, deal = int(choose_deal) )
					result = util.execute_request(**request)
					try:
						globals()['lead_id'] = int(json.loads(result.get('data')).get('leadId'))
						if not globals().get('lead_id'):
							globals()['lead_id'] = random.choice([13651632, 13651946, 19186435, 19186623])
						print 'ok response for lead id'
						print globals()['lead_id']
					except:
						print 'exception for lead id'
						globals()['lead_id'] = random.choice([13651632, 13651946, 19186435, 19186623])

					if random.randint(1,100) <= 40:

						# lead_sent
						partner_params = json.dumps({"dealId":str(choose_deal),"leadId":str(globals()['lead_id'])})
						time.sleep(random.randint(30,120))
						print '\nAdjust : EVENT____________________________________'
						request=adjust_event(campaign_data, app_data, device_data,event_token='j7p7sf', partner_params = partner_params) 
						util.execute_request(**request)

						# chat
						time.sleep(random.randint(5,10))
						print '\nAdjust : EVENT____________________________________'
						request=adjust_event(campaign_data, app_data, device_data,event_token='o0gcl0') 
						util.execute_request(**request)

					if random.randint(1,100) <= 40:

						# financing sent
						partner_params = json.dumps({"dealId":str(choose_deal),"leadId":str(globals()['lead_id'])})
						time.sleep(random.randint(30,120))
						print '\nAdjust : EVENT____________________________________'
						request=adjust_event(campaign_data, app_data, device_data,event_token='jqxrry', partner_params = partner_params) 
						util.execute_request(**request)

						# chat
						time.sleep(random.randint(5,10))
						print '\nAdjust : EVENT____________________________________'
						request=adjust_event(campaign_data, app_data, device_data,event_token='o0gcl0') 
						util.execute_request(**request)


			if random.randint(1,100) <= 20:

				#home page
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='hk84hb') 
				util.execute_request(**request)


			if random.randint(1,100) <= 11:

				#HOME_CATALOG
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='4lnqfg') 
				util.execute_request(**request)


			if random.randint(1,100) <= 11:

				# financing_simulation_result
				time.sleep(random.randint(60,120))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='bn4r1c') 
				util.execute_request(**request)

			if random.randint(1,100) <= 6:

				# home_news
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='p1dgyo') 
				util.execute_request(**request)

			if random.randint(1,100) <=21  and app_data.get('logged_in'):

				# deal_chosen_plan
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='bmyn6t') 
				util.execute_request(**request)

			if random.randint(1,100) <= 16:

				# fipe_result
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='wz3xv2') 
				util.execute_request(**request)
	
	###########		FINALIZE	################

	set_appCloseTime(app_data)
	
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	

	###########		INITIALIZE		############	

	print 'please wait...'
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	get_gcmToken(app_data)
	register_user(app_data,country='united states')

	if not app_data.get('adjust_begin_time'):
		app_data['adjust_begin_time'] = int(time.time())

	###########		CALLS		################

	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data, isOpen = True)
	util.execute_request(**request)
	app_data['adjust_begin_time'] = int(time.time())


	#home page
	time.sleep(random.randint(10,60))
	print '\nAdjust : EVENT____________________________________'
	request=adjust_event(campaign_data, app_data, device_data,event_token='hk84hb') 
	util.execute_request(**request)


	print '\nAdjust : ATTRIBUTION____________________________________'
	request = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)


	for _ in range(random.randint(1,3)):

		if random.randint(1,100) <= 80:

			if not app_data.get('logged_in') and random.randint(1,100) <= 70:

				#signup/ login
				time.sleep(random.randint(30,120))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='exlnp0') 
				util.execute_request(**request)
				app_data['logged_in'] = True
		
			if random.randint(1,100) <= 22:

				if not app_data.get('auth_call'):

					print "\nself_call_accounts_icarros\n"
					request=accounts_icarros( campaign_data, device_data, app_data )
					util.execute_request(**request)
					app_data['auth_call'] = True

				print "\nself call for deal ids\n"
				request=deal_ids_list_call( campaign_data, device_data, app_data )
				result = util.execute_request(**request)
				try:
					res = json.loads(result.get('data')).get('deals')
					globals()['deal_id'] = []

					for item in res:
						if item.get('dealId'):
							globals()['deal_id'].append(int(item.get('dealId')))

					if not globals().get('deal_id'):
						globals()['deal_id'] = [26042846, 26041478, 26115942, 24305472, 24455804, 25273296, 25273239, 25273190, 25273210, 26285742, 25273189, 26041527, 26800568, 26585212, 26801483, 26043095, 24408355, 26215525, 26214955, 26041577]

					print 'ok response for deal id'

				except:
					print 'exception for deal id'
					globals()['deal_id'] = [26042846, 26041478, 26115942, 24305472, 24455804, 25273296, 25273239, 25273190, 25273210, 26285742, 25273189, 26041527, 26800568, 26585212, 26801483, 26043095, 24408355, 26215525, 26214955, 26041577]


				#DEALS_LIST
				partner_params = json.dumps({"ids": ",".join([str(n) for n in globals()['deal_id']])}) # list of deal ids
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='5kuyd8', partner_params = partner_params) 
				util.execute_request(**request)

				if random.randint(1,100) <= 80:

					choose_deal = random.choice(globals().get('deal_id'))

					# deal_details
					partner_params = json.dumps({"dealId": str(choose_deal)})
					time.sleep(random.randint(10,60))
					print '\nAdjust : EVENT____________________________________'
					request=adjust_event(campaign_data, app_data, device_data,event_token='ke8vqc', partner_params = partner_params) 
					util.execute_request(**request)

					print "\nself call to get lead id\n"
					request=lead_id_call( campaign_data, device_data, app_data, deal = int(choose_deal) )
					result = util.execute_request(**request)
					try:
						globals()['lead_id'] = int(json.loads(result.get('data')).get('leadId'))
						if not globals().get('lead_id'):
							globals()['lead_id'] = random.choice([13651632, 13651946, 19186435, 19186623])
						print 'ok response for lead id'
						print globals()['lead_id']
					except:
						print 'exception for lead id'
						globals()['lead_id'] = random.choice([13651632, 13651946, 19186435, 19186623])

					if random.randint(1,100) <= 32:

						# lead_sent
						partner_params = json.dumps({"dealId":str(choose_deal),"leadId":str(globals()['lead_id'])})
						time.sleep(random.randint(30,120))
						print '\nAdjust : EVENT____________________________________'
						request=adjust_event(campaign_data, app_data, device_data,event_token='j7p7sf', partner_params = partner_params) 
						util.execute_request(**request)

						# chat
						time.sleep(random.randint(5,10))
						print '\nAdjust : EVENT____________________________________'
						request=adjust_event(campaign_data, app_data, device_data,event_token='o0gcl0') 
						util.execute_request(**request)

					if random.randint(1,100) <= 32:

						# financing sent
						partner_params = json.dumps({"dealId":str(choose_deal),"leadId":str(globals()['lead_id'])})
						time.sleep(random.randint(30,120))
						print '\nAdjust : EVENT____________________________________'
						request=adjust_event(campaign_data, app_data, device_data,event_token='jqxrry', partner_params = partner_params) 
						util.execute_request(**request)

						# chat
						time.sleep(random.randint(5,10))
						print '\nAdjust : EVENT____________________________________'
						request=adjust_event(campaign_data, app_data, device_data,event_token='o0gcl0') 
						util.execute_request(**request)

			if random.randint(1,100) <= 15:

				#home page
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='hk84hb') 
				util.execute_request(**request)

			if random.randint(1,100) <= 9:

				#HOME_CATALOG
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='4lnqfg') 
				util.execute_request(**request)

			if random.randint(1,100) <= 9:

				# financing_simulation_result
				time.sleep(random.randint(60,120))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='bn4r1c') 
				util.execute_request(**request)

			if random.randint(1,100) <= 4:

				# home_news
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='p1dgyo') 
				util.execute_request(**request)

			if random.randint(1,100) <=14  and app_data.get('logged_in'):

				# deal_chosen_plan
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='bmyn6t') 
				util.execute_request(**request)

			if random.randint(1,100) <= 11:

				# fipe_result
				time.sleep(random.randint(10,60))
				print '\nAdjust : EVENT____________________________________'
				request=adjust_event(campaign_data, app_data, device_data,event_token='wz3xv2') 
				util.execute_request(**request)
	
	###########		FINALIZE	################

	set_appCloseTime(app_data)

	return {'status':'true'}

#########################evENTS###################

def accounts_icarros( campaign_data, device_data, app_data ):
	url= "https://accounts.icarros.com/auth/realms/icarros/tokens/registration"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Cache-Control": "no-cache",
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "User-Agent": get_ua(device_data),
        "charset": "utf-8"}

	params= None

	data= {       "client_id": "mobileapp",
        "email": app_data['user']['email'], 
        "firstName": app_data['user']['firstname'], 
        "lastName": app_data['user']['lastname'], 
        "newsletter": "false",
        "password": app_data['user']['password'], 
        "password-confirm": app_data['user']['password'], 
        "username": app_data['user']['email']}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def deal_ids_list_call( campaign_data, device_data, app_data ):
	url= "http://internal-android-services.icarros.com.br/rest/search/deals/%20/0/20/1"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "Cache-Control": "no-cache",
        "User-Agent": get_ua(device_data),
        "charset": "utf-8"}

	query = random.choice(["chevrolet", "fiat", "volkswagen", "ford", "hyundai", "honda", "toyota", "renault", "nissan", "mitsubishi", "kia", "jeep", "bmw", "audi"])

	params= {       "includeAMDeals": random.choice(['true', 'false']), "openSearch": query}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


def lead_id_call( campaign_data, device_data, app_data, deal = '' ):
	url= "http://internal-android-services.icarros.com.br/rest/interaction/leadwithanalysis"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Cache-Control": "no-cache",
        "Content-Type": "application/json; charset=UTF-8",
        "User-Agent": get_ua(device_data),
        "charset": "utf-8"}

	params= None

	data= {       "dealId": deal,
        "message": "",
        "newsletter": False,
        "regionalGroup": 0,
        "senderEmail": app_data['user']['email'],
        "senderName": app_data['user']['firstname'] + ' ' + app_data['user']['lastname'],
        "senderPhone": app_data['user']['senderPhone'],
        "senderPhoneAreaCode": app_data['user']['senderPhoneCode'],
        "tradeIn": False,
        "wantsFinance": False}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,isOpen=False):

	set_androidUUID(app_data)
	set_installedAT(app_data,device_data)
	inc_(app_data,'session_count')

	url 	= 'http://app.adjust.com/session'
	method 	= 'post'
	headers = {
		'Client-SDK'		: campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/x-www-form-urlencoded',
		'User-Agent'		: get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'connectivity_type'	    :'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:get_date(app_data,device_data,early=random.uniform(10,12)),
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:get_date(app_data,device_data),
		'session_count'			:app_data.get('session_count'),
		'tracking_enabled'		:'1',
		'updated_at'			:get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		'vm_isa'				:'arm',
		'network_type'		    :'0',
		# 'push_token'           : app_data.get('push_token'),
		}
	if isOpen:
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['created_at'] 		= get_date(app_data,device_data,early=random.uniform(1,3))
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('appCloseTime') - app_data.get('adjust_begin_time')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('appCloseTime') - app_data.get('adjust_begin_time')

	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,source,isFirstCall=False):

	inc_(app_data,'subsession_count')
	url 	= 'http://app.adjust.com/sdk_click'
	method 	='post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'click_time'			:get_date(app_data,device_data),
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:get_date(app_data,device_data,early=random.uniform(30,40)),
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'needs_response_details':'1',
		'referrer'				:urllib.unquote(app_data.get('referrer')),
		'sent_at'				:get_date(app_data,device_data),
		'source'				:source,
		'tracking_enabled'		:'1',
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		'language'				:device_data.get('locale').get('language'),
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'updated_at'			:get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		'vm_isa'				:'arm',
		'network_type'		    :'0',
		'session_count'		    :'1',#app_data.get('session_count'),
		'session_length'		:int(time.time()) - app_data.get('adjust_begin_time'),
		'time_spent'			:int(time.time()) - app_data.get('adjust_begin_time'),
		'connectivity_type'	    :'1',
		'subsession_count'      :'1',#app_data.get('subsession_count'),

		}
	
	if source == 'reftag':
		data['raw_referrer'] = app_data.get('referrer')
		data['last_interval'] = random.randint(1,8)
		data['push_token'] = app_data.get('push_token')
	else:
	    data['install_begin_time'] = get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time"))	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data, device_data):

	# set_sessionLength(app_data,forced=True,length=random.randint(1,5))
	url 	= 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	params = {
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:get_date(app_data,device_data,early=random.uniform(4,4.5)),
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'initiated_by'          :'backend',
		'needs_response_details':'1',
		'sent_at'				:get_date(app_data,device_data),
		'tracking_enabled'		:'1',
		}
	data = None	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_sdk_info()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_sdk_info(campaign_data, app_data, device_data):

	# set_sessionLength(app_data,forced=True,length=random.randint(1,5))
	url 	= 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	data = {
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:get_date(app_data,device_data,early=random.uniform(4,4.5)),
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'source'	            :'push',
		'needs_response_details':'1',
		'sent_at'				:get_date(app_data,device_data),
		'tracking_enabled'		:'1',
		'push_token'           : app_data.get('push_token'),
		}

	params = None
		
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################

def adjust_event(campaign_data, app_data, device_data,event_token,num=False, callback_params = None, partner_params = None):
	set_androidUUID(app_data)
	set_installedAT(app_data,device_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	def_sessionLength(app_data)
	# set_sessionLength(app_data,forced=True)

	if num:
		inc_(app_data,'subsession_count')

	url 	= 'http://app.adjust.com/event'
	method 	= 'post'
	headers = {
		'Client-SDK'		: campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/x-www-form-urlencoded',
		'User-Agent'		: get_ua(device_data),
		 }
	params=None
	data = {
	    'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'connectivity_type'	    :'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:get_date(app_data,device_data,early=random.uniform(1,3)),
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:get_date(app_data,device_data),
		'session_count'			:app_data.get('session_count'),
		'tracking_enabled'		:'1',
		'vm_isa'				:'arm',
		'network_type'		    :'0',
	    'event_token'			: event_token,
	    'event_count'           : app_data.get('event_count'),
	    'session_length'	    : int(time.time()) - app_data.get('adjust_begin_time'),
		'subsession_count'      : app_data.get('subsession_count'),
		'time_spent'		    : int(time.time()) - app_data.get('adjust_begin_time'),
		'push_token'            : app_data.get('push_token'),
		}

	# if data['event_count'] == 2:
	# 	data['callback_params'] = json.dumps(app_data.get('xuid')) 

	if callback_params:
		data['callback_params'] = callback_params

	if partner_params:
		data['partner_params'] = partner_params

	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}




#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	#st 	 = str(int(time.time()*1000))
	st=device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())

def u_id(app_data):
	if not app_data.get('u_id'):
		app_data['u_id']="13"+str(util.get_random_string('decimal',7))

def transcode(app_data):
	if not app_data.get('transcode'):
		app_data['tanscode']=str(util.get_random_string('decimal',10))

def extraData(app_data):
	if not app_data.get('extradata'):
		app_data['extradata']=str(util.get_random_string('decimal',30))

###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def set_installedAT(app_data,device_data):
	if not app_data.get('installed_at'):
		app_data['installed_at'] =  get_date(app_data,device_data,early=random.uniform(5,10))

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength'): #or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def advertiser_ref_id(app_data):
	if not app_data.get('advertiser_ref_id'):
		app_data['advertiser_ref_id'] = 'GPA.'+str(util.get_random_string('decimal',4))+'-'+str(util.get_random_string('decimal',4))+'-'+str(util.get_random_string('decimal',4))+'-'+str(util.get_random_string('decimal',5))

def time_Stamp():
	return int(time.time()*1000)	

def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])	

def def_callback_params(app_data):
	a=random_string(len=12,typ='numb')
	app_data['xuid'] = {"xuid":a}

def get_gcmToken(app_data):
	if not app_data.get('push_token'):
		app_data['push_token']=util.get_random_string('all',11)+':APA91b'+''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(198))
	return app_data.get('push_token') 	
#######decoding functions

def _decode_list(data):
   rv = []
   for item in data:
	   if isinstance(item, unicode):
		   item = item.encode('utf-8')
	   elif isinstance(item, list):
		   item = _decode_list(item)
	   elif isinstance(item, dict):
		   item = _decode_dict(item)
	   rv.append(item)
   return rv
def _decode_dict(data):
   rv = {}
   for key, value in data.iteritems():
	   if isinstance(key, unicode):
		   key = key.encode('utf-8')
	   if isinstance(value, unicode):
		   value = value.encode('utf-8')
	   elif isinstance(value, list):
		   value = _decode_list(value)
	   elif isinstance(value, dict):
		   value = _decode_dict(value)
	   rv[key] = value
   return rv

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date


def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= first_name+last_name+ str(random.randint(1,200)) + "@gmail.com"
		app_data['user']['password'] 	= username + util.get_random_string('decimal', random.randint(2,4))
		app_data['user']['senderPhone'] = util.get_random_string('decimal',9)
		app_data['user']['senderPhoneCode'] = str(random.randint(80,99))