# -*- coding: utf-8 -*-
from sdk import installtimenew,purchase
from sdk import getsleep
from sdk import util
from sdk import NameLists
import time
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config
import string


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = { 
	'package_name'		 :'com.kayac.koshien_pocket',
	'app_size'			 : 77.0,#75.0,#76.0,#75.0,#70.0,#69.0,
	'app_name' 			 :'ぼくらの甲子園！ポケット　高校野球ゲーム',
	'app_version_name' 	 : '6.22.0',#'6.21.0',#'6.20.0',#'6.19.1',#'6.19.0',#6.18.0, '6.17.0',#'6.16.2',#'6.16.0',#'6.15.1',#'6.15.0',
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker' 		 : 'Adjust',
	'adjust'		 : {
		'app_token'	 : '90twj7hjd6v4',
		'sdk'		 : 'unity4.12.4@android4.12.4',
		'secret_key' : '8869716671312088963515817391578061342',
		'secret_id'	 : '4',
		},
	'adforce'		 	    : {
		'app_id'		    : '2356',
		'sdk_version'		: '3.7.1',
		'_gms_version'		: '12451000',
	},
	'purchase_var':{
							'1':{"amount":"100",'sku':'biz.kayac.kuwata.pokeg_100'},
							'2':{"amount":"500",'sku':'biz.kayac.kuwata.pokeg_600'},
							'3':{"amount":"900",'sku':'biz.kayac.kuwata.pokeg_1100'}	
				},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	pushtoken(app_data)
	set_androidUUID(app_data)
	if not app_data.get('install_id'):
		app_data['install_id'] = str(uuid.uuid4())
	################generating_realtime_differences##################
	s1=2
	s2=3
	a1=4
	a2=9
	r1=4
	r2=5
	click_time1=True
	c1=5
	c2=9
	ir1=2
	ir2=3
	click_time2=True
	e1=1
	e2=2

 ###########	 CALLS		 ############
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
		
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time2,source='install_referrer',t1=ir1,t2=ir2)
	util.execute_request(**request)
		
	print '\n'+"-----------------------------Ad-Force tmck---------------------------------"
	call = adforce_tmck(campaign_data, app_data, device_data)
	result=util.execute_request(**call)
	try:
		app_data['uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]
	except:
		app_data['uid']='foxGC0lu1gEW6E'
		
	time.sleep(random.randint(1,2))
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time1,source='reftag',t1=r1,t2=r2,t3=c1,t4=c2)
	util.execute_request(**request)
		
	print '\n'+"----------------------------- adforce Analytics ------------------------------"
	call = adforce_analytics(campaign_data, app_data, device_data)
	util.execute_request(**call)
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)
		
	print '\n'+"----------------------------- adforce Analytics ------------------------------"
	call = adforce_analytics(campaign_data, app_data, device_data)
	util.execute_request(**call)
		
	print '\n'+"----------------------------- adforce fprc ------------------------------"
	call = adforce_fprc(campaign_data, app_data, device_data)
	util.execute_request(**call)
		
	print '\n'+"----------------------------- adforce fp ------------------------------"
	call = adforce_fp(campaign_data, app_data, device_data)
	result=util.execute_request(**call)
	global fp_ID, fp_TDL
	try:
		uid=json.loads(result.get('data'))
		fp_ID = uid.get('id')
		fp_TDL = uid.get('tdl')
	except:
		fp_ID = '6%2E8%2E2%3AA314A6E4FB54ADB3FCDB834CAE7B4E044CA858E7%7Ctid%3AtGH3CB1gClM5'
		fp_TDL = '-1206245842'
		
	time.sleep(random.randint(1,2))
	print '\n'+"----------------------------- adforce cv ------------------------------"
	call = adforce_cv(campaign_data, app_data, device_data)
	result=util.execute_request(**call)
	try:
		app_data['cookie_uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]
	except:
		app_data['cookie_uid']='00bb2cf133ad737d9b8dcf15238d52e0c9'

	# time.sleep(random.randint(3,5))
	# print "\nself_call_kuwata_kaya\n"
	# request=kuwata_kaya_required_version( campaign_data, device_data, app_data )
	# response = util.execute_request(**request)
	# try:
	# 	result = json.loads(response.get('data')).get('result').get('client_data_version')
	# 	app_data['client_data_version']=result
	# except:
	# 	app_data['client_data_version']="2276"	

	# time.sleep(random.randint(25,35))
	# print "\nself_call_kuwata_kaya\n"
	# request=kuwata_kaya_startup( campaign_data, device_data, app_data )
	# util.execute_request(**request)	

	# time.sleep(random.randint(1,2))
	# print "\nself_call_kuwata_kaya\n"
	# request=kuwata_kaya_request_challenge( campaign_data, device_data, app_data )
	# response = util.execute_request(**request)
	# try:
	# 	result = json.loads(response.get('data')).get('result').get('challenge')
	# 	app_data['challenge']=result
	# except:
	# 	app_data['challenge']="fe5e8f05f752f2f6ae10bb17e1c659004a97dc5a6d6902b0a429669d46f05f11"	

	print '\n-----------Playing Game-----------------'
	time.sleep(random.randint(8*60,10*60))		

	if random.randint(1,100)<=95:
		time.sleep(random.randint(45,60))
		print '\n'+"----------------------------- adforce Analytics ------------------------------"
		call = adforce_analytics(campaign_data, app_data, device_data)
		util.execute_request(**call)
		app_data['login']=True

		print '\n-----------Playing Game-----------------'
		time.sleep(random.randint(10*60,15*60))	

		if random.randint(1,100)<=90:
			time.sleep(random.randint(25,30))
			print '\n'+"----------------------------- adforce cv short ------------------------------"
			call = adforce_cv_short(campaign_data, app_data, device_data)
			util.execute_request(**call)

			time.sleep(random.randint(1,5))
			adjust_token_y2ali8(campaign_data, app_data, device_data,e1,e2)


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	
	pushtoken(app_data)
	set_androidUUID(app_data)
	if not app_data.get('install_id'):
		app_data['install_id'] = str(uuid.uuid4())
	################generating_realtime_differences##################
	s1=2
	s2=3
	e1=1
	e2=2

 ###########	 CALLS		 ############
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,e1,e2,isOpen=True)
	util.execute_request(**request)
		
	time.sleep(random.randint(1,5))
	print '\n'+"----------------------------- adforce Analytics ------------------------------"
	call = adforce_analytics(campaign_data, app_data, device_data)
	util.execute_request(**call)

	if not app_data.get('login'):
		time.sleep(random.randint(45,60))
		print '\n'+"----------------------------- adforce Analytics ------------------------------"
		call = adforce_analytics(campaign_data, app_data, device_data)
		util.execute_request(**call)		
		app_data['login']=True
	
	print '\n-----------Playing Game-----------------'
	time.sleep(random.randint(10*60,15*60))	

	if app_data.get('login')==True:
		if purchase.isPurchase(app_data,day,advertiser_demand=10):
			
			rr=random.randint(1,100)

			if rr<=80:
				choose='1'
			elif rr<=90:
				choose='2'
			else:
				choose='3'

			amt=campaign_data.get('purchase_var').get(choose).get('amount')
			sku=campaign_data.get('purchase_var').get(choose).get('sku')

			time.sleep(random.randint(1,5))
			adjust_token_qplovm(campaign_data, app_data, device_data,t1=0,t2=0,revenue=amt)

			time.sleep(random.randint(1,5))
			print '\n'+"----------------------------- adforce Analytics ------------------------------"
			call = adforce_analytics(campaign_data, app_data, device_data)
			util.execute_request(**call)

			time.sleep(random.randint(1,5))
			print '\n'+"----------------------------- adforce cv short ------------------------------"
			call = adforce_cv_short(campaign_data, app_data, device_data,revenue=amt,name=sku)
			util.execute_request(**call)


	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def adjust_token_y2ali8(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________y2ali8'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='y2ali8',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_qplovm(campaign_data, app_data, device_data,t1=0,t2=0,revenue=''):
	print 'Adjust : EVENT______________________________qplovm'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='qplovm',t1=t1,t2=t2,revenue=revenue)
	util.execute_request(**request)

###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	pushtoken(app_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())

	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : 'JP',
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"language" : 'ja',
		"needs_response_details" : "1",
		"network_type" : "0",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"vm_isa" : "arm",

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if isOpen:
		def_(app_data,'subsession_count')
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('appCloseTime')-app_data.get('sess_len')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('appCloseTime')-app_data.get('sess_len')
		app_data['sess_len']=int(time.time())

	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'session')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,t3=0,t4=0,callback_params=None,partner_params=None):
	pushtoken(app_data)
	reftag = ''
	if app_data.get('referrer'):
		temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
		if temp_b.get('adjust_reftag'):
			reftag = temp_b.get('adjust_reftag')[0]
	sdk_clk_time=get_date(app_data,device_data)
	time.sleep(random.randint(t3,t4))
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : 'JP',
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"install_begin_time" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time")),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : 'ja',
		"needs_response_details" : "1",
		"network_type" : "0",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"referrer" : "utm_source=(not%20set)&utm_medium=(not%20set)",
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : 1,
		"session_length" : session_length,
		"source" : source,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"vm_isa" : "arm",

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		if click_time:
			data['click_time']=sdk_clk_time
		elif data.get("click_time"):
			del data['click_time']

		if data.get('install_begin_time'):
			del data['install_begin_time']
		# del data['session_length']
		# del data['subsession_count']
		# del data['time_spent']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		if click_time:
			data['click_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("click_time"))
		elif data.get("click_time"):
			del data['click_time']
		
		if data.get('raw_referrer'):
			del data['raw_referrer']
		
		data['install_begin_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_begin_time"))
		# del data['last_interval']

	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'click')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,t1=0,t2=0):
	inc_(app_data,'subsession_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"attribution_deeplink" : "1",
		"created_at" : created_at,
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		"needs_response_details" : "1",
		"sent_at" : sent_at,
		"tracking_enabled" : "1",

		}
	data = {

		}
	
	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('gps_adid'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None,revenue=''):
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Accept-Encoding" : "gzip",
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"connectivity_type" : "1",
		"country" : 'JP',
		"cpu_type" : device_data.get('cpu_abi'),
		"created_at" : created_at,
		"device_manufacturer" : device_data.get('manufacturer'),
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"display_height" : device_data.get('resolution').split('x')[0],
		"display_width" : device_data.get('resolution').split('x')[1],
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"event_count" : app_data.get('event_count'),
		"event_token" : event_token,
		"gps_adid" : device_data.get('adid'),
		"hardware_name" : device_data.get('hardware'),
		"language" : 'ja',
		"needs_response_details" : "1",
		"network_type" : "0",
		"os_build" : device_data.get('build'),
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"screen_density" : get_screen_density(device_data),
		"screen_format" : get_screen_format(device_data),
		"screen_size" : get_screen_size(device_data),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"session_length" : session_length,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		"vm_isa" : "arm",

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if event_token=='qplovm':
		data['currency']='JPY'
		data['revenue']=revenue+'.00000'
	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adforce_tmck()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
###################################################################
def adforce_tmck(campaign_data, app_data, device_data):

	method = "get"
	url = "http://app-adforce.jp/ad/p/tmck"
	headers = {
		"User-Agent" : 'ADMAGESMPHSDK/Android/'+campaign_data.get('adforce').get('sdk_version')+'/CZ/'+device_data.get('os_version')+'/'+device_data.get('model')+'/'+device_data.get('device'),

		}
	params = {
				'_app':	campaign_data.get('adforce').get('app_id'),
				'_bundle_id': campaign_data.get('package_name'),
				'_bv':	campaign_data.get('app_version_name'),
				'_model': device_data.get('model'),
				'_os_ver':	device_data.get('os_version'),
				'_sdk_ver':	campaign_data.get('adforce').get('sdk_version'),

		}
	data = {

		}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adforce_analytics()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_analytics(campaign_data, app_data, device_data):

	method = "post"
	url = "http://analytics.app-adforce.jp/fax/analytics"
	headers = {
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : 'ADMAGESMPHSDK/Android/'+campaign_data.get('adforce').get('sdk_version')+'/CZ/'+device_data.get('os_version')+'/'+device_data.get('model')+'/'+device_data.get('model'),

		}
	params = {

		}
	data = {
		"d" : 0,
		"e" : 1,
		"o" : 2,
		"p" : '00'+util.md5(device_data.get('adid')),
		"v" : campaign_data.get('adforce').get('sdk_version'),

		}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adforce_fprc()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_fprc(campaign_data, app_data, device_data):

	method = "get"
	url = "http://app-adforce.jp/ad/i/fprc"
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "gzip, deflate",
		"Accept-Language" : 'ja'+'-'+'JP',
		"Referer" : "https://app-adforce.jp/ad/view/collect.html",
		"User-Agent" : device_data.get('User-Agent'),
		"X-Requested-With" : campaign_data.get('package_name'),

		}
	params = {

		}
	data = {

		}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adforce_fp()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_fp(campaign_data, app_data, device_data):

	method = "get"
	url = "http://app-adforce.jp/ad/p/fp"
	headers = {
		"Accept" : "application/json, text/javascript, */*; q=0.01",
		"Accept-Encoding" : "gzip, deflate",
		"Accept-Language" : 'ja'+'-'+'JP',
		"Referer" : "https://app-adforce.jp/ad/view/collect.html",
		"User-Agent" : device_data.get('User-Agent'),
		"X-Requested-With" : campaign_data.get('package_name'),

		}
	params = {
		"_fpsd" : "sda44j1d7lY5BNvcKyAdMUDFBpBeA0fUm4ly_03y8rnawSdrVhXUMzJjc4BLRS5ooFrJX8vihYz3ccbbJYMLgiPFU77qZoOSix5ezdstlYyIwQOqZb0hysrhsui6Hahtd__Wgm6cPiAnqgXK_Pmtd0SHp815LyjaY2.rINj.rINM6uJ6waxKQpGjftckcKyAd65hz7M6uJ3IsbUDQlqbT1q8xUxHyVJlOloQIG2SH7biuqEwHXXTSHCSPmtd0wVYPIG_qvoPfybYb5EtElhbPHR5rvYTrYesS9PraaLL5n7TWesjEPm8LKfAaZ4ySy.aPjftcktJJNBdozlQeRnEJMpwoNSUC56MnGWpwoNHHACVZXnN9N0I9CpI1e5LtgnM.S9RdPQSzOy_Aw7UTlfd.k.B7lr93pw1JgN3dN.Rc0mX6QStNMsNMuNUXGfe2S75.0NNW5BNlYiMgBNlY0a5rVvMiADIhre0zSIrycWqy3nwgAxBExBPNNq9y.DQf",

		}
	data = {

		}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adforce_cv()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_cv(campaign_data, app_data, device_data):

	method = "get"
	url = "http://app-adforce.jp/ad/p/cv"
	headers = {
		"User-Agent" : 'ADMAGESMPHSDK/Android/'+campaign_data.get('adforce').get('sdk_version')+'/CZ/'+device_data.get('os_version')+'/'+device_data.get('model')+'/GL',

		}
	params = {
				'_adid':'00'+util.md5(device_data.get('adid')),
				'_adte': 1,
				'_app':	campaign_data.get('adforce').get('app_id'),
				'_build': device_data.get('build'),
				'_bundle_id': campaign_data.get('package_name'),
				'_country' : 'JP',
				'_cv_target' : 'true',
				"_fpid" : urlparse.unquote(str(fp_ID)),
				"_fptdl" : fp_TDL,
				'_gms_version': '9452000',
				'_hash': util.get_random_string('hex',40),
				'_install_id' : app_data.get('install_id'),
				'_language' : 'ja',
				'_local_xuniq' : 0,
				'_model':device_data.get('model'),
				'_os_ver':device_data.get('os_version'),
				'_referrer': 'utm_source=(not set)&utm_medium=(not set)',
				'_rurl' : 'default',
				'_sdk_ver':campaign_data.get('adforce').get('sdk_version'),
				'_ua': urllib.quote(device_data.get('User-Agent')),
				'_use_bw' : 'false',
				'_xevent' : 1,
				'_xuniq':'00'+util.md5(device_data.get('adid')),				
				'_xuniq_type' : 4,
				'_xuid':'',	
		}
	data = {

		}
	if app_data.get('uid'):
		headers['Cookie']='uid='+app_data.get('uid')
		
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('_xroute'):
			params['_xroute'] = temp_a['_xroute'][0]
			
		if temp_a.get('_xuid'):
			params['_xuid'] = temp_a['_xuid'][0]

		
		params['_referrer'] = urlparse.unquote(app_data.get('referrer'))

	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adforce_cv()	: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def adforce_cv_short(campaign_data, app_data, device_data, revenue=None, name=None):

	method = "get"
	url = "http://app-adforce.jp/ad/p/cv"
	headers = {
		"User-Agent" : 'ADMAGESMPHSDK/Android/'+campaign_data.get('adforce').get('sdk_version')+'/CZ/'+device_data.get('os_version')+'/'+device_data.get('model')+'/GL',

		}
	params = {
		"_adid" : '00'+util.md5(device_data.get('adid')),
		"_adte" : 1,
		'_app':	campaign_data.get('adforce').get('app_id'),
		"_buid" : random.randint(2290000,2299999),
		"_cvpoint" : random.randint(3100,3199),
		"_model" : device_data.get('model'),
		"_xtid" : str(uuid.uuid4()),
		'_xuid':'',	
		"_xuniq" : '00'+util.md5(device_data.get('adid')),

		}
	data = {

		}
	if revenue:
		params['_price']=revenue
		params['_sku']=name

	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
			
		if temp_a.get('_xuid'):
			params['_xuid'] = temp_a['_xuid'][0]	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

#################################################################
#
#			Self Calls
#
#################################################################

def kuwata_kaya_required_version( campaign_data, device_data, app_data ):
	url= "https://kuwata.kayac.com/api/android/required_version"
	method= "get"
	headers= {       
		"Accept-Encoding": "gzip",
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": get_ua(device_data),
        "X-Kuwata-App-Version": "6.015000",
        "X-Unity-Version": "5.6.3p4"
        }

	params= {       
				"client_data_version": "0"
			}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def kuwata_kaya_startup( campaign_data, device_data, app_data ):
	url= "https://kuwata.kayac.com/api/android/startup-u5.json"
	method= "get"
	headers= {       
		"Accept-Encoding": "gzip",
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": get_ua(device_data),
        "X-Kuwata-App-Version": "6.015000",
        "X-Unity-Version": "5.6.3p4"
        }

	params= {       
				"client_data_version": app_data.get('client_data_version')
			}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	


def kuwata_kaya_request_challenge( campaign_data, device_data, app_data ):
	url= "https://kuwata.kayac.com/api/request_challenge"
	method= "get"
	headers= {       
		"Accept-Encoding": "gzip",
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": get_ua(device_data),
        "X-Kuwata-App-Version": "6.015000",
        "X-Unity-Version": "5.6.3p4"
        }

	params= {       
				"client_data_version": app_data.get('client_data_version')
			}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

	
# def kuwata_kaya_pass_check_point( campaign_data, device_data, app_data ):
# 	url= "https://kuwata.kayac.com/api/tutorial/pass_check_point"
# 	method= "post"
# 	headers= {       
# 		"Accept-Encoding": "gzip",
#         "Content-Type": "application/json; charset=utf-8",
#         "User-Agent": get_ua(device_data),
#         "X-Kuwata-App-Version": "6.015000",
#         "X-Unity-Version": "5.6.3p4"
#         }

# 	params= None

# 	if not app_data.get('uuid'):
# 		app_data['uuid'] = str(uuid.uuid4())

# 	data= {       
# 		"challenge": app_data.get('challenge'),
#         "check_point": 1,
#         "client_data_version": app_data.get('client_data_version'),
#         "response": "1d56bda04c73ebd10852d63a403979875d298c713773c82f729fcb1844ac0ec0",#util.get_random_string(type='hex',size=64),
#         "uuid": app_data.get('uuid'),
#         }

# 	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}	


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	return app_data.get('android_uuid')


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']='f0L4WFS5AEk:'+util.get_random_string('google_token',140)
	return app_data.get('pushtoken')

def get_auth(device_data,app_data,created_at,gps_adid,activity_type):
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activity_type)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'

def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	