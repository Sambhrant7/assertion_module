# -*- coding: utf8 -*-
import sys
from sdk import getsleep
reload(sys)
sys.setdefaultencoding('utf-8')

from sdk import installtimenew
from sdk import util
import time
import random
import json
import datetime
import uuid
import clicker
import Config
from geopy.geocoders import Nominatim
from sdk import NameLists


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


####
####	HMA of Germany required.
####


campaign_data = {
	'package_name'		 :'com.tipico.bundesliga6',
	'app_size'		     :27.8,
	'app_name' 			 :'Bundesliga6',
	'app_version_name' 	 :'1.0',
	'app_version_code' 	 :'2018112701',
	'app_id' 			 : '1441962437',
	'ctr' 				 : 6,
	'sdk' 		 		 : 'ios',
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '9.0',
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 	: 'p6gszrewp728',
		'sdk'		 	: 'ios4.15.0',
		'app_updated_at': '2018-11-27T21:34:25.000Z',
		},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)
	install_receipt(app_data)
	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	################generating_realtime_differences##################
	s1=0
	s2=0
	a1=3
	a2=5
	id1=0
	id2=0
	click_time_iad3=False

	###########		CALLS		############

	print 'lat and lon generator'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	global lat,lon
	try:
		json_lat_lon_result=json.loads(lat_lon_result.get('data')).get('geo')
		lat=str(json_lat_lon_result.get('latitude'))
		lon=str(json_lat_lon_result.get('longitude'))
		print lat
		print lon 
		# time.sleep(10)
	except:
		print "Exception"
		lat= '51.329'
		lon= '12.3115'

	if lat==None or lat=='':
		lat= '51.329'
		lon= '12.3115'


	print "--------geolocator------------------------"

	value=str(lat)+','+str(lon)#"28.581356, 77.318415"
	try:
		geolocator = Nominatim(user_agent="bundesliga")
		location = geolocator.reverse(value,timeout=None)
		print "--------------------------"
		print location.raw
		global postal_code,city,country,street
		# try:
		postal_code=location.raw['address']['postcode'].encode('utf-8')
		city=location.raw['address']['city'].encode('utf-8')
		country=location.raw['address']['country_code'].encode('utf-8')
		street=location.raw['address']['road'].encode('utf-8')
		print postal_code
		print city
		print country
		print street
		# time.sleep(10)
		# except:
		# 	postal_code='04179'
		# 	city='Leipzig'
		# 	country='de'
		# 	street='Lützner Straße'
	except :
		print("Exception")
		postal_code='04179'
		city='Leipzig'
		country='de'
		street='Lützner Straße'

	if postal_code==None or postal_code=='':
		postal_code='04179'
		city='Leipzig'
		country='de'
		street='Lützner Straße'


	print "\nself_call_device_provisioning_googleapis\n"
	request=device_provisioning_googleapis( campaign_data, device_data, app_data )
	util.execute_request(**request)
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)
	app_data['api_hit_time'] = time.time()
	result=util.execute_request(**request)
	global adid 
	try:
		adid=json.loads(result.get('data')).get('adid')
		print adid
		# time.sleep(10)
	except:
		adid='d2e6719f78e272c9b744b61a6497f041'

	if adid==None or adid=='':
		adid='d2e6719f78e272c9b744b61a6497f041'
		
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time_iad3,source='iad3',t1=id1,t2=id2)
	util.execute_request(**request)

	print "\nself_call_tipico_de\n"
	request=tipico_de( campaign_data, device_data, app_data )
	response=util.execute_request(**request)
	global JSESSIONID,ak_bmsc,_abck,bm_sz
	try:
		JSESSIONID=response.get('res').headers.get('Set-Cookie').split('JSESSIONID=')[1].split(';')[0]
		ak_bmsc=response.get('res').headers.get('Set-Cookie').split('ak_bmsc=')[1].split(';')[0]
		_abck=response.get('res').headers.get('Set-Cookie').split('_abck=')[1].split(';')[0]
		bm_sz=response.get('res').headers.get('Set-Cookie').split('bm_sz=')[1].split(';')[0]
		print JSESSIONID
		print ak_bmsc
		print _abck
		print bm_sz
		# time.sleep(10)
	except:
		JSESSIONID='3CD0FE313CE593B4000F01FED49A2B4A.m23'
		ak_bmsc='2B523720D52633DC05981EDB095DF3860215F31E931700006372B95C1052E04F~plyeMqcaoN82jkF0/P6R6Xm6ub729gUSJEqvLEG3K/31FIP8zNVyV/T4fGgKKDn7qZDlihOUXegxCIdiv1Y6ZG7plZFocFV2pMqSzLgCzJ8e558ziaA37NdTd6KS8G6x1XfbPBffv7khpPm5TpGOoFAkuOiYFVfyYuTqyslcie0JucDwA/HmWz9VNZTgWvCmGGCn6xAdGu7DN534zDWwnzms2VUi+K+U/YzTfRJfsdUak='
		_abck='37E354CFBF40065CB981AF6FB44F27330215F31E931700006372B95CBCE0376A~-1~sS8jwvYmfYqQ3qwuZMVVnGnf7ZAZGdXs5K561097ye8=~-1~-1'
		bm_sz='5F47CB4C3220A9C4DD96CD56E0C0BE03~YAAQHvMVApuz9CFqAQAAH9ZmNAOszeZwxvCFGIZ3tDJ4jK05EjXUme+MekNv/BrdEEPUoqFJzUH6+9QhKdfFO5ZlfdTc/iMJtP0r2IBLRaGQXovp5TZ0apzlYCDmNbizmDmpp03rWpFMC2ZjvJ5S295LEyHg2PK7i+nY2++ZnWOGToQ9UzjpQmyJPvxjxOo='

	if JSESSIONID==None or JSESSIONID=='':
		JSESSIONID='3CD0FE313CE593B4000F01FED49A2B4A.m23'
	if ak_bmsc==None or ak_bmsc=='':
		ak_bmsc='2B523720D52633DC05981EDB095DF3860215F31E931700006372B95C1052E04F~plyeMqcaoN82jkF0/P6R6Xm6ub729gUSJEqvLEG3K/31FIP8zNVyV/T4fGgKKDn7qZDlihOUXegxCIdiv1Y6ZG7plZFocFV2pMqSzLgCzJ8e558ziaA37NdTd6KS8G6x1XfbPBffv7khpPm5TpGOoFAkuOiYFVfyYuTqyslcie0JucDwA/HmWz9VNZTgWvCmGGCn6xAdGu7DN534zDWwnzms2VUi+K+U/YzTfRJfsdUak='
	if _abck==None or _abck=='':
		_abck='37E354CFBF40065CB981AF6FB44F27330215F31E931700006372B95CBCE0376A~-1~sS8jwvYmfYqQ3qwuZMVVnGnf7ZAZGdXs5K561097ye8=~-1~-1'
	if bm_sz==None or bm_sz=='':
		bm_sz='5F47CB4C3220A9C4DD96CD56E0C0BE03~YAAQHvMVApuz9CFqAQAAH9ZmNAOszeZwxvCFGIZ3tDJ4jK05EjXUme+MekNv/BrdEEPUoqFJzUH6+9QhKdfFO5ZlfdTc/iMJtP0r2IBLRaGQXovp5TZ0apzlYCDmNbizmDmpp03rWpFMC2ZjvJ5S295LEyHg2PK7i+nY2++ZnWOGToQ9UzjpQmyJPvxjxOo='
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)

	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2, initiated_by = "sdk")
	util.execute_request(**request)

	time.sleep(random.randint(5,10))

	print "\nself_call_app-measurement\n"
	request=app_measurement( campaign_data, device_data, app_data )
	util.execute_request(**request)

	time.sleep(random.randint(5,10))


	print "\nself_call_bundesliga6_noenv\n"
	request=bundesliga6_noenv( campaign_data, device_data, app_data )
	util.execute_request(**request)


	if random.randint(1,100) <= 85:

		time.sleep(random.randint(5,10))

		print "\nself_call_account_tipico_de\n"
		request=register_tipico_de( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		global bm_mi
		try:
			bm_mi=response.get('res').headers.get('Set-Cookie').split('bm_mi=')[1].split(';')[0]
			print bm_mi
			# time.sleep(5)
		except:
			bm_mi='911C0DE121F3435361E250A81C99542C~3Jy9L+pessR/42SPT+Z+TcAA/n/wvmutrU/uYEI7/T/M96YaS0j4ifXYqVNDIcfpscKKitLROODAdo0qIKEPy0rbau2COVooEmxOXH2lRgNgmFeNxzLI6GUzHMm+IFaHd80aq3Va7JpBaZZP05nGAGc8+b6Qb4MHk0dS+b+XMvkPa74+SdvceW2rtb+TwsYmxXTk0UWsVrsHBMsO5xjuqAYZzgA4f5e9CNHXUakqC24='

		if bm_mi==None or bm_mi=='':
			bm_mi='911C0DE121F3435361E250A81C99542C~3Jy9L+pessR/42SPT+Z+TcAA/n/wvmutrU/uYEI7/T/M96YaS0j4ifXYqVNDIcfpscKKitLROODAdo0qIKEPy0rbau2COVooEmxOXH2lRgNgmFeNxzLI6GUzHMm+IFaHd80aq3Va7JpBaZZP05nGAGc8+b6Qb4MHk0dS+b+XMvkPa74+SdvceW2rtb+TwsYmxXTk0UWsVrsHBMsO5xjuqAYZzgA4f5e9CNHXUakqC24='


		print "\nself_call_account_tipico_de\n"
		request=account_tipico_de( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['registration'] = True

	app_data['app_close_time'] = int(time.time())	

	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	
	###########		INITIALIZE		############	

	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)

	install_receipt(app_data)

	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())

	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')

	if not app_data.get('idfv_id'):
		if not device_data.get('idfv_id'):
			app_data['idfv_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfv_id'] = device_data.get('idfv_id')

	if not app_data.get('app_close_time'):
		app_data['app_close_time'] = int(time.time())


	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 1


	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=0,t2=0, isOpen = True)
	result=util.execute_request(**request)
	global adid 
	try:
		adid=json.loads(result.get('data')).get('adid')
		print adid
		# time.sleep(10)
	except:
		adid='d2e6719f78e272c9b744b61a6497f041'

	if adid==None or adid=='':
		adid='d2e6719f78e272c9b744b61a6497f041'
	app_data['adjust_call_time'] = int(time.time())

	if not app_data.get('registration'):

		time.sleep(random.randint(10,20))

		print 'lat and lon generator'
		lat_lon=current_location(device_data)
		lat_lon_result = util.execute_request(**lat_lon)
		global lat,lon
		try:
			json_lat_lon_result=json.loads(lat_lon_result.get('data')).get('geo')
			lat=str(json_lat_lon_result.get('latitude'))
			lon=str(json_lat_lon_result.get('longitude'))
			print lat
			print lon 
			# time.sleep(10)
		except:
			print "Exception"
			lat= '51.329'
			lon= '12.3115'

		if lat==None or lat=='':
			lat= '51.329'
			lon= '12.3115'


		print "--------geolocator------------------------"

		value=str(lat)+','+str(lon)#"28.581356, 77.318415"
		try:
			geolocator = Nominatim(user_agent="bundesliga")
			location = geolocator.reverse(value,timeout=None)
			print "--------------------------"
			print location.raw
			global postal_code,city,country,street
			# try:
			postal_code=location.raw['address']['postcode'].encode('utf-8')
			city=location.raw['address']['city'].encode('utf-8')
			country=location.raw['address']['country_code'].encode('utf-8')
			street=location.raw['address']['road'].encode('utf-8')
			print postal_code
			print city
			print country
			print street
			# time.sleep(10)
			# except:
			# 	postal_code='04179'
			# 	city='Leipzig'
			# 	country='de'
			# 	street='Lützner Straße'
		except :
			print("Exception")
			postal_code='04179'
			city='Leipzig'
			country='de'
			street='Lützner Straße'

		if postal_code==None or postal_code=='':
			postal_code='04179'
			city='Leipzig'
			country='de'
			street='Lützner Straße'

		print "\nself_call_tipico_de\n"
		request=tipico_de( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		global JSESSIONID,ak_bmsc,_abck,bm_sz
		try:
			JSESSIONID=response.get('res').headers.get('Set-Cookie').split('JSESSIONID=')[1].split(';')[0]
			ak_bmsc=response.get('res').headers.get('Set-Cookie').split('ak_bmsc=')[1].split(';')[0]
			_abck=response.get('res').headers.get('Set-Cookie').split('_abck=')[1].split(';')[0]
			bm_sz=response.get('res').headers.get('Set-Cookie').split('bm_sz=')[1].split(';')[0]
			print JSESSIONID
			print ak_bmsc
			print _abck
			print bm_sz
			# time.sleep(10)
		except:
			JSESSIONID='3CD0FE313CE593B4000F01FED49A2B4A.m23'
			ak_bmsc='2B523720D52633DC05981EDB095DF3860215F31E931700006372B95C1052E04F~plyeMqcaoN82jkF0/P6R6Xm6ub729gUSJEqvLEG3K/31FIP8zNVyV/T4fGgKKDn7qZDlihOUXegxCIdiv1Y6ZG7plZFocFV2pMqSzLgCzJ8e558ziaA37NdTd6KS8G6x1XfbPBffv7khpPm5TpGOoFAkuOiYFVfyYuTqyslcie0JucDwA/HmWz9VNZTgWvCmGGCn6xAdGu7DN534zDWwnzms2VUi+K+U/YzTfRJfsdUak='
			_abck='37E354CFBF40065CB981AF6FB44F27330215F31E931700006372B95CBCE0376A~-1~sS8jwvYmfYqQ3qwuZMVVnGnf7ZAZGdXs5K561097ye8=~-1~-1'
			bm_sz='5F47CB4C3220A9C4DD96CD56E0C0BE03~YAAQHvMVApuz9CFqAQAAH9ZmNAOszeZwxvCFGIZ3tDJ4jK05EjXUme+MekNv/BrdEEPUoqFJzUH6+9QhKdfFO5ZlfdTc/iMJtP0r2IBLRaGQXovp5TZ0apzlYCDmNbizmDmpp03rWpFMC2ZjvJ5S295LEyHg2PK7i+nY2++ZnWOGToQ9UzjpQmyJPvxjxOo='

		if JSESSIONID==None or JSESSIONID=='':
			JSESSIONID='3CD0FE313CE593B4000F01FED49A2B4A.m23'
		if ak_bmsc==None or ak_bmsc=='':
			ak_bmsc='2B523720D52633DC05981EDB095DF3860215F31E931700006372B95C1052E04F~plyeMqcaoN82jkF0/P6R6Xm6ub729gUSJEqvLEG3K/31FIP8zNVyV/T4fGgKKDn7qZDlihOUXegxCIdiv1Y6ZG7plZFocFV2pMqSzLgCzJ8e558ziaA37NdTd6KS8G6x1XfbPBffv7khpPm5TpGOoFAkuOiYFVfyYuTqyslcie0JucDwA/HmWz9VNZTgWvCmGGCn6xAdGu7DN534zDWwnzms2VUi+K+U/YzTfRJfsdUak='
		if _abck==None or _abck=='':
			_abck='37E354CFBF40065CB981AF6FB44F27330215F31E931700006372B95CBCE0376A~-1~sS8jwvYmfYqQ3qwuZMVVnGnf7ZAZGdXs5K561097ye8=~-1~-1'
		if bm_sz==None or bm_sz=='':
			bm_sz='5F47CB4C3220A9C4DD96CD56E0C0BE03~YAAQHvMVApuz9CFqAQAAH9ZmNAOszeZwxvCFGIZ3tDJ4jK05EjXUme+MekNv/BrdEEPUoqFJzUH6+9QhKdfFO5ZlfdTc/iMJtP0r2IBLRaGQXovp5TZ0apzlYCDmNbizmDmpp03rWpFMC2ZjvJ5S295LEyHg2PK7i+nY2++ZnWOGToQ9UzjpQmyJPvxjxOo='


		print "\nself_call_account_tipico_de\n"
		request=register_tipico_de( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		global bm_mi
		try:
			bm_mi=response.get('res').headers.get('Set-Cookie').split('bm_mi=')[1].split(';')[0]
			print bm_mi
			# time.sleep(5)
		except:
			bm_mi='911C0DE121F3435361E250A81C99542C~3Jy9L+pessR/42SPT+Z+TcAA/n/wvmutrU/uYEI7/T/M96YaS0j4ifXYqVNDIcfpscKKitLROODAdo0qIKEPy0rbau2COVooEmxOXH2lRgNgmFeNxzLI6GUzHMm+IFaHd80aq3Va7JpBaZZP05nGAGc8+b6Qb4MHk0dS+b+XMvkPa74+SdvceW2rtb+TwsYmxXTk0UWsVrsHBMsO5xjuqAYZzgA4f5e9CNHXUakqC24='

		if bm_mi==None or bm_mi=='':
			bm_mi='911C0DE121F3435361E250A81C99542C~3Jy9L+pessR/42SPT+Z+TcAA/n/wvmutrU/uYEI7/T/M96YaS0j4ifXYqVNDIcfpscKKitLROODAdo0qIKEPy0rbau2COVooEmxOXH2lRgNgmFeNxzLI6GUzHMm+IFaHd80aq3Va7JpBaZZP05nGAGc8+b6Qb4MHk0dS+b+XMvkPa74+SdvceW2rtb+TwsYmxXTk0UWsVrsHBMsO5xjuqAYZzgA4f5e9CNHXUakqC24='


		print "\nself_call_account_tipico_de\n"
		request=account_tipico_de( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['registration'] = True

	app_data['app_close_time'] = int(time.time()) + random.randint(300,600)

	return {'status':'true'}

def current_location(device_data):
	url='http://lumtest.com/myip.json'
		
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}

def device_provisioning_googleapis( campaign_data, device_data, app_data ):
	url= "http://device-provisioning.googleapis.com/checkin"
	method= "post"
	headers= {       'Accept': '*/*',
        'Accept-Encoding': 'br, gzip, deflate',
        'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        'Content-Type': 'application/json',
        'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]}

	params= None

	data= {       'checkin': {       'iosbuild': {       'model': device_data.get('device_platform'),
                                               'os_version': 'IOS_'+device_data.get('os_version')},
                           'last_checkin_msec': 0,
                           'type': 2,
                           'user_number': 0},
        'digest': '',
        'fragment': 0,
        'id': 0,
        'locale': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
        'logging_id': 3515966563L,
        'security_token': 0,
        'timezone': device_data.get('local_tz_name'),
        'user_serial_number': 0,
        'version': 2}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def tipico_de( campaign_data, device_data, app_data ):
	url= "https://m.tipico.de/ajax/country"
	method= "get"
	headers= {       'Accept': '*/*',
        'Accept-Encoding': 'br, gzip, deflate',
        'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]}

	params= None

	data= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def app_measurement( campaign_data, device_data, app_data ):
	url= "http://app-measurement.com/config/app/1:227261920822:ios:90f41d0fd6b7400d"
	method= "head"
	headers= {       'Accept': '*/*',
        'Accept-Encoding': 'br, gzip, deflate',
        'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]}

	params= {       'app_instance_id': '0642A6D09A794CB391EB06A1967E20D4',
        'gmp_version': '40009',
        'platform': 'ios'}

	data= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def bundesliga6_noenv( campaign_data, device_data, app_data ):
	url= "https://bundesliga6.noenv.com/v1/api/config"
	method= "options"
	headers= {       'Accept': '*/*',
        'Accept-Encoding': 'br, gzip, deflate',
        'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        'Access-Control-Request-Headers': 'content-type',
        'Access-Control-Request-Method': 'GET',
        'Origin': 'https://bundesliga6.de',
        'Referer': 'https://bundesliga6.de/?client=iosb6',
        'User-Agent': device_data.get('User-Agent')}

	params= None

	data= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def register_tipico_de( campaign_data, device_data, app_data ):
	url= "https://account.tipico.de/registration"
	method= "get"
	headers= {       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding': 'br, gzip, deflate',
        'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        'Cookie': 'ak_bmsc='+ak_bmsc+'; _abck='+_abck+'; bm_sz='+bm_sz,
        'Referer': 'https://bundesliga6.de/welcome',
        'User-Agent': device_data.get('User-Agent')}

	params= {       'client': 'iosb6',
        'client_id': 'bundesliga6',
        'is_registration': 'true',
        'redirect_uri': 'https://bundesliga6.de/login',
        'response_type': 'code',
        'scope': 'scope:bundesliga6:customer:play'}

	data= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def account_tipico_de( campaign_data, device_data, app_data ):
	register_user(app_data,country='united states')
	globals()['gid']=str(int(time.time()))
	url= "https://account.tipico.de/v1/tpapi/crfes/register"
	method= "post"
	headers= {       'Accept': 'application/json',
        'Accept-Encoding': 'br, gzip, deflate',
        'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        'Content-Type': 'application/json',
        'Cookie': '_ga=GA1.2.1305859643.'+globals().get('gid')+'; _gid=GA1.2.517864287.'+globals().get('gid')+'; adjustADID='+adid+'; adjustIDFA='+app_data.get('idfa_id')+'; adjustIDFV='+app_data.get('idfv_id')+'; adjustToken=p6gszrewp728; JSESSIONID='+JSESSIONID+'; _hjIncludedInSample=1; language=en; ak_bmsc='+ak_bmsc+'; bm_mi='+bm_mi+'; _abck='+_abck+'; bm_sz='+bm_sz,
        'Origin': 'https://account.tipico.de',
        'Referer': 'https://account.tipico.de/registration/step2',
        'User-Agent': device_data.get('User-Agent'),
        'X-Requested-By': '97066a5c-fc78-11e8-8eb2-f2801f1b9fd1'}

	params= None

	if ' ' in app_data.get('user').get('username'):
		app_data['username']=app_data.get('user').get('username').replace(' ','')
	elif '.' in app_data.get('user').get('username'):
		app_data['username']=app_data.get('user').get('username').replace('.','')
	else:
		app_data['username']=app_data.get('user').get('username').replace('_','')

	if app_data.get('user').get('sex')=='male':
		salutation='Mr.'
	else:
		salutation='Mrs.'

	data= {       'birthCity': city,
        'birthCountry': country.upper(),
        'birthName': '',
        'city': city,
        'country': country.upper(),
        'dataProcessing': True,
        'dob': app_data.get('user').get('dob'),
        'email': app_data.get('user').get('email'),
        'firstName': app_data.get('user').get('firstname'),
        'ioBlackBox': '0400CIfeAe15Cx8Nf94lis1ztu5Divh068bwCoK31Bznnk64xJMsWE3Av6XDdSwLCCBvycRnFjjOSN55HFbRPICsyted+trm4ecPjZ9ET08wP5DcUJf2lLqlr0nbtw2dg9JBTIBnmyVMxomEZzeUM33gBlH2zrOhrBPTQ3Xefa+YB7KRCB2VV4OJ3MlVZCnKuLt/CjlU0y9oNd8aSCdW8mGctOiSkofrPLQXKRoSIEHeUntd/7IZh9IsDPe1cdbRuI3IxM0wXKrsscFI185O+yC2fyM4P+ON0bVsmeeG+jl3A/RPMQF00uJew5Km6PL86fNoUmdFdGz6qpNWDqBGsIdPqJmfhriDZcIIRRxWuZuH5mFLW2M3B/dWYwrtlZ3ZPYc6n9cIiHtFvDesSOREgP0ceCboe/SMT/+1O0GaliJ8scSyVt28ySe//gMb0OMxW++WQh28S34xskSFa2zpuxtfRpdVmFooa0z277A/AHthzIQI07p4tGGHBe8mR20/oWqf6pnWEhaVL+HKf2hhwqstJ+rhYKIcxurY+K9PLp1np1NKUME44PUTWdXh35GdB6Cc0rdKBra3m4Y5nu4PG3mFEflGQvYKsWgswHaf9f8MJCcIq/52JW/EUmtVsYmdWtM9T+HoQVdeZcTnrKjT6bxiRUcOjS9hgMLfyuze7nRI5dygXaqbiH9RlLPzFxmHWYCv8If6PS4nOb5hUQhLTwj1yAl2Y1Y/iacvzg1pHMDma1pnEbyxrf+VHGg48BAA46MBHt1l9vQxmkFGcOQocNRVJRLrEFkneaGzTEVdGJlxBwBigDt9U/mISLb8/k+2x38WV6i8/tb5alAK/XRNo3H5dICITePfhpeXrIyT7ddLrZSRNQWG3ejZc4OqNBd6jOK5ytTq3GlE+bxJaVv3tlev/DKx4PQbX9OHEQi+wlGoZ8X2ykV3stOnGIBUY5+4temb2HYF5oyZHJfDms1GGMwW+VLB6M5fl2EoZiuy7XetzhEJuvK70bwBpHGL9SBAEfPHBSRpGgPrZlXjGkx4+8YpnmRnGH4RU9dm02QUHAnqrNLu481Pv8wBC4Ze7otmL/ptZcXRDPGX/6lT5yFHJlRUl9R7JVZskN82fWF/uY3pqEZ+pgCd2Ux7tsZASWCrBaHXfcQ+imBrj9nDgkCaZG8JoC7ZLIHeAp9DSmPUtZ9vW+goecw7F3wq1Ozlvnyi9qMLliy11neb+tS5zy6vWX0hm+bS3mbLSRvUbQOP0JI5+gsCSaAk2PN+mptm/2IBYsbtzWT08HSwfZOHPmnClNMvcbvEP6KPUu35DRPrwFsz6MCGEBizqnNFuuWgpA6oIVtmrCTszpaOqqoTZcXwZs9oTjU87artHlYu',
        'lastName': app_data.get('user').get('lastname'),
        'licenseRegion': country.upper(),
        'nationality': country.upper(),
        'oAuthClientId': 'bundesliga6',
        'password': app_data.get('user').get('password'),
        'postcode': postal_code,
        'salutation': salutation,
        'street': util.get_random_string('decimal',1)+util.get_random_string('char_small',1)+', '+street+'. '+util.get_random_string('decimal',2),
        'username': app_data['username']}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}



################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################


###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	make_session_count(app_data,device_data)
	app_data['subsession_count'] = 1
	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		"Client-Sdk" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {

		}
	data = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_updated_at" : campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
		"app_version" : campaign_data.get('app_version_code'),
		"app_version_short" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"bundle_id" : campaign_data.get('package_name'),
		"connectivity_type" : "2",
		"country" : device_data.get('locale').get('country').upper(),
		"cpu_type" : device_data.get('cpu_type'),
		"created_at" : created_at,
		"device_name" : device_data.get('device_platform'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : 0,
		"hardware_name" : device_data.get('hardware'),
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"install_receipt" : install_receipt(app_data),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"needs_response_details" : 1,
		"os_build" : device_data.get('build'),
		"os_name" : "ios",
		"os_version" : device_data.get('os_version'),
		"persistent_ios_uuid" : app_data.get('ios_uuid'),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"tracking_enabled" : 1,

		}
	if isOpen:
		def_(app_data,'subsession_count')
		data['last_interval'] = int(time.time()) - app_data.get('app_close_time')
		data['session_length'] = app_data.get('app_close_time') - app_data.get('adjust_call_time')
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = app_data.get('app_close_time') - app_data.get('adjust_call_time')


	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	



###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,callback_params=None,partner_params=None):
	# sdk_clk_time=get_date(app_data,device_data)
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	def_(app_data,'subsession_count')
	time_spent=int(time.time())-app_data.get('adjust_call_time')
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		"Client-Sdk" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {

		}
	data = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_updated_at" : campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
		"app_version" : campaign_data.get('app_version_code'),
		"app_version_short" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"bundle_id" : campaign_data.get('package_name'),
		"connectivity_type" : "2",
		"country" : device_data.get('locale').get('country').upper(),
		"cpu_type" : device_data.get('cpu_type'),
		"created_at" : created_at,
		"details" : json.dumps({"Version3.1":{"iad-attribution":"false"}}),
		"device_name" : device_data.get('device_platform'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : 0,
		"hardware_name" : device_data.get('hardware'),
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"install_receipt" : install_receipt(app_data),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"last_interval" : 2,
		"needs_response_details" : 1,
		"os_build" : device_data.get('build'),
		"os_name" : "ios",
		"os_version" : device_data.get('os_version'),
		"persistent_ios_uuid" : app_data.get('ios_uuid'),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count',1),
		"session_length" : time_spent,
		"source" : source,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : 1,

		}
	if source=='iad3':
		if data.get('deeplink'):
			del data['deeplink']
		if data.get('click_time'):
			del data['click_time']
		data['details']=json.dumps({"Version3.1":{"iad-attribution":"false"}})

	if source=='deeplink' and click_time:
		if not data.get('deeplink'):
			data['deeplink']="CONSTANT"
			data['click_time']='sdk_clk_time'





	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,t1=0,t2=0, initiated_by = "backend"):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		"Client-Sdk" : campaign_data.get('adjust').get('sdk'),
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"attribution_deeplink" : "1",
		"created_at" : created_at,
		"environment" : "production",
		"event_buffering_enabled" : 0,
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"initiated_by" : initiated_by,
		"needs_response_details" : 1,
		"sent_at" : sent_at,

		}
	data = {

		}

	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}




	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)


def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date
# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')


def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['password'] 		= util.get_random_string('char_all',7)+"@"+util.get_random_string('decimal',3)

def install_receipt(app_data):
	return 'MIISqQYJKoZIhvcNAQcCoIISmjCCEpYCAQExCzAJBgUrDgMCGgUAMIICSgYJKoZIhvcNAQcBoIICOwSCAjcxggIzMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgCNMA0CAQoCAQEEBRYDMTIrMA0CAQ0CAQEEBQIDAdWIMA4CAQECAQEEBgIEVfKZxTAOAgEJAgEBBAYCBFAyNTIwDgIBCwIBAQQGAgQHB7ZzMA4CARACAQEEBgIEMW2W0zAQAgEPAgEBBAgCBlO2DwIwbjAUAgEAAgEBBAwMClByb2R1Y3Rpb24wFAIBAwIBAQQMDAoyMDE4MTEyNzAxMBQCARMCAQEEDAwKMjAxODExMjcwMTAYAgEEAgECBBBZbyPi3W3F/byQFKHQEpUKMBwCAQUCAQEEFIBjx4WwTmPKWFfkhihKGjnnhShgMB4CAQgCAQEEFhYUMjAxOS0wNC0wOFQxMToyNzoxNVowHgIBDAIBAQQWFhQyMDE5LTA0LTA4VDExOjI3OjE1WjAeAgESAgEBBBYWFDIwMTktMDQtMDhUMTE6Mjc6MTVaMCACAQICAQEEGAwWY29tLnRpcGljby5idW5kZXNsaWdhNjBIAgEHAgEBBECpNoJyPaHIjk42POFR5NMOulHeC5yhD3voB/K2DyjpoIi/tyt7PcC6ZCxufmW2+n/xQN8FMKNie/veyj3G0ICCMFQCAQYCAQEETCPRhCvA/HNSbiNQRrkpDAvVllu72sM5QHxogBLZw39+9ukFTeyl9OdM5ZGThKQ1GkjfnvN2nVaT7vnlhKU1WZUp3qMUQqHcFD8MKrSggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAKNSbwnOZYuzl00uLiJcSKHrkSTKF1BcLW2PX1XItAXzohRg28JfRJ4xFnagG3nugf2NMY8el5xTzPnvgK3OvpDRwqlvCaMJCiVB3l8OeZmvXcDOIfwiu/NKP7O5W6LVnSDScClZ94CX1rgZTBFB7f5E78kHDQfnfWpNpG23RoxCkbTmypvmIS669iFCZqaMi5qSVk8dV+/zKwCOAvdeF80WcdSgMIo8LJZwXLvdCjyDpYB5f1ieVxxpvJ8zeDGRJfl7xrlBMnF60DbQgtvYkZQcQTImBEPk94Pj5wdYMC7J14je6SarXydCeRE3XmBY/svnYsreV7z4itqBLx+49x8='
