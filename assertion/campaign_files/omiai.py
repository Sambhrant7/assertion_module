# -*- coding: utf-8 -*-
from sdk import util,installtimenew
from sdk import getsleep
import time
import random
import json
import string
import datetime
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'com.omiai_jp',
	'app_name' 			 :'Omiai',
	'app_size'           :21.0,
	'app_version_code'	 :'465',#'458',#'455',#'430',450
	'app_version_name'	 :'10.97.0',#'10.90.0',#'10.89.0',#'10.71.0',10.85.0
	'no_referrer'		 : True,
	'supported_os'		 :'4.4',		
	'supported_countries':'WW',	
	'CREATE_DEVICE_MODE':3,
	'ctr'               : 6,
	'device_targeting':True,
	'tracker'			 :'appsflyer',
	'gmp_version'        :16091,
	'appsflyer':{
		'key'		 : 'FY4ADGtZkejGvBNYdVPm8m',
		'dkh'		 : 'FY4ADGtZ',
		'buildnumber': '4.8.8',
		'version'	 : 'v4',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############
	print "Please wait installing..."	
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android'),
	
	get_batteryLevel(app_data)
	def_eventsRecords(app_data)	
	def_installDate(app_data,device_data)
	time.sleep(random.randint(60,100))
	app_data['gcm']=str(int(time.time()*1000))
	time.sleep(random.randint(1,2))
	def_firstLaunchDate(app_data,device_data)
	app_data['gcm1']=str(int(time.time()*1000))

	if not app_data.get('registration_complete'):
		app_data['registration_complete']=False
	
	app_data['appUserId']='4945'+util.get_random_string('decimal',3)
	# app_data['registeredUninstall'] = False

	###########	TRACKER	CALLS		################

	print '\n Omiai Self Call-------------------------------'
	request = api_omiai_update_check(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(1)
	print "Appsflyer :: gcm_token-----------------------------"
	req=gcmToken_call(campaign_data, app_data, device_data)
	resp=util.execute_request(**req)
	try:
		resp_str=str(resp)
		app_data['gcm_token']=resp_str.split('token=')[1].split('\',')[0]
		print app_data['gcm_token']
		# time.sleep(10)
	except:
		app_data['gcm_token']=util.get_random_string('google_token',11)+':APA91B'+util.get_random_string('all',134)	

	print '\n EVENT ------------------------------initial_app_launch-'
	initial_app_launch(campaign_data, app_data, device_data)

	time.sleep(random.randint(0,1))
	print '\nAppsflyer : TRACK1____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true")
	util.execute_request(**request)

	for i in range(1,4):
		print '\n APPSFLYER REGISTER 1 ----------------------------------'
		request=appsflyer_register(campaign_data, app_data, device_data, call=i)
		util.execute_request(**request)

	time.sleep(random.randint(8,11))	
	print '\n App Measurement-------------------------------'
	request=app_measurement1(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(3*60,5*60))
	print '\n Omiai Self Call-------------------------------'
	request = api_omiai_master_info(campaign_data, app_data, device_data)
	util.execute_request(**request)

	print '\nAppsflyer : TRACK1____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)

	if random.randint(1,100)<=90:		

		r=random.randint(1,90)

		if r<=45:
			time.sleep(random.randint(60,90))	
			print '\n EVENT ------------------------------complete_registration_male-'
			complete_registration_male(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			print '\n EVENT -------------------------------completeRegistration'
			completeRegistration(campaign_data, app_data, device_data)

			time.sleep(random.randint(18,23))	
			print '\nAppsflyer : TRACK1____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)

		elif r>45 and r<=80:

			time.sleep(random.randint(60,90))	
			print '\n EVENT ------------------------------complete_registration_female-'
			complete_registration_female(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			print '\n EVENT -------------------------------completeRegistration'
			completeRegistration(campaign_data, app_data, device_data)

		else:
			time.sleep(random.randint(45,60))	
			re_complete_registration(campaign_data, app_data, device_data)

		app_data['registration_complete']=True

	if app_data.get('registration_complete')==True and random.randint(1,100)<=45:
		time.sleep(random.randint(40,60))
		sent_message(campaign_data, app_data, device_data)

	if app_data.get('registration_complete')==True and random.randint(1,100)<=6:

		time.sleep(random.randint(30,45))
		initial_login(campaign_data, app_data, device_data)


	if random.randint(1,100)<=70:
		print '\nAppsflyer : TRACK1____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data)
		util.execute_request(**request)


	return {'status':True}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	

	if not app_data.get('times'):
		print "Please wait installing..."	
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	
	get_batteryLevel(app_data)
	if not app_data.get('prev_event_value') or not app_data.get('prev_event_name') or not app_data.get('prev_event_time'):
		def_eventsRecords(app_data)

	def_installDate(app_data,device_data)
	if not app_data.get('gcm'):
		time.sleep(random.randint(60,100))
		app_data['gcm']=str(int(time.time()*1000))
		time.sleep(random.randint(1,2))
	
	def_firstLaunchDate(app_data,device_data)
	if not app_data.get('gcm1'):
		app_data['gcm1']=str(int(time.time()*1000))

	if not app_data.get('registration_complete'):
		app_data['registration_complete']=False

	if not app_data.get('appUserId'):
		app_data['appUserId']='4945'+util.get_random_string('decimal',3)


	########### CALLS ###################

	print '\nAppsflyer : TRACK1____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)


	if app_data.get('registration_complete')==False:
		r=random.randint(1,90)

		if r<=45:
			time.sleep(random.randint(60,90))	
			print '\n EVENT ------------------------------complete_registration_male-'
			complete_registration_male(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			print '\n EVENT -------------------------------completeRegistration'
			completeRegistration(campaign_data, app_data, device_data)

			time.sleep(random.randint(18,23))	
			print '\nAppsflyer : TRACK1____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)

		elif r>45 and r<=80:

			time.sleep(random.randint(60,90))	
			print '\n EVENT ------------------------------complete_registration_female-'
			complete_registration_female(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			print '\n EVENT -------------------------------completeRegistration'
			completeRegistration(campaign_data, app_data, device_data)

		else:
			time.sleep(random.randint(45,60))	
			re_complete_registration(campaign_data, app_data, device_data)

		app_data['registration_complete']=True

	elif random.randint(1,100)<=7:
		time.sleep(random.randint(30,45))
		initial_login(campaign_data, app_data, device_data)

	if app_data.get('registration_complete')==True and random.randint(1,100)<=30:
		time.sleep(random.randint(40,60))
		sent_message(campaign_data, app_data, device_data)

	if random.randint(1,100)<=70:
		print '\nAppsflyer : TRACK1____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data)
		util.execute_request(**request)



	
	###########		FINALIZE	################
		
	return {'status':True}

def gcmToken_call(campaign_data, app_data, device_data):
	url 	= 'https://android.clients.google.com/c2dm/register3'
	method 	= 'post'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent'	  : 'Android-GCM/1.5 ('+device_data.get('product')+' '+device_data.get('build')+')',
		'Content-Type'	  : 'application/x-www-form-urlencoded',
		'app'			  : campaign_data.get('package_name'),
		'gcm_ver'		  : '17455020',
		'Authorization'	  : 'AidLogin 4289720365770577428:2116601572621516374',
		}
	params 	= None
	data 	= {
			'X-subtype':'447355377219',
			'sender':'447355377219',
			'X-app_ver':campaign_data.get('app_version_code'),
			'X-osv':device_data.get('sdk'),
			'X-cliv':'fiid-'+util.get_random_string('decimal',8),
			'X-gmsv':'17455020',
			'X-appid':util.get_random_string('char_all',11),
			'X-scope':'*',
			'X-gmp_app_id':'1:447355377219:android:'+device_data.get('android_id'),
			'X-app_ver_name':campaign_data.get('app_version_name'),
			'app':campaign_data.get('package_name'),
			'device':'4289720365770577428',
			'cert':util.get_random_string('hex',40),
			'app_ver':campaign_data.get('app_version_code'),
			# 'info':util.get_random_string('all',31),
			'gcm_ver':'17455020',
			'plat':'0',
			'target_ver':27,
			# 'X-google.message_id':'google.rpc1'
	}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################
def completeRegistration(campaign_data, app_data, device_data):

	eventName		  = 'af_complete_registration'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue, isFirstCall='false')
	util.execute_request(**request)

def initial_app_launch(campaign_data, app_data, device_data):

	eventName		  = 'initial_app_launch'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue, isFirstCall='false')
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)

def complete_registration_male(campaign_data, app_data, device_data):

	eventName		  = 'complete_registration_male'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue, isFirstCall='false')
	util.execute_request(**request)

def complete_registration_female(campaign_data, app_data, device_data):
	eventName		  = 'complete_registration_female'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue, isFirstCall='false')
	util.execute_request(**request)

def re_complete_registration(campaign_data, app_data, device_data):
	eventName		  = 're_complete_registration'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue, isFirstCall='false')
	util.execute_request(**request)

def initial_login(campaign_data, app_data, device_data):

	eventName		  = 'initial_login'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue, isFirstCall='false')
	util.execute_request(**request)	

def sent_message(campaign_data, app_data, device_data):
	eventName		  = 'sent_message'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue, isFirstCall='false')
	util.execute_request(**request)	


##################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################

def appsflyer_register(campaign_data,app_data,device_data, call=1):
	# def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_installDate(app_data,device_data)
 	def_(app_data,'counter')
 	# get_gcmToken(app_data)
 	# app_data['registeredUninstall'] = True
	#afGoogleInstanceID = get_afGoogleInstanceID()
		
	method 	= 'post'
	url 	= 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}	
	params = {
		'app_id'		  : campaign_data.get('package_name'),
		'buildnumber'	  : campaign_data.get('appsflyer').get('buildnumber'),
		}
	data = {
			'advertiserId' 			: device_data.get('adid'),	
			#'af_google_instance_id'	: afGoogleInstanceID,
			# 'channel'				: '(null)',
			# 'deviceFingerPrintId'	: app_data.get('deviceFingerPrintId'),
			'devkey'				: campaign_data.get('appsflyer').get('key'),
			'installDate'			: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
			'launch_counter'		: '1',
			'sdk' 					: device_data.get('sdk'),	
			'uid' 					: app_data.get('uid'),
			'af_gcm_token'			: app_data.get('gcm_token'),
			'app_name'              :campaign_data.get('app_name'),
			"app_version_code"		: campaign_data.get('app_version_code'),
		    "app_version_name"		: campaign_data.get('app_version_name'),
		    'brand'                 :device_data.get('brand'),
		    'carrier'               :device_data.get('carrier'),
		    "model"					: device_data.get('model'),
		    "network"				: device_data.get('network'),
		    "operator"              :device_data.get('carrier')
		}

	if call==2:

		data['af_gcm_token']=str(app_data.get('gcm'))+','+app_data.get('gcm_token')
		app_data['gcm_token']=data.get('af_gcm_token')

	if call==3:

		data['af_gcm_token']=str(app_data.get('gcm1'))+','+app_data.get('gcm_token')

		app_data['gcm_token']=app_data.get('gcm1')
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=-1,isFirstCall="false",isOpen=False):
	
	# def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	# get_gcmToken(app_data)

	method  = 'post'
	url 	= 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}		
	params  = {
		'app_id'		  : campaign_data.get('package_name'),
		'buildnumber'	  : campaign_data.get('appsflyer').get('buildnumber'),
		}	
	data    = {
		"advertiserId"			: device_data.get('adid'),
		"advertiserIdEnabled"	: "true",
		"af_events_api"			: "1",
		# 'af_gcm_token'			: app_data.get('af_gcm_token'),
		"af_preinstalled"		: "false",
		#"af_sdks"               :"0000000000",
		"af_timestamp"			: timestamp(),
		# "android_id"            : device_data.get('android_id'),
		"app_version_code"		: campaign_data.get('app_version_code'),
		"app_version_name"		: campaign_data.get('app_version_name'),
		#"appid"                 :campaign_data.get('package_name'),
		"appsflyerKey"			: campaign_data.get('appsflyer').get('key'),
		# "appUserId"				: app_data.get('appUserId'),
		#"batteryLevel"          : app_data.get('batteryLevel'),
		"brand"					: device_data.get('brand'),
		"carrier"				: device_data.get('carrier'),
		"counter"				: str(app_data.get('counter')),
		"country"				: 'JP',
		# "currency"				: "IDR",
		# "cksm_v1"				: util.get_random_string('hex',32),
		"date1"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device"				: device_data.get('device'),
		"deviceData"			: {
									"arch": "",
									"btch":"no",
		                            "btl":app_data.get('batteryLevel'),
									"build_display_id": device_data.get('build'),
									"cpu_abi": device_data.get('cpu_abi'),
									"cpu_abi2":  device_data.get('cpu_abi'),
									"dim":{"y_px":device_data.get('resolution').split('x')[0],"x_px":device_data.get('resolution').split('x')[1],"size":"2","xdp":device_data.get('dpi')+"."+str(random.randint(955,975)),"d_dpi":device_data.get('dpi'),"ydp":device_data.get('dpi')+"."+str(random.randint(558,572))}},
		# "deviceFingerPrintId"	: app_data.get('deviceFingerPrintId'),
		"deviceType"			: "user",
		#"dkh"					: campaign_data.get('appsflyer').get('dkh'),
		"firstLaunchDate"		: app_data.get('firstLaunchDate'),
		#"gaidError"				: "0: ClassNotFoundException",
		"iaecounter"			: str(app_data.get('iaecounter')),
		"installDate"			: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		# "installAttribution"    :'{"af_message":"organic install","af_status":"Organic"}',
		"installer_package"		: "com.android.vending",
		"isFirstCall"			: isFirstCall,
		"isGaidWithGps"			: "true",
		# "imei"					: device_data.get('imei'),
		"lang"					: '日本語',
		"lang_code"				: 'ja',
		"model"					: device_data.get('model'),
		"network"				: device_data.get('network'),
		"operator"				: device_data.get('carrier'),
		"platformextension"		: "android_native",
		"product"				: device_data.get('product'),
		# "referrer"				: urllib.unquote(app_data.get('referrer')),
		"registeredUninstall"	: False,
		"sdk"					: device_data.get('sdk'),	
		"timepassedsincelastlaunch": '-1',
		# "tokenRefreshConfigured": False,
		"uid"					: app_data.get('uid'),
		# "rfr":{"clk":int(time.time()),
		#        "code":0,
		#        "install":app_data.get('install'),
		#        "val":urllib.unquote(app_data.get('referrer')),}
		}			
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	# if app_data.get('installAttribution'):
	# 	data['installAttribution'] = json.dumps(app_data.get('installAttribution'))
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+\
						data.get('uid')[0:7]+\
						data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+\
						data.get('af_timestamp')+\
						data.get('uid')+\
						data.get('installDate')+\
						str(data.get('counter'))+\
						str(data.get('iaecounter'))
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if app_data.get('counter')==1:
		data['af_sdks']="0000000000"
		data['batteryLevel']=app_data.get('batteryLevel')
		data["deviceData"]["sensors"]=[{"sT":2,"sV":"MTK","sVE":[38.7,33.194,1.459],"sVS":[36.3,33.194,-5.254],"sN":"MAGNETOMETER"},{"sT":1,"sV":"MTK","sVE":[0.025,0.395,10.202],"sVS":[0.056,0.349,10.217],"sN":"ACCELEROMETER"},{"sT":4,"sV":"MTK","sVE":[0,0,0],"sVS":[0,0,0],"sN":"GYROSCOPE"}]

	if app_data.get('counter')>1:
		data['appUserId']=app_data.get('appUserId')
		data['timepassedsincelastlaunch'] = str(int(time.time()) - app_data.get('timepassed'))

	data['cksm_v1']=cksm_v1(data.get('installDate'), data.get('af_timestamp'))

	if app_data.get('counter')>1:
		data['af_gcm_token']=app_data.get('gcm_token')
		data['registeredUninstall']=True

	# if app_data.get('counter')>1:
	# 	data['installAttribution']='{"af_message":"organic install","af_status":"Organic"}'
	
	
	# if app_data.get('counter')>2:
	# 	data.pop('rfr')
	app_data['timepassed'] = int(time.time())

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################
def appsflyer_event(campaign_data, app_data, device_data,eventName="",eventValue="",isFirstCall="false"):
	# def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	# get_gcmToken(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	
	method 	= 'post'
	url 	= 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}
	params  = {
		'app_id'		  : campaign_data.get('package_name'),
		'buildnumber'	  : campaign_data.get('appsflyer').get('buildnumber'),
		}	
	data 	= {
		"advertiserId"			: device_data.get('adid'),
		"advertiserIdEnabled"	: "true",
		"af_events_api"			: "1",
		"af_preinstalled"		: "false",
		"af_gcm_token"          :app_data.get('gcm_token'),
		"appUserId"				: app_data.get('appUserId'),
		"af_timestamp"			: timestamp(),
		#"android_id"            : device_data.get('adid'),
		"app_version_code"		: campaign_data.get('app_version_code'),
		"app_version_name"		: campaign_data.get('app_version_name'),
		#"appid"                 :campaign_data.get('package_name'),
		"appsflyerKey"			: campaign_data.get('appsflyer').get('key'),
		"brand"					: device_data.get('brand'),
		"carrier"				: device_data.get('carrier'),
		"counter"				: str(app_data.get('counter',0)),
		"country"				: 'JP',
		# "cksm_v1"				: util.get_random_string('hex',32),
		"date1"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device"				: device_data.get('device'),
		"deviceData"			: {
									"arch": "",
									# "btch":"no",
									# "btl":app_data.get('batteryLevel'),
									"build_display_id": device_data.get('build'),
									"cpu_abi": device_data.get('cpu_abi'),
									"cpu_abi2":device_data.get('cpu_abi'),
									"dim":{"y_px":device_data.get('resolution').split('x')[0],"x_px":device_data.get('resolution').split('x')[1],"size":"3","xdp":device_data.get('dpi')+".0","d_dpi":device_data.get('dpi'),"ydp":device_data.get('dpi')+".0"}},
		# "deviceFingerPrintId"	: app_data.get('deviceFingerPrintId'),
		"deviceType"			: "user",
		#"dkh"					: campaign_data.get('appsflyer').get('dkh'),
		"firstLaunchDate"		: app_data.get('firstLaunchDate'),
		"iaecounter"			: str(app_data.get('iaecounter')),
		"installDate"			: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package"		: "com.android.vending",
		"isFirstCall"			: isFirstCall,
		"isGaidWithGps"			: "true",
		#"imei"					: device_data.get('imei'),
		"lang"					: '日本語',
		"lang_code"				: 'ja',
		"model"					: device_data.get('model'),
		"network"				: device_data.get('network'),
		"operator"				: device_data.get('carrier'),
		"platformextension"		: "android_native",
		"product"				: device_data.get('product'),
		# "referrer"				: urllib.unquote(app_data.get('referrer')),
		"registeredUninstall"	: True,
		"sdk"					: device_data.get('sdk'),	
		# "tokenRefreshConfigured": False,
		"uid"					: app_data.get('uid'),
	}
	# if app_data.get('installAttribution'):
	# 	data['installAttribution'] = json.dumps(app_data.get('installAttribution'))		
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+\
						data.get('uid')[0:7]+\
						data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+\
						data.get('af_timestamp')+\
						data.get('uid')+\
						data.get('installDate')+\
						str(data.get('counter'))+\
						str(data.get('iaecounter'))
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	data['cksm_v1']=cksm_v1(data.get('installDate'), data.get('af_timestamp'))

	if app_data.get('iaecounter')==1:
		data['registeredUninstall']=False
		data['isFirstCall']='true'
		del data['af_gcm_token']
		del data['appUserId']

	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')


	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and \
			app_data.get('prev_event_value') and \
			app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
	update_eventsRecords(app_data,eventName,eventValue)
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def stats_appsflyer(campaign_data, app_data, device_data):
	# def_time_in_app(app_data) 
	# make_launch_counter(app_data)
	inc_(app_data,'launch_counter')
 
	method = 'post'
	url = 'https://stats.appsflyer.com/stats' 
	
	headers = {
					'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
					'Content-Type': 'application/json',

				}
			
	# if not app_data.get('install_time'):
	# 	make_install_time(app_data)
	
			
	data1 = {
				'app_id' : campaign_data.get('package_name'),
				"advertiserId"			: device_data.get('adid'),
				'channel':'(null)',  # null replaced to (null)
				'deviceFingerPrintId':app_data.get('deviceFingerPrintId'),
				'devkey' : campaign_data.get('appsflyer').get('key'),
				'gcd_conversion_data_timing':'7',
				'launch_counter':app_data.get('launch_counter'), 
				'platform':'Android',
				'statType':'user_closed_app',
				# 'time_in_app':int(str(int((time.time()*1000)))[:11])-int(app_data.get('install_time')[:11]),
				'time_in_app':random.randint(20,100),
				'uid' :app_data.get('uid'),
				'originalAppsflyerId':app_data.get('uid')
			}
			
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data1)}
		
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)

	url 	= 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	method 	= 'get'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent'	  : get_ua(device_data),
		}
	params 	= {
		'devkey'		  : campaign_data.get('appsflyer').get('key'),
		'device_id' 	  : app_data.get('uid')
		}
	data 	= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

################ SELF CALLS #######################

def api_omiai_update_check(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)

	url 	= 'http://api2.omiai-jp.com/index/update_check'
	method 	= 'head'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent'	  : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (Android; OS '+device_data.get('os_version')+'; Scale/3.000000; ja_JP; '+device_data.get('model')+'; '+device_data.get('brand')+')',
		'Content-Type'    : 'application/x-www-form-urlencoded'
		}
	data 	= {
				'package' :	campaign_data.get('package_name'),
				'version' :	campaign_data.get('app_version_code'),
		}
	params 	= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def api_omiai_master_info(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)

	url 	= 'http://api2.omiai-jp.com/master/master_info'
	method 	= 'head'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent'	  : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (Android; OS '+device_data.get('os_version')+'; Scale/3.000000; ja_JP; '+device_data.get('model')+'; '+device_data.get('brand')+')',
		'Content-Type'    : 'application/x-www-form-urlencoded'
		}
	data 	= {
				'package' :	campaign_data.get('package_name'),
		}
	params 	= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}



#########################################################
#
#    APP MEASUREMENT
#
##########################################################

def app_measurement1(campaign_data, app_data, device_data):
	method 	= 'head'
	url 	= 'http://app-measurement.com/config/app/1%3A447355377219%3Aandroid%3Af841b1e890bb4f2f'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent': get_ua(device_data)
		}
	params = {
			'app_instance_id'      :'c262c0f80ec8f5f1fc2f76f6adbc5dec',
			'platform'             :'android',
			'gmp_version'          :'17455'
			}
	data   = None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':data}


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = str(int(time.time()*1000))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
# def def_deviceFingerPrintId(app_data):
# 	if not app_data.get('deviceFingerPrintId'):
# 		app_data['deviceFingerPrintId'] = 'ffffffff-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('batteryLevel'):
		app_data['batteryLevel'] = str(random.randint(10,100))+".0"

	
###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	
def def_installDate(app_data,device_data):
	if not app_data.get('installDate'):
		app_data['installDate'] = get_date(app_data,device_data)
		app_data['install']=int(time.time())

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone')
	return date

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

# def def_event_date(app_data,device_data):
# 	def_sec(app_data,device_data)
# 	date = int(time.time())
# 	app_data['event_date'] =  datetime.datetime.utcfromtimestamp(date+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:22]+device_data.get('timezone')+'['+device_data.get('local_tz_name')+']'

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))


###########################################################
# Utility methods : MISC
#
###########################################################
# def random_string(len,typ='hex'):
# 	if typ=='hex':
# 		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
# 	if typ=='numb':
# 		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))


def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

# def def_time_in_app(app_data,forced=False):
# 	if not app_data.get('time_in_app'):
# 		app_data['time_in_app']=0
# 	if forced:
# 		app_data['time_in_app']=0


def get_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token']=util.get_random_string('google_token',11)+':'+'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(156))
	
# def make_launch_counter(app_data):
# 	if not app_data.get('launch_counter'):
# 		app_data['launch_counter'] = 1
# 	else:
# 		app_data['launch_counter'] = app_data.get('launch_counter')+1
		
	# return app_data.get('launch_counter')

def random_string(len):
	return ''.join([random.choice("0123456789abcdef") for _ in range(len)])

# def make_install_time(app_data):
# 	app_data['install_time'] = timestamp()
# 	return app_data.get('install_time')


def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()

def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def generateValue1get(ts, firstLaunch, sdk_version):
    gethash = sha256(ts+firstLaunch+sdk_version)
    return gethash[:16]


def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+ins_date+datetime.datetime.utcfromtimestamp(int(ins_time)/1000).strftime("%Y-%m-%d_%H%M%S")+"+0000"
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	sb = md5_result

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')
	return sb






