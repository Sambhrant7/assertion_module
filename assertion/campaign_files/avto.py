# -*- coding: utf-8 -*-
from sdk import util,installtimenew
from sdk import getsleep
from sdk import NameLists
import time
import hmac
import hashlib
import base64
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'ru.auto.ara',
	'app_name' 			 :'Avto.ru',
	'app_version_name' 	 :'6.7.5',#'6.6.2',#'6.6.0',#'6.5.2',#'6.5.1',#6.5.0, '6.4.2',#6.4.0, '6.3.1',#'6.2.1',#'6.2.0',#'6.1.1',#'6.1.0',6.4.1
	'ctr' 				 : 6,
	'no_referrer' 		 : True,
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '5.0', #4.4
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 : 't37tcfsdppnm',
		'sdk'		 : 'android4.18.0',#'android4.17.0',#'android4.14.0',
		'secret_key' : '18209960061649460639217598684946971841',
		'secret_id'	 : '3',
		},
	'app_size': 53.0,#49.5,#48.0, #33.0
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:34,
				17:33,
				18:32,
				19:31,
				20:30,
				21:29,
				22:28,
				23:26,
				24:25,
				25:24,
				26:23,
				27:22,
				28:20,
				29:17,
				30:15,
				31:12,
				32:10,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

	# set_sessionLength(app_data,forced=True,length=0)
	pushtoken(app_data)
	# get_install_reciept(app_data)
	################generating_realtime_differences##################
	# set_installedAT(app_data,device_data)
	created_at_session=get_date(app_data,device_data)
	time.sleep(random.randint(1,3))
	sent_at_session=get_date(app_data,device_data)

	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)
	
	created_at_attribution=get_date(app_data,device_data)
	created_at_sdk1=get_date(app_data,device_data)
	time.sleep(random.randint(2,3))
	click_time1=get_date(app_data,device_data)
	sent_at_sdk1=get_date(app_data,device_data)
	time.sleep(random.randint(2,3))
	sent_at_attribution=get_date(app_data,device_data)
	t1=4
	t2=5
	app_data['sess_len']=int(time.time())

	###########		CALLS		############

	l=current_location(device_data)
	result=util.execute_request(**l)
	try:
		print "Fetching latLong."
		app_data['latitude']=str(json.loads(result.get('data')).get('geo').get('latitude'))
		app_data['longitude']=str(json.loads(result.get('data')).get('geo').get('longitude'))
		print app_data['latitude']
		print app_data['longitude']
	except:
		app_data['latitude']="28.57"
		app_data['longitude']="77.32"
		print "Exception in latLong"

	print '\n'+'Adjust : device_hello____________________________________'
	request=device_hello(campaign_data, app_data, device_data)
	output=util.execute_request(**request)
	try:
		r=output.get('res').headers.get('Set-Cookie')
		app_data['rerf']='rerf='+r.split('rerf=')[1].split(';')[0]
		print app_data['rerf']
		app_data['X-Vertis-DC']='X-Vertis-DC='+r.split('X-Vertis-DC=')[1].split(';')[0]
		print app_data['X-Vertis-DC']


		app_data['uid']=output.get('res').headers.get('X-Device-UID')
		print app_data['uid']
		app_data['session_id']=output.get('res').headers.get('X-Session-Id')
		print app_data['session_id']
		# time.sleep(5)
	except:
		app_data['rerf']='rerf=AAAAAFwvH5pJMW0VAx7aAg=='
		app_data['X-Vertis-DC']='X-Vertis-DC=sas'
		app_data['uid']='g5c2f21985b4c3etl4i16b2tmqfhav55.0a45e21c697a179da1b06da7ce04ce8f'
		app_data['session_id']='a:g5c2f21985b4c3etl4i16b2tmqfhav55.0a45e21c697a179da1b06da7ce04ce8f|1546592664549.604800.1r3pHyXEc9GefZPo_boGzA.bpkvo5inmSOlaHrCzlRIIIUX9JGU23acjwJkyFUEoIU'
		
		
	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time1,created_at=created_at_sdk1,sent_at=sent_at_sdk1,source='reftag')
	util.execute_request(**request)

	print '\n'+'Adjust : SDK INFO____________________________________'
	request=adjust_sdkinfo(campaign_data, app_data, device_data,source='push')
	util.execute_request(**request)
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,created_at=created_at_attribution,sent_at=sent_at_attribution)
	util.execute_request(**request)

	time.sleep(random.randint(5,9))

	launch(campaign_data, app_data, device_data,t1,t2)

	time.sleep(random.randint(5,7))
	if random.randint(1,100)<=90:
		for i in range(0,3):
			filter(campaign_data, app_data, device_data,t1,t2)
			time.sleep(random.randint(5,7))
		global choose
		choose=random.choice(['cars','moto','trucks'])
		print '\n'+'Adjust : search_cars____________________________________'
		request=search_cars(campaign_data, app_data, device_data,choose)
		output=util.execute_request(**request)

		global offer_id		
		global price
		global offer	
		global price_info
		global val
		
		try :
			offer_id = []
			price=[]
			offer = []
			price_info=[]
			val=''
		
			pack=json.loads(output.get('data')).get('offers')
			for k in range(len(pack)):			
				offer_id.append(pack[k].get('id'))
				price.append(pack[k].get('price_info').get('price'))
				
			print offer_id
			print "----length----"+str(len(offer_id))
			print "--------------------------------------"
			print price
			print "--------------------------------------"
			print "----length----"+str(len(price))

			# time.sleep(10)
			if len(offer_id)<1 or len(price)<1:
				print'########################EXCEPTION##################################'
				offer_id=['1081161354-02f1d5af', '1082399632-74bb7373', '1076292931-ddf369', '1082507494-e724c04f', '1082916750-184f1b40', '1082242944-5ba41011', '1080137602-dfeb5d91', '1082917186-b755dfe4', '1082845656-13a7144f', '1082703184-b47ee650', '1081428048-56cb6e32', '1081217874-3336ca88', '1079709428-75ffdf94']
				price=[1140000, 2999990, 1045000, 499000, 408100, 379000, 499000, 410000, 499000, 339000, 429000, 359000, 340000]

			for i in range(0,3):
				offer.append(offer_id[i])
				price_info.append(price[i])

			print offer
			print "----length----"+str(len(offer))
			print "--------------------------------------"
			print price_info
			print "--------------------------------------"
			print "----length----"+str(len(price_info))

			val=random.randint(0,len(offer_id)-1)

			# time.sleep(10)
			
			
		except :
			print'###########################EXCEPTION###############################'
			offer_id=['1081161354-02f1d5af', '1082399632-74bb7373', '1076292931-ddf369', '1082507494-e724c04f', '1082916750-184f1b40', '1082242944-5ba41011', '1080137602-dfeb5d91', '1082917186-b755dfe4', '1082845656-13a7144f', '1082703184-b47ee650', '1081428048-56cb6e32', '1081217874-3336ca88', '1079709428-75ffdf94']
			price=[1140000, 2999990, 1045000, 499000, 408100, 379000, 499000, 410000, 499000, 339000, 429000, 359000, 340000]
			offer=['1081161354-02f1d5af', '1082399632-74bb7373', '1076292931-ddf369']
			price_info=[1140000, 2999990, 1045000]
			val=random.randint(0,len(offer_id)-1)





		if random.randint(1,100)<=60:
			favourite(campaign_data, app_data, device_data,t1,t2)
	
		if random.randint(1,100)<=90:
			feed_charged(campaign_data, app_data, device_data,t1,t2)

		if random.randint(1,100)<=80:
			view(campaign_data, app_data, device_data,t1,t2)

			# print '\n'+'Adjust : search_cars____________________________________'
			# request=user_confirm(campaign_data, app_data, device_data)
			# output=util.execute_request(**request)

			if random.randint(1,100)<=60:
				favourite(campaign_data, app_data, device_data,t1,t2)

			if random.randint(1,100)<=60:
				message(campaign_data, app_data, device_data,t1,t2)
			else:
				var=random.choice(['spb','moscow','new'])
				if var=='spb':
					call_frm_spb(campaign_data, app_data, device_data,t1,t2)
				if var=='moscow':
					call_frm_moscow(campaign_data, app_data, device_data,t1,t2)
				if var=='new':
					new_car(campaign_data, app_data, device_data,t1,t2)

				
				if choose=='cars':
					cars(campaign_data, app_data, device_data,t1,t2)
				if choose=='moto':
					moto(campaign_data, app_data, device_data,t1,t2)
				if choose=='trucks':
					trucks(campaign_data, app_data, device_data,t1,t2)



		if random.randint(1,100)<=45:
			sale(campaign_data, app_data, device_data,t1,t2)
			if choose=='cars':
				offer_auto(campaign_data, app_data, device_data,t1,t2)
			if choose=='moto':
				offer_moto(campaign_data, app_data, device_data,t1,t2)
			if choose=='trucks':
				offer_comm(campaign_data, app_data, device_data,t1,t2)

	
		
	

	set_appCloseTime(app_data)
	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

	pushtoken(app_data)
	# get_install_reciept(app_data)
	################generating_realtime_differences##################
	
	
	created_at_session=get_date(app_data,device_data)
	time.sleep(random.randint(2,3))	
	sent_at_session=get_date(app_data,device_data)
	
	t1=4
	t2=5

	if not app_data.get('latitude'):
		l=current_location(device_data)
		result=util.execute_request(**l)
		try:
			print "Fetching latLong."
			app_data['latitude']=str(json.loads(result.get('data')).get('geo').get('latitude'))
			app_data['longitude']=str(json.loads(result.get('data')).get('geo').get('longitude'))
			print app_data['latitude']
			print app_data['longitude']
		except:
			app_data['latitude']="28.57"
			app_data['longitude']="77.32"
			print "Exception in latLong"

	if not app_data.get('session_id'):
		print '\n'+'Adjust : device_hello____________________________________'
		request=device_hello(campaign_data, app_data, device_data)
		output=util.execute_request(**request)
		try:
			r=output.get('res').headers.get('Set-Cookie')
			app_data['rerf']='rerf='+r.split('rerf=')[1].split(';')[0]
			print app_data['rerf']
			app_data['X-Vertis-DC']='X-Vertis-DC='+r.split('X-Vertis-DC=')[1].split(';')[0]
			print app_data['X-Vertis-DC']

			app_data['uid']=output.get('res').headers.get('X-Device-UID')
			print app_data['uid']
			app_data['session_id']=output.get('res').headers.get('X-Session-Id')
			print app_data['session_id']
			# time.sleep(5)
		except:
			app_data['rerf']='rerf=AAAAAFwvH5pJMW0VAx7aAg=='
			app_data['X-Vertis-DC']='X-Vertis-DC=sas'
			app_data['uid']='g5c2f21985b4c3etl4i16b2tmqfhav55.0a45e21c697a179da1b06da7ce04ce8f'
			app_data['session_id']='a:g5c2f21985b4c3etl4i16b2tmqfhav55.0a45e21c697a179da1b06da7ce04ce8f|1546592664549.604800.1r3pHyXEc9GefZPo_boGzA.bpkvo5inmSOlaHrCzlRIIIUX9JGU23acjwJkyFUEoIU'


	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session,isOpen=True)
	util.execute_request(**request)
	app_data['sess_len']=int(time.time())
	time.sleep(random.randint(5,9))
	launch(campaign_data, app_data, device_data,t1,t2)

	time.sleep(random.randint(5,7))
	if random.randint(1,100)<=80:
		for i in range(0,3):
			filter(campaign_data, app_data, device_data,t1,t2)
			time.sleep(random.randint(5,7))
		global choose
		choose=random.choice(['cars','moto','trucks'])
		print '\n'+'Adjust : search_cars____________________________________'
		request=search_cars(campaign_data, app_data, device_data,choose)
		output=util.execute_request(**request)

		global offer_id		
		global price
		global offer	
		global price_info
		global val
		
		try :
			offer_id = []
			price=[]
			offer = []
			price_info=[]
			val=''
		
			pack=json.loads(output.get('data')).get('offers')
			for k in range(len(pack)):			
				offer_id.append(pack[k].get('id'))
				price.append(pack[k].get('price_info').get('price'))
				
			print offer_id
			print "----length----"+str(len(offer_id))
			print "--------------------------------------"
			print price
			print "--------------------------------------"
			print "----length----"+str(len(price))

			# time.sleep(10)
			if len(offer_id)<1 or len(price)<1:
				print'########################EXCEPTION##################################'
				offer_id=['1081161354-02f1d5af', '1082399632-74bb7373', '1076292931-ddf369', '1082507494-e724c04f', '1082916750-184f1b40', '1082242944-5ba41011', '1080137602-dfeb5d91', '1082917186-b755dfe4', '1082845656-13a7144f', '1082703184-b47ee650', '1081428048-56cb6e32', '1081217874-3336ca88', '1079709428-75ffdf94']
				price=[1140000, 2999990, 1045000, 499000, 408100, 379000, 499000, 410000, 499000, 339000, 429000, 359000, 340000]

			for i in range(0,3):
				offer.append(offer_id[i])
				price_info.append(price[i])

			print offer
			print "----length----"+str(len(offer))
			print "--------------------------------------"
			print price_info
			print "--------------------------------------"
			print "----length----"+str(len(price_info))

			val=random.randint(0,len(offer_id)-1)

			# time.sleep(10)
			
			
		except :
			print'###########################EXCEPTION###############################'
			offer_id=['1081161354-02f1d5af', '1082399632-74bb7373', '1076292931-ddf369', '1082507494-e724c04f', '1082916750-184f1b40', '1082242944-5ba41011', '1080137602-dfeb5d91', '1082917186-b755dfe4', '1082845656-13a7144f', '1082703184-b47ee650', '1081428048-56cb6e32', '1081217874-3336ca88', '1079709428-75ffdf94']
			price=[1140000, 2999990, 1045000, 499000, 408100, 379000, 499000, 410000, 499000, 339000, 429000, 359000, 340000]
			offer=['1081161354-02f1d5af', '1082399632-74bb7373', '1076292931-ddf369']
			price_info=[1140000, 2999990, 1045000]
			val=random.randint(0,len(offer_id)-1)


		if random.randint(1,100)<=60:
			favourite(campaign_data, app_data, device_data,t1,t2)
	
		if random.randint(1,100)<=90:
			feed_charged(campaign_data, app_data, device_data,t1,t2)

		if random.randint(1,100)<=80:
			view(campaign_data, app_data, device_data,t1,t2)

			# print '\n'+'Adjust : search_cars____________________________________'
			# request=user_confirm(campaign_data, app_data, device_data)
			# output=util.execute_request(**request)

			if random.randint(1,100)<=60:
				favourite(campaign_data, app_data, device_data,t1,t2)

			if random.randint(1,100)<=55:
				message(campaign_data, app_data, device_data,t1,t2)
			else:
				var=random.choice(['spb','moscow','new'])
				if var=='spb':
					call_frm_spb(campaign_data, app_data, device_data,t1,t2)
				if var=='moscow':
					call_frm_moscow(campaign_data, app_data, device_data,t1,t2)
				if var=='new':
					new_car(campaign_data, app_data, device_data,t1,t2)

				
				if choose=='cars':
					cars(campaign_data, app_data, device_data,t1,t2)
				if choose=='moto':
					moto(campaign_data, app_data, device_data,t1,t2)
				if choose=='trucks':
					trucks(campaign_data, app_data, device_data,t1,t2)



		if random.randint(1,100)<=45:
			sale(campaign_data, app_data, device_data,t1,t2)
			if choose=='cars':
				offer_auto(campaign_data, app_data, device_data,t1,t2)
			if choose=='moto':
				offer_moto(campaign_data, app_data, device_data,t1,t2)
			if choose=='trucks':
				offer_comm(campaign_data, app_data, device_data,t1,t2)
	
		
	

	set_appCloseTime(app_data)

	return {'status':'true'}

def current_location(device_data):
	url='http://lumtest.com/myip.json'
	header={
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
			'Accept-Encoding': 'gzip, deflate',
			'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+','+device_data.get('locale').get('language')+';q=0.9',
			'Cache-Control': 'max-age=0',
			'Upgrade-Insecure-Requests': '1',
			'User-Agent': device_data.get('User-Agent')
	}
	params={}
	data={}
		
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def launch(campaign_data, app_data, device_data,t1=2,t2=4):
	print 'Adjust : EVENT______________________________launch'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='hgowal',t1=t1,t2=t2)
	util.execute_request(**request)

def filter(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(20,150))
	print 'Adjust : EVENT______________________________filter'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='h0t0li',t1=t1,t2=t2)
	util.execute_request(**request)

def feed_charged(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(20,150))
	print 'Adjust : EVENT_____________________________feed_charged'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"criteo_p":urllib.quote(str(offer))})),event_token='267qjf',t1=t1,t2=t2)
	util.execute_request(**request)

def favourite(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(20,150))

	print 'Adjust : EVENT______________________________favourite'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"fb_currency":"RUB","fb_content_type":"product","fb_content_id":offer_id[val],"_valueToSum":price[val],"criteo_p":urllib.quote(str([{"i":offer_id[val],"pr":str(price[val])+'.000000',"q":1}]))})),event_token='dl3rbt',t1=t1,t2=t2)
	util.execute_request(**request)

def view(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(20,150))
	print 'Adjust : EVENT_____________________________view'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"item_id":offer_id[val],"fb_currency":"RUB","fb_content_type":"product","fb_content_id":offer_id[val],"_valueToSum":price[val],"criteo_p":offer_id[val]})),event_token='uymrtn',t1=t1,t2=t2)
	util.execute_request(**request)
	

def message(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(20,150))
	print 'Adjust : EVENT______________________________message'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='aur3ct',t1=t1,t2=t2)
	util.execute_request(**request)


def call_frm_spb(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(20,150))
	print 'Adjust : EVENT______________________________call_frm_spb'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='l5kqzt',t1=t1,t2=t2)
	util.execute_request(**request)


def call_frm_moscow(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(20,150))
	print 'Adjust : EVENT______________________________call_frm_moscow'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='onq1l0',t1=t1,t2=t2)
	util.execute_request(**request)

def new_car(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(20,150))
	print 'Adjust : EVENT______________________________new_car'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='odl9vi',t1=t1,t2=t2)
	util.execute_request(**request)

def cars(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(0,5))
	print 'Adjust : EVENT______________________________cars'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"fb_currency":"RUB","fb_content_type":"product","fb_content_id":offer_id[val],"_valueToSum":price[val],"transaction_id":str(int(time.time()*1000)),"criteo_p":urllib.quote(str([{"i":offer_id[val],"pr":str(price[val])+'.000000',"q":1}]))})),event_token='xj8ogk',t1=t1,t2=t2)
	util.execute_request(**request)


def moto(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(0,5))
	print 'Adjust : EVENT______________________________moto'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"fb_currency":"RUB","fb_content_type":"product","fb_content_id":offer_id[val],"_valueToSum":price[val],"transaction_id":str(int(time.time()*1000)),"criteo_p":urllib.quote(str([{"i":offer_id[val],"pr":str(price[val])+'.000000',"q":1}])),"customer_id":"40960932"})),event_token='4c6tsa',t1=t1,t2=t2)
	util.execute_request(**request)

def trucks(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(0,5))
	print 'Adjust : EVENT______________________________trucks'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=str(json.dumps({"fb_currency":"RUB","fb_content_type":"product","fb_content_id":offer_id[val],"_valueToSum":price[val],"transaction_id":str(int(time.time()*1000)),"criteo_p":urllib.quote(str([{"i":offer_id[val],"pr":str(price[val])+'.000000',"q":1}])),"customer_id":"40960932"})),event_token='8vfgnp',t1=t1,t2=t2)
	util.execute_request(**request)

def sale(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(180,500))
	print 'Adjust : EVENT______________________________sale'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='r5jcn2',t1=t1,t2=t2)
	util.execute_request(**request)
	

def offer_auto(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(0,1))
	print 'Adjust : EVENT______________________________offer_auto'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='egn2wq',t1=t1,t2=t2)
	util.execute_request(**request)

def offer_moto(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(0,1))
	print 'Adjust : EVENT______________________________offer_moto'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='4uvv81',t1=t1,t2=t2)
	util.execute_request(**request)

def offer_comm(campaign_data, app_data, device_data,t1=2,t2=4):
	time.sleep(random.randint(0,1))
	print 'Adjust : EVENT_____________________________offer_comm'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='l5y339',t1=t1,t2=t2)
	util.execute_request(**request)





###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,created_at,sent_at,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	# set_installedAT(app_data,device_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')
	
	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded",
		# "Authorization" : CHECK AUTH,
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"package_name" : campaign_data.get('package_name'),
		"updated_at" : get_date_ts(app_data,device_data),
		"needs_response_details" : "1",
		"os_name" : "android",
		"device_type" : device_data.get('device_type'),
		"device_manufacturer" : device_data.get('manufacturer'),
		"tracking_enabled" : "1",
		"connectivity_type" : "1",
		"event_buffering_enabled" : "0",
		"installed_at" : get_date_ts(app_data,device_data),
		# "vm_isa" : "arm64",#"arm",
		"environment" : "production",
		"os_version" : device_data.get('os_version'),
		"android_uuid" : app_data.get('android_uuid'),
		"display_width" : device_data.get('resolution').split('x')[1],
		"app_version" : campaign_data.get('app_version_name'),
		"gps_adid" : device_data.get('adid'),
		"screen_size" : get_screen_size(device_data),
		"hardware_name" : device_data.get('hardware'),
		"screen_format" : get_screen_format(device_data),
		"display_height" : device_data.get('resolution').split('x')[0],
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"sent_at" : sent_at,
		"attribution_deeplink" : "1",
		"screen_density" : get_screen_density(device_data),
		"language" : device_data.get('locale').get('language'),
		"country" : device_data.get('locale').get('country'),
		"created_at" : created_at,
		"session_count" : app_data.get('session_count'),
		"device_name" : device_data.get('model'),
		"cpu_type" : device_data.get('cpu_abi'),
		"network_type" : "13",
		"api_level" : device_data.get('sdk'),
		"os_build" : device_data.get('build'),
		'mcc' : device_data.get('mcc'),
		'mnc' : device_data.get('mnc'),
		'gps_adid_src'	: 'service',
		'app_secret' : campaign_data.get('adjust').get('secret_key'),

		}
	if isOpen:
		def_(app_data,'subsession_count')
		# def_sessionLength(app_data)
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		if app_data.get('length'):
			data['session_length'] 	= app_data.get('length')
			data['time_spent'] 		= app_data.get('length')
		else:
			data['session_length'] 	= 0
			data['time_spent'] 		= 0
		data['subsession_count']= app_data.get('subsession_count')
		
		


	message = bytearray(json.dumps(data))
	str_adj = json.dumps(data)+adj1(app_data,device_data,message)+str(time.time())+device_data.get('sdk')+"session"+campaign_data.get('adjust').get('sdk')
	sign= (util.sha256(str_adj)).upper()
	headers['Authorization']= 'Signature signature="'+sign+'",secret_id="1",algorithm="adj1",headers_id="1"'	
	#headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'session')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time,created_at,sent_at,source='',callback_params=None,partner_params=None):
	reftag = ''
	temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
	if temp_b.get('adjust_reftag'):
		reftag = temp_b.get('adjust_reftag')[0]

	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length

	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded",
		# "Authorization" : CHECK AUTH,
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"event_buffering_enabled" : "0",
		"updated_at" : get_date_ts(app_data,device_data),
		"needs_response_details" : "1",
		"time_spent" : time_spent,
		"os_name" : "android",
		"device_type" : device_data.get('device_type'),
		"device_manufacturer" : device_data.get('manufacturer'),
		"last_interval" : get_lastInterval(app_data),
		"connectivity_type" : 1,
		"package_name" : campaign_data.get('package_name'),
		"click_time" : click_time,
		"os_version" : device_data.get('os_version'),
		"installed_at" : get_date_ts(app_data,device_data),
		# "vm_isa" : 'arm64',#"arm",
		"environment" : "production",
		"source" : source,
		"android_uuid" : app_data.get('android_uuid'),
		"display_width" : device_data.get('resolution').split('x')[1],
		"screen_density" : get_screen_density(device_data),
		"raw_referrer" : "utm_source=(not%20set)&utm_medium=(not%20set)",
		"subsession_count" : app_data.get('subsession_count'),
		"session_length" : session_length,
		"gps_adid" : device_data.get('adid'),
		"screen_size" : get_screen_size(device_data),
		"tracking_enabled" : "1",
		"screen_format" : get_screen_format(device_data),
		"display_height" : device_data.get('resolution').split('x')[0],
		"hardware_name" : device_data.get('hardware'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"sent_at" : sent_at,
		"attribution_deeplink" : "1",
		"app_version" : campaign_data.get('app_version_name'),
		"language" : device_data.get('locale').get('language'),
		"referrer" : "utm_source=(not set)&utm_medium=(not set)",
		"created_at" : created_at,
		"session_count" : 1,
		"device_name" : device_data.get('model'),
		"cpu_type" : device_data.get('cpu_abi'),
		"country" : device_data.get('locale').get('country'),
		"network_type" : '13',
		"api_level" : device_data.get('sdk'),
		"os_build" : device_data.get('build'),
		'mcc' : device_data.get('mcc'),
		'mnc' : device_data.get('mnc'),
		'gps_adid_src'	: 'service',
		'app_secret' : campaign_data.get('adjust').get('secret_key'),
		'push_token' : app_data.get('pushtoken'),

		}

	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		# del data['session_length']
		# del data['subsession_count']
		# del data['time_spent']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		# del data['last_interval']



	
	#headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'click')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,created_at,sent_at):
	inc_(app_data,'subsession_count')
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		# "Authorization" : CHECK AUTH,

		}
	params = {
		"android_uuid" : app_data.get('android_uuid'),
		"api_level" : device_data.get('sdk'),
		'app_secret' : campaign_data.get('adjust').get('secret_key'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"created_at" : created_at,
		"device_name" : device_data.get('model'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : "0",
		"gps_adid" : device_data.get('adid'),
		'gps_adid_src'	: 'service',
		"initiated_by" : "backend",
		"needs_response_details" : "1",
		"os_name" : "android",
		"os_version" : device_data.get('os_version'),
		"package_name" : campaign_data.get('package_name'),
		"sent_at" : sent_at,
		"tracking_enabled" : "1",
		'push_token' : app_data.get('pushtoken'),

		}
	data = {

		}

	message = bytearray(json.dumps(params))
	str_adj = json.dumps(params)+adj1(app_data,device_data,message)+str(time.time())+device_data.get('sdk')+"attribution"+campaign_data.get('adjust').get('sdk')
	sign= (util.sha256(str_adj)).upper()
	headers['Authorization']= 'Signature signature="'+sign+'",secret_id="1",algorithm="adj1",headers_id="1"'	
	#headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('gps_adid'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

def device_hello(campaign_data, app_data,device_data):
	
	method = "post"
	url = 'https://hello.apiauto.ru/1.0/device/hello'
	headers = {
		# "Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"User-Agent" : 'okhttp/3.9.1',
		'X-User-Location': 'lat='+app_data.get('latitude')+';lon='+app_data.get('latitude')+';acc=23.455',
		'X-Vertis-Platform': 'android/xhdpi',
		'X-Authorization': 'Vertis android-5442cd88c413ada3ce3d36a3d8061fb7',
		'X-Timezone-Name': device_data.get('local_tz_name'),
		'Content-Type': 'application/json; charset=UTF-8',
		}
	params = {
		
		}
	data = {"app_version":campaign_data.get('app_version_name'),"device":{"brand":device_data.get('brand'),"device":device_data.get('model'),"manufacturer":device_data.get('manufacturer'),"model":device_data.get('model'),"os_version":device_data.get('os_version'),"platform":"ANDROID","product":device_data.get('model')},"remote_config":{"cert_promo_disabled":"0","chat_presets_enabled":"0","google_photos_as_picker_enabled":"1","min_version_youtube_enabled":"21","only_app_ads":"0","order_wrecker_button_enabled":"0","photo_in_price_change_notification_enabled":"0","search_direct_enabled":"0","show_card_banner":"0","zen_enabled":"1"},"supported_features":["SEARCH_MMNG_MULTICHOICE","PHONE_REDIRECTS_IN_COMMERCIAL_FORM","VIN_RESOLUTION_WITH_HISTORY","SEARCH_DYNAMIC_CATALOG_EQUIPMENT","SEARCH_GROUPING_ID"]}

	
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def search_cars(campaign_data, app_data,device_data,choose):
	
	method = "post"
	url = 'https://apiauto.ru/1.0/search/'+choose
	headers = {
		# "Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"User-Agent" : 'okhttp/3.9.1',
		'X-User-Location': 'lat='+app_data.get('latitude')+';lon='+app_data.get('latitude')+';acc=23.455',
		'X-Vertis-Platform': 'android/xhdpi',
		'X-Authorization': 'Vertis android-5442cd88c413ada3ce3d36a3d8061fb7',
		'X-Timezone-Name': device_data.get('local_tz_name'),
		'Content-Type': 'application/json; charset=UTF-8',
		'Cookie':app_data.get('rerf')+';'+app_data.get('X-Vertis-DC'),
		'X-Device-Uid': app_data.get('uid'),
		'X-Session-Id': app_data.get('session_id'),
		}
	params = {
				'page':'1', 
				 'page_size':random.randint(10,20), 
				 'context':'listing', 
				 'sort':'fresh_relevance_1-desc'
		
		}
	if choose=='cars':

		data = {"creation_date_to":str(int(time.time()*1000)),"customs_state_group":"CLEARED","damage_group":"NOT_BEATEN","has_image":'true',"in_stock":"ANY_STOCK","mark_model_nameplate":["VOLKSWAGEN"],"offer_grouping":'true',"only_official":'false',"seller_group":["ANY_SELLER"],"state_group":"ALL","with_discount":'true'}
	if choose=='moto':
		params['sort']='cr_date-desc'
		data = {"creation_date_to":str(int(time.time()*1000)),"customs_state_group":"CLEARED","damage_group":"NOT_BEATEN","has_image":'true',"in_stock":"ANY_STOCK","moto_params":{"moto_category":"MOTORCYCLE","moto_type":["ALLROUND"]},"only_official":'false',"rid":[1],"seller_group":["ANY_SELLER"],"state_group":"ALL"}
	if choose=='trucks':
		params['sort']='cr_date-desc'
		data = {"creation_date_to":str(int(time.time()*1000)),"customs_state_group":"CLEARED","damage_group":"NOT_BEATEN","has_image":'true',"in_stock":"ANY_STOCK","only_official":'false',"rid":[1],"seller_group":["ANY_SELLER"],"state_group":"ALL","trucks_params":{"trucks_category":"LCV"}}

	
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def user_confirm(campaign_data, app_data,device_data):
	
	method = "post"
	url = 'https://apiauto.ru/1.0/user/confirm'
	headers = {
		# "Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"User-Agent" : 'okhttp/3.9.1',
		'X-User-Location': 'lat='+app_data.get('latitude')+';lon='+app_data.get('latitude')+';acc=23.455',
		'X-Vertis-Platform': 'android/xhdpi',
		'X-Authorization': 'Vertis android-5442cd88c413ada3ce3d36a3d8061fb7',
		'X-Timezone-Name': device_data.get('local_tz_name'),
		'Content-Type': 'application/json; charset=UTF-8',
		'Cookie':app_data.get('rerf')+';'+app_data.get('X-Vertis-DC'),
		'X-Device-Uid': app_data.get('uid'),
		'X-Session-Id': app_data.get('session_id'),
		}
	params = {
				
		
		}
	

	data = {"code":"4511","phone":"+7 (911) 806-47-28"}
	

	
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}



###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	# set_installedAT(app_data,device_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	def_sessionLength(app_data)
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length-random.randint(2,5)
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded",
		# "Authorization" : CHECK AUTH,
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"package_name" : campaign_data.get('package_name'),
		"event_token" : event_token,
		"tracking_enabled" : "1",
		"needs_response_details" : "1",
		"time_spent" : time_spent,
		"os_name" : "android",
		"device_type" : device_data.get('device_type'),
		"device_manufacturer" : device_data.get('manufacturer'),
		"connectivity_type" : "1",
		"event_buffering_enabled" : "0",
		# "vm_isa" : 'arm64',#"arm",
		"environment" : "production",
		"os_version" : device_data.get('os_version'),
		"session_length" : session_length,
		"display_width" : device_data.get('resolution').split('x')[1],
		"screen_density" : get_screen_density(device_data),
		"subsession_count" : app_data.get('subsession_count'),
		"android_uuid" : app_data.get('android_uuid'),
		"gps_adid" : device_data.get('adid'),
		"screen_size" : get_screen_size(device_data),
		"hardware_name" : device_data.get('hardware'),
		"screen_format" : get_screen_format(device_data),
		"display_height" : device_data.get('resolution').split('x')[0],
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"sent_at" : sent_at,
		"attribution_deeplink" : "1",
		"app_version" : campaign_data.get('app_version_name'),
		"language" : device_data.get('locale').get('language'),
		"event_count" : app_data.get('event_count'),
		"country" : device_data.get('locale').get('country'),
		"created_at" : created_at,
		"session_count" : app_data.get('session_count'),
		"device_name" : device_data.get('model'),
		"cpu_type" : device_data.get('cpu_abi'),
		"network_type" : "13",
		"api_level" : device_data.get('sdk'),
		"os_build" : device_data.get('build'),
		'mcc' : device_data.get('mcc'),
		'mnc' : device_data.get('mnc'),
		'gps_adid_src'	: 'service',
		'app_secret' : campaign_data.get('adjust').get('secret_key'),
		'push_token' : app_data.get('pushtoken'),
		}

	if callback_params or partner_params:
		data['callback_params']=callback_params
		data['partner_params']=partner_params

	app_data['length']=data['session_length']

	message = bytearray(json.dumps(data))
	str_adj = json.dumps(data)+adj1(app_data,device_data,message)+str(time.time())+device_data.get('sdk')+"event"+campaign_data.get('adjust').get('sdk')
	sign= (util.sha256(str_adj)).upper()
	headers['Authorization']= 'Signature signature="'+sign+'",secret_id="1",algorithm="adj1",headers_id="1"'
	#headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('gps_adid'),'event')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


def adjust_sdkinfo(campaign_data, app_data, device_data,source):

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(2,4))
	sent_at=get_date(app_data,device_data)

	url 	= 'http://app.adjust.com/sdk_info'
	method 	='post'
	headers = {
		'Client-SDK'		: campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		: get_ua(device_data),
		 }
	data = {

		'android_uuid'			:app_data.get('android_uuid'),
		'app_secret' 			: campaign_data.get('adjust').get('secret_key'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:created_at,
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'gps_adid_src'			:'service',
		'needs_response_details':'1',
		'push_token' 			: app_data.get('pushtoken'),
		'sent_at'				:sent_at,
		'source'				:source,
		'tracking_enabled'		:'1',
		}


	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}	



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	


def get_date_ts(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date


def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']='f0L4WFS5AEk:'+util.get_random_string('google_token',140)

def get_auth(device_data,app_data,created_at,gps_adid,activity_type):
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activity_type)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'


def get_install_reciept(app_data):
	install_receipt='MIISiAYJKoZIhvcNAQ'+util.get_random_string('google_token',6314)
	if not app_data.get('install_receipt'):
		app_data['install_receipt']=install_receipt

#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	# st 	 = str(int(time.time()*1000))
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"

####################################################
#
# 		ADJ 1
#
#########################################################
def adj1(app_data,device_data,message):
	digest = hmac.new(campaign_data['adjust']['secret_key'], msg=message, digestmod=hashlib.sha256).digest()
	signature = base64.b64encode(digest).decode()

	return signature			