import uuid,json,random, time,datetime, urllib, string
from Crypto.Cipher import AES
from sdk import getsleep
from sdk import util, NameLists,installtimenew
import clicker, Config


campaign_data = {
			'package_name':'com.opodo.flights',
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'ctr':6,
			'app_name' : 'Opodo',
			'app_size' : 115.2,#115.0,#116.4,
			'app_id':'646692973',
			'no_referrer': True,
			'device_targeting':True,
			'tracker':'mat',
			'app_version_code': '8359',#'7710',#6887, '6404',#"6151",#'5685',#'433', #374
			'app_version_name': '4.80',#'4.79.1',#4.79, '4.78.1',#"4.78",#'4.77.1',#'4.57', #4.51.1
			'supported_os':'11.0',#'10.0', #7.0
			'supported_countries': 'WW',
			'mat' :{
						'ver' : "6.2.0",#'6.0.4',#'5.1.1',
						'sdk' : 'ios',
						'advertiser_id' : '188712',
						'key' : '74e6a7e154ac762ed0d235be82072e18',
						'iv'  :'heF9BATUfWuISyO8',
				},
			'retention':{
							1:70,
							2:68,
							3:66,
							4:64,
							5:61,
							6:59,
							7:57,
							8:52,
							9:50,
							10:47,
							11:45,
							12:43,
							13:40,
							14:37,
							15:35,
							16:31,
							17:30,
							18:28,
							19:26,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
						}
		}

		
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)

def encrypt_AES(key, data):
	data=pad(data.encode('hex').upper())
	return AES.new(key, AES.MODE_ECB).encrypt(data).encode('hex').upper()
	
def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	if not gender_n:
		app_data['gender'] = random.choice(['male', 'female'])
		gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	if not user_info:
		user_data = util.generate_name(gender=gender_n)
		user_info = {}
		user_info['username'] = user_data.get('username').lower()
		user_info['email'] = user_data.get('username').lower() + '@gmail.com'
		user_info['f_name'] = user_data.get('firstname')
		user_info['l_name'] = user_data.get('lastname')
		user_info['sex'] = gender_n
		user_info['gender'] = str(1) if gender_n == 'female' else str(2)
		user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
		user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
		user_info['password'] = util.get_random_string(type='all',size=16)
		app_data['user_info'] = user_info

def make_checksum(User_agent,Xoom_device_id,time_2):
	data=str(User_agent)+':'+str(Xoom_device_id)+':'+str(time_2)+':'+"da7e138c86314501bba3845421e38e83"
	data= util.sha256(data)
	return data

def make_anonymous_id(app_data):
	if not app_data.get('anonymous_id'):
		app_data['anonymous_id'] = str(uuid.uuid4()).upper()
		
	return app_data.get('anonymous_id')

def def_userid(app_data):
	if not app_data.get('user_id'):
		app_data['user_id']=str(util.get_random_string('hex',20))

def def_ad_id(app_data):
	if not app_data.get('ad_id'):
		app_data['ad_id']=str(uuid.uuid4())
	
def make_session_id(app_data):
	if not app_data.get('session_id'):	
		app_data['session_id'] = str(uuid.uuid4()).replace("-","").lower()
	return app_data.get('session_id')
	
def make_client_secret(app_data):
	if not app_data.get('client_secret_id'):
		app_data['client_secret_id'] = str(uuid.uuid4()).upper()
	return app_data.get('client_secret_id')
	
def make_blackbox_string(app_data):
	if not app_data.get('user_blackbox_string'):
		app_data['user_blackbox_string'] = ''.join(random.choice(string.digits + string.ascii_letters + '/+' ) for _ in range(580))
		
	return app_data.get('user_blackbox_string')

def make_distinct_id(app_data):
	if not app_data.get('distinct_id_1'):
		app_data['distinct_id_1'] = util.get_random_string("decimal",14)+'-'+util.get_random_string("decimal",15)+'-'+util.get_random_string("decimal",8)+'-'+util.get_random_string("decimal",5)+'-'+util.get_random_string("decimal",14)
	return app_data.get('distinct_id_1')	
	
def install(app_data, device_data):

	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')

	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	def_userid(app_data)
	register_user(app_data)
	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

	print 'lat and lon generator'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	try:
		json_lat_lon_result=json.loads(lat_lon_result.get('data'))
		app_data['lat']=str(json_lat_lon_result.get('geo').get('latitude'))+str(util.get_random_string('decimal',5))
		app_data['lon']=str(json_lat_lon_result.get('geo').get('longitude'))+str(util.get_random_string('decimal',5))
	except:
		print "Exception"
		app_data['lat']="28.6358"
		app_data['lon']="77.2245"

	print"--------------------- MAT 1 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'session',True,data=1)
	app_data['api_hit_time']=time.time()
	mat1_output = util.execute_request(**mat1)
	try:
		mat1_decode = json.loads(mat1_output.get('data'))
		app_data['open_log_id'] = mat1_decode.get('log_id')
		app_data['last_open_log_id'] = mat1_decode.get('log_id')
	except:
		print "Exception"
		app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
		app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')	
	

	print"--------------------- MAT 2 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:launch',True,call=1,data=1)
	mat1_output = util.execute_request(**mat1)

	time.sleep(random.randint(2,4))
	
	print"--------------------- MAT 10 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'install',True)
	mat1_output = util.execute_request(**mat1)

	time.sleep(random.randint(1,3))

	print"--------------------- MAT 3 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:launch',True,call=2)
	mat1_output = util.execute_request(**mat1)

	time.sleep(random.randint(2,4))

	print"--------------------- MAT 2 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:deeplink_launch',True,call=1,data=1)
	mat1_output = util.execute_request(**mat1)

	time.sleep(random.randint(2,4))

	print"--------------------- MAT 5 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:launch',True,call=3)
	mat1_output = util.execute_request(**mat1)

	##################################################################

	print "\nself_call_sl_odige\n"
	request=sl_odige_nearestLocations( campaign_data, device_data, app_data )
	response = util.execute_request(**request)	 
	global iataCode_list
	iataCode_list = list()
	try:
		result = json.loads(response.get('data')).get('cities')
		for item in result:
			a = {}
			a['cityName'] = item.get('cityName')
			a['iataCode'] = item.get('iataCode')
			iataCode_list.append(a)

		if not iataCode_list:
			iataCode_list = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]	  

	except:
		iataCode_list = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]

	print "\nself_call_sl_odige\n"
	request=sl_odige_locations( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global iataCode_list_2
	iataCode_list_2 = list()
	try:
		result = json.loads(response.get('data')).get('locations')
		for item in result:
			a = {}
			a['cityName'] = item.get('cityName')
			a['iataCode'] = item.get('iataCode')
			iataCode_list_2.append(a)

		if not iataCode_list_2:
			iataCode_list_2 = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]	  

	except:
		iataCode_list_2 = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]		 

	global message,price_list
	message = [1]
	counter=0
	price_list = list()
	while counter<5:
		print "\nself_call_sl_odige\n"
		request=sl_odige_search( campaign_data, device_data, app_data )
		response = util.execute_request(**request)
		counter += 1
		try:
			result = json.loads(response.get('data'))
			message = result.get('messages')
			itineraryResults = result.get('itineraryResultsPage').get('itineraryResults')
			for item in itineraryResults:
				price_list.append(item.get('price'))
			if message==[]:
				break		
		except:
			price_list = [{'sortPrice': 2846.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.66}}, 'markup': 23.47, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.12, 'tax': 275.74, 'taxDetails': []}}}, {'sortPrice': 2916.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.32}}, 'markup': 23.49, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.13, 'tax': 346.06, 'taxDetails': []}}}, {'sortPrice': 2924.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.19}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 397.8, 'taxDetails': []}}}, {'sortPrice': 2934.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.78}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 407.21, 'taxDetails': []}}}, {'sortPrice': 2942.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.48}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2474.0, 'tax': 401.51, 'taxDetails': []}}}, {'sortPrice': 3153.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.01}}, 'markup': 5.63, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2734.13, 'tax': 358.22, 'taxDetails': []}}}, {'sortPrice': 4306.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.74}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3370.0, 'tax': 880.25, 'taxDetails': []}}}, {'sortPrice': 4911.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.23}}, 'markup': 133.55, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3883.29, 'tax': 838.92, 'taxDetails': []}}}, {'sortPrice': 5930.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.43}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 4832.0, 'tax': 1031.56, 'taxDetails': []}}}, {'sortPrice': 6171.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.97}}, 'markup': 38.69, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5042.1, 'tax': 1035.23, 'taxDetails': []}}}, {'sortPrice': 6377.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.2}}, 'markup': 23.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5239.65, 'tax': 1058.94, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6550.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.54}}, 'markup': 10.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5405.42, 'tax': 1078.83, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}]   

	if not price_list:
		price_list = [{'sortPrice': 2846.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.66}}, 'markup': 23.47, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.12, 'tax': 275.74, 'taxDetails': []}}}, {'sortPrice': 2916.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.32}}, 'markup': 23.49, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.13, 'tax': 346.06, 'taxDetails': []}}}, {'sortPrice': 2924.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.19}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 397.8, 'taxDetails': []}}}, {'sortPrice': 2934.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.78}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 407.21, 'taxDetails': []}}}, {'sortPrice': 2942.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.48}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2474.0, 'tax': 401.51, 'taxDetails': []}}}, {'sortPrice': 3153.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.01}}, 'markup': 5.63, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2734.13, 'tax': 358.22, 'taxDetails': []}}}, {'sortPrice': 4306.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.74}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3370.0, 'tax': 880.25, 'taxDetails': []}}}, {'sortPrice': 4911.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.23}}, 'markup': 133.55, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3883.29, 'tax': 838.92, 'taxDetails': []}}}, {'sortPrice': 5930.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.43}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 4832.0, 'tax': 1031.56, 'taxDetails': []}}}, {'sortPrice': 6171.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.97}}, 'markup': 38.69, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5042.1, 'tax': 1035.23, 'taxDetails': []}}}, {'sortPrice': 6377.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.2}}, 'markup': 23.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5239.65, 'tax': 1058.94, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6550.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.54}}, 'markup': 10.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5405.42, 'tax': 1078.83, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}]		

	 

	##################################################################

	if random.randint(1,100)<=30:

		time.sleep(random.randint(1,3))

		print"--------------------- MAT 6 ---------------------"
		mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:search',True,call=1)
		mat1_output = util.execute_request(**mat1)

		print"--------------------- MAT 7 ---------------------"
		mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:search',True,call=2,data=2)
		mat1_output = util.execute_request(**mat1)

		if random.randint(1,100)<=83:

			print "\nself_call_sl_odige\n"
			request=sl_odige_priceBreakdown( campaign_data, device_data, app_data )
			response = util.execute_request(**request)
			global price
			try:
				result = json.loads(response.get('data')).get('pricingBreakdownSteps')[0].get('pricingBreakdownItems')[0].get('priceItemAmount')	
				price = result
			except:
				price = random.choice([1441.09,6717.31,2576.2])

			time.sleep(random.randint(2,4))

			print"--------------------- MAT 8 ---------------------"
			mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:content_view',True,data=3)
			mat1_output = util.execute_request(**mat1)

			if random.randint(1,100)<=50:
				print"--------------------- MAT 9 ---------------------"
				mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:checkout_initiated',True,data=3)
				mat1_output = util.execute_request(**mat1)

	return {"status" : "true"}
	
def open(app_data,device_data,day=1):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')

	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	def_userid(app_data)
	register_user(app_data)
	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

	print 'lat and lon generator'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	try:
		json_lat_lon_result=json.loads(lat_lon_result.get('data'))
		app_data['lat']=str(json_lat_lon_result.get('geo').get('latitude'))+str(util.get_random_string('decimal',5))
		app_data['lon']=str(json_lat_lon_result.get('geo').get('longitude'))+str(util.get_random_string('decimal',5))
	except:
		print "Exception"
		app_data['lat']="28.6358"
		app_data['lon']="77.2245"

	print"--------------------- MAT 1 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'session',True,data=1)
	mat1_output = util.execute_request(**mat1)
	try:
		mat1_decode = json.loads(mat1_output.get('data'))
		if not app_data.get('open_log_id'):
			app_data['open_log_id'] = mat1_decode.get('log_id')
		app_data['last_open_log_id'] = mat1_decode.get('log_id')
	except:
		print "Exception"
		if not app_data.get('open_log_id'):
			app_data['open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')
		app_data['last_open_log_id'] = str(util.get_random_string('hex',32))+'-'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d"))+'-'+campaign_data.get('mat').get('advertiser_id')	
	

	print"--------------------- MAT 2 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:launch',True,call=1,data=1)
	mat1_output = util.execute_request(**mat1)

	time.sleep(random.randint(2,4))

	print"--------------------- MAT 2 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:deeplink_launch',True,call=1,data=1)
	mat1_output = util.execute_request(**mat1)

	time.sleep(random.randint(2,4))

	print"--------------------- MAT 3 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:launch',True,call=2)
	mat1_output = util.execute_request(**mat1)

	time.sleep(random.randint(2,4))

	print"--------------------- MAT 5 ---------------------"
	mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:launch',True,call=3)
	mat1_output = util.execute_request(**mat1)

	##################################################################

	print "\nself_call_sl_odige\n"
	request=sl_odige_nearestLocations( campaign_data, device_data, app_data )
	response = util.execute_request(**request)	 
	global iataCode_list
	iataCode_list = list()
	try:
		result = json.loads(response.get('data')).get('cities')
		for item in result:
			a = {}
			a['cityName'] = item.get('cityName')
			a['iataCode'] = item.get('iataCode')
			iataCode_list.append(a)

		if not iataCode_list:
			iataCode_list = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]	  

	except:
		iataCode_list = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]

	print "\nself_call_sl_odige\n"
	request=sl_odige_locations( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global iataCode_list_2
	iataCode_list_2 = list()
	try:
		result = json.loads(response.get('data')).get('locations')
		for item in result:
			a = {}
			a['cityName'] = item.get('cityName')
			a['iataCode'] = item.get('iataCode')
			iataCode_list_2.append(a)

		if not iataCode_list_2:
			iataCode_list_2 = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]	  

	except:
		iataCode_list_2 = [{'iataCode': 'SWF', 'cityName': 'Newburgh, NY'}, {'iataCode': 'NYC', 'cityName': 'New York'}, {'iataCode': 'EWR', 'cityName': 'Newark - Nj'}, {'iataCode': 'DEL', 'cityName': 'New Delhi'}, {'iataCode': 'AKL', 'cityName': 'Auckland'}, {'iataCode': 'NCL', 'cityName': 'Newcastle'}, {'iataCode': 'CHC', 'cityName': 'Christchurch'}, {'iataCode': 'NO', 'cityName': 'Noumea'}, {'iataCode': 'MSY', 'cityName': 'New Orleans'}, {'iataCode': 'NQY', 'cityName': 'Newquay'}, {'iataCode': 'WLG', 'cityName': 'Wellington'}, {'iataCode': 'ZQN', 'cityName': 'Queenstown'}, {'iataCode': 'POM', 'cityName': 'Port Moresby'}, {'iataCode': 'NTL', 'cityName': 'Newcastle, NSW'}, {'iataCode': 'DUD', 'cityName': 'Dunedin'}, {'iataCode': 'HG', 'cityName': 'Mount Hagen'}, {'iataCode': 'PHF', 'cityName': 'Newport News, VA'}, {'iataCode': 'RAB', 'cityName': 'Rabaul'}, {'iataCode': 'ROT', 'cityName': 'Rotorua'}, {'iataCode': 'NSN', 'cityName': 'Nelson'}, {'iataCode': 'HKK', 'cityName': 'Hokitika'}, {'iataCode': 'NPL', 'cityName': 'New Plymouth'}, {'iataCode': 'PMR', 'cityName': 'Palmerston North'}, {'iataCode': 'EHM', 'cityName': 'Cape Newenham'}, {'iataCode': 'FNE', 'cityName': 'Fane'}, {'iataCode': 'GKA', 'cityName': 'Goroka'}, {'iataCode': 'GON', 'cityName': 'Groton New London'}, {'iataCode': 'HKN', 'cityName': 'Hoskins'}, {'iataCode': 'BMY', 'cityName': 'Ile Art'}, {'iataCode': 'ILP', 'cityName': 'Ile Des Pins'}, {'iataCode': 'KNQ', 'cityName': 'Kone'}, {'iataCode': 'KOC', 'cityName': 'Koumac'}, {'iataCode': 'LIF', 'cityName': 'Lifo'}, {'iataCode': 'LNV', 'cityName': 'Londolovit'}, {'iataCode': 'MAG', 'cityName': 'Madang'}, {'iataCode': 'MEE', 'cityName': 'Mare'}, {'iataCode': 'PDC', 'cityName': 'Mueo'}, {'iataCode': 'EWB', 'cityName': 'New Bedford, MA'}, {'iataCode': 'EWN', 'cityName': 'New Bern, NC'}, {'iataCode': 'NCS', 'cityName': 'Newcastle'}, {'iataCode': 'VLF', 'cityName': 'Newcastle Under Lyme'}, {'iataCode': 'ECS', 'cityName': 'Newcastle'}, {'iataCode': 'NCN', 'cityName': 'New Chenega'}, {'iataCode': 'YPS', 'cityName': 'New Glasgow'}, {'iataCode': 'NHF', 'cityName': 'New Halfa'}, {'iataCode': 'HVN', 'cityName': 'New Haven, CT'}, {'iataCode': 'ARA', 'cityName': 'New Iberia,La'}, {'iataCode': 'KGK', 'cityName': 'New Koliganek'}, {'iataCode': 'LUT', 'cityName': 'New Laura'}, {'iataCode': 'ZNE', 'cityName': 'Newman, WA'}, {'iataCode': 'NMP', 'cityName': 'New Moon'}, {'iataCode': 'PHD', 'cityName': 'New Philadelphia'}, {'iataCode': 'EFK', 'cityName': 'Newport'}, {'iataCode': 'NPT', 'cityName': 'Newport'}, {'iataCode': 'NWH', 'cityName': 'Newport'}, {'iataCode': 'ONP', 'cityName': 'Newport'}, {'iataCode': 'RNH', 'cityName': 'New Richmond,Wi'}, {'iataCode': 'NRY', 'cityName': 'Newry'}, {'iataCode': 'KNW', 'cityName': 'New Stuyahok'}, {'iataCode': 'WWT', 'cityName': 'Newtok'}, {'iataCode': 'EWK', 'cityName': 'Newton'}, {'iataCode': 'TN', 'cityName': 'Newton'}, {'iataCode': 'ULM', 'cityName': 'New Ulm,Mn'}, {'iataCode': 'YBD', 'cityName': 'New Westminster'}, {'iataCode': 'PUV', 'cityName': 'Poum'}, {'iataCode': 'TIZ', 'cityName': 'Tari'}, {'iataCode': 'WWK', 'cityName': 'Wewak'}, {'iataCode': 'AEP', 'cityName': 'Buenos Aires'}, {'iataCode': 'NEV', 'cityName': 'Nevis'}, {'iataCode': 'ILM', 'cityName': 'Wilmington, NC'}, {'iataCode': 'PZ', 'cityName': 'Port Sudan'}, {'iataCode': 'PSK', 'cityName': 'Dublin'}]		 

	global message,price_list
	message = [1]
	counter=0
	price_list = list()
	while counter<5:
		print "\nself_call_sl_odige\n"
		request=sl_odige_search( campaign_data, device_data, app_data )
		response = util.execute_request(**request)
		counter += 1
		try:
			result = json.loads(response.get('data'))
			message = result.get('messages')
			itineraryResults = result.get('itineraryResultsPage').get('itineraryResults')
			for item in itineraryResults:
				price_list.append(item.get('price'))
			if message==[]:
				break		
		except:
			price_list = [{'sortPrice': 2846.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.66}}, 'markup': 23.47, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.12, 'tax': 275.74, 'taxDetails': []}}}, {'sortPrice': 2916.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.32}}, 'markup': 23.49, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.13, 'tax': 346.06, 'taxDetails': []}}}, {'sortPrice': 2924.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.19}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 397.8, 'taxDetails': []}}}, {'sortPrice': 2934.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.78}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 407.21, 'taxDetails': []}}}, {'sortPrice': 2942.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.48}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2474.0, 'tax': 401.51, 'taxDetails': []}}}, {'sortPrice': 3153.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.01}}, 'markup': 5.63, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2734.13, 'tax': 358.22, 'taxDetails': []}}}, {'sortPrice': 4306.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.74}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3370.0, 'tax': 880.25, 'taxDetails': []}}}, {'sortPrice': 4911.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.23}}, 'markup': 133.55, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3883.29, 'tax': 838.92, 'taxDetails': []}}}, {'sortPrice': 5930.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.43}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 4832.0, 'tax': 1031.56, 'taxDetails': []}}}, {'sortPrice': 6171.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.97}}, 'markup': 38.69, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5042.1, 'tax': 1035.23, 'taxDetails': []}}}, {'sortPrice': 6377.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.2}}, 'markup': 23.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5239.65, 'tax': 1058.94, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6550.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.54}}, 'markup': 10.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5405.42, 'tax': 1078.83, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}]   

	if not price_list:
		price_list = [{'sortPrice': 2846.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.66}}, 'markup': 23.47, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.12, 'tax': 275.74, 'taxDetails': []}}}, {'sortPrice': 2916.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.32}}, 'markup': 23.49, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2491.13, 'tax': 346.06, 'taxDetails': []}}}, {'sortPrice': 2924.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.19}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 397.8, 'taxDetails': []}}}, {'sortPrice': 2934.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.78}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2460.0, 'tax': 407.21, 'taxDetails': []}}}, {'sortPrice': 2942.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.48}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2474.0, 'tax': 401.51, 'taxDetails': []}}}, {'sortPrice': 3153.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.01}}, 'markup': 5.63, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 2734.13, 'tax': 358.22, 'taxDetails': []}}}, {'sortPrice': 4306.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.74}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3370.0, 'tax': 880.25, 'taxDetails': []}}}, {'sortPrice': 4911.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.23}}, 'markup': 133.55, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 3883.29, 'tax': 838.92, 'taxDetails': []}}}, {'sortPrice': 5930.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': -9.0, 'tax': 58.43}}, 'markup': 0.0, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 4832.0, 'tax': 1031.56, 'taxDetails': []}}}, {'sortPrice': 6171.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.97}}, 'markup': 38.69, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5042.1, 'tax': 1035.23, 'taxDetails': []}}}, {'sortPrice': 6377.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.2}}, 'markup': 23.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5239.65, 'tax': 1058.94, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6544.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 57.96}}, 'markup': 10.61, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1078.2, 'taxDetails': []}}}, {'sortPrice': 6550.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.54}}, 'markup': 10.2, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5405.42, 'tax': 1078.83, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6560.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.46}}, 'markup': 9.44, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5400.22, 'tax': 1094.86, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}, {'sortPrice': 6652.99, 'feeInfo': {'paymentFee': {'discount': 0, 'tax': 0}, 'searchFee': {'discount': 2.0, 'tax': 58.48}}, 'markup': 2.52, 'fareDetail': {'infantFares': None, 'fare': None, 'childFares': None, 'tax': None, 'adultFares': {'fare': 5503.41, 'tax': 1090.58, 'taxDetails': []}}}]		

	 

	##################################################################

	if random.randint(1,100)<=30:

		time.sleep(random.randint(1,3))

		print"--------------------- MAT 6 ---------------------"
		mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:search',True,call=1)
		mat1_output = util.execute_request(**mat1)

		print"--------------------- MAT 7 ---------------------"
		mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:search',True,call=2,data=2)
		mat1_output = util.execute_request(**mat1)

		if random.randint(1,100)<=83:

			print "\nself_call_sl_odige\n"
			request=sl_odige_priceBreakdown( campaign_data, device_data, app_data )
			response = util.execute_request(**request)
			global price
			try:
				result = json.loads(response.get('data')).get('pricingBreakdownSteps')[0].get('pricingBreakdownItems')[0].get('priceItemAmount')	
				price = result
			except:
				price = random.choice([1441.09,6717.31,2576.2])

			time.sleep(random.randint(2,4))

			print"--------------------- MAT 8 ---------------------"
			mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:content_view',True,data=3)
			mat1_output = util.execute_request(**mat1)

			if random.randint(1,100)<=50:
				print"--------------------- MAT 9 ---------------------"
				mat1 = mat_serve(campaign_data, app_data, device_data,'conversion:checkout_initiated',True,data=3)
				mat1_output = util.execute_request(**mat1)

	return {"status" : "true"}


#####################################################################
#
#					  Self Calls
#
#####################################################################


def sl_odige_locations( campaign_data, device_data, app_data ):
	url= "https://msl.odigeo.com/mobile-api/msl/locations"
	method= "get"
	headers= {	   
		"Accept": "application/vnd.com.odigeo.msl.v4+json;charset=utf-8",
		"Accept-Encoding": "gzip",
		"Content-Type": "application/json",
		"Device-ID": "IPHONE;"+device_data.get('model')+";"+device_data.get('os_version')+";O;OPGB;c8ff196a-b7aa-4bb7-bdd8-089a525c8510;en;en;"+campaign_data.get('app_version_name')+";"+campaign_data.get('app_version_code')+";XL",
		"User-Agent": campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		}

	params= {	   
		"departureOrArrival": "DEPARTURE",
		"distanceUnit": "KM",
		"productType": "FLIGHT",
		"searchKey": util.country_list.get(random.choice(util.country_list.keys())).get('apolitical_name'),
		}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

	
def sl_odige_nearestLocations( campaign_data, device_data, app_data ):
	url= "https://msl.odigeo.com/mobile-api/msl/nearestLocations"
	method= "get"
	headers= {	   
		"Accept": "application/vnd.com.odigeo.msl.v3+json;charset=utf-8",
		"Accept-Encoding": "gzip,deflate,sdch",
		"If-None-Match": "",
		"User-Agent": campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		}

	params= {	   
		"latitude": app_data.get('lat'),
		"locale": device_data.get('locale').get('language'),
		"longitude": app_data.get('lon'),
		"productType": "FLIGHT",
		"radiusInKm": "100"
		}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	


def sl_odige_search( campaign_data, device_data, app_data ):
	url= "https://msl.odigeo.com/mobile-api/msl/search"
	method= "post"
	headers= {	   
		"Accept": "application/vnd.com.odigeo.msl.v3+json;charset=utf-8",
		"Accept-Encoding": "gzip",
		"Content-Type": "application/json; charset=UTF-8",
		"Device-ID": "IPHONE;"+device_data.get('model')+";"+device_data.get('os_version')+";O;OPGB;c8ff196a-b7aa-4bb7-bdd8-089a525c8510;en;en;"+campaign_data.get('app_version_name')+";"+campaign_data.get('app_version_code')+";XL",
		"User-Agent":campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		}

	params= None

	global choice_1,choice_2,adults,num1,num2
	num1 = int(time.time()+(random.randint(2,5)*24*60*60))
	num2 = int(num1+(random.randint(1,3)*24*60*60))
	choice_1 = random.choice(iataCode_list)
	choice_2 = random.choice(iataCode_list_2)
	adults = 1 #random.randint(1,2)

	data= {	   
			"itinerarySearchRequest": {	   
			"cabinClass": "TOURIST",
			"isMember": False,
			"numAdults": adults,
			"numChildren": 0,
			"numInfants": 0,
			"resident": False,
			"searchMainProductType": "FLIGHT",
			"segmentRequests": [	   
									{	   
										"dateString": str(datetime.datetime.utcfromtimestamp(num1).strftime('%d-%m-%Y')),
										"departure": {	   
														"iataCode": choice_1.get('iataCode'),
														"name": choice_1.get('cityName')
													},
										 "destination": {	   
															"iataCode": choice_2.get('iataCode'),
															"name": choice_2.get('cityName')
														}
									},
									{	   
										"dateString": str(datetime.datetime.utcfromtimestamp(num2).strftime('%d-%m-%Y')),
										"departure": {	   
														"iataCode": choice_2.get('iataCode'),
														"name": choice_2.get('cityName')
													},
										 "destination": {	   
															"iataCode": choice_1.get('iataCode'),
															"name": choice_1.get('cityName')
														}
									}
								]
							}
						}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}	

   
def sl_odige_priceBreakdown( campaign_data, device_data, app_data ):
	url= "https://msl.odigeo.com/mobile-api/msl/priceBreakdown"
	method= "post"
	headers= {	   
		"Accept": "application/vnd.com.odigeo.msl.v5+json;charset=utf-8",
		"Accept-Encoding": "gzip,deflate,sdch",
		"Content-Type": "application/json",
		"Device-ID": "IPHONE;"+device_data.get('model')+";"+device_data.get('os_version')+";O;OPGB;c8ff196a-b7aa-4bb7-bdd8-089a525c8510;en;en;"+campaign_data.get('app_version_name')+";"+campaign_data.get('app_version_code')+";XL",
		"User-Agent": campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		}

	params= None

	data= {	   
		"numAdults": adults,
		"numChildren": 0,
		"numInfants": 0,
		"price": random.choice(price_list),
		"sortCriteria": "MINIMUM_PURCHASABLE_PRICE"
		}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}	

	
# ###################################################
# 													#
# 				MAT Funcation 	 					#
# 													#
# ###################################################



def mat_serve(campaign_data, app_data, device_data,action,uid=True,call=1,data={}):
	def_userid(app_data)
	param = action.split(':')
	action = param[0]


	url = 'http://%s.engine.mobileapptracking.com/serve' % campaign_data.get('mat').get('advertiser_id')
	headers = {
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/json',
				'Accept-Language':device_data.get('locale').get('language').lower()+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	method = 'post'
	params = {
				'action':action,
				'advertiser_id':campaign_data.get('mat').get('advertiser_id'),
				'package_name':campaign_data.get('package_name'),
				'response_format':'json',
				'sdk':campaign_data.get('mat').get('sdk'),
				'transaction_id':str(uuid.uuid4()).upper(),
				'ver':campaign_data.get('mat').get('ver'),
				'sdk_retry_attempt':'0',
		}
			
		

	if not app_data.get('mat_id'):
		app_data['mat_id'] = str(uuid.uuid4()).upper()

	if not app_data.get('ios_device_data'):
		ios_device_data(app_data)

	if not app_data.get('device_cpu_type'):
		for key in app_data.get('ios_device_data'):
			if key == device_data.get('device'):
				app_data['device_cpu_type'] = app_data.get('ios_device_data').get(key)
				break
			else:
				app_data['device_cpu_type']=16777228	

	data_value = {
			'app_version_name' : campaign_data.get('app_version_name'),
			'build':device_data.get('build'),
			'bundle_id' : campaign_data.get('package_name'),
			'currency_code':'USD',
			'device_cpu_subtype':1,
			'device_cpu_type':app_data.get('device_cpu_type'),
			'ios_purchase_status':'-192837465',
			'is_testflight_build' : 0,
			'locale':device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
			'rating':0,
			'quantity':0,
			'screen_density':util.getdensity(device_data.get('dpi')),
			'screen_size':device_data.get('resolution'),			
			'app_name':campaign_data.get('app_name'),
			'app_version':campaign_data.get('app_version_code'),
			'connection_type' : device_data.get('network'),
			'conversion_user_agent':device_data.get('User-Agent'),
			'country_code':device_data.get('locale').get('country'),
			'device_brand':'Apple',
			'device_carrier':device_data.get('carrier'),
			'device_model':device_data.get('device_platform'),
			'insdate': str(int(app_data.get('times').get('install_complete_time'))),
			'ios_ad_tracking':0,
			#'ios_ifa':app_data.get('idfa_id'),
			'ios_ifv':app_data.get('idfv_id'),
			'language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
			'mat_id':app_data.get('mat_id'),
			'os_jailbroke':0,
			'os_version':device_data.get('os_version'),
			#'package_name':campaign_data.get('package_name'),
			'revenue':0,
			'system_date': str(int(time.time())),  
			'location_auth_status': 0,
			'user_id': app_data.get('user_id'),
			'iad_attribution':'0',
			'existing_user':'1',
			'altitude' : random.uniform(200,300),
			'latitude' : app_data['lat'],
			'level' : 0,
			'location_horizontal_accuracy' : random.randint(50,100),
			'location_timestamp' : str(int(time.time())),
			'location_vertical_accuracy' : random.randint(5,15),
			'longitude' : app_data['lon'],
			'carrier_country_code':device_data.get('locale').get('country').lower(),
			'mobile_country_code' : device_data.get('mcc'),
			'mobile_network_code' : device_data.get('mnc')
		}
	

	if uid==True:
		data_value.pop('user_id')	
		data_value.pop('iad_attribution')
		data_value.pop('existing_user')

	if action == 'session':
			data_value.pop('location_horizontal_accuracy')	
			data_value.pop('location_timestamp')
			data_value.pop('location_vertical_accuracy')
			data_value.pop('altitude')
			data_value.pop('longitude')
			data_value.pop('latitude')	
		
	if all([app_data.get('open_log_id'),app_data.get('last_open_log_id')]):
		data_value['open_log_id'] = str(app_data['open_log_id'])
		data_value['last_open_log_id'] = str(app_data['last_open_log_id'])
 
	if action == 'conversion':
		params['site_event_name'] = param[1]
		if param[1] == 'launch':	
			if call == 1 or call == 2:
				data_value.pop('location_horizontal_accuracy')	
				data_value.pop('location_timestamp')
				data_value.pop('location_vertical_accuracy')
				data_value.pop('altitude')
				data_value.pop('longitude')
				data_value.pop('latitude')
				if call == 1:
					if all([data_value.get('open_log_id'),data_value.get('last_open_log_id')]):
						data_value.pop('open_log_id')
						data_value.pop('last_open_log_id')

		if param[1] == 'deeplink_launch':
			data_value.pop('location_horizontal_accuracy')	
			data_value.pop('location_timestamp')
			data_value.pop('location_vertical_accuracy')
			data_value.pop('altitude')
			data_value.pop('longitude')
			data_value.pop('latitude')	
			params['referral_source']='com.opodo.flights'
			params['referral_url']='op-app://trips'
			params['response_format']='json'

		if param[1] == 'search':
			if call == 2:
			   data_value['attribute_sub1'] = 1
			   data_value['attribute_sub2'] = 'R'
			   data_value['advertiser_ref_id'] = 0
			   data_value['date1'] = num1
			   data_value['date2'] = num2

		   	data = {
				'data' : {
						 '{}':{
								'item':str(choice_1.get('iataCode'))+"-"+str(choice_2.get('iataCode')),
								'quantity':'1',
								'revenue' : '0',
								'unit_price':'0'
							   }
						  }
			   } 

		if param[1] == 'content_view':
			data_value['attribute_sub1'] = 1
			data_value['attribute_sub2'] = 'R'
			data_value['advertiser_ref_id'] = 4141394568
			data_value['date1'] = num1

			data = {
				'data' : {
						 '{}':{
								'item':str(choice_1.get('iataCode'))+"-"+str(choice_2.get('iataCode')),
								'quantity':'1',
								'revenue' : str(price),
								'unit_price':str(price)
							   }
						  }
			   }

		if param[1] == 'checkout_initiated':
			data_value['attribute_sub1'] = 0
			data_value['attribute_sub2'] = 'R'
			data_value['advertiser_ref_id'] = 4141394568
			data_value['date1'] = num1
					
	if action=='install':
		params['post_conversion'] = '1'		

	da_str = urllib.urlencode(data_value)

	key = campaign_data.get('mat').get('key')
	data_str = encrypt_AES(key,da_str)
	
	params['data'] = data_str
	
	data1 = {
			'apple_receipt' : apple_receipt(),
		   }
			  

	if data == 1:
	   data = data1
			
		
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def ios_device_data(app_data):
	if not app_data.get('ios_device_data'):
		app_data['ios_device_data'] = {
		'iPad mini 2':'16777228',
		'iPad mini Wi-Fi':'12',
		'iPad mini Wi-Fi + Cellular':'12',
		'iPad 4 Wi-Fi':'16777228',
		'iPad 4 Wi-Fi + Cellular':'16777228',
		'iPhone 7 Plus':'16777228',
		'iPhone 7':'16777228',
		'iPhone SE':'16777228',
		'iPhone 6s Plus':'16777228',
		'iPhone 6s':'16777228',
		'iPhone 6 Plus':'16777228',
		'iPhone 6':'16777228', 
		'iPhone 5s':'16777228', 
		'iPhone 5c':'12',
		'iPhone 5 CDMA':'12',
		'iPhone 5':'12',
		'iPhone 4s':'12',
		'iPhone 4 CDMA':'12',
		'iPhone 4':'12',
		'iPad Air':'16777228',
		'iPad mini 3':'16777228',
		'iPad Air 2':'16777228',
		'iPad mini 4':'16777228'
		}
	return app_data.get('ios_device_data')

def current_location(device_data):
	url='http://lumtest.com/myip.json'
		
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}

	
# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0


def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"	

def apple_receipt():
	return 'MIISfwYJKoZIhvcNAQcCoIIScDCCEmwCAQExCzAJBgUrDgMCGgUAMIICIAYJKoZIhvcNAQcBoIICEQSCAg0xggIJMAoCARQCAQEEAgwAMAsCAQ4CAQEEAwIBWjALAgEZAgEBBAMCAQMwDAIBCgIBAQQEFgI0KzANAgELAgEBBAUCAw7ItjANAgENAgEBBAUCAwHVJjANAgETAgEBBAUMAzM3NDAOAgEBAgEBBAYCBCaLwG0wDgIBAwIBAQQGDAQ3NzEwMA4CAQkCAQEEBgIEUDI1MzAOAgEQAgEBBAYCBDGoCV4wEAIBDwIBAQQIAgY941SvPCkwFAIBAAIBAQQMDApQcm9kdWN0aW9uMBgCAQQCAQIEEF1AMmmdFwWarT9HNzLlEVcwGwIBAgIBAQQTDBFjb20ub3BvZG8uZmxpZ2h0czAcAgEFAgEBBBS7T/KWEHhorhLLpqe29HlOD8/4+TAeAgEIAgEBBBYWFDIwMTktMTAtMTdUMTA6MjI6MTdaMB4CAQwCAQEEFhYUMjAxOS0xMC0xN1QxMDoyMjoxN1owHgIBEgIBAQQWFhQyMDE4LTEwLTI2VDA2OjE4OjI1WjA5AgEHAgEBBDFvUbouBoHuHSSGwAuS0waVerBKRysovBKozhdjJ0DLpiAb2430LrfGE9sgdgqHcPbEME4CAQYCAQEERthJliiZYbMeE/BxfFdiIczE3Ul1YN0KNoPD+yO5HhUXlHR8IRZwK6iq9BDT4995D2r4oGfBfYEMLh7JpkMqMQZxxzxwOSaggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBABnnINrO1Oev/GUuoGTYNG8yiyApLEP3ou3h+rwaXIckkzlK7Z6Hdz/aX/vsMmjsKdaHmGJGJJAHM7i0t3sXFCO5o/5eEjhsQICW7oMM3T2gc2SubpWyYlNQcGlZHRFVhr1TYqAaPCgVD1OpSEeMgPqc9JSnntZ7+nqej4J/Ud3nBhSDAr3nqo0Mu32C8GF4ez6GhGzXMpP+8jNCAk+PsO2a2YRJxn2Bj4Gu0ctkWLHY+Mws5I4VtNL0KlzOKqvQQ0jiyfUqVtIi0buqQ5PlbxZ5SsUZf1sdbSXXHiNt343H+GO4OSUKoeBm3AFo4qWlgIV+wGRUNIZrM68QokMUAow='
	