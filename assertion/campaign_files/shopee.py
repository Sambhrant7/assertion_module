# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from sdk import util,installtimenew,NameLists,getsleep
import time
import urllib
import random
import json
import string
import datetime
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'app_size'			 : 61.0,#52.0,#51.0,#74.0,#45.0, 44.0,#42.0,
	'package_name'		 :'com.shopee.th',
	'app_name' 			 :'Shopee',
	'app_version_name' 	 : '2.44.50',#'2.44.25',#'2.43.06',#'2.42.62',#'2.41.13',#2.40.30, '2.39.10',#'2.38.18', #2.37.30
	'app_version_code' 	 : '285',#'282',#'276',#'274',#'259',#253, '249',#'244', #242
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : '52exGkmASkSHeuRyuFmKeW',
		'dkh'		 : '52exGkmA',
		'buildnumber': '4.10.0',#'4.8.14',#'4.8.11',
		'version'	 : 'v4',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")

	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])

	global temp, counter

	temp = 0
	counter = 1

	################generating_realtime_differences##################
	app_data['clk']=str(int(app_data.get('times').get('click_time')))
	app_data['install']=str(int(app_data.get('times').get('download_begin_time')))
	def_installDate(app_data,device_data)
	def_firstLaunchDate(app_data,device_data)

	app_data['registered']=False
	app_data['gcm_1'] = str(int(app_data.get('times').get('install_complete_time')*1000))
	time.sleep(random.randint(2,4))
	app_data['gcm_2'] = str(int(time.time()*1000))
	app_data['gcm_3'] = str(int(time.time()*1000))


	app_data['csrf_token']=util.get_random_string('all',32)
	app_data['spc_f']=util.get_random_string('decimal',15)+'_'+device_data.get('android_id')+'_'+util.get_random_string('hex',8)
	app_data['spc_rnbv']=util.get_random_string('decimal',7)
	app_data['i_n_m']='55b03-'+util.get_random_string('hex',32)
	app_data['in_cart']=0

	app_data['product_view']=False
	app_data["registeredUninstall"] = False

	global item_list,price_list,shop_ids,rec_md_14,rec_md_21,rec_md_31,rec_md_20
	###########		CALLS		############
	print "\nself_call_android_clients_google\n"
	request=android_clients_google( campaign_data, device_data, app_data )
	resp = util.execute_request(**request)
	try:
		r = resp.get('data').split('token=')[1]
		app_data['gcm_token']=r
	except:
		app_data['gcm_token']="dUEiORh51aQ:APA91bGZvqqB1Od9565l5rgESlaDV05iNxmzZIzmsTXZcS2FkLtDnFS1ymWYbwoxRZrddpxdTCtk620zU0XTAUNfSSly0wZBFKfmckEvrA_K9v73M-mkxwiUAVQMiaD4CH2e2-uesNtI"
	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	appopened(campaign_data, app_data, device_data)

	time.sleep(random.randint(5,8))
	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	response = util.execute_request(**request)
	catch(response,app_data,"appsflyer")

	print "SELFCALL_______________get_feature_toggles"
	req=get_feature_toggles(campaign_data, app_data, device_data)
	resp=util.execute_request(**req)
	try:
		app_data['rec_t_id']=resp.get('res').headers.get('Set-Cookie').split('REC_T_ID=')[1].split(';')[0]
		app_data['spc_si']=resp.get('res').headers.get('Set-Cookie').split('SPC_SI=')[1].split(';')[0]
		app_data['spc_t_id']=resp.get('res').headers.get('Set-Cookie').split('SPC_T_ID="')[1].split('";')[0]
		app_data['spc_t_iv']=resp.get('res').headers.get('Set-Cookie').split('SPC_T_IV="')[1].split('";')[0]
	
	except:
		app_data['rec_t_id']='2ca6f66a-8399-11e9-8d73-3ce82481cd8d'
		app_data['spc_si']='e35r5f8fh4v96tcb3u913gpasg6emq6p'
		app_data['spc_t_id']='e4vfwNvVK/Ad9kUmhJQfXZtG40cvULhLIpTPNqbgq9H7ytB25INOqaZhrHUNqOHTcaDHv/b14TxKORSl8SPTK4euZ2MVqpeLvDJ7opaVsq0='
		app_data['spc_t_iv']='gm5WtaxBsbzQu9UTDjC0BQ=='

	print "SELFCALL_______________get_rec_md_21"
	req=get_rec_md_21(campaign_data, app_data, device_data)
	resp=util.execute_request(**req)
	try:
		rec_md_21=resp.get('res').headers.get('Set-Cookie').split('REC_MD_21=')[1].split(';')[0]
	except:
		rec_md_21=int(time.time())

	print "SELFCALL_______________get_rec_md_31"
	req=get_rec_md_31(campaign_data, app_data, device_data)
	resp=util.execute_request(**req)
	try:
		rec_md_31=resp.get('res').headers.get('Set-Cookie').split('REC_MD_31=')[1].split(';')[0]
	except:
		rec_md_31=int(time.time())

	print "SELFCALL_______________get_rec_md_14"
	req=get_rec_md_14(campaign_data, app_data, device_data)
	resp=util.execute_request(**req)
	try:
		rec_md_14=resp.get('res').headers.get('Set-Cookie').split('REC_MD_14=')[1].split(';')[0]
	except:
		rec_md_14=int(time.time())

	app_data['category_list']=[]
	print "SELFCALL_______________get_categories_ids"
	req=category_list(campaign_data, app_data, device_data)
	resp=util.execute_request(**req)
	try:
		resp_decode=json.loads(resp.get('data')).get('data').get('category_list')
		for i in range(len(resp_decode)):
			app_data['category_list'].append(resp_decode[i].get('catid'))

		if len(app_data.get('category_list'))==0:
			app_data['category_list']=[48, 47, 54, 52, 57, 58, 51, 49, 1017, 2077, 55, 50, 56, 9205, 147, 12996, 60, 2083, 14449, 1315, 2085, 61]
	except:
		app_data['category_list']=[48, 47, 54, 52, 57, 58, 51, 49, 1017, 2077, 55, 50, 56, 9205, 147, 12996, 60, 2083, 14449, 1315, 2085, 61]

	time.sleep(random.randint(10,28))
	if random.randint(1,100)<=90:
		for i in range(2):
			item_list=[]
			price_list=[]
			shop_ids=[]
			print "SELFCALL_______________get_items"
			req=get_items(campaign_data, app_data, device_data)
			resp=util.execute_request(**req)
			try:
				resp_decode=json.loads(resp.get('data')).get('data').get('items')
				for i in range(len(resp_decode)):
					item_list.append(resp_decode[i].get('itemid'))
					price_list.append(int(str(resp_decode[i].get('price')).replace('00000','')))
					shop_ids.append(resp_decode[i].get('shopid'))

				if len(item_list)==0:
					item_list=[1221113970, 1765081931, 1221113897, 1782441832, 1858937589, 2231906855L, 1137309388, 825668038, 1437308515, 1776828442, 2197958399L, 2247931967L, 2047036589, 1427852937, 1557950775, 1883046647, 2000814581, 1473228068, 1641863782, 1907046728, 2118654489, 1430851757, 1153155075, 2050502712, 2147453695, 1692103556, 733013848, 1491398365, 2050475811, 1435482730, 529885330, 1483406914, 1126775698, 1838902799, 1442310984, 1136850939, 2102725976, 1695065631, 1427846692, 1640633051]
					price_list=[99, 3900, 176, 1500, 7000, 990, 2100, 99, 312, 1050, 2390, 1100, 839, 890, 79, 3990, 80, 590, 990, 890, 119, 2850, 1890, 280, 120, 1990, 890, 2199, 280, 790, 2299, 790, 259, 2550, 203, 499, 499, 5990, 890, 432]
					shop_ids=[71039516, 95573068, 71039516, 88648910, 95573068, 88648910, 51138401, 48937496, 84544309, 63539887, 43539163, 63539887, 48937496, 79719131, 84544309, 51138401, 57904479, 78398691, 49099358, 49099358, 107151968, 30970075, 48922422, 72135979, 57904479, 48922422, 43539163, 36227855, 72135979, 85449601, 36227855, 85449601, 39233325, 30970075, 71039516, 36227485, 34776792, 95573068, 79719131, 63539887]
			except:
				item_list=[1221113970, 1765081931, 1221113897, 1782441832, 1858937589, 2231906855L, 1137309388, 825668038, 1437308515, 1776828442, 2197958399L, 2247931967L, 2047036589, 1427852937, 1557950775, 1883046647, 2000814581, 1473228068, 1641863782, 1907046728, 2118654489, 1430851757, 1153155075, 2050502712, 2147453695, 1692103556, 733013848, 1491398365, 2050475811, 1435482730, 529885330, 1483406914, 1126775698, 1838902799, 1442310984, 1136850939, 2102725976, 1695065631, 1427846692, 1640633051]
				price_list=[99, 3900, 176, 1500, 7000, 990, 2100, 99, 312, 1050, 2390, 1100, 839, 890, 79, 3990, 80, 590, 990, 890, 119, 2850, 1890, 280, 120, 1990, 890, 2199, 280, 790, 2299, 790, 259, 2550, 203, 499, 499, 5990, 890, 432]
				shop_ids=[71039516, 95573068, 71039516, 88648910, 95573068, 88648910, 51138401, 48937496, 84544309, 63539887, 43539163, 63539887, 48937496, 79719131, 84544309, 51138401, 57904479, 78398691, 49099358, 49099358, 107151968, 30970075, 48922422, 72135979, 57904479, 48922422, 43539163, 36227855, 72135979, 85449601, 36227855, 85449601, 39233325, 30970075, 71039516, 36227485, 34776792, 95573068, 79719131, 63539887]


			viewlisting(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_2'),pagetype='mall')
			time.sleep(random.randint(1,2))

		if random.randint(1,100)<=95:
			time.sleep(random.randint(10,35))
			productview(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
			app_data['product_view']=True

		time.sleep(random.randint(10,20))
		app_data['gcm_4'] = str(int(time.time()*1000))
		time.sleep(random.randint(12,20))
		
		for i in range(random.randint(3,4)):
			print '\n'+'Appsflyer : Register____________________________________'
			request = appsflyer_register(campaign_data,app_data,device_data,gcm=4)
			util.execute_request(**request)

		time.sleep(random.randint(10,15))
		print '\n'+'Appsflyer : Track____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data)
		util.execute_request(**request)
		appopened(campaign_data, app_data, device_data)

		if random.randint(1,100)<=30:
			for i in range(random.randint(2,3)):
				time.sleep(random.randint(3,6))
				print '\n'+'Appsflyer : Track____________________________________'
				request  = appsflyer_track(campaign_data, app_data, device_data)
				util.execute_request(**request)

				appopened(campaign_data, app_data, device_data)

		time.sleep(random.randint(5,8))
		app_data['user_id']='15223'+util.get_random_string('decimal',4)
		print "SELFCALL_______________spc_ec"
		req=get_spc_ec(campaign_data, app_data, device_data)
		resp=util.execute_request(**req)
		try:
			app_data['spc_ec']=resp.get('res').headers.get('Set-Cookie').split('SPC_EC=')[1].split(';')[0]
		except:
			app_data['spc_ec']='vyQ8XfBOUHkUWoQhrctWozn970Ddr3VX0AqatZpmBP662WX6S3GkNRtsoKqMsFvi5J1HKpwuN2Ay4ib5EXJ4HahgQdD2HXE2ajEyCVeX64ezRzHrppBYU1DrDN46rgYgvNraM6B6RuSe02zb/igNevGZlkbtuVcAITUJDDSUqTM='

		print "SELFCALL_______________get_rec_md_20"
		req=get_rec_md_20(campaign_data, app_data, device_data)
		resp=util.execute_request(**req)
		try:
			rec_md_20=resp.get('res').headers.get('Set-Cookie').split('REC_MD_20=')[1].split(';')[0]
		except:
			rec_md_20=int(time.time())

		if random.randint(1,100)<=70:
			time.sleep(random.randint(25,40))
			register(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
			app_data['registered']=True


			if random.randint(1,100)<=90 and app_data.get('product_view')==True:
				time.sleep(random.randint(1,2))
				print "SELFCALL_______________get_category_id"
				req=get_category_id(campaign_data, app_data, device_data)
				resp=util.execute_request(**req)
				try:
					resp_decode=json.loads(resp.get('data')).get('item').get('categories')
					app_data['category']=resp_decode[0].get('catid')
					app_data['category_id']=str(app_data.get('category'))
					for i in range(1,len(resp_decode)):
						app_data['category_id']=app_data.get('category_id')+'.'+str(resp_decode[i].get('catid'))
					
				except:
					app_data['category_id']=str(app_data.get('category'))

				time.sleep(random.randint(2,8))
				add_to_cart(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
				app_data['in_cart']+=1

		if random.randint(1,100)<=65:
			time.sleep(random.randint(10,25))
			item_list=[]
			price_list=[]
			shop_ids=[]
			print "SELFCALL_______________get_items"
			req=get_items_by_search(campaign_data, app_data, device_data,isregistered=app_data.get('registered'))
			resp=util.execute_request(**req)
			try:
				resp_decode=json.loads(resp.get('data')).get('items')
				for i in range(len(resp_decode)):
					item_list.append(resp_decode[i].get('itemid'))
					price_list.append(int(str(resp_decode[i].get('price')).replace('00000','')))
					shop_ids.append(resp_decode[i].get('shopid'))

				if len(item_list)==0:
					item_list=[1695599675, 2276437314, 2247594064, 2237602117, 1020736093, 1943400838, 2245852419, 2255124282, 2271183768, 2225832964, 2252657117, 2254685063, 2267742945, 2252422068, 2276804373, 2277523162, 1655247728, 2131779519, 2201240819, 2256317223]
					price_list=[590, 13800, 30000, 8500, 3400, 695, 29900, 2900, 15800, 800, 56500, 2400, 14900, 8500, 13900, 12000, 2990, 40, 1280, 41000]
					shop_ids=[18655219, 23726969, 692097, 82341350, 40675814, 68419637, 75626086, 16979, 23726969, 146029167, 7331470, 16979, 71019794, 59808862, 5798871, 25077604, 47906423, 10372930, 136719866, 2693525]
			except:
				print "exception"
				item_list=[1695599675, 2276437314, 2247594064, 2237602117, 1020736093, 1943400838, 2245852419, 2255124282, 2271183768, 2225832964, 2252657117, 2254685063, 2267742945, 2252422068, 2276804373, 2277523162, 1655247728, 2131779519, 2201240819, 2256317223]
				price_list=[590, 13800, 30000, 8500, 3400, 695, 29900, 2900, 15800, 800, 56500, 2400, 14900, 8500, 13900, 12000, 2990, 40, 1280, 41000]
				shop_ids=[18655219, 23726969, 692097, 82341350, 40675814, 68419637, 75626086, 16979, 23726969, 146029167, 7331470, 16979, 71019794, 59808862, 5798871, 25077604, 47906423, 10372930, 136719866, 2693525]
	
			search(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))

			time.sleep(random.randint(1,2))
			viewlisting(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_2'),pagetype='search')

			if random.randint(1,100)<=83:
				time.sleep(random.randint(10,35))
				productview(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
				app_data['product_view']=True

		if not app_data.get('registered') and random.randint(1,100)<=77:
			time.sleep(random.randint(25,40))
			register(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
			app_data['registered']=True

		if app_data.get('registered')==True:
			if random.randint(1,100)<=65 and app_data.get('product_view')==True:	
				time.sleep(random.randint(2,8))
				add_to_cart(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
				app_data['in_cart']+=1

		if random.randint(1,100)<=60 and app_data.get('product_view')==True:
			time.sleep(random.randint(3,5))
			productlike(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))

		if random.randint(1,100)<=65 and app_data.get('in_cart')>0:
			time.sleep(random.randint(30,40))
			checkout(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))

		if random.randint(1,100)<=15 and app_data.get('product_view')==True:
			time.sleep(random.randint(10,20))
			productcomment(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))

		if random.randint(1,100)<=10 and app_data.get('product_view')==True:
			time.sleep(random.randint(10,15))
			share(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))

	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")

	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])

	global temp, counter

	temp = 0
	counter = 1

	################generating_realtime_differences##################
	app_data['clk']=str(int(app_data.get('times').get('click_time')))
	app_data['install']=str(int(app_data.get('times').get('download_begin_time')))
	def_installDate(app_data,device_data)
	def_firstLaunchDate(app_data,device_data)

	app_data['product_view']=False
	if not app_data.get('registeredUninstall'):
		app_data["registeredUninstall"] = False

	if not app_data.get('registered'):
		app_data['registered']=False
	
	if not app_data.get('gcm_1'):
		app_data['gcm_1'] = str(int(app_data.get('times').get('install_complete_time')*1000))
		time.sleep(random.randint(2,4))
	
	if not app_data.get('gcm_2'):
		app_data['gcm_2'] = str(int(time.time()*1000))
	
	if not app_data.get('gcm_3'):
		app_data['gcm_3'] = str(int(time.time()*1000))

	if not app_data.get('csrf_token'):
		app_data['csrf_token']=util.get_random_string('all',32)
	if not app_data.get('spc_f'):
		app_data['spc_f']=util.get_random_string('decimal',15)+'_'+device_data.get('android_id')+'_'+util.get_random_string('hex',8)
	if not app_data.get('spc_rnbv'):
		app_data['spc_rnbv']=util.get_random_string('decimal',7)
	if not app_data.get('i_n_m'):
		app_data['i_n_m']='55b03-'+util.get_random_string('hex',32)
	if not app_data.get('in_cart'):
		app_data['in_cart']=0

	global item_list,price_list,shop_ids,rec_md_14,rec_md_21,rec_md_31,rec_md_20

	###########		CALLS		############
	if not app_data.get('gcm_token'):
		print "\nself_call_android_clients_google\n"
		request=android_clients_google( campaign_data, device_data, app_data )
		resp = util.execute_request(**request)
		try:
			r = resp.get('data').split('token=')[1]
			app_data['gcm_token']=r
		except:
			app_data['gcm_token']="dUEiORh51aQ:APA91bGZvqqB1Od9565l5rgESlaDV05iNxmzZIzmsTXZcS2FkLtDnFS1ymWYbwoxRZrddpxdTCtk620zU0XTAUNfSSly0wZBFKfmckEvrA_K9v73M-mkxwiUAVQMiaD4CH2e2-uesNtI"
	appopened(campaign_data, app_data, device_data)

	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false", isOpen=True)
	util.execute_request(**request)

	if not app_data.get('rec_t_id') and not app_data.get('spc_si') and not app_data.get('spc_t_id') and not app_data.get('spc_t_iv'):
		print "SELFCALL_______________get_feature_toggles"
		req=get_feature_toggles(campaign_data, app_data, device_data)
		resp=util.execute_request(**req)
		try:
			app_data['rec_t_id']=resp.get('res').headers.get('Set-Cookie').split('REC_T_ID=')[1].split(';')[0]
			app_data['spc_si']=resp.get('res').headers.get('Set-Cookie').split('SPC_SI=')[1].split(';')[0]
			app_data['spc_t_id']=resp.get('res').headers.get('Set-Cookie').split('SPC_T_ID="')[1].split('";')[0]
			app_data['spc_t_iv']=resp.get('res').headers.get('Set-Cookie').split('SPC_T_IV="')[1].split('";')[0]
		
		except:
			app_data['rec_t_id']='2ca6f66a-8399-11e9-8d73-3ce82481cd8d'
			app_data['spc_si']='e35r5f8fh4v96tcb3u913gpasg6emq6p'
			app_data['spc_t_id']='e4vfwNvVK/Ad9kUmhJQfXZtG40cvULhLIpTPNqbgq9H7ytB25INOqaZhrHUNqOHTcaDHv/b14TxKORSl8SPTK4euZ2MVqpeLvDJ7opaVsq0='
			app_data['spc_t_iv']='gm5WtaxBsbzQu9UTDjC0BQ=='

	print "SELFCALL_______________get_rec_md_21"
	req=get_rec_md_21(campaign_data, app_data, device_data)
	resp=util.execute_request(**req)
	try:
		rec_md_21=resp.get('res').headers.get('Set-Cookie').split('REC_MD_21=')[1].split(';')[0]
	except:
		rec_md_21=int(time.time())

	print "SELFCALL_______________get_rec_md_31"
	req=get_rec_md_31(campaign_data, app_data, device_data)
	resp=util.execute_request(**req)
	try:
		rec_md_31=resp.get('res').headers.get('Set-Cookie').split('REC_MD_31=')[1].split(';')[0]
	except:
		rec_md_31=int(time.time())

	print "SELFCALL_______________get_rec_md_14"
	req=get_rec_md_14(campaign_data, app_data, device_data)
	resp=util.execute_request(**req)
	try:
		rec_md_14=resp.get('res').headers.get('Set-Cookie').split('REC_MD_14=')[1].split(';')[0]
	except:
		rec_md_14=int(time.time())

	if not app_data.get('category_list'):
		app_data['category_list']=[]
		print "SELFCALL_______________get_categories_ids"
		req=category_list(campaign_data, app_data, device_data)
		resp=util.execute_request(**req)
		try:
			resp_decode=json.loads(resp.get('data')).get('data').get('category_list')
			for i in range(len(resp_decode)):
				app_data['category_list'].append(resp_decode[i].get('catid'))
			if len(app_data.get('category_list'))==0:
				app_data['category_list']=[48, 47, 54, 52, 57, 58, 51, 49, 1017, 2077, 55, 50, 56, 9205, 147, 12996, 60, 2083, 14449, 1315, 2085, 61]
		except:
			app_data['category_list']=[48, 47, 54, 52, 57, 58, 51, 49, 1017, 2077, 55, 50, 56, 9205, 147, 12996, 60, 2083, 14449, 1315, 2085, 61]

	time.sleep(random.randint(10,28))
	if random.randint(1,100)<=85:
		global item_list,price_list,shop_ids
		for i in range(2):
			item_list=[]
			price_list=[]
			shop_ids=[]
			print "SELFCALL_______________get_items"
			req=get_items(campaign_data, app_data, device_data)
			resp=util.execute_request(**req)
			try:
				resp_decode=json.loads(resp.get('data')).get('data').get('items')
				for i in range(len(resp_decode)):
					item_list.append(resp_decode[i].get('itemid'))
					price_list.append(int(str(resp_decode[i].get('price')).replace('00000','')))
					shop_ids.append(resp_decode[i].get('shopid'))

				if len(item_list)==0:
					item_list=[1221113970, 1765081931, 1221113897, 1782441832, 1858937589, 2231906855L, 1137309388, 825668038, 1437308515, 1776828442, 2197958399L, 2247931967L, 2047036589, 1427852937, 1557950775, 1883046647, 2000814581, 1473228068, 1641863782, 1907046728, 2118654489, 1430851757, 1153155075, 2050502712, 2147453695, 1692103556, 733013848, 1491398365, 2050475811, 1435482730, 529885330, 1483406914, 1126775698, 1838902799, 1442310984, 1136850939, 2102725976, 1695065631, 1427846692, 1640633051]
					price_list=[99, 3900, 176, 1500, 7000, 990, 2100, 99, 312, 1050, 2390, 1100, 839, 890, 79, 3990, 80, 590, 990, 890, 119, 2850, 1890, 280, 120, 1990, 890, 2199, 280, 790, 2299, 790, 259, 2550, 203, 499, 499, 5990, 890, 432]
					shop_ids=[71039516, 95573068, 71039516, 88648910, 95573068, 88648910, 51138401, 48937496, 84544309, 63539887, 43539163, 63539887, 48937496, 79719131, 84544309, 51138401, 57904479, 78398691, 49099358, 49099358, 107151968, 30970075, 48922422, 72135979, 57904479, 48922422, 43539163, 36227855, 72135979, 85449601, 36227855, 85449601, 39233325, 30970075, 71039516, 36227485, 34776792, 95573068, 79719131, 63539887]
			except:
				item_list=[1221113970, 1765081931, 1221113897, 1782441832, 1858937589, 2231906855L, 1137309388, 825668038, 1437308515, 1776828442, 2197958399L, 2247931967L, 2047036589, 1427852937, 1557950775, 1883046647, 2000814581, 1473228068, 1641863782, 1907046728, 2118654489, 1430851757, 1153155075, 2050502712, 2147453695, 1692103556, 733013848, 1491398365, 2050475811, 1435482730, 529885330, 1483406914, 1126775698, 1838902799, 1442310984, 1136850939, 2102725976, 1695065631, 1427846692, 1640633051]
				price_list=[99, 3900, 176, 1500, 7000, 990, 2100, 99, 312, 1050, 2390, 1100, 839, 890, 79, 3990, 80, 590, 990, 890, 119, 2850, 1890, 280, 120, 1990, 890, 2199, 280, 790, 2299, 790, 259, 2550, 203, 499, 499, 5990, 890, 432]
				shop_ids=[71039516, 95573068, 71039516, 88648910, 95573068, 88648910, 51138401, 48937496, 84544309, 63539887, 43539163, 63539887, 48937496, 79719131, 84544309, 51138401, 57904479, 78398691, 49099358, 49099358, 107151968, 30970075, 48922422, 72135979, 57904479, 48922422, 43539163, 36227855, 72135979, 85449601, 36227855, 85449601, 39233325, 30970075, 71039516, 36227485, 34776792, 95573068, 79719131, 63539887]

			viewlisting(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_2'),pagetype='mall')
			time.sleep(random.randint(1,2))

		if random.randint(1,100)<=95:
			time.sleep(random.randint(10,35))
			productview(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
			app_data['product_view']=True

		time.sleep(random.randint(10,20))
		app_data['gcm_4'] = str(int(time.time()*1000))
		time.sleep(random.randint(12,20))
		
		if random.randint(1,100)<=50:
			for i in range(random.randint(1,2)):
				time.sleep(random.randint(3,6))
				print '\n'+'Appsflyer : Track____________________________________'
				request  = appsflyer_track(campaign_data, app_data, device_data)
				util.execute_request(**request)

				appopened(campaign_data, app_data, device_data)

		if not app_data.get('spc_ec'):
			time.sleep(random.randint(5,8))
			app_data['user_id']='15223'+util.get_random_string('decimal',4)
			print "SELFCALL_______________spc_ec"
			req=get_spc_ec(campaign_data, app_data, device_data)
			resp=util.execute_request(**req)
			try:
				app_data['spc_ec']=resp.get('res').headers.get('Set-Cookie').split('SPC_EC=')[1].split(';')[0]
			except:
				app_data['spc_ec']='vyQ8XfBOUHkUWoQhrctWozn970Ddr3VX0AqatZpmBP662WX6S3GkNRtsoKqMsFvi5J1HKpwuN2Ay4ib5EXJ4HahgQdD2HXE2ajEyCVeX64ezRzHrppBYU1DrDN46rgYgvNraM6B6RuSe02zb/igNevGZlkbtuVcAITUJDDSUqTM='

		print "SELFCALL_______________get_rec_md_20"
		req=get_rec_md_20(campaign_data, app_data, device_data)
		resp=util.execute_request(**req)
		try:
			rec_md_20=resp.get('res').headers.get('Set-Cookie').split('REC_MD_20=')[1].split(';')[0]
		except:
			rec_md_20=int(time.time())

		if app_data.get('registered')==False:
			time.sleep(random.randint(25,40))
			register(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
			app_data['registered']=True

		if app_data.get('registered')==True:
			if random.randint(1,100)<=65 and app_data.get('product_view')==True:
				time.sleep(random.randint(1,2))
				print "SELFCALL_______________get_category_id"
				req=get_category_id(campaign_data, app_data, device_data)
				resp=util.execute_request(**req)
				try:
					resp_decode=json.loads(resp.get('data')).get('item').get('categories')
					app_data['category']=resp_decode[0].get('catid')
					app_data['category_id']=str(app_data.get('category'))
					for i in range(1,len(resp_decode)):
						app_data['category_id']=app_data.get('category_id')+'.'+str(resp_decode[i].get('catid'))
					
				except:
					app_data['category_id']=str(app_data.get('category'))

				time.sleep(random.randint(2,8))
				add_to_cart(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
				app_data['in_cart']+=1

		if random.randint(1,100)<=65:
			time.sleep(random.randint(10,25))
			item_list=[]
			price_list=[]
			shop_ids=[]
			print "SELFCALL_______________get_items"
			req=get_items_by_search(campaign_data, app_data, device_data,isregistered=app_data.get('registered'))
			resp=util.execute_request(**req)
			try:
				resp_decode=json.loads(resp.get('data')).get('items')
				for i in range(len(resp_decode)):
					item_list.append(resp_decode[i].get('itemid'))
					price_list.append(int(str(resp_decode[i].get('price')).replace('00000','')))
					shop_ids.append(resp_decode[i].get('shopid'))
				if len(item_list)==0:
					item_list=[1695599675, 2276437314, 2247594064, 2237602117, 1020736093, 1943400838, 2245852419, 2255124282, 2271183768, 2225832964, 2252657117, 2254685063, 2267742945, 2252422068, 2276804373, 2277523162, 1655247728, 2131779519, 2201240819, 2256317223]
					price_list=[590, 13800, 30000, 8500, 3400, 695, 29900, 2900, 15800, 800, 56500, 2400, 14900, 8500, 13900, 12000, 2990, 40, 1280, 41000]
					shop_ids=[18655219, 23726969, 692097, 82341350, 40675814, 68419637, 75626086, 16979, 23726969, 146029167, 7331470, 16979, 71019794, 59808862, 5798871, 25077604, 47906423, 10372930, 136719866, 2693525]

			except:
				print "exception"
				item_list=[1695599675, 2276437314, 2247594064, 2237602117, 1020736093, 1943400838, 2245852419, 2255124282, 2271183768, 2225832964, 2252657117, 2254685063, 2267742945, 2252422068, 2276804373, 2277523162, 1655247728, 2131779519, 2201240819, 2256317223]
				price_list=[590, 13800, 30000, 8500, 3400, 695, 29900, 2900, 15800, 800, 56500, 2400, 14900, 8500, 13900, 12000, 2990, 40, 1280, 41000]
				shop_ids=[18655219, 23726969, 692097, 82341350, 40675814, 68419637, 75626086, 16979, 23726969, 146029167, 7331470, 16979, 71019794, 59808862, 5798871, 25077604, 47906423, 10372930, 136719866, 2693525]
	
			search(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))

			time.sleep(random.randint(1,2))
			viewlisting(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_2'),pagetype='search')

			if random.randint(1,100)<=80:
				time.sleep(random.randint(10,35))
				productview(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
				app_data['product_view']=True

				if random.randint(1,100)<=60 and app_data.get('registered')==True:	
					time.sleep(random.randint(2,8))
					add_to_cart(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
					app_data['in_cart']+=1

		if random.randint(1,100)<=60 and app_data.get('product_view')==True:
			time.sleep(random.randint(3,5))
			productlike(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))

		if random.randint(1,100)<=60 and app_data.get('in_cart')>0:
			time.sleep(random.randint(30,40))
			checkout(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))

		if random.randint(1,100)<=12 and app_data.get('product_view')==True:
			time.sleep(random.randint(10,20))
			productcomment(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))

		if random.randint(1,100)<=8 and app_data.get('product_view')==True:
			time.sleep(random.randint(10,15))
			share(campaign_data, app_data, device_data ,gcm=app_data.get('gcm_4'))
	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def appopened(campaign_data, app_data, device_data ,gcm=''):
	print 'Appsflyer : EVENT___________________________AppOpened'
	eventName		  = 'AppOpened'
	eventValue = json.dumps({"user_id":0,"country":'TH'})
	if app_data.get('user_id'):
		eventValue = json.dumps({"user_id":app_data.get('user_id'),"country":'TH'})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue,gcm='')
	util.execute_request(**request)

def viewlisting(campaign_data, app_data, device_data ,gcm='',pagetype=''):
	content_list=[]
	if len(item_list)>=3:
		for i in range(3):
			content_list.append(item_list[i])
	else:
		for i in range(len(item_list)):
			content_list.append(item_list[i])
	print 'Appsflyer : EVENT___________________________ViewListing'
	eventName		  = 'ViewListing'
	eventValue		  = json.dumps({"af_content_type":"product","pagetype":pagetype,"af_content_list":content_list,"country":'TH'})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue,gcm=gcm)
	util.execute_request(**request)

def register(campaign_data, app_data, device_data ,gcm=''):
	
	print 'Appsflyer : EVENT___________________________Register'
	eventName		  = 'Register'
	eventValue		  = json.dumps({"af_registration_method":"line","country":'TH'})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue,gcm=gcm)
	util.execute_request(**request)

def productview(campaign_data, app_data, device_data ,gcm=''):
	choice=random.randint(0,len(item_list)-1)
	app_data['product']=item_list[choice]
	app_data['price']=price_list[choice]
	app_data['shop_id']=shop_ids[choice]
	print 'Appsflyer : EVENT___________________________ProductView'
	eventName		  = 'ProductView'
	eventValue		  = json.dumps({"af_price":app_data.get('price'),"af_content_type":"product","af_content_id":app_data.get('product'),"af_currency":"THB","country":'TH'})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue,gcm=gcm)
	util.execute_request(**request)

def productlike(campaign_data, app_data, device_data ,gcm=''):
	print 'Appsflyer : EVENT___________________________ProductLike'
	eventName		  = 'ProductLike'
	eventValue		  = json.dumps({"product_id":app_data.get('product'),"shop_id":app_data.get('shop_id'),"country":'TH'})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue,gcm=gcm)
	util.execute_request(**request)

def add_to_cart(campaign_data, app_data, device_data ,gcm=''):
	content=json.dumps([{"id":str(app_data.get('product')),"quantity":1,"item_price":app_data.get('price')}])
	print 'Appsflyer : EVENT___________________________add_to_cart'
	eventName		  = 'AddToCart'
	eventValue		  = json.dumps({"af_price":str(app_data.get('price')),"af_content_id":str(app_data.get('product')),"af_quantity":"1","shop_id":str(app_data.get('shop_id')),"af_content":str(content),"af_content_type":"product","af_currency":"THB","categoryid":app_data.get('category_id')})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue,gcm=gcm)
	util.execute_request(**request)

def search(campaign_data, app_data, device_data ,gcm=''):
	search_item=random.choice(['iphone', 'parrot', 'polo tshirt', 'shoes', 'tie', 'ที่นอนขนห่าน', 'บรา 3d', 'เคสฝาพับ', 'canon eos', 'tattoo machine'])
	print 'Appsflyer : EVENT___________________________search'
	eventName		  = 'Search'
	eventValue		  = json.dumps({"af_search_string":search_item,"country":"TH"},ensure_ascii=False)
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue,gcm=gcm)
	util.execute_request(**request)

def checkout(campaign_data, app_data, device_data ,gcm=''):
	print 'Appsflyer : EVENT___________________________checkout'
	eventName		  = 'Checkout'
	eventValue		  = json.dumps({"TotalPrice":str(app_data.get('price')),"SellerID":str(app_data.get('shop_id')),"OrderPrice":"0"})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue,gcm=gcm)
	util.execute_request(**request)

def productcomment(campaign_data, app_data, device_data ,gcm=''):
	print 'Appsflyer : EVENT___________________________ProductComment'
	eventName		  = 'ProductComment'
	eventValue		  = json.dumps({"product_id":app_data.get('product'),"shop_id":app_data.get('shop_id'),"country":'TH'})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue,gcm=gcm)
	util.execute_request(**request)

def share(campaign_data, app_data, device_data ,gcm=''):
	print 'Appsflyer : EVENT___________________________Share'
	eventName		  = 'Share'
	eventValue		  = json.dumps({"platform":"sms","shared_content":"product","country":"TH","shared_content_id":app_data.get('product'),"user_id":app_data.get('user_id')})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue,gcm=gcm)
	util.execute_request(**request)

###################################################################
#
#			SELFCALLS
#
###################################################################

def get_feature_toggles(campaign_data, app_data, device_data):

	method = "get"
	url = 'http://mall.shopee.co.th/api/v2/get_feature_toggles'
	UA=campaign_data.get('app_name')+' Android Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113'
	headers = {
		"Accept-Encoding" : "gzip",
		'Cookie' : 'userid=0; shopee_token=; username=; UA='+urllib.quote(UA)+'; SPC_AFTID=; shopee_app_version=24113; shopee_rn_version=0'
		}
	params = {
		"userid" : '0'
		}
	data = None
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def get_rec_md_21(campaign_data, app_data, device_data):

	method = "get"
	url = 'http://mall.shopee.co.th/api/v2/recommendation/top_products/meta_v3'
	UA=campaign_data.get('app_name')+' Android Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113'
	headers = {
		"Accept-Encoding" : "gzip",
		'user-agent' : 'Android app '+campaign_data.get('app_name')+' appver=24113',
		'if-none-match-' : str(app_data.get('i_n_m')),
		'referer' : 'https://mall.shopee.co.th',
		'x-api-source' : 'rn',
		'Cookie' : 'userid=0; shopee_token=; username=; UA='+urllib.quote(UA)+'; SPC_AFTID=; shopee_app_version=24113; shopee_rn_version=1557926999; SPC_F='+str(app_data.get('spc_f'))+'; language=th; SPC_RNBV='+str(app_data.get('spc_rnbv'))+'; SPC_IA=-1; SPC_EC=-; SPC_U=-; REC_T_ID='+str(app_data.get('rec_t_id'))+'; SPC_T_ID='+str(app_data.get('spc_t_id'))+'; SPC_SI='+str(app_data.get('spc_si'))+'; SPC_T_IV='+str(app_data.get('spc_t_iv'))+'; csrftoken='+str(app_data.get('csrf_token'))
		}
	params = {
		"cat_limit" :	'10',
		"item_limit":	'10'
		}
	data = None

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def get_rec_md_31(campaign_data, app_data, device_data):

	method = "get"
	url = 'http://mall.shopee.co.th/api/v2/recommendation/daily_discover_category'
	UA=campaign_data.get('app_name')+' Android Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113'
	headers = {
		"Accept-Encoding" : "gzip",
		'user-agent' : 'Android app '+campaign_data.get('app_name')+' appver=24113',
		'if-none-match-' : str(app_data.get('i_n_m')),
		'referer' : 'https://mall.shopee.co.th',
		'x-api-source' : 'rn',
		'Cookie' : 'userid=0; shopee_token=; username=; UA='+urllib.quote(UA)+'; SPC_AFTID=; shopee_app_version=24113; shopee_rn_version=1557926999; SPC_F='+str(app_data.get('spc_f'))+'; language=th; SPC_RNBV='+str(app_data.get('spc_rnbv'))+'; SPC_IA=-1; SPC_EC=-; SPC_U=-; REC_T_ID='+str(app_data.get('rec_t_id'))+'; SPC_T_ID='+str(app_data.get('spc_t_id'))+'; SPC_SI='+str(app_data.get('spc_si'))+'; SPC_T_IV='+str(app_data.get('spc_t_iv'))+'; csrftoken='+str(app_data.get('csrf_token'))
		}
	params = None
	data = None

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def get_rec_md_14(campaign_data, app_data, device_data):

	method = "get"
	url = 'http://mall.shopee.co.th/api/v2/recommendation/trending_searches_v2'
	UA=campaign_data.get('app_name')+' Android Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113'
	headers = {
		"Accept-Encoding" : "gzip",
		'user-agent' : 'Android app '+campaign_data.get('app_name')+' appver=24113',
		'if-none-match-' : str(app_data.get('i_n_m')),
		'referer' : 'https://mall.shopee.co.th',
		'x-api-source' : 'rn',
		'Cookie' : 'userid=0; shopee_token=; username=; UA='+urllib.quote(UA)+'; SPC_AFTID=; shopee_app_version=24113; shopee_rn_version=1557926999; SPC_F='+str(app_data.get('spc_f'))+'; language=th; SPC_RNBV='+str(app_data.get('spc_rnbv'))+'; SPC_IA=-1; SPC_EC=-; SPC_U=-; REC_T_ID='+str(app_data.get('rec_t_id'))+'; SPC_T_ID='+str(app_data.get('spc_t_id'))+'; SPC_SI='+str(app_data.get('spc_si'))+'; SPC_T_IV='+str(app_data.get('spc_t_iv'))+'; csrftoken='+str(app_data.get('csrf_token'))+'; REC_MD_31='+str(rec_md_31)+'; REC_MD_21='+str(rec_md_21)
		}
	params = {
		'limit'  : '20',
		'offset' : '0'
}
	data = None

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def get_spc_ec(campaign_data, app_data, device_data):
	register_user(app_data,country='united states')
	method = "get"
	url = 'http://mall.shopee.co.th/api/v2/get_feature_toggles'
	UA=campaign_data.get('app_name')+' Android Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113'
	headers = {
		"Accept-Encoding" : "gzip",
		'Cookie' : 'SPC_AFTID=; shopee_app_version=24113; shopee_rn_version=1557926999; SPC_F='+str(app_data.get('spc_f'))+'; language=th; SPC_RNBV='+str(app_data.get('spc_rnbv'))+'; SPC_IA=-1; SPC_EC=-; SPC_U=-; REC_T_ID='+str(app_data.get('rec_t_id'))+'; SPC_T_ID='+str(app_data.get('spc_t_id'))+'; SPC_SI='+str(app_data.get('spc_si'))+'; SPC_T_IV='+str(app_data.get('spc_t_iv'))+'; csrftoken='+str(app_data.get('csrf_token'))+'; _ga=GA1.3.1380561699.1559286644; _gid=GA1.3.2064210064.1559286644; userid='+app_data.get('user_id')+'; shopee_token=BuCE37EhS7EG1fbbaYaZ9yid5NQWNgfj6BSID1i82FmSnJGtjaKnisotApvRPLm8; username='+app_data.get('user').get('firstname')+'; UA='+urllib.quote(device_data.get('User-Agent')+' Shopee Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113 rnver=1559197828')

		}
		
	params = {
		'userid' : app_data.get('user_id')
}
	data = None

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def get_rec_md_20(campaign_data, app_data, device_data):

	method = "get"
	url = 'http://mall.shopee.co.th/api/v2/recommendation/hot_search_words'
	UA=campaign_data.get('app_name')+' Android Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113'
	headers = {
		"Accept-Encoding" : "gzip",
		'user-agent' : 'Android app '+campaign_data.get('app_name')+' appver=24113',
		'if-none-match-' : str(app_data.get('i_n_m')),
		'referer' : 'https://mall.shopee.co.th',
		'x-api-source' : 'rn',
		'Cookie' : 'SPC_AFTID=; shopee_app_version=24113; SPC_F='+str(app_data.get('spc_f'))+'; language=th; SPC_IA=-1; REC_T_ID='+str(app_data.get('rec_t_id'))+'; SPC_SI='+str(app_data.get('spc_si'))+'; csrftoken='+str(app_data.get('csrf_token'))+'; _ga=GA1.3.1380561699.1559286644; _gid=GA1.3.2064210064.1559286644; userid='+app_data.get('user_id')+'; shopee_token=BuCE37EhS7EG1fbbaYaZ9yid5NQWNgfj6BSID1i82FmSnJGtjaKnisotApvRPLm8; username='+app_data.get('user').get('firstname')+'; SPC_EC='+str(app_data.get('spc_ec'))+'; SPC_T_ID='+str(app_data.get('spc_t_id'))+'; SPC_U='+app_data.get('user_id')+'; SPC_T_IV='+str(app_data.get('spc_t_iv'))+'; UA='+urllib.quote(UA)+'; shopee_rn_version=1559197828; SPC_RNBV='+str(app_data.get('spc_rnbv'))+'; REC_MD_31='+str(rec_md_31)+'; REC_MD_21='+str(rec_md_21)+'; REC_MD_14='+str(rec_md_14)
		}
	params = None
	data = None

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def category_list(campaign_data, app_data, device_data):

	method = "get"
	url = 'http://mall.shopee.co.th/api/v2/category_list/get'
	UA=campaign_data.get('app_name')+' Android Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113'
	headers = {
		"Accept-Encoding" : "gzip",
		'user-agent' : 'Android app '+campaign_data.get('app_name')+' appver=24113',
		'if-none-match-' : str(app_data.get('i_n_m')),
		'referer' : 'https://mall.shopee.co.th',
		'x-api-source' : 'rn',
		'Cookie' : 'userid=0; shopee_token=; username=; UA='+urllib.quote(UA)+'; SPC_AFTID=; shopee_app_version=24113; shopee_rn_version=1557926999; SPC_F='+str(app_data.get('spc_f'))+'; language=th; SPC_RNBV='+str(app_data.get('spc_rnbv'))+'; SPC_IA=-1; SPC_EC=-; SPC_U=-; REC_T_ID='+str(app_data.get('rec_t_id'))+'; SPC_T_ID='+str(app_data.get('spc_t_id'))+'; SPC_SI='+str(app_data.get('spc_si'))+'; SPC_T_IV='+str(app_data.get('spc_t_iv'))+'; csrftoken='+str(app_data.get('csrf_token'))+'; REC_MD_31='+str(rec_md_31)+'; REC_MD_21='+str(rec_md_21)+'; REC_MD_14='+str(rec_md_14)
		}
	params = None
	data = None
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def get_category_id(campaign_data, app_data, device_data):

	method = "get"
	url = 'http://mall.shopee.co.th/api/v2/item/get'
	UA=campaign_data.get('app_name')+' Android Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113'
	headers = {
		"Accept-Encoding" : "gzip",
		'user-agent' : 'Android app '+campaign_data.get('app_name')+' appver=24113',
		'if-none-match-' : str(app_data.get('i_n_m')),
		'referer' : 'https://mall.shopee.co.th',
		'x-api-source' : 'rn',
		'Cookie' : 'SPC_AFTID=; shopee_app_version=24113; SPC_F='+str(app_data.get('spc_f'))+'; language=th; SPC_IA=-1; REC_T_ID='+str(app_data.get('rec_t_id'))+'; SPC_SI='+str(app_data.get('spc_si'))+'; csrftoken='+str(app_data.get('csrf_token'))+'; _ga=GA1.3.1380561699.1559286644; _gid=GA1.3.2064210064.1559286644; userid='+app_data.get('user_id')+'; shopee_token=BuCE37EhS7EG1fbbaYaZ9yid5NQWNgfj6BSID1i82FmSnJGtjaKnisotApvRPLm8; username='+app_data.get('user').get('firstname')+'; SPC_EC='+str(app_data.get('spc_ec'))+'; SPC_T_ID='+str(app_data.get('spc_t_id'))+'; SPC_U='+app_data.get('user_id')+'; SPC_T_IV='+str(app_data.get('spc_t_iv'))+'; UA='+urllib.quote(UA)+'; shopee_rn_version=1559197828; SPC_RNBV='+str(app_data.get('spc_rnbv'))+'; REC_MD_31='+str(rec_md_31)+'; REC_MD_21='+str(rec_md_21)+'; REC_MD_14='+str(rec_md_14)+'; REC_MD_20='+str(rec_md_20)
		}

	params = {
			'itemid' : app_data.get('product'),
			'shopid' : app_data.get('shop_id')
	}
	data = None

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def get_items(campaign_data, app_data, device_data):
	app_data['category']=random.choice(app_data.get('category_list'))
	method = "get"
	url = 'http://mall.shopee.co.th/api/v2/recommend_items/get'
	UA=campaign_data.get('app_name')+' Android Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113'
	headers = {
		"Accept-Encoding" : "gzip",
		'user-agent' : 'Android app '+campaign_data.get('app_name')+' appver=24113',
		'if-none-match-' : str(app_data.get('i_n_m')),
		'referer' : 'https://mall.shopee.co.th',
		'x-api-source' : 'rn',
		'Cookie' : 'userid=0; shopee_token=; username=; UA='+urllib.quote(UA)+'; SPC_AFTID=; shopee_app_version=24113; shopee_rn_version=1557926999; SPC_F='+str(app_data.get('spc_f'))+'; language=th; SPC_RNBV='+str(app_data.get('spc_rnbv'))+'; SPC_IA=-1; SPC_EC=-; SPC_U=-; REC_T_ID='+str(app_data.get('rec_t_id'))+'; SPC_T_ID='+str(app_data.get('spc_t_id'))+'; SPC_SI='+str(app_data.get('spc_si'))+'; SPC_T_IV='+str(app_data.get('spc_t_iv'))+'; csrftoken='+str(app_data.get('csrf_token'))+'; REC_MD_31='+str(rec_md_31)+'; REC_MD_21='+str(rec_md_21)+'; REC_MD_14='+str(rec_md_14)
		}
	params = {
		'catid'	: app_data.get('category'),
		'limit'	: 40,
		'offset' : 0,
		'recommend_type' : 6
		}
	data = None
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def get_items_by_search(campaign_data, app_data, device_data,isregistered=''):

	method = "get"
	url = 'http://mall.shopee.co.th/api/v2/search_items/'
	headers = {
		"Accept-Encoding" : "gzip",
		'user-agent' : 'Android app '+campaign_data.get('app_name')+' appver=24113',
		'if-none-match-' : str(app_data.get('i_n_m')),
		'referer' : 'https://mall.shopee.co.th',
		'x-api-source' : 'rn',
		'Cookie' : 'SPC_AFTID=; shopee_app_version=24113; SPC_F='+str(app_data.get('spc_f'))+'; language=th; SPC_IA=-1; REC_T_ID='+str(app_data.get('rec_t_id'))+'; SPC_SI='+str(app_data.get('spc_si'))+'; csrftoken='+str(app_data.get('csrf_token'))+'; _ga=GA1.3.1380561699.1559286644; _gid=GA1.3.2064210064.1559286644; userid='+app_data.get('user_id')+'; username='+app_data.get('user').get('firstname')+'; SPC_T_ID='+str(app_data.get('spc_t_id'))+'; SPC_U='+app_data.get('user_id')+'; SPC_T_IV='+str(app_data.get('spc_t_iv'))+'; shopee_rn_version=1559197828; SPC_RNBV='+str(app_data.get('spc_rnbv'))+'; shopee_token=BuCE37EhS7EG1fbbaYaZ9+X2AY2Z8ql7dsHwKf6HhE2J4aHbYY0UpT8TFDBq3Y5Q; shopee_token=BuCE37EhS7EG1fbbaYaZ9+X2AY2Z8ql7dsHwKf6HhE2J4aHbYY0UpT8TFDBq3Y5Q; REC_MD_21='+str(rec_md_21)+'; REC_MD_14='+str(rec_md_14)+'; SPC_EC='+str(app_data.get('spc_ec'))+'; UA='+urllib.quote(device_data.get('User-Agent')+' Shopee Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113 rnver=1559197828')+'; REC_MD_20='+str(rec_md_20)
		}

	params = {
		'by' :'relevancy',
		'keyword' : 'tag heuer',
		'limit' : 20,
		'newest' : 0,
		'order' : 'desc',
		'page_type' : 'search'
		}

	if isregistered==False:
		headers['Cookie'] = 'userid=0; shopee_token=; username=; SPC_AFTID=; shopee_app_version=24113; shopee_rn_version=1557926999; SPC_F='+str(app_data.get('spc_f'))+'; language=th; SPC_RNBV='+str(app_data.get('spc_rnbv'))+'; SPC_IA=-1; SPC_EC=-; SPC_U=-; REC_T_ID='+str(app_data.get('rec_t_id'))+'; SPC_T_ID='+str(app_data.get('spc_t_id'))+'; SPC_SI='+str(app_data.get('spc_si'))+'; SPC_T_IV='+str(app_data.get('spc_t_iv'))+'; csrftoken='+str(app_data.get('csrf_token'))+'; UA='+urllib.quote(device_data.get('User-Agent')+' Shopee Beeshop locale/th version='+campaign_data.get('app_version_code')+' appver=24113 rnver=1559197828')+'; _ga=GA1.3.1380561699.1559286644; _gid=GA1.3.2064210064.1559286644'

	data = None
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_installDate(app_data,device_data)
	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	def_sec(app_data,device_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=-1

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		"Content-Type" : "application/json",

		}
	params = {
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),
		"app_id" : campaign_data.get('package_name'),

		}
	data = {
		"af_gcm_token" : app_data.get('gcm_token'),
		"registeredUninstall" : app_data.get('registeredUninstall'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"product" : device_data.get('product'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"advertiserId" : device_data.get('adid'),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_reactNative",
		"date1" : datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time')+int(app_data.get('sec')))).strftime("%Y-%m-%d_%H%M%S")+str(device_data.get('timezone')),
		"date2" : datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time')+int(app_data.get('sec')))).strftime("%Y-%m-%d_%H%M%S")+str(device_data.get('timezone')),
		"network" : device_data.get('network').upper(),
		"installDate" : app_data.get('installDate'),
		"af_events_api" : "1",
		# "af_sdks" : "0000000000",
		"deviceType" : "user",
		"app_version_code" : campaign_data.get('app_version_code'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"af_preinstalled" : "false",
		"isGaidWithGps" : "true",
		"rfr" : {
								"code" : "0",
								"install" : app_data.get("install"),
								"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",
								"clk" : app_data.get("clk"),
		},
		"app_version_name" : campaign_data.get('app_version_name'),
		"appUserId" : "0",
		"advertiserIdEnabled" : "true",
		"device" : device_data.get('device'),
		"lang_code" : device_data.get('locale').get('language'),
		"uid" : app_data.get('uid'),
		"sdk" : device_data.get('sdk'),
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"installer_package" : "com.android.vending",
		"deviceData" : {
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"build_display_id" : device_data.get('build'),
						"btch" : "no",
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"btl" : get_batteryLevel(app_data),
						"arch" : "",
		},
		# "deviceRm": device_data.get('build'),
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"counter" : str(app_data.get('counter')),
		"carrier" : device_data.get('carrier'),
		"isFirstCall" : isFirstCall,
		"country" : device_data.get('locale').get('country'),
		"model" : device_data.get('model'),
		"af_timestamp" : timestamp(),
		"iaecounter" : str(app_data.get('iaecounter')),
		'tokenRefreshConfigured':False,
		'p_receipt' : util.get_random_string(type='all',size=640)+"==",
		'sc_o':'p',
		"open_referrer" : 'android-app://com.android.vending',
		'ivc' : False,
		}		

	data["deviceData"]["sensors"] = [{"sT":2,"sV":"MEMSIC, Inc","sVE":[18.699139,39.766182,-71.57425],"sVS":[-68.9134,-38.91178,-36.665936],"sN":"mmc3416x-mag"},{"sT":1,"sV":"STMicroelectronics","sVE":[-4.6160207,6.7708025,2.1930888],"sVS":[4.644751,8.37013,5.401319],"sN":"lis3dh-accel"},{"sT":4,"sV":"oem","sVE":[-0.4381815,1.2354547,-2.8607621],"sVS":[-3.156011,-0.7919304,-0.80119497],"sN":"oem-pseudo-gyro"}]
			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")	
		data['af_gcm_token']=app_data.get('gcm_4')
	else:
		data['batteryLevel']=get_batteryLevel(app_data)

	if app_data.get('counter')>2:
		del data['rfr']
		del data["deviceData"]["sensors"]	
		del data['p_receipt']

	if  device_data.get('locale').get('language')=='th':	
		data['lang'] = "ไทย"

	data['cksm_v1']	= cksm_v1(data['date1'],data['af_timestamp'])	

	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey=get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))

	data["kef"+kefKey] = kefVal

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue="",gcm=''):
	def_installDate(app_data,device_data)
	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	def_sec(app_data,device_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		"Content-Type" : "application/json",

		}
	params = {
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),
		"app_id" : campaign_data.get('package_name'),

		}
	data = {
		"registeredUninstall" : app_data.get('registeredUninstall'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"product" : device_data.get('product'),
		"installer_package" : "com.android.vending",
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_reactNative",
		"date1" : datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time')+int(app_data.get('sec')))).strftime("%Y-%m-%d_%H%M%S")+str(device_data.get('timezone')),
		"date2" : datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time')+int(app_data.get('sec')))).strftime("%Y-%m-%d_%H%M%S")+str(device_data.get('timezone')),
		"network" : device_data.get('network').upper(),
		"installDate" : app_data.get('installDate'),
		"af_events_api" : "1",
		"deviceType" : "user",
		"app_version_code" : campaign_data.get('app_version_code'),
		"brand" : device_data.get('brand'),
		"af_preinstalled" : "false",
		"isGaidWithGps" : "true",
		"app_version_name" : campaign_data.get('app_version_name'),
		"appUserId" : "0",
		"advertiserIdEnabled" : "true",
		"device" : device_data.get('device'),
		"lang_code" : device_data.get('locale').get('language'),
		"uid" : app_data.get('uid'),
		"sdk" : device_data.get('sdk'),
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"advertiserId" : device_data.get('adid'),
		"deviceData" : {
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"arch" : "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"build_display_id" : device_data.get('build'),
		},
		# "deviceRm": device_data.get('build'),
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"counter" : str(app_data.get('counter')),
		"carrier" : device_data.get('carrier'),
		"isFirstCall" : "true",
		"country" : device_data.get('locale').get('country'),
		"model" : device_data.get('model'),
		"af_timestamp" : timestamp(),
		"iaecounter" : str(app_data.get('iaecounter')),
		'tokenRefreshConfigured':False,
		'sc_o':'p',
		'ivc' : False,
		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'

		data['af_gcm_token']=gcm
			
	update_eventsRecords(app_data,eventName,eventValue)

	if  device_data.get('locale').get('language')=='th':	
		data['lang'] = "ไทย"

	data['cksm_v1']	= cksm_v1(data['date1'],data['af_timestamp'])	

	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey=get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))

	data["kef"+kefKey] = kefVal

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"device_id" : app_data.get('uid'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data,gcm=0):
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	def_installDate(app_data,device_data)
	get_google_gcmToken(app_data)
 	def_(app_data,'counter')
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		"Content-Type" : "application/json",

		}

	app_data["registeredUninstall"] = True
		
	params = {
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),
		"app_id" : campaign_data.get('package_name'),

		}
	data = {
		"app_name" : campaign_data.get('app_name'),
		"installDate" : app_data.get('installDate'),
		"uid" : app_data.get('uid'),
		"brand" : device_data.get('brand').upper(),
		"advertiserId" : device_data.get('adid'),
		"network" : device_data.get('network').upper(),
		"carrier" : device_data.get('carrier'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"launch_counter" : str(app_data.get('counter')),
		"operator" : device_data.get('carrier'),
		"model" : device_data.get('model'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"sdk" : device_data.get('sdk'),
		"af_gcm_token" : app_data.get('gcm_token'),
		"appUserId":'0',
		}


	if gcm==1:
		data['af_gcm_token'] = app_data.get('gcm_1')+','+data.get('af_gcm_token')	
	elif gcm==2:
		data['af_gcm_token'] = app_data.get('gcm_2')+','+app_data.get('gcm_1')+','+data.get('af_gcm_token')	
	elif gcm==3:
		data['af_gcm_token'] = app_data.get('gcm_3')+','+app_data.get('gcm_2')+','+app_data.get('gcm_1')+','+data.get('af_gcm_token')
	elif gcm==4:
		data['af_gcm_token'] = app_data.get('gcm_4')+','+app_data.get('gcm_3')+','+app_data.get('gcm_2')+','+app_data.get('gcm_1')+','+data.get('af_gcm_token')					

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

############################################################
#
#		GCM TOKEN
#
############################################################

def android_clients_google( campaign_data, device_data, app_data ):
	url= "https://android.clients.google.com/c2dm/register3"
	method= "post"
	headers= {      
			'Accept-Encoding': 'gzip',
	        'Authorization': 'AidLogin 4541937144496662054:3862026055348089232',
	        'User-Agent': 'Android-GCM/1.5 ('+device_data.get('product')+' '+device_data.get('build')+')',
	        'app': campaign_data.get('package_name'),
	        'content-type': 'application/x-www-form-urlencoded',
	        'gcm_ver': '16091011'
        }

	params= None

	data= {       
			'X-app_ver': campaign_data.get('app_version_code'),
	        'X-app_ver_name': campaign_data.get('app_version_name'),
	        'X-appid': util.get_random_string(type='google_token',size=11),
	        'X-cliv': 'fiid-'+util.get_random_string('decimal',8),
	        'X-gmp_app_id': '1:808332928752'+':android:'+util.get_random_string('hex',16),
	        'X-gmsv': '16091011',
	        'X-osv': device_data.get('os_version'),
	        'X-scope': '*',
	        'X-subtype': '808332928752',
	        'app': campaign_data.get('package_name'),
	        'app_ver': campaign_data.get('app_version_code'),
	        'cert': util.get_random_string('hex',40),
	        'device': '4541937144496662054',
	        'gcm_ver': '16091011',
	        'plat': '0',
	        'sender': '808332928752',
	        'info'	: '0-X0Efm3qvUaUEfGgBpd_701UQXLghY',
	        'target_ver' :	device_data.get('sdk'),
        }

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

#####################################################################################################

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
# def def_deviceFingerPrintId(app_data):
# 	if not app_data.get('deviceFingerPrintId'):
# 		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

# def get_afGoogleInstanceID():
# 	return util.get_random_string('char_all',11)

###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	
def def_installDate(app_data,device_data):
	if not app_data.get('installDate'):
		date = int(app_data.get('times').get('install_complete_time'))
		app_data['installDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_firstLaunchDate(app_data,device_data):
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

# def ref_time():
# 	time.sleep(random.randint(10,15))
# 	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

###########################################################
#
#		CHECKSUM
#
###########################################################
def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()

def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
	if buildnumber=="4.8.18":
		return getKeyHalf_4_8_18(sdk, lang, af_ts)
	else:
	    c = '\u0000'
	    obj2 = af_ts[::-1]
	    out = calculate(sdk, obj2, brand)
	    i = len(out)
	    if i > 4:
	        i2 = globals()['counter']+65
	        globals()['temp'] = i2 % 128
	        if (1 if i2 % 2 != 0 else None) != 1:
	            out.delete(4, i)
	        else:
	            out.delete(5, i)
	    else:
	        while i < 4:
	            i3 = globals()['counter'] + 119
	            globals()['temp'] = i3 % 128
	            if (16 if i3 % 2 != 0 else 93) != 16:
	                i += 1
	                out+='1'
	            else:
	                i += 66
	                out+='H'
	            i3 = globals()['counter'] + 109
	            globals()['temp'] = i3 % 128
	            i3 %= 2
	    return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
    ts = ts[::-1]
    appends = [sdk,lang,ts]
    lengths = [len(sdk),len(lang),len(ts)]
    l = sorted(lengths)
    least = l[0]
    out = ""
    for x in range(least):
        numb = None
        for y in range(len(appends)):
            charAt = ord(appends[y][x])
            if numb:
                charAt = int((charAt ^ int(numb)))

            numb = charAt
            
        out+=str(hex(numb))[2:]

    if len(out)>4:
    	out = out[:4]
    elif len(out)<4:
    	while len(out)<4:
    		out+="1"
    
    return out

"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""

def generateValue(b,x,s,p,ts,fl,buildnumber):
    part1=generateValue1get(ts,fl,buildnumber)
    str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p,'utf-8')
    for i in range(len(str_)):
        str_[i] = int((str_[i] ^ ((i % 2) + 42)))

    stringBuilder = ""
    for toHexString in str_:
        toHexString2 = str(hex(int(toHexString)))[2:]
        if 1 == len(toHexString2):
            toHexString2 = "0"+str(toHexString2)
        stringBuilder+=toHexString2
    part2 = stringBuilder.__str__()
    return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
    gethash = sha256(ts+firstLaunch+buildnumber)
    return gethash[:16]

"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""

def calculate(sdk, obj, brand):
    allList = [sdk, obj, brand]
    arrayList = []
    i = 0
    while True:
        flag = 1
        if i >= 3:
            break
        i2 = globals()['temp'] + 63
        globals()['counter'] = i2 % 128
        if i2 % 2 != 0:
            flag = None
        if flag != None:
            arrayList.append(len(allList[i]))
            i += 31
        else:
            arrayList.append(len(allList[i]))
            i += 1
    sorted(arrayList)
    intValue = int(arrayList[0])
    out = ""
    i3 = 0
    while i3 < intValue:
        i4 = globals()['counter']+99
        globals()['temp'] = i4 % 128
        i5 =  globals()['temp']+67
        globals()['counter'] = i5 % 128
        i5 %= 2
        number = None
        if i4 % 2 != 0:
            a = 57
        else:
            a = 83
        if a != 83:
            i4 = 1
        else:
            i4 = 0
        while i4 < 3:
            i5 = globals()['temp'] + 87
            globals()['counter'] = i5 % 128
            i5 %= 2
            i5 = ord(allList[i4][i3])
            if (92 if number == None else 39) != 39:
                i6 = globals()['temp'] + 55
                globals()['counter'] = i6 % 128
                i6 %= 2
                i6 = globals()['counter'] + 39
                globals()['temp'] = i6 % 128
                i6 %= 2
            else:
                i5 ^= int(number)
            number = i5
            i4 += 1
        out+=str(hex(int(number)))[2:]
        i3 += 1
    return out

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100
		
	sb = insert_char(sb,23,str(n3))
	return sb
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		# app_data['user']['sex'] 		= gender
		# app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		# app_data['user']['lastname'] 	= last_name
		# app_data['user']['username'] 	= username
		# app_data['user']['email'] 		= username+"@gmail.com"