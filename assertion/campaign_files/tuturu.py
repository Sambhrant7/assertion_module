from sdk import util,installtimenew,getsleep
import time
import random
import datetime
import urllib
import uuid
import string
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'ru.tutu.tutu_emp',
	'app_name' 			 :'Tutu.ru',
	'app_version_name'	 :'3.9.2',#'3.9.0',#'3.0.2',#'3.0.0', #2.3.2
	'no_referrer'		 : True,
	'ctr'				 : 6,
	'device_targeting'	 : True,
	'supported_os'		 :'4.4',		
	'supported_countries':'WW',
	'app_size'			 :26.0,#70.0,
	'tracker'			 :'adjust',
	'adjust':{
		'app_token'	: 'g53936tne2o0',
		'sdk'		: 'android4.14.0',	
		'secret_key': '131057467970036960056520268870699566',
		'secret_id'	: '1',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print 'please wait installing___________'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	app_data['installed_at']=get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time'))

	global created_at
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(1,3))

	global sent_at
	sent_at=get_date(app_data,device_data)
	time.sleep(random.randint(2,4))
	
	global attribution_sent
	attribution_sent=get_date(app_data,device_data)
	
	global adjust_call_time
	adjust_call_time=int(time.time())
	def_(app_data,'session_count')
	def_(app_data,'subsession_count')
	

	###########		CALLS		################	
	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	print '\nAdjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,source='reftag')
	util.execute_request(**request)

	time.sleep(random.randint(1,2))
	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)

	call_pattern(campaign_data,app_data,device_data,activity='install')

	###########		FINALIZE	################
	
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
		app_data['installed_at']=get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time'))

	global created_at
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(1,2))

	global sent_at
	sent_at=get_date(app_data,device_data)
	
	global adjust_call_time
	adjust_call_time=int(time.time())
	def_(app_data,'session_count')
	def_(app_data,'subsession_count')
	
	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,True)
	util.execute_request(**request)
	time.sleep(random.randint(6,10))

	call_pattern(campaign_data,app_data,device_data,activity='open')
	
	return {'status':'true'}

def call_pattern(campaign_data,app_data,device_data,activity='install'):
	if activity=='install':
		isOpen=False
		
	elif activity=='open':
		isOpen=True

	if random.randint(1,100)<=90:
		for i in range (0,random.randint(3,5)):
			choice=random.choice([1,1,1,2,2,2,2,2,2,3,3,3,3,3,3])
			if choice==1:
				flight_ticket_details(campaign_data, app_data, device_data,isOpen,t1=2,t2=4)
				time.sleep(random.randint(5,8))

				if random.randint(1,100)<=80:
					flight_issue_ticket(campaign_data, app_data, device_data,isOpen,t1=1,t2=3)
					time.sleep(random.randint(10,15))

					flight_payment_show(campaign_data, app_data, device_data,isOpen,t1=1,t2=2)
					time.sleep(random.randint(10,15))

			if choice==2:
				train_ticket_details(campaign_data, app_data, device_data,isOpen,t1=4,t2=6)
				time.sleep(random.randint(5,8))

				if random.randint(1,100)<=85:
					train_payment_show(campaign_data, app_data, device_data,isOpen,t1=1,t2=2)
					time.sleep(random.randint(10,15))

			if choice==3:
				bus_ticket_details(campaign_data, app_data, device_data,isOpen,t1=4,t2=6)
				time.sleep(random.randint(5,8))

				if random.randint(1,100)<=85:
					bus_payment_show(campaign_data, app_data, device_data,isOpen,t1=1,t2=2)
					time.sleep(random.randint(10,15))

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def flight_ticket_details(campaign_data, app_data, device_data,isOpen=None,t1=1,t2=2):
	print '\nAdjust : EVENT____________________________'
	request=adjust_event(campaign_data, app_data, device_data,isOpen,event_token='qv9wmd',t1=t1,t2=t2)
	util.execute_request(**request)

def flight_issue_ticket(campaign_data, app_data, device_data,isOpen=None,t1=1,t2=2):
	print '\nAdjust : EVENT____________________________'
	request=adjust_event(campaign_data, app_data, device_data,isOpen,event_token='j3qdix',t1=t1,t2=t2)
	util.execute_request(**request)

def flight_payment_show(campaign_data, app_data, device_data,isOpen=None,t1=1,t2=2):
	print '\nAdjust : EVENT____________________________'
	request=adjust_event(campaign_data, app_data, device_data,isOpen,event_token='ae4zf3',t1=t1,t2=t2)
	util.execute_request(**request)

def train_ticket_details(campaign_data, app_data, device_data,isOpen=None,t1=1,t2=2):
	print '\nAdjust : EVENT____________________________'
	request=adjust_event(campaign_data, app_data, device_data,isOpen,event_token='w5gmey',t1=t1,t2=t2)
	util.execute_request(**request)

def train_payment_show(campaign_data, app_data, device_data,isOpen=None,t1=1,t2=2):
	print '\nAdjust : EVENT____________________________'
	request=adjust_event(campaign_data, app_data, device_data,isOpen,event_token='39c4hn',t1=t1,t2=t2)
	util.execute_request(**request)

def bus_ticket_details(campaign_data, app_data, device_data,isOpen=None,t1=1,t2=2):
	print '\nAdjust : EVENT____________________________'
	request=adjust_event(campaign_data, app_data, device_data,isOpen,event_token='nai0dw',t1=t1,t2=t2)
	util.execute_request(**request)

def bus_payment_show(campaign_data, app_data, device_data,isOpen=None,t1=1,t2=2):
	print '\nAdjust : EVENT____________________________'
	request=adjust_event(campaign_data, app_data, device_data,isOpen,event_token='hunzte',t1=t1,t2=t2)
	util.execute_request(**request)

###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,isOpen=False):
	set_androidUUID(app_data)
	inc_(app_data,'session_count')
	time_spent=int(time.time())-adjust_call_time

	url 	= 'http://app.adjust.com/session'
	method 	= 'post'
	headers = {
		'Client-SDK'		: campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/x-www-form-urlencoded',
		'User-Agent'		: get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'connectivity_type'		:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('brand').upper(),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:app_data.get('installed_at'),
		'language'				:device_data.get('locale').get('language'),
		'mcc'					:device_data.get('mcc'),
		'mnc'					:device_data.get('mnc'),
		'needs_response_details':'1',
		'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'large',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'tracking_enabled'		:'1',
		'updated_at'			:app_data.get('installed_at'),
		'vm_isa'				:'arm64',
		}
	if isOpen:
		del data['country']
		data['last_interval']=int(time.time())-adjust_call_time,
		data['session_length']=time_spent
		data['time_spent']=time_spent
		data['subsession_count']=app_data.get('subsession_count')

	headers['Authorization']= get_auth(app_data,activity_kind='session',gps_adid=str(data['gps_adid']),created_at=data['created_at'])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,source=''):
	set_androidUUID(app_data)
	inc_(app_data,'subsession_count')

	sessionLength=int(time.time())-adjust_call_time

	url 	= 'http://app.adjust.com/sdk_click'
	method 	='post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
	
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'click_time'			:created_at,
		'connectivity_type'		:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('brand').upper(),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:app_data.get('installed_at'),
		'language'				:device_data.get('locale').get('language'),
		'last_interval'			:int(time.time())-adjust_call_time,
		'mcc'					:device_data.get('mcc'),
		'mnc'					:device_data.get('mnc'),
		'needs_response_details':'1',
		'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'referrer'				:urllib.quote(app_data.get('referrer')),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'large',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'session_length'		:sessionLength,
		'source'				:source,
		'subsession_count'		:app_data.get('subsession_count'),
		'time_spent'			:sessionLength,
		'tracking_enabled'		:'1',
		'updated_at'			:app_data.get('installed_at'),
		'vm_isa'				:'arm64',
		}
	
	if source=='reftag':
		data['raw_referrer']=urllib.quote(app_data.get('referrer'))
		data['referrer']=urllib.unquote(app_data.get('referrer'))

	headers['Authorization']= get_auth(app_data,activity_kind='click',gps_adid=str(data['gps_adid']),created_at=data['created_at'])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_sdkinfo(campaign_data, app_data, device_data):
	url 	= 'http://app.adjust.com/sdk_info'
	method 	='post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	data = {
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:get_date(app_data,device_data),
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'needs_response_details':'1',
		'push_token'			:get_gcmToken(app_data),
		'sent_at'				:get_date(app_data,device_data),
		'source'				:'push',
		'tracking_enabled'		:'1',

		}
	headers['Authorization'] = get_auth(app_data,activity_kind='info',gps_adid=str(data['gps_adid']),created_at=data['created_at'])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data, device_data):
	url 	= 'http://app.adjust.com/attribution'
	method 	='head'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	params = {
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:created_at,
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'needs_response_details':'1',
		'initiated_by'			:'backend',
		'sent_at'				:attribution_sent,
		'tracking_enabled'		:'1',
		}

	headers['Authorization']= get_auth(app_data,activity_kind='attribution',gps_adid=str(params['gps_adid']),created_at=params['created_at'])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,isOpen,event_token,callback_params=None,partner_params=None,t1=1,t2=2):
	inc_(app_data,'event_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	set_androidUUID(app_data)
	sessionLength=int(time.time())-adjust_call_time

	url 	= 'http://app.adjust.com/event'
	method 	= 'post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'connectivity_type'		:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('brand').upper(),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'event_count'			:app_data.get('event_count'),
		'event_token'			:event_token,
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'language'				:device_data.get('locale').get('language'),
		'mcc'					:device_data.get('mcc'),
		'mnc'					:device_data.get('mnc'),
		'needs_response_details':'1',
		'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'large',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'session_length'		:sessionLength,
		'subsession_count'		:app_data.get('subsession_count'),
		'tracking_enabled'		:'1',
		'time_spent'			:sessionLength,
		'vm_isa'				:'arm64',
		}

	if isOpen:
		del data['country']

	if callback_params:
		data['callback_params']	= callback_params
	if partner_params:
		data['partner_params']	= partner_params

	headers['Authorization']= get_auth(app_data,activity_kind='event',gps_adid=str(data['gps_adid']),created_at=data['created_at'])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())

def dev_token(app_data):
	if not app_data.get('device_token'):
		app_data['device_token']=str(uuid.uuid4())		

def u_id(app_data):
	if not app_data.get('u_id'):
		app_data['u_id']="13"+str(util.get_random_string('decimal',7))


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def set_installedAT(app_data,device_data):
	if not app_data.get('installed_at'):
		app_data['installed_at'] =  get_date(app_data,device_data,early=random.uniform(5,10))

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_auth(app_data,activity_kind,gps_adid,created_at):
	final_str=str(created_at)+str(campaign_data.get('adjust').get('secret_key'))+str(gps_adid)+str(activity_kind)
	sign=util.sha256(final_str)
	auth='Signature secret_id="'+str(campaign_data.get('adjust').get('secret_id'))+'",signature="'+str(sign)+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind "'
	return auth

def get_gcmToken(app_data):
	if not app_data.get('push_token'):
		d=''.join(random.choice(string.digits + string.ascii_letters) for _ in range(11))
		app_data['push_token']=d+':'+'APA91b'+''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))
	return app_data.get('push_token')

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date