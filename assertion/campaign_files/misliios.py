# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
import time
import random
import json
import datetime
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.tikle.misli',
	'app_size'			 : 68.6,#68.2,#59.5,
	'app_name' 			 :'Misli',
	'app_version_name' 	 : '4.0.4',#'4.0.3',#'4.0.2',#'4.0.1',#'4.0.0',#'3.4.9',
	'app_version_code' 	 : '443',#'438',#'434',#'431',#'427',#'296',
	'app_id' 			 : '531126816',
	'ctr' 				 : 6,
	'sdk' 		 		 : 'ios',
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '9.0',
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 	: '4k0x0iv4h3eo',
		'sdk'		 	: 'ios4.17.3',
		'app_updated_at': '2019-09-20T19:19:11.000Z',#'2019-09-11T20:52:59.000Z',#'2019-09-02T17:49:29.000Z',#'2019-08-28T08:05:56.000Z',#'2019-08-23T02:36:43.000Z',#'2019-07-31T14:44:49.000Z',
		},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),
	install_receipt()
	pushtoken(app_data)
	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	################generating_realtime_differences##################
	s1=0
	s2=0
	a1=0
	a2=0
	id1=0
	id2=0
	click_time_iad3=False

 ###########	 CALLS		 ############
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)

	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,initiated_by="sdk",t1=a1,t2=a2)
	util.execute_request(**request)

	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time_iad3,source='iad3',t1=id1,t2=id2)
	util.execute_request(**request)

	return {'status':'true'}

#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),
	
	install_receipt()
	pushtoken(app_data)
	
	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	################generating_realtime_differences##################
	s1=0
	s2=0
	a1=0
	a2=0

 ###########	 CALLS		 ############
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2,isOpen=True)
	util.execute_request(**request)
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2)
	util.execute_request(**request)

	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################


###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	make_session_count(app_data,device_data)
	app_data['subsession_count'] = 0
	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		"Client-Sdk" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {

		}
	data = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_updated_at" : campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
		"app_version" : campaign_data.get('app_version_code'),
		"app_version_short" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"bundle_id" : campaign_data.get('package_name'),
		"connectivity_type" : "2",
		"country" : device_data.get('locale').get('country').upper(),
		"cpu_type" : device_data.get('cpu_type'),
		"created_at" : created_at,
		"device_name" : device_data.get('device_platform'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : 0,
		"hardware_name" : device_data.get('hardware'),
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"install_receipt" : install_receipt(),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : 1,
		"network_type" : "CTRadioAccessTechnologyHSDPA",
		"os_build" : device_data.get('build'),
		"os_name" : "ios",
		"os_version" : device_data.get('os_version'),
		"persistent_ios_uuid" : app_data.get('ios_uuid'),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"tracking_enabled" : 1,

		}
	if isOpen:
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = app_data.get('session_Length')
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = app_data.get('session_Length')


	
	# headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	



###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,initiated_by="backend",t1=0,t2=0):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		"Client-Sdk" : campaign_data.get('adjust').get('sdk'),
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_code'),
		"app_version_short" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"bundle_id" : campaign_data.get('package_name'),
		"created_at" : created_at,
		"device_name" : device_data.get('device_platform'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : 0,
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"initiated_by" : initiated_by,
		"needs_response_details" : 1,
		"os_build" : device_data.get('build'),
		"os_name" : "ios",
		"os_version" : device_data.get('os_version'),
		"persistent_ios_uuid" : app_data.get('ios_uuid'),
		"sent_at" : sent_at,

		}
	data = {

		}

	
	# headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('idfa'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,callback_params=None,partner_params=None):
	sdk_clk_time=get_date(app_data,device_data)
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	def_(app_data,'subsession_count')
	time_spent=int(time.time())-app_data.get('adjust_call_time')
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		"Client-Sdk" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {

		}
	data = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_updated_at" : campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
		"app_version" : campaign_data.get('app_version_code'),
		"app_version_short" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"bundle_id" : campaign_data.get('package_name'),
		"connectivity_type" : "2",
		"country" : device_data.get('locale').get('country').upper(),
		"cpu_type" : device_data.get('cpu_type'),
		"created_at" : created_at,
		"details" : json.dumps({"Version3.1":{"iad-attribution":"false"}}),
		"device_name" : device_data.get('device_platform'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : 0,
		"hardware_name" : device_data.get('hardware'),
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"install_receipt" : install_receipt(),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"last_interval" : '3',
		"needs_response_details" : 1,
		"os_build" : device_data.get('build'),
		"os_name" : "ios",
		"os_version" : device_data.get('os_version'),
		"persistent_ios_uuid" : app_data.get('ios_uuid'),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count',1),
		"session_length" : time_spent,
		"source" : source,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : 1,
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"network_type" :	"CTRadioAccessTechnologyLTE",
		}
	if source=='iad3':
		if data.get('deeplink'):
			del data['deeplink']
		if data.get('click_time'):
			del data['click_time']
		data['details']=json.dumps({"Version3.1":{"iad-attribution":"false"}})

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}



def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('hex',64)

def device_id(app_data):
	if not app_data.get('x-device-id'):
		app_data['x-device-id']=str(uuid.uuid4()).upper()

def get_auth(device_data,app_data,created_at,idfa,activity_type):
	data1= str(campaign_data['adjust']['secret_key']+activity_type+idfa+created_at)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'	

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date
# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"

def install_receipt():
	return 'MIISeAYJKoZIhvcNAQcCoIISaTCCEmUCAQExCzAJBgUrDgMCGgUAMIICGQYJKoZIhvcNAQcBoIICCgSCAgYxggICMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgCNMA0CAQMCAQEEBQwDNDMxMA0CAQoCAQEEBRYDMTcrMA0CAQ0CAQEEBQIDAdWIMA0CARMCAQEEBQwDMjk2MA4CAQECAQEEBgIEH6haIDAOAgEJAgEBBAYCBFAyNTMwDgIBCwIBAQQGAgQHIwefMA4CARACAQEEBgIEMZ+f6jAQAgEPAgEBBAgCBkRBxdRM6DAUAgEAAgEBBAwMClByb2R1Y3Rpb24wGAIBBAIBAgQQjmmBPoojZ3zsbxERqCprmTAZAgECAgEBBBEMD2NvbS50aWtsZS5taXNsaTAcAgEFAgEBBBQmRMeGrJWmE1fjjUXhI/zkqXOB3DAeAgEIAgEBBBYWFDIwMTktMDgtMzBUMTA6MDk6MTVaMB4CAQwCAQEEFhYUMjAxOS0wOC0zMFQxMDowOToxNVowHgIBEgIBAQQWFhQyMDE5LTA4LTIyVDA4OjM5OjQ1WjBAAgEGAgEBBDine74kB1z16GM492cDPMxeJT5/iuvLQB+WTOLepJHFJoiwMUQ9d+NAX+8K88Q9QFTHU0cKPbvlJTBAAgEHAgEBBDgJh4JuHvNvdxsUGYh/fdgaQLdlqE57KiKM8jKhsYbsgx6gqHoF0Jjptphs244I3periaDUCn9pXqCCDmUwggV8MIIEZKADAgECAggO61eH554JjTANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xNTExMTMwMjE1MDlaFw0yMzAyMDcyMTQ4NDdaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClz4H9JaKBW9aH7SPaMxyO4iPApcQmyz3Gn+xKDVWG/6QC15fKOVRtfX+yVBidxCxScY5ke4LOibpJ1gjltIhxzz9bRi7GxB24A6lYogQ+IXjV27fQjhKNg0xbKmg3k8LyvR7E0qEMSlhSqxLj7d0fmBWQNS3CzBLKjUiB91h4VGvojDE2H0oGDEdU8zeQuLKSiX1fpIVK4cCc4Lqku4KXY/Qrk8H9Pm/KwfU8qY9SGsAlCnYO3v6Z/v/Ca/VbXqxzUUkIVonMQ5DMjoEC0KCXtlyxoWlph5AQaCYmObgdEHOwCl3Fc9DfdjvYLdmIHuPsB8/ijtDT+iZVge/iA0kjAgMBAAGjggHXMIIB0zA/BggrBgEFBQcBAQQzMDEwLwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcjA0MB0GA1UdDgQWBBSRpJz8xHa3n6CK9E31jzZd7SsEhTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHgYDVR0gBIIBFTCCAREwggENBgoqhkiG92NkBQYBMIH+MIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDYGCCsGAQUFBwIBFipodHRwOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQANphvTLj3jWysHbkKWbNPojEMwgl/gXNGNvr0PvRr8JZLbjIXDgFnf4+LXLgUUrA3btrj+/DUufMutF2uOfx/kd7mxZ5W0E16mGYZ2+FogledjjA9z/Ojtxh+umfhlSFyg4Cg6wBA3LbmgBDkfc7nIBf3y3n8aKipuKwH8oCBc2et9J6Yz+PWY4L5E27FMZ/xuCk/J4gao0pfzp45rUaJahHVl0RYEYuPBX/UIqc9o2ZIAycGMs/iNAGS6WGDAfK+PdcppuVsq1h1obphC9UynNxmbzDscehlD86Ntv0hgBgw2kivs3hi1EdotI9CO/KBpnBcbnoB7OUdFMGEvxxOoMIIEIjCCAwqgAwIBAgIIAd68xDltoBAwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTEzMDIwNzIxNDg0N1oXDTIzMDIwNzIxNDg0N1owgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjgaYwgaMwHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwDgYDVR0PAQH/BAQDAgGGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQBPz+9Zviz1smwvj+4ThzLoBTWobot9yWkMudkXvHcs1Gfi/ZptOllc34MBvbKuKmFysa/Nw0Uwj6ODDc4dR7Txk4qjdJukw5hyhzs+r0ULklS5MruQGFNrCk4QttkdUGwhgAqJTleMa1s8Pab93vcNIx0LSiaHP7qRkkykGRIZbVf1eliHe2iK5IaMSuviSRSqpd1VAKmuu0swruGgsbwpgOYJd+W+NKIByn/c4grmO7i77LpilfMFY0GCzQ87HUyVpNur+cmV6U/kTecmmYHpvPm0KdIBembhLoz2IYrF+Hjhga6/05Cdqa3zr/04GpZnMBxRpVzscYqCtGwPDBUfMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCA7rV4fnngmNMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEAfwQOTggo7JbXsq+0YM68Rh95YgThR4dxuKcZ+j8kiS6XkYvY32N/JeJ5+IqnMD2cDEYHRqwervdzdDlxtGsQ5SzRJOWhgVxioqfQhXHf0WU27skFURrps6xn4S7/1QICheiUvfXGu9G7dTGHvfV2374+sB3Yai7R9R1GEryAT5Y7bRa6FGsmbewVPcwD+Dw4LmVisTRBd1Mh2knDXwRIQOBWyJnaBblwlLZvtbn4VdeC86nwfNjJ31BQgnedoZmqcUi9jbszxb3lMVhzzRVB9EB/xMDTWpIoG0a30Id3IZfnYgX6ME+jVZ+8kBHN+SZQ/exmPXShNPq0TKRZX1hjwQ=='
