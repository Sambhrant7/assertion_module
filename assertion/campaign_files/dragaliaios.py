import random,time,datetime,uuid, json
from sdk import NameLists
from sdk import getsleep
from sdk import util,installtimenew
import urllib
import clicker,Config

campaign_data = {
	'app_size' : 343.2,#316.7,#3072.0,#3891.2, 4096.0,#3686.4,#3276.8,
	'package_name' : 'com.nintendo.zaga',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'ctr':6,
	'app_name' : 'Dragalia',
	'device_targeting' : True,
	'app_id':'1352230941',
	'app_version_code': '78',#'77',#76, '74',#'72',#'71',#67, '66',#'64',#'63',#'57',#'54',#'50',#'49',#'48','43', #'40',
	'app_version_name': '1.13.1',#'1.13.0',#1.12.0, '1.11.0',#'1.10.1',#'1.10.0',#1.9.1, '1.9.0',#'1.8.0',#'1.7.1',#'1.5.1',#'1.4.5',#'1.4.0',#'1.3.1',#'1.3.0','1.2.1',#'1.1.2',
	'supported_os':'9.0',
	'supported_countries': 'WW',
	'sdk' : 'ios',
	'tracker': 'adjust',
	'adjust':{
				'app_token': 'x6cekznkwuf4',
				'app_secret': '1433645557355794929756316731603491472',
				'sdk':'unity4.14.1@ios4.14.1',
				'app_updated_at': '2019-10-30T12:12:54.000Z',#'2019-10-24T04:57:40.000Z',#2019-09-17T09:11:30.000Z, '2019-08-20T07:14:57.000Z',#'2019-07-22T16:38:46.000Z',#2019-07-05T16:20:56.000Z, '2019-06-20T18:16:00.000Z',#'2019-05-20T17:51:44.000Z',#'2019-04-22T20:35:10.000Z',#'2019-02-21T13:12:10.000Z',#'2019-02-06T11:19:00.000Z',#'2019-01-18T18:13:24.000Z',#'2018-12-20T13:43:32.000Z',#'2018-12-12T19:07:52.000Z','2018-11-27T15:30:58.000Z', #'2018-10-26T19:07:22.000Z',
				'signature_id':'1',
			},
	'retention':{
					1:71,
					2:69,
					3:67,
					4:65,
					5:63,
					6:61,
					7:57,
					8:55,
					9:53,
					10:51,
					11:48,
					12:45,
					13:42,
					14:39,
					15:37,
					16:35,
					17:33,
					18:32,
					19:31,
					20:30,
					21:30,
					22:29,
					23:28,
					24:27,
					25:26,
					26:25,
					27:25,
					28:24,
					29:23,
					30:22,
					31:20,
					32:16,
					33:12,
					34:8,
					35:5,
	}
}


def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	
	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')
		
def make_subsession_count(app_data,device_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 1
	else:
		app_data['subsession_count'] += 1
		
	return app_data.get('subsession_count')

def get_install_reciept(app_data):
	install_receipt='MIISiAYJKoZIhvcNAQ'+util.get_random_string('google_token',6322)
	if not app_data.get('install_receipt'):
		app_data['install_receipt']=install_receipt

def install(app_data, device_data):

	print 'plz wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")
	pushtoken(app_data)
	def_sec(app_data,device_data)

	app_data['installed_at'] = datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	
	get_install_reciept(app_data)
	print '*************Install**********************'
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('start_game_first_time'):
		app_data['start_game_first_time'] = True


	time.sleep(random.randint(60,100))
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data, t1 = 1, t2 = 3)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)


	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data, t1 = 2, t2 = 4)
	util.execute_request(**adjustSession)


	print '-----------------------ADJUST Event-------------------------  app open  ----'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '2w7jh2', t1 = 6, t2 = 8)  
	util.execute_request(**adjust_event_req)


	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,t1=8,t2=10)
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,t1=8,t2=10,initiated_by='sdk')
	util.execute_request(**adjustSession)


	print "\n----------------------------ADJUST Sdk_info ------------------------------------"
	adjustSdk_info = adjust_sdkinfo(campaign_data, app_data, device_data,t1=5,t2=7)
	util.execute_request(**adjustSdk_info)

	if random.randint(1,100) <= 80:

		time.sleep(random.randint(420,480))
		print '-----------------------ADJUST Event--------------------------  summon character  ----'
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'v8am5a', t1 = 0, t2 = 0)  
		util.execute_request(**adjust_event_req)

		time.sleep(random.randint(120,180))
		print '-----------------------ADJUST Event--------------------------  start game  ----'
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '22kx5g', t1 = 0, t2 = 0)  
		util.execute_request(**adjust_event_req)
		app_data['start_game_first_time'] = False

		if random.randint(1,100) <= 95:
			time.sleep(random.randint(180,240))
			print '-----------------------ADJUST Event--------------------------  game end  ----'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'aero2w', t1 = 0, t2 = 0)  
			util.execute_request(**adjust_event_req)

	app_data['app_close_time'] = int(time.time())
		
	return {'status':True}


###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):

	if not app_data.get('installed_at'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")
		def_sec(app_data,device_data)
		app_data['installed_at'] = datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	

	if not app_data.get('app_close_time'):
		app_data['app_close_time'] = int(time.time())

	get_install_reciept(app_data)

	print '*************Open**********************'
	
	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		if not device_data.get('idfv_id'):
			app_data['idfv_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfv_id'] = device_data.get('idfv_id')

		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())


	if not app_data.get('start_game_first_time'):
		app_data['start_game_first_time'] = True


	################## CALLS #####################

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data, t1 = 2, t2 = 4, isOpen = True)
	util.execute_request(**adjustSession)

	print '-----------------------ADJUST Event-------------------------  app open  ----'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '2w7jh2', t1 = 6, t2 = 8, isOpen = True)  
	util.execute_request(**adjust_event_req)

	if app_data.get('start_game_first_time'):
		time.sleep(random.randint(420,480))
		print '-----------------------ADJUST Event--------------------------  summon character  ----'
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'v8am5a', t1 = 0, t2 = 0, isOpen = True)  
		util.execute_request(**adjust_event_req)

		time.sleep(random.randint(120,180))
		print '-----------------------ADJUST Event--------------------------  start game  ----'
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '22kx5g', t1 = 0, t2 = 0, isOpen = True)  
		util.execute_request(**adjust_event_req)
		app_data['start_game_first_time'] = False

	if random.randint(1,100) <= 65:
		time.sleep(random.randint(180,240))
		print '-----------------------ADJUST Event--------------------------  game end  ----'
		adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'aero2w', t1 = 0, t2 = 0, isOpen = True)  
		util.execute_request(**adjust_event_req)

	app_data['app_close_time'] = int(time.time())

	return {'status':True}



###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0, isOpen = False):
	
	
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'connectivity_type' : 2,
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': created_at,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'install_receipt':app_data.get('install_receipt'),
			'installed_at' : app_data.get('installed_at'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_build':	device_data.get('build'),
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			# 'queue_size': 1,
			'push_token':app_data.get('pushtoken'),
			'sent_at': sent_at,
			'session_count': make_session_count(app_data,device_data),
			'tracking_enabled':	0,
			}

	if isOpen:
		pushtoken(app_data)
		data['push_token'] = app_data.get('pushtoken')
		data['session_length'] = (app_data.get('app_close_time') - app_data.get('time_passes')) if app_data.get('time_passes') else 0
		data['time_spent'] = (data.get('session_length') - random.randint(20,50)) if data.get('session_length') > 100 else data.get('session_length')
		data['subsession_count'] = 9
		data['last_interval'] = int(time.time()) - app_data.get('app_close_time')
		if data.get('queue_size'):
			del data['queue_size']
		
	app_data['time_passes'] = int(time.time())

	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	



def adjust_sdkclick(campaign_data, app_data, device_data,t1=0,t2=0,callback_params=False,partner_params=False):
	
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	
	params={}

	created_at=get_date(app_data,device_data)
	created_at_time = int(time.time())
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'connectivity_type' : 2,
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': created_at,
			'details': json.dumps({"Version3.1":{"iad-attribution":"false"}}),
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'install_receipt':app_data.get('install_receipt'),
			'installed_at' : app_data.get('installed_at'),
			'language':	device_data.get('locale').get('language'),
			'last_interval' : 1,
			'needs_response_details': 1,
			'os_build':	device_data.get('build'),
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'sent_at': sent_at,
			'session_count':str(app_data.get('session_count',1)),
			'source': 'iad3',
			'subsession_count': 1,
			'tracking_enabled': 0,
			'push_token':app_data.get('pushtoken'),
			}

	if app_data.get('time_passes'):
		data['session_length'] = created_at_time - app_data.get('time_passes')
		data['time_spent'] = data['session_length']
	else:
		data['time_spent'] = 0
		data['session_length'] = 0
		app_data['time_passes'] = int(time.time())
	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'click')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	

def adjust_attribution(campaign_data, app_data, device_data,t1=0,t2=0,initiated_by='backend'):

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	
	data={}

	params = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink' : 1,
			'created_at': created_at,
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'initiated_by' : initiated_by,
			'needs_response_details': 1,
			'sent_at': sent_at
			}

	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('idfa'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	


def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=False, partner_params=False, purchase=False,t1=0,t2=0, isOpen = False):
	
	created_at_event=get_date(app_data,device_data)
	created_at_time = int(time.time())
	time.sleep(random.randint(t1,t2))
	sent_at_event=get_date(app_data,device_data)
	inc_(app_data, 'event_count')	
		
	url = 'http://app.adjust.com/event'
	
	headers = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}

	data = {
			'app_token':campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short':campaign_data.get('app_version_name'),
			'attribution_deeplink': 1,
			'bundle_id':campaign_data.get('package_name'),
			'connectivity_type' : 2,
			'country':device_data.get('locale').get('country'),
			'cpu_type':device_data.get('cpu_type'),
			'created_at': created_at_event,
			'device_name':device_data.get('device_platform'),
			'device_type':device_data.get('device_type'),
			'environment':'production',
			'event_buffering_enabled':	0,
			'event_count':app_data.get('event_count'),
			'event_token':event_token,
			'hardware_name': device_data.get('hardware'),	
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'install_receipt':app_data.get('install_receipt'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details':1,
			'os_build':	device_data.get('build'),
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			# 'queue_size': 1,
			'push_token':app_data.get('pushtoken'),
			'sent_at': sent_at_event,
			'session_count':str(app_data.get('session_count',0)),
			'subsession_count': 1,
			'tracking_enabled': '0',
			}

	if app_data.get('time_passes'):
		data['session_length'] = created_at_time - app_data.get('time_passes')
		data['time_spent'] = data['session_length']
	else:
		data['time_spent'] = 0
		data['session_length'] = 0
		app_data['time_passes'] = int(time.time())

	# if app_data.get('pushtoken'):
	# 	data['push_token'] = app_data.get('pushtoken')

	if app_data.get('event_count',0) > 1:
		data['subsession_count'] = 7
		if data.get('queue_size'):
			del data['queue_size']
	
	if partner_params:
		data['partner_params']=partner_params	
	if callback_params:
		data['callback_params'] = callback_params

	if isOpen:
		if data.get('queue_size'):
			del data['queue_size']
		
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'event')

	return {'url': url, 'httpmethod': 'post', 'headers': headers, 'params':None, 'data': data}	


def adjust_sdkinfo(campaign_data, app_data, device_data, t1 = 0, t2 = 0):
	pushtoken(app_data)

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	url = 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent':urllib.pathname2url(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}

	params={}

	data = {

		'app_token': campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink' : 1,
		'created_at': created_at,
		'environment' : 'production',
		'event_buffering_enabled': 0,
		'idfa': app_data.get('idfa_id'),
		'idfv':	app_data.get('idfv_id'),
		'needs_response_details': 1,
		'persistent_ios_uuid': app_data.get('ios_uuid'),
		'push_token':app_data.get('pushtoken'),
		'sent_at': sent_at,
		'source' :'push'
	}
	headers['Authorization']=headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'info')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###########################################################
#						UTIL							  #
###########################################################

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	# st = str(int(time.time()*1000))
	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

	
def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('hex',64)

def device_id(app_data):
	if not app_data.get('x-device-id'):
		app_data['x-device-id']=str(uuid.uuid4()).upper()

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['phone'] 		="+971"+'-50-'+util.get_random_string('decimal',7)
		app_data['user']['password'] 	= app_data['user']['lastname']+random.choice(["@","!"])+app_data['user']['dob'].replace("-","") 

def get_auth(device_data,app_data,created_at,idfa,activity_type):
	data1= str(campaign_data['adjust']['app_secret']+activity_type+idfa+created_at)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('signature_id')+'",signature="'+sign+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'	
