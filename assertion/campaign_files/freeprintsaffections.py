from sdk import util, purchase
from sdk import NameLists,installtimenew
from sdk import getsleep
from Crypto.Cipher import AES
import time
import random
import json
import uuid
import urllib
import clicker
import Config


#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'com.photoaffections.freeprints',
	'app_name' 			 :'FREEPRINT',
	'app_version_code'	 :'24867',#'11884',#'11478',#'11436',#'11314',#'11207',#'11162',#'11107',#'10901',10956, 11306
	'app_version_name'	 :'2.48.0',#'2.30.3',#'2.25.0',#'2.24.0',#'2.23.1',#'2.22.0',#'2.21.0',#'2.20.4',#'2.18.2',2.19.0, 2.23.0
	'no_referrer'		 : True,
	'supported_os'		 :'4.4',#'4.0',		
	'supported_countries':'WW',
	'device_targeting':True,
	'ctr'				 :6,
	'app_size'			: 66.0,#51.0,
	'tracker':'branch',
	'api_branch':{
	    'branchkey'  :'key_live_kdm7yMnRk6IuCgRuH9ncMfbcqrpV1KEv',
	    'sdk'        :'android3.0.3',
	    'sdk_version'   :'3.0.3'  
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:19,
		24:18,
		25:17,
		26:16,
		27:15,
		28:14,
		29:13,
		30:12,
		31:11,
		32:8,
		33:7,
		34:6,
		35:5,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")
	
	###########		CALLS		################	
	app_data['device_id']=str(uuid.uuid4()).upper()

	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	app_data['api_hit_time']=time.time()
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'	

	time.sleep(random.randint(1,2))
	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(8,12))
	print "config call-----------"
	req = measurement_config(campaign_data, app_data, device_data)
	util.execute_request(**req)

	time.sleep(random.randint(20,30))
	print "SELFCALL___________________Registration/Login"
	req=registration_selfcall(campaign_data, app_data, device_data)
	util.execute_request(**req)


	for _ in range(random.randint(1,3)):

		if random.randint(1,100) <= 50:

			time.sleep(random.randint(2,5))

			print "Branch______open"
			request=api_branch_open(campaign_data, app_data, device_data)
			util.execute_request(**request)

			time.sleep(random.randint(1,2))
			print "Branch______sdk"
			request=branch_call(campaign_data, app_data, device_data)
			util.execute_request(**request)

			time.sleep(random.randint(15,20))
			print "Branch______close"
			request=api_branch_close(campaign_data, app_data, device_data)
			util.execute_request(**request)


	if purchase.isPurchase(app_data,day=1,advertiser_demand=5):
		time.sleep(random.randint(60,100))
		transaction_id = "FP"+str(random.randint(227162000000,227162999999))
		print "Branch______event"
		event_data={"currency":"USD","description":"initiate purchase","shipping":0,"tax":0,"revenue":0.01,"search_query":transaction_id,"transaction_id":transaction_id}
		req=api_branch_event(campaign_data, app_data, device_data, event_name='INITIATE_PURCHASE',event_data=event_data)
		util.execute_request(**req)

		print "Branch______event"
		event_data={"currency":"USD","description":"purchase","shipping":0,"tax":0,"revenue":0.01,"search_query":transaction_id,"transaction_id":transaction_id}
		req=api_branch_event(campaign_data, app_data, device_data, event_name='PURCHASE',event_data=event_data)
		util.execute_request(**req)


	time.sleep(random.randint(15,20))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)


	###########		FINALIZE	################
		
	return {'status':'true'}



#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	if not app_data.get('times'):
		print "Please wait installing..."
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")
	
	###########		CALLS		################	
	if not app_data.get('isregistered'):
		app_data['isregistered']=False


	if not app_data.get('device_fingerprint_id1'):
		print "Branch______install"
		request=api_branch_install(campaign_data,app_data,device_data)
		app_data['api_hit_time']=time.time()
		result=util.execute_request(**request)
		try:
			json_result=json.loads(result.get('data'))
			app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
			app_data['identity_id']=json_result.get('identity_id')
			app_data['session_id']=json_result.get('session_id')
		except:
			app_data['device_fingerprint_id1']='613284344550945604'
			app_data['identity_id']='641581897171978611'
			app_data['session_id']='641581897189090110'	

	else:

		print "Branch______open"
		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)


	for _ in range(random.randint(1,3)):
		if random.randint(1,100) <= 50:
			time.sleep(random.randint(1,2))
			print "Branch______sdk"
			request=branch_call(campaign_data, app_data, device_data)
			util.execute_request(**request)


			time.sleep(random.randint(15,20))
			print "Branch______close"
			request=api_branch_close(campaign_data, app_data, device_data)
			util.execute_request(**request)


	if purchase.isPurchase(app_data,day,advertiser_demand=10):
		time.sleep(random.randint(60,100))
		transaction_id = "FP"+str(random.randint(227162000000,227162999999))
		print "Branch______event"
		event_data={"currency":"USD","description":"initiate purchase","shipping":0,"tax":0,"revenue":0.01,"search_query":transaction_id,"transaction_id":transaction_id}
		req=api_branch_event(campaign_data, app_data, device_data, event_name='INITIATE_PURCHASE',event_data=event_data)
		util.execute_request(**req)

		print "Branch______event"
		event_data={"currency":"USD","description":"purchase","shipping":0,"tax":0,"revenue":0.01,"search_query":transaction_id,"transaction_id":transaction_id}
		req=api_branch_event(campaign_data, app_data, device_data, event_name='PURCHASE',event_data=event_data)
		util.execute_request(**req)



	time.sleep(random.randint(15,20))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)


	# print '\nMat : SESSION____________________________________'
	# request=mat_serve(campaign_data, app_data, device_data, isOpen=True)
	# response=util.execute_request(**request)
	# catch(response,app_data,'mat')
	# set_OpenLogID(app_data)
	# set_LastOpenLogID(app_data)

	# time.sleep(random.randint(10,20))
	# print '\nMat : SESSION____________________________________'
	# request=mat_serve(campaign_data, app_data, device_data, isOpen=True)
	# response=util.execute_request(**request)
	# catch(response,app_data,'mat')
	# set_OpenLogID(app_data)
	# set_LastOpenLogID(app_data)

	# if app_data.get('isregistered')==False:
	# 	time.sleep(random.randint(30,60))
	# 	registrationEvent(campaign_data, app_data, device_data)
	# 	app_data['isregistered']=True

	# if app_data.get('isregistered')==True:
	# 	if purchase.isPurchase(app_data,day,advertiser_demand=45):
	# 		time.sleep(random.randint(10,20))
	# 		purchaseEvent(campaign_data, app_data, device_data)

	# if random.randint(1,100)<=90:
	# 	print '\nMat : SESSION____________________________________'
	# 	request=mat_serve(campaign_data, app_data, device_data, isOpen=True)
	# 	response=util.execute_request(**request)
	# 	catch(response,app_data,'mat')
	# 	set_OpenLogID(app_data)
	# 	set_LastOpenLogID(app_data)

	###########		FINALIZE	################
		
	return {'status':'true'}

def test(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")
	
	###########		CALLS		################	
	app_data['device_id']=str(uuid.uuid4()).upper()

	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	app_data['api_hit_time']=time.time()
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'	

	time.sleep(random.randint(1,2))
	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(8,12))
	print "config call-----------"
	req = measurement_config(campaign_data, app_data, device_data)
	util.execute_request(**req)

	time.sleep(random.randint(35,45))
	print "SELFCALL___________________Registration/Login"
	req=registration_selfcall(campaign_data, app_data, device_data)
	util.execute_request(**req)

	time.sleep(random.randint(500,800))
	transaction_id = "FP"+str(random.randint(227162000000,227162999999))
	print "Branch______event"
	event_data={"currency":"USD","description":"initiate purchase","shipping":0,"tax":0,"revenue":0.01,"search_query":transaction_id,"transaction_id":transaction_id}
	req=api_branch_event(campaign_data, app_data, device_data, event_name='INITIATE_PURCHASE',event_data=event_data)
	util.execute_request(**req)

	print "Branch______event"
	event_data={"currency":"USD","description":"purchase","shipping":0,"tax":0,"revenue":0.01,"search_query":transaction_id,"transaction_id":transaction_id}
	req=api_branch_event(campaign_data, app_data, device_data, event_name='PURCHASE',event_data=event_data)
	util.execute_request(**req)


	time.sleep(random.randint(15,20))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)


	###########		FINALIZE	################
		
	return {'status':'true'}


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################


###################################################################
# mat_deeplink()	: method
# parameter 		: campaign_data, app_data, device_data,
#				      action, includeLogID
#
# Simulates Mat's behaviour for retargeting campaigns.
###################################################################



###################################################################
# mat_serve()	: method
# parameter 	: campaign_data, app_data, device_data,
#				  action, includeLogID, isOpen
#
# Simulates Mat's behaviour whenever the App gets open
# or incase of an in-app event.
###################################################################
def branch_call(campaign_data, app_data, device_data):
	url='http://cdn.branch.io/sdk/uriskiplist_v1.json'
	method='get'
	headers={
		'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
	}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': None}


def api_branch_install(campaign_data,app_data,device_data):
	method='post'
	url='http://api2.branch.io/v1/install'

	headers = {
				'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
				'Content-Type':'application/json',
				'Accept':'application/json',
			}

	data={"app_version":campaign_data.get('app_version_name'),
		"facebook_app_link_checked":False,
		"is_referrable":1,
		"update":0,
		"debug":False,
		"metadata":{},
		"hardware_id":device_data.get('android_id'),
		# 'clicked_referrer_ts':int(app_data.get('times').get('click_time')),
		# 'install_begin_ts':int(app_data.get('times').get('download_begin_time')),
		"is_hardware_id_real":True,
		"brand":device_data.get('brand').upper(),
		"model":device_data.get('model'),
		"screen_dpi":int(device_data.get('dpi')),
		"screen_height":int(device_data.get('resolution').split('x')[0]),
		"screen_width":int(device_data.get('resolution').split('x')[1]),
		"wifi":True,
		"os":"Android",
		"os_version":int(device_data.get('sdk')),
		"country":device_data.get('locale').get('country'),
		"language":device_data.get('locale').get('language'),
		"local_ip":device_data.get('private_ip'),
		"cd":{"mv":"-1","pn":campaign_data.get('package_name')},
		"google_advertising_id":device_data.get('adid'),
		"lat_val":0,
		"instrumentation":{"v1/install-qwt":"0"},
		"sdk":campaign_data.get('api_branch').get('sdk'),
		"retryNumber":0,
		"branch_key":campaign_data.get('api_branch').get('branchkey'),
		'environment':'FULL_APP',
		'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
		'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'ui_mode':'UI_MODE_TYPE_NORMAL',
		'previous_update_time':0
		} 

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}


def api_branch_event(campaign_data, app_data, device_data, event_name='',event_data=''):	
	
	url='http://api2.branch.io/v2/event/standard'
	header={
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={"name":event_name,
		"event_data":event_data,
		"user_data":{"android_id":device_data.get('android_id'),
		"brand":device_data.get('brand').upper(),
		"model":device_data.get('model'),
		"screen_dpi":device_data.get('dpi'),
		"screen_height":device_data.get('resolution').split('x')[0],
		"screen_width":device_data.get('resolution').split('x')[1],
		"os":"Android",
		"os_version":device_data.get('sdk'),
		"country":device_data.get('locale').get('country'),
		"language":device_data.get('locale').get('language'),
		"local_ip":device_data.get('private_ip'),
		"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
		"app_version":campaign_data.get('app_version_name'),
		"sdk":"android",
		"sdk_version":campaign_data.get('api_branch').get('sdk_version'),
		"user_agent":device_data.get('User-Agent'),
		"environment":"FULL_APP",
		"developer_identity":"bnc_no_value",
		"aaid":device_data.get('adid'),
		"limit_ad_tracking":0},
		"metadata":{},
		"instrumentation":{"v2/event/standard-qwt":"4",
		"v1/install-brtt":"1282"},
		"branch_key":campaign_data.get('api_branch').get('branchkey'),
		"retryNumber":0
		}
		
	if event_name=='PURCHASE':
		data["custom_data"]={"is_first_order":"true"}
		data['instrumentation']={"v2/event/standard-brtt":"3139","v2/event/standard-qwt":"3151"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def api_branch_open(campaign_data, app_data, device_data, call_seq=1):	
	
	url='http://api.branch.io/v1/open'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": "vivo",
        "cd": {       "mv": "-1", "pn": campaign_data.get('package_name')},
        "country": device_data.get('locale').get('country'),
        "debug": False,
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "environment": "FULL_APP",
        "facebook_app_link_checked": False,
        "first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {   "v1/open-qwt": "0"},
        "is_hardware_id_real": True,
        "is_referrable": 1,
        "language": "en",
        "lat_val": 0,
        "latest_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "latest_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "previous_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "update": 1,
        "wifi": True}
	if call_seq==2:
		data['instrumentation']={"v1/close-brtt":"891","v1/open-qwt":"0"}
	if call_seq==3:
		data['instrumentation']={"v1/close-brtt":"681","v1/open-qwt":"0"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}


def api_branch_close(campaign_data, app_data, device_data,call_seq=1):	
	
	url='http://api.branch.io/v1/close'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "country": device_data.get('locale').get('country'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       "v1/close-qwt": "0", "v1/event-brtt": "303"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val":"0",
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}
	if call_seq==2:
		data['instrumentation']={"v1/close-qwt":"10","v1/open-brtt":"692"}
	if call_seq==3:
		data['instrumentation']={"v1/close-qwt":"1","v1/open-brtt":"507"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}


def measurement_config(campaign_data, app_data, device_data):

	url = 'http://app-measurement.com/config/app/1%3A266165273665%3Aandroid%3A2d3bab06e9096512'
	
	header={
		'User-Agent': get_ua(device_data),
		'Accept-Encoding': 'gzip',
	}
		
	params={
		'app_instance_id':	'52a736f4709df5011476eceab0003790',
		'platform':	'android',
		'gmp_version':	'19420',
	}
	data=None

	return {'url': url, 'httpmethod': 'head', 'headers': header, 'params': params, 'data': data}

def registration_selfcall(campaign_data, app_data, device_data):
	register_user(app_data,country='united states')
	url = 'http://fp.photoaffections.com/api/'
	
	header={
		'User-Agent': campaign_data.get('app_name')+' Android/'+campaign_data.get('app_version_name'),
		'Accept-Encoding': 'gzip',
		'Content-Type' : 'application/x-www-form-urlencoded'
	}
		
	params={
		'_device' : app_data.get('device_id'),
		'_full_version' : campaign_data.get('app_version_name')+'.'+campaign_data.get('app_version_code'),
		'_method' : 'account.create',
		'_screen' : 'SigninActivity',
		'_app' : 'Android-FPUS-'+campaign_data.get('app_version_name'),
		'_language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')
	}
	data={
		'email' : app_data.get('user').get('email'),
		'password' : app_data.get('user').get('pswd'),
		'first_name' : app_data.get('user').get('firstname'),
		'last_name' : app_data.get('user').get('lastname')
	}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params': params, 'data': data}


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		# app_data['user']['sex'] 		= gender
		# app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		# app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['pswd']		= last_name+util.get_random_string('decimal',4)



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################
def get_locale(device_data):
	return device_data['locale']['language']+"_"+device_data['locale']['country']


###########################################################
# Utility methods : MAT
#
# Define/declare/Set/Unset/Increment various parameters
# required by Adjust
###########################################################

def def_matID(app_data):
	if not app_data.get('matID'):
		app_data['matID'] = str(uuid.uuid4())

def set_OpenLogID(app_data):
	if app_data.get('log_id'):
		app_data['open_log_id'] = app_data['log_id']

def set_LastOpenLogID(app_data):
	if app_data.get('log_id'):
		app_data['last_open_log_id'] = app_data['log_id']

def catch(response,app_data,paramName):
	try:
		if paramName=="mat":
			app_data['log_id'] = json.loads(response.get('data')).get('log_id')
	except Exception as inst:
		print inst		


###########################################################
# Utility methods : MISC
#
###########################################################
def time_Stamp():
	return int(time.time())

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1


########################################################
#					   AES							   #
########################################################
class AESCipher:
	def __init__(self, key):
		self.key = key
	def pad(self, raw):
		l = len(raw) % 16
		l = 16 - l
		for x in range(l):
			raw += ' '
		return raw

	def encrypt(self, iv, raw):
		raw = self.pad(raw)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return (cipher.encrypt(raw)).encode('hex')

	def decrypt(self, enc , iv):
		enc = enc.decode('hex')
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return cipher.decrypt(enc)