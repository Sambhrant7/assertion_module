from sdk import util,getsleep
from sdk import installtimenew
from sdk import NameLists
import time
import random
import json
import datetime
import urllib
import urlparse
import uuid
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'io.cleanfox.android',
	'app_name' 			 :'Cleanfox',
	'app_version_code'	 :'90',
	'app_version_name'	 : '3.4.3-101',#'3.4.2-100',#'3.2.10-90',#'3.2.9-89',#3.2.1-80, 3.1.4-78, '3.1.3-77',#'3.0.9-70',#'3.0.8-69',
	'no_referrer'		 : True,
	'supported_os'		 :'4.4',		
	'supported_countries':'WW',
	'ctr'				 :6,
	'app_size'			 : 11.0,#8.4, #8.3, 8.1
	'device_targeting':True,
	'tracker':'adjust',
	'adjust':{
		'app_token'	: 'dv85cnwnb8cg',
		'sdk'		: 'android4.18.3',#'android4.18.0',#'android4.17.0',#'android4.11.4',	
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############	
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	set_sessionLength(app_data,forced=True,length=0)
	
	###########		CALLS		################	
	if app_data.get('referrer'):
		print '\nAdjust : SDK CLICK____________________________________'
		request=adjust_sdkclick(campaign_data, app_data, device_data)
		app_data['api_hit_time']=time.time()
		util.execute_request(**request)


	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(8,20))

	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)

	if random.randint(1,100)<=90:
		time.sleep(random.randint(10,20))
		registration(campaign_data, app_data, device_data)

	###########		FINALIZE	################
	set_appCloseTime(app_data)
	
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	# ###########		INITIALIZE		############	
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	set_sessionLength(app_data,forced=True,length=random.randint(300,500))

	# ###########		CALLS		################	
	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,isOpen=True)
	util.execute_request(**request)

	set_sessionLength(app_data,forced=True,length=0)

	# ###########		FINALIZE	################
	# set_appCloseTime(app_data)
	
	return {'status':'true'}


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################
def registration(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT____________________________REGISTRATION'
	interval 		= random.randint(10,20)
	set_sessionLength(app_data,length=interval)
	request=adjust_event(campaign_data, app_data, device_data,'m77nc3')
	util.execute_request(**request)


###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,isOpen=False,callback_params=None):
	set_androidUUID(app_data)
	set_installedAT(app_data,device_data)
	inc_(app_data,'session_count')
	# fb_id(app_data)

	url 	= 'http://app.adjust.com/session'
	method 	= 'post'
	headers = {
		'Client-SDK'		: campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/x-www-form-urlencoded',
		'User-Agent'		: get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:get_date(app_data,device_data),
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		# 'fb_id'					:app_data.get('fb_id'),
		'gps_adid'				:device_data.get('adid'),
		'gps_adid_src'			:'service',
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:get_date(app_data,device_data),
		'session_count'			:app_data.get('session_count'),
		'tracking_enabled'		:'1',
		'updated_at'			:get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		#'vm_isa'				:'arm64',
		'connectivity_type'		:'1',
		'network_type'			:'13',
		'mcc'					:device_data.get('mcc'),
		'mnc'					:device_data.get('mnc'),
		}
	if isOpen:
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('sessionLength')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('sessionLength')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	set_installedAT(app_data,device_data)
	# inc_(app_data,'session_count')
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	def_sessionLength(app_data)
	# fb_id(app_data)

	url 	= 'http://app.adjust.com/event'
	method 	= 'post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			: get_date(app_data,device_data),
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'event_count'			:app_data.get('event_count'),
		'event_token'			:event_token,
		'environment'			:'production',
		'event_buffering_enabled':'0',
		# 'fb_id'					:app_data.get('fb_id'),
		'gps_adid'				:device_data.get('adid'),
		'gps_adid_src'			:'service',
		'hardware_name'			:device_data.get('hardware'),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:get_date(app_data,device_data),
		'session_count'			:app_data.get('session_count'),
		'session_length'		:app_data.get('sessionLength'),
		'subsession_count'		:app_data.get('subsession_count'),
		'tracking_enabled'		:'1',
		'time_spent'			:app_data.get('sessionLength'),
		#'vm_isa'				:'arm64',
		'connectivity_type'		:'1',
		'network_type'			:'13',
		'mcc'					:device_data.get('mcc'),
		'mnc'					:device_data.get('mnc'),
		}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data):
	set_androidUUID(app_data)
	set_installedAT(app_data,device_data)
	# inc_(app_data,'session_count')
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	def_sessionLength(app_data)
	# fb_id(app_data)
	reftag = ''
	temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
	if temp_b.get('adjust_reftag'):
		reftag = temp_b.get('adjust_reftag')[0]

	url 	= 'http://app.adjust.com/sdk_click'
	method 	='post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'click_time'			:get_date(app_data,device_data),
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:get_date(app_data,device_data),
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		# 'fb_id'					:app_data.get('fb_id'),
		'gps_adid'				:device_data.get('adid'),
		'gps_adid_src'			:'service',
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		'language'				:device_data.get('locale').get('language'),
		'last_interval'			:'0',
		'needs_response_details':'1',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'raw_referrer'			:app_data.get('referrer'),
		'package_name'			:campaign_data.get('package_name'),
		'referrer'				:urllib.unquote(app_data.get('referrer')),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:get_date(app_data,device_data),
		'source'				:'reftag',
		'session_count'			:app_data.get('session_count','1'),
		'session_length'		:app_data.get('sessionLength'),
		'subsession_count'		:app_data.get('subsession_count'),
		'time_spent'			:app_data.get('sessionLength'),
		'tracking_enabled'		:'1',
		'updated_at'			:get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		#'vm_isa'				:'arm64',
		'connectivity_type'		:'1',
		'network_type'			:'13',
		'mcc'					:device_data.get('mcc'),
		'mnc'					:device_data.get('mnc'),
		}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data, device_data):
	url 	= 'http://app.adjust.com/attribution'
	method 	='head'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	params = {
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:get_date(app_data,device_data),
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'needs_response_details':'1',
		'sent_at'				:get_date(app_data,device_data),
		'tracking_enabled'		:'1',
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_version'			:campaign_data.get('app_version_name'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'gps_adid_src'			:'service',
		'initiated_by'			:'sdk',
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),

		}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}



#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(user_name_list)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

# def fb_id(app_data):
# 	if not app_data.get('fb_id'):
# 		app_data['fb_id']= str(uuid.uuid4())	

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def set_installedAT(app_data,device_data):
	if not app_data.get('installed_at'):
		app_data['installed_at'] =  get_date(app_data,device_data,early=random.uniform(5,10))

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date
