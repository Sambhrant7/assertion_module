import random,time,datetime,uuid,json
from sdk import NameLists
from sdk import getsleep 
from sdk import installtimenew
from sdk import util
import urllib
import clicker,Config

campaign_data = {
	'package_name' : 'com.tipico.classic',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'ctr':6,
	'app_name' : 'TipicoSport',
	'app_id':'1051329602',
	'app_version_code':'2019030801',#'2018060802',#'449',
	'app_version_name':'4.1.6',#'4.1.3',#'1.12',
	'supported_os':'7.0',
	'app_size' : 45.4,
	'supported_countries': 'WW',
	'device_targeting':True,
	'sdk' : 'ios',
	'tracker': 'adjust',
	'adjust':{
				'app_token': 'i7f68cgpy3nk',
				'sdk': 'ios4.15.0',#'ios4.11.5',
				'app_updated_at': '2019-06-20T10:09:23.000Z',#'2018-06-08T21:42:57.000Z',
			},
	'retention':{
					1:71,
					2:69,
					3:67,
					4:65,
					5:63,
					6:61,
					7:57,
					8:55,
					9:53,
					10:51,
					11:48,
					12:45,
					13:42,
					14:39,
					15:37,
					16:35,
					17:33,
					18:32,
					19:31,
					20:20,
					21:19,
					22:18,
					23:17,
					24:16,
					25:15,
					26:14,
					27:13,
					28:12,
					29:11,
					30:10,
					31:9,
					32:8,
					33:7,
					34:6,
					35:5,
	}
}


def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	
	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')
		
def make_subsession_count(app_data,device_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 1
	else:
		app_data['subsession_count'] += 1
		
	return app_data.get('subsession_count')

def get_install_reciept(app_data):
	install_receipt = 'MIISlQYJKoZIhvcNAQcCoIIShjCCEoICAQExCzAJBgUrDgMCGgUAMIICNgYJKoZIhvcNAQcBoIICJwSCAiMxggIfMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgCNMA0CAQoCAQEEBRYDMTcrMA0CAQ0CAQEEBQIDAdWIMA4CAQECAQEEBgIEPqoEQjAOAgEJAgEBBAYCBFAyNTMwDgIBCwIBAQQGAgQHB7ZzMA4CARACAQEEBgIEMYGMBDAQAgEPAgEBBAgCBlO3L4SjrzAUAgEAAgEBBAwMClByb2R1Y3Rpb24wFAIBAwIBAQQMDAoyMDE5MDMwODAxMBQCARMCAQEEDAwKMjAxODA2MDgwMjAYAgEEAgECBBDutqzNY6hKA7WzvWhvS/e5MBwCAQICAQEEFAwSY29tLnRpcGljby5jbGFzc2ljMBwCAQUCAQEEFG14WtuKNQVYJ7VzqWfEV/Bi4K9AMB4CAQgCAQEEFhYUMjAxOS0wOS0xOFQwODozMDoxNVowHgIBDAIBAQQWFhQyMDE5LTA5LTE4VDA4OjMwOjE1WjAeAgESAgEBBBYWFDIwMTgtMDktMjZUMTE6MDg6MTlaMDYCAQcCAQEELsJCMIfDtYfNREN9jdppFudOOOV2fQPgN6sg3lfINa0IjVXksDPobBWb4KYLgtUwVgIBBgIBAQROGtDo1Tw/TJpV0kMgvXBRmHoBGaQS2ltzcfn/9rVRGEWyiiQlzXLC2sugb5PbkAZanovf07HGBxmWCjtmlqbxdwvvTLEXu0efocAweh4joIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQCQG/N0/ZN50bxcZvksTYYEF9x4wit/EJlXZ0ZqjSx8fFP5sARSIGGJtAl7j2NmTMD80Af+c2p1IP6+31kZHs9Lb2O7F/QLyan6pe9ZOn9NXsS00uK8MGL+sFGYlKWD+7q14I+e6lCGTv4ql4aDetMLAoX22TPgRv8baD9NSfP2MVTECg57FGTIqPy8IhYI9wywjQeBPVTMM9q1RNdcUCQ+DPFVdNgp+yBDnl8wrBsXsBh5ENgaCHHDdVAz+GSbAx2bBg+8qN0hYAmd9d3rPhbgzqqwTEFI5kseTK6V6/mtP3wAj0KlfIL4qf4ENwOTDD89OQevaSZdD8tyrPoK6rA0'
	return install_receipt

def install(app_data, device_data):

	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')

	time.sleep(random.randint(2,4))
	created_at_attribution= get_date(app_data,device_data)
	created_at_session= get_date(app_data,device_data)
	sent_at_session= get_date(app_data,device_data)
	time.sleep(random.randint(1,3))
	created_at_sdk= get_date(app_data,device_data) 
	sent_at_sdk= get_date(app_data,device_data)
	time.sleep(random.randint(60,100))
	sent_at_attribution= get_date(app_data,device_data)
	
	get_install_reciept(app_data)

	print '*************Install**********************'

	###########		Initialize		############
	
	app_data['adjust_call_time']=int(time.time())
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)

	if not app_data.get('make_dpi'):
		app_data['make_dpi'] = device_data.get('dpi') if device_data.get('dpi') else '326'

	set_sessionLength(app_data,forced=True,length=0)

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session)
	app_data['api_hit_time']=time.time()
	result = util.execute_request(**adjustSession)
	try: 
		app_data['adid'] = json.loads(result.get('data')).get('adid')
		if not app_data.get('adid'):
			app_data['adid'] = 'f919aeedf7414679d7f166a3854be3e2'

		print 'ok response for adid'
	except:
		print 'Exception for adid'
		app_data['adid'] = 'f919aeedf7414679d7f166a3854be3e2'

	
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data,created_at=created_at_sdk,sent_at=sent_at_sdk)
	util.execute_request(**adjustSession)

	time.sleep(random.randint(10,30))

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,created_at=created_at_attribution,sent_at=sent_at_attribution)
	util.execute_request(**adjustSession)
	

	if not app_data.get('registration') and random.randint(1,100) <= 65:

		time.sleep(random.randint(60,120))
		print "\nAdjust event \n"
		request=s2s_adjust_event( campaign_data, device_data, app_data, event_token = "z5wk44" )
		util.execute_request(**request)

		time.sleep(random.randint(1,4))
		print "\nAdjust event \n"
		callback_params = json.dumps({"reguuid": str(uuid.uuid4()), "cparam2":"THIS_SLOT_CAN_BE_USED_TO_PASS_DEPOSIT_VALUE"})
		request=s2s_adjust_event( campaign_data, device_data, app_data, event_token = "bbhi4q", callback_params = callback_params )
		util.execute_request(**request)
		app_data['registration'] = True


	time.sleep(random.randint(1*60,10*60))
	
	set_appCloseTime(app_data)
		
	return {'status':True}


###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):

	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')

	time.sleep(random.randint(2,4))
	created_at_attribution= get_date(app_data,device_data)
	created_at_session= get_date(app_data,device_data)
	sent_at_session= get_date(app_data,device_data)
	time.sleep(random.randint(1,3))
	sent_at_attribution= get_date(app_data,device_data)
	
	get_install_reciept(app_data)

	###########		Initialize		############
	
	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())

	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)

	if not app_data.get('make_dpi'):
		app_data['make_dpi'] = device_data.get('dpi') if device_data.get('dpi') else '326'


	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session, type='open')
	result = util.execute_request(**adjustSession)
	try: 
		app_data['adid'] = json.loads(result.get('data')).get('adid')
		if not app_data.get('adid'):
			app_data['adid'] = 'f919aeedf7414679d7f166a3854be3e2'

		print 'ok response for adid'
	except:
		print 'Exception for adid'
		app_data['adid'] = 'f919aeedf7414679d7f166a3854be3e2'

	app_data['adjust_call_time']=int(time.time())


	if not app_data.get('registration') and random.randint(1,100) <= 65:

		time.sleep(random.randint(60,120))
		print "\nAdjust event \n"
		request=s2s_adjust_event( campaign_data, device_data, app_data, event_token = "z5wk44" )
		util.execute_request(**request)

		time.sleep(random.randint(1,4))
		print "\nAdjust event \n"
		callback_params = json.dumps({"reguuid": str(uuid.uuid4()), "cparam2":"THIS_SLOT_CAN_BE_USED_TO_PASS_DEPOSIT_VALUE"})
		request=s2s_adjust_event( campaign_data, device_data, app_data, event_token = "bbhi4q", callback_params = callback_params )
		util.execute_request(**request)
		app_data['registration'] = True


	time.sleep(random.randint(2*60,10*60))

	set_appCloseTime(app_data)

	return {'status':True}



###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,created_at,sent_at,type='install'):
	pushtoken(app_data)
	app_data['subsession_count'] = 0
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': created_at,
			"connectivity_type" :	"2",
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'sent_at': sent_at,
			'session_count': make_session_count(app_data,device_data),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
			'os_build':	device_data.get('build'),
			'install_receipt': get_install_reciept(app_data),
			}
	if type == "open":		
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = app_data.get('appCloseTime') - app_data.get('adjust_call_time')
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = app_data.get('appCloseTime') - app_data.get('adjust_call_time')

	#headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')
		
	app_data['time_passes'] = int(time.time())

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	



def adjust_sdkclick(campaign_data, app_data, device_data,created_at,sent_at,callback_params=False,partner_params=False):
	pushtoken(app_data)
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	
	params={}

	data = {
			'details': '{"Version3.1":{"iad-attribution":"false"}}',
			'source': 'iad3',
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': created_at,
			'connectivity_type' : 	'2',
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'sent_at': sent_at,
			'session_count': app_data.get('session_count'),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
			'os_build':	device_data.get('build'),
			'last_interval': 2,
			'session_length': int(time.time()) - app_data.get('adjust_call_time'),
			'subsession_count': 1,
			'time_spent' : int(time.time()) - app_data.get('adjust_call_time'),
			'install_receipt': get_install_reciept(app_data),
			}

	if callback_params:
		data['callback_params']=callback_params
	if partner_params:
		data['partner_params']=partner_params
	
	#headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'click')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	

def adjust_attribution(campaign_data, app_data, device_data,created_at,sent_at):
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	
	data={}
	
	params = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink' : 1,
			'created_at': created_at,
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'needs_response_details': 1,
			'sent_at': sent_at,
			'initiated_by' :	'backend',
			}

	#headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('idfa'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		


def s2s_adjust_event( campaign_data, device_data, app_data, event_token, callback_params = "" ):
	url= "http://s2s.adjust.com/event"
	method= "post"
	headers= {       "Accept": "application/json, text/javascript, */*; q=0.01",
        "Accept-Encoding": "br, gzip, deflate",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "Origin": "https://m.tipico.de",
        "Referer": "https://m.tipico.de/",
        "User-Agent": device_data.get('User-Agent')}

	params= {       "adid": app_data.get('adid'),
        "app_token": campaign_data.get('adjust').get('app_token'),
        "event_token": event_token,
        "idfa": app_data.get('idfa_id'),
        "idfv": app_data.get('idfv_id'),
        "s2s": "1"}

	if callback_params:
		params['callback_params'] = callback_params

	if event_token == 'bbhi4q':
		params['created_at'] = int(time.time())

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}



def adjust_sdkinfo(campaign_data, app_data, device_data):
	pushtoken(app_data)
	url = 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent':urllib.pathname2url(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}

	params={}

	data = {

		'app_token': campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink' : 1,
# 'created_at': datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		'created_at': get_date(app_data,device_data),
		'environment' : 'production',
		'event_buffering_enabled': 0,
		'idfa': app_data.get('idfa_id'),
		'idfv':	app_data.get('idfv_id'),
		#'initiated_by':'backend',
		'needs_response_details': 1,
# 'sent_at': datetime.datetime.utcfromtimestamp(time.time()+random.uniform(0,1)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		'sent_at': get_date(app_data,device_data),
		'persistent_ios_uuid': app_data.get('ios_uuid'),
		'push_token':app_data.get('pushtoken'),
		'source' :'push'
	}
	headers['Authorization']=headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###########################################################
#						UTIL							  #
###########################################################

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

	
def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('hex',64)

def device_id(app_data):
	if not app_data.get('x-device-id'):
		app_data['x-device-id']=str(uuid.uuid4()).upper()

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['phone'] 		="+971"+'-50-'+util.get_random_string('decimal',7)
		app_data['user']['password'] 	= app_data['user']['lastname']+random.choice(["@","!"])+app_data['user']['dob'].replace("-","") 

def get_auth(device_data,app_data,created_at,idfa,activity_type):
	data1= str(campaign_data['adjust']['app_secret']+activity_type+idfa+created_at)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('signature_id')+'",signature="'+sign+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'	

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date