# -*- coding: utf-8 -*-
from sdk import util, installtimenew, purchase
from sdk import getsleep
from sdk import NameLists
import uuid
import time
import random
import json
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 : 'com.planetart.fpuk',
	'app_name' 			 : 'FreePrints',
	'app_version_name'	 : '2.48.1',#'2.48.0',#'2.47.1',#'2.46.3',#'2.46.0',#'2.43.0',
	'app_version_code'	 : '24884',#'23241',
	'no_referrer'		 : False,
	'device_targeting'   : True,
	'app_size'			 : 61.0,#60.0,#58.0,
	'supported_os'		 : '5.0',#'4.4',		
	'supported_countries': 'WW',
	'ctr'				 : 6,
	'tracker'			 : 'branch',
	'api_branch':{
	    'branchkey'  :'key_live_kok9uO4avLoN7vmPzDnVTjnpxyogQHkn',
	    'sdk'        :'android3.0.3',
	    'sdk_version'        :'3.0.3'  
	},

	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	print '______Please Wait__________________'
	installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android",min_sleep=0)
	###########		INITIALIZE		############

	time.sleep(random.randint(1,2))
	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	app_data['api_hit_time']=time.time()
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'

	time.sleep(random.randint(1,2))
	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)

	if random.randint(1,100)<=70:	

		transaction_id = "FP"+str(random.randint(227162000000,227162999999))

		time.sleep(random.randint(40,60))
		print "Branch______event"
		event_data={"currency":"GBP","description":"initiate purchase","shipping":0,"tax":0,"revenue":0.01,"search_query":transaction_id,"transaction_id":transaction_id}
		req=api_branch_event(campaign_data, app_data, device_data, event_name='INITIATE_PURCHASE',event_data=event_data)
		util.execute_request(**req)

		time.sleep(random.randint(1,2))
		print "Branch______event"
		event_data={"currency":"GBP","description":"purchase","shipping":0,"tax":0,"revenue":0.01,"search_query":transaction_id,"transaction_id":transaction_id}
		req=api_branch_event(campaign_data, app_data, device_data, event_name='PURCHASE',event_data=event_data)
		util.execute_request(**req)

		app_data['first_purchase'] = True

	for _ in range(random.randint(1,3)):

		time.sleep(random.randint(1,2))
		print "Branch______sdk"
		request=branch_call(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(20,60))
		print "Branch______close"
		request=api_branch_close(campaign_data, app_data, device_data,call_seq=random.randint(1,3))
		util.execute_request(**request)

		time.sleep(random.randint(8,12))
		print "Branch______open"
		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)

	if app_data.get('first_purchase'):
		if purchase.isPurchase(app_data,day=1,advertiser_demand=5):	


			print "\nself_call_fp_freeprintsapp_co_uk\n"
			request=fp_freeprintsapp_co_uk( campaign_data, device_data, app_data )
			response=util.execute_request(**request)
			global products_list,shipping_price
			products_list = {}
			try:
				result = json.loads(response.get('data')).get('preferences')
				products = result.get('priceTable').keys()
				for item in products:
					if products_list.get(item.split("_")[0]):
						products_list[item.split("_")[0]].append(result.get('priceTable').get(item).get('items')[0].get('price'))	
					else:
						products_list[item.split("_")[0]] = list()
						products_list[item.split("_")[0]].append(result.get('priceTable').get(item).get('items')[0].get('price'))	

				shipping_price = result.get('shippingTier').get('std').values()				

				if not products_list:
					products_list = {'canvas': [12.99, 19.99, 12.99, 7.99, 7.99, 21.99, 21.99, 21.99, 16.99, 14, 33, 30.99, 19, 19, 30.99, 42, 23, 12.99, 21.99, 19, 33, 21.99, 9, 21.99, 0, 19, 30.99, 19, 42, 19, 21.99, 16.99, 21.99, 24, 19.99, 7.99, 30.99, 28, 7.99, 16.99, 33, 24, 5, 16.99, 28, 12.99, 14, 33], 'easel': [12.99, 12.99, 12, 12.99, 12, 12.99, 12.99, 12.99], 'tote': [18, 18, 16, 16], 'mug': [8, 8, 7.99, 7.99, 9, 9, 9, 9, 9, 8, 10, 9, 7.99, 9, 9, 10, 8], 'frame': [9.99, 7.99, 9.99, 9.99, 7.99, 9.99, 9.99, 9.99, 9.99, 9.99], 'wallframe': [24, 19.99, 19.99, 19.99, 24, 19.99, 24, 24, 19.99, 19.99, 19.99, 19.99, 24, 24, 24, 24], 'magnet': [2.3333, 1.6667, 2.3333, 1.6667, 1.6667, 1.6667, 2.3333, 1.6667, 1.6667, 2.3333], 'tile': [0, 0, 0, 0], 'tshirt': [17, 15, 17, 15, 15, 17, 15, 15, 15, 17, 17, 17, 15, 17, 15, 15, 17, 15, 17, 17, 15, 17, 15, 17, 15, 17, 17, 15, 17, 17, 17, 15, 17, 17, 17, 17, 15, 15, 15, 15, 17, 15, 15, 15, 15, 17, 15, 17, 15, 17, 15, 17, 17, 17, 15, 15, 15, 17, 15, 17], 'giftbox': [2, 1.99, 2, 2, 2, 1.99], 'print': [14, 2.99, 16.99, 0.09, 19.59, 12, 0.39, 5.59, 0.96, 1.19, 0.12, 14, 12, 0.19, 0.17], 'android': [1], 'pillow': [7, 26, 12, 10, 26, 10, 19, 19.99, 19.99, 19, 19, 12, 0, 19]}	
					
				if not shipping_price:
					shipping_price = [1.49, 1.99, 2.69, 2.99, 3.49, 3.99]		
			except:			
				products_list = {'canvas': [12.99, 19.99, 12.99, 7.99, 7.99, 21.99, 21.99, 21.99, 16.99, 14, 33, 30.99, 19, 19, 30.99, 42, 23, 12.99, 21.99, 19, 33, 21.99, 9, 21.99, 0, 19, 30.99, 19, 42, 19, 21.99, 16.99, 21.99, 24, 19.99, 7.99, 30.99, 28, 7.99, 16.99, 33, 24, 5, 16.99, 28, 12.99, 14, 33], 'easel': [12.99, 12.99, 12, 12.99, 12, 12.99, 12.99, 12.99], 'tote': [18, 18, 16, 16], 'mug': [8, 8, 7.99, 7.99, 9, 9, 9, 9, 9, 8, 10, 9, 7.99, 9, 9, 10, 8], 'frame': [9.99, 7.99, 9.99, 9.99, 7.99, 9.99, 9.99, 9.99, 9.99, 9.99], 'wallframe': [24, 19.99, 19.99, 19.99, 24, 19.99, 24, 24, 19.99, 19.99, 19.99, 19.99, 24, 24, 24, 24], 'magnet': [2.3333, 1.6667, 2.3333, 1.6667, 1.6667, 1.6667, 2.3333, 1.6667, 1.6667, 2.3333], 'tile': [0, 0, 0, 0], 'tshirt': [17, 15, 17, 15, 15, 17, 15, 15, 15, 17, 17, 17, 15, 17, 15, 15, 17, 15, 17, 17, 15, 17, 15, 17, 15, 17, 17, 15, 17, 17, 17, 15, 17, 17, 17, 17, 15, 15, 15, 15, 17, 15, 15, 15, 15, 17, 15, 17, 15, 17, 15, 17, 17, 17, 15, 15, 15, 17, 15, 17], 'giftbox': [2, 1.99, 2, 2, 2, 1.99], 'print': [14, 2.99, 16.99, 0.09, 19.59, 12, 0.39, 5.59, 0.96, 1.19, 0.12, 14, 12, 0.19, 0.17], 'android': [1], 'pillow': [7, 26, 12, 10, 26, 10, 19, 19.99, 19.99, 19, 19, 12, 0, 19]}	

				shipping_price = [1.49, 1.99, 2.69, 2.99, 3.49, 3.99]


			transaction_id = "FP"+str(random.randint(227162000000,227162999999))
			price = eval(str(random.choice(products_list.get('print'))))
			shipping = eval(str(random.choice(shipping_price)))+eval("0.00000000"+str(random.randint(95000000,95999999)))
			revenue = price+shipping

			time.sleep(random.randint(40,60))
			print "Branch______event"
			event_data={"currency":"GBP","description":"initiate purchase","shipping":shipping,"tax":0,"revenue":revenue,"search_query":transaction_id,"transaction_id":transaction_id}
			req=api_branch_event(campaign_data, app_data, device_data, event_name='INITIATE_PURCHASE',event_data=event_data)
			util.execute_request(**req)

			time.sleep(random.randint(1,2))
			print "Branch______event"
			event_data={"currency":"GBP","description":"purchase","shipping":shipping,"tax":0,"revenue":revenue,"search_query":transaction_id,"transaction_id":transaction_id}
			req=api_branch_event(campaign_data, app_data, device_data, event_name='PURCHASE',event_data=event_data)
			util.execute_request(**req)

	###########		CALLS		################
		
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android",min_sleep=0)

	if not app_data.get('device_fingerprint_id1'):	
		time.sleep(random.randint(1,2))
		print "Branch______install"
		request=api_branch_install(campaign_data,app_data,device_data)
		result=util.execute_request(**request)
		try:
			json_result=json.loads(result.get('data'))
			app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
			app_data['identity_id']=json_result.get('identity_id')
			app_data['session_id']=json_result.get('session_id')
		except:
			app_data['device_fingerprint_id1']='613284344550945604'
			app_data['identity_id']='641581897171978611'
			app_data['session_id']='641581897189090110'	
	else:
		print "Branch______open"
		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)	

	time.sleep(random.randint(1,2))
	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)

	if random.randint(1,100)<=70 and not app_data.get('first_purchase'):	

		transaction_id = "FP"+str(random.randint(227162000000,227162999999))

		time.sleep(random.randint(40,60))
		print "Branch______event"
		event_data={"currency":"GBP","description":"initiate purchase","shipping":0,"tax":0,"revenue":0.01,"search_query":transaction_id,"transaction_id":transaction_id}
		req=api_branch_event(campaign_data, app_data, device_data, event_name='INITIATE_PURCHASE',event_data=event_data)
		util.execute_request(**req)

		time.sleep(random.randint(1,2))
		print "Branch______event"
		event_data={"currency":"GBP","description":"purchase","shipping":0,"tax":0,"revenue":0.01,"search_query":transaction_id,"transaction_id":transaction_id}
		req=api_branch_event(campaign_data, app_data, device_data, event_name='PURCHASE',event_data=event_data)
		util.execute_request(**req)

		app_data['first_purchase'] = True

	for _ in range(random.randint(1,3)):

		time.sleep(random.randint(1,2))
		print "Branch______sdk"
		request=branch_call(campaign_data, app_data, device_data)
		util.execute_request(**request)

		time.sleep(random.randint(20,60))
		print "Branch______close"
		request=api_branch_close(campaign_data, app_data, device_data,call_seq=random.randint(1,3))
		util.execute_request(**request)

		time.sleep(random.randint(8,12))
		print "Branch______open"
		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)

	if app_data.get('first_purchase'):
		if purchase.isPurchase(app_data,day,advertiser_demand=15):	


			print "\nself_call_fp_freeprintsapp_co_uk\n"
			request=fp_freeprintsapp_co_uk( campaign_data, device_data, app_data )
			response=util.execute_request(**request)
			global products_list,shipping_price
			products_list = {}
			try:
				result = json.loads(response.get('data')).get('preferences')
				products = result.get('priceTable').keys()
				for item in products:
					if products_list.get(item.split("_")[0]):
						products_list[item.split("_")[0]].append(result.get('priceTable').get(item).get('items')[0].get('price'))	
					else:
						products_list[item.split("_")[0]] = list()
						products_list[item.split("_")[0]].append(result.get('priceTable').get(item).get('items')[0].get('price'))	

				shipping_price = result.get('shippingTier').get('std').values()				

				if not products_list:
					products_list = {'canvas': [12.99, 19.99, 12.99, 7.99, 7.99, 21.99, 21.99, 21.99, 16.99, 14, 33, 30.99, 19, 19, 30.99, 42, 23, 12.99, 21.99, 19, 33, 21.99, 9, 21.99, 0, 19, 30.99, 19, 42, 19, 21.99, 16.99, 21.99, 24, 19.99, 7.99, 30.99, 28, 7.99, 16.99, 33, 24, 5, 16.99, 28, 12.99, 14, 33], 'easel': [12.99, 12.99, 12, 12.99, 12, 12.99, 12.99, 12.99], 'tote': [18, 18, 16, 16], 'mug': [8, 8, 7.99, 7.99, 9, 9, 9, 9, 9, 8, 10, 9, 7.99, 9, 9, 10, 8], 'frame': [9.99, 7.99, 9.99, 9.99, 7.99, 9.99, 9.99, 9.99, 9.99, 9.99], 'wallframe': [24, 19.99, 19.99, 19.99, 24, 19.99, 24, 24, 19.99, 19.99, 19.99, 19.99, 24, 24, 24, 24], 'magnet': [2.3333, 1.6667, 2.3333, 1.6667, 1.6667, 1.6667, 2.3333, 1.6667, 1.6667, 2.3333], 'tile': [0, 0, 0, 0], 'tshirt': [17, 15, 17, 15, 15, 17, 15, 15, 15, 17, 17, 17, 15, 17, 15, 15, 17, 15, 17, 17, 15, 17, 15, 17, 15, 17, 17, 15, 17, 17, 17, 15, 17, 17, 17, 17, 15, 15, 15, 15, 17, 15, 15, 15, 15, 17, 15, 17, 15, 17, 15, 17, 17, 17, 15, 15, 15, 17, 15, 17], 'giftbox': [2, 1.99, 2, 2, 2, 1.99], 'print': [14, 2.99, 16.99, 0.09, 19.59, 12, 0.39, 5.59, 0.96, 1.19, 0.12, 14, 12, 0.19, 0.17], 'android': [1], 'pillow': [7, 26, 12, 10, 26, 10, 19, 19.99, 19.99, 19, 19, 12, 0, 19]}	
					
				if not shipping_price:
					shipping_price = [1.49, 1.99, 2.69, 2.99, 3.49, 3.99]		
			except:			
				products_list = {'canvas': [12.99, 19.99, 12.99, 7.99, 7.99, 21.99, 21.99, 21.99, 16.99, 14, 33, 30.99, 19, 19, 30.99, 42, 23, 12.99, 21.99, 19, 33, 21.99, 9, 21.99, 0, 19, 30.99, 19, 42, 19, 21.99, 16.99, 21.99, 24, 19.99, 7.99, 30.99, 28, 7.99, 16.99, 33, 24, 5, 16.99, 28, 12.99, 14, 33], 'easel': [12.99, 12.99, 12, 12.99, 12, 12.99, 12.99, 12.99], 'tote': [18, 18, 16, 16], 'mug': [8, 8, 7.99, 7.99, 9, 9, 9, 9, 9, 8, 10, 9, 7.99, 9, 9, 10, 8], 'frame': [9.99, 7.99, 9.99, 9.99, 7.99, 9.99, 9.99, 9.99, 9.99, 9.99], 'wallframe': [24, 19.99, 19.99, 19.99, 24, 19.99, 24, 24, 19.99, 19.99, 19.99, 19.99, 24, 24, 24, 24], 'magnet': [2.3333, 1.6667, 2.3333, 1.6667, 1.6667, 1.6667, 2.3333, 1.6667, 1.6667, 2.3333], 'tile': [0, 0, 0, 0], 'tshirt': [17, 15, 17, 15, 15, 17, 15, 15, 15, 17, 17, 17, 15, 17, 15, 15, 17, 15, 17, 17, 15, 17, 15, 17, 15, 17, 17, 15, 17, 17, 17, 15, 17, 17, 17, 17, 15, 15, 15, 15, 17, 15, 15, 15, 15, 17, 15, 17, 15, 17, 15, 17, 17, 17, 15, 15, 15, 17, 15, 17], 'giftbox': [2, 1.99, 2, 2, 2, 1.99], 'print': [14, 2.99, 16.99, 0.09, 19.59, 12, 0.39, 5.59, 0.96, 1.19, 0.12, 14, 12, 0.19, 0.17], 'android': [1], 'pillow': [7, 26, 12, 10, 26, 10, 19, 19.99, 19.99, 19, 19, 12, 0, 19]}	

				shipping_price = [1.49, 1.99, 2.69, 2.99, 3.49, 3.99]
			

			transaction_id = "FP"+str(random.randint(227162000000,227162999999))
			price = eval(str(random.choice(products_list.get('print'))))
			shipping = eval(str(random.choice(shipping_price)))+eval("0.00000000"+str(random.randint(95000000,95999999)))
			revenue = price+shipping

			time.sleep(random.randint(40,60))
			print "Branch______event"
			event_data={"currency":"GBP","description":"initiate purchase","shipping":shipping,"tax":0,"revenue":revenue,"search_query":transaction_id,"transaction_id":transaction_id}
			req=api_branch_event(campaign_data, app_data, device_data, event_name='INITIATE_PURCHASE',event_data=event_data)
			util.execute_request(**req)

			time.sleep(random.randint(1,2))
			print "Branch______event"
			event_data={"currency":"GBP","description":"purchase","shipping":shipping,"tax":0,"revenue":revenue,"search_query":transaction_id,"transaction_id":transaction_id}
			req=api_branch_event(campaign_data, app_data, device_data, event_name='PURCHASE',event_data=event_data)
			util.execute_request(**req)	
	
		
	return {'status':'true'}

###################################################################

def fp_freeprintsapp_co_uk( campaign_data, device_data, app_data ):
	url= "http://fp.freeprintsapp.co.uk/api/"
	method= "post"
	headers= {       
		"Accept-Encoding": "gzip",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "FREEPRINT Android/"+campaign_data.get('app_version_name')
        }

	params= {       
		"_app": "Android-FPUK-"+campaign_data.get('app_version_name'),
        "_device": str(uuid.uuid4()).upper(),
        "_full_version": campaign_data.get('app_version_name')+"."+campaign_data.get('app_version_code'),
        "_language": "en-GB",
        "_method": "app.launch",
        "_screen": "WelcomeViewController"
        }

	data= {       
		"client_params": json.dumps({"device_pay_ability":"1","first_launch":"1","has_uninstalled_apps":"1","push_notif_onoff":"on"}),
        "experiment_names": json.dumps(["show_device_pay","fp_your_world","first_launch_1908_ab","new_checkout_ui","new_ui_2_30_6","mcrib_exp_1902","mcrib_new_1902","deluxe_exp_1904","deluxe_new_1904","shamrock_exp_1903","shamrock_new_1903","mccard_exp_1904","mccard_father_1906","mctiles_1907","mcink_1907","mcbooks_1907","mcblanket_1907","mcblanket_new_1907","money_back_guarantee","home_create_experiment"]),
        "multiple_data": json.dumps({"preferences":{"refresh":"true"}})
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	


###################################################################
################ BRANCH CALLS ###################

def branch_call(campaign_data, app_data, device_data):
	url='http://cdn.branch.io/sdk/uriskiplist_v2.json'
	method='get'
	headers={
		'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
	}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': None}


def api_branch_install(campaign_data,app_data,device_data):
	method='post'
	url='http://api.branch.io/v1/install'

	headers = {
				'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
				'Content-Type':'application/json',
				'Accept':'application/json',
			}

	data={"app_version":campaign_data.get('app_version_name'),
		"facebook_app_link_checked":False,
		"is_referrable":1,
		"update":0,
		"debug":False,
		"metadata":{},
		"hardware_id":device_data.get('android_id'),
		# 'clicked_referrer_ts':int(app_data.get('times').get('click_time')),
		# 'install_begin_ts':int(app_data.get('times').get('download_begin_time')),
		"is_hardware_id_real":True,
		"brand":device_data.get('brand').upper(),
		"model":device_data.get('model'),
		"screen_dpi":int(device_data.get('dpi')),
		"screen_height":int(device_data.get('resolution').split('x')[0]),
		"screen_width":int(device_data.get('resolution').split('x')[1]),
		"wifi":True,
		"os":"Android",
		"os_version":int(device_data.get('sdk')),
		"country":device_data.get('locale').get('country'),
		"language":device_data.get('locale').get('language'),
		"local_ip":device_data.get('private_ip'),
		"cd":{"mv":"-1","pn":campaign_data.get('package_name')},
		"google_advertising_id":device_data.get('adid'),
		"lat_val":0,
		"instrumentation":{"v1/install-qwt":"0"},
		"sdk":campaign_data.get('api_branch').get('sdk'),
		"retryNumber":0,
		"branch_key":campaign_data.get('api_branch').get('branchkey'),
		'environment':'FULL_APP',
		'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
		'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'ui_mode':'UI_MODE_TYPE_NORMAL',
		'previous_update_time':0
		} 

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}


def api_branch_open(campaign_data, app_data, device_data, call_seq=1):	
	
	url='http://api.branch.io/v1/open'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"app_version": campaign_data.get('app_version_name'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand').upper(),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			"country":device_data.get('locale').get('country'),
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/close-brtt":"1065","v1/open-qwt":"0"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
			"cd":{'mv':'-1','pn':campaign_data.get('package_name')},
			"debug" :False,
			"environment" :'FULL_APP',
			"facebook_app_link_checked":False,
			"google_advertising_id":device_data.get('adid'),
			"is_referrable":1,
			"lat_val":0,
			"update":1,
			'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
			'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
			'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
			'previous_update_time':int(app_data.get('times').get('install_complete_time')*1000)
	}
	if call_seq==2:
		data['instrumentation']={"v1/close-brtt":"891","v1/open-qwt":"0"}
	if call_seq==3:
		data['instrumentation']={"v1/close-brtt":"681","v1/open-qwt":"0"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}


def api_branch_close(campaign_data, app_data, device_data,call_seq=1):	
	
	url='http://api.branch.io/v1/close'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"session_id":app_data.get('session_id'),
			"app_version": campaign_data.get('app_version_name'),
			# 'google_advertising_id': device_data.get('adid'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand').upper(),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			"country":device_data.get('locale').get('country'),
			# "lat_val":0,
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/close-qwt":"2","v1/install-brtt":"648"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
	}
	if call_seq==2:
		data['instrumentation']={"v1/close-qwt":"10","v1/open-brtt":"692"}
	if call_seq==3:
		data['instrumentation']={"v1/close-qwt":"1","v1/open-brtt":"507"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def api_branch_event(campaign_data, app_data, device_data, event_name='',event_data=''):	
	
	url='http://api2.branch.io/v2/event/standard'
	header={
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={"name":event_name,
		"event_data":event_data,
		"user_data":{"android_id":device_data.get('android_id'),
		"brand":device_data.get('brand').upper(),
		"model":device_data.get('model'),
		"screen_dpi":device_data.get('dpi'),
		"screen_height":device_data.get('resolution').split('x')[0],
		"screen_width":device_data.get('resolution').split('x')[1],
		"os":"Android",
		"os_version":device_data.get('sdk'),
		"country":device_data.get('locale').get('country'),
		"language":device_data.get('locale').get('language'),
		"local_ip":device_data.get('private_ip'),
		"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
		"app_version":campaign_data.get('app_version_name'),
		"sdk":"android",
		"sdk_version":campaign_data.get('api_branch').get('sdk_version'),
		"user_agent":device_data.get('User-Agent'),
		"environment":"FULL_APP",
		"developer_identity":"bnc_no_value",
		"aaid":device_data.get('adid'),
		"limit_ad_tracking":0},
		"metadata":{},
		"instrumentation":{"v2/event/standard-qwt":"1",
		"v1/install-brtt":"883"},
		"branch_key":campaign_data.get('api_branch').get('branchkey'),
		"retryNumber":0
		}
		
	if event_name=='PURCHASE':
		data["custom_data"]={"is_first_order":"true"}
		data['instrumentation']={"v2/event/standard-brtt":"1557","v2/event/standard-qwt":"1571"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def connectivity_check(campaign_data, app_data, device_data):
	url = 'http://connectivitycheck.gstatic.com/generate_204'
	method = 'head'
	headers = {
		'Accept-Encoding': 'gzip',
		'User-Agent': device_data.get('User-Agent'),
	}
	params=None
	data = None
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def measurement_config(campaign_data, app_data, device_data):

	url = 'http://app-measurement.com/config/app/1%3A661240310080%3Aandroid%3A0ad3d352a734fdec'
	
	header={
		'User-Agent': get_ua(device_data),
		'Accept-Encoding': 'gzip',
	}
		
	params={
		'app_instance_id':	'ff70eed7ba918a419f66873cebbbca8d',
		'platform':	'android',
		'gmp_version':	'18382',
	}
	data=None

	return {'url': url, 'httpmethod': 'head', 'headers': header, 'params': params, 'data': data}
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)


def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

# def def_sec(app_data,device_data):
# 	timez = device_data.get('timezone')
# 	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
# 	if int(timez) < 0:
# 		sec = (-1) * sec
# 	if not app_data.get('sec'):
# 		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"
# 		app_data['user']['pswd']		= last_name+app_data['user']['dob'].replace('-','')



