# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import installtimenew
from sdk import getsleep
from sdk import util
from sdk import NameLists
import time,uuid
import random
import json
import string
import datetime
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.techbins.niki',
	'app_size'			 : 9.1,
	'app_name' 			 :'Niki: Online Recharge, Utility Bill Payment & more',
	'app_version_name' 	 :'2.58',#'2.57',
	'app_version_code' 	 :'99',#'98',
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : '9dec8cc9-6f64-4f91-8388-beeff231f84f',
		'dkh'		 : '9dec8cc9',
		'buildnumber': '6.2',
		'version'	 : 'v4',
	},
	'api_branch':{
                'branchkey'  :'key_live_eopTW35ElfSrcQNKg947cbnfwBjdzNm7',
                'sdk'        :'android2.12.1',#'android3.0.2' 
            },
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	

	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	def_sec(app_data,device_data)

	################generating_realtime_differences##################

	def_firstLaunchDate(app_data,device_data)

 	###########	 CALLS		 ############	

	print "\nBranch install \n"
	req = api_branch_install(campaign_data,app_data,device_data)
	app_data['api_hit_time']=time.time()
	result = util.execute_request(**req)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'

	time.sleep(random.randint(60,100))

	app_data['user_id'] = str(uuid.uuid4())

	if random.randint(1,100) <= 95:

		time.sleep(random.randint(30,60))

		print "\nBranch profile \n"
		req = api2_branch_profile( campaign_data, device_data, app_data, identity=app_data.get('user_id'))
		util.execute_request(**req)

		print "\nBranch event \n"
		req = api_branch_event(campaign_data, app_data, device_data,'Sign up')
		util.execute_request(**req)

		sign_up(campaign_data, app_data, device_data)

		if random.randint(1,100) <= 70:
			app_data['reg_lang'] = 'eng'
			sign_up_english(campaign_data, app_data, device_data)
		else:
			app_data['reg_lang'] = 'hindi'
			sign_up_hindi(campaign_data, app_data, device_data)
	
		print '\n'+'Appsflyer : Track____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
		util.execute_request(**request)

		app_data['registration'] = True


		if random.randint(1,100) <= 50:

			time.sleep(random.randint(30,40))

			print '\n'+'Appsflyer : Stats____________________________________'
			request  = appsflyer_stats(campaign_data, app_data, device_data)
			util.execute_request(**request)
			
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)


		if random.randint(1,100) <= 30:

			print "\nApi branch url\n"
			request=api_branch_url( campaign_data, device_data, app_data )
			util.execute_request(**request)

			print "\nBranch register view\n"
			request=api_branch_register( campaign_data, device_data, app_data )
			util.execute_request(**request)


	print "\nbranch close\n"
	request=api_branch_close( campaign_data, device_data, app_data )
	util.execute_request(**request)

	print '\n'+'Appsflyer : Stats____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)


	if random.randint(1,100) <= 10:

		print "\nbranch open call\n"
		request=api_branch_open( campaign_data, device_data, app_data )
		util.execute_request(**request)

		print '\n'+'Appsflyer : Track____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data)
		util.execute_request(**request)


	return {'status':'true'}
	



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	
	###########		INITIALIZE		############	
	print "Please wait ..."

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)

	if not app_data.get('prev_event_name'):
		def_eventsRecords(app_data)

	if not app_data.get('user_id'):
		app_data['user_id'] = str(uuid.uuid4())

	def_sec(app_data,device_data)

	################generating_realtime_differences##################
	
	def_firstLaunchDate(app_data,device_data)

 	###########	 CALLS		 ############

 	if not app_data.get('device_fingerprint_id1') or not app_data.get('identity_id') or not app_data.get('session_id'):

 		print "\nBranch install \n"
		req = api_branch_install(campaign_data,app_data,device_data)
		result = util.execute_request(**req)
		try:
			json_result=json.loads(result.get('data'))
			app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
			app_data['identity_id']=json_result.get('identity_id')
			app_data['session_id']=json_result.get('session_id')
		except:
			app_data['device_fingerprint_id1']='613284344550945604'
			app_data['identity_id']='641581897171978611'
			app_data['session_id']='641581897189090110'


	print "\nbranch open call\n"
	request=api_branch_open( campaign_data, device_data, app_data )
	util.execute_request(**request)

	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)	


	if not app_data.get('registration'):

		time.sleep(random.randint(30,60))

		print "\nBranch profile \n"
		req = api2_branch_profile( campaign_data, device_data, app_data, identity=app_data.get('user_id'))
		util.execute_request(**req)

		print "\nBranch event \n"
		req = api_branch_event(campaign_data, app_data, device_data,'Sign up')
		util.execute_request(**req)

		sign_up(campaign_data, app_data, device_data)

		if random.randint(1,100) <= 70:
			sign_up_english(campaign_data, app_data, device_data)
			app_data['reg_lang'] = 'eng'
		else:
			app_data['reg_lang'] = 'hindi'
			sign_up_hindi(campaign_data, app_data, device_data)
	
		print '\n'+'Appsflyer : Track____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
		util.execute_request(**request)

		app_data['registration'] = True


	if random.randint(1,100) <= 30:

		time.sleep(random.randint(30,60))

		print "\nApi branch url\n"
		request=api_branch_url( campaign_data, device_data, app_data )
		util.execute_request(**request)

		print "\nBranch register view\n"
		request=api_branch_register( campaign_data, device_data, app_data )
		util.execute_request(**request)


	if random.randint(1,100) <= 5:

		time.sleep(random.randint(30,60))

		print "\nBranch profile \n"
		req = api2_branch_profile( campaign_data, device_data, app_data, identity=app_data.get('user_id'))
		util.execute_request(**req)

		print "\nBranch event \n"
		req = api_branch_event(campaign_data, app_data, device_data,'Login')
		util.execute_request(**req)

		login(campaign_data, app_data, device_data)

		if random.randint(1,100) <= 70:
			app_data['reg_lang'] = 'eng'
			login_english(campaign_data, app_data, device_data)
		else:
			app_data['reg_lang'] = 'hindi'
			login_hindi(campaign_data, app_data, device_data)
	
		print '\n'+'Appsflyer : Track____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data)
		util.execute_request(**request)


	time.sleep(random.randint(30,60))
	print "\nbranch close\n"
	request=api_branch_close( campaign_data, device_data, app_data )
	util.execute_request(**request)

	print '\n'+'Appsflyer : Stats____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)


	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def sign_up(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________sign_up'
	eventName			= 'sign_up'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def login(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________login'
	eventName			= 'login'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sign_up_english(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________sign_up_english'
	eventName			= 'sign_up_english'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def sign_up_hindi(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________sign_up_hindi'
	eventName			= 'sign_up_hindi'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def login_hindi(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________login_hindi'
	eventName			= 'login_hindi'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def login_english(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________login_english'
	eventName			= 'login_english'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
 	def_(app_data,'counter')
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"app_name" : campaign_data.get('app_name'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand').upper(),
		"carrier" : device_data.get('carrier'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"appUserId" : "17785142-a732-48d3-a636-ae5d539156f0",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceFingerPrintId" : app_data.get('deviceFingerPrintId'),
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"installer_package" : "com.android.vending",
		"isFirstCall" : "true",
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"originalAppsflyerId" : app_data.get('uid'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"reinstallCounter" : "2",
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),

		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))


	if app_data.get('reg_lang') == 'hindi':
		data['lang'] = "हिन्दी".encode('utf-8')
		data['lang_code'] = 'hi'
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_sdks" : "0001000000",
		"af_timestamp" : timestamp(),
		"appUserId" : "17785142-a732-48d3-a636-ae5d539156f0",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceFingerPrintId" : app_data.get('deviceFingerPrintId'),
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"originalAppsflyerId" : app_data.get('uid'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"reinstallCounter" : "3",
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),

		}			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('counter')>2:
		if data.get('rfr'):
			del data['rfr']
		if data.get('deviceData'):
			if data.get('deviceData').get('sensors'):
				del data['deviceData']['sensors']

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))

	if app_data.get('reg_lang') == 'hindi':
		data['lang'] = "हिन्दी".encode('utf-8')
		data['lang_code'] = 'hi'
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_stats()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Appsflyer's behaviour whenever user leaves app's context
# example : User drags notification bar,
#			Facebook SignUp page opens in app, etc
###################################################################
def appsflyer_stats(campaign_data, app_data, device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	inc_(app_data,'launchCounter')
	def_(app_data,'counter')
	method = "post"
	url = "https://stats.appsflyer.com/stats"
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"app_id" : campaign_data.get('package_name'),
		"channel" : None,
		"deviceFingerPrintId" : app_data.get('deviceFingerPrintId'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"gcd_conversion_data_timing" : 0,
		"launch_counter" : str(app_data.get('counter')),
		"originalAppsflyerId" : app_data.get('uid'),
		"platform" : "Android",
		"statType" : "user_closed_app",
		"time_in_app" : str(int(random.randint(0,2))),
		"uid" : app_data.get('uid'),

		}

	
	if app_data.get('time_in_app'):
		data['time_in_app']=str(int(time.time())-app_data['time_in_app'])
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

##################################################################
#
#					Branch
###################################################################

def api_branch_install(campaign_data,app_data,device_data):
    method='post'
    url='http://api.branch.io/v1/install'

    headers = {
                'User-Agent'        :get_ua(device_data),
                'Accept-Encoding': 'gzip',
                'Content-Type':'application/json',
                'Accept':'application/json',
            }

    data={       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "cd": {       "mv": "-1", "pn": campaign_data.get('package_name')},
        # "country": device_data.get('locale').get('country'),
        "debug": False,
        "environment": "FULL_APP",
        "facebook_app_link_checked": False,
        # "first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "instrumentation": {       "v1/install-qwt": "0"},
        "is_hardware_id_real": True,
        "is_referrable": 1,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        # "latest_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        # "latest_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        # "previous_update_time": 0,
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        # "ui_mode": "UI_MODE_TYPE_NORMAL",
        "update": 0,
        "wifi": True} 

    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}


def api_branch_close( campaign_data, device_data, app_data ):
	url= "http://api.branch.io/v1/close"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data)}

	params= None

	data= {       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       "v1/close-qwt": "1", "v1/open-brtt": "378"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "local_ip": device_data.get('private_ip'),
        "metadata": {},
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "wifi": True}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


def api_branch_open( campaign_data, device_data, app_data ):
	url= "http://api.branch.io/v1/open"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data)}

	params= None

	data= {       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "cd": {       "mv": "-1", "pn": campaign_data.get('package_name')},
        "debug": False,
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "environment": "FULL_APP",
        "facebook_app_link_checked": False,
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       "v1/open-qwt": "0"},
        "is_hardware_id_real": True,
        "is_referrable": 1,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {},
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "update": 1,
        "wifi": True}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}



def api_branch_register( campaign_data, device_data, app_data ):
	url= "http://api.branch.io/v1/register-view"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data)}

	params= None

	data= {       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "instrumentation": {       "v1/register-view-qwt": "1642",
                                   "v1/url-brtt": "351"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {"ref_code": util.get_random_string('hex',7)},
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "params": {       "$keywords": [], "$publicly_indexable": True},
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "wifi": True}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


def api_branch_url( campaign_data, device_data, app_data ):
	url= "http://api.branch.io/v1/url"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data)}

	params= None

	data= {       "alias": "",
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "campaign": "",
        "channel": "google_search",
        "data": '{"$og_title":"Niki","$canonical_identifier":"c851571377289205","$og_description":"Niki is your virtual friend to help you book cabs, order food, recharge, pay postpaid, electricity, datacard, DTH bills and much more simply via chatting. She is an Artificial Intelligence (AI) powered chatbot that wants to be your simplified personal assistant. Niki is free to use, and you pay only for the services used, and make the most out of the offers available. You will always get responses with lightning speed. \\\\n\\\\n\\\\n\\\\n  None Initiating a new prepaid conversation \xf0\x9f\x99\x8b Please enter your mobile number for recharge. 9654812351 Picking your number as <b> 9654812351 <\\\\/b> Which mobile network are you using ? vodafone Got it, <b> vodafone <\\\\/b> number Which state or circle does your mobile number belong to ? E.g.:Karnataka or Mumbai ncr Almost there! Tell me the amount for the recharge please? Browse Plans Please select plan type Topup please enter amount 10 Can you confirm the order details ?<br> Number: 9654812351 <br> Network: vodafone <br>State\\\\/Circle: delhi ncr <br>Plan: topup <br> Amount:<b> 10 <\\\\/b> Yes Final Payable amount including convenience fee charged by Niki due to surcharges charged by different payment options are as given below Perfect&#x1F60A;. Tap on below options to \"Pay Now\" Cheers. \xf0\x9f\x98\x8a I have received the payment. Your order will be processed soon The recharge is in process, I will let you know soon as it completes or initiate refund after 48 hours!Your credits used will be added back as it is.","$og_image_url":"https:\\\\/\\\\/static.niki.ai\\\\/img\\\\/logo.png","$publicly_indexable":"True","msg":"Niki","source":"android"}',
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "feature": "Share",
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "local_ip": device_data.get('private_ip'),
        "metadata": {},
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi":  int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "stage": "",
        "tags": [],
        "wifi": True}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


def api2_branch_profile( campaign_data, device_data, app_data, identity=''):
    url= "http://api.branch.io/v1/profile"
    method= "post"
    headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data)}

    params= None

    data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        # "country": device_data.get('country'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        # "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity": identity,
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {"v1/profile-qwt":"1","v1/install-brtt":"6899"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        # "lat_val":"0",
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        # "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}

    # if call == 2:
    #     data['instrumentation'] = {"v1\/profile-qwt":"1","v1\/install-brtt":"6899"}

    return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}



def api_branch_event(campaign_data, app_data, device_data,event_name):	
	
	url='http://api.branch.io/v1/event'
	header={
		# 'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"session_id":app_data.get('session_id'),
			# "app_version": campaign_data.get('app_version_name'),
			'environment' :'FULL_APP',
			'event'       :event_name,
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand'),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			# "ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": 'Android',
			"os_version": int(device_data.get('sdk')),		
			# "country":device_data.get('locale').get('country'),
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			
			"instrumentation":{"v1/event-qwt":"277","v1/profile-brtt":"342"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
	}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}




###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone')

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def ref_time():
	time.sleep(random.randint(10,15))
	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"