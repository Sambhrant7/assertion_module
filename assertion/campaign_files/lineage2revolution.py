# -*- coding: utf8 -*-
from sdk import util,purchase,installtimenew
from sdk import NameLists
from sdk import getsleep
from Crypto.Cipher import AES
import json,time,uuid,hashlib,urllib,random,string,datetime,base64,urlparse
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'com.netmarble.lineageII',
	'app_name' 			 :u'리니지2 레볼루션'.encode('utf-8'),
	'app_version_code'	 : '270',#'227',#'200',#'198',#'188',#158, '151',#'146',#'141',#'130',138, 150, 173, 183
	'app_version_name'	 : '0.63.18',#'0.61.34',#'0.59.16',#'0.59.12',#'0.58.28',#0.56.14, '0.55.20',#'0.55.12',#'0.54.18',#'0.53.18',0.54.14, 0.55.18, 0.57.26, 0.58.22
	'no_referrer'		 : True,
	'ctr'				 : 6,
	'supported_os'		 :'4.4', #Requires Android 4.4 and up
	'app_size'			 : 91.3,#91.0,
	'supported_countries':'WW',
	'device_targeting'	 :True,
	'tracker'			 :'kochava',
	'mat':{
		'advertiser_id'	 : '175038',
		'iv'			 : 'heF9BATUfWuISyO8',
		'sdk'			 : 'android',
		'key'		     : '8fb464cc8e7ab83b455c3c3830e7ab41',
		'version'		 : '4.16.0',
	},
	'kochava':{
		'sdk_version' : 'AndroidTracker 3.3.1',
		'sdk_protocol': '10',
		'kochava_app_id':'kolineage-ii-revolution-android-3yi',
		'sdk_build_date': '2018-02-09T18:47:10Z'
	},
	'netmarble':{
		'version': "4.3.2.1",
		'gamecode':'lineageII'
	},
	'purchase':{
		'1':{
				'name':'lineage2_dia_120b',
				'price':'3300'
			},
		'2':{
				'name':'lineage2_dia_210b',
				'price':'5500'
			},
		'3':{
				'name':'lineage2_dia_425',
				'price':'11000'
			},
	},
	'apsalar':{
					'version': 'Singular/v9.2.0',#'Singular/v7.3.3.PROD',
					'key':'netmarblekr_9b38e90b',
					'secret':'c52013bac93dfe2b0c9bf61a35d10452'
				},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:23,
		22:22,
		23:21,
		24:20,
		25:19,
		26:18,
		27:17,
		28:16,
		29:15,
		30:14,
		31:13,
		32:10,
		33:8,
		34:7,
		35:5,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print 'please wait installing_____________'

	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")

	global start_s
	start_s = str(int(app_data.get('times').get('install_complete_time')*1000))	

	get_user_id(app_data)
	kochava_device_ID(campaign_data,app_data,device_data)
	app_data['level_clear']=1
	app_data['registration_flag']= False
	app_data['battery_level']=str(random.randint(10,100))

	if not app_data.get('ssid'):
		app_data['ssid']=util.get_random_string("char_all",random.randint(4,10))

	if not app_data.get('singular_install_id'):
		app_data['singular_install_id'] = str(uuid.uuid4())
	
	time.sleep(random.randint(30,60))
	###########		CALLS		################	

	print '---------------------- kochava init ----------------------'
	kochava_init=android_Kvinit(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**kochava_init)
	app_data['kochava_last_call_time'] = int(time.time())

	print "\nself_call_bileapi_netmarble\n"
	request=bileapi_netmarble_getKey( campaign_data, device_data, app_data )
	util.execute_request(**request)

	print "\nself_call_api_at_netmarble_net\n"
	request=api_at_netmarble_net( campaign_data, device_data, app_data )
	util.execute_request(**request)

	print "----------Apsalar Resolve call---------"
	req=apsalarResolve(campaign_data, app_data, device_data)
	util.execute_request(**req)

	print "----------Apsalar start call---------"
	req=apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**req)

	print '---------------------- kochava tracker ----------------------'
	kochava_tracker=android_KvTracker(campaign_data, app_data, device_data)
	util.execute_request(**kochava_tracker)

	print "\nself_call_bileauth-rest_netmarble\n"
	request=bileauth_rest_netmarble( campaign_data, device_data, app_data )
	util.execute_request(**request)
	
	print '\nMat : SESSION____________________________________'
	request=mat_serve(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	catch(response,app_data,'mat')
	set_OpenLogID(app_data)
	set_LastOpenLogID(app_data)

	x= 'Check_in'
	y={"user_id":app_data.get('user_id')}
	req=apsalarEvent(campaign_data, app_data, device_data,x,y)
	util.execute_request(**req)
	time.sleep(random.randint(5,10))
	checkIn(campaign_data, app_data, device_data)

	for i in range(0,random.randint(1,3)):
		x= 'app_open_event'
		y= {'appGuid':campaign_data.get('kochava').get('kochava_app_id'),'value':'app_openevent'}
		z={"appGuid":campaign_data.get('apsalar').get('key')}
		req=apsalarEvent(campaign_data, app_data, device_data,x,z)
		util.execute_request(**req)
		time.sleep(random.randint(5,10))
		print '---------------------- kochava tracker ----------------------'
		kochava_tracker=android_KvTrackerII(campaign_data, app_data, device_data,x,y)
		util.execute_request(**kochava_tracker)
		z={}
	
	x= 'Check_in'
	y= {"user_id":app_data.get('user_id')}
	print '---------------------- kochava tracker ----------------------'
	kochava_tracker=android_KvTrackerII(campaign_data, app_data, device_data,x,y)
	util.execute_request(**kochava_tracker)
	
	x= 'cdn_complete'
	y= {"user_id":app_data.get('user_id'),"value":"cdn_complete"}
	z={"user_id":app_data.get('user_id')}
	req=apsalarEvent(campaign_data, app_data, device_data,x,z)
	util.execute_request(**req)
	time.sleep(random.randint(5,10))
	print '---------------------- kochava tracker ----------------------'
	kochava_tracker=android_KvTrackerII(campaign_data, app_data, device_data,x,y)
	util.execute_request(**kochava_tracker)
	
	################In App Download#######################
	time.sleep(random.randint(100,150))

	call_pattern(app_data,device_data,type='install')


	statelist=['pause','resume','pause','resume','pause','resume']

	for i in range(0,random.randint(1,3)):

		req=kochava_session(app_data,campaign_data,device_data,state=statelist[i])
		util.execute_request(**req)
	
	###########		FINALIZE	################
		
	return {'status':True}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")

	global start_s
	start_s = str(int(time.time()*1000))

	get_user_id(app_data)
	kochava_device_ID(campaign_data,app_data,device_data)
	if not app_data.get('level_clear'):
		app_data['level_clear']=1

	if not app_data.get('ssid'):
		app_data['ssid']=util.get_random_string("char_all",random.randint(4,10))

	if not app_data.get('singular_install_id'):
		app_data['singular_install_id'] = str(uuid.uuid4())

	if not app_data.get('registration_flag'):
		app_data['registration_flag']=False
	###########		CALLS		################	
	print '---------------------- kochava init ----------------------'
	kochava_init=android_Kvinit(campaign_data, app_data, device_data)
	util.execute_request(**kochava_init)
	app_data['kochava_last_call_time'] = int(time.time())

	print "----------Apsalar Resolve call---------"
	req=apsalarResolve(campaign_data, app_data, device_data)
	util.execute_request(**req)


	print "----------Apsalar start call---------"
	req=apsalarStart(campaign_data, app_data, device_data, isOpen = True)
	util.execute_request(**req)


	for i in range(0,random.randint(1,3)):
		x= 'app_open_event'
		y= {'appGuid':campaign_data.get('kochava').get('kochava_app_id'),'value':'app_openevent',}
		z={"appGuid":campaign_data.get('apsalar').get('key')}
		req=apsalarEvent(campaign_data, app_data, device_data,x,z)
		util.execute_request(**req)
		time.sleep(random.randint(5,10))
		print '---------------------- kochava tracker I----------------------'
		kochava_tracker=android_KvTrackerII(campaign_data, app_data, device_data,x,y)
		util.execute_request(**kochava_tracker)


	
	print '\nMat : SESSION____________________________________'
	request=mat_serve(campaign_data, app_data, device_data, isOpen=True)
	response=util.execute_request(**request)
	catch(response,app_data,'mat')
	set_OpenLogID(app_data)
	set_LastOpenLogID(app_data)

	call_pattern(app_data,device_data,type='open',day=day)
	
	print "--------- Purchase Call--------------"
	if purchase.isPurchase(app_data,day):	
		flag = 0
		purchase_random = random.randint(1,100)
		
		if purchase_random <= 70:
			purchase_type = '1'
		
		elif purchase_random > 70 and purchase_random <= 95:
			purchase_type = '2'
			
		elif purchase_random > 95 and purchase_random <= 100:
			purchase_type = '3'
			
		else:
			flag = 1
			purchase_type = False
		
		if flag == 0:
			if not purchase_type == False:
				time.sleep(random.randint(30,60))
				print '\nMat : SESSION____________________________________'
				request=mat_serve(campaign_data, app_data, device_data)
				response=util.execute_request(**request)
				catch(response,app_data,'mat')
				set_OpenLogID(app_data)
				set_LastOpenLogID(app_data)
	
				print '\nMat : EVENT____________________________Purchase'
				request=mat_serve_purchase(campaign_data, app_data, device_data, purchase_type)
				util.execute_request(**request)
				
				print '---------------------- kochava purchase tracker ----------------------'
				kochava_tracker=android_kvTracker_purchase(campaign_data, app_data, device_data)
				util.execute_request(**kochava_tracker)
	

	###########		FINALIZE	################
		
	return {'status':True}

def call_pattern(app_data,device_data,type=False,day=False):

	if type=='install':
		level_clear = random.choice([2,2,3,3,4,4,4,5,5,5])
	elif type == "open":
		if day == 1:
			level_clear = random.choice([1,2,2,2,3,3,3,4,4])
		else:
			level_clear = random.choice([0,1,1,2,2,2,3,3,3,4])
	else:
		print "not install and open"

	if app_data.get('registration_flag')==False and random.randint(1,100)<=90:	
		registration(campaign_data, app_data, device_data)


		x= 'RegistrationComplete'
		y= {"user_id":app_data.get('user_id')}	
		x1='registration_complete'
		req=apsalarEvent(campaign_data, app_data, device_data,x1,y)
		util.execute_request(**req)
		time.sleep(random.randint(5,10))
		print '---------------------- kochava tracker ----------------------'
		kochava_tracker=android_KvTrackerII(campaign_data, app_data, device_data,x,y)
		util.execute_request(**kochava_tracker)
		app_data['registration_flag']= True
		
		for i in range(0,level_clear):	
			
			achievement(campaign_data, app_data, device_data)
			time.sleep(random.randint(2,5))
			x= 'achieve_achievement'
			y= {"user_id":app_data.get('user_id'),"idx":get_idx(app_data)}
			req=apsalarEvent(campaign_data, app_data, device_data,x,y)
			util.execute_request(**req)
			time.sleep(random.randint(5,10))
			print '---------------------- kochava tracker ----------------------'
			kochava_tracker=android_KvTrackerII(campaign_data, app_data, device_data,x,y)
			util.execute_request(**kochava_tracker)
			
			characterLevel(campaign_data, app_data, device_data)
		
			x= 'character_level'
			y= {"user_id":app_data.get('user_id'),"level":app_data.get('level_clear')}

			req=apsalarEvent(campaign_data, app_data, device_data,x,y)
			util.execute_request(**req)
			time.sleep(random.randint(5,10))
			print '---------------------- kochava tracker ----------------------'
			kochava_tracker=android_KvTrackerII(campaign_data, app_data, device_data,x,y)
			util.execute_request(**kochava_tracker)	
		
			if app_data.get('level_clear')== 7:
				tutorial(campaign_data, app_data, device_data)
				
				x= 'tutorialcomplete'
				y= {"user_id":app_data.get('user_id'),"value":"tutorial_complete"}
				print '---------------------- kochava tracker ----------------------'
				kochava_tracker=android_KvTrackerII(campaign_data, app_data, device_data,x,y)
				util.execute_request(**kochava_tracker)	
				
				app_data['tutorial_flag']= True
				
			if app_data.get('tutorial_flag')==True and app_data.get('checkoutstart_flag')==False and random.randint(1,100)<=60:	
				
				achievement(campaign_data, app_data, device_data)
			
				x= 'achieve_achievement'
				y= {"user_id":app_data.get('user_id'),"idx":get_idx(app_data)}
				print '---------------------- kochava tracker ----------------------'
				kochava_tracker=android_KvTrackerII(campaign_data, app_data, device_data,x,y)
				util.execute_request(**kochava_tracker)
		
				checkout(campaign_data, app_data, device_data)

				x1='checkout_initiated'	
				x= 'checkoutstart'
				y= {"user_id":app_data.get('user_id')}

				req=apsalarEvent(campaign_data, app_data, device_data,x1,y)
				util.execute_request(**req)

				time.sleep(random.randint(5,10))
				print '---------------------- kochava tracker ----------------------'
				kochava_tracker=android_KvTrackerII(campaign_data, app_data, device_data,x,y)
				util.execute_request(**kochava_tracker)
		
			app_data['level_clear'] += 1
			print '*****************************************************'
			print app_data.get('level_clear')	
			time.sleep(random.randint(20,40))	
	
	
################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################
def checkIn(campaign_data, app_data, device_data):
	print '\nMat : EVENT____________________________CheckIn'
	request=mat_serve(campaign_data, app_data, device_data, action="conversion:Check_in", includeLogID=True)
	util.execute_request(**request)
	
def achievement(campaign_data, app_data, device_data):
	print '\nMat : EVENT____________________________Achievement'
	request=mat_serve(campaign_data, app_data, device_data, action="conversion:achieve_achievement", includeLogID=True)
	util.execute_request(**request)

def checkout(campaign_data, app_data, device_data):
	print '\nMat : EVENT____________________________Checkout'
	request=mat_serve(campaign_data, app_data, device_data, action="conversion:checkout_initiated", includeLogID=True)
	util.execute_request(**request)

def characterLevel(campaign_data, app_data, device_data):
	print '\nMat : EVENT____________________________CharacterLevel'
	request=mat_serve(campaign_data, app_data, device_data, action="conversion:character_level", includeLogID=True)
	util.execute_request(**request)

def tutorial(campaign_data, app_data, device_data):
	print '\nMat : EVENT____________________________Tutorial'
	request=mat_serve(campaign_data, app_data, device_data, action="conversion:tutorial_complete", includeLogID=True)
	util.execute_request(**request)

def registration(campaign_data, app_data, device_data):
	print '\nMat : EVENT____________________________Registration'
	request=mat_serve(campaign_data, app_data, device_data, action="conversion:registration", includeLogID=True)
	util.execute_request(**request)

###################################################################
# mat_serve()	: method
# parameter 	: campaign_data, app_data, device_data,
#				  action, includeLogID, isOpen
#
# Simulates Mat's behaviour whenever the App gets open
# or incase of an in-app event.
###################################################################
def mat_serve(campaign_data, app_data, device_data, action="session", includeLogID=False, isOpen=False, purchase_choose=False):
	def_appInstallData(app_data)
	def_matID(app_data)
	event = ""
	if "conversion" in action:
		event 	= action.split(":")[1]
		action 	= action.split(":")[0]

	method	= 'post'
	url 	= 'http://'+campaign_data['mat']['advertiser_id']+'.engine.mobileapptracking.com/serve'
	headers = {
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/json',
		'Accept'			: 'application/json',
		'User-Agent'		: get_ua(app_data,device_data),
	}
	data=json.dumps({"data":[]})
	params = {
		'action'			: action,
		'advertiser_id'		: campaign_data['mat']['advertiser_id'],
		'package_name'		: campaign_data['package_name'],
		'sdk'				: 'android',
		'sdk_retry_attempt'	: '0',
		'transaction_id'	: str(uuid.uuid4()),
		'ver'				: campaign_data['mat']['version'],
	}
	data_arr = {
		'app_ad_tracking'		:'0',
		'app_name'				: campaign_data['app_name'],
		'app_version'			: campaign_data['app_version_code'],
		'app_version_name'		: campaign_data['app_version_name'],
		'build'					: device_data.get('build'),
		'connection_type'		: "wifi",
		'conversion_user_agent'	: get_ua(app_data,device_data),
		# 'country_code'			: device_data.get('locale').get('country').lower(),
		'currency_code'			: 'USD',
		'device_brand'			: device_data['brand'].upper(),
		'device_carrier'		: device_data['carrier'],
		'device_cpu_type'		: device_data.get('cpu_abi'),
		'device_model'			: device_data['model'],
		'fire_ad_tracking_disabled':'0',
		'google_ad_tracking_disabled': '0',
		'google_aid'			: device_data['adid'],
		'insdate'				: app_data['appInstallDate'],
		'installer'				: 'com.android.vending',
		'is_coppa'				: '0',
		'language'				: device_data['locale']['language'],
		'locale'				: device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper(),
		'mat_id'				: app_data['matID'],
		'mobile_country_code'	: device_data['mcc'],
		'mobile_network_code'	: device_data['mnc'],
		'os_version'			: device_data['os_version'],
		'revenue'				: '0.0',
		'screen_density'		: util.getdensity(device_data['dpi']),
		'screen_layout_size'	: device_data['resolution'].split('x')[0]+"x"+device_data['resolution'].split('x')[1],
		'sdk_version'			: campaign_data['mat']['version'],
		# 'referrer_delay'		:random.randint(1000,2000),
		'system_date'			: time_Stamp(),
		}
	if app_data.get('referrer'):
		data_arr['install_referrer']	= urllib.unquote(app_data['referrer'])

	if action == 'conversion':
		params['site_event_name'] 	 	= event
		
		if event== 'purchase':
			data_arr['revenue']= campaign_data.get('purchase').get(purchase_choose).get('price')+".0"
			data_arr['is_paying_user']='1'
			data_arr['currency_code']= 'KRW'
			data_arr['advertiser_ref_id']= '21'+str(util.get_random_string('decimal',17))
			
			data= json.dumps({"data":[{"unit_price":campaign_data.get('purchase').get(purchase_choose).get('price')+".0","quantity":"1","item":campaign_data.get('purchase').get(purchase_choose).get('name')}]})

	if includeLogID or isOpen:
		if app_data.get('open_log_id') and app_data.get('last_open_log_id'):
			data_arr['open_log_id']		= app_data['open_log_id']
			data_arr['last_open_log_id']= app_data['last_open_log_id']

	da_str 	= urllib.urlencode(data_arr)
	key 	= campaign_data['mat']['key']
	iv 		= campaign_data['mat']['iv']
	aes 	= AESCipher(key)

	params['data'] = aes.encrypt(iv, da_str)
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}
	
def mat_serve_purchase(campaign_data, app_data, device_data,purchase_choose=False):
	def_appInstallData(app_data)
	def_matID(app_data)
	
	method	= 'post'
	url 	= 'http://'+campaign_data['mat']['advertiser_id']+'.engine.mobileapptracking.com/serve'
	headers = {
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/json',
		'Accept'			: 'application/json',
		'User-Agent'		: get_ua(app_data,device_data),
	}
	data=json.dumps({"data":[]})
	params = {
		'action'			: 'conversion',
		'advertiser_id'		: campaign_data['mat']['advertiser_id'],
		'package_name'		: campaign_data['package_name'],
		'sdk'				: 'android',
		'sdk_retry_attempt'	: '0',
		'site_event_name'	: 'purchase',
		'transaction_id'	: str(uuid.uuid4()),
		'ver'				: campaign_data['mat']['version'],
	}
	data_arr = {
		'app_name'				: campaign_data['app_name'],
		'app_version'			: campaign_data['app_version_code'],
		'app_version_name'		: campaign_data['app_version_name'],
		'connection_type'		: "wifi",
		'conversion_user_agent'	:app_data.get('ua'),
		'currency_code'			: 'USD',
		'device_brand'			: device_data['brand'],
		'device_carrier'		: device_data['carrier'],
		'device_model'			: device_data['model'],
		'google_ad_tracking_disabled': 0,
		'google_aid'			: device_data['adid'],
		'insdate'				: app_data['appInstallDate'],
		'installer'				: 'com.android.vending',
		'language'				: device_data['locale']['language'],
		'mat_id'				: app_data['matID'],
		'mobile_country_code'	: device_data['mcc'],
		'mobile_network_code'	: device_data['mnc'],
		'os_version'			: device_data['os_version'],
		'revenue'				: '0.0',
		'screen_density'		: util.getdensity(device_data['dpi']),
		'screen_layout_size'	: device_data['resolution'].split('x')[1]+"x"+device_data['resolution'].split('x')[0],
		'sdk_version'			: campaign_data['mat']['version'],
		'system_date'			: time_Stamp(),
		'is_paying_user'		: '1',
		'advertiser_ref_id'		: '21'+str(util.get_random_string('decimal',17))
		}
	if app_data.get('referrer'):
		data_arr['install_referrer']	= urllib.unquote(app_data['referrer'])
		
	data= json.dumps({"data":[{"unit_price":campaign_data.get('purchase').get(purchase_choose).get('price')+".0","quantity":"1","item":campaign_data.get('purchase').get(purchase_choose).get('name')}]})

	if app_data.get('open_log_id') and app_data.get('last_open_log_id'):
		data_arr['open_log_id']		= app_data['open_log_id']
		data_arr['last_open_log_id']= app_data['last_open_log_id']

	da_str 	= urllib.urlencode(data_arr)
	key 	= campaign_data['mat']['key']
	iv 		= campaign_data['mat']['iv']
	aes 	= AESCipher(key)

	params['data'] = aes.encrypt(iv, da_str)
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}	
	
###########################################################
#														  #
#						KOCHAVA							  #
#														  #
###########################################################
def id_generator(size):
  	 return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(size))
def nt_id(app_data) :
	if not app_data.get('nt_id'):
		app_data['nt_id'] = id_generator(5)+'-'+str(uuid.uuid4())


def android_Kvinit(campaign_data, app_data, device_data):	
	kochava_device_ID(campaign_data,app_data,device_data)
	get_ua(app_data,device_data)
	nt_id(app_data)



	
	url='http://kvinit-prod.api.kochava.com/track/kvinit'
	method='post'
	header={
		'User-Agent':get_ua(app_data,device_data),
		'Content-Type' : 'application/json; charset=UTF-8',
		'Accept-Encoding':'gzip'
	}
	usertime=int(time.time())

	data={
		"kochava_app_id": campaign_data.get('kochava').get('kochava_app_id'),
		"sdk_version": campaign_data.get('kochava').get('sdk_version'),
		"sdk_protocol": campaign_data.get('kochava').get('sdk_protocol'),
		"data": {
			"package": campaign_data.get('package_name'),
			"platform": "android",
			"os_version": 'Android '+device_data.get('os_version'),
			"usertime": str(usertime),
			"uptime": '0.'+str(random.randint(100,999)),
		},
		"nt_id": app_data.get('nt_id'),
		"action": "init",
		"kochava_device_id": app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else kochava_device_ID(campaign_data,app_data,device_data),
		"send_date": datetime.datetime.fromtimestamp(time.time()+1).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',
		'sdk_build_date':campaign_data.get('kochava').get('sdk_build_date')
	}
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': json.dumps(data)	}

def android_KvTracker(campaign_data, app_data, device_data):	
	kochava_device_ID(campaign_data,app_data,device_data)
	nt_id(app_data)
	get_ua(app_data,device_data)
	get_screen_inches(app_data)

	url='http://control.kochava.com/track/json'
	method='post'
	header={
		'User-Agent':get_ua(app_data,device_data),
		'Content-Type' : 'application/json; charset=UTF-8',
		'Accept-Encoding':'gzip'
	}
	usertime=int(time.time())
	# uptime = usertime - app_data.get('kochava_last_call_time') + 1 if app_data.get('kochava_last_call_time') else random.randint(1,10)
	data={
		'kochava_app_id': campaign_data.get('kochava').get('kochava_app_id'),
		'kochava_device_id': app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else kochava_device_ID(campaign_data,app_data,device_data),
		'action' : 'initial',
		'sdk_version': campaign_data.get('kochava').get('sdk_version'),
		'send_date': datetime.datetime.fromtimestamp(time.time()+1).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',
		'nt_id': app_data.get('nt_id'),
		'sdk_protocol': campaign_data.get('kochava').get('sdk_protocol'),

		'data':{
			'usertime': str(usertime),
			'uptime': '0.'+str(random.randint(100,999)),
			'screen_brightness': '0.'+str(random.randint(1000,9999)),
			'device_orientation': 'landscape',
			'network_conn_type': 'wifi',
			'volume': '6',
			"conversion_type":"gplay",
			'carrier_name':device_data.get('carrier').lower(),
			# 'connected_devices':["Mouse: hbtp_vm"],
			'adid': device_data.get('adid'),
			'device': device_data.get('model')+'-'+device_data.get('brand').lower(),
			'disp_h': int(device_data.get('resolution').split('x')[1]),
			'disp_w': int(device_data.get('resolution').split('x')[0]),
			'package': campaign_data.get('package_name'),
			'product_name':device_data.get('product'),
			'state_active':True,
			'app_version': campaign_data.get('app_name')+" "+campaign_data.get('app_version_code'),
			'android_id': device_data.get('android_id'), 
			'os_version': 'Android '+ device_data.get('os_version'),
			'app_limit_tracking': False,
			'device_limit_tracking': False,
			'app_short_string':campaign_data.get('app_version_name'),
			'architecture':'armv7l',
			'battery_level': app_data.get('battery_level'),
			'battery_status': 'discharging',
			'device_cores': device_data.get('cpu_core'),
			'install_referrer':{'status':'missing_dependency'},
			'installed_date': str(int(app_data.get("times").get("install_complete_time"))),
			'installer_package':'com.android.vending',
			'is_genuine':True,
			'language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
			'locale':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
			'manufacturer': device_data.get('brand').upper(),
			'screen_dpi': device_data.get('dpi'),
			'screen_inches': app_data.get('screen_inches'),
			'ssid':app_data.get('ssid'),
			'timezone':device_data.get('local_tz_name'),
			'ui_mode':'Normal',
		}	
	}
	if app_data.get('referrer'):
		referrer = app_data.get('referrer') if app_data.get('referrer') else ''
		referrerpart = urlparse.parse_qs(referrer)
		utm_medium = referrerpart.get('utm_medium')[0] if referrerpart.get('utm_medium') else ''
		# utm_content = referrerpart.get('utm_content')[0] if referrerpart.get('utm_content') else ''
		# utm_term = referrerpart.get('utm_term')[0] if referrerpart.get('utm_term') else ''
		utm_source = referrerpart.get('utm_source')[0] if referrerpart.get('utm_source') else ''
		# utm_campaign = referrerpart.get('utm_campaign')[0] if referrerpart.get('utm_campaign') else ''
		data['data']['conversion_data']=urllib.quote('utm_source='+utm_source+'&utm_medium='+utm_medium+'')
	else:
		data['data']['conversion_data']='utm_source=google-play&utm_medium=organic'
		

	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': json_str}

def android_KvTrackerI(campaign_data, app_data, device_data):	
	kochava_device_ID(campaign_data,app_data,device_data)
	get_ua(app_data,device_data)

	url='http://control.kochava.com/track/kvTracker.php'
	method='post'
	header={
		'User-Agent':get_ua(app_data,device_data),
		'Content-Type' : 'application/json; charset=UTF-8',
		'Accept-Encoding':'gzip'
	}
	usertime=int(time.time())
	uptime = usertime - app_data.get('kochava_last_call_time') + 1 if app_data.get('kochava_last_call_time') else random.randint(1,10)

	data={
			"kochava_app_id": campaign_data.get('kochava').get('kochava_app_id'),
			"kochava_device_id":app_data.get('kochava_device_id'),
			"action": "session",
			"data": {
				"usertime": str(usertime),
				"uptime": str(uptime+random.randint(150,180)),
				"updelta": "0",
				"state": "exit"
			}
		}, {
			"kochava_app_id": campaign_data.get('kochava').get('kochava_app_id'),
			"kochava_device_id":app_data.get('kochava_device_id'),
			"action": "session",
			"data": {
				"usertime": str(usertime),
				"uptime": str(uptime+random.randint(10,30)),
				"updelta": str(uptime+random.randint(50,60)),
				"state": "launch"
			}
		}

	json_str=json.dumps([data])
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': json_str}

def android_KvTrackerII(campaign_data, app_data, device_data,event_name,event_data):	
	kochava_device_ID(campaign_data,app_data,device_data)
	get_ua(app_data,device_data)
	nt_id(app_data)

	url='http://control.kochava.com/track/json'
	method='post'
	header={
		'User-Agent':get_ua(app_data,device_data),
		'Content-Type' : 'application/json; charset=UTF-8',
		'Accept-Encoding':'gzip'
	}
	usertime=int(time.time())
	# uptime = usertime - app_data.get('kochava_last_call_time') + 1 if app_data.get('kochava_last_call_time') else random.randint(1,10)

	data={
			"kochava_app_id": campaign_data.get('kochava').get('kochava_app_id'),
			"kochava_device_id": app_data.get('kochava_device_id'),
			"action": "event",
			'sdk_version': campaign_data.get('kochava').get('sdk_version'),
			"send_date": datetime.datetime.fromtimestamp(time.time()+1).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',
			"nt_id": app_data.get('nt_id'),
			'sdk_protocol': campaign_data.get('kochava').get('sdk_protocol'),



			"data": {
				'usertime': str(usertime),
				'uptime': '0.'+str(random.randint(100,999)),
				"screen_brightness": '0.'+str(random.randint(1000,9999)),
				"device_orientation": "landscape",
				"network_conn_type": "wifi",
				'volume': '0',
				# 'connected_devices':["Mouse: hbtp_vm"],
				'carrier_name':device_data.get('carrier').lower(),
				'device': device_data.get('model')+'-'+device_data.get('brand').lower(),
				'disp_h': int(device_data.get('resolution').split('x')[1]),
				'disp_w': int(device_data.get('resolution').split('x')[0]),
				'app_version': campaign_data.get('app_name')+" "+campaign_data.get('app_version_code'),
				'os_version': 'Android '+ device_data.get('os_version'),
				'app_short_string':campaign_data.get('app_version_name'),
				'architecture':'armv7l',
				'battery_level': app_data.get('battery_level'),
				'battery_status': 'discharging',
				'locale':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
				'manufacturer': device_data.get('brand').upper(),
				'screen_dpi': device_data.get('dpi'),
				'ssid':app_data.get('ssid'),
				"event_data": event_data,
				"event_name": event_name,
				'timezone':device_data.get('local_tz_name'),
				'ui_mode':'Normal',
				'product_name':device_data.get('product'),
				'state_active':True
			}
		}

	json_str=json.dumps([data])
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': json_str}	


def kochava_session(app_data,campaign_data,device_data,state=''):
	kochava_device_ID(campaign_data,app_data,device_data)
	get_ua(app_data,device_data)
	nt_id(app_data)
	url='http://control.kochava.com/track/json'
	method='post'
	header={
		'User-Agent':get_ua(app_data,device_data),
		'Content-Type' : 'application/json; charset=UTF-8',
		'Accept-Encoding':'gzip'
	}
	usertime=int(time.time())
	# uptime = usertime - app_data.get('kochava_last_call_time') + 1 if app_data.get('kochava_last_call_time') else random.randint(1,10)

	data={
			"kochava_app_id": campaign_data.get('kochava').get('kochava_app_id'),
			"kochava_device_id": app_data.get('kochava_device_id'),
			"action": "session",
			'sdk_version': campaign_data.get('kochava').get('sdk_version'),
			"send_date": datetime.datetime.fromtimestamp(time.time()+1).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',
			"nt_id": app_data.get('nt_id'),
			'sdk_protocol': campaign_data.get('kochava').get('sdk_protocol'),

			"data": {
				'usertime': str(usertime),
				'uptime': '0.'+str(random.randint(100,999)),
				"screen_brightness": '0.'+str(random.randint(1000,9999)),
				"device_orientation": "landscape",
				"network_conn_type": "wifi",
				'volume': '0',
				# # 'connected_devices':["Mouse: hbtp_vm"],
				'carrier_name':device_data.get('carrier').lower(),
				'device': device_data.get('model')+'-'+device_data.get('brand').lower(),
				'disp_h': int(device_data.get('resolution').split('x')[1]),
				'disp_w': int(device_data.get('resolution').split('x')[0]),
				'app_version': campaign_data.get('app_name')+" "+campaign_data.get('app_version_code'),
				'os_version': 'Android '+ device_data.get('os_version'),
				'app_short_string':campaign_data.get('app_version_name'),
				'architecture':'armv7l',
				'battery_level': app_data.get('battery_level'),
				'battery_status': 'discharging',
				'locale':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
				'manufacturer': device_data.get('brand').upper(),
				'screen_dpi': device_data.get('dpi'),
				'ssid':app_data.get('ssid'),
				
				'timezone':device_data.get('local_tz_name'),
				'ui_mode':'Normal',
				'product_name':device_data.get('product'),
				'state_active':True,
				'state':str(state),
			}
		}

	if state=='pause':
		data['data']['state_active_count']=1

	json_str=json.dumps([data])
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': json_str}	



def android_kvTracker_purchase(campaign_data, app_data, device_data):
	kochava_device_ID(campaign_data,app_data,device_data)
	url='http://control.kochava.com/track/kvTracker.php'
	
	header={
		'Content-Type' : 'application/json; charset=UTF-8',
		'User-Agent'   : 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
		'Accept-Encoding': 'gzip'
		
	}
	usertime=int(time.time())
	uptime = usertime - app_data.get('kochava_last_call_time') + 1 if app_data.get('kochava_last_call_time') else random.randint(1,10)
	params={}	
	
			
	data = [{"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),"kochava_device_id":app_data.get('kochava_device_id'),"action":"session","data":{"usertime":str(usertime),"uptime": str(uptime+random.randint(10,30)),"updelta":str(uptime+random.randint(50,60)),"state":"launch"}}]
	
	json_str=json.dumps(data)
	
	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params': params, 'data': json_str}	
	

#################################################################################################################
#
#					Apsalar calls-------------
#
##############################%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%###############################
def generate_gcm_id(app_data):
	if not app_data.get('device_gcm_id'):
		app_data['device_gcm_id'] = 'APA' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(137))
	return app_data.get('device_gcm_id')

def apsalarResolve(campaign_data, app_data, device_data):
	url = 'https://sdk-api-v1.singular.net/api/v1/resolve' #'http://e-ssl.apsalar.com/api/v1/resolve'
	method = 'post' #'get'
	headers = {
		'User-Agent': 'Singular/SDK-v9.2.0.PROD',#'Singular/SDK-v7.3.3.PROD',
		'Accept-Encoding': 'gzip'
	}
	
	# params ='a='+str(campaign_data.get('apsalar').get('key'))+'&c=wifi&i='+str(campaign_data.get('package_name'))+'&k=AIFA'+'&lag='+str(generateLag())+'&p=Android&pi=1&rt=json'+'&u='+str(device_data.get('adid'))+'&v='+str(device_data.get('os_version'))
	
	data_dict = {
		"a":str(campaign_data.get('apsalar').get('key')),
		"c":"wifi",
		"i":campaign_data.get('package_name'),
		"k":"AIFA",
		"lag":str(generateLag()),
		"p":"Android",
		"pi":"1",
		"rt":"json",
		"u":device_data.get('adid'),
		"v":device_data.get('os_version'),
	}

	params = get_params(data_dict)
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarStart(campaign_data, app_data, device_data, isOpen = False):
	# generate_gcm_id(app_data)
	def_appInstallData(app_data)
	
	url = 'https://sdk-api-v1.singular.net/api/v1/start' #'http://e-ssl.apsalar.com/api/v1/start'
	method = 'post'#'get'
	headers = {
		'User-Agent': 'Singular/SDK-v9.2.0.PROD', #'Singular/SDK-v7.3.3.PROD', 
		'Accept-Encoding': 'gzip'
	}
	
	# if not app_data.get('upt'):
	# 	app_data['upt'] = str(int(time.time()*1000))
	# time.sleep(random.randint(20,30))
	# if not app_data.get('s'):
	# 	app_data['s'] = str(int(time.time()*1000))
	
	
	# params ='a='+str(campaign_data.get('apsalar').get('key'))+'&ab='+str(device_data['cpu_abi'])+'&aifa='+str(device_data['adid'])+'&av='+str(campaign_data.get('app_version_name'))+'&br='+str(device_data['brand'])+'&c=wifi&current_device_time='+str(int(time.time()*1000))+'&ddl_enabled=false&de='+str(device_data['device'])+'&device_type='+str(device_data['device_type'])+'&dnt=0&i='+str(campaign_data.get('package_name'))+'&install_time='+str(app_data.get('appInstallDate')*1000)+'&is=true&k=AIFA&lag='+str(generateLag())+'&lc='+str(device_data['locale']['language']+'_'+device_data['locale']['country'])+'&ma='+str(device_data['manufacturer'])+'&mo='+str(device_data['model'])+'&n='+str(urllib.quote(campaign_data.get('app_name')))+'&p=Android&pr='+str(device_data['product'])+'&rt=json&s='+str(app_data.get('s'))+'&sdk='+str(campaign_data.get('apsalar').get('version'))+'&src=com.android.vending&u='+str(device_data['adid'])+'&update_time='+app_data.get('upt')+'&v='+str(device_data['os_version'])


	data_dict = {
				"a":str(campaign_data.get('apsalar').get('key')),
				"ab":device_data.get('cpu_abi',"armeabi"),
				"aifa":device_data.get('adid'),
				"av":campaign_data.get('app_version_name'),
				"br":device_data.get('brand'),
				"c":"wifi",
				"current_device_time":str(int(time.time()*1000)),
				"ddl_enabled":"true",
				"ddl_to":"60",
				"de":device_data.get('device'),
				"device_type":"phone",
				"device_user_agent":get_ua(app_data,device_data),
				"dnt":"0",
				"event_index": str(app_data.get('seq', '0')),
				"i":campaign_data.get('package_name'),
				"install_time":str(int(app_data.get('times').get('download_end_time')*1000)),
				"is":"true",
				"k":"AIFA",
				"lag":str(generateLag()),
				"lc":device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
				"ma":device_data.get('manufacturer'),
				"mo":device_data.get('model'),
				"n":campaign_data.get('app_name'),
				"p":"Android",
				"pr":device_data.get('product'),
				"rt":"json",
				"s": start_s,
				"sdk": str(campaign_data.get('apsalar').get('version')),
				"singular_install_id":app_data.get('singular_install_id'),
				"src":"com.android.vending",
				"u":device_data.get('adid'),
				"update_time":str(int(app_data.get('times').get('download_end_time')*1000)),
				"v":device_data.get('os_version'),
	}

	if isOpen:
		data_dict['is'] = "false"

	
	params = get_params(data_dict)
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarEvent(campaign_data, app_data, device_data,event_name,value):
	url = 'https://sdk-api-v1.singular.net/api/v1/event' #'http://e-ssl.apsalar.com/api/v1/event'

	method = 'post' #'get'
	headers = {
		'User-Agent': 'Singular/SDK-v9.2.0.PROD',#'Singular/SDK-v7.3.2.PROD',
		'Accept-Encoding': 'gzip'
	}

	# if not app_data.get('s'):
		# app_data['s'] = str(uuid.uuid4())

	# params='a='+str(campaign_data.get('apsalar').get('key'))+'&aifa='+str(device_data.get('adid'))+'&av='+str(campaign_data.get('app_version_name'))+'&c=wifi'+'&e'+'='+str(json.dumps(value))+'&i='+str(campaign_data.get('package_name'))+'&k=AIFA'+'&lag='+str(generateLag())+'&n='+str(event_name)+'&p=Android'+'&rt=json&s='+str(app_data.get('s'))+'&sdk='+str(campaign_data.get('apsalar').get('version'))+'&seq='+str(cal_seq(app_data))+'&t='+str(generate_t())+'&u='+str(device_data.get('adid'))

	cal_seq(app_data)

	data_dict = {
	"a": str(campaign_data.get('apsalar').get('key')),
	"aifa":device_data.get('adid'),
	"av":campaign_data.get('app_version_name'),
	"c":"wifi",
	"event_index":str(app_data.get('seq')),
	"i":campaign_data.get('package_name'),
	"k":"AIFA",
	"lag":str(generateLag()),
	"n":str(event_name),
	"p":"Android",
	"rt":"json",
	"s": start_s,
	"sdk":campaign_data.get('apsalar').get('version'),
	"seq": str(app_data.get('seq')),
	"singular_install_id":app_data.get('singular_install_id'),
	"t": str(generate_t()),
	"u":device_data.get('adid'),
	}


	params = get_params(data_dict)	
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h

	if not value.get('is_revenue_event'):
		value['is_revenue_event'] = False

	q = json.dumps({"e":json.dumps(value)}) # Your payload dict
	q = q.replace(" ","").strip()


	jsonsignature = payloadsignature(q,campaign_data.get('apsalar').get('secret'))

	data = {
	'payload':q,
	'signature': jsonsignature
	}	


	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data).replace(" ","").strip() }	

###########################################################################
#
#			Netmarble 
#
###########################################################################


def bileapi_netmarble_getKey( campaign_data, device_data, app_data ):
	if not app_data.get('NMPlayerID'):
		app_data['NMPlayerID'] = util.get_random_string(type='hex',size=32)
	url= "https://mobileapi.netmarble.com/v2/commonCs/getKey"
	method= "get"
	headers= {       
		"Accept-Encoding": "gzip",
        "Cookie": "NS_Lang=en_US; NS_Region=;",
        "DeviceModel": device_data.get('model'),
        "GeoLocation": "",
        "NMChannelUserID": "{}",
        "NMCity": "",
        "NMCountryCode": "",
        "NMDeviceKey": device_data.get('android_id'),
        "NMDeviceLanguage": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "NMDeviceModel": device_data.get('model'),
        "NMGameCode": "lineageII",
        "NMJoinedCountryCode": "",
        "NMLanguage": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "NMLocalizedLevel": "1",
        "NMManufacturer": device_data.get('manufacturer'),
        "NMMarketType": "googleplay",
        "NMModel": device_data.get('model'),
        "NMNetworkStatus": device_data.get('network'),
        "NMOSVersion": device_data.get('os_version'),
        "NMPlatform": "Android",
        "NMPlayerID": app_data.get('NMPlayerID'),
        "NMRegion": "",
        "NMSDKVersion": campaign_data.get('netmarble').get('version'),
        "NMScreenSize": device_data.get('resolution').replace('x'," ,"),
        "NMTimeZone": device_data.get('timezone'),
        "NMWorld": "",
        "NS_Lang": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "NS_Region": "",
        "NetworkStatus": device_data.get('network'),
        "ScreenSize": device_data.get('resolution').replace('x'," ,"),
        "TimeZone": device_data.get('timezone'),
        "User-Agent": get_ua(app_data,device_data),
        "locale": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "module": "sdk",
        "platform": "android",
        "version": campaign_data.get('netmarble').get('version')
        }

	params= {       
		"checksum": "4a4763b1f649b22231a09feb23bc1e4d58d4a1489d4d9a035faf4a73391a7231",
        "gameCode": campaign_data.get('netmarble').get('gamecode'),
        "localeCode": "ko_kr",
        "serviceCode": "netmarbles",
        "timeZoneID": device_data.get('local_tz_name'),
        "version": "4.3",
        "zone": "real"
        }

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

	
def api_at_netmarble_net( campaign_data, device_data, app_data ):
	if not app_data.get('NMPlayerID'):
		app_data['NMPlayerID'] = util.get_random_string(type='hex',size=32)
	url= "http://api.at.netmarble.net/ver1/launch"
	method= "get"
	headers= {       
		"Accept-Encoding": "gzip",
        "Cookie": "NS_Lang=en_US; NS_Region=;",
        "DeviceModel": device_data.get('model'),
        "GeoLocation": "",
        "NMChannelUserID": "{}",
        "NMCity": "",
        "NMCountryCode": "",
        "NMDeviceKey": device_data.get('android_id'),
        "NMDeviceLanguage": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "NMDeviceModel": device_data.get('model'),
        "NMGameCode": "lineageII",
        "NMJoinedCountryCode": "",
        "NMLanguage": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "NMLocalizedLevel": "1",
        "NMManufacturer": device_data.get('manufacturer'),
        "NMMarketType": "googleplay",
        "NMModel": device_data.get('model'),
        "NMNetworkStatus": device_data.get('network'),
        "NMOSVersion": device_data.get('os_version'),
        "NMPlatform": "Android",
        "NMPlayerID": app_data.get('NMPlayerID'),
        "NMRegion": "",
        "NMSDKVersion": campaign_data.get('netmarble').get('version'),
        "NMScreenSize": device_data.get('resolution').replace('x'," ,"),
        "NMTimeZone": device_data.get('timezone'),
        "NMWorld": "",
        "NS_Lang": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "NS_Region": "",
        "NetworkStatus": device_data.get('network'),
        "ScreenSize": device_data.get('resolution').replace('x'," ,"),
        "TimeZone": device_data.get('timezone'),
        "User-Agent": get_ua(app_data,device_data),
        "locale": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "module": "sdk",
        "platform": "android",
        "version": campaign_data.get('netmarble').get('version')
        }

	params= {       
		"channelUserId": app_data.get('NMPlayerID'),
        "deviceKey": device_data.get('android_id'),
        "platformAdId": device_data.get('adid'),
        "targetGameCode": campaign_data.get('netmarble').get('gamecode'),
        "userId": app_data.get('NMPlayerID'),
        }

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	


def bileauth_rest_netmarble( campaign_data, device_data, app_data ):
	if not app_data.get('NMPlayerID'):
		app_data['NMPlayerID'] = util.get_random_string(type='hex',size=32)
	if not app_data.get('deviceKey'):
		app_data['deviceKey'] = util.get_random_string(type='hex',size=32)	
	url= "https://mobileauth-rest.netmarble.com/signin"
	method= "post"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Cookie": "NS_Lang=en_US; NS_Region=;",
        "DeviceModel": device_data.get('model'),
        "GeoLocation": "",
        "NMChannelUserID": "{}",
        "NMCity": "",
        "NMCountryCode": "",
        "NMDeviceKey": device_data.get('android_id'),
        "NMDeviceLanguage": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "NMDeviceModel": device_data.get('model'),
        "NMGameCode": "lineageII",
        "NMJoinedCountryCode": "",
        "NMLanguage": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "NMLocalizedLevel": "1",
        "NMManufacturer": device_data.get('manufacturer'),
        "NMMarketType": "googleplay",
        "NMModel": device_data.get('model'),
        "NMNetworkStatus": device_data.get('network'),
        "NMOSVersion": device_data.get('os_version'),
        "NMPlatform": "Android",
        "NMPlayerID": app_data.get('NMPlayerID'),
        "NMRegion": "",
        "NMSDKVersion": campaign_data.get('netmarble').get('version'),
        "NMScreenSize": device_data.get('resolution').replace('x'," ,"),
        "NMTimeZone": device_data.get('timezone'),
        "NMWorld": "",
        "NS_Lang": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "NS_Region": "",
        "NetworkStatus": device_data.get('network'),
        "ScreenSize": device_data.get('resolution').replace('x'," ,"),
        "TimeZone": device_data.get('timezone'),
        "User-Agent": get_ua(app_data,device_data),
        "locale": device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country'),
        "module": "sdk",
        "platform": "android",
        "version": campaign_data.get('netmarble').get('version')
        }

	params= None

	data= {       
		"adId": device_data.get('adid'),
        "countryCode": "KR",
        "deviceKey": app_data.get('deviceKey'),
        "gameCode": campaign_data.get('netmarble').get('gamecode'),
        "nmDeviceKey": device_data.get('android_id'),
        "playerId": app_data.get('NMPlayerID'),
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(app_data,device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################
def get_locale(device_data):
	return device_data['locale']['language']+"_"+device_data['locale']['country']


###########################################################
# Utility methods : MAT
#
# Define/declare/Set/Unset/Increment various parameters
# required by Adjust
###########################################################
def get_user_id(app_data):
	if not app_data.get('user_id'):
		app_data['user_id']=str(util.get_random_string('hex',32)).upper()
	return app_data.get('user_id')
	
def get_idx(app_data):
	app_data['idx']=str(random.randint(10000,30000))
	return app_data.get('idx')	
	
def kochava_device_ID(campaign_data,app_data,device_data):
	if not app_data.get('kochava_device_id'):
		app_data['kochava_device_id'] ='KA331'+datetime.datetime.fromtimestamp(time.time()).strftime("%Y%m%d")+str(util.get_random_string('hex',32))
		#'KA331'+datetime.datetime.fromtimestamp(time.time()).strftime("%Y%m%d")+str(util.get_random_string('decimal',14))+str(util.get_random_string('hex',29))	

def def_appInstallData(app_data):
	if not app_data.get('appInstallDate'):
		app_data['appInstallDate'] = int(app_data.get("times").get("install_complete_time"))

def def_matID(app_data):
	if not app_data.get('matID'):
		app_data['matID'] = str(uuid.uuid4())

def set_OpenLogID(app_data):
	if not app_data.get('open_log_id') and app_data.get('log_id'):
		app_data['open_log_id'] = app_data['log_id']

def set_LastOpenLogID(app_data):
	if app_data.get('log_id'):
		app_data['last_open_log_id'] = app_data['log_id']

def catch(response,app_data,paramName):
	try:
		if paramName=="mat":
			app_data['log_id'] = json.loads(response.get('data')).get('log_id')
	except Exception as inst:
		print inst		


###########################################################
# Utility methods : MISC
#
###########################################################
def time_Stamp():
	return int(time.time())

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def def_nt_id(app_data):
	if not app_data.get('nt_id'):
		app_data['nt_id'] = str(uuid.uuid4())

def get_screen_inches(app_data):
	if not app_data.get('screen_inches'):
		app_data['screen_inches']=random.choice(['5','4','6'])


########################################################
#					   AES							   #
########################################################
class AESCipher:
	def __init__(self, key):
		self.key = key
	def pad(self, raw):
		l = len(raw) % 16
		l = 16 - l
		for x in range(l):
			raw += ' '
		return raw

	def encrypt(self, iv, raw):
		raw = self.pad(raw)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return (cipher.encrypt(raw)).encode('hex')

	def decrypt(self, enc , iv):
		enc = enc.decode('hex')
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return cipher.decrypt(enc)

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def generateLag():
	return (random.randint(10,90)*0.001)

def generate_t():
	return "%.3f" %(random.uniform(300,1000))

def mxTransactionID(app_data):
	if not app_data.get('mxTransactionID'):
		app_data['mxTransactionID']= str(util.get_random_string('hex',16))

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())

def cal_seq(app_data):
	if not app_data.get('seq'):
		app_data['seq'] = 1
	else:
		app_data['seq'] = app_data.get('seq')+1
	return app_data.get('seq')


def get_params(data_value):
	key = data_value.keys()
	a = ''
	for value in sorted(key):
		a += str(urllib.quote_plus(str(value)).encode('utf-8'))+'='+str(urllib.quote_plus(str(data_value.get(value))).encode('utf-8'))+'&'
	a = a.rsplit("&",1)[0]	

	return a


def payloadsignature(str1,str2, radix=16):
	import hashlib
	sha_obj = hashlib.sha1()
	sha_obj.update(str2.encode('utf-8'))
	sha_obj.update(str1.encode('utf-8'))
	if radix == 16:		
		return sha_obj.hexdigest()
	elif radix == 64:		
		return base64(sha_obj.digest())