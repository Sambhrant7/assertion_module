# -*- coding: utf-8 -*-
from sdk import util, installtimenew
from sdk import getsleep
from sdk import NameLists
import datetime
import time
import random
import json
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 : 'com.testbook.tbapp',
	'app_name' 			 : 'Exam Preparation App: Free Mock Tests | Live Class',
	'app_version_name'	 : '4.18.3',
	'no_referrer'		 : False,
	'device_targeting'   : True,
	'app_size'			 : 9.8,
	'supported_os'		 : '4.4',		
	'supported_countries': 'WW',
	'ctr'				 : 6,
	'tracker'			 : 'branch',
	'api_branch':{
	    'branchkey'  :'key_live_kbGnFVwdbtMxCTzjhD0QJhhltygch2KY',
	    'sdk'        :'android2.19.5',
	},

	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	

	print '______Please Wait__________________'
	installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android",min_sleep=0)

	###########		INITIALIZE		############

	def_sec(app_data,device_data)

	time.sleep(random.randint(1,2))
	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	app_data['api_hit_time']=time.time()
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'	


	time.sleep(random.randint(1,2))
	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)


	time.sleep(random.randint(60,120))
	print "Branch______event"
	event_data = {"client": "TBApp","medium": "Email","screen": "OnBoardingLogin","userID": "5d8e0498f566420e3145884b"}
	req=api_branch_event(campaign_data, app_data, device_data, event_name='signIn', event_data = event_data)
	util.execute_request(**req)


	time.sleep(random.randint(40,70))
	print "Branch______event"
	start_timestamp = int(time.time()) - int(time.time())%1800  + random.randint(2,6)*60*60
	start_time = datetime.datetime.utcfromtimestamp(start_timestamp+app_data.get('sec')).strftime("%a %b %d %H:%M:%S GMT" + device_data.get('timezone') + " %Y")
	end_time = datetime.datetime.utcfromtimestamp(start_timestamp+ 3600 +app_data.get('sec')).strftime("%a %b %d %H:%M:%S GMT" + device_data.get('timezone') + " %Y")
	event_data = {"course": "Bank Clerk","exam": "GATE CE","examList": "[GATE CE]","fromBanner": "true","isFree": "false","isLive": "true","liveTestEndTime": end_time,"liveTestStartTime": start_time,"screen": "Live Test Banner","testID": "5d8ed46cf60d5d078f1e2243","testName": "GATE CE Reward Quiz - Solid Mechanics Quiz"}
	req=api_branch_event(campaign_data, app_data, device_data, event_name='test_registered', event_data = event_data)
	util.execute_request(**req)


	time.sleep(random.randint(1,2))
	print "Branch______event"
	start_timestamp = int(time.time()) - int(time.time())%1800  + random.randint(2,6)*60*60
	start_time = datetime.datetime.utcfromtimestamp(start_timestamp+app_data.get('sec')).strftime("%a %b %d %H:%M:%S GMT" + device_data.get('timezone') + " %Y")
	end_time = datetime.datetime.utcfromtimestamp(start_timestamp+ 3600 +app_data.get('sec')).strftime("%a %b %d %H:%M:%S GMT" + device_data.get('timezone') + " %Y")
	event_data = {"screen":"Live Test Banner","liveTestStartTime":start_time,"examList":"[GATE EC]","course":"Bank Clerk","exam":"GATE EC","testName":"GATE EC Rewards Quiz - Electronic Devices","testID":"5d8e1675f60d5d078f1e1656","fromBanner":"true","liveTestEndTime":end_time,"isLive":"true","isFree":"false"}
	req=api_branch_event(campaign_data, app_data, device_data, event_name='test_registered', event_data = event_data)
	util.execute_request(**req)


	time.sleep(random.randint(10,20))
	print "Branch______event"
	event_data = {"screen":"Live Test Banner","course":"Bank Clerk","testName":"IBPS Clerk - Live Test 3","language":"English","testID":"5d88b204f60d5d5f0d99761d","fromBanner":"true","isLive":"true","isFree":"false"}
	req=api_branch_event(campaign_data, app_data, device_data, event_name='test_started', event_data = event_data)
	util.execute_request(**req)


	time.sleep(random.randint(10,20))
	print "Branch______event"
	event_data = {"screen":"Live Test Banner","method":"Manual","course":"Bank Clerk","testName":"IBPS Clerk - Live Test 3","language":"English","testID":"5d88b204f60d5d5f0d99761d","fromBanner":"true","isLive":"true","isFree":"false"}
	req=api_branch_event(campaign_data, app_data, device_data, event_name='test_submitted', event_data = event_data)
	util.execute_request(**req)


	time.sleep(random.randint(15,20))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)


		
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):
		
	return {'status':'true'}



###################################################################
################ BRANCH CALLS ###################

def branch_call(campaign_data, app_data, device_data,typ='v1'):
	url='http://cdn.branch.io/sdk/uriskiplist_'+typ+'.json'
	method='get'
	headers={
		'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
	}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': None}


def api_branch_install(campaign_data,app_data,device_data):
	method='post'
	url='http://api.branch.io/v1/install'

	headers = {
				'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
				'Content-Type':'application/json',
				'Accept':'application/json',
			}

	data={       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "cd": {       "mv": "-1", "pn": campaign_data.get('package_name')},
        #"country": device_data.get('locale').get('country'),
        "debug": False,
        "environment": "FULL_APP",
        "facebook_app_link_checked": False,
        "first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "instrumentation": {       "v1\\/install-qwt": "0"},
        "is_hardware_id_real": True,
        "is_referrable": 1,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "latest_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "latest_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "previous_update_time": 0,
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "update": 0,
        "wifi": True}
 

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}

def api2_branch_profile( campaign_data, device_data, app_data, identity='',call=1):
	url= "http://api2.branch.io/v1/profile"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data)}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "country": device_data.get('country'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity": identity,
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       "v1/install-brtt": "1179",
                                   "v1/profile-qwt": "2043"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val":"0",
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}

	if call == 2:
		data['instrumentation'] = {"v1/profile-qwt":"1","v1/profile-brtt":"285"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


def api_branch_open(campaign_data, app_data, device_data, call_seq=1):	
	
	url='http://api.branch.io/v1/open'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": "vivo",
        "cd": {       "mv": "-1", "pn": campaign_data.get('package_name')},
        "country": device_data.get('locale').get('country'),
        "debug": False,
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "environment": "FULL_APP",
        "facebook_app_link_checked": False,
        "first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       "v1/event-brtt": "385", "v1/open-qwt": "0"},
        "is_hardware_id_real": True,
        "is_referrable": 1,
        "language": "en",
        "lat_val": 0,
        "latest_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "latest_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "previous_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "update": 1,
        "wifi": True}
	if call_seq==2:
		data['instrumentation']={"v1/close-brtt":"891","v1/open-qwt":"0"}
	if call_seq==3:
		data['instrumentation']={"v1/close-brtt":"681","v1/open-qwt":"0"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}


def api_branch_close(campaign_data, app_data, device_data,call_seq=1):	
	
	url='http://api.branch.io/v1/close'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        #"country": device_data.get('locale').get('country'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        #"google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       "v1/close-qwt": "1", "v2/event/custom-brtt":"680"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        #"lat_val":"0",
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def api_branch_event(campaign_data, app_data, device_data, event_name='',event_data=''):	
	
	url='http://api.branch.io/v2/event/custom'
	header={
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "instrumentation": {       "v1\\/install-brtt": util.get_random_string('decimal', 3),
                                   "v2\\/event\\/custom-qwt": str(random.randint(0,9))},
        "metadata": {       },
        "name": event_name,
        "retryNumber": 0,
        "user_data": {       "aaid": device_data.get('adid'),
                             "android_id": device_data.get('android_id'),
                             "app_version": campaign_data.get('app_version_name'),
                             "brand": device_data.get('brand'),
                             "developer_identity": "bnc_no_value",
                             "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
                             "environment": "FULL_APP",
                             "language": device_data.get('locale').get('language'),
                             "limit_ad_tracking": 0,
                             "local_ip": device_data.get('private_ip'),
                             "model": device_data.get('model'),
                             "os": "Android",
                             "os_version": int(device_data.get('sdk')),
                             "screen_dpi": int(device_data.get('dpi')),
                             "screen_height": int(device_data.get('resolution').split('x')[0]),
                             "screen_width": int(device_data.get('resolution').split('x')[1]),
                             "sdk": "android",
                             "sdk_version": "2.19.5",
                             "user_agent": device_data.get('User-Agent'),
                             }}

	if event_data:
		data['custom_data'] = event_data


	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def connectivity_check(campaign_data, app_data, device_data):
	url = 'http://connectivitycheck.gstatic.com/generate_204'
	method = 'head'
	headers = {
		'Accept-Encoding': 'gzip',
		'User-Agent': device_data.get('User-Agent'),
	}
	params=None
	data = None
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def measurement_config(campaign_data, app_data, device_data):

	url = 'http://app-measurement.com/config/app/1%3A661240310080%3Aandroid%3A0ad3d352a734fdec'
	
	header={
		'User-Agent': get_ua(device_data),
		'Accept-Encoding': 'gzip',
	}
		
	params={
		'app_instance_id':	'ff70eed7ba918a419f66873cebbbca8d',
		'platform':	'android',
		'gmp_version':	'18382',
	}
	data=None

	return {'url': url, 'httpmethod': 'head', 'headers': header, 'params': params, 'data': data}
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)


def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"
# 		app_data['user']['pswd']		= last_name+app_data['user']['dob'].replace('-','')



