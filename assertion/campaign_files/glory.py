# -*- coding: utf-8 -*-
from sdk import util, installtimenew
from sdk import getsleep
from sdk import purchase
import time
import random
import json
import string
import datetime
import urllib
import urlparse
import base64
from Crypto.Cipher import AES
import uuid
import clicker
import Config


#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'app_size'			 : 21.0,#19.0, #27.0
	'package_name'		 :'com.ulugames.honors.google',
	'app_name' 			 :unicode('글로리',"utf-8"),
	'app_version_code'	 :'102',#'101',#'85',#77, '70',#'55',#'46',#'40',#'33',#'25',
	'app_version_name'	 :'1.0.102',#'1.0.101',#'1.0.85',#1.0.77, '1.0.70',#'1.0.55',#'1.0.46',#'1.0.40',#'1.0.33',#'1.0.25',
	'no_referrer'		 : True,
	'supported_os'		 :'4.4',		
	'supported_countries':'WW',
	'ctr'				 :6,
	'CREATE_DEVICE_MODE'	:3,#2,
	'device_targeting'		:True,
	'tracker' 			 :'appsflyer',
	'appsflyer':{
		'key'		 : 'ernx85LSi7VY3pH44rAiDW',
		'dkh'		 : 'ernx85LS',
		'buildnumber': '4.8.13',#'6.2',
		'version'	 : 'v4',
	},
	'ad-brix': {
		'app_key': '851490495',
		'ver': '4.5.2a',
		'key_getreferral': '803f7a91a53c4ed5803f7a91a53c4ed5',
		'iv_getreferral' : '803f7a91a53c4ed5',
		'key_tracking': 'srkterowgawrsozerruly82nfij625w9',
		'iv_tracking' : 'srkterowgawrsoze'
			},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:33,
		17:32,
		18:30,
		19:28,
		20:26,
		21:24,
		22:23,
		23:20,
		24:17,
		25:15,
		26:13,
		27:12,
		28:11,
		29:10,
		30:9,
		31:8,
		32:7,
		33:6,
		34:6,
		35:5,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print 'Please wait installing...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=20)
	def_eventsRecords(app_data)

	get_demo_value(app_data,device_data)
	app_data['click_time'] = int(app_data.get('times').get('click_time'))
	app_data['app_install_start_time'] = int(app_data.get('times').get('download_begin_time'))
	app_data['app_install_completed_time'] = int(app_data.get('times').get('install_complete_time'))
	app_data['app_install_completed_time_date'] = datetime.datetime.utcfromtimestamp(app_data['app_install_completed_time']).strftime("%Y%m%d%H%M%S")
	app_data['app_first_open_time'] = get_timestamp(para1=10,para2=20)
	update_activity(app_data,device_data)
	make_sec(app_data,device_data)

	app_data['create_character'] = False

	email_sha1 = ""
	for i in range(0,random.randint(1,1)):
		return_userinfo(app_data)
		email_sha1 = email_sha1+util.sha1(app_data.get('user_info').get('email'))+'|'
	
	app_data['email_ag'] = email_sha1[:len(email_sha1)-1]

	app_data['timepassed'] = int(time.time())

	###########		CALLS		################	
	print "Ad Brix Get Schedule"
	ad_brix_schedule = ad_brix_get_schedule(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**ad_brix_schedule)

	print "AD BRIX get Referral"
	get_referral = ad_brix_get_referral(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'])
	get_referral_result =util.execute_request(**get_referral)
	try:
		if json.loads(get_referral_result.get('data')).get('ResultCode')==5600:
			print "AD BRIX get Referral   (Base  16)"
			get_referral = ad_brix_get_referral(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],base_change=True)
			get_referral_result =util.execute_request(**get_referral)
			try:
				get_referral_result_decode = json.loads(get_referral_result.get('data'))
				app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
				app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))
			except:
				app_data['user_number'] = "1131947493142244150"
				app_data['shard_number'] = "14"
		get_referral_result_decode = json.loads(get_referral_result.get('data'))
		app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
		app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))
	except:
		app_data['user_number'] = "1131947493142244150"
		app_data['shard_number'] = "14"

	time.sleep(random.randint(2,4))	

	print "Ad Brix Tracking"
	ad_brix_tracking_1 = ad_brix_tracking_start(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'])
	util.execute_request(**ad_brix_tracking_1)

	time.sleep(random.randint(1,3))	

	print "Ad Brix Update Conversion"
	ad_brix_conversion= ad_brix_update_conversion(campaign_data, app_data, device_data)
	util.execute_request(**ad_brix_conversion)

	time.sleep(random.randint(8,15))	

	print "Ad Brix Get Schedule"
	ad_brix_schedule = ad_brix_get_schedule(campaign_data, app_data, device_data)
	util.execute_request(**ad_brix_schedule)

	time.sleep(random.randint(2,4))	

	print "Ad Brix Tracking"
	ad_brix_tracking_1 = ad_brix_tracking_start_2(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'])
	util.execute_request(**ad_brix_tracking_1)

	time.sleep(random.randint(3,5))	

	print "Ad Brix Get login"
	ad_brix_login= ad_brix_user_login(campaign_data, app_data, device_data)
	response=util.execute_request(**ad_brix_login)
	
	try:
		get_data=json.loads(response.get('data'))
		app_data['session_id']=get_data.get('user').get('sessionId')
	except:
		app_data['session_id']=	'4018cc614fc9c0f3'

	print "Ad-brix Users Save\n\n"
	req= ad_brix_users_save(app_data,campaign_data,device_data)	
	util.execute_request(**req)

	time.sleep(random.randint(1,3))	

	print "Ad Brix Update Registration"
	ad_brix_conversion= ad_brix_update_registration(campaign_data, app_data, device_data)
	util.execute_request(**ad_brix_conversion)

	print '\nAppsflyer : TRACK____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true",isOpen=True,timepass='-1')
	util.execute_request(**request)

	print '\nAppsflyer : API____________________________________'
	request  = appsflyer_api(campaign_data, app_data, device_data)
	util.execute_request(**request)

	timepass = random.randint(40,60)
	time.sleep(timepass)

	print "Ad Brix Tracking"
	adtg=ad_brix_tracking_SetUserDemographic(app_data, device_data)
	util.execute_request(**adtg)

	print "Ad Brix Tracking"
	adtg=ad_brix_tracking_SetUserDemographic(app_data, device_data)
	util.execute_request(**adtg)

	print '\nAppsflyer : TRACK____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false",timepass=timepass)
	util.execute_request(**request)

	if random.randint(1,100)<=90:

		time.sleep(random.randint(10,15))
		create_characterEvent(campaign_data, app_data, device_data)
		app_data['create_character'] = True
		
		call_func(app_data,device_data,call_type='install')
	
	###########		FINALIZE	################
		
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	

	if not app_data.get('times'):
		print 'Please wait installing...'
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=20)

	if not app_data.get('create_character'):
		app_data['create_character']=False

	if not app_data.get('timepassed'):
		app_data['timepassed'] = int(time.time())	

	get_demo_value(app_data,device_data)
	make_sec(app_data,device_data)
	if not app_data.get('click_time'):
		app_data['click_time'] = int(app_data.get('times').get('click_time'))
		app_data['app_install_start_time'] = int(app_data.get('times').get('download_end_time'))
		app_data['app_install_completed_time'] = int(app_data.get('times').get('install_complete_time'))
		app_data['app_install_completed_time_date'] = datetime.datetime.utcfromtimestamp(app_data['app_install_completed_time']).strftime("%Y%m%d%H%M%S")
		app_data['app_first_open_time'] = get_timestamp(para1=10,para2=20)		
	
	if not app_data.get('activity'):
		update_activity(app_data,device_data)

	if not app_data.get('email_ag'):
		email_sha1 = ""
		for i in range(0,random.randint(1,1)):
			return_userinfo(app_data)
			email_sha1 = email_sha1+util.sha1(app_data.get('user_info').get('email'))+'|'
		
		app_data['email_ag'] = email_sha1[:len(email_sha1)-1]

	if not app_data.get('user_number'):
		print "AD BRIX get Referral"
		get_referral = ad_brix_get_referral(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'])
		get_referral_result =util.execute_request(**get_referral)
		try:
			if json.loads(get_referral_result.get('data')).get('ResultCode')==5600:
				print "AD BRIX get Referral   (Base  16)"
				get_referral = ad_brix_get_referral(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],base_change=True)
				get_referral_result =util.execute_request(**get_referral)
				try:
					get_referral_result_decode = json.loads(get_referral_result.get('data'))
					app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
					app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))
				except:
					app_data['user_number'] = "1131947493142244150"
					app_data['shard_number'] = "14"
			get_referral_result_decode = json.loads(get_referral_result.get('data'))
			app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
			app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))
		except:
			app_data['user_number'] = "1131947493142244150"
			app_data['shard_number'] = "14"

	print "Ad Brix Get Schedule"
	ad_brix_schedule = ad_brix_get_schedule(campaign_data, app_data, device_data)
	util.execute_request(**ad_brix_schedule)

	time.sleep(random.randint(1,2))	

	print "tracking open"
	adbrix_event=ad_brix_tracking_open(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'])
	util.execute_request(**adbrix_event)

	time.sleep(random.randint(3,5))	

	print "Ad Brix Get login"
	ad_brix_login= ad_brix_user_login(campaign_data, app_data, device_data)
	response=util.execute_request(**ad_brix_login)
	
	try:
		get_data=json.loads(response.get('data'))
		app_data['session_id']=get_data.get('user').get('sessionId')
	except:
		app_data['session_id']=	'4018cc614fc9c0f3'

	print '\nAppsflyer : TRACK____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false")
	util.execute_request(**request)

	if app_data.get('create_character') == False:
		create_characterEvent(campaign_data, app_data, device_data)
		app_data['create_character'] =True
		

	if app_data.get('create_character') == True:

		call_func(app_data,device_data,call_type='open',day=day)	

	if purchase.isPurchase(app_data,day,advertiser_demand=20):
		time.sleep(random.randint(10,15))
		purchaseEvent(campaign_data, app_data, device_data)	

	print '\nAppsflyer : STATS____________________________________'
	request  = appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**request)

	return {'status':'true'}

def call_func(app_data,device_data,call_type='install',day=1):

	if not app_data.get('level'):
		app_data['level']=1

	if call_type=='install':
			top=random.choice([20,30,30,30,30,30,40,40,40,40])
	elif call_type=='open':
		if day==1:
			print "day is"+str(day)
			top=random.choice([10,10,15,15,15,20,20,20,30,30])
		else:
			print "day is"+str(day)
			top=random.choice([10,10,15,15,15,20,20,20,25,25])					

	if random.randint(1,100)<=95:
		for x in range(top):

			if app_data.get('level')<=150:
				if app_data.get('level')<=40:
					print app_data.get('level')
					time.sleep(random.randint(60,90))
				elif app_data.get('level')>40 and app_data.get('level')<=100:
					print app_data.get('level')
					time.sleep(random.randint(90,120))
				else:
					print app_data.get('level')
					time.sleep(random.randint(120,150))
				
				if app_data.get('level')%10==0 and app_data.get('level')<=50:
					levelEvent(campaign_data, app_data, device_data)
				elif app_data.get('level')>50 and app_data.get('level')%50==0:
					levelEvent(campaign_data, app_data, device_data)

				app_data['level']+=1	

			else:
				print "-------out of range------"
				break	

class AESCipher(object):

    def __init__(self, key, bs=32): 
        self.bs = bs
        self.key = key.encode()
		
    def encrypt(self, iv,raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')
        
    def decrypt(self, enc):
        # enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

class AESCipherTracking(object):

    def __init__(self, key): 
        self.bs = 16
        self.key = key.encode()
		
    def encrypt(self, iv,raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')
        
    def decrypt(self, enc):
        # enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

################################################################
#							adbrix
################################################################
def ad_brix_get_referral(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1,base_change=False):

	url = 'http://cvr.ad-brix.com/v1/conversion/GetReferral'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	
	
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value = '{"appkey":"'+campaign_data.get('ad-brix').get('app_key')+'","package_name":"'+campaign_data.get('package_name')+'","installer":"com.android.vending","version":"'+campaign_data.get('ad-brix').get('ver')+'","app_version_name":"'+campaign_data.get('app_version_name')+'","app_version_code":'+campaign_data.get('app_version_code')+',"referral_info":{"conversion_key":'+conversion_key_value+',"session_no":'+session_number_value+',"referrer_param":"'+app_data.get('referrer')+'"},"conversion_cache":[],"adbrix_user_info":{"adbrix_user_no":-1,"shard_no":-1,"install_datetime":"","install_mdatetime":"","life_hour":-1,"app_launch_count":1,"referral_key":-1,"reengagement_conversion_key":-1,"last_referral_key":-1,"last_referral_data":"","last_referral_datetime":"","set_referral_key":true,"sig_type":0},"user_info":{"puid":"","mudid":"","openudid":"","openudid_md5":"","openudid_sha1":"","android_id_md5":"'+util.md5(device_data.get('android_id'))+'","android_id_sha1":"'+util.sha1(device_data.get('android_id'))+'","device_id_md5":"","device_id_sha1":"","google_ad_id":"'+device_data.get('adid')+'","google_ad_id_opt_out":false,"initial_ad_id":"'+device_data.get('adid')+'","ag":"","odin":"'+util.sha1(device_data.get('android_id'))+'","carrier":"'+device_data.get('carrier')+'","country":"'+device_data.get('locale').get('country')+'","language":"'+device_data.get('locale').get('language')+'","android_id":"'+base64.b64encode(device_data.get('android_id'))+'","first_install_time":"'+app_install_completed_time_date_1+'","first_install_time_offset":'+str(app_data.get('sec'))+'},"cohort_info":{},"device_info":{"vendor":"google","model":"'+device_data.get('device')+' '+device_data.get('model')+'","kn":"'+device_data.get('kernel').split('-')[0]+'","is_wifi_only":false,"network":"wifi","noncustomnetwork":'+device_data.get('mnc')+',"os":"a_'+device_data.get('os_version')+'","ptype":"android","width":'+device_data.get('resolution').split('x')[1]+',"height":'+device_data.get('resolution').split('x')[0]+',"is_portrait":false,"utc_offset":'+str(float(app_data.get('sec'))/float(60)/float(60))+',"build_id":"'+device_data.get('build')+'"},"demographics":[],"complete_conversions":[],"activity_info":[],"impression_info":[]}'		

	if base_change:
		key = campaign_data.get('ad-brix').get('key_getreferral')
		iv = campaign_data.get('ad-brix').get('iv_getreferral')
		aes = AESCipher(key,bs=16)
		data['j'] = aes.encrypt(iv,data_value)	
	else:			
		key = campaign_data.get('ad-brix').get('key_getreferral')
		iv = campaign_data.get('ad-brix').get('iv_getreferral')
		aes = AESCipher(key)
		data['j'] = aes.encrypt(iv,data_value)
	
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}	
		
def ad_brix_tracking_start(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	event_date_1 = get_date(app_data,device_data,para1=4,para2=7)
	event_date = get_date(app_data,device_data,para1=0,para2=0)
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value = '{"appkey":"'+campaign_data.get('ad-brix').get('app_key')+'","package_name":"'+campaign_data.get('package_name')+'","installer":"com.android.vending","version":"'+campaign_data.get('ad-brix').get('ver')+'","app_version_name":"'+campaign_data.get('app_version_name')+'","app_version_code":'+campaign_data.get('app_version_code')+',"referral_info":{"conversion_key":'+conversion_key_value+',"session_no":'+session_number_value+',"referrer_param":"'+app_data.get('referrer')+'"},"conversion_cache":['+conversion_key_value+'],"adbrix_user_info":{"adbrix_user_no":'+app_data.get('user_number')+',"shard_no":'+app_data.get('shard_number')+',"install_datetime":"'+event_date_1+'","install_mdatetime":'+str(int(time.time()*1000))+',"life_hour":0,"app_launch_count":1,"referral_key":'+conversion_key_value+',"reengagement_conversion_key":-1,"last_referral_key":-1,"last_referral_data":"","last_referral_datetime":"","set_referral_key":true,"sig_type":0},"user_info":{"puid":"","mudid":"","openudid":"","openudid_md5":"","openudid_sha1":"","android_id_md5":"'+util.md5(device_data.get('android_id'))+'","android_id_sha1":"'+util.sha1(device_data.get('android_id'))+'","device_id_md5":"","device_id_sha1":"","google_ad_id":"'+device_data.get('adid')+'","google_ad_id_opt_out":false,"initial_ad_id":"'+device_data.get('adid')+'","ag":"'+email_ag_1+'","odin":"'+util.sha1(device_data.get('android_id'))+'","carrier":"'+device_data.get('carrier')+'","country":"'+device_data.get('locale').get('country')+'","language":"'+device_data.get('locale').get('language')+'","android_id":"'+base64.b64encode(device_data.get('android_id'))+'","first_install_time":"'+app_install_completed_time_date_1+'","first_install_time_offset":'+str(app_data.get('sec'))+'},"cohort_info":{},"device_info":{"vendor":"google","model":"'+device_data.get('model')+'","kn":"'+device_data.get('kernel').split('-')[0]+'","is_wifi_only":false,"network":"wifi","noncustomnetwork":'+device_data.get('mnc')+',"os":"a_'+device_data.get('os_version')+'","ptype":"android","width":'+device_data.get('resolution').split('x')[1]+',"height":'+device_data.get('resolution').split('x')[0]+',"is_portrait":false,"utc_offset":'+str(float(app_data.get('sec'))/float(60)/float(60))+',"build_id":"'+device_data.get('build')+'"},"demographics":[],"complete_conversions":[],"activity_info":[{"prev_group":"","prev_activity":"","group":"session","activity":"retention","param":"","event_id":"be14677b-de9c-4230-bb27-5f20c5a7e4b5","created_at":"'+event_date+'"},{"prev_group":"","prev_activity":"","group":"session","activity":"start","param":"","event_id":"0bfa1fdb-33f9-44f1-9029-e2f4d076ed92","created_at":"'+event_date+'"}],"impression_info":[]}'
	

	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)
	
	update_activity(app_data,device_data,'session','start')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}		

def ad_brix_tracking_start_2(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	event_date_1 = get_date(app_data,device_data,para1=4,para2=7)
	event_date = get_date(app_data,device_data,para1=0,para2=0)
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value = '{"appkey":"'+campaign_data.get('ad-brix').get('app_key')+'","package_name":"'+campaign_data.get('package_name')+'","installer":"com.android.vending","version":"'+campaign_data.get('ad-brix').get('ver')+'","app_version_name":"'+campaign_data.get('app_version_name')+'","app_version_code":'+campaign_data.get('app_version_code')+',"referral_info":{"conversion_key":'+conversion_key_value+',"session_no":'+session_number_value+',"referrer_param":"'+app_data.get('referrer')+'"},"conversion_cache":['+conversion_key_value+'],"adbrix_user_info":{"adbrix_user_no":'+app_data.get('user_number')+',"shard_no":'+app_data.get('shard_number')+',"install_datetime":"'+event_date_1+'","install_mdatetime":'+str(int(time.time()*1000))+',"life_hour":0,"app_launch_count":2,"referral_key":'+conversion_key_value+',"reengagement_conversion_key":-1,"last_referral_key":-1,"last_referral_data":"","last_referral_datetime":"","set_referral_key":true,"sig_type":0},"user_info":{"puid":"","mudid":"","openudid":"","openudid_md5":"","openudid_sha1":"","android_id_md5":"'+util.md5(device_data.get('android_id'))+'","android_id_sha1":"'+util.sha1(device_data.get('android_id'))+'","device_id_md5":"","device_id_sha1":"","google_ad_id":"'+device_data.get('adid')+'","google_ad_id_opt_out":false,"initial_ad_id":"'+device_data.get('adid')+'","ag":"'+email_ag_1+'","odin":"'+util.sha1(device_data.get('android_id'))+'","carrier":"'+device_data.get('carrier')+'","country":"'+device_data.get('locale').get('country')+'","language":"'+device_data.get('locale').get('language')+'","android_id":"'+base64.b64encode(device_data.get('android_id'))+'","first_install_time":"'+app_install_completed_time_date_1+'","first_install_time_offset":'+str(app_data.get('sec'))+'},"cohort_info":{},"device_info":{"vendor":"google","model":"'+device_data.get('model')+'","kn":"'+device_data.get('kernel').split('-')[0]+'","is_wifi_only":false,"network":"wifi","noncustomnetwork":'+device_data.get('mnc')+',"os":"a_'+device_data.get('os_version')+'","ptype":"android","width":'+device_data.get('resolution').split('x')[1]+',"height":'+device_data.get('resolution').split('x')[0]+',"is_portrait":false,"utc_offset":'+str(float(app_data.get('sec'))/float(60)/float(60))+',"build_id":"'+device_data.get('build')+'"},"demographics":[],"complete_conversions":[],"activity_info":[{"prev_group":"","prev_activity":"","group":"session","activity":"start","param":"","event_id":"a748aba3-61d6-4723-a200-b2952f83e056","created_at":"'+event_date+'"}],"impression_info":[]}'
	
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)
	
	update_activity(app_data,device_data,'session','start')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}		
			

def ad_brix_tracking_event(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1,group='',activity=''):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		cb_param1_value = " "
		cb_param2_value = " "
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	event_date_1 = get_date(app_data,device_data,para1=4,para2=7)
	event_date = get_date(app_data,device_data,para1=0,para2=0)
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value ='{"appkey":"'+campaign_data.get('ad-brix').get('app_key')+'","package_name":"'+campaign_data.get('package_name')+'","installer":"unknown","version":"'+campaign_data.get('ad-brix').get('ver')+'","app_version_name":"'+campaign_data.get('app_version_name')+'","app_version_code":'+campaign_data.get('app_version_code')+',"referral_info":{"conversion_key":'+conversion_key_value+',"session_no":'+session_number_value+',"referrer_param":"'+app_data.get('referrer')+'"},"conversion_cache":['+conversion_key_value+'],"adbrix_user_info":{"adbrix_user_no":'+app_data.get('user_number')+',"shard_no":'+app_data.get('shard_number')+',"install_datetime":"'+event_date_1+'","install_mdatetime":'+str(int(time.time()*1000))+',"life_hour":0,"app_launch_count":1,"referral_key":'+conversion_key_value+',"referral_data":"'+referrer_additional_info+'","reengagement_conversion_key":-1,"last_referral_key":-1,"last_referral_data":"","last_referral_datetime":"","set_referral_key":true,"sig_type":0},"user_info":{"puid":"","mudid":"","openudid":"","openudid_md5":"","openudid_sha1":"","android_id_md5":"'+util.md5(device_data.get('android_id'))+'","android_id_sha1":"'+util.sha1(device_data.get('android_id'))+'","device_id_md5":"","device_id_sha1":"","google_ad_id":"'+device_data.get('adid')+'","google_ad_id_opt_out":false,"initial_ad_id":"'+device_data.get('adid')+'","ag":"'+email_ag_1+'","odin":"'+util.sha1(device_data.get('android_id'))+'","carrier":"'+device_data.get('carrier')+'","country":"'+device_data.get('locale').get('country')+'","language":"'+device_data.get('locale').get('language')+'","android_id":"'+base64.b64encode(device_data.get('android_id'))+'","first_install_time":"'+app_install_completed_time_date_1+'","first_install_time_offset":'+str(app_data.get('sec'))+'},"cohort_info":{},"device_info":{"vendor":"google","model":"'+device_data.get('model')+'","kn":"'+device_data.get('kernel').split('-')[0]+'","is_wifi_only":false,"network":"wifi","noncustomnetwork":'+device_data.get('mnc')+',"os":"a_'+device_data.get('os_version')+'","ptype":"android","width":'+device_data.get('resolution').split('x')[1]+',"height":'+device_data.get('resolution').split('x')[0]+',"is_portrait":false,"utc_offset":'+str(float(app_data.get('sec'))/float(60)/float(60))+',"build_id":"'+device_data.get('build')+'"},"demographics":[],"complete_conversions":[],"activity_info":[{"prev_group":"'+app_data.get('group')+'","prev_activity":"'+app_data.get('activity')+'","group":"'+group+'","activity":"'+activity+'","param":"","event_id":"'+str(uuid.uuid4())+'","created_at":"'+event_date+'"}],"impression_info":[],"installation_info":{"market_referrer":"'+urlparse.unquote(app_data.get('referrer'))+'","install_actions_timestamp":{"market_install_btn_clicked":'+str(click_time_1)+',"app_install_start":'+str(app_install_start_time_1)+',"app_install_completed":'+str(app_install_completed_time_1)+',"app_first_open":'+str(app_first_open_time_1)+'}},"request_info":{"client_timestamp":'+str(int(time.time()))+'}}'

			
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)

	update_activity(app_data,device_data,group,activity)
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}		

def ad_brix_tracking_open(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		cb_param1_value = " "
		cb_param2_value = " "
			
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	event_date_1 = get_date(app_data,device_data,para1=4,para2=7)
	event_date = get_date(app_data,device_data,para1=0,para2=0)
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value ='{"appkey":"'+campaign_data.get('ad-brix').get('app_key')+'","package_name":"'+campaign_data.get('package_name')+'","installer":"com.android.vending","version":"'+campaign_data.get('ad-brix').get('ver')+'","app_version_name":"'+campaign_data.get('app_version_name')+'","app_version_code":'+campaign_data.get('app_version_code')+',"referral_info":{"conversion_key":'+conversion_key_value+',"session_no":'+session_number_value+',"referrer_param":"'+app_data.get('referrer')+'"},"conversion_cache":['+conversion_key_value+'],"adbrix_user_info":{"adbrix_user_no":'+app_data.get('user_number')+',"shard_no":'+app_data.get('shard_number')+',"install_datetime":"'+event_date_1+'","install_mdatetime":'+str(int(time.time()*1000))+',"life_hour":0,"app_launch_count":1,"referral_key":'+conversion_key_value+',"reengagement_conversion_key":-1,"last_referral_key":-1,"last_referral_data":"","last_referral_datetime":"","set_referral_key":true,"sig_type":0},"user_info":{"puid":"","mudid":"","openudid":"","openudid_md5":"","openudid_sha1":"","android_id_md5":"'+util.md5(device_data.get('android_id'))+'","android_id_sha1":"'+util.sha1(device_data.get('android_id'))+'","device_id_md5":"","device_id_sha1":"","google_ad_id":"'+device_data.get('adid')+'","google_ad_id_opt_out":false,"initial_ad_id":"'+device_data.get('adid')+'","ag":"'+email_ag_1+'","odin":"'+util.sha1(device_data.get('android_id'))+'","carrier":"'+device_data.get('carrier')+'","country":"'+device_data.get('locale').get('country')+'","language":"'+device_data.get('locale').get('language')+'","android_id":"'+base64.b64encode(device_data.get('android_id'))+'","first_install_time":"'+app_install_completed_time_date_1+'","first_install_time_offset":'+str(app_data.get('sec'))+'},"cohort_info":{},"device_info":{"vendor":"google","model":"'+device_data.get('model')+'","kn":"'+device_data.get('kernel').split('-')[0]+'","is_wifi_only":false,"network":"wifi","noncustomnetwork":'+device_data.get('mnc')+',"os":"a_'+device_data.get('os_version')+'","ptype":"android","width":'+device_data.get('resolution').split('x')[1]+',"height":'+device_data.get('resolution').split('x')[0]+',"is_portrait":false,"utc_offset":'+str(float(app_data.get('sec'))/float(60)/float(60))+',"build_id":"'+device_data.get('build')+'"},"demographics":[{"demo_key":"userId","demo_value":"'+app_data.get('demo_value')+'"}],"complete_conversions":[],"activity_info":[{"prev_group":"'+app_data.get('group')+'","prev_activity":"'+app_data.get('activity')+'","group":"session","activity":"end","param":"","event_id":"'+str(uuid.uuid4())+'","created_at":"'+event_date+'"},{"prev_group":"session","prev_activity":"end","group":"session","activity":"start","param":"","event_id":"'+str(uuid.uuid4())+'","created_at":"'+event_date+'"},{"prev_group":"","prev_activity":"","group":"session","activity":"retention","param":"","event_id":"'+str(uuid.uuid4())+'","created_at":"'+event_date+'"}],"impression_info":[]}'
		
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)

	update_activity(app_data,device_data,'session','start')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


def ad_brix_getpopups(app_data,campaign_data,device_data,callno=None):
	url='https://as.ad-brix.com/v1/users/getPopups'
	method='post'
	headers={

	'Accept-Charset':'UTF-8',
	'Accept-Encoding':'gzip',
	'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
	'Content-Type':'application/json; charset=utf-8',

	}

	if callno==2:
		r=',"revision":'+str(app_data.get('revision'))+'}'
	else:
		r=',"revision":0}'
			
	data='{"appKey":"'+campaign_data.get('ad-brix').get('app_key')+'"'+r

	params=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


def adbrix_purchase(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1):
	url='http://apiab4c.ad-brix.com/v1/event'

	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		cb_param1_value = " "
		cb_param2_value = " "
			
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	event_date_1 = get_date(app_data,device_data,para1=4,para2=7)
	event_date = get_date(app_data,device_data,para1=0,para2=0)
	
	data={"appkey":campaign_data.get('ad-brix').get('app_key'),
	"package_name":campaign_data.get('package_name'),
	"installer":"com.android.vending",
	"version":campaign_data.get('ad-brix').get('ver'),
	"app_version_name":campaign_data.get('app_version_name'),
	"app_version_code":campaign_data.get('app_version_code'),
	"referral_info":{"conversion_key":conversion_key_value,
	"session_no":session_number_value,
	"referrer_param":app_data.get('referrer')},
	"conversion_cache":[conversion_key_value],
	"adbrix_user_info":{"adbrix_user_no":app_data.get('shard_number'),
	"shard_no":app_data.get('shard_number'),
	"install_datetime":event_date_1,
	"install_mdatetime":int(time.time()*1000),
	"life_hour":95,
	"app_launch_count":4,
	"referral_key":conversion_key_value,
	"referral_data":referrer_additional_info,
	"reengagement_conversion_key":-1,
	"last_referral_key":-1,
	"last_referral_data":"",
	"last_referral_datetime":"",
	"set_referral_key":True,
	"sig_type":0},
	"user_info":{"puid":"",
	"mudid":"",
	"openudid":"",
	"openudid_md5":"",
	"openudid_sha1":"",
	"android_id_md5":util.md5(device_data.get('android_id')),
	"android_id_sha1":util.sha1(device_data.get('android_id')),
	"device_id_md5":"",
	"device_id_sha1":"",
	"google_ad_id":device_data.get('adid'),
	"google_ad_id_opt_out":False,
	"initial_ad_id":device_data.get('adid'),
	"ag":email_ag_1,
	"odin":util.sha1(device_data.get('android_id')),
	"carrier":device_data.get('carrier'),
	"country":device_data.get('locale').get('country'),
	"language":device_data.get('locale').get('language'),
	"android_id":base64.b64encode(device_data.get('android_id')),
	"first_install_time":app_install_completed_time_date_1,
	"first_install_time_offset":str(app_data.get('sec'))},
	"cohort_info":{},
	"device_info":{"vendor":"google",
	"model":device_data.get('model'),
	"kn":device_data.get('kernel'),
	"is_wifi_only":False,
	"network":"wifi",
	"noncustomnetwork":device_data.get('mnc'),
	"os":'a_'+device_data.get('os_version'),
	"ptype":"android",
	"width":device_data.get('resolution').split('x')[1],
	"height":device_data.get('resolution').split('x')[0],
	"is_portrait":False,
	"utc_offset":str(float(app_data.get('sec'))/float(60)/float(60)),
	"build_id":device_data.get('build')},
	"demographics":[{"demo_key":"userId",
	"demo_value":app_data.get('demo_value')}],
	"complete_conversions":[],
	"activity_info":[],
	"impression_info":[],
	"installation_info":{"market_referrer":urlparse.unquote(app_data.get('referrer')),
	"install_actions_timestamp":{"market_install_btn_clicked":click_time_1,
	"app_install_start":app_install_start_time_1,
	"app_install_completed":app_install_completed_time_1,
	"app_first_open":app_first_open_time_1}},
	"request_info":{"client_timestamp":int(time.time())},
	"commerce_events":[{"event_type":"purchase",
	"event_id":str(uuid.uuid4()),
	"created_at":event_date,
	"attributes":{"order_id":"GPA."+str(util.get_random_string('decimal',4))+"-"+str(util.get_random_string('decimal',4))+"-"+str(util.get_random_string('decimal',4))+"-"+str(util.get_random_string('decimal',4)),
	"discount":0,
	"delivery_charge":0,
	"payment_method":"MobilePayment"},
	"products":[{"product_id":"g15f5f_py2_google_010",
	"product_name":"Week_Normal_3300",
	"price":3300,
	"discount":0,
	"quantity":1,
	"currency":"KRW",
	"extra_attrs":{"category1":"Week_Normal_3300"}}]}]}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':json.dumps(data)}

def update_activity(app_data,device_data,group='',activity=''):
	app_data['group']=group
	app_data['activity']=activity

		
def ad_brix_tracking_SetUserDemographic(app_data, device_data):

	url = 'http://tracking.ad-brix.com/v1/tracking/SetUserDemographic'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value = '{"puid":"","google_ad_id":"'+device_data.get('adid')+'","user_demo_info":[{"demo_key":"userId","demo_value":"'+app_data.get('demo_value')+'"}]}'	
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}		

def get_demo_value(app_data,device_data):
	if not app_data.get('demo_value'):
		app_data['demo_value']=device_data.get('android_id')

def ad_brix_get_schedule(campaign_data, app_data, device_data):

	method = 'post'
	
	url = 'http://campaign.ad-brix.com/v1/CampaignVer2/GetSchedule'
	
	header={
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
		}
	data = {
				'checksum': 0,
				'co' : device_data.get('locale').get('country'),
				'google_ad_id': device_data.get('adid'),
				'k': campaign_data.get('ad-brix').get('app_key'),
				'la': device_data.get('locale').get('language'),
				'os': 'a_'+device_data.get('os_version'),
				'puid': '',	
				'version' : 'a_'+device_data.get('os_version')
	}
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}		

def ad_brix_user_login(campaign_data, app_data, device_data):

	method = 'post'
	
	url = 'https://as.ad-brix.com/v1/users/login'
	
	header={
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/json; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
		}
		
	data = '{"appKey":"'+campaign_data.get('ad-brix').get('app_key')+'","adid":"'+device_data.get('adid')+'","userId":"'+app_data.get('demo_value')+'","deviceType":"Android"}'
	
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}	

def ad_brix_users_save(app_data,campaign_data,device_data):
	url='https://as.ad-brix.com/v1/users/save'
	method='post'
	headers={

	'Accept-Charset':'UTF-8',
	'Accept-Encoding':'gzip',
	'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
	'Content-Type':'application/json; charset=utf-8',
	}

	data={
	"appKey":campaign_data.get('ad-brix').get('app_key'), 
	"puid":device_data.get('adid'), 
	"attrs":{
	"gp1":device_data.get('locale').get('country').upper(), 
	}, 
	"sessionId":app_data.get('session_id')
	}

	params=None


	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def ad_brix_user_enablePushService(campaign_data, app_data, device_data):

	method = 'post'

	url = 'https://as.ad-brix.com/v1/users/enablePushService'

	header={
	'Accept-Charset': 'UTF-8',
	'Content-Type': 'application/json; charset=utf-8',
	'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
	'Accept-Encoding': 'gzip',
	}
	data='{"appKey":"'+campaign_data.get('ad-brix').get('app_key')+'","puid":"'+device_data.get('adid')+'","enable":true,"sessionId":"'+app_data.get('session_id')+'"}'

	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}


def ad_brix_update_conversion(campaign_data, app_data, device_data):

	method = 'post'
	
	url = 'https://as.ad-brix.com/v1/users/updateConversion'
	
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
	
	header={
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/json; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
		}
		
	data = '{"appKey":"'+campaign_data.get('ad-brix').get('app_key')+'","adid":"'+device_data.get('adid')+'","ck":'+conversion_key_value+',"sub_ck":-1}'
	
	
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}	
	

def ad_brix_update_registration(campaign_data, app_data, device_data):
	method = 'post'
	def_gcmToken(app_data)
	url = 'https://as.ad-brix.com/v1/users/updateRegistration'
	
	header={
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/json; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
		}
		
	data = 	'{"appKey":"'+campaign_data.get('ad-brix').get('app_key')+'","puid":"","adid":"'+device_data.get('adid')+'","registrationId":"'+app_data.get('gcm')+'","sessionId":"'+app_data.get('session_id')+'","userId":"'+app_data.get('demo_value')+'"}'

	
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}	
################################################################
#
#				
#
################################################################
def def_gcmToken(app_data):
	if not app_data.get('gcm'):
		app_data['gcm']='APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def lineyou_platform_key(campaign_data, app_data, device_data):
	
	boundary='3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f'
	header={
		'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('brand')+' '+device_data.get('model')+' Build/'+  device_data.get('build')+')QWHttpClient',
		'Content-Type': 'multipart/form-data; boundary='+boundary
	}

	lines = []
	CRLF = '\r\n'
	
	lines.append('--'+boundary)
	lines.append('Content-Disposition: form-data; name="data"')
	lines.append('')
	lines.append('{"platform":"8f08421975b87fe5c7f0534f3ecdf4c5","game":"fengyun31","adChannelId":"0","adChannelChildId":"0"}')
	lines.append('--'+boundary)
	lines.append('Content-Disposition: form-data; name="sign"')
	lines.append('')
	lines.append('0e2f23cf9019d729c94d40bbd58b98ea')
	lines.append('--'+boundary)
	lines.append('')
	lines.append('--'+boundary)	
	
	data = CRLF.join(lines)

	return {'url': 'http://g.linnyou.com/api/platformKey', 'httpmethod': 'post', 'headers': header, 'params': None, 'data': data}

def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	app_data['gender'] = random.choice(['male', 'female'])
	gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	user_data = util.generate_name(gender=gender_n)
	user_info = {}
	user_info['username'] = user_data.get('username').lower()
	user_info['email'] = user_data.get('username').lower() + '@gmail.com'
	user_info['f_name'] = user_data.get('firstname')
	user_info['l_name'] = user_data.get('lastname')
	user_info['sex'] = gender_n
	user_info['gender'] = str(1) if gender_n == 'female' else str(2)
	user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
	user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
	user_info['password'] = util.get_random_string(type='all',size=16)
	app_data['user_info'] = user_info	
	
	return app_data.get('user_info')

def get_date(app_data,device_data,para1=0,para2=0):
	time.sleep(random.randint(para1,para2))
	# time.sleep(random.randint(0,0))
	make_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())).strftime("%Y%m%d%H%M%S")
	return date

def get_timestamp(para1=0,para2=0):
	time.sleep(random.randint(para1,para2))
	# time.sleep(random.randint(0,0))
	time1 = int(time.time())
	return time1

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def create_characterEvent(campaign_data, app_data, device_data):
	
	app_data['af_content_id'] = af_content_id()

	print '\nAppsflyer : EVENT____________________________'
	timeSinceLastCall = random.randint(10,99)
	eventName		  = "create_character"
	eventValue		  = json.dumps({"af_content_id":app_data.get('af_content_id')})
	request	= appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=timeSinceLastCall,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def enterinstanceEvent(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT____________________________'
	timeSinceLastCall = random.randint(10,99)
	eventName		  = "enterinstance"
	eventValue		  = json.dumps({"af_content_id":app_data.get('af_content_id')})
	request	= appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=timeSinceLastCall,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def levelEvent(campaign_data, app_data, device_data):
	print '\nAppsflyer : EVENT____________________________'
	timeSinceLastCall = random.randint(10,99)
	eventName		  = "level_"+str(app_data.get('level'))
	eventValue		  = '{'+eventName+':'+u"升级到".encode('utf-8')+str(app_data.get('level'))+u"级".encode('utf-8')+'}'
	request	= appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=timeSinceLastCall,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def purchaseEvent(campaign_data, app_data, device_data):
	r=random.choice([0,0,0,0,0,0,1,1,1,1,2,2,2,2,3,3])
	pur_list=[["50다이아","50_gems","1100"],["150다이아","150_gems","3300"],["250다이아","250_gems","5500"],["500다이아","500_gems","11000"]]

	print '\nAppsflyer : EVENT____________________________purchase'
	timeSinceLastCall = random.randint(10,99)
	eventName		  = "af_purchase"
	content_id        = "GPA."+ util.get_random_string('decimal',4) +"-"+ util.get_random_string('decimal',4) +"-"+ util.get_random_string('decimal',4) +"-" + util.get_random_string('decimal',5)
	eventValue		  = '{"af_content_type":"'+unicode(pur_list[r][0],"utf-8")+'","af_content_id":"'+content_id+'","af_revenue":"'+pur_list[r][2]+'","af_currency":"KRW"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=timeSinceLastCall,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=False,timepass=''):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	make_battery_level(app_data)

	method  = 'post'
	url 	= 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}		
	params  = {
		'app_id'		  : campaign_data.get('package_name'),
		'buildnumber'	  : campaign_data.get('appsflyer').get('buildnumber'),
		}	
	data    = {
		"advertiserId"			: device_data.get('adid'),
		"advertiserIdEnabled"	: "true",
		"af_preinstalled"		: "false",
		"af_timestamp"			: timestamp(),
		"app_version_code"		: campaign_data.get('app_version_code'),
		"app_version_name"		: campaign_data.get('app_version_name'),
		"appsflyerKey"			: campaign_data.get('appsflyer').get('key'),
		"brand"					: device_data.get('brand'),
		"carrier"				: device_data.get('carrier'),
		"country"				: device_data.get('locale').get('country'),
		"date1"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%d_%H%M%S")+'+0000',
		"date2"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%d_%H%M%S")+'+0000',
		"device"				: device_data.get('device'),
		"deviceType"			: "user",
		"firstLaunchDate"		: app_data.get('firstLaunchDate'),
		"iaecounter"			: str(app_data.get('iaecounter')),
		"installDate"			: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%d_%H%M%S")+'+0000',
		"isFirstCall"			: isFirstCall,
		"lang"					: util.get_language_name(device_data.get('locale').get('language')),
		"lang_code"				: device_data.get('locale').get('language'),
		"model"					: device_data.get('model'),
		"network"				: "WIFI",
		"operator"				: device_data.get('carrier'),
		"product"				: device_data.get('product'),
		"sdk"					: device_data.get('sdk'),	
		"uid"					: app_data.get('uid'),
		"af_v"					: app_data.get('af_v'),
		"af_events_api"			:"1",
		# "deviceFingerPrintId"	: app_data.get('deviceFingerPrintId'),
		"installer_package"		: 'com.android.vending',
		"isGaidWithGps"			:"true",
		"platformextension"		:"android_unity",
		'registeredUninstall'	: False,
		"timepassedsincelastlaunch"   :timepass,
		"counter"				: str(app_data.get('counter')),
		# "android_id"			: device_data.get('android_id'),
		'cksm_v1'					:'123',
		# "imei"					:device_data.get("imei"),
		'tokenRefreshConfigured':False,
		"deviceData": {
					"btch":"no",
					"build_display_id":device_data.get('build'),
					"arch":"",
					"cpu_abi":device_data.get('cpu_abi'),
					"btl":get_batteryLevel(app_data),
					"cpu_abi2":device_data.get('cpu_abi'),
					"dim":{"y_px":device_data.get('resolution').split('x')[1],"x_px":device_data.get('resolution').split('x')[0],"size":"3","xdp":device_data.get('dpi')+".0","d_dpi":device_data.get('dpi'),"ydp":device_data.get('dpi')+".0"}
				 }
		}			
	if app_data.get('referrer'):
		data['referrer'] = urllib.unquote(app_data.get('referrer'))
		data['extraReferrers'] = '{'+'"'+urllib.unquote(app_data.get('referrer'))+'"'+':'+'"['+timestamp()+','+inc_timestamp()+']"'+'}'
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+\
						data.get('uid')[0:7]+\
						data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+\
						data.get('af_timestamp')+\
						data.get('uid')+\
						data.get('installDate')+\
						data.get('counter')+\
						data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isOpen:
		data['af_sdks'] 	 = "0000000000"
		data['batteryLevel'] = get_batteryLevel(app_data)

	data['cksm_v1'] = checksum(data.get("date1"),data.get("af_timestamp"))	

	if app_data.get('counter')<=2:	
		data["deviceData"]["sensors"] =  [{
										"sT": 2,
										"sV": "MTK",
										"sVS": [-40.8, 85.598, -39.114],
										"sN": "MAGNETOMETER"
									}, {
										"sT": 1,
										"sV": "MTK",
										"sN": "ACCELEROMETER"
									}, {
										"sT": 4,
										"sV": "MTK",
										"sN": "GYROSCOPE"
									}]

	if isFirstCall=='false':
		data['timepassedsincelastlaunch'] = int(time.time())-app_data.get('timepassed')	
		app_data['timepassed'] = int(time.time())
	else:
		app_data['timepassed'] = int(time.time())		

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)

	url 	= 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	method 	= 'head'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent'	  : get_ua(device_data),
		}
	params 	= {
		'devkey'		  : campaign_data.get('appsflyer').get('key'),
		'device_id' 	  : app_data.get('uid')
		}
	data 	= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_stats()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Appsflyer's behaviour whenever user leaves app's context
# example : User drags notification bar,
#			Facebook SignUp page opens in app, etc
###################################################################
def appsflyer_stats(campaign_data, app_data, device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	inc_(app_data,'launchCounter')

	method 	= 'post'
	url 	= 'https://stats.appsflyer.com/stats' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}
	params = None
	data   = {
		"advertiserId"			: device_data.get('adid'),
		"app_id"				: campaign_data.get('package_name'),
		"channel"				: '(null)',
		"deviceFingerPrintId"	: app_data.get('deviceFingerPrintId'),
		"devkey"				: campaign_data.get('appsflyer').get('key'),
		"gcd_conversion_data_timing": "0",
		"launch_counter"		: str(app_data.get('launchCounter')),
		"originalAppsflyerId"	: app_data.get('uid'),
		"platform"				: "Android",
		"statType"				: "user_closed_app",
		"time_in_app"			: str(int(random.randint(20,60))),
		"uid"					: app_data.get('uid'),
		}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################
def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')

	method 	= 'post'
	url 	= 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}
	params  = {
		'app_id'		  : campaign_data.get('package_name'),
		'buildnumber'	  : campaign_data.get('appsflyer').get('buildnumber'),
		}	
	data 	= {
		"advertiserId"			: device_data.get('adid'),
		"advertiserIdEnabled"	: "true",
		"af_events_api"			: "1",
		"af_preinstalled"		: "false",
		"af_timestamp"			: timestamp(),
		# "android_id"			: device_data.get('android_id'),
		"app_version_code"		: campaign_data.get('app_version_code'),
		"app_version_name"		: campaign_data.get('app_version_name'),
		"appsflyerKey"			: campaign_data.get('appsflyer').get('key'),
		"brand"					: device_data.get('brand'),
		"carrier"				: device_data.get('carrier'),
		"counter"				: str(app_data.get('counter')),
		"country"				: device_data.get('locale').get('country'),
		"date1"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%d_%H%M%S")+'+0000',
		"date2"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%d_%H%M%S")+'+0000',
		"device"				: device_data.get('device'),
		# "deviceFingerPrintId"	: app_data.get('deviceFingerPrintId'),
		"deviceType"			: "user",
		"firstLaunchDate"		: app_data.get('firstLaunchDate'),
		"iaecounter"			: str(app_data.get('iaecounter')),
		"installDate"			: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%d_%H%M%S")+'+0000',
		"installer_package"		: "com.android.vending",
		"isFirstCall"			: "false",
		"isGaidWithGps"			: "true",
		"lang"					: util.get_language_name(device_data.get('locale').get('language')),
		"lang_code"				: device_data.get('locale').get('language'),
		"model"					: device_data.get('model'),
		"network"				: "WIFI",
		"operator"				: device_data.get('carrier'),
		"platformextension"		: "android_unity",
		"product"				: device_data.get('product'),
		'registeredUninstall'	: False,
		"sdk"					: device_data.get('sdk'),	
		# "timepassedsincelastlaunch": str(timeSinceLastCall),
		"uid"					: app_data.get('uid'),
		# "imei"					:device_data.get("imei"),
		'cksm_v1'					:'123',
		'tokenRefreshConfigured':False,
		"deviceData": {
					"arch":"",
					"build_display_id":device_data.get('build'),
					"cpu_abi":device_data.get('cpu_abi'),
					"cpu_abi2":device_data.get('cpu_abi'),
					"dim":{"y_px":device_data.get('resolution').split('x')[1],"x_px":device_data.get('resolution').split('x')[0],"size":"3","xdp":device_data.get('dpi')+".0","d_dpi":device_data.get('dpi'),"ydp":device_data.get('dpi')+".0"}}

	}			
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+\
						data.get('uid')[0:7]+\
						data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+\
						data.get('af_timestamp')+\
						data.get('uid')+\
						data.get('installDate')+\
						data.get('counter')+\
						data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName
	data['referrer'] = urllib.unquote(app_data.get('referrer'))
	data['extraReferrers'] = '{'+'"'+urllib.unquote(app_data.get('referrer'))+'"'+':'+'"['+timestamp()+','+inc_timestamp()+']"'+'}'

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and \
			app_data.get('prev_event_value') and \
			app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
	update_eventsRecords(app_data,eventName,eventValue)
	data['cksm_v1'] = checksum(data.get("date1"),data.get("af_timestamp"))	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

#####################################################################################
#
#					CHECKSUM GENERATE
#
#####################################################################################

def checksum(date1,af_time):

	string = "google.ulugames.honors.comcom.ulugames.honors.googlecom.ulugames.honors.googletrue"+date1+af_time
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	

		
	sb = md5_result

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')


	n = af_time
	n4 = 0
	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
	 		
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100
		
	sb = insert_char(sb,23,str(n3))
	print sb

	return sb

################################################################
#
#			Methods
#
################################################################
def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()

def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = 'ffffffff-'+random_string(4)+'-'+random_string(4)+'-0000-0000'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('battery_level_'):
		app_data['battery_level_'] = str(random.randint(10,100))+".0"
	return app_data.get('battery_level_')

def cksm(app_data):
	cksm_data= util.get_random_string('hex',34)

	return cksm_data

def make_battery_level(app_data):
	app_data['battery_level'] = str(random.randint(30,90))+'.'+str(util.get_random_string('decimal',6))


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%d_%H%M%S")+'+0000'

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))


###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def af_content_id():
	return util.get_random_string('decimal',10)

def inc_timestamp():
	return  str(int(time.time()*1000 + random.randint(11,99)))

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')

#######################################################################
#
#				Neccessary Methods
#
#######################################################################	
def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

