# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from Crypto.Cipher import AES
from sdk import util
from sdk import NameLists
import time
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config
import hashlib
import base64


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.netmarble.kofkr',
	'app_size'			 : 79.0,
	'app_name' 			 :'KOF올스타',
	'app_version_name' 	 :'1.4.3',
	'app_version_code' 	 :'201040301',
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'			 : 'apsalar',
	'mat'		 	   : {
		'ver'		   : '5.0.2',
		'advertiser_id': '175038',
		'sdk'		   : 'android',
		'key'		   : '8fb464cc8e7ab83b455c3c3830e7ab41',
		'iv'		   : 'heF9BATUfWuISyO8',
	},
	'apsalar'		 	    : {
		'version'		    : 'Singular/v7.4.0.PROD',
		'key'				: 'netmarblekr_9b38e90b',
		'secret'		    : 'c52013bac93dfe2b0c9bf61a35d10452',
	},
	'kochava'		 	    : {
		'sdk_version'		: 'AndroidTracker 3.4.0 (Unity 3.2.0)',
		'sdk_protocol'		: '11',
		'kochava_app_id'	: 'kokof-all-star-kr-android-pw6a1y',
		'sdk_build_date'	: '2018-05-01T20:19:09Z (2018-05-03T17:07:46Z)',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	

	###########		INITIALIZE		############

	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	def_sec(app_data,device_data)
	app_data['battery_level']=random.randint(5,85)
	app_data['screen_brightness']=round(random.uniform(0,1), 4)
	app_data['volume']=random.randint(0,1)

	app_data['nt_id_first']=util.get_random_string('hex',5)+'-'

	app_data['call_run_time']=time.time()

 	###########	 CALLS		 ############
		
	print '\n'+"-----------------------------MAT----session---------------------------------"
	call = mat_serve(campaign_data, app_data, device_data,action="session")
	app_data['api_hit_time']=time.time()
	response = util.execute_request(**call)
	catch(response,app_data,"mat")
	set_matOpenLogID(app_data)
		
	print '\n'+"-----------------------------Kochava kvinit---------------------------------"
	Kv_init=kochavaKvinit(campaign_data, app_data, device_data)
	util.execute_request(**Kv_init)
		
	print '\n'+"-----------------------------APSALAR Resolve---------------------------------"
	call = apsalarResolve(campaign_data, app_data, device_data)
	util.execute_request(**call)

	time.sleep(random.randint(5,10))
		
	print '\n'+"-----------------------------APSALAR Start---------------------------------"
	call = apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**call)

	apsalar_event_app_open_event(campaign_data, app_data, device_data)
		
	print '\n'+"-----------------------------Kochava initial---------------------------------"
	request=kochava_initial(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(2*60,3*60))
		
	print '\n'+"-----------------------------MAT----touch_screen---------------------------------"
	call = mat_serve(campaign_data, app_data, device_data,action="conversion:touch_screen")
	response = util.execute_request(**call)

	apsalar_event_touch_screen(campaign_data, app_data, device_data, custom_user_id=True)


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	# def_appsflyerUID(app_data)
	# def_eventsRecords(app_data)
	# generate_realtime_differences(device_data,app_data)

	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def apsalar_event_app_open_event(campaign_data, app_data, device_data, custom_user_id=False):
	print 'Apsalr : EVENT______________________________app_open_event'
	request=apsalar_event(campaign_data, app_data, device_data,event_name='app_open_event',custom_user_id=custom_user_id)
	util.execute_request(**request)

def apsalar_event_touch_screen(campaign_data, app_data, device_data, custom_user_id=False):
	print 'Apsalr : EVENT______________________________touch_screen'
	request=apsalar_event(campaign_data, app_data, device_data,event_name='touch_screen',custom_user_id=custom_user_id)
	util.execute_request(**request)


###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def mat_serve(campaign_data, app_data, device_data,action,log=False):
	set_userAgent(app_data,device_data)
	return_userinfo(app_data)
	set_MATID(app_data)

	if ":" in action:
		site_event_name = action.split(":")[1]
		action = action.split(":")[0]

	method = "post"
	url = 'http://'+campaign_data.get('mat').get('advertiser_id')+'.engine.mobileapptracking.com/serve'
	headers = {
		"Accept" : "application/json",
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : app_data.get('ua'),
		}
	params = {
		"action" : action,
		"advertiser_id" : campaign_data.get('mat').get('advertiser_id'),
		"package_name" : campaign_data.get('package_name'),
		"sdk" : campaign_data.get('mat').get('sdk'),
		"sdk_retry_attempt" : 0,
		"transaction_id" : str(uuid.uuid4()).lower(),
		"ver" : campaign_data.get('mat').get('ver'),
		}
	data = {
		"data" : [],
		}
	data_value = {
		"app_ad_tracking" : "1",
		"app_name" : campaign_data.get('app_name'),
		"app_version" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"build" : device_data.get('build'),
		"click_timestamp" : int(app_data.get('times').get('click_time')),
		"connection_type" : "wifi",
		"conversion_user_agent" : app_data.get('ua'),
		"currency_code" : "USD",
		"device_brand" : device_data.get('brand'),
		"device_cpu_type" : device_data.get('cpu_abi'),
		"device_model" : device_data.get('model'),
		"download_date" : int(app_data.get('times').get('click_time')),
		"fire_ad_tracking_disabled" : "0",
		"google_ad_tracking_disabled" : "0",
		"google_aid" : device_data.get('adid'),
		"insdate" : int(app_data.get('times').get('install_complete_time')),
		"install_referrer" : "utm_source=(not set)&utm_medium=(not set)",
		"installer" : "com.android.vending",
		"is_coppa" : "0",
		"language" : device_data.get('locale').get('language'),
		"locale" : device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper(),
		"mat_id" : app_data.get('mat_id'),
		"os_version" : device_data.get('os_version'),
		"revenue" : "0.0",
		"screen_density" : util.getdensity(device_data.get('dpi')),
		"screen_layout_size" : device_data.get('resolution'),
		"sdk_plugin" : "unity",
		"sdk_version" : campaign_data.get('mat').get('ver'),
		"system_date" : str(int(time.time())),
	}

	if(log):
		if app_data.get('open_log_id') and app_data.get('last_open_log_id'):
			data_value['open_log_id'] = str(app_data['open_log_id'])
			data_value['last_open_log_id'] = str(app_data['last_open_log_id'])
	if action=="conversion":
		params['site_event_name'] = site_event_name

	if app_data.get('referrer'):
		data_value['install_referrer'] = app_data.get('referrer')

	if not app_data.get('referrer_delay'):
		app_data['referrer_delay']=random.randint(100,200)

	da_str = urllib.urlencode(data_value)

	key = campaign_data.get('mat').get('key')
	iv = campaign_data.get('mat').get('iv')
	aes = AESCipher(key)
	params['data'] = aes.encrypt(iv, da_str)	


	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}


#################################################
#												#
# 			AppSalar Resolve funcation 			#
#												#
#################################################

def apsalarResolve(campaign_data, app_data, device_data):
	custom_user_id_create(app_data)
	method = "get"
	url = "http://e-ssl.apsalar.com/api/v1/resolve"
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : "Singular/SDK-v7.4.0.PROD",
		}
	params = {
		"a" : campaign_data.get('apsalar').get('key'),
		"c" : "wifi",
		"i" : campaign_data.get('package_name'),
		"k" : "AIFA",
		"lag" : str(generateLag()),
		"p" : "Android",
		"pi" : "1",
		"rt" : "json",
		"u" : device_data.get('adid'),
		"v" : device_data.get('os_version'),
		}

	params = get_params(params)
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


#################################################
#												#
# 			AppSalar Start funcation 			#
#												#
#################################################

def apsalarStart(campaign_data, app_data, device_data):
	custom_user_id_create(app_data)	

	method = "get"
	url = "http://e-ssl.apsalar.com/api/v1/start"
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : "Singular/SDK-v7.4.0.PROD",
		}
	params = {
		"a" : campaign_data.get('apsalar').get('key'),
		"ab" : device_data.get('cpu_abi','armeabi'),
		"aifa" : device_data.get('adid'),
		"av" : campaign_data.get('app_version_name'),
		"br" : device_data.get('brand'),
		"c" : "wifi",
		"current_device_time" : str(int(time.time()*1000)),
		"ddl_enabled" : "true",
		"ddl_to" : "60",
		"de" : device_data.get('device'),
		"device_type" : device_data.get('device_type'),
		"dnt" : "0",
		"i" : campaign_data.get('package_name'),
		"install_time" : str(int(app_data.get('times').get('download_end_time')*1000)),
		"is" : "true",
		"k" : "AIFA",
		"lag" : str(generateLag()),
		"lc" : device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
		"ma" : device_data.get('manufacturer'),
		"mo" : device_data.get('model'),
		"n" : campaign_data.get('app_name'),
		"p" : "Android",
		"pr" : device_data.get('product'),
		"rt" : "json",
		"s" : str(int(app_data.get('times').get('install_complete_time')*1000)),
		"sdk" : campaign_data.get('apsalar').get('version'),
		"src" : "com.android.vending",
		"u" : device_data.get('adid'),
		"update_time" : str(int(app_data.get('times').get('download_end_time')*1000)),
		"v" : device_data.get('os_version'),
		}

	
	params = get_params(params)
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h

	app_data['apsalar_time']=time.time()

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


#################################################
#												#
# 			AppSalar Event funcation 			#
#												#
#################################################

def apsalar_event(campaign_data, app_data, device_data,event_name='',value='',custom_user_id=False):
	custom_user_id_create(app_data)

	if not app_data.get('seq'):
		app_data['seq'] = 1
	else:
		app_data['seq'] += 1

	t = str(time.time()-app_data.get('apsalar_time'))[:-8]
	method = "get"
	url = "http://e-ssl.apsalar.com/api/v1/event"
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : "Singular/SDK-v7.4.0.PROD",
		}
	params = {
		"a" : campaign_data.get('apsalar').get('key'),
		"aifa" : device_data.get('adid'),
		"av" : campaign_data.get('app_version_name'),
		"c" : "wifi",
		"i" : campaign_data.get('package_name'),
		"k" : "AIFA",
		"lag" : str(generateLag()),
		"n" : event_name,
		"p" : "Android",
		"rt" : "json",
		"s" : str(int(app_data.get('times').get('install_complete_time')*1000)),
		"sdk" : campaign_data.get('apsalar').get('version'),
		"seq" : app_data.get('seq'),
		"t" : t,
		"u" : device_data.get('adid'),
		}

	if value:
		params['e'] = json.dumps(value)

	if custom_user_id:
		params['custom_user_id'] = 	app_data.get('custom_user_id')	

	params = get_params(params)

	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


def kochavaKvinit(campaign_data, app_data, device_data):
	set_kochava_device_id(app_data)
	method = "post"
	url = 'http://kvinit-prod.api.kochava.com/track/kvinit'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json; charset=UTF-8",
		"User-Agent" : get_ua(device_data),

		}
	params = {}
	data = {
		"action" : "init",
		"data" : {
						"locale" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
						"package" : campaign_data.get('package_name'),
						"platform" : "android",
						"timezone" : device_data.get('local_tz_name'),
						"uptime" : float('0.'+str(random.randint(100,999))),
						"usertime" : int(time.time()),
		},
		"kochava_app_id" : campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id" : app_data.get('kochava_device_id'),
		"nt_id" : app_data.get('nt_id_first')+str(uuid.uuid4()),
		"sdk_build_date" : campaign_data.get('kochava').get('sdk_build_date'),
		"sdk_protocol" : campaign_data.get('kochava').get('sdk_protocol'),
		"sdk_version" : campaign_data.get('kochava').get('sdk_version'),
		"send_date" : datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',
		}

	
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}



def kochava_initial(campaign_data, app_data, device_data):	
	set_kochava_device_id(app_data)
	get_screen_inches(app_data)
	app_data['uptime']=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),5)
	email_ids=''

	for i in range(random.randint(3,6)):
		return_userinfo(app_data)
		email_ids+=app_data.get('user_info').get('email')+','
	email_ids = email_ids[:-1]
	method = "post"
	url = 'http://control.kochava.com/track/json'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json; charset=UTF-8",
		"User-Agent" : get_ua(device_data),

		}
	params = {}
	data = {
		"action" : "initial",
		"data" : {
						"adid" : device_data.get('adid'),
						"android_id" : device_data.get('android_id'),
						"app_limit_tracking" : False,
						"app_short_string" : campaign_data.get('app_version_name'),
						"app_version" : campaign_data.get('app_name')+' '+campaign_data.get('app_version_code'),
						"architecture" : device_data.get('cpu_abi'),
						"battery_level" : app_data.get('battery_level'),
						"battery_status" : "charging",
						"carrier_name" : device_data.get('carrier'),
						"connected_devices" : [u'Keyboard: mtk-tpd'],
						"conversion_data" : get_conversion_data(app_data),
						"conversion_type" : "gplay",
						"device" : device_data.get('model')+"-"+device_data.get('brand'),
						"device_cores" : device_data.get('cpu_core'),
						"device_limit_tracking" : False,
						"device_orientation" : "landscape",
						"disp_h" : int(device_data.get('resolution').split('x')[0]),
						"disp_w" : int(device_data.get('resolution').split('x')[1]),
						"install_referrer" : {
						"attempt_count" : 1,
						"duration" : float('0.0'+str(random.randint(11,99))),
						"install_begin_time" : int(app_data.get('times').get('download_begin_time')),
						"referrer" : get_conversion_data(app_data),
						"referrer_click_time" : int(app_data.get('times').get('click_time')),
						"status" : "ok",
			},
						"installed_date" : int(app_data.get('times').get('install_complete_time')),
						"installer_package" : "com.android.vending",
						"is_genuine" : True,
						"language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
						"locale" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
						"manufacturer" : device_data.get('manufacturer'),
						"network_conn_type" : "wifi",
						"notifications_enabled" : True,
						"os_version" : "Android "+device_data.get("os_version"),
						"package" : campaign_data.get('package_name'),
						"product_name" : device_data.get('product'),
						"screen_brightness" : app_data.get('screen_brightness'),
						"screen_dpi" : int(device_data.get('dpi')),
						"screen_inches" : float(app_data.get('screen_inches')) if app_data.get('screen_inches') else get_screen_inches(),
						"ssid" : device_data.get('carrier'),
						"state_active" : True,
						"timezone" : device_data.get('local_tz_name'),
						"ui_mode" : "Normal",
						"uptime" : app_data.get('uptime'),
						"usertime" : int(app_data.get('previous_Call_time')),
						"volume" : app_data.get('volume'),
		},
		"kochava_app_id" : campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id" : app_data.get('kochava_device_id'),
		"nt_id" : app_data.get('nt_id_first')+str(uuid.uuid4()),
		"sdk_protocol" : campaign_data.get('kochava').get('sdk_protocol'),
		"sdk_version" : campaign_data.get('kochava').get('sdk_version'),
		"send_date" : datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',

		}

	
	app_data['previous_Call_time']=time.time()
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}




###########################################################
#														  #
#						UTIL							  #
#														  #
###########################################################
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid = device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_ua(device_data):
	if int(device_data.get("sdk")) >19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def set_kochava_device_id(app_data):
	if not app_data.get('kochava_device_id'):
		app_data['kochava_device_id'] ='KA350'+str(int(time.time()))+'t'+ str(uuid.uuid4()).replace('-','') 

def get_conversion_data(app_data):	
	conversion_data="utm_source=ko_c4cc5b7147cedb111&utm_medium=1&utm_campaign=kotopface-android-s073792fc1e7d6f3&utm_term=&utm_content=&"
	if app_data.get('referrer'):
		referrer = urllib.unquote(app_data.get('referrer'))
		referrerpart = urlparse.parse_qs(referrer)
		utm_medium = referrerpart.get('utm_medium')[0] if referrerpart.get('utm_medium') else '(not%20set)'
		utm_content = referrerpart.get('utm_content')[0] if referrerpart.get('utm_content') else '(not%20set)'
		utm_term = referrerpart.get('utm_term')[0] if referrerpart.get('utm_term') else '(not%20set)'
		utm_source = referrerpart.get('utm_source')[0] if referrerpart.get('utm_source') else '(not%20set)'
		utm_campaign = referrerpart.get('utm_campaign')[0] if referrerpart.get('utm_campaign') else '(not%20set)'
		conversion_data = 'utm_source='+utm_source+'&utm_medium='+utm_medium+'&utm_campaign='+utm_campaign+'&utm_term='+utm_term+'&utm_content='+utm_content+'&'
	return conversion_data

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_screen_inches(app_data):
	if not app_data.get('screen_inches'):
		app_data['screen_inches']=random.choice(['5.1','4.7','6','4.7','6.5'])

	
#####################################################
#													#
# 			Apsalar Utility function 				#
#													#
#####################################################

def get_params(data_value):
	key = data_value.keys()
	a = ''
	for value in sorted(key):
		a += str(urllib.quote_plus(str(value)).encode('utf-8'))+'='+str(urllib.quote_plus(str(data_value.get(value))).encode('utf-8'))+'&'
	a = a.rsplit("&",1)[0]	

	return a
			
def generateLag():
	return (random.randint(40,300)*0.001)

def custom_user_id_create(app_data):
	if not app_data.get('custom_user_id'):
		app_data['custom_user_id']= util.get_random_string('hex',32).upper()

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())





###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	




def set_MATID(app_data):
	if not app_data.get('mat_id'):
		app_data['mat_id'] = str(uuid.uuid4()).upper()

def set_matOpenLogID(app_data):
	if app_data.get('last_open_log_id'):
		app_data['open_log_id'] = app_data['last_open_log_id']

def catch(response,app_data,paramName):
	try:
		if paramName=="PHPSESSID":
			header = response.get('res').headers
			return header.get('Set-Cookie').split("PHPSESSID=")[1].split(";")[0]
		if paramName=='COOKIE':
			header = response.get('res').headers
			return header.get('Set-Cookie')
		if paramName=="user_id":
			jsonData = json.loads(response.get('data'))
			app_data['user_id'] = jsonData.get('result').get('Achievements_getUserAchievementsAndProgress')[0].get('user_achievement').get('user_id')
		if paramName=="mat":
			jsonData = json.loads(response.get('data'))
			app_data['last_open_log_id'] = jsonData.get('log_id')

	except:
		print "Exception:: Couldn't fetch "+paramName+" from response"

class AESCipher:
 def __init__(self, key):
		self.key = key

 def pad(self, raw):
		l = len(raw) % 16
		l = 16 - l
		for x in range(l):
			raw += ' '
		return raw

 def encrypt(self, iv, raw):
		raw = self.pad(raw)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return (cipher.encrypt(raw)).encode('hex')

 def decrypt(self, enc , iv):
		enc = enc.decode('hex')#base64.b64decode(enc)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return cipher.decrypt(enc)

def set_userAgent(app_data,device_data):
	if not app_data.get('ua'):
		if int(device_data.get("sdk")) >19:
			app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
		else:
			app_data['ua'] = 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'



def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	if not gender_n:
		app_data['gender'] = random.choice(['male', 'female'])
		gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	if not user_info:
		user_data = util.generate_name(gender=gender_n)
		user_info = {}
		user_info['username'] = user_data.get('username').lower()
		user_info['email'] = user_data.get('username').lower() + '@gmail.com'
		user_info['f_name'] = user_data.get('firstname')
		user_info['l_name'] = user_data.get('lastname')
		user_info['sex'] = gender_n
		user_info['gender'] = str(1) if gender_n == 'female' else str(2)
		user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
		user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
		user_info['password'] = util.get_random_string(type='all',size=16)
		app_data['user_info'] = user_info




#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"