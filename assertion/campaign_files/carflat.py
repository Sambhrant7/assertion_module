#-*- coding:UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import getsleep
import uuid,time,datetime,random,json,urllib,string, urlparse,base64
from sdk import util,installtimenew
from Crypto.Cipher import AES
import clicker,Config


campaign_data = {
			'package_name':'kr.co.plat.carplat',	
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'ctr':6,
			'app_name' : u'카플랫 - 렌터카(렌트카), 집 앞에서 타는 렌터카'.encode('utf-8'),
			'no_referrer': True,
			'app_version_code':'3092602',#'3070901',#'3040805',#'3031001',#'3012201',#'3011401',#'2122301',#'2111501',#'2110501',#'2102302',#'2101601',#'12',
			'app_version_name':"3.9.26",#'3.7.9',#'3.4.8',#'3.3.10',#'3.1.22',#'3.1.14',#'2.12.23',#'2.11.15',#'2.11.5',#'2.10.23',#'2.10.16',#'1.1.2',
			'device_targeting':True,
			'supported_countries': 'WW',
			'supported_os':'4.4',
			'app_size' : 8.1,
			'sdk' : 'Android',
			'tracker':'ad-brix',
			'ad-brix': {
							'app_key': '886248944',
							'ver': '4.5.4a',
							'key_getreferral': '485802f96b994869485802f96b994869',
							'iv_getreferral' : '485802f96b994869',
							'key_tracking': 'srkterowgawrsozerruly82nfij625w9',
							'iv_tracking' : 'srkterowgawrsoze'
			},
			'retention':{
							1:75,
							2:72,
							3:70,
							4:68,
							5:66,
							6:60,
							7:55,
							8:52,
							9:45,
							10:40,
							11:38,
							12:36,
							13:35,
							14:32,
							15:30,
							16:28,
							17:26,
							18:23,
							19:21,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5
						}
		}
		
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0
	
def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')
	
def make_install_date(app_data,device_data):
	sec_1 = app_data.get('sec') if app_data.get('sec') else make_sec(app_data,device_data)
	app_data['install_date'] = datetime.datetime.fromtimestamp(int(time.time())+sec_1).strftime("%Y%m%d")
	return app_data.get('install_date')
	

####################################
#		   install				 #
####################################

def install(app_data, device_data):
	print 'please wait installing____'
	installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os='android')
	make_sec(app_data,device_data)
	app_data['click_time'] = int(app_data.get('times').get('click_time'))
	app_data['app_install_start_time'] = int(app_data.get('times').get('download_end_time'))
	app_data['app_install_completed_time'] = int(app_data.get('times').get('install_complete_time'))
	app_data['app_install_completed_time_date'] = datetime.datetime.utcfromtimestamp(app_data['app_install_completed_time']).strftime("%Y%m%d%H%M%S")
	app_data['app_first_open_time'] = get_timestamp(para1=60,para2=100)
	app_data['client_timestamp1'] = get_timestamp(para1=0,para2=2)
	app_data['client_timestamp2'] = get_timestamp(para1=4,para2=6)
	time.sleep(random.randint(60,100))
	app_data['install_mdatetime'] = str(int(time.time()*1000))
	get_demo_value(app_data,device_data)
	update_activity(app_data,device_data)



	email_sha1 = ""
	for i in range(0,random.randint(1,1)):
		return_userinfo(app_data)
		email_sha1 = email_sha1+util.sha1(app_data.get('user_info').get('email'))+'|'
	
	app_data['email_ag'] = email_sha1[:len(email_sha1)-1]

	cars_list=['compact_AVANTE AD','medium_ALL NEW K5','medium_LF SONATA','medium_SM5','medium_K5','medium_2018 K5','luxury_SM6','luxury_GRANDEUR IG','luxury_ALL NEW K7','luxury_K7']

	selected_car=random.randint(0,len(cars_list)-1)	
	
	print "Ad Brix Get Schedule"
	ad_brix_schedule = ad_brix_get_schedule(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**ad_brix_schedule)
	
	print "AD BRIX get Referral"
	get_referral = ad_brix_get_referral(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'])
	get_referral_result =util.execute_request(**get_referral)
	try:
		get_referral_result_decode = json.loads(get_referral_result.get('data'))
		app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
		app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))
	except:
		app_data['user_number'] = "961013195959585686"
		app_data['shard_number'] = "6"

	
	print "Ad Brix Tracking"
	ad_brix_tracking_1 = ad_brix_tracking_start(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'])
	util.execute_request(**ad_brix_tracking_1)
	
	
	if random.randint(1,100)<=40:
		time.sleep(random.randint(5,80))
		print "Ad Brix Tracking-- try signup"
		ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='fte',activity='trySignUp',param='main')
		util.execute_request(**ad_brix_tracking_1)

		time.sleep(random.randint(6,90))
		
		print "Ad Brix Tracking-- try signup"
		ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='trySignUp',param='main')
		util.execute_request(**ad_brix_tracking_1)

	########### here ##############

	shuffle=[1,2,3,4,5,6]
	# shuffle=[5,5,5,5,5,5]
	random.shuffle(shuffle)
	print shuffle
	for i in range(len(shuffle)-random.randint(1,2)):
		choice=shuffle[i]
		if choice==1:
			rr=random.randint(1,100)
			if rr<=99:
				if rr<=55:
					time.sleep(random.randint(6,25))
					print "Ad Brix Tracking-- try MidTerm "
					ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='tryMidTerm',param='')
					util.execute_request(**ad_brix_tracking_1)
					

				if rr>55:
					time.sleep(random.randint(40,90))
					print "Ad Brix Tracking-- try LongTerm "
					ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='tryLongTerm',param='')
					util.execute_request(**ad_brix_tracking_1)

				if random.randint(1,100)<=95: #80
					group_list=['fte','ret']
					for i in range(len(group_list)):
						time.sleep(random.randint(15,60))
						print "Ad Brix Tracking-- try filter "
						ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group=str(group_list[i]),activity='tryFilter',param=random.choice(['mainMenu','promotion','promotion','promotion']))
						util.execute_request(**ad_brix_tracking_1)

					if random.randint(1,100)<=95: #70
						group_list=['fte','ret']
						for i in range(len(group_list)):
							time.sleep(random.randint(12,35))
							print "Ad Brix Tracking-- complete filter"
							ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group=str(group_list[i]),activity='completeFilter',param='notLoggedIn')
							util.execute_request(**ad_brix_tracking_1)

						for i in range(len(group_list)):
							time.sleep(random.randint(10,15))
							print "Ad Brix Tracking-- complete Select CarModel"
							ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],2,group=str(group_list[i]),activity='completeSelectCarModel',param=str(cars_list[selected_car]))
							util.execute_request(**ad_brix_tracking_1)
					
		if choice == 2 and random.randint(1,100)<=80:
			time.sleep(random.randint(10,14))

			print "\nself_call_promotions\n"
			request=promotions( campaign_data, device_data, app_data )
			result=util.execute_request(**request)
			global promotion_list
			try:
				promotion_list=[]
				info=json.loads(result.get('data')).get('data').get('promotions')
				for i in range(len(info)):
					if info[i].get('type')=='general':
						promotion_list.append(info[i].get('title'))

				if len(promotion_list)<1:
					promotion_list=['최대 10만원 저렴하게! 카플랫 할인 혜택 총 정리','함께하면 더욱 저렴한 카플랫, 친구 초대 기능 오픈!','카플랫 회원가입 즉시, 웰컴 포인트 2만원 지급']

				print promotion_list

			except:
				promotion_list=['최대 10만원 저렴하게! 카플랫 할인 혜택 총 정리','함께하면 더욱 저렴한 카플랫, 친구 초대 기능 오픈!','카플랫 회원가입 즉시, 웰컴 포인트 2만원 지급']

			print "Ad Brix Tracking-- open Promotion"
			ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='openPromotion',param=str(random.choice(promotion_list)).encode('utf-7'))
			util.execute_request(**ad_brix_tracking_1)


		if choice == 3 and random.randint(1,100)<=80 :
			time.sleep(random.randint(10,60))
			print "\nself_call_magazine\n"
			request=magazine( campaign_data, device_data, app_data,types='story' )
			result=util.execute_request(**request)
			global story_list
			try:
				story_list=[]
				info=json.loads(result.get('data')).get('data').get('magazines')
				for i in range(len(info)):				
					story_list.append(info[i].get('title'))

				if len(story_list)<1:
					story_list=['카플랫 이메일 구독 신청 이벤트','카플랫 서비스 지역 안내','카플랫 스테이 런칭기념 이벤트','‘카플랫’으로 내맘에 쏙 드는 차, 집 앞에서 받는다','카플랫 차량 보험 - 렌터카 보험 사례로 알아봐요!','렌터카 예약 앱 ‘카플랫’, 고객 건강을 위한 차량 관리 세차 프로그램 선보여']

				print story_list

			except:
				print "-----------exception-----------------------------"
				story_list=['카플랫 이메일 구독 신청 이벤트','카플랫 서비스 지역 안내','카플랫 스테이 런칭기념 이벤트','‘카플랫’으로 내맘에 쏙 드는 차, 집 앞에서 받는다','카플랫 차량 보험 - 렌터카 보험 사례로 알아봐요!','렌터카 예약 앱 ‘카플랫’, 고객 건강을 위한 차량 관리 세차 프로그램 선보여']
				

			print "Ad Brix Tracking-- open Story"
			ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='openStory',param=str(random.choice(story_list)).encode('utf-7'))
			util.execute_request(**ad_brix_tracking_1)

		if choice == 4 and random.randint(1,100)<=80:
			time.sleep(random.randint(20,35))
			print "\nself_call_promotions\n"
			request=promotions( campaign_data, device_data, app_data )
			result=util.execute_request(**request)
			global deal_list
			try:
				deal_list=[]
				info=json.loads(result.get('data')).get('data').get('promotions')
				for i in range(len(info)):
					if info[i].get('type')=='hotdeal':
						deal_list.append(info[i].get('title'))		

				if len(deal_list)<1:
					deal_list=['차량이 연락없이 30분이상 늦는다면?']

				print deal_list

			except:
				deal_list=['차량이 연락없이 30분이상 늦는다면?']

			

			print "Ad Brix Tracking--open Hotdeal"
			ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='openHotdeal',param=str(random.choice(deal_list)).encode('utf-7'))
			util.execute_request(**ad_brix_tracking_1)

		if choice == 5 and random.randint(1,100)<=80:
			time.sleep(random.randint(10,30))

			print "\nself_call_magazine\n"
			request=magazine( campaign_data, device_data, app_data ,types='magazine')
			result=util.execute_request(**request)
			global magazines_list
			try:
				magazines_list=[]
				info=json.loads(result.get('data')).get('data').get('magazines')
				for i in range(len(info)):				
					magazines_list.append(info[i].get('title'))

				

				if len(magazines_list)<1:
					magazines_list=['도심 속 자연에서 힐링하기 좋은 제주 카페 추천 ','해변따라 달리는 고성 드라이브 코스','아기자기한 디저트가 맛있는 제주 감성카페 추천','해외여행 안 가도 되는 이국적인 국내 풀빌라','여름엔 카페로 피서가자! 울산・부산 카페 추천'] 

				print magazines_list

			except:
				print "-----------exception-----------------------------"
				magazines_list=['도심 속 자연에서 힐링하기 좋은 제주 카페 추천 ','해변따라 달리는 고성 드라이브 코스','아기자기한 디저트가 맛있는 제주 감성카페 추천','해외여행 안 가도 되는 이국적인 국내 풀빌라','여름엔 카페로 피서가자! 울산・부산 카페 추천']




			print "Ad Brix Tracking-- open Magazine"
			ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='openMagazine',param=str(random.choice(magazines_list)).encode('utf-7'))
			util.execute_request(**ad_brix_tracking_1)


		if choice == 6 and random.randint(1,100)<=40:
			time.sleep(random.randint(10,30))
			print "Ad Brix Tracking-- openNotification"
			ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='openNotification',param='')
			util.execute_request(**ad_brix_tracking_1)



	######### end ###############

	

	return {'status':'true'}
	
def open(app_data,device_data,day='1'):

	print "inside open--"

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os='android')
		make_sec(app_data,device_data)
		app_data['click_time'] = int(app_data.get('times').get('click_time'))
		app_data['app_install_start_time'] = int(app_data.get('times').get('download_end_time'))
		app_data['app_install_completed_time'] = int(app_data.get('times').get('install_complete_time'))
		app_data['app_install_completed_time_date'] = datetime.datetime.utcfromtimestamp(app_data['app_install_completed_time']).strftime("%Y%m%d%H%M%S")
		app_data['app_first_open_time'] = get_timestamp(para1=60,para2=100)
		app_data['client_timestamp1'] = get_timestamp(para1=0,para2=2)
		app_data['client_timestamp2'] = get_timestamp(para1=4,para2=6)
		time.sleep(random.randint(60,100))
		app_data['install_mdatetime'] = str(int(time.time()*1000))

	if not app_data.get('activity'):	
		update_activity(app_data,device_data)

	get_demo_value(app_data,device_data)


	if not app_data.get('email_ag'):
		email_sha1 = ""
		for i in range(0,random.randint(1,1)):
			return_userinfo(app_data)
			email_sha1 = email_sha1+util.sha1(app_data.get('user_info').get('email'))+'|'
		
		app_data['email_ag'] = email_sha1[:len(email_sha1)-1]

	cars_list=['compact_AVANTE AD','medium_ALL NEW K5','medium_LF SONATA','medium_SM5','medium_K5','medium_2018 K5','luxury_SM6','luxury_GRANDEUR IG','luxury_ALL NEW K7','luxury_K7']

	selected_car=random.randint(0,len(cars_list)-1)	
	
	print "Ad Brix Get Schedule"
	ad_brix_schedule = ad_brix_get_schedule(campaign_data, app_data, device_data)
	util.execute_request(**ad_brix_schedule)

	if not app_data.get('user_number'):
		print "AD BRIX get Referral"
		get_referral = ad_brix_get_referral(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'])
		get_referral_result =util.execute_request(**get_referral)
		try:
			get_referral_result_decode = json.loads(get_referral_result.get('data'))
			app_data['user_number'] = str(get_referral_result_decode.get('Data').get('user_no'))
			app_data['shard_number'] = str(get_referral_result_decode.get('Data').get('shard_no'))
		except:
			app_data['user_number'] = "961013195959585686"
			app_data['shard_number'] = "6"

	print "Ad Brix Tracking"
	ad_brix_tracking_1 = ad_brix_tracking_open(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'])
	util.execute_request(**ad_brix_tracking_1)	
	
	if random.randint(1,100)<=30:
		time.sleep(random.randint(5,80))
		print "Ad Brix Tracking-- try signup"
		ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='fte',activity='trySignUp',param='main')
		util.execute_request(**ad_brix_tracking_1)

		time.sleep(random.randint(6,90))
		
		print "Ad Brix Tracking-- try signup"
		ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='trySignUp',param='main')
		util.execute_request(**ad_brix_tracking_1)

	shuffle=[1,2,3,4,5,6]
	random.shuffle(shuffle)
	print shuffle
	for i in range(len(shuffle)-random.randint(1,2)):
		choice=shuffle[i]
		if choice==1:
			rr=random.randint(1,100)
			if rr<=90:
				if rr<=50:
					time.sleep(random.randint(6,20))
					print "Ad Brix Tracking-- try MidTerm "
					ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='tryMidTerm',param='')
					util.execute_request(**ad_brix_tracking_1)

				if rr>50 and rr<=90:
					time.sleep(random.randint(40,90))
					print "Ad Brix Tracking-- try LongTerm "
					ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='tryLongTerm',param='')
					util.execute_request(**ad_brix_tracking_1)

				if random.randint(1,100)<=95: #70
					group_list=['fte','ret']
					for i in range(len(group_list)):
						time.sleep(random.randint(15,60))
						print "Ad Brix Tracking-- try filter "
						ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group=str(group_list[i]),activity='tryFilter',param='mainMenu')
						util.execute_request(**ad_brix_tracking_1)

					if random.randint(1,100)<=95: #60
						group_list=['fte','ret']
						for i in range(len(group_list)):
							time.sleep(random.randint(10,35))
							print "Ad Brix Tracking-- complete filter"
							ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group=str(group_list[i]),activity='completeFilter',param='notLoggedIn')
							util.execute_request(**ad_brix_tracking_1)

						for i in range(len(group_list)):
							time.sleep(random.randint(10,15))
							print "Ad Brix Tracking-- complete Select CarModel"
							ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],2,group=str(group_list[i]),activity='completeSelectCarModel',param=str(cars_list[selected_car]))
							util.execute_request(**ad_brix_tracking_1)
					
		if choice == 2 and random.randint(1,100)<=70:
			time.sleep(random.randint(10,14))

			print "\nself_call_promotions\n"
			request=promotions( campaign_data, device_data, app_data )
			result=util.execute_request(**request)
			global promotion_list
			try:
				promotion_list=[]
				info=json.loads(result.get('data')).get('data').get('promotions')
				for i in range(len(info)):
					if info[i].get('type')=='general':
						promotion_list.append(info[i].get('title'))

				if len(promotion_list)<1:
					promotion_list=["최대 10만원 저렴하게! 카플랫 할인 혜택 총 정리","함께하면 더욱 저렴한 카플랫, 친구 초대 기능 오픈!","카플랫 회원가입 즉시, 웰컴 포인트 2만원 지급"]

				print promotion_list

			except:
				promotion_list=["최대 10만원 저렴하게! 카플랫 할인 혜택 총 정리","함께하면 더욱 저렴한 카플랫, 친구 초대 기능 오픈!","카플랫 회원가입 즉시, 웰컴 포인트 2만원 지급"]

			print "Ad Brix Tracking-- open Promotion"
			ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='openPromotion',param=str(random.choice(promotion_list)).encode('utf-7'))
			util.execute_request(**ad_brix_tracking_1)


		if choice == 3 and random.randint(1,100)<=70 :
			time.sleep(random.randint(10,60))
			print "\nself_call_magazine\n"
			request=magazine( campaign_data, device_data, app_data,types='story' )
			result=util.execute_request(**request)
			global story_list
			try:
				story_list=[]
				info=json.loads(result.get('data')).get('data').get('magazines')
				for i in range(len(info)):				
					story_list.append(info[i].get('title'))

				if len(story_list)<1:
					story_list=["카플랫 이메일 구독 신청 이벤트","카플랫 서비스 지역 안내","카플랫 스테이 런칭기념 이벤트","‘카플랫’으로 내맘에 쏙 드는 차, 집 앞에서 받는다","카플랫 차량 보험 - 렌터카 보험 사례로 알아봐요!","렌터카 예약 앱 ‘카플랫’, 고객 건강을 위한 차량 관리 세차 프로그램 선보여"]

				print story_list

			except:
				print "-----------exception-----------------------------"
				story_list=["카플랫 이메일 구독 신청 이벤트","카플랫 서비스 지역 안내","카플랫 스테이 런칭기념 이벤트","‘카플랫’으로 내맘에 쏙 드는 차, 집 앞에서 받는다","카플랫 차량 보험 - 렌터카 보험 사례로 알아봐요!","렌터카 예약 앱 ‘카플랫’, 고객 건강을 위한 차량 관리 세차 프로그램 선보여"]
				

			print "Ad Brix Tracking-- open Story"
			ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='openStory',param=str(random.choice(story_list)).encode('utf-7'))
			util.execute_request(**ad_brix_tracking_1)

		if choice == 4 and random.randint(1,100)<=70:
			time.sleep(random.randint(20,35))
			print "\nself_call_promotions\n"
			request=promotions( campaign_data, device_data, app_data )
			result=util.execute_request(**request)
			global deal_list
			try:
				deal_list=[]
				info=json.loads(result.get('data')).get('data').get('promotions')
				for i in range(len(info)):
					if info[i].get('type')=='hotdeal':
						deal_list.append(info[i].get('title'))		

				if len(deal_list)<1:
					deal_list=["차량이 연락없이 30분이상 늦는다면?"]

				print deal_list

			except:
				deal_list=["차량이 연락없이 30분이상 늦는다면?"]

			

			print "Ad Brix Tracking--open Hotdeal"
			ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='openHotdeal',param=str(random.choice(deal_list)).encode('utf-7'))
			util.execute_request(**ad_brix_tracking_1)

		if choice == 5 and random.randint(1,100)<=70:
			time.sleep(random.randint(10,12))

			print "\nself_call_magazine\n"
			request=magazine( campaign_data, device_data, app_data ,types='magazine')
			result=util.execute_request(**request)
			global magazines_list
			try:
				magazines_list=[]
				info=json.loads(result.get('data')).get('data').get('magazines')
				for i in range(len(info)):				
					magazines_list.append(info[i].get('title'))

				

				if len(magazines_list)<1:
					magazines_list=["도심 속 자연에서 힐링하기 좋은 제주 카페 추천 ","해변따라 달리는 고성 드라이브 코스","아기자기한 디저트가 맛있는 제주 감성카페 추천","해외여행 안 가도 되는 이국적인 국내 풀빌라","여름엔 카페로 피서가자! 울산・부산 카페 추천"] 

				print magazines_list

			except:
				print "-----------exception-----------------------------"
				magazines_list=["도심 속 자연에서 힐링하기 좋은 제주 카페 추천 ","해변따라 달리는 고성 드라이브 코스","아기자기한 디저트가 맛있는 제주 감성카페 추천","해외여행 안 가도 되는 이국적인 국내 풀빌라","여름엔 카페로 피서가자! 울산・부산 카페 추천"]




			print "Ad Brix Tracking-- open Magazine"
			ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='openMagazine',param=str(random.choice(magazines_list)).encode('utf-7'))
			util.execute_request(**ad_brix_tracking_1)


		if choice == 6 and random.randint(1,100)<=30:
			time.sleep(random.randint(10,30))
			print "Ad Brix Tracking-- openNotification"
			ad_brix_tracking_1 = ad_brix_tracking_event(campaign_data, app_data, device_data,app_data['click_time'],app_data['app_install_start_time'],app_data['app_install_completed_time'],app_data['app_install_completed_time_date'],app_data['app_first_open_time'],app_data['email_ag'],1,group='ret',activity='openNotification',param='')
			util.execute_request(**ad_brix_tracking_1)


	
		

	return {'status':'true'}


def promotions( campaign_data, device_data, app_data ):
	url= "https://api.carplat.co.kr/promotions/"
	method= "get"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "User-Agent": 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+') Carplat/'+campaign_data.get('app_version_name')}

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def magazine( campaign_data, device_data, app_data,types='' ):
	url= "https://api.carplat.co.kr/magazines/"
	method= "get"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "User-Agent": 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+') Carplat/'+campaign_data.get('app_version_name')}

	params= {       "limit": str(random.randint(4,7)), "type": types}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}




class AESCipher(object):

    def __init__(self, key): 
        self.bs = 32
        self.key = key.encode()
		
    def encrypt(self, iv,raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')
        
    def decrypt(self, enc):
        # enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

class AESCipherTracking(object):

    def __init__(self, key): 
        self.bs = 32
        self.key = key.encode()
		
    def encrypt(self, iv,raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return (cipher.encrypt(raw)).encode('hex')
        
    def decrypt(self, enc):
        # enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]		
	
def ad_brix_get_referral(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1):

	url = 'http://cvr.ad-brix.com/v1/conversion/GetReferral'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	
	
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value = '{"appkey":"'+campaign_data.get('ad-brix').get('app_key')+'","package_name":"'+campaign_data.get('package_name')+'","installer":"com.android.vending","version":"'+campaign_data.get('ad-brix').get('ver')+'","app_version_name":"'+campaign_data.get('app_version_name')+'","app_version_code":'+campaign_data.get('app_version_code')+',"referral_info":{"conversion_key":'+conversion_key_value+',"session_no":'+session_number_value+',"referrer_param":"'+app_data.get('referrer')+'"},"conversion_cache":[],"adbrix_user_info":{"adbrix_user_no":-1,"shard_no":-1,"install_datetime":"","install_mdatetime":"","life_hour":-1,"app_launch_count":1,"referral_key":-1,"reengagement_conversion_key":-1,"last_referral_key":-1,"last_referral_data":"","last_referral_datetime":"","set_referral_key":true,"sig_type":0},"user_info":{"puid":"","mudid":"","openudid":"","openudid_md5":"","openudid_sha1":"","android_id_md5":"'+util.md5(device_data.get('android_id'))+'","android_id_sha1":"'+util.sha1(device_data.get('android_id'))+'","device_id_md5":"","device_id_sha1":"","google_ad_id":"'+device_data.get('adid')+'","google_ad_id_opt_out":false,"initial_ad_id":"'+device_data.get('adid')+'","ag":"'+email_ag_1+'","odin":"'+util.sha1(device_data.get('android_id'))+'","carrier":"'+device_data.get('carrier')+'","country":"'+device_data.get('locale').get('country')+'","language":"'+device_data.get('locale').get('language')+'","android_id":"'+base64.b64encode(device_data.get('android_id'))+'","first_install_time":"'+app_install_completed_time_date_1+'","first_install_time_offset":'+str(app_data.get('sec'))+'},"cohort_info":{},"device_info":{"vendor":"google","model":"'+device_data.get('brand')+device_data.get('model')+'","kn":"'+device_data.get('kernel')+'","is_wifi_only":false,"network":"wifi","noncustomnetwork":'+device_data.get('mnc')+',"os":"a_'+device_data.get('os_version')+'","ptype":"android","width":'+device_data.get('resolution').split('x')[1]+',"height":'+device_data.get('resolution').split('x')[0]+',"is_portrait":true,"utc_offset":'+str(float(app_data.get('sec'))/float(60)/float(60))+',"build_id":"'+device_data.get('build')+'"},"demographics":[],"complete_conversions":[],"activity_info":[],"impression_info":[],"installation_info":{"market_referrer":"'+urlparse.unquote(app_data.get('referrer'))+'","install_actions_timestamp":{"market_install_btn_clicked":'+str(click_time_1)+',"app_install_start":'+str(app_install_start_time_1)+',"app_install_completed":'+str(app_install_completed_time_1)+',"app_first_open":'+str(app_first_open_time_1)+'}},"request_info":{"client_timestamp":'+str(app_data.get('client_timestamp1'))+'}}'		

	print "--------------------------------------------------------"
	print data_value
	print  "--------------------------------------------------------"

	key = campaign_data.get('ad-brix').get('key_getreferral')
	iv = campaign_data.get('ad-brix').get('iv_getreferral')
	aes = AESCipher(key)
	data['j'] = aes.encrypt(iv,data_value)
	
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}	
		
def ad_brix_tracking_start(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	event_date_1 = get_date(app_data,device_data,para1=0,para2=0)
	event_date = get_date(app_data,device_data,para1=1,para2=3)
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value = '{"appkey":"'+campaign_data.get('ad-brix').get('app_key')+'","package_name":"'+campaign_data.get('package_name')+'","installer":"com.android.vending","version":"'+campaign_data.get('ad-brix').get('ver')+'","app_version_name":"'+campaign_data.get('app_version_name')+'","app_version_code":'+campaign_data.get('app_version_code')+',"referral_info":{"conversion_key":'+conversion_key_value+',"session_no":'+session_number_value+',"referrer_param":"'+app_data.get('referrer')+'"},"conversion_cache":['+conversion_key_value+'],"adbrix_user_info":{"adbrix_user_no":'+app_data['user_number']+',"shard_no":'+app_data.get('shard_number')+',"install_datetime":"'+event_date_1+'","install_mdatetime":'+app_data.get('install_mdatetime')+',"life_hour":0,"app_launch_count":1,"referral_key":'+conversion_key_value+',"referral_data":"'+referrer_additional_info+'","reengagement_conversion_key":-1,"last_referral_key":-1,"last_referral_data":"","last_referral_datetime":"","set_referral_key":true,"sig_type":0},"user_info":{"puid":"","mudid":"","openudid":"","openudid_md5":"","openudid_sha1":"","android_id_md5":"'+util.md5(device_data.get('android_id'))+'","android_id_sha1":"'+util.sha1(device_data.get('android_id'))+'","device_id_md5":"","device_id_sha1":"","google_ad_id":"'+device_data.get('adid')+'","google_ad_id_opt_out":false,"initial_ad_id":"'+device_data.get('adid')+'","ag":"'+email_ag_1+'","odin":"'+util.sha1(device_data.get('android_id'))+'","carrier":"'+device_data.get('carrier')+'","country":"'+device_data.get('locale').get('country')+'","language":"'+device_data.get('locale').get('language')+'","android_id":"'+base64.b64encode(device_data.get('android_id'))+'","first_install_time":"'+app_install_completed_time_date_1+'","first_install_time_offset":'+str(app_data.get('sec'))+'},"cohort_info":{},"device_info":{"vendor":"google","model":"'+device_data.get('model')+'","kn":"'+device_data.get('kernel')+'","is_wifi_only":false,"network":"wifi","noncustomnetwork":'+device_data.get('mnc')+',"os":"a_'+device_data.get('os_version')+'","ptype":"android","width":'+device_data.get('resolution').split('x')[1]+',"height":'+device_data.get('resolution').split('x')[0]+',"is_portrait":true,"utc_offset":'+str(float(app_data.get('sec'))/float(60)/float(60))+',"build_id":"'+device_data.get('build')+'"},"demographics":[],"complete_conversions":[],"activity_info":[{"prev_group":"session","prev_activity":"start","group":"session","activity":"end","param":"857626","event_id":"ec27e8c0-1e9c-4793-b8ae-c4d9289dd2e1","created_at":"'+str(event_date)+'"},{"prev_group":"","prev_activity":"","group":"session","activity":"retention","param":"","event_id":"a23b7720-1283-4996-a1b1-baa80498adfb","created_at":"'+str(event_date)+'"},{"prev_group":"","prev_activity":"","group":"session","activity":"start","param":"","event_id":"6ee3517a-8c9f-44dc-9f34-5a5da421a9d5","created_at":"'+str(event_date)+'"}],"impression_info":[],"installation_info":{"market_referrer":"'+urlparse.unquote(app_data.get('referrer'))+'","install_actions_timestamp":{"market_install_btn_clicked":'+str(click_time_1)+',"app_install_start":'+str(app_install_start_time_1)+',"app_install_completed":'+str(app_install_completed_time_1)+',"app_first_open":'+str(app_first_open_time_1)+'}},"request_info":{"client_timestamp":'+str(app_data.get('client_timestamp2'))+'}}'
	print "--------------------------------------------------------"
	print data_value
	print  "--------------------------------------------------------"


	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)
	
	update_activity(app_data,device_data,'session','start')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}		
			

def ad_brix_tracking_event(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1,callseq,group='',activity='',param=''):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	event_date_1 = get_date(app_data,device_data,para1=0,para2=0)
	event_date = get_date(app_data,device_data,para1=0,para2=0)
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	if callseq==1:
		activity_info='"activity_info":[{"prev_group":"'+app_data.get('group')+'","prev_activity":"'+app_data.get('activity')+'","group":"'+group+'","activity":"'+activity+'","param":"'+param+'","event_id":"'+str(uuid.uuid4())+'","created_at":"'+event_date+'"}]'
	elif callseq==2:
		activity_info='"activity_info":[{"prev_group":"'+app_data.get('group')+'","prev_activity":"'+app_data.get('activity')+'","group":"ret","activity":"'+app_data.get('activity')+'","param":"notLoggedIn","event_id":"'+str(uuid.uuid4())+'","created_at":"'+event_date+'"},{"prev_group":"'+app_data.get('group')+'","prev_activity":"'+app_data.get('activity')+'","group":"'+group+'","activity":"'+activity+'","param":"'+param+'","event_id":"'+str(uuid.uuid4())+'","created_at":"'+event_date+'"}]'


	data_value ='{"appkey":"'+campaign_data.get('ad-brix').get('app_key')+'","package_name":"'+campaign_data.get('package_name')+'","installer":"com.android.vending","version":"'+campaign_data.get('ad-brix').get('ver')+'","app_version_name":"'+campaign_data.get('app_version_name')+'","app_version_code":'+campaign_data.get('app_version_code')+',"referral_info":{"conversion_key":'+conversion_key_value+',"session_no":'+session_number_value+',"referrer_param":"'+app_data.get('referrer')+'"},"conversion_cache":['+conversion_key_value+'],"adbrix_user_info":{"adbrix_user_no":'+app_data.get('user_number')+',"shard_no":'+app_data.get('shard_number')+',"install_datetime":"'+event_date_1+'","install_mdatetime":'+str(int(time.time()*1000))+',"life_hour":0,"app_launch_count":1,"referral_key":'+conversion_key_value+',"reengagement_conversion_key":-1,"last_referral_key":-1,"last_referral_data":"","last_referral_datetime":"","set_referral_key":true,"sig_type":0},"user_info":{"puid":"","mudid":"","openudid":"","openudid_md5":"","openudid_sha1":"","android_id_md5":"'+util.md5(device_data.get('android_id'))+'","android_id_sha1":"'+util.sha1(device_data.get('android_id'))+'","device_id_md5":"","device_id_sha1":"","google_ad_id":"'+device_data.get('adid')+'","google_ad_id_opt_out":false,"initial_ad_id":"'+device_data.get('adid')+'","ag":"'+email_ag_1+'","odin":"'+util.sha1(device_data.get('android_id'))+'","carrier":"'+device_data.get('carrier')+'","country":"'+device_data.get('locale').get('country')+'","language":"'+device_data.get('locale').get('language')+'","android_id":"'+base64.b64encode(device_data.get('android_id'))+'","first_install_time":"'+app_install_completed_time_date_1+'","first_install_time_offset":'+str(app_data.get('sec'))+'},"cohort_info":{},"device_info":{"vendor":"google","model":"'+device_data.get('model')+'","kn":"'+device_data.get('kernel')+'","is_wifi_only":false,"network":"wifi","noncustomnetwork":'+device_data.get('mnc')+',"os":"a_'+device_data.get('os_version')+'","ptype":"android","width":'+device_data.get('resolution').split('x')[1]+',"height":'+device_data.get('resolution').split('x')[0]+',"is_portrait":false,"utc_offset":'+str(float(app_data.get('sec'))/float(60)/float(60))+',"build_id":"'+device_data.get('build')+'"},"demographics":[],"complete_conversions":[],'+activity_info+',"impression_info":[],"installation_info":{"market_referrer":"'+urlparse.unquote(app_data.get('referrer'))+'","install_actions_timestamp":{"market_install_btn_clicked":'+str(click_time_1)+',"app_install_start":'+str(app_install_start_time_1)+',"app_install_completed":'+str(app_install_completed_time_1)+',"app_first_open":'+str(app_first_open_time_1)+'}},"request_info":{"client_timestamp":'+str(int(time.time()))+'}}'


	# print "--------------------------------------------------------"
	# print data_value
	# print  "--------------------------------------------------------"


			
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)

	update_activity(app_data,device_data,group,activity)
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}		

def ad_brix_tracking_open(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1):

	url = 'http://tracking.ad-brix.com/v1/tracking'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	event_date_1 = get_date(app_data,device_data,para1=4,para2=7)
	event_date = get_date(app_data,device_data,para1=0,para2=0)
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value ='{"appkey":"'+campaign_data.get('ad-brix').get('app_key')+'","package_name":"'+campaign_data.get('package_name')+'","installer":"com.android.vending","version":"'+campaign_data.get('ad-brix').get('ver')+'","app_version_name":"'+campaign_data.get('app_version_name')+'","app_version_code":'+campaign_data.get('app_version_code')+',"referral_info":{"conversion_key":'+conversion_key_value+',"session_no":'+session_number_value+',"referrer_param":"'+app_data.get('referrer')+'"},"conversion_cache":['+conversion_key_value+'],"adbrix_user_info":{"adbrix_user_no":'+app_data.get('user_number')+',"shard_no":'+app_data.get('shard_number')+',"install_datetime":"'+event_date_1+'","install_mdatetime":'+str(int(time.time()*1000))+',"life_hour":0,"app_launch_count":1,"referral_key":'+conversion_key_value+',"referral_data":"'+referrer_additional_info+'","reengagement_conversion_key":-1,"last_referral_key":-1,"last_referral_data":"","last_referral_datetime":"","set_referral_key":true,"sig_type":0},"user_info":{"puid":"","mudid":"","openudid":"","openudid_md5":"","openudid_sha1":"","android_id_md5":"'+util.md5(device_data.get('android_id'))+'","android_id_sha1":"'+util.sha1(device_data.get('android_id'))+'","device_id_md5":"","device_id_sha1":"","google_ad_id":"'+device_data.get('adid')+'","google_ad_id_opt_out":false,"initial_ad_id":"'+device_data.get('adid')+'","ag":"'+email_ag_1+'","odin":"'+util.sha1(device_data.get('android_id'))+'","carrier":"'+device_data.get('carrier')+'","country":"'+device_data.get('locale').get('country')+'","language":"'+device_data.get('locale').get('language')+'","android_id":"'+base64.b64encode(device_data.get('android_id'))+'","first_install_time":"'+app_install_completed_time_date_1+'","first_install_time_offset":'+str(app_data.get('sec'))+'},"cohort_info":{},"device_info":{"vendor":"google","model":"'+device_data.get('model')+'","kn":"'+device_data.get('kernel')+'","is_wifi_only":false,"network":"wifi","noncustomnetwork":'+device_data.get('mnc')+',"os":"a_'+device_data.get('os_version')+'","ptype":"android","width":'+device_data.get('resolution').split('x')[1]+',"height":'+device_data.get('resolution').split('x')[0]+',"is_portrait":false,"utc_offset":'+str(float(app_data.get('sec'))/float(60)/float(60))+',"build_id":"'+device_data.get('build')+'"},"demographics":[{"demo_key":"userId","demo_value":"'+app_data.get('demo_value')+'"}],"complete_conversions":[],"activity_info":[{"prev_group":"'+app_data.get('group')+'","prev_activity":"'+app_data.get('activity')+'","group":"ret","activity":"end","param":"'+str(random.randint(390000,400000))+'","event_id":"'+str(uuid.uuid4())+'","created_at":"'+event_date+'"},{"prev_group":"ret","prev_activity":"","group":"session","activity":"retention","param":"","event_id":"'+str(uuid.uuid4())+'","created_at":"'+event_date+'"},{"prev_group":"","prev_activity":"","group":"session","activity":"start","param":"","event_id":"'+str(uuid.uuid4())+'","created_at":"'+event_date+'"}],"impression_info":[],"installation_info":{"market_referrer":"'+urlparse.unquote(app_data.get('referrer'))+'","install_actions_timestamp":{"market_install_btn_clicked":'+str(click_time_1)+',"app_install_start":'+str(app_install_start_time_1)+',"app_install_completed":'+str(app_install_completed_time_1)+',"app_first_open":'+str(app_first_open_time_1)+'}},"request_info":{"client_timestamp":'+str(int(time.time()))+'}}'


	print "--------------------------------------------------------"
	print data_value
	print  "--------------------------------------------------------"
			
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)

	update_activity(app_data,device_data,'session','start')
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


def ad_brix_getpopups(app_data,campaign_data,device_data,callno=None):
	url='https://as.ad-brix.com/v1/users/getPopups'
	method='post'
	headers={

	'Accept-Charset':'UTF-8',
	'Accept-Encoding':'gzip',
	'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
	'Content-Type':'application/json; charset=utf-8',

	}

	# data={

	# "appkey":campaign_data.get('ad-brix').get('app_key'),
	# "revision":0,
	# }

	if callno==2:
		r=',"revision":'+str(app_data.get('revision'))+'}'
	else:
		r=',"revision":0}'
			

	data='{"appKey":"'+campaign_data.get('ad-brix').get('app_key')+'"'+r

	

	params=None


	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


def adbrix_purchase(campaign_data, app_data, device_data,click_time_1,app_install_start_time_1,app_install_completed_time_1,app_install_completed_time_date_1,app_first_open_time_1,email_ag_1):
	url='https://apiab4c.ad-brix.com/v1/event'

	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	referrer_additional_info = ""
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('sn'):
			session_number_value = temp_a['sn'][0]
		else:
			session_number_value = str(random.randint(11111111111,99999999999))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
			
		if temp_a.get('cb_param1'):
			cb_param1_value = "cb_param1="+temp_a['cb_param1'][0]
			referrer_additional_info = referrer_additional_info+cb_param1_value
		else:
			cb_param1_value = " "
			
		if temp_a.get('cb_param2'):
			cb_param2_value = "cb_param2="+temp_a['cb_param2'][0]
			referrer_additional_info = referrer_additional_info+cb_param2_value
			
		else:
			cb_param2_value = " "
		
	else:
		app_data['referrer'] = urllib.quote("utm_source=google-play&utm_medium=organic")
		conversion_key_value = str(random.randint(1111111,9999999))
		session_number_value = str(random.randint(11111111111,99999999999))
		cb_param1_value = " "
		cb_param2_value = " "
			
		
	referrer_additional_info =  urllib.quote(referrer_additional_info)
	
	event_date_1 = get_date(app_data,device_data,para1=4,para2=7)
	event_date = get_date(app_data,device_data,para1=0,para2=0)
	


	data={"appkey":campaign_data.get('ad-brix').get('app_key'),
	"package_name":campaign_data.get('package_name'),
	"installer":"com.android.vending",
	"version":campaign_data.get('ad-brix').get('ver'),
	"app_version_name":campaign_data.get('app_version_name'),
	"app_version_code":campaign_data.get('app_version_code'),
	"referral_info":{"conversion_key":conversion_key_value,
	"session_no":session_number_value,
	"referrer_param":app_data.get('referrer')},
	"conversion_cache":[conversion_key_value],
	"adbrix_user_info":{"adbrix_user_no":app_data.get('user_number'),
	"shard_no":app_data.get('shard_number'),
	"install_datetime":event_date_1,
	"install_mdatetime":int(time.time()*1000),
	"life_hour":95,
	"app_launch_count":4,
	"referral_key":conversion_key_value,
	"referral_data":referrer_additional_info,
	"reengagement_conversion_key":-1,
	"last_referral_key":-1,
	"last_referral_data":"",
	"last_referral_datetime":"",
	"set_referral_key":True,
	"sig_type":0},
	"user_info":{"puid":"",
	"mudid":"",
	"openudid":"",
	"openudid_md5":"",
	"openudid_sha1":"",
	"android_id_md5":util.md5(device_data.get('android_id')),
	"android_id_sha1":util.sha1(device_data.get('android_id')),
	"device_id_md5":"",
	"device_id_sha1":"",
	"google_ad_id":device_data.get('adid'),
	"google_ad_id_opt_out":False,
	"initial_ad_id":device_data.get('adid'),
	"ag":email_ag_1,
	"odin":util.sha1(device_data.get('android_id')),
	"carrier":device_data.get('carrier'),
	"country":device_data.get('locale').get('country'),
	"language":device_data.get('locale').get('language'),
	"android_id":base64.b64encode(device_data.get('android_id')),
	"first_install_time":app_install_completed_time_date_1,
	"first_install_time_offset":str(app_data.get('sec'))},
	"cohort_info":{},
	"device_info":{"vendor":"google",
	"model":device_data.get('model'),
	"kn":device_data.get('kernel'),
	"is_wifi_only":False,
	"network":"wifi",
	"noncustomnetwork":device_data.get('mnc'),
	"os":'a_'+device_data.get('os_version'),
	"ptype":"android",
	"width":device_data.get('resolution').split('x')[1],
	"height":device_data.get('resolution').split('x')[0],
	"is_portrait":False,
	"utc_offset":str(float(app_data.get('sec'))/float(60)/float(60)),
	"build_id":device_data.get('build')},
	"demographics":[{"demo_key":"userId",
	"demo_value":app_data.get('demo_value')}],
	"complete_conversions":[],
	"activity_info":[],
	"impression_info":[],
	"installation_info":{"market_referrer":urlparse.unquote(app_data.get('referrer')),
	"install_actions_timestamp":{"market_install_btn_clicked":click_time_1,
	"app_install_start":app_install_start_time_1,
	"app_install_completed":app_install_completed_time_1,
	"app_first_open":app_first_open_time_1}},
	"request_info":{"client_timestamp":int(time.time())},
	"commerce_events":[{"event_type":"purchase",
	"event_id":str(uuid.uuid4()),
	"created_at":event_date,
	"attributes":{"order_id":"GPA."+str(util.get_random_string('decimal',4))+"-"+str(util.get_random_string('decimal',4))+"-"+str(util.get_random_string('decimal',4))+"-"+str(util.get_random_string('decimal',4)),
	"discount":0,
	"delivery_charge":0,
	"payment_method":"MobilePayment"},
	"products":[{"product_id":"g15f5f_py2_google_010",
	"product_name":"Week_Normal_3300",
	"price":3300,
	"discount":0,
	"quantity":1,
	"currency":"KRW",
	"extra_attrs":{"category1":"Week_Normal_3300"}}]}]}


	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':json.dumps(data)}

def update_activity(app_data,device_data,group='',activity=''):
	app_data['group']=group
	app_data['activity']=activity

		
def ad_brix_tracking_SetUserDemographic(app_data, device_data):

	url = 'https://tracking.ad-brix.com/v1/tracking/SetUserDemographic'
	headers = {	
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
			}
				
	method = 'post'
	#referrer_additional_info = ""
	
	data = {
					'k' : campaign_data.get('ad-brix').get('app_key'),
	}
	
	data_value = '{"puid":"","google_ad_id":"'+device_data.get('adid')+'","user_demo_info":[{"demo_key":"userId","demo_value":"'+app_data.get('demo_value')+'"}]}'	
	key = campaign_data.get('ad-brix').get('key_tracking')
	iv = campaign_data.get('ad-brix').get('iv_tracking')
	aes = AESCipherTracking(key)
	data['j'] = aes.encrypt(iv,data_value)
	
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}		

def get_demo_value(app_data,device_data):
	if not app_data.get('demo_value'):
		app_data['demo_value']=device_data.get('adid')

def ad_brix_get_schedule(campaign_data, app_data, device_data):

	method = 'post'
	
	url = 'http://campaign.ad-brix.com/v1/CampaignVer2/GetSchedule'
	
	header={
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
		}
		
	data = {
				'checksum': 0,
				'co' : device_data.get('locale').get('country'),
				'google_ad_id': device_data.get('adid'),
				'k': campaign_data.get('ad-brix').get('app_key'),
				'la': device_data.get('locale').get('language'),
				'os': 'a_'+device_data.get('os_version'),
				'puid': '',	
				'version' : 'a_'+device_data.get('os_version')
	}
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}		

def ad_brix_user_login(campaign_data, app_data, device_data):

	method = 'post'
	
	url = 'https://as.ad-brix.com/v1/users/login'
	
	header={
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/json; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
		}
		
	data = '{"appKey":"'+campaign_data.get('ad-brix').get('app_key')+'","adid":"'+device_data.get('adid')+'","userId":"'+app_data.get('demo_value')+'","deviceType":"Android"}'
	
	
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}	

def ad_brix_users_save(app_data,campaign_data,device_data):
	url='https://as.ad-brix.com/v1/users/save'
	method='post'
	headers={

	'Accept-Charset':'UTF-8',
	'Accept-Encoding':'gzip',
	'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
	'Content-Type':'application/json; charset=utf-8',

	}

	data={

	"appKey":campaign_data.get('ad-brix').get('app_key'), 
	"puid":device_data.get('adid'), 
	"attrs":{"gp4":device_data.get('locale').get('language'), 
	"gp7":"android", 
	"ap10":"a_"+device_data.get('os_version'), 
	"gp1":device_data.get('locale').get('country').upper(), 
	"gp5":device_data.get('model')}, 
	"sessionId":app_data.get('session_id')
	}

	params=None


	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def ad_brix_user_enablePushService(campaign_data, app_data, device_data):

	method = 'post'

	url = 'https://as.ad-brix.com/v1/users/enablePushService'

	header={
	'Accept-Charset': 'UTF-8',
	'Content-Type': 'application/json; charset=utf-8',
	'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
	'Accept-Encoding': 'gzip',
	}
	data='{"appKey":"'+campaign_data.get('ad-brix').get('app_key')+'","puid":"'+device_data.get('adid')+'","enable":true,"sessionId":"'+app_data.get('session_id')+'"}'



	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}

def ad_brix_update_conversion(campaign_data, app_data, device_data):

	method = 'post'
	
	url = 'https://as.ad-brix.com/v1/users/updateConversion'
	
	if app_data.get('referrer'):
		temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
		if temp_a.get('ck'):
			conversion_key_value = temp_a['ck'][0]
		else:
			conversion_key_value = str(random.randint(1111111,9999999))
	
	header={
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/json; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
		}
		
	data = '{"appKey":"'+campaign_data.get('ad-brix').get('app_key')+'","adid":"'+device_data.get('adid')+'","userId":"'+app_data.get('demo_value')+'","ck":'+conversion_key_value+',"sub_ck":-1}'
	
	
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}	
	
def ad_brix_update_registration(campaign_data, app_data, device_data):
	method = 'post'
	def_gcmToken(app_data)
	url = 'https://as.ad-brix.com/v1/users/updateRegistration'
	
	# if app_data.get('referrer'):
	# 	temp_a = urlparse.parse_qs(urlparse.unquote(app_data.get('referrer')))
		
	# 	if temp_a.get('ck'):
	# 		conversion_key_value = temp_a['ck'][0]
	# 	else:
	# 		conversion_key_value = str(random.randint(1111111,9999999))
	
	header={
				'Accept-Charset': 'UTF-8',
				'Content-Type': 'application/json; charset=utf-8',
				'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding': 'gzip',
		}
		
	data = 	'{"appKey":"'+campaign_data.get('ad-brix').get('app_key')+'","puid":"","adid":"'+device_data.get('adid')+'","registrationId":"'+app_data.get('gcm')+'","sessionId":"'+app_data.get('session_id')+'","userId":"'+app_data.get('demo_value')+'"}'

	
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': None, 'data': data}	
	
def def_gcmToken(app_data):
	if not app_data.get('gcm'):
		app_data['gcm']='APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def map_naver(app_data,campaign_data,device_data):
	url='https://openapi.naver.com/v1/map/reversegeocode'
	method='get'
	headers={
	'Accept-Encoding': 'gzip',
	'User-Agent': 'okhttp/3.9.0',
	'X-Naver-Client-Id': '3l_OBi2TTWSMtEwGL779',
	'X-Naver-Client-Secret': 'SXAuCEVRKj',

	}
	params={
	'query':str(app_data.get('latitude'))+','+str(app_data.get('longitude')),
	}

	data=None

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

	
	
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)
	
def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	gender = random.choice(['male', 'female'])
	gender_n = gender
		
	user_info = app_data.get('user_info')
	user_data = util.generate_name(gender=gender_n)
	user_info = {}
	username = user_data.get('username').lower()
	user_info['email'] = user_data.get('username').lower() + '@gmail.com'
	# user_info['f_name'] = user_data.get('firstname')
	# user_info['l_name'] = user_data.get('lastname')
	# user_info['sex'] = gender_n
	# user_info['gender'] = str(1) if gender_n == 'female' else str(2)
	# user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
	# user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
	# user_info['password'] = util.get_random_string(type='all',size=16)
	app_data['user_info'] = user_info	
	
	return app_data.get('user_info')
		
def get_screen_size(app_data, device_data):
	s_size = app_data.get('screen_size')
	if not s_size:
		s_size = random.choice(['normal','long'])
		app_data['screen_size'] = s_size
	return s_size
	
def get_screen_density(app_data, device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(app_data, device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'
			
def get_date(app_data,device_data,para1=0,para2=0):
	time.sleep(random.randint(para1,para2))
	# time.sleep(random.randint(0,0))
	make_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y%m%d%H%M%S")
	return date
	
def get_timestamp(para1=0,para2=0):
	time.sleep(random.randint(para1,para2))
	# time.sleep(random.randint(0,0))
	time1 = int(time.time())
	return time1


def current_location(device_data):
	url='http://lumtest.com/myip.json'
			
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}
