# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util,purchase
from sdk import NameLists
import time
import random
import json
import string
import datetime
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.yottagames.mafiawar',
	'app_size'			 : 79.0,
	'app_name' 			 :'Mafia City',
	'app_version_name' 	 : '1.3.766',#'1.3.763',#'1.3.736',1.3.759
	'app_version_code' 	 : '320',#'319',#'315',318
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'zoVuo66f4of7gdo2JFuATc',
		'dkh'		 : 'zoVuo66f',
		'buildnumber': '4.8.10',
		'version'	 : 'v4',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	def_firstLaunchDate(app_data,device_data)

	app_data["af_param_1"] = str(random.randint(941800000,941899999))

	app_data['registration']=False

 	###########	 CALLS		 ############
	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
		
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)
		
	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	catch(response,app_data,"appsflyer")
		
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data,temp=True)
	util.execute_request(**request)

	if random.randint(1,100)<=95:
		time.sleep(random.randint(4,6))
		af_complete_registration(campaign_data, app_data, device_data)

		time.sleep(random.randint(90,100))
		tutorial(campaign_data, app_data, device_data)
		app_data['registration']=True


		call_pattern(app_data,device_data,type1='install',day=False)


		if random.randint(1,100)<=50:
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data,isOpen=False)
			util.execute_request(**request)	


		


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	def_firstLaunchDate(app_data,device_data)
	if not app_data.get('af_param_1'):
		app_data["af_param_1"] = str(random.randint(941800000,941899999))


	if not app_data.get('registration'):
		app_data['registration']=False

 	###########	 CALLS		 ############
	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data, isOpen=False)
	util.execute_request(**request)
		
	
	if app_data.get('registration')==False:
		time.sleep(random.randint(4,6))
		af_complete_registration(campaign_data, app_data, device_data)

		time.sleep(random.randint(90,100))
		tutorial(campaign_data, app_data, device_data)
		app_data['registration']=True

	else:
		time.sleep(random.randint(8,10))
		login(campaign_data, app_data, device_data)


	if day==1:
		time.sleep(random.randint(3,5))
		day2retention(campaign_data, app_data, device_data)

		if random.randint(1,100)<=90:
			time.sleep(random.randint(180,300))
			af_mobile_recruit_day2(campaign_data, app_data, device_data)



	if app_data.get('registration')==True:
		call_pattern(app_data,device_data,type1='open',day=day)


		if random.randint(1,100)<=30:#30
			purchaseclick(campaign_data, app_data, device_data)

			if  purchase.isPurchase(app_data,day,advertiser_demand=50):
				time.sleep(random.randint(30,40))
				print "------------------purchase----------------------------------"
				pur_var=random.randint(1,100)
				if pur_var<=70:
					revenue='0.99'
					
				elif pur_var>70 and  pur_var<=90:
					revenue='4.99'
				
				else:
					revenue='9.99'


				#      purchase done but tokens appears after restarting game	


				print '\n'+'Appsflyer : Track____________________________________'
				request  = appsflyer_track(campaign_data, app_data, device_data,isOpen=False)
				util.execute_request(**request)	


				login(campaign_data, app_data, device_data)


				if revenue=='4.99':
					af_purchase_small(campaign_data, app_data, device_data)
				elif revenue=='9.99':
					af_purchase_small(campaign_data, app_data, device_data)
					af_purchase_plus(campaign_data, app_data, device_data)
				else:
					print "---------------nothing--------------------"


				af_purchase(campaign_data, app_data, device_data,revenue=revenue)


		if random.randint(1,100)<=50:
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data,isOpen=False)
			util.execute_request(**request)	

	return {'status':'true'}


def call_pattern(app_data,device_data,type1=False,day=False):

	

	if type1=='install':
		level_clear = random.choice([3,3,3,4,4,4,5,5,5,5])
	elif type1 == "open":
		if day == 1:
			level_clear = random.choice([2,2,2,3,3,3,4,4,4,4])
		else:
			level_clear = random.choice([2,2,2,3,3,3,4,4,4,4])
	else:
		print "not install and open"

	if not app_data.get('level_clear'):
		app_data['level_clear']=2


	achieved=[3,4,5,6,7,8,9,10]
	level=[5,6,7,10]
	
	
	playUpto= app_data.get('level_clear')+level_clear

	print playUpto

	print 'Playing game... '

	# level_list=[11,21,31,41,51,61,71,81,91]

	for i in range(app_data.get('level_clear'),playUpto):
		
		if i<=4:
			print "-------------level---------"+str(i)
			time.sleep(random.randint(150,250))
		elif i==6:
			print "-------------level---------"+str(i)
			time.sleep(random.randint(450,850))
		else:
			print "-------------level---------"+str(i)
			time.sleep(random.randint(420,480))




		if i in achieved:				
			af_mobile_level_achieved(campaign_data, app_data, device_data,i)

		if i in level:
			lvl(campaign_data, app_data, device_data,i)

		

		if i==4:
			time.sleep(random.randint(25,30))
			af_join_alliance(campaign_data, app_data, device_data)
			joinalliance(campaign_data, app_data, device_data)



		if random.randint(1,100)<=20:
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data,isOpen=False)
			util.execute_request(**request)	


			


		
	app_data['level_clear'] = playUpto
	print '*****************************************************'
	print app_data.get('level_clear')


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def af_complete_registration(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_complete_registration'
	eventName			= 'af_complete_registration'
	eventValue			= json.dumps({"af_param_2":0,"af_param_1":app_data.get('af_param_1'),"af_param_10":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def tutorial(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________tutorial'
	eventName			= 'tutorial'
	eventValue			= json.dumps({"af_param_10":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_mobile_level_achieved(campaign_data, app_data, device_data,n):
	print 'Appsflyer : EVENT___________________________af_mobile_level_achieved'+str(n)
	eventName			= 'af_mobile_level_achieved'+str(n)
	eventValue			= json.dumps({"af_level":n,"af_param_10":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)



def af_join_alliance(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_join_alliance'
	eventName			= 'af_join_alliance'
	eventValue			= json.dumps({"af_level":3,"af_param_10":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def joinalliance(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________join alliance'
	eventName			= 'join alliance'
	eventValue			= json.dumps({"af_param_10":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def lvl(campaign_data, app_data, device_data,n):
	print 'Appsflyer : EVENT___________________________lvl'+str(n)
	eventName			= 'lvl'+str(n)
	eventValue			= json.dumps({"af_param_10":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def login(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_login'
	eventName			= 'af_login'
	eventValue			= json.dumps({"af_param_2":0,"af_param_1":app_data.get('af_param_1'),"af_param_10":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def day2retention(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________day2 retention'
	eventName			= 'day2 retention'
	eventValue			= json.dumps({"af_param_10":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def af_mobile_recruit_day2(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_mobile_recruit_day2'
	eventName			= 'af_mobile_recruit_day2'
	eventValue			= json.dumps({"af_day":2,"af_param_10":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def purchaseclick(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________purchase click'
	eventName			= 'purchase click'
	eventValue			= json.dumps({"af_param_10":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_purchase(campaign_data, app_data, device_data,revenue):
	print 'Appsflyer : EVENT___________________________af_purchase'
	eventName			= 'af_purchase'
	eventValue			= json.dumps({"af_revenue":revenue,"af_param_10":"","af_currency":"USD"})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_purchase_small(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_purchase_small'
	eventName			= 'af_purchase_small'
	eventValue			= json.dumps({"af_param_2":"small","af_param_1":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def af_purchase_plus(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_purchase_plus'
	eventName			= 'af_purchase_plus'
	eventValue			= json.dumps({"af_param_2":"plus","af_param_1":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)






###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_sdks" : "0000000000",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "no",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"sensors" : [{u'sN': u'MAGNETOMETER', u'sVE': [8.85, 89.305, -64.218], u'sV': u'MTK', u'sVS': [8.4, 88.968, -60.423], u'sT': 2}, {u'sN': u'ACCELEROMETER', u'sVE': [-0.356, -0.553, 9.704], u'sV': u'MTK', u'sVS': [-0.385, -0.547, 9.704], u'sT': 1}, {u'sN': u'GYROSCOPE', u'sVE': [0, 0, 0], u'sV': u'MTK', u'sVS': [0, 0, 0], u'sT': 4}],
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_unity",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),

		}			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('counter')>2:
		if data.get('rfr'):
			del data['rfr']
		if data.get('deviceData').get('sensors'):
			del data['deviceData']['sensors']
		if data.get("p_receipt"):
			del data["p_receipt"]

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data,temp=False):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
 	def_(app_data,'counter')
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"app_name" : campaign_data.get('app_name'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand').upper(),
		"carrier" : device_data.get('carrier'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}

	if temp:
		num = str(int(time.time()*1000))
		data["af_gcm_token"] = num+","+app_data.get('af_gcm_token')
		app_data['af_gcm_token'] = num	

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"device_id" : app_data.get('uid'),
		"devkey" : campaign_data.get('appsflyer').get('key'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_unity",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : True,
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}



###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################

def change_char(s, p, r):
	return s[:p]+r+s[p+1:]

def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+ins_date+datetime.datetime.utcfromtimestamp(int(ins_time)/1000).strftime('%Y-%m-%d_%H%M%S')+'+0000'
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	sb = md5_result
	
	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')
	
	return sb



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def ref_time():
	time.sleep(random.randint(10,15))
	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"