# -*- coding: utf8 -*-
from sdk import util,purchase, NameLists,installtimenew
from sdk import getsleep
import uuid, random, time , urlparse, json,urllib,datetime
import clicker, Config

campaign_data = {
			'app_size' : 47.0,#46,#39.0,#38.0,#37.0,
			'package_name':'jp.gungho.padEN',
			'app_name' : 'PAD',
			'no_referrer': True,
			'app_version_code': '18000',#'17310',#'17300',#'17210',#'17200',#'17100',#'17000',#'16300',#'16210',#'16200',#'16000',#'15300',#'12330',
			'app_version_name': '18.0.0',#'17.3.1',#'17.3.0',#'17.2.1',#'17.2.0',#'17.1.0',#'17.0.0',#'16.3.0',#'16.2.1',#'16.2.0',#'16.0.0',#'15.3.0',#'12.3.3',
			'supported_countries': 'WW',
			'device_targeting':True,
			'ctr' : 6,
			'tracker':'kochava',
			'supported_os': '4.4',
			'kochava':{
					'sdk_version' : 'AndroidTracker 3.4.0',
					'sdk_protocol': '11',
					'kochava_app_id':'kopaden-and-tfwo',
					},

			'purchase'	: 
			 {
			 	'1'	: {'price': '0.99000000953674316',
			 			'content_id' : '1 Magic Stone'},
			 	'2' : {'price': '4.99000000895762486',
			 			'content_id' : '6 Magic Stones'}
			 },
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention':{
							1:80,
							2:77,
							3:75,
							4:72,
							5:68,
							6:62,
							7:58,
							8:55,
							9:51,
							10:48,
							11:45,
							12:40,
							13:38,
							14:37,
							15:35,
							16:32,
							17:30,
							18:28,
							19:25,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
			}
}
###########################################################
#														  #
#						INSTALL							  #
#														  #
###########################################################


def install(app_data, device_data):
	print "please wait..."
	print '---------------------------Install---------------------------------'
	###########		Initialize		############
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)
	global installed_date
	installed_date=int(app_data.get('times').get('install_complete_time'))
	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;

	if not app_data.get('ssid'):
		app_data['ssid'] = util.get_random_string("char_all",random.randint(4,10))	
	

	app_data['battery_level']=random.randint(5,85)
	app_data['screen_brightness']=random.uniform(0,1)
	app_data['tutorialComplete_flag'] =  False

	if not app_data.get('levels_achieved'):
		app_data['levels_achieved'] = 5	

	###########			Calls		############
	
	print '\n\nkochava kvinit------------------------------------------------------------------'
	Kv_init = Kvinit(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**Kv_init)

	# print '\n\nbase_na_adr------------------------------------------------------------------'
	# selfbase = base_na_adr(campaign_data, app_data, device_data)
	# result = util.execute_request(**selfbase)
	# na_addr=""
	# try:
	# 	na_addr=json.loads(result.get('data')).get('pj')
	# except:
	# 	print "Exception"
	# 	na_addr="180507003435"

	# print '\n\nretrieve_na_adr------------------------------------------------------------------'
	# selfretrieve = retrieve_na_adr(campaign_data, app_data, device_data, na_addr)
	# util.execute_request(**selfretrieve)

	print '\n\nkochava KvTracker initial-------------------------------------------------------'
	KvTrack = KvTracker(campaign_data, app_data, device_data)
	util.execute_request(**KvTrack)

	# print '\n\npadsv_self------------------------------------------------------------------'
	# padsvSelf = padsv_self(campaign_data, app_data, device_data, urlpart="ext/mon18050201115711048424925ae980dda4d45/extlist.bin")
	# util.execute_request(**padsvSelf)

	# print '\n\npadsv_self------------------------------------------------------------------'
	# padsvSelf = padsv_self(campaign_data, app_data, device_data, urlpart="efl/extf1804301815311570058725ae7cdc380a8a/adr/extdllist.bin")
	# util.execute_request(**padsvSelf)

	# j=1
	# for i in range(4):
	# 	print '\n\npadsv_self------------------------------------------------------------------'
	# 	padsvSelf = padsv_self(campaign_data, app_data, device_data, urlpart="ext/mon18050201115711048424925ae980dda4d45/mons_00"+str(i+j)+".bc")
	# 	util.execute_request(**padsvSelf)
	# 	if i==0:
	# 		j+=2

	if random.randint(1,100)<=95:
		register_user(app_data)
		print 'playing__________'
		time.sleep(random.randint(40,60))
		input_name(campaign_data, app_data, device_data)

		app_data['user_id'] = util.get_random_string('decimal',9)

		if random.randint(1,100)<=95:
			print 'playing__________'
			time.sleep(random.randint(400,600))
			tutorial_start(campaign_data, app_data, device_data)

			if random.randint(1,100)<=90:
				print 'playing__________'
				time.sleep(random.randint(400,600))
				tutorial_complete(campaign_data, app_data, device_data)
				app_data['tutorialComplete_flag'] =  True

				if random.randint(1,100)<=90:

					play=random.choice([7,8,9,10,15,20,25])
					print 'playing__________'
					time.sleep(random.randint(400,600))
					call_pattern(campaign_data, app_data, device_data, play)

	if random.randint(1,100)<=50:	
		time.sleep(random.randint(40,60))	
		print '\n\nkochava KvTracker session-------------------------------------------------------'
		KvTrack =android_pause_resume(campaign_data, app_data, device_data,pause=True,resume=True)
		util.execute_request(**KvTrack)

	
	return {'status':True}


###########################################################
#														  #
#						OPEN							  #
#														  #
###########################################################
def open(app_data, device_data,day=1):
	
	print '---------------------------Open---------------------------------'
	########		Initialize		############

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)
	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;

	if not app_data.get('battery_level'):

		app_data['battery_level']=random.randint(5,85)

	if not app_data.get('screen_brightness'):
		app_data['screen_brightness']=random.uniform(0,1)

	if not app_data.get('tutorialComplete_flag'):
		app_data['tutorialComplete_flag'] =  False

	if not app_data.get('levels_achieved'):
		app_data['levels_achieved'] = 5

	if not app_data.get('ssid'):
		app_data['ssid'] = util.get_random_string("char_all",random.randint(4,10))


	
	########			Calls		############	
	print '\n\nkochava kvinit------------------------------------------------------------------'
	Kv_init = Kvinit(campaign_data, app_data, device_data)
	util.execute_request(**Kv_init)

	if not app_data.get('user_id') and random.randint(1,100)<=95:
		register_user(app_data)
		print 'playing__________'
		time.sleep(random.randint(40,60))
		input_name(campaign_data, app_data, device_data)

		app_data['user_id'] = util.get_random_string('decimal',9)

		if app_data.get('tutorialComplete_flag')== False and random.randint(1,100)<=95:
			print 'playing__________'
			time.sleep(random.randint(400,600))
			tutorial_start(campaign_data, app_data, device_data)

			if random.randint(1,100)<=90:
				print 'playing__________'
				time.sleep(random.randint(400,600))
				tutorial_complete(campaign_data, app_data, device_data)
				app_data['tutorialComplete_flag'] =  True

	if random.randint(1,100)<=50 and app_data.get('tutorialComplete_flag') == True:

		play=random.choice([15,20,25,30,35])
		call_pattern(campaign_data, app_data, device_data, play, day=day)


	if purchase.isPurchase(app_data,day, advertiser_demand=10) and app_data.get('user_id'):
		time.sleep(random.randint(30,50))
		purchase_random = random.randint(1,100)
		
		if purchase_random <= 90:
			purchase_type = '1'	
		elif purchase_random > 90 and purchase_random <= 100:
			purchase_type = '2'
		else:
			purchase_type = '1'
					
		print '\n\nkochava Purchase Event------------------------------------------------------------------'
		event_name='Purchase'
		event_data={"currency":"USD","price":campaign_data.get('purchase').get(purchase_type).get('price'),"user_id":app_data.get('user_id'),'content_id':campaign_data.get('purchase').get(purchase_type).get('content_id')}
		KvTrack = android_events(campaign_data, app_data, device_data,event_name=event_name,event_data=event_data)
		util.execute_request(**KvTrack)	

	if random.randint(1,100)<=50:	
		time.sleep(random.randint(40,60))	
		print '\n\nkochava KvTracker session-------------------------------------------------------'
		KvTrack =android_pause_resume(campaign_data, app_data, device_data,pause=True,resume=True)
		util.execute_request(**KvTrack)

	return {'status':True}


def call_pattern(campaign_data, app_data, device_data, play=False,day=None):
	
	playUpto= app_data.get('levels_achieved')+play
	if day:
		print 'playing__________'
		time.sleep(random.randint(45,65))

	for i in range(app_data.get('levels_achieved'),playUpto):

		print 'Playing game level ' +str(i)
		

		if i in ([7, 8, 9, 10, 40, 50, 66]):
			if i==40:
				print 'playing__________'
				time.sleep(random.randint(800,1200))
			rank_achieved(campaign_data, app_data, device_data, rank=i)



		app_data['levels_achieved']=playUpto

def input_name(campaign_data, app_data, device_data):
	print '\n\nkochava NAME_INPUT Event-------------------------------------------------------'
	KvTrack = android_events(campaign_data, app_data, device_data,event_name="NAME_INPUT",event_data={"user_name":app_data.get('user').get('username'),"start_monster":0})
	util.execute_request(**KvTrack)

def tutorial_start(campaign_data, app_data, device_data):
	print '\n\nkochava TUTORIAL_START Event-------------------------------------------------------'
	KvTrack = android_events(campaign_data, app_data, device_data,event_name="TUTORIAL_START",event_data={"user_id":app_data.get('user_id')})
	util.execute_request(**KvTrack)

def tutorial_complete(campaign_data, app_data, device_data):
	print '\n\nkochava Tutorial Complete Event-------------------------------------------------------'
	KvTrack = android_events(campaign_data, app_data, device_data,event_name="Tutorial Complete",event_data={"user_id":app_data.get('user_id')})
	util.execute_request(**KvTrack)

def rank_achieved(campaign_data, app_data, device_data, rank=''):
	print '\n\nRANK_ACHIEVED Event-------------------------------------------------------'
	KvTrack = android_events(campaign_data, app_data, device_data,event_name="RANK_ACHIEVED",event_data={"user_id":app_data.get('user_id'), "rank": rank})
	util.execute_request(**KvTrack)




###########################################################
#														  #
#						SELF							  #
#														  #
###########################################################
def base_na_adr(campaign_data, app_data, device_data):
	url='https://dl-na.padsv.gungho.jp/base-na-adr.json'
	method='get'
	header={
		'User-Agent': 'GunghoPuzzleAndDungeon',
		'Accept-Charset': 'utf-8',
		'Content-Type': 'application/x-www-form-urlencoded',
		'Accept-Encoding': 'gzip'
		}
	params=None
	data = None
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': data}

def retrieve_na_adr(campaign_data, app_data, device_data, na_addr):
	url='https://dl-na.padsv.gungho.jp/pj/pad-na-adr-'+na_addr+'.json'
	method='get'
	header={
		'User-Agent': 'GunghoPuzzleAndDungeon',
		'Accept-Charset': 'utf-8',
		'Content-Type': 'application/x-www-form-urlencoded',
		'Accept-Encoding': 'gzip'
		}
	params=None
	data = None
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': data}

def padsv_self(campaign_data, app_data, device_data, urlpart):
	url='https://dl-na.padsv.gungho.jp/'+urlpart
	method='get'
	header={
		'User-Agent': 'GunghoPuzzleAndDungeon',
		'Accept-Charset': 'utf-8',
		'Content-Type': 'application/x-www-form-urlencoded',
		'Accept-Encoding': 'gzip'
		}
	params=None
	data = None
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': data}





###########################################################
#														  #
#						KOCHAVA							  #
#														  #
###########################################################
def Kvinit(campaign_data, app_data, device_data):
	
	set_kochava_device_id(app_data)
	set_updelta(app_data)
	set_uptime(app_data)
	#url='http://control.kochava.com/track/kvinit'
	url='http://kvinit-prod.api.kochava.com/track/kvinit'
	method='post'
	header={
		'User-Agent': set_ua(app_data,device_data),
		'Content-Type': 'application/json; charset=UTF-8',
		'X-Unity-Version': campaign_data.get('kochava').get('x_unity_version'),
		'Accept-Encoding': 'gzip',
		}
	params=None
	data = {"action":"init",
		"data":
			{
			"locale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').upper(),
			#"partner_name":"",
			# "last_post_time":"0",
			# "os_version": device_data.get('os_version'),
			"package":campaign_data.get('package_name'),
			"platform":"android",
			"timezone":device_data.get('local_tz_name'),
			#"session_tracking":"full",
			#"currency":"USD",
			# "os_version":device_data.get('manufacturer')+' '+device_data.get('os_version'),
			"uptime":str(app_data.get('uptime'))+'.'+util.get_random_string('decimal',3),
			# "updelta":app_data.get('updelta'),
			"usertime":int(time.time())
			},
			"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
			"kochava_device_id":app_data.get('kochava_device_id'),
			"nt_id": '01a49-'+str(uuid.uuid4()),
			"sdk_version":campaign_data.get('kochava').get('sdk_version'),
			"sdk_protocol":campaign_data.get('kochava').get('sdk_protocol'),
			'sdk_build_date': '2018-05-01T20:19:09Z',
			"send_date":datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',
			}

	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json_str}

def KvTracker(campaign_data, app_data, device_data,a=0):
	set_kochava_device_id(app_data)
	inc_updelta(app_data)
	inc_uptime(app_data)
	get_screen_inches(app_data)

	if not app_data.get('battery_level'):
		app_data['battery_level']=random.randint(5,85)

	if not app_data.get('screen_brightness'):
		app_data['screen_brightness']=random.uniform(0,1)

	url='http://control.kochava.com/track/json'
	method='post'
	header={
		'User-Agent': set_ua(app_data,device_data),
		'Content-Type': 'application/json; charset=UTF-8',
		'X-Unity-Version': campaign_data.get('kochava').get('x_unity_version'),
		'Accept-Encoding': 'gzip',
		}
	params={
		}
		
	inc = random.randint(1,5)
	inc_uptime(app_data,inc=inc+1)
	data ={"action":"initial",
			"data":{"device":device_data.get('model')+'-'+device_data.get('brand'),
					"adid":device_data.get('adid'),
					"app_version": campaign_data.get('app_name')+' '+campaign_data.get('app_version_code'),
					"architecture":device_data.get('cpu_abi'),
					"battery_level":app_data.get('battery_level'),
					"battery_status":"not_charging",
					"android_id":device_data.get('android_id'),
					# "carrier_name":"",
					"device_cores":device_data.get('cpu_core'),
					"device_orientation":"portrait",
					"locale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').upper(),
					"conversion_data":get_conversion_data(app_data),
					"conversion_type":"gplay",
					"installer_package":"com.android.vending",
					"connected_devices":["Keyboard: mtk-tpd"],
					#"package_name":campaign_data.get('package_name'),
					"app_short_string":campaign_data.get('app_version_name'),
					"disp_h":int(device_data.get('resolution').split('x')[0]),
					"disp_w":int(device_data.get('resolution').split('x')[1]),
					"screen_brightness":app_data.get('screen_brightness'),
					"network_conn_type":"wifi",
					"os_version":'Android '+device_data.get('os_version'),
					"app_limit_tracking":False,
					"device_limit_tracking":False,
					"carrier_name":device_data.get('carrier'),
					"volume":"0",
					"is_genuine":True,
					"language":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
					# "fb_attribution_id":"",
					"uptime":str(app_data.get('uptime'))+'.'+util.get_random_string('decimal',3),
					# "updelta":str(app_data.get('updelta'))+'.'+util.get_random_string('decimal',3),
					"manufacturer":device_data.get('manufacturer'),
					'notifications_enabled' :True,
					"product_name":device_data.get('product'),
					"package":campaign_data.get('package_name'),
					"timezone":device_data.get('local_tz_name'),
					'state_active'	: True,
					'ui_mode': 'Normal',
					# "last_post_time":"0."+str(util.get_random_string('decimal',3)),
					'install_referrer' : {
						'attempt_count'	: '1',
						'duration': '0.00'+ str(random.randint(1,9)),
						'status': 'missing_dependency'
					},
					'installed_date': installed_date,
					"ssid":app_data.get('ssid'),#device_data.get('carrier'),
					"screen_dpi":device_data.get('dpi'),
					"usertime":int(time.time()),
					"screen_inches":app_data.get('screen_inches')},
			"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
			"kochava_device_id":app_data.get('kochava_device_id'),
			"nt_id": '01a49-'+str(uuid.uuid4()),
			"sdk_version":campaign_data.get('kochava').get('sdk_version'),
			"sdk_procotol":campaign_data.get('kochava').get('sdk_protocol'),
			"send_date":datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',
			# "sdk_build_date":datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%b %d %Y %H:%M:%S PDT"),
			}

	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json.dumps(data)}

def kv_Query(campaign_data,app_data,device_data):

	inc = random.randint(1,5)
	inc_uptime(app_data,inc=inc+1)

	url='http://control.kochava.com/track/kvquery'
	method='post'
	header={
		'User-Agent': set_ua(app_data,device_data),
		'Content-Type': 'application/json; charset=UTF-8',
		'X-Unity-Version': campaign_data.get('kochava').get('x_unity_version'),
		'Accept-Encoding': 'gzip',
		}
	params={
		}
	data = {
		"action":"get_attribution",
		"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id":app_data.get('kochava_device_id'),
		"sdk_version":campaign_data.get('kochava').get('sdk_version'),
		"sdk_procotol":campaign_data.get('kochava').get('sdk_protocol')
	}	


	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json_str}	

def android_pause_launch(campaign_data, app_data, device_data,pause=False,launch=False):	
	url='http://control.kochava.com/track/json'
	method='post'
	header={
		'User-Agent': set_ua(app_data,device_data),
		'Content-Type': 'application/json',
		'X-Unity-Version': campaign_data.get('kochava').get('x_unity_version'),
		'Accept-Encoding': 'gzip',
	}
	params=None
	data=[]
	
	data1={
		"action":"session",
		"data":{
			"state":"pause",
			"usertime":int(time.time()),
			"uptime":str(random.randint(5,10)+5),
			"updelta":str(random.randint(5,10)+5),
		},
		"last_post_time":"0."+str(util.get_random_string('decimal',3)),
		"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
		"sdk_version":campaign_data.get('kochava').get('sdk_version'),
		"sdk_procotol":campaign_data.get('kochava').get('sdk_protocol'),
	}
	if pause==True:
		data.append(data1)
	
	data2={
		"action":"session",
		"data":{
			"state":"launch",
			"usertime":int(time.time()),
			"uptime":str(random.randint(5,10)+5),
			"updelta":str(random.randint(5,10)+5),
		},
		"last_post_time":"0."+str(util.get_random_string('decimal',9)),
		"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
		"sdk_version":campaign_data.get('kochava').get('sdk_version'),
		"sdk_procotol":campaign_data.get('kochava').get('sdk_protocol'),
	}
	
	if launch==True:
		data.append(data2)

	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json_str}
	
def android_pause_resume(campaign_data, app_data, device_data,pause=False,resume=False):	
	url='http://control.kochava.com/track/json'
	method='post'
	header={
		'User-Agent': set_ua(app_data,device_data),
		'Content-Type': 'application/json',
		'X-Unity-Version': campaign_data.get('kochava').get('x_unity_version'),
		'Accept-Encoding': 'gzip',
	}
	params=None
	data=[]
	
	data1={
		"action":"session",
		"data":{
			"state":"pause",

			# "device":device_data.get('model')+'-'+device_data.get('brand'),
			"architecture":device_data.get('cpu_abi'),
			"battery_level":app_data.get('battery_level'),
			"battery_status":"charging",
			# "carrier_name":"",
			"device":device_data.get('device'),
			"device_orientation":"portrait",
			"locale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').upper(),
			# "last_post_time":"0."+str(util.get_random_string('decimal',3)),
			#"package_name":campaign_data.get('package_name'),
			"app_version":campaign_data.get('app_name')+" "+campaign_data.get('app_version_code'),
			"app_short_string":campaign_data.get('app_version_name'),
			"disp_h":int(device_data.get('resolution').split('x')[0]),
			"disp_w":int(device_data.get('resolution').split('x')[1]),
			"screen_brightness":app_data.get('screen_brightness'),
			"network_conn_type":"wifi",
			"os_version":'Android '+device_data.get('os_version'),
			"connected_devices":["Keyboard: mtk-tpd"],
			# "app_limit_tracking":False,
			# "device_limit_tracking":False,
			#"bssid":device_data.get('mac'),
			"carrier_name":device_data.get('carrier'),
			"volume":"0",
			#"fb_attribution_id":"",
			"manufacturer":device_data.get('manufacturer'),
			'notifications_enabled'	: True,
			"product_name":device_data.get('product'),
			"timezone":device_data.get('local_tz_name'),
			"usertime":int(time.time()),
			"uptime":str(random.randint(5,10)+5)+'.'+str(random.randint(100,999)),
			'ui_mode': 'Normal',
			# "updelta":str(random.randint(5,10)+5)+'.'+str(random.randint(100,999)),
			"ssid":app_data.get('ssid'),#device_data.get('carrier'),
			'state_active' : True,
			'state_active_count':1,
			"screen_dpi":device_data.get('dpi'),
		},
		"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id":app_data.get('kochava_device_id'),
		"nt_id": '01a49-'+str(uuid.uuid4()),
		"sdk_version":campaign_data.get('kochava').get('sdk_version'),
		"sdk_protocol":campaign_data.get('kochava').get('sdk_protocol'),
		"send_date":datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',
	}
	if pause==True:
		data.append(data1)
	
	data2={
		"action":"session",
		"data":{
			"state":"resume",
			
			# "device":device_data.get('model')+'-'+device_data.get('brand'),
			"architecture":device_data.get('cpu_abi'),
			"battery_level":app_data.get('battery_level'),
			"battery_status":"charging",
			# "carrier_name":"",
			"device":device_data.get('device'),
			"device_orientation":"portrait",
			"locale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').upper(),
			# "last_post_time":"0."+str(util.get_random_string('decimal',3)),
			#"package_name":campaign_data.get('package_name'),
			"app_version":campaign_data.get('app_name')+" "+campaign_data.get('app_version_code'),
			"app_short_string":campaign_data.get('app_version_name'),
			"disp_h":int(device_data.get('resolution').split('x')[0]),
			"disp_w":int(device_data.get('resolution').split('x')[1]),
			"screen_brightness":app_data.get('screen_brightness'),
			"network_conn_type":"wifi",
			"os_version":'Android '+device_data.get('os_version'),
			"connected_devices":["Keyboard: mtk-tpd"],
			# "app_limit_tracking":False,
			# "device_limit_tracking":False,
			#"bssid":device_data.get('mac'),
			"carrier_name":device_data.get('carrier'),
			"volume":"0",
			#"fb_attribution_id":"",
			"manufacturer":device_data.get('manufacturer'),
			'notifications_enabled'	: True,
			"product_name":device_data.get('product'),
			"timezone":device_data.get('local_tz_name'),
			"usertime":int(time.time()),
			"uptime":str(random.randint(5,10)+5)+'.'+str(random.randint(100,999)),
			'ui_mode': 'Normal',
			# "updelta":str(random.randint(5,10)+5)+'.'+str(random.randint(100,999)),
			"ssid":app_data.get('ssid'),#device_data.get('carrier'),
			'state_active' : True,
			"screen_dpi":device_data.get('dpi'),
		},
		"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id":app_data.get('kochava_device_id'),
		"nt_id": '01a49-'+str(uuid.uuid4()),
		"sdk_version":campaign_data.get('kochava').get('sdk_version'),
		"sdk_protocol":campaign_data.get('kochava').get('sdk_protocol'),
		"send_date":datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',
	}
	
	if resume==True:
		data.append(data2)

	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json_str}
	
def android_deeplink(campaign_data, app_data, device_data):	
	url='http://control.kochava.com/track/json'
	method='post'
	header={
		'User-Agent': set_ua(app_data,device_data),
		'Content-Type': 'application/json',
		'X-Unity-Version': campaign_data.get('kochava').get('x_unity_version'),
		'Accept-Encoding': 'gzip',
	}
	params=None
	data=[] 

	data1={
			"action":"deeplink",
			# "currency":"USD",
			"data":{
				"source_app":"(null)",
				"uri": 'https://play.google.com/store/apps/details?id='+campaign_data.get('package_name')+'?referrer='+app_data.get('referrer'),
				"usertime":int(time.time()),
			},
			"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
			"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
			"sdk_version":campaign_data.get('kochava').get('sdk_version'),
			"sdk_procotol":campaign_data.get('kochava').get('sdk_protocol')
		}
	data.append(data1)

	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json_str}

def android_events(campaign_data, app_data, device_data,event_name=False,event_data=False):	
	url='http://control.kochava.com/track/json'
	method='post'
	header={
		'User-Agent': set_ua(app_data,device_data),
		'Content-Type': 'application/json; charset=UTF-8',
		'X-Unity-Version': campaign_data.get('kochava').get('x_unity_version'),
		'Accept-Encoding': 'gzip',
	}
	params=None

	data= [{"action":"event",
			"data":{
				"event_name":event_name,
				"event_data":event_data,
				# "device":device_data.get('model')+'-'+device_data.get('brand'),
				"architecture":device_data.get('cpu_abi'),
				"battery_level":app_data.get('battery_level'),
				"battery_status":"charging",
				# "carrier_name":"",
				"device":device_data.get('device'),
				"device_orientation":"portrait",
				"locale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').upper(),
				# "last_post_time":"0."+str(util.get_random_string('decimal',3)),
				#"package_name":campaign_data.get('package_name'),
				"app_version":campaign_data.get('app_name')+" "+campaign_data.get('app_version_code'),
				"app_short_string":campaign_data.get('app_version_name'),
				"disp_h":int(device_data.get('resolution').split('x')[0]),
				"disp_w":int(device_data.get('resolution').split('x')[1]),
				"screen_brightness":app_data.get('screen_brightness'),
				"network_conn_type":"wifi",
				"os_version":'Android '+device_data.get('os_version'),
				"connected_devices":["Keyboard: mtk-tpd"],
				# "app_limit_tracking":False,
				# "device_limit_tracking":False,
				#"bssid":device_data.get('mac'),
				"carrier_name":device_data.get('carrier'),
				"volume":"0",
				#"fb_attribution_id":"",
				"manufacturer":device_data.get('manufacturer'),
				'notifications_enabled'	: True,
				"product_name":device_data.get('product'),
				"timezone":device_data.get('local_tz_name'),
				"usertime":int(time.time()),
				"uptime":str(random.randint(5,10)+5)+'.'+str(random.randint(100,999)),
				'ui_mode': 'Normal',
				# "updelta":str(random.randint(5,10)+5)+'.'+str(random.randint(100,999)),
				"ssid":app_data.get('ssid'),#device_data.get('carrier'),
				'state_active' : True,
				"screen_dpi":device_data.get('dpi'),
			},
			"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
			"kochava_device_id":app_data.get('kochava_device_id'),
			"nt_id": '01a49-'+str(uuid.uuid4()),
			"sdk_version":campaign_data.get('kochava').get('sdk_version'),
			"sdk_protocol":campaign_data.get('kochava').get('sdk_protocol'),
			"send_date":datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z',
			# "sdk_build_date":datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%b %d %Y %H:%M:%S PDT"),
	}]

	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json_str}

def android_KvTracker_III(campaign_data, app_data, device_data,exit=0,launch=0):	
	url='http://control.kochava.com/track/kvTracker.php'
	method='post'
	header={
		'Content-Type' : 'application/json; charset=UTF-8',
		'User-Agent':set_ua(app_data,device_data),
		'Accept-Encoding':'gzip'
	}
	params=None
	data=[]

	for i in range(0,exit):

		data2={
				"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
				"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
				"action":"session",
				"data":{
					"usertime":str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
					"uptime":str(random.randint(5,10)+5),
					"updelta":str(random.randint(5,10)+5),
					"state":"exit"
				}
		}
		data.append(data2)

		for i in range(0,launch):

			data3={
				"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
				"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
				"action":"session",
				"data":{
					"usertime":str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
					"uptime":str(random.randint(5,10)+5),
					"updelta":str(random.randint(5,10)+5),
					"state":"launch"
				}
			}
			data.append(data3)



	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json_str}

def android_KvTracker_IV(campaign_data, app_data, device_data,exit=0,launch=0):	
	url='http://control.kochava.com/track/kvTracker.php'
	method='post'
	header={
		'Content-Type' : 'application/json; charset=UTF-8',
		'User-Agent':	set_ua(app_data,device_data),
		'Accept-Encoding':'gzip'
	}
	params=None

	app_data['get_registration_flag']=True


	data=[]


	data1={
				"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
				"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
				"action": "event",
				"last_post_time": 0,
				"currency": "USD",
				"data": {
					"usertime": str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
					"uptime": "0",
					"updelta": "2",
					"event_data": json.dumps({"AppTrigger":"RegistrationManagerImpl","Category":"OneTimeAttribution","OsVersion":device_data.get('sdk'),"TimeZone":device_data.get('local_tz_name'),"AppVersion":campaign_data.get('app_version_code'),"AppVersionFriendlyName":campaign_data.get('app_version_name'),"DirectedId":"amzn1.account.AFLOIX25LCUWKS2NUCCRE6B5NFUA","SourceCode":"AFAGBBN10261690Z1","isSharedPool":"true","MarketplaceId":"AF2M0KC94RCEA","PrimeMember":"false","FreeTrialEligible":"false"}),
					"event_name": "AppFirstRegistration"
				},
			}
	data2={

				"action": "event",
				"currency": "USD",
				"data": {
					"usertime": str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
					"uptime": "1",
					"updelta": "0",
					"event_data": json.dumps({"AppTrigger":"FreeTrialSignInCallbackImpl","Category":"OneTimeAttribution","OsVersion":device_data.get('sdk'),"TimeZone":device_data.get('local_tz_name'),"AppVersion":campaign_data.get('app_version_code'),"AppVersionFriendlyName":campaign_data.get('app_version_name'),"DirectedId":"amzn1.account.AFLOIX25LCUWKS2NUCCRE6B5NFUA","SourceCode":"AFAGBBN10261690Z1","isSharedPool":"true","MarketplaceId":"AF2M0KC94RCEA","PrimeMember":"false","FreeTrialEligible":"false","Asin":"<EMPTY>"}),
					"event_name": "LaunchTrialPage"
				},
				"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
				"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
				"last_post_time": 0

	}
	data.append(data1)
	data.append(data2)

	for i in range(0,exit):


		data3={
					"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
					"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
					"action":"session",
					"data":{
						"usertime":str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
						"uptime":str(random.randint(5,10)+5),
						"updelta":str(random.randint(5,10)+5),
						"state":"exit"
					}
		}
		data.append(data3)

	for i in range(0,launch):

		data4={
				"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
				"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
				"action":"session",
				"data":{
					"usertime":str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
					"uptime":str(random.randint(5,10)+5),
					"updelta":str(random.randint(5,10)+5),
					"state":"launch"
				}
		}
		data.append(data4)
    


	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json_str}

def android_KvTracker_V(campaign_data, app_data, device_data,exit=0,launch=0):	
	url='http://control.kochava.com/track/kvTracker.php'
	method='post'
	header={
		'Content-Type' : 'application/json; charset=UTF-8',
		'User-Agent':set_ua(app_data,device_data),
		'Accept-Encoding':'gzip'
	}
	params=None

	app_data['get_membership_flag']=True

	data=[]

	data1={

			"action": "event",
		"currency": "USD",
		"data": {
			"usertime": str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
			"uptime": str(random.randint(50,100)+8),
			"updelta": str(random.randint(50,100)+8),
			"event_data": json.dumps({"AppTrigger":"NewPurchaseJavascriptHandler","Category":"OneTimeAttribution","OsVersion":device_data.get('sdk'),"TimeZone":device_data.get('local_tz_name'),"AppVersion":campaign_data.get('app_version_code'),"AppVersionFriendlyName":campaign_data.get('app_version_name'),"DirectedId":"amzn1.account.AFJVONPGROOQKCNW3H7JNWQLB6JQ","SourceCode":"AFAGBBN10261690Z1","isSharedPool":"true","MarketplaceId":"AF2M0KC94RCEA","PrimeMember":"false","FreeTrialEligible":"true","CustomerSegment":"RegNoBuy","Asin":"undefined","MembershipAsin":"undefined"}),
			"event_name": "MembershipTaken"
					
		},
		"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
		"last_post_time": 0,
	}
	data2={

		"action": "event",
		"currency": "USD",
		"data": {
			"usertime": str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
			"uptime": "1",
			"updelta": "0",
			"event_data": json.dumps({"AppTrigger":"LibraryActivity","Category":"OneTimeAttribution","OsVersion":device_data.get('sdk'),"TimeZone":device_data.get('local_tz_name'),"AppVersion":campaign_data.get('app_version_code'),"AppVersionFriendlyName":campaign_data.get('app_version_name'),"DirectedId":"amzn1.account.AFLOIX25LCUWKS2NUCCRE6B5NFUA","SourceCode":"AFAGBBN10261690Z1","isSharedPool":"true","MarketplaceId":"AF2M0KC94RCEA","PrimeMember":"false","FreeTrialEligible":"true","CustomerSegment":"RegNoBuy"}),
			"event_name": "ViewInLibrary"
		},
		"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
		"last_post_time": 0
	}

	data.append(data1)
	data.append(data2)

	for i in range(0,exit) :
		data3={
					"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
					"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
					"action":"session",
					"data":{
						"usertime":str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
						"uptime":str(random.randint(5,10)+5),
						"updelta":str(random.randint(5,10)+5),
						"state":"exit"
					}
		}
		data.append(data3)

	for i in range(0,launch):

		data4={
				"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
				"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
				"action":"session",
				"data":{
					"usertime":str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
					"uptime":str(random.randint(5,10)+5),
					"updelta":str(random.randint(5,10)+5),
					"state":"launch"
				}
		}
		data.append(data4)

	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json_str}

def android_KvTracker_VI(campaign_data, app_data, device_data,exit=0,launch=0):	
	url='http://control.kochava.com/track/kvTracker.php'
	method='post'
	header={
		'Content-Type' : 'application/json; charset=UTF-8',
		'User-Agent':set_ua(app_data,device_data),
		'Accept-Encoding':'gzip'
	}
	params=None

	data=[]
	app_data['get_channel_flag']=True

	data1 = {
		"action": "event",
		"currency": "USD",
		"data": {
			"usertime": str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
			"uptime": str(random.randint(50,100)+8),
			"updelta": str(random.randint(50,100)+8),
			"event_data": json.dumps({"AppTrigger":"PlayerInitializer","Category":"OneTimeAttribution","OsVersion":device_data.get('sdk'),"TimeZone":device_data.get('local_tz_name'),"AppVersion":campaign_data.get('app_version_code'),"AppVersionFriendlyName":campaign_data.get('app_version_name'),"DirectedId":"amzn1.account.AFLOIX25LCUWKS2NUCCRE6B5NFUA","SourceCode":"AFAGBBN10261690Z1","isSharedPool":"true","MarketplaceId":"AF2M0KC94RCEA","PrimeMember":"false","FreeTrialEligible":"false","CustomerSegment":"TrialAL","Asin":"B018SO1QLQ","ChannelId":"12630447011"}),
			"event_name": "FirstChannelsListen"
		},
		"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
		"last_post_time": 0
	}
	data.append(data1)
	
	data4 = {
		"action": "event",
		"currency": "USD",
		"data": {
			"usertime": str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
			"uptime": str(random.randint(50,100)+8),
			"updelta": str(random.randint(50,100)+8),
			"event_data": json.dumps({"AppTrigger":"PlayerInitializer","Category":"OneTimeAttribution","OsVersion":device_data.get('sdk'),"TimeZone":device_data.get('local_tz_name'),"AppVersion":campaign_data.get('app_version_code'),"AppVersionFriendlyName":campaign_data.get('app_version_name'),"DirectedId":"amzn1.account.AFLOIX25LCUWKS2NUCCRE6B5NFUA","SourceCode":"AFAGBBN10261690Z1","isSharedPool":"true","MarketplaceId":"AF2M0KC94RCEA","PrimeMember":"false","FreeTrialEligible":"false","CustomerSegment":"TrialAL","Asin":"B018SO1QLQ","ChannelId":"12630447011"}),
			"event_name": "FirstListen"
		},
		"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
		"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
		"last_post_time": 0
	}
	if app_data.get('FirstListenFlag')==False:
		data.append(data4)
		app_data['FirstListenFlag']=True

	for i in range(0,exit):
		data2={
					"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
					"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
					"action":"session",
					"data":{
						"usertime":str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
						"uptime":str(random.randint(5,10)+5),
						"updelta":str(random.randint(5,10)+5),
						"state":"exit"
					}
		}
		data.append(data2)

	for i in range(0,launch):

		data3={
				"kochava_app_id":campaign_data.get('kochava').get('kochava_app_id'),
				"kochava_device_id":app_data.get('kochava_device_id') if app_data.get('kochava_device_id') else set_kochava_device_id(app_data),
				"action":"session",
				"data":{
					"usertime":str(app_data.get('kvTrack_usertime') +1) if app_data.get('kvTrack_usertime') else '',
					"uptime":str(random.randint(5,10)+5),
					"updelta":str(random.randint(5,10)+5),
					"state":"launch"
				}
		}
		data.append(data3)

	json_str=json.dumps(data)
	return {'url': url, 'httpmethod': method, 'headers': header, 'params': params, 'data': json_str}

			
def get_screen_inches(app_data):
	if not app_data.get('screen_inches'):
		app_data['screen_inches']=random.choice(['5.1','4.7','6','4.7','6.5'])

def set_updelta(app_data,forced=False):
	if not app_data.get('updelta') or forced:
		app_data['updelta'] = 0

def inc_updelta(app_data,inc=1,forced=False):
	set_updelta(app_data,forced)
	app_data['updelta'] += inc

def set_uptime(app_data,forced=False):
	if not app_data.get('uptime') or forced:
		app_data['uptime'] = 0

def inc_uptime(app_data,inc=1,forced=False):
	set_uptime(app_data,forced)
	app_data['uptime'] += inc

# def set_last_post_time(app_data):
# 	if not app_data.get('last_post_time'):
# 		app_data['last_post_time'] = float("{0:.8f}".format(random.uniform(0,3)))

def def_uid(app_data):
	if not app_data.get('uid'):
		app_data['uid']=str(random.randint(0,5))+str(util.get_random_string('decimal',6))+str(random.choice([1,3,5,7,9]))

def def_cid(app_data):
	if not app_data.get('cid'):
		app_data['cid'] = ''.join([random.choice("0123456789abcdef") for _ in range(64)])
	


###########################################################
#														  #
#						UTIL							  #
#														  #
###########################################################
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)


def set_ua(app_data,device_data):
	if not app_data.get('ua'):
		if int(device_data.get("sdk")) >19:
			ua = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
		else:
			ua = 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

	return ua
def set_kochava_device_id(app_data):
	if not app_data.get('kochava_device_id'):
		app_data['kochava_device_id'] ='KA340'+str(datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y%m%d"))+str(uuid.uuid4()).replace('-','')

def get_conversion_data(app_data):
	conversion_data = "utm_medium:(not set)&utm_source:(not set)"
	if app_data.get('referrer'):
		referrer = urllib.unquote(app_data.get('referrer'))
		referrerpart = urlparse.parse_qs(referrer)
		utm_medium = referrerpart.get('utm_medium')[0] if referrerpart.get('utm_medium') else ''
		utm_content = referrerpart.get('utm_content')[0] if referrerpart.get('utm_content') else ''
		utm_term = referrerpart.get('utm_term')[0] if referrerpart.get('utm_term') else ''
		utm_source = referrerpart.get('utm_source')[0] if referrerpart.get('utm_source') else ''
		utm_campaign = referrerpart.get('utm_campaign')[0] if referrerpart.get('utm_campaign') else ''
		conversion_data = 'utm_medium:'+utm_medium+'&utm_content:'+utm_content+'&utm_term:'+utm_term+'&utm_source:'+utm_source+'&utm_campaign:'+utm_campaign
	return conversion_data
	
def userId(app_data):
	if not app_data.get('user_id'):
		app_data['user_id']=str(util.get_random_string('hex',32))
	return app_data.get('user_id')


def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		# app_data['user']['sex'] 		= gender
		# app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		# app_data['user']['firstname'] 	= first_name
		# app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		# app_data['user']['email'] 		= username+"@gmail.com"
