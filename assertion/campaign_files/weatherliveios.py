import random,time,datetime,uuid,json
from sdk import NameLists
from sdk import util, installtimenew
from sdk import getsleep
import urllib
import clicker,Config

campaign_data = {
	'package_name' : 'com.apalonapps.wlf',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'ctr':6,
	'app_name' : 'Weather Live',
	'app_id':'749083919',
	'app_version_code':'6.9.2',#'6.5',#'6.3',#'6.2.1',#'5.17',#'449',5.15.1, 5.16.7
	'app_version_name':'6.9',#'6.5',#'6.3',#'6.2',#'5.17',#'1.12',5.15, 5.16
	'supported_os':'9.3',
	'supported_countries': 'WW',
	'device_targeting':True,
	'app_size' : 152.2,#142.0,#139.4,
	'sdk' : 'ios',
	'tracker':'adjust',
	'adjust':{
				'app_token': 'yd5vpkbueccf',
				'app_secret': '1162824340168352775613891294031386311394',
				'sdk':'ios4.17.3',#'ios4.17.2',#'ios4.16.0', #ios4.14.3
				'app_updated_at': '2019-09-24T23:39:03.000Z',#'2019-06-21T17:27:46.000Z',#'2019-04-19T21:18:39.000Z',#'2019-04-10T20:18:38.000Z',#'2018-11-23T21:54:13.000Z',
				'signature_id':'1'
			},
	'retention':{
					1:71,
					2:69,
					3:67,
					4:65,
					5:63,
					6:61,
					7:57,
					8:55,
					9:53,
					10:51,
					11:48,
					12:45,
					13:42,
					14:39,
					15:37,
					16:35,
					17:33,
					18:32,
					19:31,
					20:30,
					21:29,
					22:28,
					23:27,
					24:26,
					25:25,
					26:24,
					27:23,
					28:22,
					29:21,
					30:20,
					31:17,
					32:14,
					33:10,
					34:7,
					35:5,
	}
}


def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	
	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')

def get_install_reciept(app_data):
	install_receipt='MIISfwYJKoZIhvcNAQcCoIIScDCCEmwCAQExCzAJBgUrDgMCGgUAMIICIAYJKoZIhvcNAQcBoIICEQSCAg0xggIJMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMAwCAQ4CAQEEBAICAI0wDQIBAwIBAQQFDAM2LjUwDQIBDQIBAQQFAgMB1YgwDQIBEwIBAQQFDAM2LjUwDgIBAQIBAQQGAgQsph0PMA4CAQkCAQEEBgIEUDI1MzAOAgELAgEBBAYCBAC98DgwDgIBEAIBAQQGAgQxkSCWMBACAQ8CAQEECAIGHD6GJ/7rMBQCAQACAQEEDAwKUHJvZHVjdGlvbjAYAgEEAgECBBB4e9kHgiSI6F5aVBUx4jhXMBwCAQICAQEEFAwSY29tLmFwYWxvbmFwcHMud2xmMBwCAQUCAQEEFB1okJMz1ia+6iNn/qZNfN4djZxTMB4CAQgCAQEEFhYUMjAxOS0wNy0wM1QwNDoyMzo0MlowHgIBDAIBAQQWFhQyMDE5LTA3LTAzVDA0OjIzOjQyWjAeAgESAgEBBBYWFDIwMTktMDctMDNUMDQ6MjM6NDJaMDQCAQcCAQEELAVIr+7bssg+RCxIGmfiPHLAXQW3t0D2m/y9K9kIBIucGmV3l2bGUov9RbdvMFECAQYCAQEESRYJUjh/XiZLcLpK4d1fUF1FCI+o6W4mySkGY/4hdyKWXCLt5vPO9F6NnNbZR1ttvAo+Xkj5TOrtCysNI1kHPAtCoKoyHiZYHOGggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAC9oJIF8CMSxcbbAk1TGig0uMQRZcPlihTJPHxm6GymvVY0QiPT8OInw1+w87RGBI9fTNwXUeR+TkQ0+th7iCkpl72bbVl3Sk5gBW6LoBVaH1S1aAGZlKLf2PDfh4fc9AomBM8EB//LepOyT7C4eUQjQHPsV80y82t6ZyMNZBeEvTO2/XzBpIzzQGNcpPlJYQwZzikXVJN9aBh0HOLyNx4RdVWZ3MAA43CpDK+ugy0oOAx3tok+iFcKqqttu7uB5KPM3/aZzLx5t7GM4nU+j0rp57kehR/PX4mwU908sDgwHEf6cVb5H8pp98fUMURO6EDlxI0aCJ/lipKboNHRwezc='
	if not app_data.get('install_receipt'):
		app_data['install_receipt']=install_receipt

def install(app_data, device_data):
	print 'plz wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)

	get_install_reciept(app_data)
	print '*************Install**********************'
	###########		Initialize		############
	global tid
	tid=['YYA00001','UNS00000']
	global adType
	adType=['banner','native','interstitial']
	global adntwrk
	adntwrk=['facebook','admob','mopub','mopub_html','mopub_mraid','inneractive']
	
	app_data['adjust_call_time']=int(time.time())
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)

	
	shuffle=[0,1,2]

	set_sessionLength(app_data,forced=True,length=0)

	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	callback_params=json.dumps({"ldtrackid":"YYA00001"})
	#partner_params=json.dumps({"nlocale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),"app_version":"I"+str(campaign_data.get('app_version_name'))})
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data,callback_params=callback_params)
	util.execute_request(**adjustSession)
	
	time.sleep(random.randint(0,4))
	homepage(campaign_data, app_data, device_data,t1=4,t2=6)
	app_data['time_passes']=int(time.time())
	
	time.sleep(random.randint(30,40))
	print "\n----------------------------ADJUST Attribution ------------------------------------"
	request = adjust_attribution(campaign_data, app_data, device_data,t1=6,t2=8)
	util.execute_request(**request)

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	request = adjust_attribution(campaign_data, app_data, device_data,t1=6,t2=8,initiated_by='sdk')
	util.execute_request(**request)

	if random.randint(1,100)<=45:
		for i in range(0,random.randint(1,2)):
			time.sleep(random.randint(0,15))
			ads(campaign_data, app_data, device_data,t1=0,t2=0)

	time.sleep(random.randint(10,20))
	print "\n----------------------------ADJUST info ------------------------------------"
	adjust_sdk=adjust_sdkinfo(campaign_data, app_data, device_data)
	util.execute_request(**adjust_sdk)

	for i in range(0,len(shuffle)):
		random.shuffle(shuffle)
		print shuffle
		if i==shuffle[0] and random.randint(1,100)<=50:
			time.sleep(random.randint(0,15))
			ads(campaign_data, app_data, device_data,t1=0,t2=0)
		if i==shuffle[1] and random.randint(1,100)<=50:
			time.sleep(random.randint(5,10))
			clickonadd(campaign_data, app_data, device_data,t1=0,t2=0)
			time.sleep(random.randint(0,4))
			homepage(campaign_data, app_data, device_data,t1=4,t2=6)
		if i==shuffle[2] and random.randint(1,100)<=35:
			time.sleep(random.randint(40,60))
			dpnd(campaign_data, app_data, device_data,t1=0,t2=0)
			time.sleep(random.randint(0,4))
			homepage(campaign_data, app_data, device_data,t1=4,t2=6)

	return {'status':True}

###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):
	if not app_data.get('times'):
		print 'plz wait...'
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")

	get_install_reciept(app_data)

	global tid
	tid=['YYA00001','UNS00000']
	global adType
	adType=['banner','native','interstitial']
	global adntwrk
	adntwrk=['facebook','admob','mopub','mopub_html','mopub_mraid','inneractive']

	app_data['adjust_call_time']=int(time.time())
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)

	
	shuffle=[0,1,2]
	homepage(campaign_data, app_data, device_data,t1=4,t2=6)

	time.sleep(random.randint(4,7))
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,type1='open')
	util.execute_request(**adjustSession)

	time.sleep(random.randint(7,10))
	print "\n----------------------------ADJUST Attribution ------------------------------------"
	request = adjust_attribution(campaign_data, app_data, device_data,t1=6,t2=8)
	util.execute_request(**request)
	
	app_data['time_passes']=int(time.time())

	for i in range(0,len(shuffle)):
		random.shuffle(shuffle)
		print shuffle
		if i==shuffle[0] and random.randint(1,100)<=60:
			time.sleep(random.randint(0,15))
			ads(campaign_data, app_data, device_data,t1=0,t2=0)
		if i==shuffle[1] and random.randint(1,100)<=40:
			time.sleep(random.randint(5,10))
			clickonadd(campaign_data, app_data, device_data,t1=0,t2=0)
			time.sleep(random.randint(0,4))
			homepage(campaign_data, app_data, device_data,t1=4,t2=6)
		if i==shuffle[2] and random.randint(1,100)<=20:
			time.sleep(random.randint(40,60))
			dpnd(campaign_data, app_data, device_data,t1=0,t2=0)
			time.sleep(random.randint(0,4))
			homepage(campaign_data, app_data, device_data,t1=4,t2=6)

	return {'status':True}

def homepage(campaign_data, app_data, device_data,t1=0,t2=0):
	print '-----------------------ADJUST Event------------------homepage-----------'
	callback_params=json.dumps({"ldtrackid":random.choice(tid)})
	#partner_params=json.dumps({"nlocale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),"app_version":"I"+str(campaign_data.get('app_version_name'))})
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'di7a3w',callback_params=callback_params,t1=t1,t2=t2)  
	util.execute_request(**adjust_event_req)

def clickonadd(campaign_data, app_data, device_data,t1=0,t2=0):
	print '-----------------------ADJUST Event-----------------------clickonadd------'
	callback_params=json.dumps({"ldtrackid":random.choice(tid),"adType":random.choice(adType),"adNetwork":random.choice(adntwrk)})
	#partner_params=json.dumps({"nlocale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),"app_version":"I"+str(campaign_data.get('app_version_name'))})
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'7dnt39',callback_params=callback_params,t1=t1,t2=t2)  
	util.execute_request(**adjust_event_req)
	
	

def dpnd(campaign_data, app_data, device_data,t1=0,t2=0):
	print '-----------------------ADJUST Event-----------------------dpnd------'
	callback_params=json.dumps({"campaign_name":"Crosspromo__^BUL^amsBUX^^^_organic","ldtrackid":random.choice(tid)})
	#partner_params=json.dumps({"nlocale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),"app_version":"I"+str(campaign_data.get('app_version_name'))})
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'ni8lja',callback_params=callback_params,t1=t1,t2=t2)  
	util.execute_request(**adjust_event_req)
	
	

def ads(campaign_data, app_data, device_data,t1=0,t2=0):
	print '-----------------------ADJUST Event------------------------ads-----'
	callback_params=json.dumps({"ldtrackid":random.choice(tid),"adType":random.choice(adType),"adNetwork":random.choice(adntwrk)})
	#partner_params=json.dumps({"nlocale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),"app_version":"I"+str(campaign_data.get('app_version_name'))})
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'sz4dh5',callback_params=callback_params,t1=t1,t2=t2)  
	util.execute_request(**adjust_event_req)
	
	


###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,type1='install'):
	pushtoken(app_data)
	def_(app_data,'subsession_count')
	def_sessionLength(app_data)
	get_install_reciept(app_data)
	# app_data['subsession_count'] = 0
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'callback_params':'{"ldtrackid":"YYA00001"}',
			'connectivity_type':'2',
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			# 'created_at': datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'created_at': created_at,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			# 'ios_uuid':	app_data.get('ios_uuid'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			# 'sent_at':	datetime.datetime.utcfromtimestamp(time.time()+random.uniform(0,1)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'sent_at': sent_at,
			'session_count': make_session_count(app_data,device_data),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'os_build':	device_data.get('build'),
			'install_receipt':app_data.get('install_receipt'),
			'push_token':app_data.get('pushtoken'),
			'queue_size': 1,
			# 'session_length':app_data.get('sessionLength'),
			# 'subsession_count':app_data.get('subsession_count'),
			# 'time_spent':app_data.get('sessionLength'),
			# 'last_interval':get_lastInterval(app_data),
			# 'tce':	1,
			}

	if type1=='open':
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		if app_data.get('sessionLength'):
			data['session_length'] 	= app_data.get('sessionLength')
			data['time_spent'] 		= app_data.get('sessionLength')
		else:
			data['session_length'] 	= 0
			data['time_spent'] 		= 0
		data['subsession_count']= app_data.get('subsession_count')


	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')
		
	app_data['time_passes'] = int(time.time())

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adjust_sdkclick(campaign_data, app_data, device_data,t1=0,t2=0,callback_params=False,partner_params=False):
	pushtoken(app_data)
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	params={}

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	data = {
			# 'app_token': campaign_data.get('adjust').get('app_token'),
			# 'attribution_deeplink' : 1,
			# 'created_at': datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			# 'created_at': get_date(app_data,device_data,early=random.uniform(4,4.5)),
			'details': '{"Version3.1":{"iad-attribution":"false"}}',
			# 'environment' : 'production',
			# 'event_buffering_enabled': 0,
			# 'idfa': app_data.get('idfa_id'),
			# 'idfv':	app_data.get('idfv_id'),
			# 'needs_response_details': 1,
			# 'sent_at': datetime.datetime.utcfromtimestamp(time.time()+random.uniform(0,1)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			# 'sent_at': get_date(app_data,device_data),
			'source': 'iad3',
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'callback_params':callback_params,
			'connectivity_type':'2',
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			# 'created_at': datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'created_at': created_at,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			# 'ios_uuid':	app_data.get('ios_uuid'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			# 'sent_at':	datetime.datetime.utcfromtimestamp(time.time()+random.uniform(0,1)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'sent_at': sent_at,
			'session_count': app_data.get('session_count'),
			'tracking_enabled':	1,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'os_build':	device_data.get('build'),
			# 'queue_size': 1,
			# 'tce':	1,
			'last_interval': 0,
			'session_length': 0,
			'subsession_count': 1,
			'time_spent' : 0,
			'install_receipt':app_data.get('install_receipt'),
			'push_token':app_data.get('pushtoken'),
			}
	if callback_params:
		data['callback_params']=callback_params
	if partner_params:
		data['partner_params']=partner_params
	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'click')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adjust_attribution(campaign_data, app_data, device_data,t1=0,t2=0,initiated_by='backend'):
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	data={}
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	params = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink' : 1,
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'bundle_id': campaign_data.get('package_name'),
			# 'created_at': datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'created_at': created_at,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'initiated_by':initiated_by,
			'needs_response_details': 1,
			# 'sent_at': datetime.datetime.utcfromtimestamp(time.time()+random.uniform(0,1)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'sent_at': sent_at,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'os_build':	device_data.get('build'),
			# 'persistent_ios_uuid': app_data.get('ios_uuid'),
			}
	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('idfa'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	
def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=False, partner_params=False,t1=0,t2=0):
	created_at_event=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at_event=get_date(app_data,device_data)
	#pushtoken(app_data)
	def_sessionLength(app_data)
	def_(app_data,'subsession_count')
	if not app_data.get('device_token'):	
		app_data['device_token'] = util.get_random_string("all",22)	
		
	url = 'http://app.adjust.com/event'
	method = 'post'
	headers = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
			}
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	# timeSpent = int(time.time())-app_data.get('adjust_call_time')
	
	# app_data['timeSpent'] = timeSpent

	if not app_data.get('user_id'):
		app_data['user_id'] = util.get_random_string(type='decimal',size=8)
		
	# if not app_data.get('receipt_id'):
		# app_data['receipt_id'] = apple_receipt()

	data = {
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'hardware_name':'D101AP',	
			'event_buffering_enabled':	0,
			'cpu_type':device_data.get('cpu_type'),
			'attribution_deeplink': '1',
			# 'ios_uuid':app_data.get('ios_uuid'),
			'connectivity_type':'2',
			'os_name':'ios',
			'environment':'production',
			'needs_response_details':1,
			'event_count':app_data.get('event_count'),
			'time_spent':app_data.get('sessionLength'),
			'session_count':str(app_data.get('session_count')),
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			# 'created_at': datetime.datetime.utcfromtimestamp((time.time()-0.04)-app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'created_at': created_at_event,
			'event_token':event_token,
			'bundle_id':campaign_data.get('package_name'),
			'subsession_count':app_data.get('subsession_count'),
			'os_version':device_data.get('os_version'),
			'app_version': campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country'),
			'language':	device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'session_length':app_data.get('sessionLength'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': '1',
			'device_name':device_data.get('device_platform'),
			# 'sent_at': datetime.datetime.utcfromtimestamp((time.time())-app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'sent_at': sent_at_event,
			'os_build':	device_data.get('build'),
			# 'tce': '1',
			'install_receipt':app_data.get('install_receipt'),
			'push_token':app_data.get('pushtoken'),
			}
	if callback_params:
		data['callback_params']=callback_params
	if partner_params:
		data['partner_params']=partner_params	
	# if purchase:
	# 	data['revenue'] = purchase+'.00000'
	# 	data['currency'] = "USD"
	
	if data['event_count']==1:
		data['time_spent']=0
		data['session_length']=data['time_spent']
	elif data['event_count']==2:
		data['time_spent']=(int(time.time())-app_data['time_passes'])
		data['session_length']=data['time_spent']
	else:
		data['time_spent']=(int(time.time())-app_data['time_passes'])
		data['session_length']=data['time_spent']+6
	if data['event_count']>=3:
		data['push_token']=app_data.get('pushtoken')
	if data['event_count']==1:
		app_data['subsession_count']=1
		data['subsession_count']=app_data.get('subsession_count')
	elif data['event_count']==3:
		app_data['subsession_count']+=1
		data['subsession_count']=app_data.get('subsession_count')
		
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'event')

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':None, 'data': data}	

def adjust_sdkinfo(campaign_data, app_data, device_data):
	pushtoken(app_data)
	url = 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',

# 'Accept': '*/*',
		'User-Agent':urllib.pathname2url(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	params={}

	data = {

		'app_token': campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink' : 1,
# 'created_at': datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		'created_at': get_date(app_data,device_data),
		'environment' : 'production',
		'event_buffering_enabled': 0,
		'idfa': app_data.get('idfa_id'),
		'idfv':	app_data.get('idfv_id'),
		#'initiated_by':'backend',
		'needs_response_details': 1,
# 'sent_at': datetime.datetime.utcfromtimestamp(time.time()+random.uniform(0,1)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
		'sent_at': get_date(app_data,device_data),
		'persistent_ios_uuid': app_data.get('ios_uuid'),
		'push_token':app_data.get('pushtoken'),
		'source' :'push'
	}
	headers['Authorization']=headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###########################################################
#						UTIL							  #
###########################################################

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)
	
def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('hex',64)

# def device_id(app_data):
# 	if not app_data.get('x-device-id'):
# 		app_data['x-device-id']=str(uuid.uuid4()).upper()

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['phone'] 		="+971"+'-50-'+util.get_random_string('decimal',7)
		app_data['user']['password'] 	= app_data['user']['lastname']+random.choice(["@","!"])+app_data['user']['dob'].replace("-","") 

def get_auth(device_data,app_data,created_at,idfa,activity_type):
	data1= str(campaign_data['adjust']['app_secret']+activity_type+idfa+created_at)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('signature_id')+'",signature="'+sign+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'	
