# -*- coding:UTF-8 -*- 
import time,random,json,urllib,hashlib
from sdk import getsleep
from sdk import util, purchase,installtimenew
from Crypto.Cipher import AES
import clicker,Config


campaign_data = {
			'package_name':'com.com2us.probaseball3d.normal.freefull.google.global.android.common',
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'ctr':6,
			'app_name' : u'컴투스프로야구2019'.encode('utf-8'),#u'컴프야2018'.encode('utf-8'),
			'app_version_name': '5.1.1',#'5.1.0',#'5.0.9',#5.0.8, '5.0.7',#'5.0.6',#'5.0.5',#5.0.4, '5.0.3',#'5.0.2',#'5.0.1',#'4.1.5',#'4.1.4',#'4.1.3',#'4.1.2',#'4.1.1', #4.1.0, 4.1.7
			'supported_countries': 'WW',
			'device_targeting':True,
			'tracker':'apsalar',
			'no_referrer': True,
			'supported_os':'4.4',
			'app_size' : 67.0,#66.0,#43.0,#69.0, #65.0
			'apsalar':{
					'sdk':'Singular/v7.4.2.PROD', #Singular/v7.3.2.PROD
					'key':'com2ususa_ef7d31a4',
					'secret':'0ad35eca4cda05edd901a1fb892f39b1'
				},		
			'retention':{
							1:70,
							2:68,
							3:66,
							4:64,
							5:61,
							6:59,
							7:57,
							8:52,
							9:50,
							10:47,
							11:45,
							12:43,
							13:40,
							14:37,
							15:35,
							16:31,
							17:30,
							18:28,
							19:26,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5

						}
		}


def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0
	
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)
	
def install(app_data, device_data):

	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

	global start_s
	start_s = str(int(app_data.get('times').get('install_complete_time')*1000))	

	date_generator(app_data)

	if not app_data.get('u_id'):
		app_data['u_id'] = '20544'+str(random.randint(1000,9999))

	print '----------------------------- APSALAR Resolve ---------------------------'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**apsalar_Resolve)	
	
	print '----------------------------- APSALAR START -----------------------------'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_start)

	time.sleep(random.randint(1,4))
	eve_val=json.dumps({"referrer":get_referrer(app_data,device_data),"referrer_source":"service","clickTimestampSeconds":'0',"installBeginTimestampSeconds":'0',"current_device_time":int(time.time()*1000)})
	print '----------------------------- APSALAR EVENT --------------------------__InstallReferrer---'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__InstallReferrer',eve_val, k='e', user_id=False)
	util.execute_request(**apsalar_event)

	if random.randint(1,100)<=90:
		eve_val=json.dumps({"responseCode":"0","signedData":"0|193981735|com.com2us.probaseball3d.normal.freefull.google.global.android.common|74|ANlOHQPNlso6yrOz6N\/UMWepghuQfEvDwA==|1553491936606:VT=9223372036854775807","signature":"Tihd7uFo29PN6De9pNyVtQ3Pdo4d7R9hZ2N334fTXiIeGT70U+aFP68PWHBMJxIb04solFT1I8eUpen\/ibpZsAbPg1GKzw5mLJ4P1aMtXLfd8uAb7\/NXkjUDak\/\/tL7PX24ET\/UyPQNxgQWv2nRmT89pgxVjibGpesQKI5IjDEVsOrykkCJ\/Au7H3XQpmiLgraHWLD\/Nc5hUsYakwVWvRNLAIpzvgdHoYnMF5tcOVsYb\/P7QDeTIc5pl8ry8PBhVrn3J8KNmpHqb31Ye8A1ERGY39cD4s6mAQlAHdXlpbPWPIEjDR1prsuLpGWdML+UeFlxRiyEtHtBkafoUwsMuZQ=="})
		print '----------------------------- APSALAR EVENT --------------------------__LicensingStatus---'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__LicensingStatus',eve_val, k='e', user_id=False)
		util.execute_request(**apsalar_event)

	if random.randint(1,100)<=50:
		time.sleep(random.randint(480,700))
		print '----------------------------- APSALAR EVENT ---------------------------Tutorial Complete--'	
		apsalar_event = apsalarEvent1(campaign_data, app_data, device_data,event_name='Tutorial Complete')
		util.execute_request(**apsalar_event)
		app_data['tutorial_flag']=True
	
					
	return {'status':'true'}

def open(app_data,device_data,day=1):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

	global start_s
	start_s = str(int(time.time()*1000))

	date_generator(app_data)
	if not app_data.get('tutorial_flag'):
		app_data['tutorial_flag']= False
	
	print '----------------------------- APSALAR Resolve ---------------------------'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_Resolve)	
	
	print '----------------------------- APSALAR START -----------------------------'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data, isOpen = True)
	util.execute_request(**apsalar_start)

	if app_data.get('tutorial_flag')== False and random.randint(1,100)<=90:
		time.sleep(random.randint(480,700))	
		print '----------------------------- APSALAR EVENT ---------------------------Tutorial Complete--'	
		apsalar_event = apsalarEvent1(campaign_data, app_data, device_data,event_name='Tutorial Complete')
		util.execute_request(**apsalar_event)
		app_data['tutorial_flag']=True

	if purchase.isPurchase(app_data,day):
		time.sleep(random.randint(10,30))
		make_purchase(campaign_data, app_data, device_data)

	return {'status':'true'}
	
################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################
def make_purchase(campaign_data, app_data, device_data):
	time.sleep(random.randint(15,30))
	rev= random.choice(['1.9900000095367432','1.9900000095367432','1.9900000095367432','1.9900000095367432','1.9900000095367432','2.9900000000000000','2.9900000000000000','2.9900000000000000','2.9900000000000000','4.9900000000000000',])

	eve_val=urllib.quote(json.dumps({"pcc":"USD","r":rev,"pk":'Google:GPA.33'+util.get_random_string('decimal',2)+'-'+util.get_random_string('decimal',4)+'-'+util.get_random_string('decimal',4)+'-'+util.get_random_string('decimal',5),"pn":campaign_data.get('package_name'),"pc":"","pq":'1',"pp":rev}))
	print '----------------------------- APSALAR EVENT -----------------------------'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__iap__',eve_val, k='e')
	util.execute_request(**apsalar_event)

class AESCipher:
 def __init__(self, key):
		self.key = key

 def pad(self, raw):
		l = len(raw) % 16
		l = 16 - l
		for x in range(l):
			raw += ' '
		return raw

 def encrypt(self, iv, raw):
		raw = self.pad(raw)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return (cipher.encrypt(raw)).encode('hex')

 def decrypt(self, enc , iv):
	enc = enc.decode('hex')#base64.b64decode(enc)
	cipher = AES.new(self.key, AES.MODE_CBC, iv)
	return cipher.decrypt(enc)		

#########################################
#										#
# 			AppSalar funcation 			#
#										#
#########################################

def apsalarResolve(campaign_data, app_data, device_data):
	url = 'http://e-ssl.apsalar.com/api/v1/resolve'
	method = 'get'
	headers = {
		'User-Agent': 'Singular/SDK-v7.4.2.PROD', #Singular/SDK-v7.3.2.PROD
		'Accept-Encoding': 'gzip'
	}

	# params ='a='+campaign_data.get('apsalar').get('key')+'&c=wifi&i='+campaign_data.get('package_name')+'&k=AIFA&lag='+str(generateLag())+'&p=Android&pi=1&rt=json&u='+device_data.get('adid')+'&v='+device_data.get('os_version')

	data_dict = {
		"a":campaign_data.get('apsalar').get('key'),
		"c":"wifi",
		"i":campaign_data.get('package_name'),
		"k":"AIFA",
		"lag":str(generateLag()),
		"p":"Android",
		"pi":"1",
		"rt":"json",
		"u":device_data.get('adid'),
		"v":device_data.get('os_version'),
	}

	params = get_params(data_dict)

	app_data['singular_time']=time.time()

	# params = params.replace(' ','+')

	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h


	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarStart(campaign_data, app_data, device_data, isOpen = False):
	# generate_gcm_id(app_data)

	url = 'http://e-ssl.apsalar.com/api/v1/start'
	method = 'get'
	headers = {
		'User-Agent': 'Singular/SDK-v7.4.2.PROD', #Singular/SDK-v7.3.2.PROD
		'Accept-Encoding': 'gzip'
	}

	data_dict = {
				"a":campaign_data.get('apsalar').get('key'),
				"ab":device_data.get('cpu_abi',"armeabi"),
				"aifa":device_data.get('adid'),
				"av":campaign_data.get('app_version_name'),
				"br":device_data.get('brand'),
				"c":"wifi",
				"current_device_time":str(int(time.time()*1000)),
				"ddl_enabled":"true",
				"ddl_to":"60",
				"de":device_data.get('device'),
				"device_type":"phone",
				"device_user_agent":get_ua(device_data),
				"dnt":"0",
				# "event_index":"0",
				"i":campaign_data.get('package_name'),
				"install_time":str(int(app_data.get('times').get('download_end_time')*1000)),
				"is":"true",
				"k":"AIFA",
				"lag":str(generateLag()),
				"lc":device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
				"ma":device_data.get('manufacturer'),
				"mo":device_data.get('model'),
				"n":campaign_data.get('app_name'),
				"p":"Android",
				"pr":device_data.get('product'),
				"rt":"json",
				"s": start_s,
				"sdk":campaign_data.get('apsalar').get('sdk'),
				# "singular_install_id":app_data.get('singular_install_id'),
				"src":"com.android.vending",
				"u":device_data.get('adid'),
				"update_time":str(int(app_data.get('times').get('download_end_time')*1000)),
				"v":device_data.get('os_version'),

	}

	if isOpen:
		data_dict['is'] = 'false'

	params = get_params(data_dict)

	# params ='a='+str(campaign_data.get('apsalar').get('key'))+'&ab='+str(device_data.get('cpu_abi'))+'&aifa='+str(device_data.get('adid'))+'&av='+str(campaign_data.get('app_version_name'))+'&br='+str(device_data.get('brand'))+'&c=wifi&current_device_time='+str(int(time.time()*1000))+'&ddl_enabled=true&ddl_to=60&de='+str(device_data.get('device'))+'&device_type='+str(device_data.get('device_type'))+'&device_user_agent='+'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')'+'&dnt=0&i='+str(campaign_data.get('package_name'))+'&install_time='+str(app_data.get('install_time'))+'&is=true&k=AIFA&lag='+str(generateLag())+'&lc='+str(device_data.get('locale').get('language'))+'_'+str(device_data.get('locale').get('country'))+'&ma='+str(device_data.get('brand')).upper()+'&mo='+device_data.get('model')+'&n='+str(urllib.quote(campaign_data.get('app_name')))+'&p=Android&pr='+str(device_data.get('product'))+'&rt=json&s='+app_data.get('s_time')+'&sdk='+str(campaign_data.get('apsalar').get('sdk'))+'&src=com.android.vending&u='+str(device_data.get('adid'))+'&update_time='+str(app_data.get('install_time'))+'&v='+str(device_data.get('os_version'))

	# params = params.replace(' ','+')

	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h


	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarEvent(campaign_data, app_data, device_data,event_name,value,k, user_id=True):
	cal_seq(app_data)
	url = 'http://e-ssl.apsalar.com/api/v1/event'

	method = 'get'
	headers = {
		'User-Agent':'Singular/SDK-v7.4.2.PROD', #Singular/SDK-v7.3.2.PROD
		'Accept-Encoding': 'gzip',

	}

	if user_id==False:
		val='&c=wifi'

	else:
		val='&c=wifi&custom_user_id='+str(app_data.get('u_id'))


	t = str(time.time()-app_data.get('singular_time'))[:-8]

	data_dict = {
	"a":campaign_data.get('apsalar').get('key'),
	"aifa":device_data.get('adid'),#"ee81b619-c3ee-4c49-82ad-f70fce157d1f",
	"av":campaign_data.get('app_version_name'),
	"c":"wifi",
	# "event_index":str(app_data.get('seq')),
	"i":campaign_data.get('package_name'),
	"k":"AIFA",
	"lag":str(generateLag()),#"0.032",
	"n":str(event_name),
	"p":"Android",
	"rt":"json",
	"s": start_s,
	"sdk":campaign_data.get('apsalar').get('sdk'),
	"seq":str(app_data.get('seq')),
	# "singular_install_id":app_data.get('singular_install_id'),#"a6afffe5-d9e0-4298-b715-27a81d517794",
	"t":str(t),#"14.222",
	"u":device_data.get('adid'),#"ee81b619-c3ee-4c49-82ad-f70fce157d1f",
	
	}
	data_dict[k]=value

	params = get_params(data_dict)	

	# params = params.replace(' ','+')
	# h = getHash('?'+params,campaign_data.get('singular').get('secret'))
	# params = params+'&h='+h

	# params='a='+campaign_data.get('apsalar').get('key')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+val+'&'+k+'='+value+'&i='+campaign_data.get('package_name')+'&k=AIFA'+'&lag='+str(generateLag())+'&n='+event_name+'&p=Android'+'&rt=json&s='+app_data.get('s_time')+'&sdk='+campaign_data.get('apsalar').get('sdk')+'&seq='+str(cal_seq(app_data))+'&t='+generate_t()+'&u='+device_data.get('adid')

	# params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def apsalarEvent1(campaign_data, app_data, device_data,event_name='Tutorial Complete'):
	cal_seq(app_data)
	url = 'http://e-ssl.apsalar.com/api/v1/event'

	method = 'get'
	headers = {
		'User-Agent':'Singular/SDK-v7.4.2.PROD', #Singular/SDK-v7.3.2.PROD
		'Accept-Encoding': 'gzip',
		'cookie':''
	}

	t = str(time.time()-app_data.get('singular_time'))[:-8]

	data_dict = {
	"a":campaign_data.get('apsalar').get('key'),
	"aifa":device_data.get('adid'),#"ee81b619-c3ee-4c49-82ad-f70fce157d1f",
	"av":campaign_data.get('app_version_name'),
	"c":"wifi",
	"custom_user_id":str(app_data.get('u_id')),
	# "event_index":str(app_data.get('seq')),
	"i":campaign_data.get('package_name'),
	"k":"AIFA",
	"lag":str(generateLag()),#"0.032",
	"n":str(event_name),
	"p":"Android",
	"rt":"json",
	"s":str(int(app_data.get('times').get('install_complete_time')*1000)),
	"sdk":campaign_data.get('apsalar').get('sdk'),
	"seq":str(app_data.get('seq')),
	# "singular_install_id":app_data.get('singular_install_id'),#"a6afffe5-d9e0-4298-b715-27a81d517794",
	"t":str(t),#"14.222",
	"u":device_data.get('adid'),#"ee81b619-c3ee-4c49-82ad-f70fce157d1f",
	
	}

	params = get_params(data_dict)	


	# params='a='+campaign_data.get('apsalar').get('key')+'&aifa='+device_data.get('adid')+'&av='+campaign_data.get('app_version_name')+'&c=wifi&custom_user_id='+str(app_data.get('u_id'))+'&i='+campaign_data.get('package_name')+'&k=AIFA'+'&lag='+str(generateLag())+'&n='+event_name+'&p=Android'+'&rt=json&s='+app_data.get('s_time')+'&sdk='+campaign_data.get('apsalar').get('sdk')+'&seq='+str(cal_seq(app_data))+'&t='+generate_t()+'&u='+device_data.get('adid')

	# params = params.replace(' ','+')
	h = getHash('?'+params,campaign_data.get('apsalar').get('secret'))
	params = params+'&h='+h
	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def cal_seq(app_data):
	if not app_data.get('seq'):
		app_data['seq'] = 1
	else:
		app_data['seq'] = app_data.get('seq')+1
	return app_data.get('seq')
		
def random_string(len):
	return ''.join([random.choice("0123456789abcdef") for _ in range(len)])

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def generate_t():
	return "%.3f" %(random.uniform(300,1000))

def generateLag():
	return (random.randint(10,90)*0.001)

def get_referrer(app_data,device_data):
	if app_data.get('referrer'):
		return urllib.unquote(app_data.get('referrer'))
	else:
		return "utm_source=google-play&utm_medium=organic"

def date_generator(app_data):
	if not app_data.get('install_time'):
		app_data['install_time'] = str(int(app_data.get('times').get('install_complete_time')*1000))

	time.sleep(random.randint(60,100))	

	if not app_data.get('s_time'):
		app_data['s_time'] = str(int(time.time()*1000))

	print app_data.get('install_time')	

def get_params(data_value):
	key = data_value.keys()
	a = ''
	for value in sorted(key):
		a += str(urllib.quote_plus(value).encode('utf-8'))+'='+str(urllib.quote_plus(data_value.get(value)).encode('utf-8'))+'&'
	a = a.rsplit("&",1)[0]	

	return a

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
