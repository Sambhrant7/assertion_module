from sdk import util
from sdk import NameLists
from sdk import util,installtimenew
from Crypto.Cipher import AES
from datetime import timedelta
from sdk import getsleep
import time, datetime
import random
import json
import uuid
import urllib
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'com.edreams.travel',
	'app_name' 			 :'eDreams',
	'app_version_code'	 : "412801",#'412700',#'412500',#'44900',412201
	'app_version_name'	 : "4.128.1",#'4.127.0',#'4.125.0',#'4.49.0',4.122.1
	'no_referrer'		 : True,
	'supported_os'		 :'4.4',
	'app_size':				40.0, #45.0
	'tracker'			 :'mat',		
	'supported_countries':'WW',
	'ctr' 				 :6,
	'device_targeting':True,
	'mat':{
		'advertiser_id'	 : '188712',
		'iv'			 : 'heF9BATUfWuISyO8',
		'key'		     : '74e6a7e154ac762ed0d235be82072e18',
		'version'		 : '6.0.3',#'4.10.1',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5,
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############	
	
	###########		CALLS		################	

	print 'please wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")

	
	if not app_data.get('device_id'):
		app_data['device_id']=str(uuid.uuid4())


	print 'lat and lon generator'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	try:
	    json_lat_lon_result=json.loads(lat_lon_result.get('data'))
	    app_data['latitude']=json_lat_lon_result.get('geo').get('latitude')
	    app_data['longitude']=json_lat_lon_result.get('geo').get('longitude')
	    print app_data['latitude']
	    print app_data['longitude']
	except:
	    print "Exception"  
	    app_data['latitude']='28.57'
	    app_data['longitude']='77.32' 

    ### This call is for getting altitude from location coordinated (latitude & longitude) 

	print "Call for getting altitude\n\n"
	req=alitude_cal(app_data,campaign_data,device_data)
	res=util.execute_request(**req)
	try:
		fetch=json.loads(res.get('data'))
		app_data['altitude']=fetch.get('results')['0']['elevation']
		print "\n"
		print  app_data['altitude']
	except:
		app_data['altitude']='0.0'



	print '\nMat : SESSION____________________________________'
	request=mat_serve(campaign_data, app_data, device_data,includelatlon=False)
	app_data['api_hit_time'] = time.time()
	response=util.execute_request(**request)
	catch(response,app_data,'mat')
	set_OpenLogID(app_data)
	set_LastOpenLogID(app_data)

	for i in range(0,4):
		print '\nMat : SESSION____________________________________'
		request=mat_serve(campaign_data, app_data, device_data)
		response=util.execute_request(**request)
		catch(response,app_data,'mat')
		# set_OpenLogID(app_data)
		set_LastOpenLogID(app_data)
		time.sleep(random.randint(15,20))

	ed=edreams(app_data,device_data)
	util.execute_request(**ed)	
	
	time.sleep(random.randint(15,20))

	print '\nMat : SESSION____________________________________'
	request=mat_serve(campaign_data, app_data, device_data,ref=True)
	response=util.execute_request(**request)
	catch(response,app_data,'mat')
	# set_OpenLogID(app_data)
	set_LastOpenLogID(app_data)

	if random.randint(1,100)<=70: #70

		request=get_TS01a91593_call( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			resp_decode=response.get('res').headers.get('Set-Cookie')
			app_data['ts01a91593']=resp_decode.split('TS01a91593=')[1].split(';')[0]
		
		except:
			print 'Exception'
			app_data['ts01a91593']='017fb7f60aaf148ee06e41865427640302277aa3172cfae291a962e5f789aaf9e8df2cbb1caec768d73d18aa23231cc15242b8298d'

		request=get_cookies_call( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			resp_decode=response.get('res').headers.get('Set-Cookie')
			app_data['jsessionid']=resp_decode.split('JB64DOCKERJSESSIONID=')[1].split(';')[0]
			app_data['msl_test_token']=resp_decode.split('MSL.TestToken=')[1].split(';')[0]
			app_data['ts019382bd']=resp_decode.split('TS019382bd=')[1].split(';')[0]
			app_data['visit_test_token']=resp_decode.split('VISIT.TestTokenSet=')[1].split(';')[0]

		except:
			print 'Exception'
			app_data['jsessionid']='zKzVYm7FXSakjzIjgC0gxy7I.c18f09cec876'
			
			app_data['msl_test_token']='e0ZDLTE9MSwgRkMtMj0yLCBGQy0zPTIsIEZDLTQ9MSwgRkMtNT0xLCBGQy02PTMsIEZDLTc9MSwgRkktMT0xLCBGSS0yPTIsIEZJLTM9MSwgRkktND0yLCBGSS01PTEsIEZJLTY9MSwgRkktNz0xLCBGUjEtMT0xLCBGUjEtMj0xLCBGUjEtMz0xLCBGUjEtND0yLCBGUjEtNT0xLCBGUjEtNj00LCBGUjEtNz0xLCBGUjItMT0yLCBGUjItMj0yLCBGUjItMz0xLCBGUjItND0yLCBGUjItNT0yLCBGUjItNj0xLCBGUjItNz0xLCBPRjEtMT0xLCBPRjEtMj0yLCBPRjEtMz0xLCBPRjEtND0xLCBPRjEtNT0xLCBPRjEtNj0yLCBPRjEtNz0yLCBQTDEtMT0xLCBQTDEtMj0xLCBQTDEtMz0xLCBQTDEtND0xLCBQTDEtNT0yLCBQTDEtNj0xLCBQTDEtNz0yLCBQTDItMT0xLCBQTDItMj0xLCBQTDItMz0xLCBQTDItND0yLCBQTDItNT0xLCBQTDItNj0xLCBQTDItNz0xLCBQTDMtMT0xLCBQTDMtMj0xLCBQTDMtMz0xLCBQTDMtND0yLCBQTDMtNT0yLCBQTDMtNj0xLCBQTDMtNz00LCBQTDQtMT0xLCBQTDQtMj0zLCBQTDQtMz0xLCBQTDQtND0xLCBQTDQtNT0yLCBQTDQtNj0xLCBQTDQtNz0yLCBQTDUtMT0xLCBQTDUtMj0xLCBQTDUtMz0yLCBQTDUtND0xLCBQTDUtNT0yLCBQTDUtNj0yLCBQTDUtNz0xLCBQTDYtMT0xLCBQTDYtMj0xLCBQTDYtMz0xLCBQTDYtND0xLCBQTDYtNT0yLCBQTDYtNj0xLCBQTDYtNz0xLCBQUi0xPTEsIFBSLTI9MSwgUFItMz0xLCBQUi00PTEsIFBSLTU9MSwgUFItNj0yLCBQUi03PTEsIFVYMS0xPTEsIFVYMS0yPTEsIFVYMS0zPTIsIFVYMS00PTEsIFVYMS01PTEsIFVYMS02PTEsIFVYMS03PTEsIFVYMi0xPTIsIFVYMi0yPTEsIFVYMi0zPTEsIFVYMi00PTEsIFVYMi01PTEsIFVYMi02PTEsIFVYMi03PTQsIFVYMy0xPTIsIFVYMy0yPTEsIFVYMy0zPTIsIFVYMy00PTEsIFVYMy01PTEsIFVYMy02PTEsIFVYMy03PTMsIFgxNi0xPTEsIFgxNi0yPTEsIFgxNi0zPTIsIFgxNi00PTEsIFgxNi01PTIsIFgxNi02PTEsIFgxNi03PTIsIFgxNy0xPTEsIFgxNy0yPTIsIFgxNy0zPTEsIFgxNy00PTEsIFgxNy01PTEsIFgxNy02PTEsIFgxNy03PTIsIFgxOC0xPTIsIFgxOC0yPTEsIFgxOC0zPTIsIFgxOC00PTEsIFgxOC01PTEsIFgxOC02PTEsIFgxOC03PTIsIFgxOS0xPTEsIFgxOS0yPTIsIFgxOS0zPTEsIFgxOS00PTMsIFgxOS01PTEsIFgxOS02PTEsIFgxOS03PTEsIFgyMC0xPTIsIFgyMC0yPTMsIFgyMC0zPTEsIFgyMC00PTEsIFgyMC01PTEsIFgyMC02PTEsIFgyMC03PTIsIFgyMS0xPTEsIFgyMS0yPTEsIFgyMS0zPTEsIFgyMS00PTEsIFgyMS01PTEsIFgyMS02PTEsIFgyMS03PTEsIFgyMi0xPTIsIFgyMi0yPTIsIFgyMi0zPTEsIFgyMi00PTEsIFgyMi01PTMsIFgyMi02PTEsIFgyMi03PTJ9'
			
			app_data['ts019382bd']='017fb7f60a2c73d2c4f89f3545e14a6d5fa9d1e5192cfae291a962e5f789aaf9e8df2cbb1cb9390d8fe8515473f9f17e8c8d0f39062938308eed1f8a96452956684debd43d'
			
			app_data['visit_test_token']='1#4011-2#33025066634-3#38670-4#26891-5#1853837319-6#131545-7#123318-8#13081712930-9#36660878583-10#51165355238-11#22214077232-12#26641717357-13#34776485427-14#86471079273-15#33370318349-16#47988146054-17#51504451364-18#52035541663-19#96538890549-20#55896197379-21#45530139570-22#55777642435|17-09-2019.08:25'


		global locations_list, cityName_list
		locations_list=[]
		cityName_list=[]

		request=get_location_data_call( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			resp_decode=json.loads(response.get('data')).get('locations')
			if resp_decode and len(resp_decode)>=1:
				for i in range(len(resp_decode)):
					related_locations = resp_decode[i].get('relatedLocations')
					if related_locations and len(related_locations)>=1:
						for j in range(len(related_locations)):
							locations_list.append(str(related_locations[j].get('iataCode')))
							cityName_list.append(str(related_locations[j].get('cityName')))

			if len(locations_list)<1 or len(cityName_list)<1 or len(locations_list)!=len(cityName_list):

				locations_list=['LCY', 'LHR', 'LGW', 'LTN', 'STN', 'SEN', 'LGB', 'LAX', 'ELS', 'LDB', 'LYR', 'LGI', 'YXU', 'LRE', 'GGG', 'LNE']

				cityName_list=['London', 'London', 'London', 'London', 'London', 'London', 'Long Beach, CA', 'Los Angeles', 'East London', 'Londrina', 'Longyearbyen', 'Deadmans Cay Long Island', 'London', 'Longreach', 'Longview, TX', 'Lonorore']
							
		except:
			print 'Exception'

			locations_list=['LCY', 'LHR', 'LGW', 'LTN', 'STN', 'SEN', 'LGB', 'LAX', 'ELS', 'LDB', 'LYR', 'LGI', 'YXU', 'LRE', 'GGG', 'LNE']

			cityName_list=['London', 'London', 'London', 'London', 'London', 'London', 'Long Beach, CA', 'Los Angeles', 'East London', 'Londrina', 'Longyearbyen', 'Deadmans Cay Long Island', 'London', 'Longreach', 'Longview, TX', 'Lonorore']


		
		time.sleep(random.randint(30,60))
		search(campaign_data, app_data, device_data)

		for i in range(random.randint(1,4)):

			if random.randint(1,100)<=76:

				app_data['exception_data']=False


				request=get_unitPrice_call( campaign_data, device_data, app_data )
				response=util.execute_request(**request)
				try:
					output=json.loads(response.get('data')).get('itineraryResultsPage').get('itineraryResults')

					choice=random.randint(0, len(output)-1)

					app_data['unit_price']=output[choice].get('price').get('sortPrice')			

				except:
					print 'Exception'
					app_data['exception_data']=True

					app_data['unit_price']=6420.56


				time.sleep(random.randint(10,50))
				
				viewproduct(campaign_data, app_data, device_data)

	print '\nMat : SESSION____________________________________'
	request=mat_serve(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	catch(response,app_data,'mat')
	set_LastOpenLogID(app_data)

	###########		FINALIZE	################
		
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	# ###########		INITIALIZE		############	

	if not app_data.get('times'):
		print 'please wait...'
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android")
	
	if not app_data.get('device_id'):
		app_data['device_id']=str(uuid.uuid4())


	
	# ###########		CALLS		################	

	if not app_data.get('latitude') or not app_data.get('longitude'):
		print 'lat and lon generator'
		lat_lon=current_location(device_data)
		lat_lon_result = util.execute_request(**lat_lon)
		try:
		    json_lat_lon_result=json.loads(lat_lon_result.get('data'))
		    app_data['latitude']=json_lat_lon_result.get('geo').get('latitude')
		    app_data['longitude']=json_lat_lon_result.get('geo').get('longitude')
		    print app_data['latitude']
		    print app_data['longitude']
		except:
		    print "Exception"  
		    app_data['latitude']='28.57'
		    app_data['longitude']='77.32' 

	if not app_data.get('altitude'):

	    ### This call is for getting altitude from location coordinated (latitude & longitude) 

		print "Call for getting altitude\n\n"
		req=alitude_cal(app_data,campaign_data,device_data)
		res=util.execute_request(**req)
		try:
			fetch=json.loads(res.get('data'))
			app_data['altitude']=fetch.get('results')['0']['elevation']
			print "\n"
			print  app_data['altitude']
		except:
			app_data['altitude']='0.0'

	print '\nMat : SESSION____________________________________'
	request=mat_serve(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	catch(response,app_data,'mat')
	set_OpenLogID(app_data)
	set_LastOpenLogID(app_data)

	if day<=3:
		randomness=65
	elif day>3 and day<=5:
		randomness=45
	else:
		randomness=25

	if random.randint(1,100)<=randomness: #70

		if not app_data.get('ts01a91593'):

			request=get_TS01a91593_call( campaign_data, device_data, app_data )
			response=util.execute_request(**request)
			try:
				resp_decode=response.get('res').headers.get('Set-Cookie')
				app_data['ts01a91593']=resp_decode.split('TS01a91593=')[1].split(';')[0]
			
			except:
				print 'Exception'
				app_data['ts01a91593']='017fb7f60aaf148ee06e41865427640302277aa3172cfae291a962e5f789aaf9e8df2cbb1caec768d73d18aa23231cc15242b8298d'

		if not app_data.get('jsessionid') or not app_data.get('msl_test_token') or not app_data.get('ts019382bd') or not app_data.get('visit_test_token'):

			request=get_cookies_call( campaign_data, device_data, app_data )
			response=util.execute_request(**request)
			try:
				resp_decode=response.get('res').headers.get('Set-Cookie')
				app_data['jsessionid']=resp_decode.split('JB64DOCKERJSESSIONID=')[1].split(';')[0]
				app_data['msl_test_token']=resp_decode.split('MSL.TestToken=')[1].split(';')[0]
				app_data['ts019382bd']=resp_decode.split('TS019382bd=')[1].split(';')[0]
				app_data['visit_test_token']=resp_decode.split('VISIT.TestTokenSet=')[1].split(';')[0]

			except:
				print 'Exception'
				app_data['jsessionid']='zKzVYm7FXSakjzIjgC0gxy7I.c18f09cec876'
				
				app_data['msl_test_token']='e0ZDLTE9MSwgRkMtMj0yLCBGQy0zPTIsIEZDLTQ9MSwgRkMtNT0xLCBGQy02PTMsIEZDLTc9MSwgRkktMT0xLCBGSS0yPTIsIEZJLTM9MSwgRkktND0yLCBGSS01PTEsIEZJLTY9MSwgRkktNz0xLCBGUjEtMT0xLCBGUjEtMj0xLCBGUjEtMz0xLCBGUjEtND0yLCBGUjEtNT0xLCBGUjEtNj00LCBGUjEtNz0xLCBGUjItMT0yLCBGUjItMj0yLCBGUjItMz0xLCBGUjItND0yLCBGUjItNT0yLCBGUjItNj0xLCBGUjItNz0xLCBPRjEtMT0xLCBPRjEtMj0yLCBPRjEtMz0xLCBPRjEtND0xLCBPRjEtNT0xLCBPRjEtNj0yLCBPRjEtNz0yLCBQTDEtMT0xLCBQTDEtMj0xLCBQTDEtMz0xLCBQTDEtND0xLCBQTDEtNT0yLCBQTDEtNj0xLCBQTDEtNz0yLCBQTDItMT0xLCBQTDItMj0xLCBQTDItMz0xLCBQTDItND0yLCBQTDItNT0xLCBQTDItNj0xLCBQTDItNz0xLCBQTDMtMT0xLCBQTDMtMj0xLCBQTDMtMz0xLCBQTDMtND0yLCBQTDMtNT0yLCBQTDMtNj0xLCBQTDMtNz00LCBQTDQtMT0xLCBQTDQtMj0zLCBQTDQtMz0xLCBQTDQtND0xLCBQTDQtNT0yLCBQTDQtNj0xLCBQTDQtNz0yLCBQTDUtMT0xLCBQTDUtMj0xLCBQTDUtMz0yLCBQTDUtND0xLCBQTDUtNT0yLCBQTDUtNj0yLCBQTDUtNz0xLCBQTDYtMT0xLCBQTDYtMj0xLCBQTDYtMz0xLCBQTDYtND0xLCBQTDYtNT0yLCBQTDYtNj0xLCBQTDYtNz0xLCBQUi0xPTEsIFBSLTI9MSwgUFItMz0xLCBQUi00PTEsIFBSLTU9MSwgUFItNj0yLCBQUi03PTEsIFVYMS0xPTEsIFVYMS0yPTEsIFVYMS0zPTIsIFVYMS00PTEsIFVYMS01PTEsIFVYMS02PTEsIFVYMS03PTEsIFVYMi0xPTIsIFVYMi0yPTEsIFVYMi0zPTEsIFVYMi00PTEsIFVYMi01PTEsIFVYMi02PTEsIFVYMi03PTQsIFVYMy0xPTIsIFVYMy0yPTEsIFVYMy0zPTIsIFVYMy00PTEsIFVYMy01PTEsIFVYMy02PTEsIFVYMy03PTMsIFgxNi0xPTEsIFgxNi0yPTEsIFgxNi0zPTIsIFgxNi00PTEsIFgxNi01PTIsIFgxNi02PTEsIFgxNi03PTIsIFgxNy0xPTEsIFgxNy0yPTIsIFgxNy0zPTEsIFgxNy00PTEsIFgxNy01PTEsIFgxNy02PTEsIFgxNy03PTIsIFgxOC0xPTIsIFgxOC0yPTEsIFgxOC0zPTIsIFgxOC00PTEsIFgxOC01PTEsIFgxOC02PTEsIFgxOC03PTIsIFgxOS0xPTEsIFgxOS0yPTIsIFgxOS0zPTEsIFgxOS00PTMsIFgxOS01PTEsIFgxOS02PTEsIFgxOS03PTEsIFgyMC0xPTIsIFgyMC0yPTMsIFgyMC0zPTEsIFgyMC00PTEsIFgyMC01PTEsIFgyMC02PTEsIFgyMC03PTIsIFgyMS0xPTEsIFgyMS0yPTEsIFgyMS0zPTEsIFgyMS00PTEsIFgyMS01PTEsIFgyMS02PTEsIFgyMS03PTEsIFgyMi0xPTIsIFgyMi0yPTIsIFgyMi0zPTEsIFgyMi00PTEsIFgyMi01PTMsIFgyMi02PTEsIFgyMi03PTJ9'
				
				app_data['ts019382bd']='017fb7f60a2c73d2c4f89f3545e14a6d5fa9d1e5192cfae291a962e5f789aaf9e8df2cbb1cb9390d8fe8515473f9f17e8c8d0f39062938308eed1f8a96452956684debd43d'
				
				app_data['visit_test_token']='1#4011-2#33025066634-3#38670-4#26891-5#1853837319-6#131545-7#123318-8#13081712930-9#36660878583-10#51165355238-11#22214077232-12#26641717357-13#34776485427-14#86471079273-15#33370318349-16#47988146054-17#51504451364-18#52035541663-19#96538890549-20#55896197379-21#45530139570-22#55777642435|17-09-2019.08:25'


		global locations_list, cityName_list
		locations_list=[]
		cityName_list=[]

		request=get_location_data_call( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			resp_decode=json.loads(response.get('data')).get('locations')
			if resp_decode and len(resp_decode)>=1:
				for i in range(len(resp_decode)):
					related_locations = resp_decode[i].get('relatedLocations')
					if related_locations and len(related_locations)>=1:
						for j in range(len(related_locations)):
							locations_list.append(str(related_locations[j].get('iataCode')))
							cityName_list.append(str(related_locations[j].get('cityName')))

			if len(locations_list)<1 or len(cityName_list)<1 or len(locations_list)!=len(cityName_list):

				locations_list=['LCY', 'LHR', 'LGW', 'LTN', 'STN', 'SEN', 'LGB', 'LAX', 'ELS', 'LDB', 'LYR', 'LGI', 'YXU', 'LRE', 'GGG', 'LNE']

				cityName_list=['London', 'London', 'London', 'London', 'London', 'London', 'Long Beach, CA', 'Los Angeles', 'East London', 'Londrina', 'Longyearbyen', 'Deadmans Cay Long Island', 'London', 'Longreach', 'Longview, TX', 'Lonorore']
							
		except:
			print 'Exception'

			locations_list=['LCY', 'LHR', 'LGW', 'LTN', 'STN', 'SEN', 'LGB', 'LAX', 'ELS', 'LDB', 'LYR', 'LGI', 'YXU', 'LRE', 'GGG', 'LNE']

			cityName_list=['London', 'London', 'London', 'London', 'London', 'London', 'Long Beach, CA', 'Los Angeles', 'East London', 'Londrina', 'Longyearbyen', 'Deadmans Cay Long Island', 'London', 'Longreach', 'Longview, TX', 'Lonorore']

		time.sleep(random.randint(30,60))		

		search(campaign_data, app_data, device_data)

		if day<=3:
			nofday=random.randint(1,4)
		elif day>3 and day<=5:
			nofday=random.randint(1,2)
		else:
			nofday=random.randint(0,1)

		for i in range(nofday):

			if random.randint(1,100)<=70: #56

				app_data['exception_data']=False


				request=get_unitPrice_call( campaign_data, device_data, app_data )
				response=util.execute_request(**request)
				try:
					output=json.loads(response.get('data')).get('itineraryResultsPage').get('itineraryResults')

					choice=random.randint(0, len(output)-1)

					app_data['unit_price']=output[choice].get('price').get('sortPrice')			

				except:
					print 'Exception'
					app_data['exception_data']=True

					app_data['unit_price']=6420.56


				time.sleep(random.randint(10,50))
				
				viewproduct(campaign_data, app_data, device_data)

				if random.randint(1,100)<=20:
					time.sleep(random.randint(10,50))
					ViewBasket(campaign_data, app_data, device_data)

	# ###########		FINALIZE	################
		
	return {'status':'true'}

######### self calls ##############

def get_TS01a91593_call( campaign_data, device_data, app_data ):
	url= "https://msl.odigeo.com/mobile-api/msl/checkVersion"
	method= "get"
	headers= {       "Accept": "application/vnd.com.odigeo.msl.v3+json;charset=utf-8",
        "Accept-Encoding": "gzip,deflate,sdch",
        "Content-Type": "application/json",
        "Device-ID": "ANDROID;"+device_data.get('model')+";"+device_data.get('os_version')+";E;"+device_data.get('locale').get('country')+";"+app_data.get('device_id')+";"+device_data.get('locale').get('language')+";"+device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country')+";"+campaign_data.get('app_version_name')+";"+campaign_data.get('app_version_code')+";XL",
        "User-Agent": "okhttp/3.10.0"}

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def get_cookies_call( campaign_data, device_data, app_data ):
	url= "https://msl.odigeo.com/mobile-api/msl/visits"
	method= "post"
	headers= {       "Accept": "application/vnd.com.odigeo.msl.v4+json;charset=utf-8",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json; charset=UTF-8",
        "Cookie": "TS01a91593="+app_data.get('ts01a91593'),
        "Device-ID": "ANDROID;"+device_data.get('model')+";"+device_data.get('os_version')+";E;"+device_data.get('locale').get('country')+";"+app_data.get('device_id')+";"+device_data.get('locale').get('language')+";"+device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country')+";"+campaign_data.get('app_version_name')+";"+campaign_data.get('app_version_code')+";XL",
        "User-Agent": "okhttp/3.10.0"}

	params= None

	data= {"testAssignments":["BBU_BBUSTERS1462","BBU_BBUSTERS1502","BBU_BBUSTERS1450","BBU_BBUSTERS1452","BBU_BBUSTERS1464","BBU_BBUSTERS1448","BBU_BBUSTERS1190","BBU_BBUSTERS1350","BBU_BBUSTERS1302","BBU_BBUSTERS1024","BBU_BBUSTERS1026","BBU_BBUSTERS1202","BBU_BBUSTERS1213","BBU_BBUSTERS1222","BBU_BBUSTERS798","BBU_BBUSTERS1269","BBU_BBUSTERS_1373","BBU_BBUSTERS1289","BBU_BBUSTERS1381","BBU_BBUSTERS1405","BBU_BBUSTERS1437","BBU_BBUSTERS1375","BBU_BBUSTERS1353","TRA_TRIPATT517","TRA_TRIPATT675","TRA_TRIPATT1379","TRA_TRIPATT1428","DIS_DISCOVER1137","DIS_DISCOVER1402","DIS_DISCOVER1441","DIS_DISCOVER1054","DIS_DISCOVER1132","DIS_DISCOVER955","DIS_DISCOVER1090","DIS_DISCOVER1128","DIS_DISCOVER1231","DIS_DISCOVER1356","DIS_DISCOVER1186","DIS_DISCOVER1434","PTS_PTIVES145","PTS_PTIVES248","PTS_PTIVES221","PTS_PTIVES249","PTS_PTIVES294","GOZ_GOZ50","GOZ_GOZ23","ULC_UNL669","GOZ_GOZ110"]}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def get_nearestLocaltion( campaign_data, device_data, app_data ):
	url= "https://msl.odigeo.com/mobile-api/msl/nearestLocations"
	method= "get"
	headers= {       "Accept": "application/vnd.com.odigeo.msl.v3+json;charset=utf-8",
        "Accept-Encoding": "gzip,deflate,sdch",
        "Cookie": "JB64DOCKERJSESSIONID="+app_data.get('jsessionid')+"; VISIT.TestTokenSet="+app_data.get('visit_test_token')+"; MSL.TestToken="+app_data.get('msl_test_token')+"; TS01a91593="+app_data.get('ts01a91593')+"; TS019382bd="+app_data.get('ts019382bd')+"",
        "If-None-Match": "",
        "User-Agent": get_ua(device_data),}

	params= {       "latitude": app_data.get('latitude'),
        "locale": device_data.get('locale').get('language')+"_"+ device_data.get('locale').get('country'),
        "longitude": app_data.get('longitude'),
        "productType": "FLIGHT",
        "radiusInKm": random.randint(80,150)}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def get_location_data_call( campaign_data, device_data, app_data ):
	url= "https://msl.odigeo.com/mobile-api/msl/locations"
	method= "get"
	headers= {       "Accept": "application/vnd.com.odigeo.msl.v4+json;charset=utf-8",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "Cookie": "JB64DOCKERJSESSIONID="+app_data.get('jsessionid')+"; VISIT.TestTokenSet="+app_data.get('visit_test_token')+"; MSL.TestToken="+app_data.get('msl_test_token')+"; TS01a91593="+app_data.get('ts01a91593')+"; TS019382bd="+app_data.get('ts019382bd'),
        "Device-ID": "ANDROID;"+device_data.get('model')+";"+device_data.get('os_version')+";E;"+device_data.get('locale').get('country')+";"+app_data.get('device_id')+";"+device_data.get('locale').get('language')+";"+device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country')+";"+campaign_data.get('app_version_name')+";"+campaign_data.get('app_version_code')+";XL",
        "User-Agent": "okhttp/3.10.0"}

	params= {       "departureOrArrival": "DEPARTURE",
        "distanceUnit": "KM",
        "productType": "FLIGHT",
        "searchKey": random.choice(['lon','ind','esp','fra'])}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def get_unitPrice_call( campaign_data, device_data, app_data ):
	url= "https://msl.odigeo.com/mobile-api/msl/search"
	method= "post"
	headers= {       "Accept": "application/vnd.com.odigeo.msl.v3+json;charset=utf-8",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json; charset=UTF-8",
        "Cookie": "JB64DOCKERJSESSIONID="+app_data.get('jsessionid')+"; VISIT.TestTokenSet="+app_data.get('visit_test_token')+"; MSL.TestToken="+app_data.get('msl_test_token')+"; TS01a91593="+app_data.get('ts01a91593')+"; TS019382bd="+app_data.get('ts019382bd'),
        "Device-ID": "ANDROID;"+device_data.get('model')+";"+device_data.get('os_version')+";E;"+device_data.get('locale').get('country')+";"+app_data.get('device_id')+";"+device_data.get('locale').get('language')+";"+device_data.get('locale').get('language')+"_"+device_data.get('locale').get('country')+";"+campaign_data.get('app_version_name')+";"+campaign_data.get('app_version_code')+";XL",
        "User-Agent": "okhttp/3.10.0"}

	params= None
	try:
		departure_iataCode=locations_list[app_data.get('source')]
		departure_name=cityName_list[app_data.get('source')]

		destination_iataCode=locations_list[app_data.get('destination')]
		destination_name=cityName_list[app_data.get('destination')]


	except:
		print 'Exception'
		departure_iataCode='LNE'
		departure_name='Lonorore'

		destination_iataCode='LHR'
		destination_name='London'

	if not app_data.get('dateString'):

		num=random.randint(10,15)
		date1=datetime.datetime.utcfromtimestamp((time.time()))+timedelta(days=num)
		app_data['date1']=int(time.mktime(date1.timetuple()))
		date2=datetime.datetime.utcfromtimestamp((time.time()))+timedelta(days=num+1)
		app_data['date2']=int(time.mktime(date2.timetuple()))

		app_data['dateString']=(date1.strftime('%d-%m-%Y'))

	data= {       "itinerarySearchRequest": {       "cabinClass": "TOURIST",
                                          "isMember": False,
                                          "numAdults": 1,
                                          "numChildren": 0,
                                          "numInfants": 0,
                                          "resident": False,
                                          "searchMainProductType": "FLIGHT",
                                          "segmentRequests": [       {       "dateString": app_data.get('dateString'),
                                                                             "departure": {       "iataCode": departure_iataCode,
                                                                                                  "name": departure_name},
                                                                             "destination": {       "iataCode": destination_iataCode,
                                                                                                    "name": destination_name}},
                                                                                                    ]
                                                                                                    }}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################
def search(campaign_data, app_data, device_data):

	app_data['source']=random.randint(0,len(locations_list)-1)
	app_data['destination']=random.randint(0,len(locations_list)-1)
	
	while(app_data.get('source')==app_data.get('destination')):
		app_data['destination']=random.randint(0,len(locations_list)-1)

	if not app_data.get('dateString'):

		num=random.randint(10,15)
		date1=datetime.datetime.utcfromtimestamp((time.time()))+timedelta(days=num)
		app_data['date1']=int(time.mktime(date1.timetuple()))
		date2=datetime.datetime.utcfromtimestamp((time.time()))+timedelta(days=num+1)
		app_data['date2']=int(time.mktime(date2.timetuple()))

		app_data['dateString']=(date1.strftime('%d-%m-%Y'))

	print '\nMat : EVENT____________________________search'
	request=mat_serve(campaign_data, app_data, device_data, action="conversion:search",ref=False)
	util.execute_request(**request)

def viewproduct(campaign_data, app_data, device_data):
	print '\nMat : EVENT____________________________ViewProduct'
	request=mat_serve(campaign_data, app_data, device_data, action="conversion:ViewProduct",ref=False)
	util.execute_request(**request)

def ViewBasket(campaign_data, app_data, device_data):
	print '\nMat : EVENT____________________________ViewBasket'
	request=mat_serve(campaign_data, app_data, device_data, action="conversion:ViewBasket",ref=False)
	util.execute_request(**request)



def edreams(app_data,device_data):
	url='https://169736.measurementapi.com/serve'
	method='get'

	headers={
			'Accept-Encoding': 'gzip',
			'User-Agent': 'Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6',
			'Referer': 'http://www.google.com',
	}

	params={
			'action':'click',
			'publisher_id':169736,
			'my_campaign':'SHARE_APP',
			'my_publisher':'Internal',
			'my_site':'ED',
			'site_id_android':103306,
			'site_id_ios':103298,
			'site_id_web':119555 
			}
	data=None

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# mat_serve()	: method
# parameter 	: campaign_data, app_data, device_data,
#				  action, includeLogID, isOpen
#
# Simulates Mat's behaviour whenever the App gets open
# or incase of an in-app event.
###################################################################
def mat_serve(campaign_data, app_data, device_data, action="session",ref= False,includelatlon=True):
	# def_appInstallData(app_data)
	def_matID(app_data)
	event = ""
	if "conversion" in action:
		event 	= action.split(":")[1]
		action 	= action.split(":")[0]

	method	= 'post'
	url 	= 'http://'+campaign_data['mat']['advertiser_id']+'.engine.mobileapptracking.com/serve'
	headers = {
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/json',
		'Accept'			: 'application/json',
		'User-Agent'		: get_ua(device_data),
	}

	# date1=int(time.time())
	# date2=int(time.time()+random.randint(172750,172800))

	data={"data":[]}
	params = {
		'action'			: action,
		'advertiser_id'		: campaign_data['mat']['advertiser_id'],
		'package_name'		: campaign_data['package_name'],
		'sdk'				: 'android',
		'sdk_retry_attempt'	: '0',
		'transaction_id'	: str(uuid.uuid4()),
		'ver'				: campaign_data['mat']['version'],
	}
	data_arr = {
		'app_name'				: campaign_data['app_name'],
		'app_version'			: campaign_data['app_version_code'],
		'app_version_name'		: campaign_data['app_version_name'],
		'build' 				: device_data['build'],
		'connection_type'		: "wifi",
		'conversion_user_agent'	: get_ua(device_data),
		# 'currency_code'			: 'USD',
		'device_brand'			: device_data['brand'],
		'device_cpu_type'		: device_data['cpu_abi'],
		'device_model'			: device_data['model'],
		'existing_user' 		: 1,
		'google_ad_tracking_disabled': 0,
		'google_aid'			: device_data['adid'],
		'insdate'				: str(int(app_data.get('times').get('install_complete_time'))),
		'installer'				: 'com.android.vending',
		'locale'				: device_data['locale']['language']+'_'+device_data['locale']['country'],
		'mat_id'				: app_data['matID'],
		'os_version'			: device_data['os_version'],
		'language' : device_data['locale']['language'],
		'revenue'				: '0.0',
		'screen_density'		: util.getdensity(device_data['dpi']),
		'screen_layout_size'	: device_data['resolution'].split('x')[1]+"x"+device_data['resolution'].split('x')[0],
		'sdk_version'			: campaign_data['mat']['version'],
		'system_date'			: time_Stamp(),

		'click_timestamp':str(int(app_data.get('times').get('click_time'))),
		'download_date':str(int(app_data.get('times').get('download_end_time'))),
		'fire_ad_tracking_disabled':0,
		'is_coppa':0,
		'platform_ad_tracking_disabled':0,
		'platform_aid': device_data.get('adid')
		}

	if ref:
		params['referral_source'] 		= campaign_data.get('package_name')
		data_arr['referral_source']=campaign_data.get('package_name')
		
		data_arr['referrer_delay']=random.randint(800,900)

	if includelatlon:
		data_arr['latitude']=str(app_data.get('latitude'))+str(random.randint(10000,99999))
		data_arr['longitude']=str(app_data.get('longitude'))+str(random.randint(10000,99999))
		data_arr['altitude']=str(app_data.get('altitude'))+'.'+str(random.randint(0,9))


	if app_data.get('referrer'):
		data_arr['install_referrer']	= urllib.unquote(app_data['referrer'])

	if action == 'conversion':
		params['site_event_name'] 	 	= event

		if event=='search' or event=='ViewProduct':
			data_arr['date1']=app_data.get('date1')
			data_arr['date2']=app_data.get('date2')

		data_arr['currency_code']='USD'

		if event=='search':
			try:
				source=locations_list[app_data.get('source')]
				destination=locations_list[app_data.get('destination')]

			except:
				print 'Exception'
				source='LNE'
				destination='LHR'

			data={"data":[{"quantity":"1","item":source+"-"+destination}]}

		if event=='ViewProduct':
			try:
				source=locations_list[app_data.get('source')]
				destination=locations_list[app_data.get('destination')]

				if app_data.get('exception_data')==True:
					source='LNE'
					destination='LHR'


			except:
				print 'Exception'
				source='LNE'
				destination='LHR'
				app_data['unit_price']=6420.56

			data={"data":[{"unit_price":app_data.get('unit_price'),"quantity":"1","item":source+"-"+destination}]}

		if event=='ViewBasket':

			try:
				source=locations_list[app_data.get('source')]
				destination=locations_list[app_data.get('destination')]

				if app_data.get('exception_data')==True:
					source='LNE'
					destination='LHR'

			except:
				print 'Exception'
				source='LNE'
				destination='LHR'
				app_data['unit_price']=6420.56

			data={"data":[{"unit_price":app_data.get('unit_price'),"item":source+"-"+destination}]}

	
	if app_data.get('open_log_id') and app_data.get('last_open_log_id'):
		data_arr['open_log_id']		= app_data['open_log_id']
		data_arr['last_open_log_id']= app_data['last_open_log_id']

	da_str 	= urllib.urlencode(data_arr)
	key 	= campaign_data['mat']['key']
	iv 		= campaign_data['mat']['iv']
	aes 	= AESCipher(key)

	params['data'] = aes.encrypt(iv, da_str)
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


def alitude_cal(app_data,campaign_data,device_data):
	url='https://www.advancedconverter.com/ajax/getElev.php'
	method='get'

	headers={
			'Accept': 'application/json, text/javascript, */*; q=0.01',
			'Accept-Encoding': 'gzip, deflate, br',								
			'Accept-Language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+','+device_data.get('locale').get('language')+';q=0.9',
			'User-Agent':device_data.get('User-Agent'),
			'X-Requested-With': 'XMLHttpRequest',
			'Cookie':str(app_data.get('referrer')),
			'Referer': 'https://www.advancedconverter.com/map-tools/find-altitude-by-coordinates'

	}

	params={
		'lat':app_data.get('latitude'),
		'lng':app_data.get('longitude'),
	}

	data=None

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def current_location(device_data):
	url='http://lumtest.com/myip.json'
			
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = str(int(time.time()*1000))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################
def get_locale(device_data):
	return device_data['locale']['language']+"_"+device_data['locale']['country']


###########################################################
# Utility methods : MAT
#
# Define/declare/Set/Unset/Increment various parameters
# required by Adjust
###########################################################
def def_appInstallData(app_data):
	if not app_data.get('appInstallDate'):
		app_data['appInstallDate'] = time_Stamp()

def def_matID(app_data):
	if not app_data.get('matID'):
		app_data['matID'] = str(uuid.uuid4())

def set_OpenLogID(app_data):
	if not app_data.get('open_log_id'):
		if app_data.get('log_id'):
			app_data['open_log_id'] = app_data['log_id']

def set_LastOpenLogID(app_data):
	if app_data.get('log_id'):
		app_data['last_open_log_id'] = app_data['log_id']

def catch(response,app_data,paramName):
	try:
		if paramName=="mat":
			app_data['log_id'] = json.loads(response.get('data')).get('log_id')
	except Exception as inst:
		print inst		


###########################################################
# Utility methods : MISC
#
###########################################################
def time_Stamp():
	return int(time.time())

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1


########################################################
#					   AES							   #
########################################################
class AESCipher:
	def __init__(self, key):
		self.key = key
	def pad(self, raw):
		l = len(raw) % 16
		l = 16 - l
		for x in range(l):
			raw += ' '
		return raw

	def encrypt(self, iv, raw):
		raw = self.pad(raw)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return (cipher.encrypt(raw)).encode('hex')

	def decrypt(self, enc , iv):
		enc = enc.decode('hex')
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return cipher.decrypt(enc)