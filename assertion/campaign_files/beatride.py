# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util,purchase
import urllib
import random
import time
import hashlib
import json
import base64
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
				'package_name' : 'gr.androiddev.taxibeat',
				'sdk' : 'Android',
				'app_version_name' :'10.53',#'10.52',#'10.51.1',#10.50, 10.49, '10.48',#'10.47',#'10.46', 
				'app_name' : 'Beat',
				'supported_countries': 'WW',
				'device_targeting':True,
				'no_referrer': False,
				'CREATE_DEVICE_MODE' : 3,
				'tracker':'singular',
				'ctr':6,
				'supported_os':'5.0',
				'app_size' : 50.0,#46.0,
				'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
				'singular':{
					'version':'Singular/v9.0.5.PROD',
					'key':'taxibeat_3e4971d2',
					'secret':'525b7d53d078045f827c1956b8991687',
				},
				'api_branch':{
							    'branchkey'  :'key_live_ffhZZbsNFgfuaVCXRZCwvionuxk5zbEo',
							    'sdk'        :'android3.1.0' 
							},	
				'retention':{
							1:50,
							2:48,
							3:46,
							4:44,
							5:40,
							6:38,
							7:35,
							8:33,
							9:32,
							10:31,
							11:30,
							12:29,
							13:28,
							14:27,
							15:26,
							16:25,
							17:24,
							18:23,
							19:22,
							20:21,
							21:20,
							22:19,
							23:18,
							24:17,
							25:16,
							26:15,
							27:14,
							28:13,
							29:12,
							30:11,
							31:10,
							32:9,
							33:8,
							34:6,
							35:5,
				}
}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android")

	#####################################

	global start_s
	start_s = str(int(app_data.get('times').get('install_complete_time')*1000))	

	app_data['seq'] = 1

	if not app_data.get('first_ride'):
		app_data['first_ride']=False

	if not app_data.get('verification'):
		app_data['verification']=False

	if not app_data.get('user_id'):
		app_data['user_id']=util.get_random_string('decimal',40)

 	###########	 	CALLS		 ########
 	print "Branch______install"
 	request=api_branch_install(campaign_data,app_data,device_data)
	app_data['api_hit_time']=time.time()
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'

	time.sleep(random.randint(1,2))
 	print '\n APSALAR Resolve \n'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_Resolve)

	time.sleep(random.randint(1,2))
	print '\n APSALAR START \n'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data)
	util.execute_request(**apsalar_start)

	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(1,2))
	if app_data.get('referrer'):
		print '\n APSALAR EVENT - __InstallReferrer \n'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__InstallReferrer',value={"referrer":urllib.unquote(app_data.get('referrer')),"referrer_source":"service","clickTimestampSeconds":int(app_data.get('times').get('click_time')),"installBeginTimestampSeconds":int(app_data.get('times').get('download_begin_time')),"current_device_time":int(time.time()*1000),"is_revenue_event":False})
		util.execute_request(**apsalar_event)

	time.sleep(random.randint(1,2))
	print '\n APSALAR EVENT - __LicensingStatus \n'	
	apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'__LicensingStatus',value={"responseCode":"0","signedData":"0|970153727|gr.androiddev.taxibeat|254|ANlOHQNvPoAmIeQGtmeCVwWEGj/XAFudrg==|"+str(int(time.time()*1000))+":VT=9223372036854775807","signature":"XS+B+Gesn5qTWwj+YcoSpaBCe65BGpzOp1IuZ35ySp8zndVALYyuAK33kHo+XcbOWKY12PGb9Ag2DWyT4vKDNMTjiLF9YSTbiGaFj5pkuODsxW1cU8q0yFNPdSFUzNCmODG1tvC1rrBsdTFnl0U5G86F2qwnBBGPazPTgjIPmOuYgX8aodOLdaocOHA+KbRmsy5Pd2JLuc0wy57EvE2inP/jEB3SVwyAQDb3zUECbnCNnbtpUpg5gnAXIQzuNXdMJhuBulMe5fH9NX8zStAbZhtNQcjKiqnGMYEs8On/hbBRueHczEXWxuaBCAoKoO8ifyails14X5RRuiCohx75Tg==","is_revenue_event":False})
	util.execute_request(**apsalar_event)

	time.sleep(random.randint(5,8))
	print "App-measurement______config"
	request=app_measurement_config(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(450,550))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data,type='install')
	util.execute_request(**request)

	time.sleep(random.randint(25,30))
	print "Branch______open"
	request=api_branch_open(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(1,2))
	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data, call=2)
	util.execute_request(**request)

	if random.randint(1,100)<=90:

		time.sleep(random.randint(20,30))
		print '\n APSALAR EVENT - Pin_Verified \n'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Pin_Verified',value={"is_revenue_event":False})
		util.execute_request(**apsalar_event)


		time.sleep(random.randint(10,15))
		print '\n APSALAR EVENT - Added_Personal_Info \n'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Added_Personal_Info',value={"is_revenue_event":False})
		util.execute_request(**apsalar_event)
		app_data['verification']=True


		if random.randint(1,100)<=97: #87
			time.sleep(random.randint(1,5))
			print '\n APSALAR EVENT - Location_view \n'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Location_view',value={"is_revenue_event":False})
			util.execute_request(**apsalar_event)



			if random.randint(1,100)<=92: #80
				
				time.sleep(random.randint(10,15))
				print '\n APSALAR EVENT - Dropoff_view \n'	
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Dropoff_view',value={"is_revenue_event":False})
				util.execute_request(**apsalar_event)


				if random.randint(1,100)<=44: #35

					time.sleep(random.randint(8,10))
					print '\n APSALAR EVENT - Edit_Pickup \n'	
					apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Edit_Pickup',value={"is_revenue_event":False})
					util.execute_request(**apsalar_event)

					time.sleep(random.randint(10,12))
					print '\n APSALAR EVENT - Change_search_to_dropoff \n'	
					apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Change_search_to_dropoff',value={"is_revenue_event":False})
					util.execute_request(**apsalar_event)


					if random.randint(1,100)<=88: #30

						time.sleep(random.randint(2,5))
						print '\n APSALAR EVENT - Dropoff_search \n'	
						apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Dropoff_search',value={"is_revenue_event":False})
						util.execute_request(**apsalar_event)
						


				if random.randint(1,100)<=94: #75

					time.sleep(random.randint(15,20))
					print '\n APSALAR EVENT - Search_Ride \n'	
					apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Search_Ride',value={"is_revenue_event":False})
					util.execute_request(**apsalar_event)

					time.sleep(random.randint(5,20))
					print '\n APSALAR EVENT - Find_taxi \n'	
					apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Find_taxi',value={"is_revenue_event":False})
					util.execute_request(**apsalar_event)


					if random.randint(1,100)<=54: #40

						time.sleep(random.randint(6,8))
						print '\n APSALAR EVENT - Options_screen \n'	
						apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Options_screen',value={"is_revenue_event":False})
						util.execute_request(**apsalar_event)
						

					if random.randint(1,100)<=96:

						time.sleep(random.randint(25,30))
						print '\n APSALAR EVENT - 1st_ride \n'	
						apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'1st_ride',value={"is_revenue_event":False})
						util.execute_request(**apsalar_event)
						app_data['first_ride']=True

			if random.randint(1,100)<=70:

				time.sleep(random.randint(10,12))
				print '\n APSALAR EVENT - Payments_screen \n'	
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Payments_screen',value={"is_revenue_event":False})
				util.execute_request(**apsalar_event)

				if random.randint(1,100)<=92:
				
					time.sleep(random.randint(5,8))
					print '\n APSALAR EVENT - Payment_addcard_screen \n'	
					apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Payment_addcard_screen',value={"is_revenue_event":False})
					util.execute_request(**apsalar_event)

					if random.randint(1,100)<=91:

						time.sleep(random.randint(15,20))
						print '\n APSALAR EVENT - Add_Payment \n'	
						apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Add_Payment',value={"is_revenue_event":False})
						util.execute_request(**apsalar_event)

						app_data['card_added']=True

	return {'status':'true'}


#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os="android")

	#####################################

	global start_s
	start_s = str(int(time.time()*1000))

	if not app_data.get('seq'):
		app_data['seq'] = 1

	if not app_data.get('first_ride'):
		app_data['first_ride']=False

	if not app_data.get('verification'):
		app_data['verification']=False

	if not app_data.get('card_added'):
		app_data['card_added']=False

	if not app_data.get('user_id'):
		app_data['user_id']=util.get_random_string('decimal',40)

 	###########	 	CALLS		 ########

 	if not app_data.get('device_fingerprint_id1') or not app_data.get('identity_id') or not app_data.get('session_id'):
	 	print "Branch______install"
	 	request=api_branch_install(campaign_data,app_data,device_data)
		result=util.execute_request(**request)
		try:
			json_result=json.loads(result.get('data'))
			app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
			app_data['identity_id']=json_result.get('identity_id')
			app_data['session_id']=json_result.get('session_id')
		except:
			app_data['device_fingerprint_id1']='613284344550945604'
			app_data['identity_id']='641581897171978611'
			app_data['session_id']='641581897189090110'

	else:
		print "Branch______open"
		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)


	time.sleep(random.randint(1,2))
 	print '\n APSALAR Resolve \n'	
	apsalar_Resolve = apsalarResolve(campaign_data, app_data, device_data, isOpen=True)
	util.execute_request(**apsalar_Resolve)

	time.sleep(random.randint(1,2))
	print '\n APSALAR START \n'	
	apsalar_start = apsalarStart(campaign_data, app_data, device_data, isOpen=True)
	util.execute_request(**apsalar_start)

	print "Branch______sdk"
	request=branch_call(campaign_data, app_data, device_data)
	util.execute_request(**request)	

	if app_data.get('verification')==False:

		time.sleep(random.randint(20,30))
		print '\n APSALAR EVENT - Pin_Verified \n'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Pin_Verified',value={"is_revenue_event":False})
		util.execute_request(**apsalar_event)


		time.sleep(random.randint(10,15))
		print '\n APSALAR EVENT - Added_Personal_Info \n'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Added_Personal_Info',value={"is_revenue_event":False})
		util.execute_request(**apsalar_event)
		app_data['verification']=True


	if app_data.get('verification')==True and random.randint(1,100)<=85: 

		time.sleep(random.randint(1,5))
		print '\n APSALAR EVENT - Location_view \n'	
		apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Location_view',value={"is_revenue_event":False})
		util.execute_request(**apsalar_event)

		if random.randint(1,100)<=90: #75
			
			time.sleep(random.randint(10,15))
			print '\n APSALAR EVENT - Dropoff_view \n'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Dropoff_view',value={"is_revenue_event":False})
			util.execute_request(**apsalar_event)


			if random.randint(1,100)<=40: #30

				time.sleep(random.randint(8,10))
				print '\n APSALAR EVENT - Edit_Pickup \n'	
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Edit_Pickup',value={"is_revenue_event":False})
				util.execute_request(**apsalar_event)

				time.sleep(random.randint(10,12))
				print '\n APSALAR EVENT - Change_search_to_dropoff \n'	
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Change_search_to_dropoff',value={"is_revenue_event":False})
				util.execute_request(**apsalar_event)


				if random.randint(1,100)<=85:

					time.sleep(random.randint(2,5))
					print '\n APSALAR EVENT - Dropoff_search \n'	
					apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Dropoff_search',value={"is_revenue_event":False})
					util.execute_request(**apsalar_event)
					


			if random.randint(1,100)<=94:

				time.sleep(random.randint(15,20))
				print '\n APSALAR EVENT - Search_Ride \n'	
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Search_Ride',value={"is_revenue_event":False})
				util.execute_request(**apsalar_event)

				time.sleep(random.randint(5,20))
				print '\n APSALAR EVENT - Find_taxi \n'	
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Find_taxi',value={"is_revenue_event":False})
				util.execute_request(**apsalar_event)


				if random.randint(1,100)<=50:

					time.sleep(random.randint(6,8))
					print '\n APSALAR EVENT - Options_screen \n'	
					apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Options_screen',value={"is_revenue_event":False})
					util.execute_request(**apsalar_event)
					

				if app_data.get('first_ride')==False and random.randint(1,100)<=90:

					time.sleep(random.randint(25,30))
					print '\n APSALAR EVENT - 1st_ride \n'	
					apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'1st_ride',value={"is_revenue_event":False})
					util.execute_request(**apsalar_event)
					app_data['first_ride']=True

		if random.randint(1,100)<=65:

			time.sleep(random.randint(10,12))
			print '\n APSALAR EVENT - Payments_screen \n'	
			apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Payments_screen',value={"is_revenue_event":False})
			util.execute_request(**apsalar_event)

			if random.randint(1,100)<=91:
			
				time.sleep(random.randint(5,8))
				print '\n APSALAR EVENT - Payment_addcard_screen \n'	
				apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Payment_addcard_screen',value={"is_revenue_event":False})
				util.execute_request(**apsalar_event)

				if random.randint(1,100)<=90 and app_data.get('card_added')==False:

					time.sleep(random.randint(15,20))
					print '\n APSALAR EVENT - Add_Payment \n'	
					apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Add_Payment',value={"is_revenue_event":False})
					util.execute_request(**apsalar_event)
					app_data['card_added']=True


				if app_data.get('card_added')==True:
					if random.randint(1,100)<=10:
						time.sleep(random.randint(8,10))
						print '\n APSALAR EVENT - Promo_code \n'	
						apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Promo_code',value={"is_revenue_event":False})
						util.execute_request(**apsalar_event)

					if  purchase.isPurchase(app_data,day,advertiser_demand=35):
						rr=random.randint(1,100)
						if rr<=20:
							revenue=random.randint(2,3)
							
						elif rr>20 and rr<=80:
							revenue=random.randint(3,8)
							
						else:
							revenue=random.randint(8,12)

						time.sleep(random.randint(10,12))
						print '\n APSALAR EVENT - Took_Ride \n'	
						apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Took_Ride',value={"is_revenue_event":False})
						util.execute_request(**apsalar_event)


						if random.randint(1,100)<=50:
							time.sleep(random.randint(10,12))
							print '\n APSALAR EVENT - Change_search_to_pickup \n'	
							apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Change_search_to_pickup',value={"is_revenue_event":False})
							util.execute_request(**apsalar_event)


						time.sleep(random.randint(300,900))

						print '\n APSALAR EVENT - Completed_1st_ride \n'	
						apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Completed_1st_ride',value={"pcc":"EUR","r":revenue,"is_revenue_event":True})
						util.execute_request(**apsalar_event)


					if app_data.get('flag_isPurchaseDone')==True:

						if random.randint(1,100)<=50:
							time.sleep(random.randint(60,120))
							print '\n APSALAR EVENT - Docs_received \n'	
							apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Docs_received',value={"is_revenue_event":False})
							util.execute_request(**apsalar_event)

						if random.randint(1,100)<=50:
							time.sleep(random.randint(8,10))
							print '\n APSALAR EVENT - Unblocked \n'	
							apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Unblocked',value={"is_revenue_event":False})
							util.execute_request(**apsalar_event)

						if random.randint(1,100)<=50:
							time.sleep(random.randint(300,900))
							print '\n APSALAR EVENT - Completed_Ride \n'	
							apsalar_event = apsalarEvent(campaign_data, app_data, device_data,'Completed_Ride',value={"is_revenue_event":False})
							util.execute_request(**apsalar_event)



	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data,type='open')
	util.execute_request(**request)

	return {'status':'true'}


#########################################
#										#
# 			AppSalar funcation 			#
#										#
#########################################

def apsalarResolve(campaign_data, app_data, device_data, isOpen=False):

	url = 'http://sdk-api-v1.singular.net/api/v1/resolve' 
	method = 'get'
	headers = {
		'User-Agent': 'Singular/SDK-v9.0.5.PROD',
		'Accept-Encoding': 'gzip'
	}

	data_dict = {
		"a":campaign_data.get('singular').get('key'),
		"c":"wifi",
		"i":campaign_data.get('package_name'),
		"k":"AIFA",
		"lag":str(generateLag()),
		"p":"Android",
		"pi":"1",
		"rt":"json",
		"u":device_data.get('adid'),
		"v":device_data.get('os_version'),
	}

	if isOpen==True:
		data_dict['custom_user_id'] = app_data.get('user_id')

	app_data['apsalar_time']=time.time()

	params = get_params(data_dict)
	h = getHash('?'+params,campaign_data.get('singular').get('secret'))
	params = params+'&h='+h

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


def apsalarStart(campaign_data, app_data, device_data, isOpen=False):

	url = "http://sdk-api-v1.singular.net/api/v1/start"
	method = 'get'
	headers = {
		'User-Agent': 'Singular/SDK-v9.0.5.PROD', 
		'Accept-Encoding': 'gzip'
	}

	data_dict = {
				"a":campaign_data.get('singular').get('key'),
				"ab":device_data.get('cpu_abi',"armeabi"),
				"aifa":device_data.get('adid'),
				"av":campaign_data.get('app_version_name'),
				"br":device_data.get('brand'),
				"c":"wifi",
				"current_device_time":str(int(time.time()*1000)),
				"ddl_enabled":"false",
				"de":device_data.get('device'),
				"device_type":"phone",
				"device_user_agent":get_ua(device_data),
				"dnt":"0",
				"i":campaign_data.get('package_name'),
				"install_time":str(int(app_data.get('times').get('download_end_time')*1000)),
				"is":"true",
				"k":"AIFA",
				"lag":str(generateLag()),
				"lc":device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
				"ma":device_data.get('manufacturer'),
				"mo":device_data.get('model'),
				"n":campaign_data.get('app_name'),
				"p":"Android",
				"pr":device_data.get('product'),
				"rt":"json",
				"s": start_s,
				"sdk":campaign_data.get('singular').get('version'),
				"src":"com.android.vending",
				"u":device_data.get('adid'),
				"update_time":str(int(app_data.get('times').get('download_end_time')*1000)),
				"v":device_data.get('os_version'),
	}

	if isOpen == True:
		data_dict['custom_user_id'] = app_data.get('user_id')
		data_dict['is'] = "false"

	params = get_params(data_dict)
	h = getHash('?'+params,campaign_data.get('singular').get('secret'))
	params = params+'&h='+h

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }


def apsalarEvent(campaign_data, app_data, device_data,event_name,value=''):
	
	url = "http://sdk-api-v1.singular.net/api/v1/event"

	method = 'get'
	headers = {
		'User-Agent':'Singular/SDK-v9.0.5.PROD',
		'Accept-Encoding': 'gzip'
	}

	seq = str(app_data.get('seq'))	

	t = str(time.time()-app_data.get('apsalar_time'))[:-8]

	data_dict = {
	"a":campaign_data.get('singular').get('key'),
	"aifa":device_data.get('adid'),
	"av":campaign_data.get('app_version_name'),
	"c":"wifi",
	"custom_user_id" : app_data.get('user_id'),
	"i":campaign_data.get('package_name'),
	"k":"AIFA",
	"lag":str(generateLag()),
	"n":str(event_name),
	"p":"Android",
	"rt":"json",
	"s": start_s,
	"sdk":campaign_data.get('singular').get('version'),
	"seq":str(app_data.get('seq')),
	"t":str(t),
	"u":device_data.get('adid'),
	}

	if value:
		data_dict['e'] = json.dumps(value).replace(" ","").strip()

	params = get_params(data_dict)	
	h = getHash('?'+params,campaign_data.get('singular').get('secret'))
	params = params+'&h='+h

	if app_data.get('seq'):
		app_data['seq']=app_data.get('seq')+1

	return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': None }

	
################ BRANCH CALLS ###################

def branch_call(campaign_data, app_data, device_data, call=1):
	url='http://cdn.branch.io/sdk/uriskiplist_v1.json'
	method='get'
	headers={
		'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
	}

	if call==2:
		url='http://cdn.branch.io/sdk/uriskiplist_v2.json'

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': None}

def api_branch_install(campaign_data,app_data,device_data):
	method='post'
	url='http://api2.branch.io/v1/install'

	headers = {
				
				
				'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
				'Content-Type':'application/json',
				'Accept':'application/json',
			}

	data={"app_version":campaign_data.get('app_version_name'),
		"facebook_app_link_checked":False,
		"is_referrable":1,
		"update":0,
		"debug":False,
		"metadata":{},
		"hardware_id":device_data.get('android_id'),
		'clicked_referrer_ts':int(app_data.get('times').get('click_time')),
		'install_begin_ts':int(app_data.get('times').get('download_begin_time')),

		"is_hardware_id_real":True,
		"brand":device_data.get('brand'),
		"model":device_data.get('model'),
		"screen_dpi":int(device_data.get('dpi')),
		"screen_height":int(device_data.get('resolution').split('x')[0]),
		"screen_width":int(device_data.get('resolution').split('x')[1]),
		"wifi":True,
		"os":"Android",
		"os_version":int(device_data.get('sdk')),
		"country":device_data.get('locale').get('country'),
		"language":device_data.get('locale').get('language'),
		"local_ip":device_data.get('private_ip'),
		"cd":{"mv":"-1","pn":campaign_data.get('package_name')},
		"google_advertising_id":device_data.get('adid'),
		"lat_val":0,
		"instrumentation":{"v1/install-qwt":"0"},
		"sdk":campaign_data.get('api_branch').get('sdk'),
		"retryNumber":0,
		"branch_key":campaign_data.get('api_branch').get('branchkey'),
		'environment':'FULL_APP',
		'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
		'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'ui_mode':'UI_MODE_TYPE_NORMAL',
		'previous_update_time':0
		} 


	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}

def api_branch_open(campaign_data, app_data, device_data):	
	
	url='http://api2.branch.io/v1/open'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"app_version": campaign_data.get('app_version_name'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand'),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			"country":device_data.get('locale').get('country'),
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/open-qwt":"0"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
			"cd":{'mv':'-1','pn':campaign_data.get('package_name')},
			"debug" :False,
			"environment" :'FULL_APP',
			"facebook_app_link_checked":False,
			"google_advertising_id":device_data.get('adid'),
			"is_referrable":1,
			"lat_val":0,
			"update":1,
			'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
			'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
			'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
			'previous_update_time':int(app_data.get('times').get('install_complete_time')*1000)
	}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def api_branch_close(campaign_data, app_data, device_data,type=''):	
	
	url='http://api2.branch.io/v1/close'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"session_id":app_data.get('session_id'),
			"app_version": campaign_data.get('app_version_name'),
			'google_advertising_id': device_data.get('adid'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand'),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			"country":device_data.get('locale').get('country'),
			"lat_val":0,
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/close-qwt":"1","v1/install-brtt":"1359"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
	}
	if type=='open':
		data['instrumentation']={"v1/close-brtt":"938","v1/open-qwt":"0"}
	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def app_measurement_config(campaign_data, app_data, device_data):
	url='http://app-measurement.com/config/app/1%3A878146912845%3Aandroid%3A773ba4b285931285'
	method='head'
	header={
	    'Accept-Encoding'  :'gzip',
	    'User-Agent'  :'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
	}
	params={
	    'app_instance_id':'1ea474e1662100add45734c94eb9d930',
	    'gmp_version'    :'17785',
	    'platform'       :'android'
	}
	data=None
	return {'url': url, 'httpmethod': 'get', 'headers': header, 'params':params, 'data': data}

#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"



######################################
#	Apsalar functions
######################################

def getHash(url,secret):
	h = hashlib.sha1()
	h.update(secret)
	h.update(url)
	return h.hexdigest()

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return base64(sha_obj.digest())

def generateLag():
	return (random.randint(40,300)*0.001)

def get_params(data_value):
	key = data_value.keys()
	a = ''
	for value in sorted(key):
		a += str(urllib.quote_plus(str(value)).encode('utf-8'))+'='+str(urllib.quote_plus(str(data_value.get(value))).encode('utf-8'))+'&'
	a = a.rsplit("&",1)[0]	

	return a