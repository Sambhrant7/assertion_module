# -*- coding: utf8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from sdk import installtimenew
from sdk import getsleep,purchase
from sdk import util
from sdk import NameLists
import time
import uuid
import random
import json
import string
import datetime
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.zzkko',
	'app_size'			 : 26.14,#25.6,
	'app_name' 			 :'SHEIN',
	'app_version_name' 	 : '6.7.9',#'6.7.8',#'6.7.7',#'6.7.4',
	'app_version_code' 	 : '301',#'300',#'298',#'290',
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'gmp_version'        :'12874',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'VDTLA7vDAFTNihAaBiASsP',
		'dkh'		 : 'VDTLA7vD',
		'buildnumber': '4.10.3',#'4.10.2',
		'version'	 : 'v4',
	},
	'api_branch':{
	    'branchkey'  :'key_live_fpOkEEuXwDXym1NmhrDlwnjjAunvw7KT',
	    'sdk'        :'android3.2.0',
	    'sdk_version'        :'3.2.0'  
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	def_firstLaunchDate(app_data,device_data)

	if not app_data.get('DeviceId'):
		app_data['DeviceId'] = "shein_"+str(uuid.uuid4())

	if not app_data.get('member_id'):
		app_data['member_id'] = ""	

	global cart
	cart = list()	

 	###########	 CALLS		 ############

	request= api_service(campaign_data,app_data,device_data,url_part='homepage/app_start_image_info')
	response = util.execute_request(**request)
	try:
		result = response.get('res').headers.get('Token')
		app_data['token'] = result
	except:
		app_data['token'] = "40440_5d7f356511b428.36927104_06450bef7ef682cda015e042e72f48ea2b70125a"	

	time.sleep(random.randint(1,2))
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'	
		
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)
		
	time.sleep(random.randint(1,2))
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)
		
	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)
		
	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	catch(response,app_data,"appsflyer")
		
	if random.randint(1,100)<=60:
		print '\n'+'Appsflyer : Register____________________________________'
		request = appsflyer_register(campaign_data,app_data,device_data)
		util.execute_request(**request)

		print "\nself_call_api-service_shein\n"
		request=api_service_shein_email_register( campaign_data, device_data, app_data )
		response = util.execute_request(**request)
		try:
			result = json.loads(response.get('data')).get('info').get('member').get('member_id')
			app_data['member_id'] = result
			app_data['token'] = json.loads(response.get('data')).get('info').get('member').get('token')
		except:
			app_data['member_id'] = "94687766"
			app_data['token']="143394823762_5d8085bf136ad1.17954119_55be2827e8e93a91521e45b0d78f901d22183bfb"	

		print "\nself_call_api2_branch_i\n"
		request=api_branch_profile( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['register'] = True

	print "\nself_call_api-service_shein\n"
	request=api_service_shein_tab_index( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global category
	category = list()
	try:
		result = json.loads(response.get('data')).get('info').get('tabs')
		for item in result:
			category.append(item.get('id'))
	except:
		category = ['368', '369', '375', '373']

	print "\nself_call_api-service_shein\n"
	request=api_service_shein_home_index( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global product_list
	product_list = list()
	try:
		result = json.loads(response.get('data')).get('info').get('content')[0].get('operations')
		for item in result:
			if item.get('oper_key')=="ITEM_LIST_COMPONENT":
				for i in item.get('content').get('props').get('items'):
					for k in i.get('product_data').get('products'):
						a = {}
						a['cat_id'] = k.get('cat_id')
						a['goods_id'] = k.get('goods_id')
						a['amount'] = k.get('salePrice').get('amount')
						product_list.append(a)
		print "Response ok"				
	except:
		product_list = [{'cat_id': 1738, 'amount': '8.00', 'goods_id': 697759}, {'cat_id': 1727, 'amount': '9.00', 'goods_id': 811383}, {'cat_id': 1773, 'amount': '11.00', 'goods_id': 823684}, {'cat_id': 1738, 'amount': '3.00', 'goods_id': 790895}, {'cat_id': 1732, 'amount': '6.00', 'goods_id': 816764}]


	print "\nself_call_api-service_shein\n"
	request=api_service_shein_get_products_by_keywords( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global cat_id,goods_id
	cat_id = list()
	goods_id = list()
	try:
		result = json.loads(response.get('data')).get('info').get('products')
		for item in result:
			cat_id.append(item.get('cat_id'))
			goods_id.append(item.get('goods_id'))
		print "Response ok"		
	except:
		cat_id = [1738, 1779, 1779, 1738, 1738, 1733, 1738, 1733, 1738, 1779, 1733, 2223, 1738, 1738, 1779, 1779, 1733, 1738, 1779, 1733]
		goods_id = [776067, 792650, 772887, 782798, 776273, 781633, 768686, 788622, 767719, 796358, 774853, 811380, 726387, 780283, 789788, 771565, 753059, 777170, 806830, 720934]	


	for _ in range(random.randint(1,2)):

		global product
		product = random.choice(product_list)

		if random.randint(1,100)<=60:
			time.sleep(random.randint(12,15))
			af_search(campaign_data, app_data, device_data)	

		if random.randint(1,100)<=60:
			time.sleep(random.randint(10,15))
			af_add_to_cart(campaign_data, app_data, device_data)

			print "Branch______event"
			event_data=[{"$content_schema":"COMMERCE_PRODUCT","$quantity":1,"$sku":str(product.get('goods_id')),"$publicly_indexable":True,"$locally_indexable":True,"$creation_timestamp":int(time.time()*1000)}]
			req=api_branch_event(campaign_data, app_data, device_data, event_name='ADD_TO_CART',event_data=event_data)
			util.execute_request(**req)	

			cart.append(product)

		if app_data.get('register') and random.randint(1,100)<=60:
			time.sleep(random.randint(10,15))
			af_add_to_wishlist(campaign_data, app_data, device_data)

			print "Branch______event"
			event_data=[{"$content_schema":"COMMERCE_PRODUCT","$quantity":1,"$sku":str(product.get('goods_id')),"$publicly_indexable":True,"$locally_indexable":True,"$creation_timestamp":int(time.time()*1000)}]
			req=api_branch_event(campaign_data, app_data, device_data, event_name='ADD_TO_WISHLIST',event_data=event_data)
			util.execute_request(**req)

		if random.randint(1,100)<=60:
			time.sleep(random.randint(7,10))
			print "Branch______event"
			req=api_branch_event(campaign_data, app_data, device_data, event_name='VIEW_CART',event_data=None)
			util.execute_request(**req)	

			if cart and app_data.get('register'):
				if random.randint(1,100)<=55:
					time.sleep(random.randint(10,15))
					af_initiated_checkout(campaign_data, app_data, device_data)

					print "Branch______event"
					req=api_branch_event(campaign_data, app_data, device_data, event_name='INITIATE_PURCHASE',event_data=None)
					util.execute_request(**req)	

					if random.randint(1,100)<=80:
						time.sleep(random.randint(1,3))
						print "Branch______event"
						req=api_branch_event(campaign_data, app_data, device_data, event_name='ADD_PAYMENT_INFO',event_data=None)
						util.execute_request(**req)	

						if purchase.isPurchase(app_data,day=1,advertiser_demand=5):

							total = 0
							for i in cart:
								total += eval(i.get('amount'))

							order_id = util.get_random_string(type='char',size=3).upper()+util.get_random_string(type='decimal',size=5)
							shipping = round(random.uniform(1,3),2)
							revenue = eval(str(total))+shipping

							time.sleep(random.randint(10,15))
							af_purchase(campaign_data, app_data, device_data, revenue=revenue, af_order_id=order_id)

							print "Branch______event"
							event_data={"currency":"USD","coupon":"","revenue":revenue}
							req=api_branch_event(campaign_data, app_data, device_data, event_name='PURCHASE',event_data=event_data)
							util.execute_request(**req)

		if random.randint(1,100)<=70:
			time.sleep(random.randint(5,10))
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)

	if random.randint(1,100)<=10:
		time.sleep(random.randint(10,15))
		af_login(campaign_data, app_data, device_data)

		time.sleep(random.randint(10,15))
		print "\nself_call_api2_branch_i\n"
		request=api_branch_logout( campaign_data, device_data, app_data )
		util.execute_request(**request)

	time.sleep(random.randint(30,35))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)	


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)
	def_firstLaunchDate(app_data,device_data)

	if not app_data.get('DeviceId'):
		app_data['DeviceId'] = "shein_"+str(uuid.uuid4())

	if not app_data.get('member_id'):
		app_data['member_id'] = ""	

	global cart
	cart = list()

 	###########	 CALLS		 ############
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data, isOpen=True)
	util.execute_request(**request)

	if not app_data.get('device_fingerprint_id1'):
		print "Branch______install"
		request=api_branch_install(campaign_data,app_data,device_data)
		result=util.execute_request(**request)
		try:
			json_result=json.loads(result.get('data'))
			app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
			app_data['identity_id']=json_result.get('identity_id')
			app_data['session_id']=json_result.get('session_id')
		except:
			app_data['device_fingerprint_id1']='613284344550945604'
			app_data['identity_id']='641581897171978611'
			app_data['session_id']='641581897189090110'	
	else:
		print "Branch______open"
		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)		
		
	if not app_data.get('register') and random.randint(1,100)<=60:
		print '\n'+'Appsflyer : Register____________________________________'
		request = appsflyer_register(campaign_data,app_data,device_data)
		util.execute_request(**request)

		print "\nself_call_api-service_shein\n"
		request=api_service_shein_email_register( campaign_data, device_data, app_data )
		response = util.execute_request(**request)
		try:
			result = json.loads(response.get('data')).get('info').get('member').get('member_id')
			app_data['member_id'] = result
			app_data['token'] = json.loads(response.get('data')).get('info').get('member').get('token')
		except:
			app_data['member_id'] = "94687766"
			app_data['token']="143394823762_5d8085bf136ad1.17954119_55be2827e8e93a91521e45b0d78f901d22183bfb"

		print "\nself_call_api2_branch_i\n"
		request=api_branch_profile( campaign_data, device_data, app_data )
		util.execute_request(**request)

		app_data['register'] = True

	if app_data.get('register'):
		print "\nself_call_api-service_shein\n"
		request=api_service_shein_common_login( campaign_data, device_data, app_data )
		response = util.execute_request(**request)
		try:
			result = json.loads(response.get('data')).get('info').get('member').get('token')
			app_data['token'] = result
		except:
			app_data['token']="143394823762_5d8085bf136ad1.17954119_55be2827e8e93a91521e45b0d78f901d22183bfb"		

	print "\nself_call_api-service_shein\n"
	request=api_service_shein_tab_index( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global category
	category = list()
	try:
		result = json.loads(response.get('data')).get('info').get('tabs')
		for item in result:
			category.append(item.get('id'))
	except:
		category = ['368', '369', '375', '373']

	print "\nself_call_api-service_shein\n"
	request=api_service_shein_home_index( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global product_list
	product_list = list()
	try:
		result = json.loads(response.get('data')).get('info').get('content')[0].get('operations')
		for item in result:
			if item.get('oper_key')=="ITEM_LIST_COMPONENT":
				for i in item.get('content').get('props').get('items'):
					for k in i.get('product_data').get('products'):
						a = {}
						a['cat_id'] = k.get('cat_id')
						a['goods_id'] = k.get('goods_id')
						a['amount'] = k.get('salePrice').get('amount')
						product_list.append(a)
	except:
		product_list = [{'cat_id': 1738, 'amount': '8.00', 'goods_id': 697759}, {'cat_id': 1727, 'amount': '9.00', 'goods_id': 811383}, {'cat_id': 1773, 'amount': '11.00', 'goods_id': 823684}, {'cat_id': 1738, 'amount': '3.00', 'goods_id': 790895}, {'cat_id': 1732, 'amount': '6.00', 'goods_id': 816764}]


	print "\nself_call_api-service_shein\n"
	request=api_service_shein_get_products_by_keywords( campaign_data, device_data, app_data )
	response = util.execute_request(**request)
	global cat_id,goods_id
	cat_id = list()
	goods_id = list()
	try:
		result = json.loads(response.get('data')).get('info').get('products')
		for item in result:
			cat_id.append(item.get('cat_id'))
			goods_id.append(item.get('goods_id'))
	except:
		cat_id = [1738, 1779, 1779, 1738, 1738, 1733, 1738, 1733, 1738, 1779, 1733, 2223, 1738, 1738, 1779, 1779, 1733, 1738, 1779, 1733]
		goods_id = [776067, 792650, 772887, 782798, 776273, 781633, 768686, 788622, 767719, 796358, 774853, 811380, 726387, 780283, 789788, 771565, 753059, 777170, 806830, 720934]	

	if day <=4:		
		number_of_days = random.randint(1,3)
	elif day <=7:
		number_of_days = random.randint(1,2)
	elif day>7:
		number_of_days = random.randint(0,1)		


	for _ in range(number_of_days):

		global product
		product = random.choice(product_list)

		if random.randint(1,100)<=60:
			time.sleep(random.randint(12,15))
			af_search(campaign_data, app_data, device_data)	

		if random.randint(1,100)<=60:
			time.sleep(random.randint(10,15))
			af_add_to_cart(campaign_data, app_data, device_data)

			print "Branch______event"
			event_data=[{"$content_schema":"COMMERCE_PRODUCT","$quantity":1,"$sku":str(product.get('goods_id')),"$publicly_indexable":True,"$locally_indexable":True,"$creation_timestamp":int(time.time()*1000)}]
			req=api_branch_event(campaign_data, app_data, device_data, event_name='ADD_TO_CART',event_data=event_data)
			util.execute_request(**req)	

			cart.append(product)

		if app_data.get('register') and random.randint(1,100)<=60:
			time.sleep(random.randint(10,15))
			af_add_to_wishlist(campaign_data, app_data, device_data)

			print "Branch______event"
			event_data=[{"$content_schema":"COMMERCE_PRODUCT","$quantity":1,"$sku":str(product.get('goods_id')),"$publicly_indexable":True,"$locally_indexable":True,"$creation_timestamp":int(time.time()*1000)}]
			req=api_branch_event(campaign_data, app_data, device_data, event_name='ADD_TO_WISHLIST',event_data=event_data)
			util.execute_request(**req)

		if random.randint(1,100)<=60:
			time.sleep(random.randint(7,10))
			print "Branch______event"
			req=api_branch_event(campaign_data, app_data, device_data, event_name='VIEW_CART',event_data=None)
			util.execute_request(**req)	

			if cart and app_data.get('register'):
				if random.randint(1,100)<=55:
					time.sleep(random.randint(10,15))
					af_initiated_checkout(campaign_data, app_data, device_data)

					print "Branch______event"
					req=api_branch_event(campaign_data, app_data, device_data, event_name='INITIATE_PURCHASE',event_data=None)
					util.execute_request(**req)	

					if random.randint(1,100)<=80:
						time.sleep(random.randint(1,3))
						print "Branch______event"
						req=api_branch_event(campaign_data, app_data, device_data, event_name='ADD_PAYMENT_INFO',event_data=None)
						util.execute_request(**req)	

						if purchase.isPurchase(app_data,day,advertiser_demand=15):

							total = 0
							for i in cart:
								total += eval(i.get('amount'))

							order_id = util.get_random_string(type='char',size=3).upper()+util.get_random_string(type='decimal',size=5)
							shipping = round(random.uniform(1,3),2)
							revenue = eval(str(total))+shipping

							time.sleep(random.randint(10,15))
							af_purchase(campaign_data, app_data, device_data, revenue=revenue, af_order_id=order_id)

							print "Branch______event"
							event_data={"currency":"USD","coupon":"","revenue":revenue}
							req=api_branch_event(campaign_data, app_data, device_data, event_name='PURCHASE',event_data=event_data)
							util.execute_request(**req)

		if random.randint(1,100)<=70:
			time.sleep(random.randint(5,10))
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)

	if random.randint(1,100)<=5:
		time.sleep(random.randint(10,15))
		af_login(campaign_data, app_data, device_data)

		time.sleep(random.randint(10,15))
		print "\nself_call_api2_branch_i\n"
		request=api_branch_logout( campaign_data, device_data, app_data )
		util.execute_request(**request)

	time.sleep(random.randint(30,35))
	print "Branch______close"
	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)	

	return {'status':'true'}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def af_login(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_login'
	eventName			= 'af_login'
	eventValue			= json.dumps({})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)	

def af_add_to_cart(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_add_to_cart'
	eventName			= 'af_add_to_cart'
	eventValue			= json.dumps({"af_content_type":str(product.get('cat_id')),"af_price":eval(product.get('amount')),"af_content_id":str(product.get('goods_id')),"af_quantity":1,"af_currency":"USD"})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_add_to_wishlist(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_add_to_wishlist'
	eventName			= 'af_add_to_wishlist'
	eventValue			= json.dumps({"af_content_type":str(product.get('cat_id')),"af_price":eval(product.get('amount')),"af_content_id":str(product.get('goods_id')),"af_quantity":1,"af_currency":"USD"})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_search(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________af_search'
	eventName			= 'af_search'
	eventValue			= json.dumps({"af_search_string":keywords,"af_content_type":"goods","af_success":goods_id})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def af_initiated_checkout(campaign_data, app_data, device_data):
	af_content_type = list()
	af_price = list()
	af_content_id = list()
	af_quantity = list()
	for i in cart:
		af_content_type.append(str(i.get('cat_id')))
		af_price.append(eval(i.get('amount')))
		af_content_id.append(str(i.get('goods_id')))
		af_quantity.append(1)
	print 'Appsflyer : EVENT___________________________af_initiated_checkout'
	eventName			= 'af_initiated_checkout'
	eventValue			= json.dumps({"af_content_type":af_content_type,"af_price":af_price,"af_content_id":af_content_id,"af_quantity":af_quantity,"af_currency":"USD","af_payment_info_available":""})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def af_purchase(campaign_data, app_data, device_data, revenue=0, af_order_id=''):
	af_content_type = list()
	af_price = list()
	af_content_id = list()
	af_quantity = list()
	for i in cart:
		af_content_type.append(str(i.get('cat_id')))
		af_price.append(eval(i.get('amount')))
		af_content_id.append(str(i.get('goods_id')))
		af_quantity.append(1)
	print 'Appsflyer : EVENT___________________________af_purchase'
	eventName			= 'af_purchase'
	eventValue			= json.dumps({"af_price":af_price,"af_content_type":af_content_type,"af_content_id":af_content_id,"af_quantity":af_quantity,"af_currency":"USD","af_revenue":revenue,"af_order_id":af_order_id})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)		

##################### CALLS ######################

def api_service(campaign_data, app_data, device_data,url_part):
	
	url 	= 'http://api-service.shein.com/'+url_part
	method 	= 'get'
	headers = {
		'Accept-Encoding' : 'gzip',
		'Accept'          :'application/json',
 		'User-Agent'	  :'okhttp/3.10.0',
 		'appcountry'      :device_data.get('locale').get('country'),
 		'app-from'        :campaign_data.get('app_name').lower(),
 		'AppLanguage'     :device_data.get('locale').get('language'),
 		'AppName'         :campaign_data.get('app_name').lower()+' '+'app',
 		'AppType'         :campaign_data.get('app_name').lower(),
 		'AppVersion'      :campaign_data.get('app_version_name'),
 		'ClientId'        :'100',
 		'Device'          :device_data.get('device')+' Android'+device_data.get('os_version'),
 		'DeviceId'        :app_data.get('DeviceId'),
 		'dev-id'          :app_data.get('DeviceId'),
 		'Devtype'         :'Android',
 		'Language'        :device_data.get('locale').get('language'),
 		'network-type'    :device_data.get('network'),
 		'SiteUID'         :'android',
 		'version'         :campaign_data.get('app_version_name'),
 		}
	params 	= None
	data 	= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def measurement(campaign_data, app_data, device_data):
	url 	= 'http://app-measurement.com/config/app/1%3A353013613329%3Aandroid%3Ac993a0054ab36cf7'
	method  = 'get'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent'	  : get_ua(device_data),
		}
	params 	= {
	    'app_instance_id' :'76eddabe41946498f2462ae600a8a279',
		'platform'        :'android',
		'gmp_version'     :campaign_data.get('gmp_version'),
		}
	data 	= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}	

	
def api_service_shein_email_register( campaign_data, device_data, app_data ):
	register_user(app_data)
	url= "http://api-service.shein.com/user/email_register"
	method= "post"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "AppCurrency": "USD",
        "AppLanguage":  device_data.get('locale').get('language'),
        "AppName": "shein app",
        "AppType": "shein",
        "AppVersion": campaign_data.get('app_version_name'),
        "ClientId": "100",
        "Content-Type": "application/x-www-form-urlencoded",
        "Device": device_data.get('model')+" Android"+device_data.get('os_version'),
        "DeviceId": app_data.get('DeviceId'),
        "Devtype": "Android",
        "Language": device_data.get('locale').get('language'),
        "LocalCountry": "other",
        "SiteUID": "android",
        "User-Agent": "okhttp/3.12.1",
        "UserCountry":  device_data.get('locale').get('country'),
        "app-from": "shein",
        "appcountry":  device_data.get('locale').get('country'),
        "currency": "USD",
        "dev-id": app_data.get('DeviceId'),
        "network-type": device_data.get('network'),
        "token": app_data.get('token'),
        "version": campaign_data.get('app_version_name')
        }

	params= None

	data= {       
		"email": app_data.get('user').get('email'),
        "is_agreed_clause": "1",
        "is_subscribed": "1",
        "password": app_data.get('user').get('username'),
        "password_confirm": app_data.get('user').get('username'),
        "registerSource": "1"
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

	
def api_service_shein_tab_index( campaign_data, device_data, app_data ):
	url= "http://api-service.shein.com/ccc/tab_index"
	method= "get"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "AppLanguage": device_data.get('locale').get('language'),
        "AppName": "shein app",
        "AppType": "shein",
        "AppVersion": campaign_data.get('app_version_name'),
        "ClientId": "100",
        "Device": device_data.get('model')+" Android"+device_data.get('os_version'),
        "DeviceId": app_data.get('DeviceId'),
        "Devtype": "Android",
        "Language": device_data.get('locale').get('language'),
        "SiteUID": "android",
        "User-Agent": "okhttp/3.12.1",
        "app-from": "shein",
        "appcountry": device_data.get('locale').get('country'),
        "dev-id": app_data.get('DeviceId'),
        "network-type": device_data.get('network'),
        "version": campaign_data.get('app_version_name')
        }

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

	
def api_service_shein_home_index( campaign_data, device_data, app_data ):
	url= "http://api-service.shein.com/ccc/home_index"
	method= "get"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "AppCurrency": "USD",
        "AppLanguage":  device_data.get('locale').get('language'),
        "AppName": "shein app",
        "AppType": "shein",
        "AppVersion": campaign_data.get('app_version_name'),
        "ClientId": "100",
        "Device": device_data.get('model')+" Android"+device_data.get('os_version'),
        "DeviceId": app_data.get('DeviceId'),
        "Devtype": "Android",
        "Language": "en",
        "LocalCountry": "other",
        "SiteUID": "android",
        "User-Agent": "okhttp/3.12.1",
        "UserCountry":  device_data.get('locale').get('country'),
        "app-from": "shein",
        "appcountry":  device_data.get('locale').get('country'),
        "currency": "USD",
        "dev-id": app_data.get('DeviceId'),
        "network-type":  device_data.get('network'),
        "token": app_data.get('token'),
        "version": campaign_data.get('app_version_name')
        }

	params= {       
				"position": "1", 
				"scene_id": str(random.choice(category))
			}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

	
def api_service_shein_get_products_by_keywords( campaign_data, device_data, app_data ):
	url= "http://api-service.shein.com/product/get_products_by_keywords"
	method= "get"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "AppCurrency": "USD",
        "AppLanguage":  device_data.get('locale').get('language'),
        "AppName": "shein app",
        "AppType": "shein",
        "AppVersion": campaign_data.get('app_version_name'),
        "ClientId": "100",
        "Device": device_data.get('model')+" Android"+device_data.get('os_version'),
        "DeviceId": app_data.get('DeviceId'),
        "Devtype": "Android",
        "Language": "en",
        "LocalCountry": "other",
        "NewUid": str(app_data.get('member_id')),
        "SiteUID": "android",
        "User-Agent": "okhttp/3.12.1",
        "UserCountry":  device_data.get('locale').get('country'),
        "abt-params": "rule_id=route_v2:true|query:as|call:hotrank|sort:route|rough:vectornew|resort:new71",
        "abt-poskey": "SAndSearch",
        "app-from": "shein",
        "appcountry": device_data.get('locale').get('country'),
        "currency": "USD",
        "dev-id": app_data.get('DeviceId'),
        "device-brand": device_data.get('brand'),
        "device-size": device_data.get('os_version'),
        "network-type": device_data.get('network'),
        "screen-pixel": device_data.get('resolution'),
        "token": app_data.get('token'),
        "version": campaign_data.get('app_version_name')
        }

	global keywords
	keywords = random.choice(["top","bottoms","pants","shirt","suit","joggers","jeans","jacket","shoes"])
	params= {       
		"cat_id": "",
        "filter": "",
        "is_force": "0",
        "keywords": keywords,
        "limit": "20",
        "page": "1",
        "page_name": "page_search",
        "sort": "0"
        }

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

def api_service_shein_common_login( campaign_data, device_data, app_data ):
	register_user(app_data)
	url= "http://api-service.shein.com/user/common_login"
	method= "post"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "AppCurrency": "USD",
        "AppLanguage": device_data.get('locale').get('language'),
        "AppName": "shein app",
        "AppType": "shein",
        "AppVersion":campaign_data.get('app_version_name'),
        "ClientId": "100",
        "Content-Type": "application/x-www-form-urlencoded",
        "Device": device_data.get('model')+" Android"+device_data.get('os_version'),
        "DeviceId": app_data.get('DeviceId'),
        "Devtype": "Android",
        "Language": device_data.get('locale').get('language'),
        "LocalCountry": device_data.get('locale').get('country'),
        "NewUid": app_data.get('member_id'),
        "SiteUID": "android",
        "User-Agent": "okhttp/3.12.1",
        "UserCountry": device_data.get('locale').get('country'),
        "app-from": "shein",
        "appcountry": device_data.get('locale').get('country'),
        "currency": "USD",
        "dev-id": app_data.get('DeviceId'),
        "network-type": device_data.get('network'),
        "token": app_data.get('token'),
        "version": campaign_data.get('app_version_name')
        }

	params= None

	data= {       
		"email": app_data.get('user').get('email'),
        "password": app_data.get('user').get('username'),
        "silent_login": "1"
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}	

###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}

	if not app_data.get('mandroid_id'):
		app_data['mandroid_id']='shein_'+str(uuid.uuid4())

	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"android_id" : app_data.get('mandroid_id'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"channel" : "Google",
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "ac",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"sensors" : [{u'sN': u'mmc3416x-mag', u'sVE': [-15.94065, -10.399259, -150.83809], u'sV': u'MEMSIC, Inc', u'sVS': [22.702608, -48.89605, -41.279686], u'sT': 2}, {u'sN': u'lis3dh-accel', u'sVE': [0.17238252, 0.35434186, 9.653421], u'sV': u'STMicroelectronics', u'sVS': [0.1340753, 0.41180268, 9.548077], u'sT': 1}, {u'sN': u'oem-pseudo-gyro', u'sVE': [-0.10032661, -5.1423507, 0.16162205], u'sV': u'oem', u'sVS': [-1.6363289, 3.0370739, 1.8632056], u'sT': 4}],
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"open_referrer" : 'android-app://com.android.vending',
		"operator" : device_data.get('carrier'),
		"p_receipt" : util.get_random_string('google_token',637).replace('-','+').replace('_','/')+'=',
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"sc_o" : "p",
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"tokenRefreshConfigured" : False,
		"uid" : app_data.get('uid'),

		}			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('counter')==2:
		data['gcd_timing']="1547"

	if app_data.get('counter')>2:
		if data.get('rfr'):
			del data['rfr']
		if data.get('deviceData').get('sensors'):
			del data['deviceData']['sensors']
		if data.get("p_receipt"):
			del data["p_receipt"]

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))	
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
 	def_(app_data,'counter')
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"app_name" : campaign_data.get('app_name'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand').upper(),
		"carrier" : device_data.get('carrier'),
		"channel" : "Google",
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"device_id" : app_data.get('uid'),
		"devkey" : campaign_data.get('appsflyer').get('key'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"android_id" : app_data.get('mandroid_id'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"channel" : "Google",
		"cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : True,
		"sc_o" : "p",
		"sdk" : device_data.get('sdk'),
		"tokenRefreshConfigured" : False,
		"uid" : app_data.get('uid'),

		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

###################################################################
################ BRANCH CALLS ###################

def api_branch_install(campaign_data,app_data,device_data):
	method='post'
	url='http://api.branch.io/v1/install'

	headers = {
				'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
				'Content-Type':'application/json',
				'Accept':'application/json',
			}

	data={"app_version":campaign_data.get('app_version_name'),
		"facebook_app_link_checked":False,
		"is_referrable":1,
		"update":0,
		"debug":False,
		"metadata":{},
		"hardware_id":device_data.get('android_id'),
		'clicked_referrer_ts':int(app_data.get('times').get('click_time')),
		'install_begin_ts':int(app_data.get('times').get('download_begin_time')),
		"is_hardware_id_real":True,
		"brand":device_data.get('brand').upper(),
		"model":device_data.get('model'),
		"screen_dpi":int(device_data.get('dpi')),
		"screen_height":int(device_data.get('resolution').split('x')[0]),
		"screen_width":int(device_data.get('resolution').split('x')[1]),
		"wifi":True,
		"os":"Android",
		"os_version":int(device_data.get('sdk')),
		"country":device_data.get('locale').get('country'),
		"language":device_data.get('locale').get('language'),
		"local_ip":device_data.get('private_ip'),
		"cd":{"mv":"-1","pn":campaign_data.get('package_name')},
		"google_advertising_id":device_data.get('adid'),
		"lat_val":0,
		"instrumentation":{"v1/install-qwt":"0"},
		"sdk":campaign_data.get('api_branch').get('sdk'),
		"retryNumber":0,
		"branch_key":campaign_data.get('api_branch').get('branchkey'),
		'environment':'FULL_APP',
		'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
		'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'ui_mode':'UI_MODE_TYPE_NORMAL',
		'previous_update_time':0
		} 

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}


def api_branch_open(campaign_data, app_data, device_data, call_seq=1):	
	
	url='http://api.branch.io/v1/open'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"app_version": campaign_data.get('app_version_name'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand').upper(),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			"country":device_data.get('locale').get('country'),
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/close-brtt":"1065","v1/open-qwt":"0"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
			"cd":{'mv':'-1','pn':campaign_data.get('package_name')},
			"debug" :False,
			"environment" :'FULL_APP',
			"facebook_app_link_checked":False,
			"google_advertising_id":device_data.get('adid'),
			"is_referrable":1,
			"lat_val":0,
			"update":1,
			'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
			'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
			'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
			'previous_update_time':int(app_data.get('times').get('install_complete_time')*1000)
	}
	if call_seq==2:
		data['instrumentation']={"v1/close-brtt":"891","v1/open-qwt":"0"}
	if call_seq==3:
		data['instrumentation']={"v1/close-brtt":"681","v1/open-qwt":"0"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}


def api_branch_close(campaign_data, app_data, device_data,call_seq=1):	
	
	url='http://api.branch.io/v1/close'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"session_id":app_data.get('session_id'),
			"app_version": campaign_data.get('app_version_name'),
			'google_advertising_id': device_data.get('adid'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand').upper(),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			"country":device_data.get('locale').get('country'),
			"lat_val":0,
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/close-qwt":"2","v1/install-brtt":"648"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
	}
	if call_seq==2:
		data['instrumentation']={"v1/close-qwt":"10","v1/open-brtt":"692"}
	if call_seq==3:
		data['instrumentation']={"v1/close-qwt":"1","v1/open-brtt":"507"}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def api_branch_event(campaign_data, app_data, device_data, event_name='',event_data=''):	
	
	url='http://api2.branch.io/v2/event/standard'
	header={
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
		"name":event_name,
		"user_data":{"android_id":device_data.get('android_id'),
		"brand":device_data.get('brand').upper(),
		"model":device_data.get('model'),
		"screen_dpi":int(device_data.get('dpi')),
		"screen_height":int(device_data.get('resolution').split('x')[0]),
		"screen_width":int(device_data.get('resolution').split('x')[1]),
		"os":"Android",
		"os_version":device_data.get('sdk'),
		"country":device_data.get('locale').get('country'),
		"language":device_data.get('locale').get('language'),
		"local_ip":device_data.get('private_ip'),
		"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
		"app_version":campaign_data.get('app_version_name'),
		"sdk":"android",
		"sdk_version":campaign_data.get('api_branch').get('sdk_version'),
		"user_agent":device_data.get('User-Agent'),
		"environment":"FULL_APP",
		"developer_identity":app_data.get('member_id'),
		"aaid":device_data.get('adid'),
		"limit_ad_tracking":0},
		"metadata":{  },
		"instrumentation":{"v2/event/standard-brtt":"1557","v2/event/standard-qwt":"1571"},
		"branch_key":campaign_data.get('api_branch').get('branchkey'),
		"retryNumber":0
		}

	if event_data:
		data['content_items'] = event_data	

	if event_name=='PURCHASE':
		data['event_data'] = event_data	
		if data.get('content_items'):
			del data['content_items']
	
	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

	
def api_branch_profile( campaign_data, device_data, app_data ):
	url= "http://api2.branch.io/v1/profile"
	method= "post"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
        }

	params= None

	data= {       
		"branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "country": device_data.get('locale').get('country'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity": app_data.get('member_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       
        							"v1\\/install-brtt": "4905",
                                   	"v1\\/profile-qwt": "0"
                           },
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {},
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": device_data.get('sdk'),
        "retryNumber": 0,
        "screen_dpi": device_data.get('dpi'),
        "screen_height": device_data.get('resolution').split('x')[0],
        "screen_width": device_data.get('resolution').split('x')[1],
        "sdk": campaign_data.get('api_branch').get('sdk_version'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}	

	
def api_branch_logout( campaign_data, device_data, app_data ):
	url= "http://api2.branch.io/v1/logout"
	method= "post"
	headers= {       
		"Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
        }

	params= None

	data= {       
		"branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "country": device_data.get('locale').get('country'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       
        							"v1\\/logout-qwt": "3",
                                   	"v2\\/event\\/standard-brtt": "399"
                            },
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": device_data.get('sdk'),
        "retryNumber": 0,
        "screen_dpi": device_data.get('dpi'),
        "screen_height": device_data.get('resolution').split('x')[0],
        "screen_width": device_data.get('resolution').split('x')[1],
        "sdk": campaign_data.get('api_branch').get('sdk_version'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True
        }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}	


###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################
def change_char(s, p, r):
	return s[:p]+r+s[p+1:]
		
def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100

	if n3<10:
		n3 = '0'+str(n3)
		
	sb = insert_char(sb,23,str(n3))
	return sb



###################################################################
# appsflyer_kef_key and value generator
# parameter 			: brand, sdk, lang, af_ts, buildnumber, etc
#
# Generates kef key and value for appsflyer.
###################################################################
global temp, counter
temp = 0
counter = 1

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
	if buildnumber=="4.8.18":
		return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
	else:
		c = '\u0000'
		obj2 = af_ts[::-1]
		out = calculate(sdk, obj2, brand)
		i = len(out)
		if i > 4:
			i2 = globals()['counter']+65
			globals()['temp'] = i2 % 128
			if (1 if i2 % 2 != 0 else None) != 1:
				out.delete(4, i)
			else:
				out.delete(5, i)
		else:
			while i < 4:
				i3 = globals()['counter'] + 119
				globals()['temp'] = i3 % 128
				if (16 if i3 % 2 != 0 else 93) != 16:
					i += 1
					out+='1'
				else:
					i += 66
					out+='H'
				i3 = globals()['counter'] + 109
				globals()['temp'] = i3 % 128
				i3 %= 2
		return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
	ts = ts[::-1]
	appends = [sdk,lang,ts]
	lengths = [len(sdk),len(lang),len(ts)]
	l = sorted(lengths)
	least = l[0]
	out = ""
	for x in range(least):
		numb = None
		for y in range(len(appends)):
			charAt = ord(appends[y][x])
			if numb:
				charAt = int((charAt ^ int(numb)))

			numb = charAt
			
		out+=str(hex(numb))[2:]

	if len(out)>4:
		out = out[:4]
	elif len(out)<4:
		while len(out)<4:
			out+="1"
	
	return out

"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""

def generateValue(b,x,s,p,ts,fl,buildnumber):
	part1=generateValue1get(ts,fl,buildnumber)
	str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, "utf-8")
	for i in range(len(str_)):
		str_[i] = int((str_[i] ^ ((i % 2) + 42)))

	stringBuilder = ""
	for toHexString in str_:
		toHexString2 = str(hex(int(toHexString)))[2:]
		if 1 == len(toHexString2):
			toHexString2 = "0"+str(toHexString2)
		stringBuilder+=toHexString2
	part2 = stringBuilder.__str__()
	return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
	gethash = sha256(ts+firstLaunch+buildnumber)
	return gethash[:16]

"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""

def calculate(sdk, obj, brand):
	allList = [sdk, obj, brand]
	arrayList = []
	i = 0
	while True:
		flag = 1
		if i >= 3:
			break
		i2 = globals()['temp'] + 63
		globals()['counter'] = i2 % 128
		if i2 % 2 != 0:
			flag = None
		if flag != None:
			arrayList.append(len(allList[i]))
			i += 31
		else:
			arrayList.append(len(allList[i]))
			i += 1
	sorted(arrayList)
	intValue = int(arrayList[0])
	out = ""
	i3 = 0
	while i3 < intValue:
		i4 = globals()['counter']+99
		globals()['temp'] = i4 % 128
		i5 =  globals()['temp']+67
		globals()['counter'] = i5 % 128
		i5 %= 2
		number = None
		if i4 % 2 != 0:
			a = 57
		else:
			a = 83
		if a != 83:
			i4 = 1
		else:
			i4 = 0
		while i4 < 3:
			i5 = globals()['temp'] + 87
			globals()['counter'] = i5 % 128
			i5 %= 2
			i5 = ord(allList[i4][i3])
			if (92 if number == None else 39) != 39:
				i6 = globals()['temp'] + 55
				globals()['counter'] = i6 % 128
				i6 %= 2
				i6 = globals()['counter'] + 39
				globals()['temp'] = i6 % 128
				i6 %= 2
			else:
				i5 ^= int(number)
			number = i5
			i4 += 1
		out+=str(hex(int(number)))[2:]
		i3 += 1
	return out



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def ref_time():
	time.sleep(random.randint(10,15))
	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"