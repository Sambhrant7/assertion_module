# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import installtimenew
from sdk import getsleep
from sdk import util
from sdk import NameLists
import time
import random,urllib
import json
import datetime
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.privalia.privalia',
	'app_size'			 : 81.3,#79.7,#81.9,#81.8,
	'app_name' 			 :'Privalia',
	'app_version_name' 	 : '4.9.3',#'4.8.10',#'4.8.9',#'4.8.8',
	'app_version_code' 	 : '201909131317',#'201906271633',#'201906111554',#'201905031015',
	'app_id' 			 : '394874573',
	'ctr' 				 : 6,
	'sdk' 		 		 : 'ios',
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '9.0',
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 	: 'sbq5s4tu2wao',
		'sdk'		 	: 'ios4.14.3',
		'app_updated_at':  '2019-09-13T17:18:58.000Z',#'2019-06-27T20:29:11.000Z',#'2019-06-11T19:56:51.000Z',#'2019-05-03T14:02:55.000Z',
		'secret_key' : '180038966612400991641748893048411038812',
		'secret_id'	 : '1',
		},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),
	# pushtoken(app_data)
	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	################generating_realtime_differences##################
	s1=0
	s2=0
	a1=18
	a2=20
	e1=0
	e2=0
	id1=0
	id2=0
	click_time_iad3=False

	global currency_list,country_lists
	country_lists=['MX','ES','BR','IT']
	currency_list=['MXN','EUR','BRL','EUR']

	app_data['n']=random.randint(0,len(country_lists)-1)

	global criteo
	criteo=[]

	global temp
	temp=[]

	if not app_data.get('register'):
		app_data['register']=False


	if not app_data.get('count'):
		app_data['count']=0

	if not app_data.get('amount'):
		app_data['amount']=0

 ###########	 CALLS		 ############
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,click_time=click_time_iad3,source='iad3',t1=id1,t2=id2)
	util.execute_request(**request)

	print "\nself_call_getBanners\n"
	request=getBanners( campaign_data, device_data, app_data )
	output=util.execute_request(**request)	
	try:
		# app_data['outputs']= output.get('res').headers.get('Set-Cookie')
		app_data['PRIVALIASESSID_mex']=output.get('res').headers.get('Set-Cookie').split('PRIVALIASESSID_mex=')[1].split(';')[0]
		app_data['TS01fa3430']=output.get('res').headers.get('Set-Cookie').split('TS01fa3430=')[1].split(';')[0]
		app_data['TS01b98eb7']=output.get('res').headers.get('Set-Cookie').split('TS01b98eb7=')[1].split(';')[0]
		
		

		if app_data.get('PRIVALIASESSID_mex')=='' or app_data.get('PRIVALIASESSID_mex')==None:
			app_data['PRIVALIASESSID_mex']='scerp9ok5ctt8vpm5q97mmpf26'
			app_data['TS01fa3430']='018bcad25e3738536ea78c30ce4165137a77e819dcabb88a82a4d7ecabc6d90d6efeca43b5'
			app_data['TS01b98eb7']='018bcad25e9f1d9393165e68285e0b2a65ba1acc57e7f10dbd54195e717ed2aa17bf1bcba31465a1db191afe6ba04aeba492b9daa6'



		print app_data['PRIVALIASESSID_mex']
		print app_data['TS01fa3430']
		print app_data['TS01b98eb7']
		
	except:
		app_data['PRIVALIASESSID_mex']='scerp9ok5ctt8vpm5q97mmpf26'
		app_data['TS01fa3430']='018bcad25e3738536ea78c30ce4165137a77e819dcabb88a82a4d7ecabc6d90d6efeca43b5'
		app_data['TS01b98eb7']='018bcad25e9f1d9393165e68285e0b2a65ba1acc57e7f10dbd54195e717ed2aa17bf1bcba31465a1db191afe6ba04aeba492b9daa6'

	if app_data.get('PRIVALIASESSID_mex')=='' or app_data.get('PRIVALIASESSID_mex')==None:
		app_data['PRIVALIASESSID_mex']='scerp9ok5ctt8vpm5q97mmpf26'
		app_data['TS01fa3430']='018bcad25e3738536ea78c30ce4165137a77e819dcabb88a82a4d7ecabc6d90d6efeca43b5'
		app_data['TS01b98eb7']='018bcad25e9f1d9393165e68285e0b2a65ba1acc57e7f10dbd54195e717ed2aa17bf1bcba31465a1db191afe6ba04aeba492b9daa6'
		
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2,initiated_by='backend')
	util.execute_request(**request)

	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,t1=a1,t2=a2,initiated_by='sdk')
	util.execute_request(**request)



	time.sleep(random.randint(45,50))

	print "\nself_call_registerProcess\n"
	request=registerProcess( campaign_data, device_data, app_data )
	output=util.execute_request(**request)	
	try:
		# app_data['outputs']= output.get('res').headers.get('Set-Cookie')
		app_data['PRIVALIASESSID_mex']=output.get('res').headers.get('Set-Cookie').split('PRIVALIASESSID_mex=')[1].split(';')[0]
		app_data['TS01fa3430']=output.get('res').headers.get('Set-Cookie').split('TS01fa3430=')[1].split(';')[0]
		app_data['TS01b98eb7']=output.get('res').headers.get('Set-Cookie').split('TS01b98eb7=')[1].split(';')[0]
		app_data['FC_PRIVALIA_S_mex']=output.get('res').headers.get('Set-Cookie').split('FC_PRIVALIA_S_mex=')[1].split(';')[0]
		app_data['session_timestamp']=output.get('res').headers.get('Set-Cookie').split('session_timestamp=')[1].split(';')[0]
		app_data['watsonAutoLogin']=output.get('res').headers.get('Set-Cookie').split('watsonAutoLogin=')[1].split(';')[0]
		app_data['watsonRegistered']=output.get('res').headers.get('Set-Cookie').split('watsonRegistered=')[1].split(';')[0]
		app_data['TS01170bf2']=output.get('res').headers.get('Set-Cookie').split('TS01170bf2=')[1].split(';')[0]
		
		

		if app_data.get('PRIVALIASESSID_mex')=='' or app_data.get('PRIVALIASESSID_mex')==None:
			app_data['PRIVALIASESSID_mex']='mgdo101jjqdegflshi5hhiugh2'
			app_data['TS01fa3430']='018bcad25ecd74cbca2eed5e6334f5aef3aad8c575d6b2c6eae870fd8f13541570dd7e0ab829a76a5d9e544f4ff9cc2321aecd80c865367d16fd0b92c0d3bde55eaceccfb922e32bf8c51d75a1556d49272c4cd929'
			app_data['TS01b98eb7']='018bcad25e985ab328b4e0fe6fcaf19789be6924bcc4bf0ee134fbbbf6122bb093483cb8b764d43690acaae54e3632fd30f33e67e38268cea6714d5259d24ce51cc0527cb7'
			app_data['FC_PRIVALIA_S_mex']='77939915-0a1a79f40594e7607b3c5313950e7ae0-5ab9b27adf8f58dac6468fa3400f2685'
			app_data['session_timestamp']='1560246551%3B77939915'
			app_data['watsonAutoLogin']='77939915'
			app_data['watsonRegistered']='77939915'
			app_data['TS01170bf2']='018bcad25ed4492c5ebc7f0d0693df5ce4bc2fc746d6b2c6eae870fd8f13541570dd7e0ab814a0f535e15589a83c2f76f579c52c9a526a896906749a57674abfef0e09eb401c47e93d9edab7596b061b482929e6ed0744729a4cbdf6f5b33609d2f7afa796'

		print app_data['PRIVALIASESSID_mex']
		print app_data['TS01fa3430']
		print app_data['TS01b98eb7']
		print app_data['FC_PRIVALIA_S_mex']
		print app_data['session_timestamp']
		print app_data['watsonAutoLogin']
		print app_data['watsonRegistered']
		print app_data['TS01170bf2']
		
		
	except:
		app_data['PRIVALIASESSID_mex']='mgdo101jjqdegflshi5hhiugh2'
		app_data['TS01fa3430']='018bcad25ecd74cbca2eed5e6334f5aef3aad8c575d6b2c6eae870fd8f13541570dd7e0ab829a76a5d9e544f4ff9cc2321aecd80c865367d16fd0b92c0d3bde55eaceccfb922e32bf8c51d75a1556d49272c4cd929'
		app_data['TS01b98eb7']='018bcad25e985ab328b4e0fe6fcaf19789be6924bcc4bf0ee134fbbbf6122bb093483cb8b764d43690acaae54e3632fd30f33e67e38268cea6714d5259d24ce51cc0527cb7'
		app_data['FC_PRIVALIA_S_mex']='77939915-0a1a79f40594e7607b3c5313950e7ae0-5ab9b27adf8f58dac6468fa3400f2685'
		app_data['session_timestamp']='1560246551%3B77939915'
		app_data['watsonAutoLogin']='77939915'
		app_data['watsonRegistered']='77939915'
		app_data['TS01170bf2']='018bcad25ed4492c5ebc7f0d0693df5ce4bc2fc746d6b2c6eae870fd8f13541570dd7e0ab814a0f535e15589a83c2f76f579c52c9a526a896906749a57674abfef0e09eb401c47e93d9edab7596b061b482929e6ed0744729a4cbdf6f5b33609d2f7afa796'

	if app_data.get('PRIVALIASESSID_mex')=='' or app_data.get('PRIVALIASESSID_mex')==None:
		app_data['PRIVALIASESSID_mex']='mgdo101jjqdegflshi5hhiugh2'
		app_data['TS01fa3430']='018bcad25ecd74cbca2eed5e6334f5aef3aad8c575d6b2c6eae870fd8f13541570dd7e0ab829a76a5d9e544f4ff9cc2321aecd80c865367d16fd0b92c0d3bde55eaceccfb922e32bf8c51d75a1556d49272c4cd929'
		app_data['TS01b98eb7']='018bcad25e985ab328b4e0fe6fcaf19789be6924bcc4bf0ee134fbbbf6122bb093483cb8b764d43690acaae54e3632fd30f33e67e38268cea6714d5259d24ce51cc0527cb7'
		app_data['FC_PRIVALIA_S_mex']='77939915-0a1a79f40594e7607b3c5313950e7ae0-5ab9b27adf8f58dac6468fa3400f2685'
		app_data['session_timestamp']='1560246551%3B77939915'
		app_data['watsonAutoLogin']='77939915'
		app_data['watsonRegistered']='77939915'
		app_data['TS01170bf2']='018bcad25ed4492c5ebc7f0d0693df5ce4bc2fc746d6b2c6eae870fd8f13541570dd7e0ab814a0f535e15589a83c2f76f579c52c9a526a896906749a57674abfef0e09eb401c47e93d9edab7596b061b482929e6ed0744729a4cbdf6f5b33609d2f7afa796'

	

	if random.randint(1,100)<=90:
		print "\nself_call_login\n"
		request=login( campaign_data, device_data, app_data )
		util.execute_request(**request)

		adjust_token_soci2a(campaign_data, app_data, device_data,e1,e2)

		print "\nself_call_member\n"
		request=member( campaign_data, device_data, app_data )
		output=util.execute_request(**request)	
		try:
			# app_data['outputs']= output.get('res').headers.get('Set-Cookie')
			app_data['ADRUM_BT']=output.get('res').headers.get('Set-Cookie').split('ADRUM_BT=')[1].split(';')[0]
			
			

			if app_data.get('ADRUM_BT')=='' or app_data.get('ADRUM_BT')==None:
				app_data['ADRUM_BT']='R%3A0%7Cg%3A2306c1e2-6394-4873-8a38-e2b37881fe7962190%7Cn%3Aprivalia_ba4d61d5-dcd0-4326-835c-7531a86cf86d%7Cd%3A89%7Ce%3A181'
			print app_data['ADRUM_BT']
			
			
		except:
			app_data['ADRUM_BT']='R%3A0%7Cg%3A2306c1e2-6394-4873-8a38-e2b37881fe7962190%7Cn%3Aprivalia_ba4d61d5-dcd0-4326-835c-7531a86cf86d%7Cd%3A89%7Ce%3A181'

		if app_data.get('ADRUM_BT')=='' or app_data.get('ADRUM_BT')==None:
				app_data['ADRUM_BT']='R%3A0%7Cg%3A2306c1e2-6394-4873-8a38-e2b37881fe7962190%7Cn%3Aprivalia_ba4d61d5-dcd0-4326-835c-7531a86cf86d%7Cd%3A89%7Ce%3A181'
		
		adjust_token_vfxzkk(campaign_data, app_data, device_data,e1,e2)

		app_data['register']=True

		
		for i in range(random.randint(1,3)):

			print "\nself_call_menu\n"
			request=menu( campaign_data, device_data, app_data )
			output=util.execute_request(**request)
			global category_id_list
			try:
				category_id_list=[]
				
				menu_item=json.loads(output.get('data')).get('data').get('menu')
				for i in range(len(menu_item)):
					children=menu_item[i].get('children')
					for j in range(len(children)):
						sub_child=children[j].get('children')
						for k in range(len(sub_child)):
							category_id_list.append(sub_child[k].get('id'))

				if category_id_list==None or len(category_id_list)<1:
					category_id_list=[234626, 237242, 240155, 231521, 231362, 237809, 240158, 239300, 237647, 234914, 237701, 238619, 239981, 235757, 236990, 239798, 239948, 237458, 236765, 238778, 237740, 231689, 237236, 236864, 237143, 237674, 237581, 235973, 239537, 235970, 232034, 244613, 239117, 238892, 238685, 238613, 237842, 237650, 237608, 237587, 237194, 237071, 236882, 236720, 236198, 235574, 239216, 237893, 237818, 237749, 237677, 237440, 235265, 239894, 239792, 239498, 238883, 237452, 235997, 234980, 233408, 242951, 239315, 237851, 237449, 237398, 237854, 237437, 235259, 237662, 237638, 238166, 236141, 234431, 238124, 242216, 237695, 237149, 237683, 237710, 237743, 238013, 241091, 243419, 239132, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 234626, 237242, 231362, 231521, 234914, 239300, 238619, 239981, 235757, 236990, 236765, 237809, 237236, 244613, 238892, 238685, 238613, 237842, 237740, 237194, 237071, 236720, 236198, 235970, 235574, 232034, 240158, 239537, 237818, 237749, 235265, 239894, 238883, 235997, 234980, 233408, 242951, 239315, 237851, 237398, 237854, 237143, 235973, 235259, 231689, 238166, 236141, 239132, 243419, 241091, 238013, 20, 50, 100, 140, 190, 250, 320, 400, 460, 540, 580, 590, 237242, 231521, 237809, 237236, 234626, 238619, 235757, 239948, 236765, 238892, 238685, 238613, 237194, 237071, 236198, 235970, 235574, 232034, 240158, 239537, 237893, 237818, 234980, 233408, 242951, 239315, 237851, 237854, 237437, 237143, 235973, 235259, 231689, 241091, 239132, 238124, 243419, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 237701, 237638, 237674, 236864, 239798, 237458, 238778, 237581, 240155, 239792, 237650, 237608, 237587, 239216, 237677, 237452, 237449, 237662, 237695, 242216, 237710, 237683, 1340, 1360, 1380, 1400, 1420, 1460, 1480, 1500, 237242, 239117, 238619, 236864, 238613, 236882, 236720, 240158, 237818, 239498, 237854, 237809, 237743, 1180, 1200, 1260, 1280, 1300]
							

				print category_id_list
				
			except:
				print "-----------------------exception------------------------------"
				category_id_list=[234626, 237242, 240155, 231521, 231362, 237809, 240158, 239300, 237647, 234914, 237701, 238619, 239981, 235757, 236990, 239798, 239948, 237458, 236765, 238778, 237740, 231689, 237236, 236864, 237143, 237674, 237581, 235973, 239537, 235970, 232034, 244613, 239117, 238892, 238685, 238613, 237842, 237650, 237608, 237587, 237194, 237071, 236882, 236720, 236198, 235574, 239216, 237893, 237818, 237749, 237677, 237440, 235265, 239894, 239792, 239498, 238883, 237452, 235997, 234980, 233408, 242951, 239315, 237851, 237449, 237398, 237854, 237437, 235259, 237662, 237638, 238166, 236141, 234431, 238124, 242216, 237695, 237149, 237683, 237710, 237743, 238013, 241091, 243419, 239132, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 234626, 237242, 231362, 231521, 234914, 239300, 238619, 239981, 235757, 236990, 236765, 237809, 237236, 244613, 238892, 238685, 238613, 237842, 237740, 237194, 237071, 236720, 236198, 235970, 235574, 232034, 240158, 239537, 237818, 237749, 235265, 239894, 238883, 235997, 234980, 233408, 242951, 239315, 237851, 237398, 237854, 237143, 235973, 235259, 231689, 238166, 236141, 239132, 243419, 241091, 238013, 20, 50, 100, 140, 190, 250, 320, 400, 460, 540, 580, 590, 237242, 231521, 237809, 237236, 234626, 238619, 235757, 239948, 236765, 238892, 238685, 238613, 237194, 237071, 236198, 235970, 235574, 232034, 240158, 239537, 237893, 237818, 234980, 233408, 242951, 239315, 237851, 237854, 237437, 237143, 235973, 235259, 231689, 241091, 239132, 238124, 243419, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 237701, 237638, 237674, 236864, 239798, 237458, 238778, 237581, 240155, 239792, 237650, 237608, 237587, 239216, 237677, 237452, 237449, 237662, 237695, 242216, 237710, 237683, 1340, 1360, 1380, 1400, 1420, 1460, 1480, 1500, 237242, 239117, 238619, 236864, 238613, 236882, 236720, 240158, 237818, 239498, 237854, 237809, 237743, 1180, 1200, 1260, 1280, 1300]

			if category_id_list==None or len(category_id_list)<1:
				category_id_list=[234626, 237242, 240155, 231521, 231362, 237809, 240158, 239300, 237647, 234914, 237701, 238619, 239981, 235757, 236990, 239798, 239948, 237458, 236765, 238778, 237740, 231689, 237236, 236864, 237143, 237674, 237581, 235973, 239537, 235970, 232034, 244613, 239117, 238892, 238685, 238613, 237842, 237650, 237608, 237587, 237194, 237071, 236882, 236720, 236198, 235574, 239216, 237893, 237818, 237749, 237677, 237440, 235265, 239894, 239792, 239498, 238883, 237452, 235997, 234980, 233408, 242951, 239315, 237851, 237449, 237398, 237854, 237437, 235259, 237662, 237638, 238166, 236141, 234431, 238124, 242216, 237695, 237149, 237683, 237710, 237743, 238013, 241091, 243419, 239132, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 234626, 237242, 231362, 231521, 234914, 239300, 238619, 239981, 235757, 236990, 236765, 237809, 237236, 244613, 238892, 238685, 238613, 237842, 237740, 237194, 237071, 236720, 236198, 235970, 235574, 232034, 240158, 239537, 237818, 237749, 235265, 239894, 238883, 235997, 234980, 233408, 242951, 239315, 237851, 237398, 237854, 237143, 235973, 235259, 231689, 238166, 236141, 239132, 243419, 241091, 238013, 20, 50, 100, 140, 190, 250, 320, 400, 460, 540, 580, 590, 237242, 231521, 237809, 237236, 234626, 238619, 235757, 239948, 236765, 238892, 238685, 238613, 237194, 237071, 236198, 235970, 235574, 232034, 240158, 239537, 237893, 237818, 234980, 233408, 242951, 239315, 237851, 237854, 237437, 237143, 235973, 235259, 231689, 241091, 239132, 238124, 243419, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 237701, 237638, 237674, 236864, 239798, 237458, 238778, 237581, 240155, 239792, 237650, 237608, 237587, 239216, 237677, 237452, 237449, 237662, 237695, 242216, 237710, 237683, 1340, 1360, 1380, 1400, 1420, 1460, 1480, 1500, 237242, 239117, 238619, 236864, 238613, 236882, 236720, 240158, 237818, 239498, 237854, 237809, 237743, 1180, 1200, 1260, 1280, 1300]

			print "\nself_call_getCategory\n"
			request=getCategory( campaign_data, device_data, app_data )
			output=util.execute_request(**request)
			global sub_category_id_list,sub_category_name_list
			try:
				sub_category_id_list=[]
				sub_category_name_list=[]
				
				categories_data=json.loads(output.get('data')).get('data').get('categories_data')
				for i in range(len(categories_data)):		
					sub_category_id_list.append(categories_data[i].get('wcategory_PK'))
					sub_category_name_list.append(categories_data[i].get('wcategory_name'))

				if len(sub_category_id_list)<1 or len(sub_category_name_list)<1:
					sub_category_id_list=[7776, 8359778, 8359781, 8359784]
					sub_category_name_list=['Most wanted', 'Botas Y Botines', 'Tenis Y Mocasines', 'Zapatos']
							

				print sub_category_id_list
				print sub_category_name_list

			except:
				print "-----------------exception-------------------------------"
				sub_category_id_list=[7776, 8359778, 8359781, 8359784]
				sub_category_name_list=['Most wanted', 'Botas Y Botines', 'Tenis Y Mocasines', 'Zapatos']

			if len(sub_category_id_list)<1 or len(sub_category_name_list)<1:
				sub_category_id_list=[7776, 8359778, 8359781, 8359784]
				sub_category_name_list=['Most wanted', 'Botas Y Botines', 'Tenis Y Mocasines', 'Zapatos']

			print "\nself_call_getProducts\n"
			request=getProducts( campaign_data, device_data, app_data )
			output=util.execute_request(**request)
			global product_id_list,product_name_list,product_price_list,campaign_name_list,stock_id_list,context_list
			try:
				product_id_list=[]
				product_name_list=[]
				product_price_list=[]
				campaign_name_list=[]
				stock_id_list=[]
				context_list=[]
				
				product_data=json.loads(output.get('data')).get('data').get('products_data')
				for i in range(len(product_data)):		
					product_id_list.append(product_data[i].get('product_PK'))
					product_name_list.append(product_data[i].get('product_name'))
					product_price_list.append(product_data[i].get('product_pvs'))
					campaign_name_list.append(product_data[i].get('campaign_name'))
					stock_id_list.append(product_data[i].get('product_sizes')[0].get('stockp_PK'))
					context_list.append(product_data[i].get('product_sizes')[0].get('quantity'))

				if len(product_id_list)<3 or len(product_name_list)<3:
					product_id_list=[34764929, 34764914, 34764938, 34764917, 34764920, 34764962, 34764974, 34764923, 34764926, 34764977, 34764998, 34764932, 34764935, 34764941, 34764944, 34764947, 34764950, 34764953, 34764956, 34764959, 34764965, 34764968, 34764971, 34764980, 34764983, 34764986, 34764989, 34764992, 34764995, 34765001]
					product_name_list=['Baterda de Cocina Pards Granito<br /> 7 Pzas<br /> Rojo', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Azul', 'Baterda de Cocina Simple Cooking<br /> 5 Pzas<br /> Rojo', 'Baterda de Cocina Lisboa 6 piezas<BR>6 Pzas<BR>Cobre', 'Baterda de Cocina Lisboa<BR>5 Pzas<BR>Gris', 'Olla Pards con Tapa<br /> 28 cm<br /> Negro', 'Olla My Lovely Kitchen<br /> 18 cm<br /> Multicolor', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Rojo', 'Baterda de Cocina Victoria<BR>5 Pzas<BR>Rojo', 'Olla para Pasta My Lovely Kitchen<br /> 22 cm<br /> Negro y Dorado', 'Olla "Brunette"<br /> 5.7 L<br /> Negro', 'Baterda de Cocina Milan<BR>5 Pzas<BR>Negro', 'Baterda Versalhes<BR>5 Pzas<BR>Negro', 'Baterda de Cocina Pulido Every Day<BR>5 Pzas<BR>Plateado', 'Baterda Primaware<BR>5 Pzas<BR>Rojo', 'Juego de Sartenes "Napoli"<BR>4 Pzas<BR>Negro', 'Juego de Sartenes Chelsea<BR>3 Pzas<BR>Rojo', 'Sart9n Bergamo<BR>24 cm<BR>Azul', 'Sart9n Bergamo<BR>25 cm<BR>Azul', 'Juego de Sartenes "Loreto"<BR>2 Pzas<BR>Rojo', 'Sart9n My Lovely Kitchen<BR>22 cm<BR>Multicolor', 'Sart9n con Honda My Lovely Kitchen<BR>24 cm<BR>Dorado', 'Sart9n My Lovely Kitchen<BR>24 cm<BR>Negro y Dorado', 'Olla Wok My Lovely Kitchen<BR>32 cm<BR>Negro y Dorado', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Verde', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Azul', 'Juego de Sartenes "Brunette"<BR>2 Pzas<BR>Negro', 'Juego de Sartenes Primaware<BR>2 Pzas<BR>Negro']
					product_price_list=['$1,289.00 ', '$869.00 ', '$1,189.00 ', '$869.00 ', '$869.00 ', '$879.00 ', '$389.00 ', '$869.00 ', '$1,479.00 ', '$789.00 ', '$769.00 ', '$869.00 ', '$989.00 ', '$1,489.00 ', '$869.00 ', '$319.00 ', '$629.00 ', '$109.00 ', '$139.00 ', '$239.00 ', '$169.00 ', '$289.00 ', '$169.00 ', '$819.00 ', '$169.00 ', '$209.00 ', '$119.00 ', '$119.00 ', '$819.00 ', '$389.00 ']
					campaign_name_list=['Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina']

					stock_id_list=['80916044', '80916029', '80916053', '80916032', '80916035', '80916077', '80916089', '80916038', '80916041', '80916092', '80916113', '80916047', '80916050', '80916056', '80916059', '80916062', '80916065', '80916068', '80916071', '80916074', '80916080', '80916083', '80916086', '80916095', '80916098', '80916101', '80916104', '80916107', '80916110', '80916116']
					context_list=[10, 10, 10, 10, 10, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]

				print product_id_list
				print product_name_list
				print product_price_list
				print campaign_name_list
				print stock_id_list
				print context_list

			except:
				print "-----------------exception-------------------------------"
				product_id_list=[34764929, 34764914, 34764938, 34764917, 34764920, 34764962, 34764974, 34764923, 34764926, 34764977, 34764998, 34764932, 34764935, 34764941, 34764944, 34764947, 34764950, 34764953, 34764956, 34764959, 34764965, 34764968, 34764971, 34764980, 34764983, 34764986, 34764989, 34764992, 34764995, 34765001]
				product_name_list=['Baterda de Cocina Pards Granito<br /> 7 Pzas<br /> Rojo', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Azul', 'Baterda de Cocina Simple Cooking<br /> 5 Pzas<br /> Rojo', 'Baterda de Cocina Lisboa 6 piezas<BR>6 Pzas<BR>Cobre', 'Baterda de Cocina Lisboa<BR>5 Pzas<BR>Gris', 'Olla Pards con Tapa<br /> 28 cm<br /> Negro', 'Olla My Lovely Kitchen<br /> 18 cm<br /> Multicolor', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Rojo', 'Baterda de Cocina Victoria<BR>5 Pzas<BR>Rojo', 'Olla para Pasta My Lovely Kitchen<br /> 22 cm<br /> Negro y Dorado', 'Olla "Brunette"<br /> 5.7 L<br /> Negro', 'Baterda de Cocina Milan<BR>5 Pzas<BR>Negro', 'Baterda Versalhes<BR>5 Pzas<BR>Negro', 'Baterda de Cocina Pulido Every Day<BR>5 Pzas<BR>Plateado', 'Baterda Primaware<BR>5 Pzas<BR>Rojo', 'Juego de Sartenes "Napoli"<BR>4 Pzas<BR>Negro', 'Juego de Sartenes Chelsea<BR>3 Pzas<BR>Rojo', 'Sart9n Bergamo<BR>24 cm<BR>Azul', 'Sart9n Bergamo<BR>25 cm<BR>Azul', 'Juego de Sartenes "Loreto"<BR>2 Pzas<BR>Rojo', 'Sart9n My Lovely Kitchen<BR>22 cm<BR>Multicolor', 'Sart9n con Honda My Lovely Kitchen<BR>24 cm<BR>Dorado', 'Sart9n My Lovely Kitchen<BR>24 cm<BR>Negro y Dorado', 'Olla Wok My Lovely Kitchen<BR>32 cm<BR>Negro y Dorado', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Verde', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Azul', 'Juego de Sartenes "Brunette"<BR>2 Pzas<BR>Negro', 'Juego de Sartenes Primaware<BR>2 Pzas<BR>Negro']
				product_price_list=['$1,289.00 ', '$869.00 ', '$1,189.00 ', '$869.00 ', '$869.00 ', '$879.00 ', '$389.00 ', '$869.00 ', '$1,479.00 ', '$789.00 ', '$769.00 ', '$869.00 ', '$989.00 ', '$1,489.00 ', '$869.00 ', '$319.00 ', '$629.00 ', '$109.00 ', '$139.00 ', '$239.00 ', '$169.00 ', '$289.00 ', '$169.00 ', '$819.00 ', '$169.00 ', '$209.00 ', '$119.00 ', '$119.00 ', '$819.00 ', '$389.00 ']
				campaign_name_list=['Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina']

				stock_id_list=['80916044', '80916029', '80916053', '80916032', '80916035', '80916077', '80916089', '80916038', '80916041', '80916092', '80916113', '80916047', '80916050', '80916056', '80916059', '80916062', '80916065', '80916068', '80916071', '80916074', '80916080', '80916083', '80916086', '80916095', '80916098', '80916101', '80916104', '80916107', '80916110', '80916116']
				context_list=[10, 10, 10, 10, 10, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]

			if len(product_id_list)<3 or len(product_name_list)<3:
				product_id_list=[34764929, 34764914, 34764938, 34764917, 34764920, 34764962, 34764974, 34764923, 34764926, 34764977, 34764998, 34764932, 34764935, 34764941, 34764944, 34764947, 34764950, 34764953, 34764956, 34764959, 34764965, 34764968, 34764971, 34764980, 34764983, 34764986, 34764989, 34764992, 34764995, 34765001]
				product_name_list=['Baterda de Cocina Pards Granito<br /> 7 Pzas<br /> Rojo', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Azul', 'Baterda de Cocina Simple Cooking<br /> 5 Pzas<br /> Rojo', 'Baterda de Cocina Lisboa 6 piezas<BR>6 Pzas<BR>Cobre', 'Baterda de Cocina Lisboa<BR>5 Pzas<BR>Gris', 'Olla Pards con Tapa<br /> 28 cm<br /> Negro', 'Olla My Lovely Kitchen<br /> 18 cm<br /> Multicolor', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Rojo', 'Baterda de Cocina Victoria<BR>5 Pzas<BR>Rojo', 'Olla para Pasta My Lovely Kitchen<br /> 22 cm<br /> Negro y Dorado', 'Olla "Brunette"<br /> 5.7 L<br /> Negro', 'Baterda de Cocina Milan<BR>5 Pzas<BR>Negro', 'Baterda Versalhes<BR>5 Pzas<BR>Negro', 'Baterda de Cocina Pulido Every Day<BR>5 Pzas<BR>Plateado', 'Baterda Primaware<BR>5 Pzas<BR>Rojo', 'Juego de Sartenes "Napoli"<BR>4 Pzas<BR>Negro', 'Juego de Sartenes Chelsea<BR>3 Pzas<BR>Rojo', 'Sart9n Bergamo<BR>24 cm<BR>Azul', 'Sart9n Bergamo<BR>25 cm<BR>Azul', 'Juego de Sartenes "Loreto"<BR>2 Pzas<BR>Rojo', 'Sart9n My Lovely Kitchen<BR>22 cm<BR>Multicolor', 'Sart9n con Honda My Lovely Kitchen<BR>24 cm<BR>Dorado', 'Sart9n My Lovely Kitchen<BR>24 cm<BR>Negro y Dorado', 'Olla Wok My Lovely Kitchen<BR>32 cm<BR>Negro y Dorado', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Verde', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Azul', 'Juego de Sartenes "Brunette"<BR>2 Pzas<BR>Negro', 'Juego de Sartenes Primaware<BR>2 Pzas<BR>Negro']
				product_price_list=['$1,289.00 ', '$869.00 ', '$1,189.00 ', '$869.00 ', '$869.00 ', '$879.00 ', '$389.00 ', '$869.00 ', '$1,479.00 ', '$789.00 ', '$769.00 ', '$869.00 ', '$989.00 ', '$1,489.00 ', '$869.00 ', '$319.00 ', '$629.00 ', '$109.00 ', '$139.00 ', '$239.00 ', '$169.00 ', '$289.00 ', '$169.00 ', '$819.00 ', '$169.00 ', '$209.00 ', '$119.00 ', '$119.00 ', '$819.00 ', '$389.00 ']
				campaign_name_list=['Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina']

				stock_id_list=['80916044', '80916029', '80916053', '80916032', '80916035', '80916077', '80916089', '80916038', '80916041', '80916092', '80916113', '80916047', '80916050', '80916056', '80916059', '80916062', '80916065', '80916068', '80916071', '80916074', '80916080', '80916083', '80916086', '80916095', '80916098', '80916101', '80916104', '80916107', '80916110', '80916116']
				context_list=[10, 10, 10, 10, 10, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]

			temp=[]
			for i in range(random.randint(1,3)):
				temp.append(product_id_list[i])

			app_data['index']=random.randint(0,len(temp)-1)

			

			if random.randint(1,100)<=90:

				time.sleep(random.randint(20,45))

				adjust_token_kz9hjw(campaign_data, app_data, device_data,e1,e2)

				time.sleep(random.randint(20,30))
				adjust_token_pkwzq0(campaign_data, app_data, device_data,e1,e2)

				if random.randint(1,100)<=90:

					# print "\nself_call_productsheet\n"
					# request=productsheet( campaign_data, device_data, app_data )
					# util.execute_request(**request)


					time.sleep(random.randint(10,25))

					adjust_token_w1b3pq(campaign_data, app_data, device_data,e1,e2)

					if random.randint(1,100)<=90:
						print "\nself_call_shoppingcart\n"
						request=shoppingcart( campaign_data, device_data, app_data )
						output=util.execute_request(**request)
						try:
							app_data['cartID']=json.loads(output.get('data')).get('data').get('cart').keys()[0]

							if app_data.get('cartID')=='' or app_data.get('cartID')==None:
								app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

							print app_data['cartID']
						except:
							app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

						if app_data.get('cartID')=='' or app_data.get('cartID')==None:
							app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

						time.sleep(random.randint(15,20))

						adjust_token_bnrwg8(campaign_data, app_data, device_data,e1,e2)
						app_data['count']+=1

						if random.randint(1,100)<=90:
						
							time.sleep(random.randint(10,20))

							adjust_token_x3aw91(campaign_data, app_data, device_data,e1,e2)

							if random.randint(1,100)<=70:

								time.sleep(random.randint(40,60))

								adjust_token_xvc4tu(campaign_data, app_data, device_data,e1,e2)
								app_data['count']=0
								criteo=[]
								app_data['amount']=0

			if random.randint(1,100)<=95:

				time.sleep(random.randint(20,30))

				adjust_token_pkwzq0(campaign_data, app_data, device_data,e1,e2)

				if random.randint(1,100)<=90:

					# print "\nself_call_productsheet\n"
					# request=productsheet( campaign_data, device_data, app_data )
					# util.execute_request(**request)


					time.sleep(random.randint(10,25))

					adjust_token_w1b3pq(campaign_data, app_data, device_data,e1,e2)

					if random.randint(1,100)<=90:
						print "\nself_call_shoppingcart\n"
						request=shoppingcart( campaign_data, device_data, app_data )
						output=util.execute_request(**request)
						try:
							app_data['cartID']=json.loads(output.get('data')).get('data').get('cart').keys()[0]

							if app_data.get('cartID')=='' or app_data.get('cartID')==None:
								app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

							print app_data['cartID']
						except:
							app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

						if app_data.get('cartID')=='' or app_data.get('cartID')==None:
							app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

						time.sleep(random.randint(15,20))

						adjust_token_bnrwg8(campaign_data, app_data, device_data,e1,e2)
						app_data['count']+=1

						if random.randint(1,100)<=90:
						
							time.sleep(random.randint(10,20))

							adjust_token_x3aw91(campaign_data, app_data, device_data,e1,e2)

							if random.randint(1,100)<=70:

								time.sleep(random.randint(40,60))

								adjust_token_xvc4tu(campaign_data, app_data, device_data,e1,e2)
								app_data['count']=0
								criteo=[]
								app_data['amount']=0

	set_appCloseTime(app_data)
	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),
	# pushtoken(app_data)
	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	################generating_realtime_differences##################
	s1=0
	s2=0
	a1=18
	a2=20
	e1=0
	e2=0


	global currency_list,country_lists
	country_lists=['MX','ES','BR','IT']
	currency_list=['MXN','EUR','BRL','EUR']
	
	if not app_data.get('n'):
		app_data['n']=random.randint(0,len(country_lists)-1)

	global criteo
	criteo=[]

	global temp
	temp=[]


	if not app_data.get('count'):
		app_data['count']=0

	if not app_data.get('amount'):
		app_data['amount']=0

	if not app_data.get('register'):
		app_data['register']=False

 ###########	 CALLS		 ############
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,t1=s1,t2=s2,isOpen=True)
	util.execute_request(**request)

	if not app_data.get('PRIVALIASESSID_mex'):
		print "\nself_call_getBanners\n"
		request=getBanners( campaign_data, device_data, app_data )
		output=util.execute_request(**request)	
		try:
			# app_data['outputs']= output.get('res').headers.get('Set-Cookie')
			app_data['PRIVALIASESSID_mex']=output.get('res').headers.get('Set-Cookie').split('PRIVALIASESSID_mex=')[1].split(';')[0]
			app_data['TS01fa3430']=output.get('res').headers.get('Set-Cookie').split('TS01fa3430=')[1].split(';')[0]
			app_data['TS01b98eb7']=output.get('res').headers.get('Set-Cookie').split('TS01b98eb7=')[1].split(';')[0]
			
			

			if app_data.get('PRIVALIASESSID_mex')=='' or app_data.get('PRIVALIASESSID_mex')==None:
				app_data['PRIVALIASESSID_mex']='scerp9ok5ctt8vpm5q97mmpf26'
				app_data['TS01fa3430']='018bcad25e3738536ea78c30ce4165137a77e819dcabb88a82a4d7ecabc6d90d6efeca43b5'
				app_data['TS01b98eb7']='018bcad25e9f1d9393165e68285e0b2a65ba1acc57e7f10dbd54195e717ed2aa17bf1bcba31465a1db191afe6ba04aeba492b9daa6'



			print app_data['PRIVALIASESSID_mex']
			print app_data['TS01fa3430']
			print app_data['TS01b98eb7']


			
			
		except:
			app_data['PRIVALIASESSID_mex']='scerp9ok5ctt8vpm5q97mmpf26'
			app_data['TS01fa3430']='018bcad25e3738536ea78c30ce4165137a77e819dcabb88a82a4d7ecabc6d90d6efeca43b5'
			app_data['TS01b98eb7']='018bcad25e9f1d9393165e68285e0b2a65ba1acc57e7f10dbd54195e717ed2aa17bf1bcba31465a1db191afe6ba04aeba492b9daa6'

		if app_data.get('PRIVALIASESSID_mex')=='' or app_data.get('PRIVALIASESSID_mex')==None:
			app_data['PRIVALIASESSID_mex']='scerp9ok5ctt8vpm5q97mmpf26'
			app_data['TS01fa3430']='018bcad25e3738536ea78c30ce4165137a77e819dcabb88a82a4d7ecabc6d90d6efeca43b5'
			app_data['TS01b98eb7']='018bcad25e9f1d9393165e68285e0b2a65ba1acc57e7f10dbd54195e717ed2aa17bf1bcba31465a1db191afe6ba04aeba492b9daa6'
		
		
	if not app_data.get('FC_PRIVALIA_S_mex'):
		time.sleep(random.randint(45,50))

		print "\nself_call_registerProcess\n"
		request=registerProcess( campaign_data, device_data, app_data )
		output=util.execute_request(**request)	
		try:
			# app_data['outputs']= output.get('res').headers.get('Set-Cookie')
			app_data['PRIVALIASESSID_mex']=output.get('res').headers.get('Set-Cookie').split('PRIVALIASESSID_mex=')[1].split(';')[0]
			app_data['TS01fa3430']=output.get('res').headers.get('Set-Cookie').split('TS01fa3430=')[1].split(';')[0]
			app_data['TS01b98eb7']=output.get('res').headers.get('Set-Cookie').split('TS01b98eb7=')[1].split(';')[0]
			app_data['FC_PRIVALIA_S_mex']=output.get('res').headers.get('Set-Cookie').split('FC_PRIVALIA_S_mex=')[1].split(';')[0]
			app_data['session_timestamp']=output.get('res').headers.get('Set-Cookie').split('session_timestamp=')[1].split(';')[0]
			app_data['watsonAutoLogin']=output.get('res').headers.get('Set-Cookie').split('watsonAutoLogin=')[1].split(';')[0]
			app_data['watsonRegistered']=output.get('res').headers.get('Set-Cookie').split('watsonRegistered=')[1].split(';')[0]
			app_data['TS01170bf2']=output.get('res').headers.get('Set-Cookie').split('TS01170bf2=')[1].split(';')[0]
			
			

			if app_data.get('PRIVALIASESSID_mex')=='' or app_data.get('PRIVALIASESSID_mex')==None:
				app_data['PRIVALIASESSID_mex']='mgdo101jjqdegflshi5hhiugh2'
				app_data['TS01fa3430']='018bcad25ecd74cbca2eed5e6334f5aef3aad8c575d6b2c6eae870fd8f13541570dd7e0ab829a76a5d9e544f4ff9cc2321aecd80c865367d16fd0b92c0d3bde55eaceccfb922e32bf8c51d75a1556d49272c4cd929'
				app_data['TS01b98eb7']='018bcad25e985ab328b4e0fe6fcaf19789be6924bcc4bf0ee134fbbbf6122bb093483cb8b764d43690acaae54e3632fd30f33e67e38268cea6714d5259d24ce51cc0527cb7'
				app_data['FC_PRIVALIA_S_mex']='77939915-0a1a79f40594e7607b3c5313950e7ae0-5ab9b27adf8f58dac6468fa3400f2685'
				app_data['session_timestamp']='1560246551%3B77939915'
				app_data['watsonAutoLogin']='77939915'
				app_data['watsonRegistered']='77939915'
				app_data['TS01170bf2']='018bcad25ed4492c5ebc7f0d0693df5ce4bc2fc746d6b2c6eae870fd8f13541570dd7e0ab814a0f535e15589a83c2f76f579c52c9a526a896906749a57674abfef0e09eb401c47e93d9edab7596b061b482929e6ed0744729a4cbdf6f5b33609d2f7afa796'

			print app_data['PRIVALIASESSID_mex']
			print app_data['TS01fa3430']
			print app_data['TS01b98eb7']
			print app_data['FC_PRIVALIA_S_mex']
			print app_data['session_timestamp']
			print app_data['watsonAutoLogin']
			print app_data['watsonRegistered']
			print app_data['TS01170bf2']
			
			
		except:
			app_data['PRIVALIASESSID_mex']='mgdo101jjqdegflshi5hhiugh2'
			app_data['TS01fa3430']='018bcad25ecd74cbca2eed5e6334f5aef3aad8c575d6b2c6eae870fd8f13541570dd7e0ab829a76a5d9e544f4ff9cc2321aecd80c865367d16fd0b92c0d3bde55eaceccfb922e32bf8c51d75a1556d49272c4cd929'
			app_data['TS01b98eb7']='018bcad25e985ab328b4e0fe6fcaf19789be6924bcc4bf0ee134fbbbf6122bb093483cb8b764d43690acaae54e3632fd30f33e67e38268cea6714d5259d24ce51cc0527cb7'
			app_data['FC_PRIVALIA_S_mex']='77939915-0a1a79f40594e7607b3c5313950e7ae0-5ab9b27adf8f58dac6468fa3400f2685'
			app_data['session_timestamp']='1560246551%3B77939915'
			app_data['watsonAutoLogin']='77939915'
			app_data['watsonRegistered']='77939915'
			app_data['TS01170bf2']='018bcad25ed4492c5ebc7f0d0693df5ce4bc2fc746d6b2c6eae870fd8f13541570dd7e0ab814a0f535e15589a83c2f76f579c52c9a526a896906749a57674abfef0e09eb401c47e93d9edab7596b061b482929e6ed0744729a4cbdf6f5b33609d2f7afa796'

		if app_data.get('PRIVALIASESSID_mex')=='' or app_data.get('PRIVALIASESSID_mex')==None:
			app_data['PRIVALIASESSID_mex']='mgdo101jjqdegflshi5hhiugh2'
			app_data['TS01fa3430']='018bcad25ecd74cbca2eed5e6334f5aef3aad8c575d6b2c6eae870fd8f13541570dd7e0ab829a76a5d9e544f4ff9cc2321aecd80c865367d16fd0b92c0d3bde55eaceccfb922e32bf8c51d75a1556d49272c4cd929'
			app_data['TS01b98eb7']='018bcad25e985ab328b4e0fe6fcaf19789be6924bcc4bf0ee134fbbbf6122bb093483cb8b764d43690acaae54e3632fd30f33e67e38268cea6714d5259d24ce51cc0527cb7'
			app_data['FC_PRIVALIA_S_mex']='77939915-0a1a79f40594e7607b3c5313950e7ae0-5ab9b27adf8f58dac6468fa3400f2685'
			app_data['session_timestamp']='1560246551%3B77939915'
			app_data['watsonAutoLogin']='77939915'
			app_data['watsonRegistered']='77939915'
			app_data['TS01170bf2']='018bcad25ed4492c5ebc7f0d0693df5ce4bc2fc746d6b2c6eae870fd8f13541570dd7e0ab814a0f535e15589a83c2f76f579c52c9a526a896906749a57674abfef0e09eb401c47e93d9edab7596b061b482929e6ed0744729a4cbdf6f5b33609d2f7afa796'

	

	if app_data.get('register')==False:
		print "\nself_call_login\n"
		request=login( campaign_data, device_data, app_data )
		util.execute_request(**request)

		adjust_token_soci2a(campaign_data, app_data, device_data,e1,e2)

		print "\nself_call_member\n"
		request=member( campaign_data, device_data, app_data )
		output=util.execute_request(**request)	
		try:
			# app_data['outputs']= output.get('res').headers.get('Set-Cookie')
			app_data['ADRUM_BT']=output.get('res').headers.get('Set-Cookie').split('ADRUM_BT=')[1].split(';')[0]
			
			

			if app_data.get('ADRUM_BT')=='' or app_data.get('ADRUM_BT')==None:
				app_data['ADRUM_BT']='R%3A0%7Cg%3A2306c1e2-6394-4873-8a38-e2b37881fe7962190%7Cn%3Aprivalia_ba4d61d5-dcd0-4326-835c-7531a86cf86d%7Cd%3A89%7Ce%3A181'
			print app_data['ADRUM_BT']
			
			
		except:
			app_data['ADRUM_BT']='R%3A0%7Cg%3A2306c1e2-6394-4873-8a38-e2b37881fe7962190%7Cn%3Aprivalia_ba4d61d5-dcd0-4326-835c-7531a86cf86d%7Cd%3A89%7Ce%3A181'

		if app_data.get('ADRUM_BT')=='' or app_data.get('ADRUM_BT')==None:
				app_data['ADRUM_BT']='R%3A0%7Cg%3A2306c1e2-6394-4873-8a38-e2b37881fe7962190%7Cn%3Aprivalia_ba4d61d5-dcd0-4326-835c-7531a86cf86d%7Cd%3A89%7Ce%3A181'
		
			
		adjust_token_vfxzkk(campaign_data, app_data, device_data,e1,e2)
		
		app_data['register']=True


	if app_data.get('register')==True:		
		for i in range(random.randint(1,3)):

			print "\nself_call_menu\n"
			request=menu( campaign_data, device_data, app_data )
			output=util.execute_request(**request)
			global category_id_list
			try:
				category_id_list=[]
				
				menu_item=json.loads(output.get('data')).get('data').get('menu')
				for i in range(len(menu_item)):
					children=menu_item[i].get('children')
					for j in range(len(children)):
						sub_child=children[j].get('children')
						for k in range(len(sub_child)):
							category_id_list.append(sub_child[k].get('id'))

				if category_id_list==None or len(category_id_list)<1:
					category_id_list=[234626, 237242, 240155, 231521, 231362, 237809, 240158, 239300, 237647, 234914, 237701, 238619, 239981, 235757, 236990, 239798, 239948, 237458, 236765, 238778, 237740, 231689, 237236, 236864, 237143, 237674, 237581, 235973, 239537, 235970, 232034, 244613, 239117, 238892, 238685, 238613, 237842, 237650, 237608, 237587, 237194, 237071, 236882, 236720, 236198, 235574, 239216, 237893, 237818, 237749, 237677, 237440, 235265, 239894, 239792, 239498, 238883, 237452, 235997, 234980, 233408, 242951, 239315, 237851, 237449, 237398, 237854, 237437, 235259, 237662, 237638, 238166, 236141, 234431, 238124, 242216, 237695, 237149, 237683, 237710, 237743, 238013, 241091, 243419, 239132, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 234626, 237242, 231362, 231521, 234914, 239300, 238619, 239981, 235757, 236990, 236765, 237809, 237236, 244613, 238892, 238685, 238613, 237842, 237740, 237194, 237071, 236720, 236198, 235970, 235574, 232034, 240158, 239537, 237818, 237749, 235265, 239894, 238883, 235997, 234980, 233408, 242951, 239315, 237851, 237398, 237854, 237143, 235973, 235259, 231689, 238166, 236141, 239132, 243419, 241091, 238013, 20, 50, 100, 140, 190, 250, 320, 400, 460, 540, 580, 590, 237242, 231521, 237809, 237236, 234626, 238619, 235757, 239948, 236765, 238892, 238685, 238613, 237194, 237071, 236198, 235970, 235574, 232034, 240158, 239537, 237893, 237818, 234980, 233408, 242951, 239315, 237851, 237854, 237437, 237143, 235973, 235259, 231689, 241091, 239132, 238124, 243419, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 237701, 237638, 237674, 236864, 239798, 237458, 238778, 237581, 240155, 239792, 237650, 237608, 237587, 239216, 237677, 237452, 237449, 237662, 237695, 242216, 237710, 237683, 1340, 1360, 1380, 1400, 1420, 1460, 1480, 1500, 237242, 239117, 238619, 236864, 238613, 236882, 236720, 240158, 237818, 239498, 237854, 237809, 237743, 1180, 1200, 1260, 1280, 1300]
							

				print category_id_list
				
			except:
				print "-----------------------exception------------------------------"
				category_id_list=[234626, 237242, 240155, 231521, 231362, 237809, 240158, 239300, 237647, 234914, 237701, 238619, 239981, 235757, 236990, 239798, 239948, 237458, 236765, 238778, 237740, 231689, 237236, 236864, 237143, 237674, 237581, 235973, 239537, 235970, 232034, 244613, 239117, 238892, 238685, 238613, 237842, 237650, 237608, 237587, 237194, 237071, 236882, 236720, 236198, 235574, 239216, 237893, 237818, 237749, 237677, 237440, 235265, 239894, 239792, 239498, 238883, 237452, 235997, 234980, 233408, 242951, 239315, 237851, 237449, 237398, 237854, 237437, 235259, 237662, 237638, 238166, 236141, 234431, 238124, 242216, 237695, 237149, 237683, 237710, 237743, 238013, 241091, 243419, 239132, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 234626, 237242, 231362, 231521, 234914, 239300, 238619, 239981, 235757, 236990, 236765, 237809, 237236, 244613, 238892, 238685, 238613, 237842, 237740, 237194, 237071, 236720, 236198, 235970, 235574, 232034, 240158, 239537, 237818, 237749, 235265, 239894, 238883, 235997, 234980, 233408, 242951, 239315, 237851, 237398, 237854, 237143, 235973, 235259, 231689, 238166, 236141, 239132, 243419, 241091, 238013, 20, 50, 100, 140, 190, 250, 320, 400, 460, 540, 580, 590, 237242, 231521, 237809, 237236, 234626, 238619, 235757, 239948, 236765, 238892, 238685, 238613, 237194, 237071, 236198, 235970, 235574, 232034, 240158, 239537, 237893, 237818, 234980, 233408, 242951, 239315, 237851, 237854, 237437, 237143, 235973, 235259, 231689, 241091, 239132, 238124, 243419, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 237701, 237638, 237674, 236864, 239798, 237458, 238778, 237581, 240155, 239792, 237650, 237608, 237587, 239216, 237677, 237452, 237449, 237662, 237695, 242216, 237710, 237683, 1340, 1360, 1380, 1400, 1420, 1460, 1480, 1500, 237242, 239117, 238619, 236864, 238613, 236882, 236720, 240158, 237818, 239498, 237854, 237809, 237743, 1180, 1200, 1260, 1280, 1300]

			if category_id_list==None or len(category_id_list)<1:
				category_id_list=[234626, 237242, 240155, 231521, 231362, 237809, 240158, 239300, 237647, 234914, 237701, 238619, 239981, 235757, 236990, 239798, 239948, 237458, 236765, 238778, 237740, 231689, 237236, 236864, 237143, 237674, 237581, 235973, 239537, 235970, 232034, 244613, 239117, 238892, 238685, 238613, 237842, 237650, 237608, 237587, 237194, 237071, 236882, 236720, 236198, 235574, 239216, 237893, 237818, 237749, 237677, 237440, 235265, 239894, 239792, 239498, 238883, 237452, 235997, 234980, 233408, 242951, 239315, 237851, 237449, 237398, 237854, 237437, 235259, 237662, 237638, 238166, 236141, 234431, 238124, 242216, 237695, 237149, 237683, 237710, 237743, 238013, 241091, 243419, 239132, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 234626, 237242, 231362, 231521, 234914, 239300, 238619, 239981, 235757, 236990, 236765, 237809, 237236, 244613, 238892, 238685, 238613, 237842, 237740, 237194, 237071, 236720, 236198, 235970, 235574, 232034, 240158, 239537, 237818, 237749, 235265, 239894, 238883, 235997, 234980, 233408, 242951, 239315, 237851, 237398, 237854, 237143, 235973, 235259, 231689, 238166, 236141, 239132, 243419, 241091, 238013, 20, 50, 100, 140, 190, 250, 320, 400, 460, 540, 580, 590, 237242, 231521, 237809, 237236, 234626, 238619, 235757, 239948, 236765, 238892, 238685, 238613, 237194, 237071, 236198, 235970, 235574, 232034, 240158, 239537, 237893, 237818, 234980, 233408, 242951, 239315, 237851, 237854, 237437, 237143, 235973, 235259, 231689, 241091, 239132, 238124, 243419, 610, 660, 700, 740, 790, 840, 890, 980, 1030, 1110, 1190, 237701, 237638, 237674, 236864, 239798, 237458, 238778, 237581, 240155, 239792, 237650, 237608, 237587, 239216, 237677, 237452, 237449, 237662, 237695, 242216, 237710, 237683, 1340, 1360, 1380, 1400, 1420, 1460, 1480, 1500, 237242, 239117, 238619, 236864, 238613, 236882, 236720, 240158, 237818, 239498, 237854, 237809, 237743, 1180, 1200, 1260, 1280, 1300]


			print "\nself_call_getCategory\n"
			request=getCategory( campaign_data, device_data, app_data )
			output=util.execute_request(**request)
			global sub_category_id_list,sub_category_name_list
			try:
				sub_category_id_list=[]
				sub_category_name_list=[]
				
				categories_data=json.loads(output.get('data')).get('data').get('categories_data')
				for i in range(len(categories_data)):		
					sub_category_id_list.append(categories_data[i].get('wcategory_PK'))
					sub_category_name_list.append(categories_data[i].get('wcategory_name'))

				if len(sub_category_id_list)<1 or len(sub_category_name_list)<1:
					sub_category_id_list=[7776, 8359778, 8359781, 8359784]
					sub_category_name_list=['Most wanted', 'Botas Y Botines', 'Tenis Y Mocasines', 'Zapatos']
							

				print sub_category_id_list
				print sub_category_name_list

			except:
				print "-----------------exception-------------------------------"
				sub_category_id_list=[7776, 8359778, 8359781, 8359784]
				sub_category_name_list=['Most wanted', 'Botas Y Botines', 'Tenis Y Mocasines', 'Zapatos']

			if len(sub_category_id_list)<1 or len(sub_category_name_list)<1:
				sub_category_id_list=[7776, 8359778, 8359781, 8359784]
				sub_category_name_list=['Most wanted', 'Botas Y Botines', 'Tenis Y Mocasines', 'Zapatos']

			print "\nself_call_getProducts\n"
			request=getProducts( campaign_data, device_data, app_data )
			output=util.execute_request(**request)
			global product_id_list,product_name_list,product_price_list,campaign_name_list,stock_id_list,context_list
			try:
				product_id_list=[]
				product_name_list=[]
				product_price_list=[]
				campaign_name_list=[]
				stock_id_list=[]
				context_list=[]
				
				product_data=json.loads(output.get('data')).get('data').get('products_data')
				for i in range(len(product_data)):		
					product_id_list.append(product_data[i].get('product_PK'))
					product_name_list.append(product_data[i].get('product_name'))
					product_price_list.append(product_data[i].get('product_pvs'))
					campaign_name_list.append(product_data[i].get('campaign_name'))
					stock_id_list.append(product_data[i].get('product_sizes')[0].get('stockp_PK'))
					context_list.append(product_data[i].get('product_sizes')[0].get('quantity'))

				if len(product_id_list)<3 or len(product_name_list)<3:
					product_id_list=[34764929, 34764914, 34764938, 34764917, 34764920, 34764962, 34764974, 34764923, 34764926, 34764977, 34764998, 34764932, 34764935, 34764941, 34764944, 34764947, 34764950, 34764953, 34764956, 34764959, 34764965, 34764968, 34764971, 34764980, 34764983, 34764986, 34764989, 34764992, 34764995, 34765001]
					product_name_list=['Baterda de Cocina Pards Granito<br /> 7 Pzas<br /> Rojo', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Azul', 'Baterda de Cocina Simple Cooking<br /> 5 Pzas<br /> Rojo', 'Baterda de Cocina Lisboa 6 piezas<BR>6 Pzas<BR>Cobre', 'Baterda de Cocina Lisboa<BR>5 Pzas<BR>Gris', 'Olla Pards con Tapa<br /> 28 cm<br /> Negro', 'Olla My Lovely Kitchen<br /> 18 cm<br /> Multicolor', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Rojo', 'Baterda de Cocina Victoria<BR>5 Pzas<BR>Rojo', 'Olla para Pasta My Lovely Kitchen<br /> 22 cm<br /> Negro y Dorado', 'Olla "Brunette"<br /> 5.7 L<br /> Negro', 'Baterda de Cocina Milan<BR>5 Pzas<BR>Negro', 'Baterda Versalhes<BR>5 Pzas<BR>Negro', 'Baterda de Cocina Pulido Every Day<BR>5 Pzas<BR>Plateado', 'Baterda Primaware<BR>5 Pzas<BR>Rojo', 'Juego de Sartenes "Napoli"<BR>4 Pzas<BR>Negro', 'Juego de Sartenes Chelsea<BR>3 Pzas<BR>Rojo', 'Sart9n Bergamo<BR>24 cm<BR>Azul', 'Sart9n Bergamo<BR>25 cm<BR>Azul', 'Juego de Sartenes "Loreto"<BR>2 Pzas<BR>Rojo', 'Sart9n My Lovely Kitchen<BR>22 cm<BR>Multicolor', 'Sart9n con Honda My Lovely Kitchen<BR>24 cm<BR>Dorado', 'Sart9n My Lovely Kitchen<BR>24 cm<BR>Negro y Dorado', 'Olla Wok My Lovely Kitchen<BR>32 cm<BR>Negro y Dorado', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Verde', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Azul', 'Juego de Sartenes "Brunette"<BR>2 Pzas<BR>Negro', 'Juego de Sartenes Primaware<BR>2 Pzas<BR>Negro']
					product_price_list=['$1,289.00 ', '$869.00 ', '$1,189.00 ', '$869.00 ', '$869.00 ', '$879.00 ', '$389.00 ', '$869.00 ', '$1,479.00 ', '$789.00 ', '$769.00 ', '$869.00 ', '$989.00 ', '$1,489.00 ', '$869.00 ', '$319.00 ', '$629.00 ', '$109.00 ', '$139.00 ', '$239.00 ', '$169.00 ', '$289.00 ', '$169.00 ', '$819.00 ', '$169.00 ', '$209.00 ', '$119.00 ', '$119.00 ', '$819.00 ', '$389.00 ']
					campaign_name_list=['Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina']

					stock_id_list=['80916044', '80916029', '80916053', '80916032', '80916035', '80916077', '80916089', '80916038', '80916041', '80916092', '80916113', '80916047', '80916050', '80916056', '80916059', '80916062', '80916065', '80916068', '80916071', '80916074', '80916080', '80916083', '80916086', '80916095', '80916098', '80916101', '80916104', '80916107', '80916110', '80916116']
					context_list=[10, 10, 10, 10, 10, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]

				print product_id_list
				print product_name_list
				print product_price_list
				print campaign_name_list
				print stock_id_list
				print context_list

			except:
				print "-----------------exception-------------------------------"
				product_id_list=[34764929, 34764914, 34764938, 34764917, 34764920, 34764962, 34764974, 34764923, 34764926, 34764977, 34764998, 34764932, 34764935, 34764941, 34764944, 34764947, 34764950, 34764953, 34764956, 34764959, 34764965, 34764968, 34764971, 34764980, 34764983, 34764986, 34764989, 34764992, 34764995, 34765001]
				product_name_list=['Baterda de Cocina Pards Granito<br /> 7 Pzas<br /> Rojo', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Azul', 'Baterda de Cocina Simple Cooking<br /> 5 Pzas<br /> Rojo', 'Baterda de Cocina Lisboa 6 piezas<BR>6 Pzas<BR>Cobre', 'Baterda de Cocina Lisboa<BR>5 Pzas<BR>Gris', 'Olla Pards con Tapa<br /> 28 cm<br /> Negro', 'Olla My Lovely Kitchen<br /> 18 cm<br /> Multicolor', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Rojo', 'Baterda de Cocina Victoria<BR>5 Pzas<BR>Rojo', 'Olla para Pasta My Lovely Kitchen<br /> 22 cm<br /> Negro y Dorado', 'Olla "Brunette"<br /> 5.7 L<br /> Negro', 'Baterda de Cocina Milan<BR>5 Pzas<BR>Negro', 'Baterda Versalhes<BR>5 Pzas<BR>Negro', 'Baterda de Cocina Pulido Every Day<BR>5 Pzas<BR>Plateado', 'Baterda Primaware<BR>5 Pzas<BR>Rojo', 'Juego de Sartenes "Napoli"<BR>4 Pzas<BR>Negro', 'Juego de Sartenes Chelsea<BR>3 Pzas<BR>Rojo', 'Sart9n Bergamo<BR>24 cm<BR>Azul', 'Sart9n Bergamo<BR>25 cm<BR>Azul', 'Juego de Sartenes "Loreto"<BR>2 Pzas<BR>Rojo', 'Sart9n My Lovely Kitchen<BR>22 cm<BR>Multicolor', 'Sart9n con Honda My Lovely Kitchen<BR>24 cm<BR>Dorado', 'Sart9n My Lovely Kitchen<BR>24 cm<BR>Negro y Dorado', 'Olla Wok My Lovely Kitchen<BR>32 cm<BR>Negro y Dorado', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Verde', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Azul', 'Juego de Sartenes "Brunette"<BR>2 Pzas<BR>Negro', 'Juego de Sartenes Primaware<BR>2 Pzas<BR>Negro']
				product_price_list=['$1,289.00 ', '$869.00 ', '$1,189.00 ', '$869.00 ', '$869.00 ', '$879.00 ', '$389.00 ', '$869.00 ', '$1,479.00 ', '$789.00 ', '$769.00 ', '$869.00 ', '$989.00 ', '$1,489.00 ', '$869.00 ', '$319.00 ', '$629.00 ', '$109.00 ', '$139.00 ', '$239.00 ', '$169.00 ', '$289.00 ', '$169.00 ', '$819.00 ', '$169.00 ', '$209.00 ', '$119.00 ', '$119.00 ', '$819.00 ', '$389.00 ']
				campaign_name_list=['Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina']

				stock_id_list=['80916044', '80916029', '80916053', '80916032', '80916035', '80916077', '80916089', '80916038', '80916041', '80916092', '80916113', '80916047', '80916050', '80916056', '80916059', '80916062', '80916065', '80916068', '80916071', '80916074', '80916080', '80916083', '80916086', '80916095', '80916098', '80916101', '80916104', '80916107', '80916110', '80916116']
				context_list=[10, 10, 10, 10, 10, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]

			if len(product_id_list)<3 or len(product_name_list)<3:
				product_id_list=[34764929, 34764914, 34764938, 34764917, 34764920, 34764962, 34764974, 34764923, 34764926, 34764977, 34764998, 34764932, 34764935, 34764941, 34764944, 34764947, 34764950, 34764953, 34764956, 34764959, 34764965, 34764968, 34764971, 34764980, 34764983, 34764986, 34764989, 34764992, 34764995, 34765001]
				product_name_list=['Baterda de Cocina Pards Granito<br /> 7 Pzas<br /> Rojo', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Azul', 'Baterda de Cocina Simple Cooking<br /> 5 Pzas<br /> Rojo', 'Baterda de Cocina Lisboa 6 piezas<BR>6 Pzas<BR>Cobre', 'Baterda de Cocina Lisboa<BR>5 Pzas<BR>Gris', 'Olla Pards con Tapa<br /> 28 cm<br /> Negro', 'Olla My Lovely Kitchen<br /> 18 cm<br /> Multicolor', 'Baterda de Cocina Bergamo<BR>5 Pzas<BR>Rojo', 'Baterda de Cocina Victoria<BR>5 Pzas<BR>Rojo', 'Olla para Pasta My Lovely Kitchen<br /> 22 cm<br /> Negro y Dorado', 'Olla "Brunette"<br /> 5.7 L<br /> Negro', 'Baterda de Cocina Milan<BR>5 Pzas<BR>Negro', 'Baterda Versalhes<BR>5 Pzas<BR>Negro', 'Baterda de Cocina Pulido Every Day<BR>5 Pzas<BR>Plateado', 'Baterda Primaware<BR>5 Pzas<BR>Rojo', 'Juego de Sartenes "Napoli"<BR>4 Pzas<BR>Negro', 'Juego de Sartenes Chelsea<BR>3 Pzas<BR>Rojo', 'Sart9n Bergamo<BR>24 cm<BR>Azul', 'Sart9n Bergamo<BR>25 cm<BR>Azul', 'Juego de Sartenes "Loreto"<BR>2 Pzas<BR>Rojo', 'Sart9n My Lovely Kitchen<BR>22 cm<BR>Multicolor', 'Sart9n con Honda My Lovely Kitchen<BR>24 cm<BR>Dorado', 'Sart9n My Lovely Kitchen<BR>24 cm<BR>Negro y Dorado', 'Olla Wok My Lovely Kitchen<BR>32 cm<BR>Negro y Dorado', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n + Esp1tula Breakfast<BR>2 Pzas<BR>Multicolor', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Verde', 'Sart9n para Huevos + Esp1tula de Nylon<BR>3 Pzas<BR>Azul', 'Juego de Sartenes "Brunette"<BR>2 Pzas<BR>Negro', 'Juego de Sartenes Primaware<BR>2 Pzas<BR>Negro']
				product_price_list=['$1,289.00 ', '$869.00 ', '$1,189.00 ', '$869.00 ', '$869.00 ', '$879.00 ', '$389.00 ', '$869.00 ', '$1,479.00 ', '$789.00 ', '$769.00 ', '$869.00 ', '$989.00 ', '$1,489.00 ', '$869.00 ', '$319.00 ', '$629.00 ', '$109.00 ', '$139.00 ', '$239.00 ', '$169.00 ', '$289.00 ', '$169.00 ', '$819.00 ', '$169.00 ', '$209.00 ', '$119.00 ', '$119.00 ', '$819.00 ', '$389.00 ']
				campaign_name_list=['Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina', 'Tramontina']

				stock_id_list=['80916044', '80916029', '80916053', '80916032', '80916035', '80916077', '80916089', '80916038', '80916041', '80916092', '80916113', '80916047', '80916050', '80916056', '80916059', '80916062', '80916065', '80916068', '80916071', '80916074', '80916080', '80916083', '80916086', '80916095', '80916098', '80916101', '80916104', '80916107', '80916110', '80916116']
				context_list=[10, 10, 10, 10, 10, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]

			temp=[]
			for i in range(random.randint(1,3)):
				temp.append(product_id_list[i])

			app_data['index']=random.randint(0,len(temp)-1)

			

			if random.randint(1,100)<=85:

				time.sleep(random.randint(20,45))

				adjust_token_kz9hjw(campaign_data, app_data, device_data,e1,e2)

				time.sleep(random.randint(20,30))
				adjust_token_pkwzq0(campaign_data, app_data, device_data,e1,e2)

				if random.randint(1,100)<=90:

					# print "\nself_call_productsheet\n"
					# request=productsheet( campaign_data, device_data, app_data )
					# util.execute_request(**request)


					time.sleep(random.randint(10,25))

					adjust_token_w1b3pq(campaign_data, app_data, device_data,e1,e2)

					if random.randint(1,100)<=90:
						print "\nself_call_shoppingcart\n"
						request=shoppingcart( campaign_data, device_data, app_data )
						output=util.execute_request(**request)
						try:
							app_data['cartID']=json.loads(output.get('data')).get('data').get('cart').keys()[0]

							if app_data.get('cartID')=='' or app_data.get('cartID')==None:
								app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

							print app_data['cartID']
						except:
							app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

						if app_data.get('cartID')=='' or app_data.get('cartID')==None:
							app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

						time.sleep(random.randint(15,20))

						adjust_token_bnrwg8(campaign_data, app_data, device_data,e1,e2)
						app_data['count']+=1

						if random.randint(1,100)<=90:
						
							time.sleep(random.randint(10,20))

							adjust_token_x3aw91(campaign_data, app_data, device_data,e1,e2)

							if random.randint(1,100)<=70:

								time.sleep(random.randint(40,60))

								adjust_token_xvc4tu(campaign_data, app_data, device_data,e1,e2)
								app_data['count']=0
								criteo=[]
								app_data['amount']=0

			if random.randint(1,100)<=90:

				time.sleep(random.randint(20,30))

				adjust_token_pkwzq0(campaign_data, app_data, device_data,e1,e2)

				if random.randint(1,100)<=90:

					# print "\nself_call_productsheet\n"
					# request=productsheet( campaign_data, device_data, app_data )
					# util.execute_request(**request)


					time.sleep(random.randint(10,25))

					adjust_token_w1b3pq(campaign_data, app_data, device_data,e1,e2)

					if random.randint(1,100)<=90:
						print "\nself_call_shoppingcart\n"
						request=shoppingcart( campaign_data, device_data, app_data )
						output=util.execute_request(**request)
						try:
							app_data['cartID']=json.loads(output.get('data')).get('data').get('cart').keys()[0]

							if app_data.get('cartID')=='' or app_data.get('cartID')==None:
								app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

							print app_data['cartID']
						except:
							app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

						if app_data.get('cartID')=='' or app_data.get('cartID')==None:
							app_data['cartID']='PRMX-8b143daf-47ed-4ab2-870f-d2ac53a9e556'

						time.sleep(random.randint(15,20))

						adjust_token_bnrwg8(campaign_data, app_data, device_data,e1,e2)
						app_data['count']+=1

						if random.randint(1,100)<=80:
						
							time.sleep(random.randint(10,20))

							adjust_token_x3aw91(campaign_data, app_data, device_data,e1,e2)

							if random.randint(1,100)<=60:

								time.sleep(random.randint(40,60))

								adjust_token_xvc4tu(campaign_data, app_data, device_data,e1,e2)
								app_data['count']=0
								criteo=[]
								app_data['amount']=0
	set_appCloseTime(app_data)
	return {'status':'true'}

def getBanners( campaign_data, device_data, app_data ):
	url= "https://mex.privalia.com/private/getBanners/zones/37970,37980,37990"
	method= "get"
	headers= {       "Accept": "*/*",
        "Accept-Encoding": "gzip;q=1.0, compress;q=0.5",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+";q=1.0, ja-DE;q=0.9, ar-DE;q=0.8, fr-FR;q=0.7, fr-DE;q=0.6, de-DE;q=0.5",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": campaign_data.get('app_name')+"/"+campaign_data.get('app_version_name')+" ("+campaign_data.get('package_name')+"; build:"+campaign_data.get('app_version_code')+"; iOS "+device_data.get('os_version')+") Alamofire/"+campaign_data.get('app_version_name'),
        "x-privalia-app": "app",
        "x-privalia-app-version": campaign_data.get('app_version_name'),
        "x-privalia-device": "smartphone",
        "x-privalia-platform-system": "iOS",
        "x-privalia-platform-system-version": device_data.get('os_version')}

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def registerProcess( campaign_data, device_data, app_data ):
	register_user(app_data,country='united states')
	url= "https://mex.privalia.com/public/registerProcess"
	method= "post"
	headers= {       "Accept": "*/*",
        "Accept-Encoding": "gzip;q=1.0, compress;q=0.5",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+";q=1.0, ja-DE;q=0.9, ar-DE;q=0.8, fr-FR;q=0.7, fr-DE;q=0.6, de-DE;q=0.5",
        "Content-Type": "application/x-www-form-urlencoded",
        "Cookie": "PRIVALIASESSID_mex="+app_data.get('PRIVALIASESSID_mex')+"; TS01b98eb7="+app_data.get('TS01b98eb7')+"; TS01fa3430="+app_data.get('TS01fa3430'),
        "User-Agent": campaign_data.get('app_name')+"/"+campaign_data.get('app_version_name')+" ("+campaign_data.get('package_name')+"; build:"+campaign_data.get('app_version_code')+"; iOS "+device_data.get('os_version')+") Alamofire/"+campaign_data.get('app_version_name'),
        "x-privalia-app": "app",
        "x-privalia-app-version": campaign_data.get('app_version_name'),
        "x-privalia-device": "smartphone",
        "x-privalia-platform-system": "iOS",
        "x-privalia-platform-system-version": device_data.get('os_version')}

	params= None

	data= {       "addMember": "true",
        "adv_checkbox": "off",
        "member_email": app_data.get('user').get('email'),
        "member_godfather_code": "",
        "member_lastname_1": "surname",
        "member_lastname_2": "middleName",
        "member_name": app_data.get('user').get('username'),
        "member_password": app_data.get('user').get('password'),
        "member_password_confirm": app_data.get('user').get('password'),
        "member_publisher_code": "mobile:nm:organic:direct:privalia.com:iphoneapp::",
        "member_sex": "unknown",
        "members_hs_call": "iph!39_8Uit5wK",
        "register_accept": "true"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def login( campaign_data, device_data, app_data ):
	url= "https://mex.privalia.com/auth/login"
	method= "post"
	headers= {       "Accept": "*/*",
        "Accept-Encoding": "gzip;q=1.0, compress;q=0.5",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+";q=1.0, ja-DE;q=0.9, ar-DE;q=0.8, fr-FR;q=0.7, fr-DE;q=0.6, de-DE;q=0.5",
        "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
        "Cookie": "FC_PRIVALIA_S_mex="+app_data.get('FC_PRIVALIA_S_mex')+"; PRIVALIASESSID_mex="+app_data.get('PRIVALIASESSID_mex')+"; TS01170bf2="+app_data.get('TS01170bf2')+"; TS01b98eb7="+app_data.get('TS01b98eb7')+"; session_timestamp="+app_data.get('session_timestamp')+"; watsonAutoLogin="+app_data.get('watsonAutoLogin')+"; watsonRegistered="+app_data.get('watsonRegistered')+"; TS01fa3430="+app_data.get('TS01fa3430')+"; advertisingThirdParties=false; memberjustregistered=true",
        "User-Agent": campaign_data.get('app_name')+"/"+campaign_data.get('app_version_name')+" ("+campaign_data.get('package_name')+"; build:"+campaign_data.get('app_version_code')+"; iOS "+device_data.get('os_version')+") Alamofire/"+campaign_data.get('app_version_name'),
        "x-privalia-app": "app",
        "x-privalia-app-version": campaign_data.get('app_version_name'),
        "x-privalia-context": "",
        "x-privalia-device": "smartphone",
        "x-privalia-platform-system": "iOS",
        "x-privalia-platform-system-version": device_data.get('os_version')}

	params= None

	data= {       "login_type": "md5",
        "member_login_email": app_data.get('user').get('email'),
        "member_login_password": util.md5(app_data.get('user').get('password')),
        "source": "mobile"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def member( campaign_data, device_data, app_data ):
	url= "https://mex.privalia.com/api/user/account/v1/member/"+app_data.get('watsonAutoLogin')
	method= "post"
	headers= {       "Accept": "*/*",
        "Accept-Encoding": "gzip;q=1.0, compress;q=0.5",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+";q=1.0, ja-DE;q=0.9, ar-DE;q=0.8, fr-FR;q=0.7, fr-DE;q=0.6, de-DE;q=0.5",
        "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
        "Cookie": "TS01fa3430="+app_data.get('TS01fa3430')+"; PRIVALIASESSID_mex="+app_data.get('PRIVALIASESSID_mex')+"; TS01b98eb7="+app_data.get('TS01b98eb7')+"; FC_PRIVALIA_S_mex="+app_data.get('FC_PRIVALIA_S_mex')+"; TS01170bf2="+app_data.get('TS01170bf2')+"; session_timestamp="+app_data.get('session_timestamp')+"; watsonAutoLogin="+app_data.get('watsonAutoLogin')+"; watsonRegistered="+app_data.get('watsonRegistered')+"; advertisingThirdParties=false; memberjustregistered=true",
        "User-Agent": campaign_data.get('app_name')+"/"+campaign_data.get('app_version_name')+" ("+campaign_data.get('package_name')+"; build:"+campaign_data.get('app_version_code')+"; iOS "+device_data.get('os_version')+") Alamofire/"+campaign_data.get('app_version_name'),
        "x-method": "PUT",
        "x-privalia-app": "app",
        "x-privalia-app-version": campaign_data.get('app_version_name'),
        "x-privalia-device": "smartphone",
        "x-privalia-platform-system": "iOS",
        "x-privalia-platform-system-version": device_data.get('os_version')}

	params= None

	if app_data.get('user').get('sex')=='male':
		gen='m'
	else:
		gen='f'

	data= {       "gender":gen }

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def menu( campaign_data, device_data, app_data ):
	url= "https://mex.privalia.com/api/menu/v2"
	method= "get"
	headers= {       "Accept": "*/*",
        "Accept-Encoding": "gzip;q=1.0, compress;q=0.5",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+";q=1.0, ja-DE;q=0.9, ar-DE;q=0.8, fr-FR;q=0.7, fr-DE;q=0.6, de-DE;q=0.5",
        "Cookie": "TS01fa3430="+app_data.get('TS01fa3430')+"; PRIVALIASESSID_mex="+app_data.get('PRIVALIASESSID_mex')+"; TS01b98eb7="+app_data.get('TS01b98eb7')+"; FC_PRIVALIA_S_mex="+app_data.get('FC_PRIVALIA_S_mex')+"; TS01170bf2="+app_data.get('TS01170bf2')+"; session_timestamp="+app_data.get('session_timestamp')+"; watsonAutoLogin="+app_data.get('watsonAutoLogin')+"; watsonRegistered="+app_data.get('watsonRegistered')+"; advertisingThirdParties=false; memberjustregistered=true",
        "User-Agent": campaign_data.get('app_name')+"/"+campaign_data.get('app_version_name')+" ("+campaign_data.get('package_name')+"; build:"+campaign_data.get('app_version_code')+"; iOS "+device_data.get('os_version')+") Alamofire/"+campaign_data.get('app_version_name'),
        "x-privalia-app": "app",
        "x-privalia-app-version": campaign_data.get('app_version_name'),
        "x-privalia-device": "smartphone",
        "x-privalia-platform-system": "iOS",
        "x-privalia-platform-system-version": device_data.get('os_version')}

	params= {       "private": ""}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def getCategory( campaign_data, device_data, app_data ):

	app_data['category_id']=random.choice(category_id_list)

	url= "https://mex.privalia.com/microsites/getCategoryTree/campaign/"+str(app_data.get('category_id'))
	method= "get"
	headers= {       "Accept": "*/*",
        "Accept-Encoding": "gzip;q=1.0, compress;q=0.5",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+";q=1.0, ja-DE;q=0.9, ar-DE;q=0.8, fr-FR;q=0.7, fr-DE;q=0.6, de-DE;q=0.5",
        "Content-Type": "application/x-www-form-urlencoded",
        "Cookie": "TS01fa3430="+app_data.get('TS01fa3430')+"; ADRUM_BT="+app_data.get('ADRUM_BT')+"; PRIVALIASESSID_mex="+app_data.get('PRIVALIASESSID_mex')+"; TS01b98eb7="+app_data.get('TS01b98eb7')+"; FC_PRIVALIA_S_mex="+app_data.get('FC_PRIVALIA_S_mex')+"; session_timestamp="+app_data.get('session_timestamp')+"; watsonAutoLogin="+app_data.get('watsonAutoLogin')+"; watsonRegistered="+app_data.get('watsonRegistered'),
        "User-Agent": campaign_data.get('app_name')+"/"+campaign_data.get('app_version_name')+" ("+campaign_data.get('package_name')+"; build:"+campaign_data.get('app_version_code')+"; iOS "+device_data.get('os_version')+") Alamofire/"+campaign_data.get('app_version_name'),
        "x-privalia-app": "app",
        "x-privalia-app-version": campaign_data.get('app_version_name'),
        "x-privalia-device": "smartphone",
        "x-privalia-platform-system": "iOS",
        "x-privalia-platform-system-version": device_data.get('os_version')}

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def getProducts( campaign_data, device_data, app_data ):
	
	app_data['sub_category_id']=random.choice(sub_category_id_list)
	if not app_data.get('sub_category_name'):
		app_data['sub_category_name']=random.choice(sub_category_name_list)
	url= "https://mex.privalia.com/microsites/getProducts/campaign/"+str(app_data.get('category_id'))+"/category/"+str(app_data.get('sub_category_id'))+"/order/1/block/1"
	method= "post"
	headers= {       "Accept": "*/*",
        "Accept-Encoding": "gzip;q=1.0, compress;q=0.5",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+";q=1.0, ja-DE;q=0.9, ar-DE;q=0.8, fr-FR;q=0.7, fr-DE;q=0.6, de-DE;q=0.5",
        "Content-Type": "application/x-www-form-urlencoded",
        "Cookie": "TS01fa3430="+app_data.get('TS01fa3430')+"; ADRUM_BT="+app_data.get('ADRUM_BT')+"; PRIVALIASESSID_mex="+app_data.get('PRIVALIASESSID_mex')+"; TS01b98eb7="+app_data.get('TS01b98eb7')+"; FC_PRIVALIA_S_mex="+app_data.get('FC_PRIVALIA_S_mex')+"; session_timestamp="+app_data.get('session_timestamp')+"; watsonAutoLogin="+app_data.get('watsonAutoLogin')+"; watsonRegistered="+app_data.get('watsonRegistered'),
        "User-Agent": campaign_data.get('app_name')+"/"+campaign_data.get('app_version_name')+" ("+campaign_data.get('package_name')+"; build:"+campaign_data.get('app_version_code')+"; iOS "+device_data.get('os_version')+") Alamofire/"+campaign_data.get('app_version_name'),
        "x-privalia-app": "app",
        "x-privalia-app-version": campaign_data.get('app_version_name'),
        "x-privalia-device": "smartphone",
        "x-privalia-platform-system": "iOS",
        "x-privalia-platform-system-version": device_data.get('os_version')}

	params= None

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

# def productsheet( campaign_data, device_data, app_data ):
# 	url= "https://es.privalia.com/api/catalog/productsheet/v1/product/"+str(product_id_list[app_data.get('index')])
# 	method= "get"
# 	headers= {       "Accept": "*/*",
#         "Accept-Encoding": "gzip;q=1.0, compress;q=0.5",
#         "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+";q=1.0, ja-DE;q=0.9, ar-DE;q=0.8, fr-FR;q=0.7, fr-DE;q=0.6, de-DE;q=0.5",
#         "Content-Type": "application/x-www-form-urlencoded",
#         "Cookie": "TS01fa3430="+app_data.get('TS01fa3430')+"; ADRUM_BT="+app_data.get('ADRUM_BT')+"; PRIVALIASESSID_mex="+app_data.get('PRIVALIASESSID_mex')+"; TS01b98eb7="+app_data.get('TS01b98eb7')+"; FC_PRIVALIA_S_mex="+app_data.get('FC_PRIVALIA_S_mex')+"; session_timestamp="+app_data.get('session_timestamp')+"; watsonAutoLogin="+app_data.get('watsonAutoLogin')+"; watsonRegistered="+app_data.get('watsonRegistered'),
#         "User-Agent": campaign_data.get('app_name')+"/"+campaign_data.get('app_version_name')+" ("+campaign_data.get('package_name')+"; build:"+campaign_data.get('app_version_code')+"; iOS "+device_data.get('os_version')+") Alamofire/"+campaign_data.get('app_version_name'),
#         "x-privalia-app": "app",
#         "x-privalia-app-version": campaign_data.get('app_version_name'),
#         "x-privalia-device": "smartphone",
#         "x-privalia-platform-system": "iOS",
#         "x-privalia-platform-system-version": device_data.get('os_version')}

# 	params= None

# 	data= None
# 	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def shoppingcart( campaign_data, device_data, app_data ):
	url= "https://mex.privalia.com/api/account/shoppingcart/v1/cart/0"
	method= "post"
	headers= {       "Accept": "*/*",
        "Accept-Encoding": "gzip;q=1.0, compress;q=0.5",
        "Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+";q=1.0, ja-DE;q=0.9, ar-DE;q=0.8, fr-FR;q=0.7, fr-DE;q=0.6, de-DE;q=0.5",
        "Content-Type": "application/x-www-form-urlencoded",
        "Cookie": "TS01fa3430="+app_data.get('TS01fa3430')+"; PRIVALIASESSID_mex="+app_data.get('PRIVALIASESSID_mex')+"; TS01b98eb7="+app_data.get('TS01b98eb7')+"; FC_PRIVALIA_S_mex="+app_data.get('FC_PRIVALIA_S_mex')+"; session_timestamp="+app_data.get('session_timestamp')+"; watsonAutoLogin="+app_data.get('watsonAutoLogin')+"; watsonRegistered="+app_data.get('watsonRegistered')+"",
        "User-Agent": campaign_data.get('app_name')+"/"+campaign_data.get('app_version_name')+" ("+campaign_data.get('package_name')+"; build:"+campaign_data.get('app_version_code')+"; iOS "+device_data.get('os_version')+") Alamofire/"+campaign_data.get('app_version_name'),
        "x-privalia-app": "app",
        "x-privalia-app-version": campaign_data.get('app_version_name'),
        "x-privalia-device": "smartphone",
        "x-privalia-platform-system": "iOS",
        "x-privalia-platform-system-version": device_data.get('os_version')}

	params= None

	data= {       "context": '10', "product": str(stock_id_list[app_data.get('index')]), "quantity": "1"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def adjust_token_soci2a(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________COMPLETED_REGISTRATION'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"userID":str(app_data.get('watsonAutoLogin')),"country":country_lists[app_data.get('n')],"registrationMethod":"email"})),partner_params=None,event_token='soci2a',t1=t1,t2=t2)
	util.execute_request(**request)
	

def adjust_token_vfxzkk(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________COMPLETED_REGISTRATION'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"userID":str(app_data.get('watsonAutoLogin')),"loginMethod":"auto","country":country_lists[app_data.get('n')]})),partner_params=None,event_token='vfxzkk',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_pkwzq0(campaign_data, app_data, device_data,t1=0,t2=0):
	

	print 'Adjust : EVENT______________________________VIEWED_PRODUCT_LISTING'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"userID":str(app_data.get('watsonAutoLogin')),"categoryName":app_data.get('sub_category_name'),"categoryID":str(app_data.get('sub_category_id'))})),partner_params=str(json.dumps({"criteo_email_hash":util.md5(app_data.get('user').get('email')),"criteo_p":urllib.quote(str(temp)),"ui_status":"repeat"})),event_token='pkwzq0',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_bnrwg8(campaign_data, app_data, device_data,t1=0,t2=0):
	
	
	print 'Adjust : EVENT______________________________ADDED_TO_CART'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"productName":product_name_list[app_data.get('index')].encode('utf-8'),"currency":currency_list[app_data.get('n')],"amount":product_price_list[app_data.get('index')].split('.')[0][1:],"userID":app_data.get('watsonAutoLogin'),"productID":str(product_id_list[app_data.get('index')])})),partner_params=None,event_token='bnrwg8',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_x3aw91(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________VIEWED_SHOPPING_CART'

	cart_dict={"i":stock_id_list[app_data.get('index')],"pr":product_price_list[app_data.get('index')][1:],"q":1}


	if ',' in product_price_list[app_data.get('index')][1:]:
		print "find comma"
		app_data['amount']+=float(product_price_list[app_data.get('index')][1:].replace(',',''))
	else:
		print "not found comma"
		app_data['amount']+=float(product_price_list[app_data.get('index')][1:])

	criteo.append(cart_dict)
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"amount":str(app_data.get('amount')),"currency":currency_list[app_data.get('n')],"cartID":app_data.get('cartID'),"numItems":str(app_data.get('count')),"userID":app_data.get('watsonAutoLogin')})),partner_params=str(json.dumps({"criteo_email_hash":util.md5(app_data.get('user').get('email')),"criteo_p":urllib.quote(str(criteo)),"ui_status":"repeat"})),event_token='x3aw91',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_xvc4tu(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________INITIATED_CHECKOUT'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"amount":product_price_list[app_data.get('index')][1:],"currency":currency_list[app_data.get('n')],"numItems":str(app_data.get('count')),"userID":app_data.get('watsonAutoLogin')})),partner_params=None,event_token='xvc4tu',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_w1b3pq(campaign_data, app_data, device_data,t1=0,t2=0):
	app_data['index']=random.randint(0,len(temp)-1)
	print 'Adjust : EVENT_____________________________VIEWED_PRODUCT'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"productName":product_name_list[app_data.get('index')].encode('utf-8'),"currency":currency_list[app_data.get('n')],"amount":product_price_list[app_data.get('index')].split('.')[0][1:],"userID":app_data.get('watsonAutoLogin'),"productID":str(product_id_list[app_data.get('index')])})),partner_params=str(json.dumps({"criteo_email_hash":util.md5(app_data.get('user').get('email')),"criteo_p":product_id_list[app_data.get('index')],"ui_status":"repeat"})),event_token='w1b3pq',t1=t1,t2=t2)
	util.execute_request(**request)

def adjust_token_kz9hjw(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________VIEWED_CAMPAIGN'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"campaignName":str(campaign_name_list[app_data.get('index')]),"campaignId":str(app_data.get('category_id')),"userID":app_data.get('watsonAutoLogin')})),partner_params=None,event_token='kz9hjw',t1=t1,t2=t2)
	util.execute_request(**request)


###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0,isOpen=False,callback_params=None,partner_params=None):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	make_session_count(app_data,device_data)
	app_data['subsession_count'] = 0
	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		# "Authorization" : CHECK AUTH,
		"Client-Sdk" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {

		}
	data = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_updated_at" : campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
		"app_version" : campaign_data.get('app_version_code'),
		"app_version_short" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"bundle_id" : campaign_data.get('package_name'),
		"connectivity_type" : "2",
		"country" : device_data.get('locale').get('country').upper(),
		"cpu_type" : device_data.get('cpu_type'),
		"created_at" : created_at,
		"device_name" : device_data.get('device_platform'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : 0,
		"hardware_name" : device_data.get('hardware'),
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"install_receipt" : install_receipt(),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : 1,
		"network_type" : "CTRadioAccessTechnologyHSDPA",
		"os_build" : device_data.get('build'),
		"os_name" : "ios",
		"os_version" : device_data.get('os_version'),
		"persistent_ios_uuid" : app_data.get('ios_uuid'),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count'),
		"tracking_enabled" : "1",

		}
	if isOpen:
		def_(app_data,'subsession_count')
		# def_sessionLength(app_data)
		data['last_interval'] = get_lastInterval(app_data)
		if app_data.get('session_Length'):
			data['session_length'] = app_data.get('session_Length')
			data['time_spent'] = app_data.get('session_Length')
		else:
			data['session_length'] = 0
			data['time_spent'] = 0
		data['subsession_count'] = app_data.get('subsession_count')
		


	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_sdkclick(campaign_data, app_data, device_data,click_time='',source='',t1=0,t2=0,callback_params=None,partner_params=None):
	sdk_clk_time=get_date(app_data,device_data)
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	def_(app_data,'subsession_count')
	time_spent=int(time.time())-app_data.get('adjust_call_time')
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		"Client-Sdk" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {

		}
	data = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_updated_at" : campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
		"app_version" : campaign_data.get('app_version_code'),
		"app_version_short" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : "1",
		"bundle_id" : campaign_data.get('package_name'),
		"connectivity_type" : "2",
		"country" : device_data.get('locale').get('country').upper(),
		"cpu_type" : device_data.get('cpu_type'),
		"created_at" : created_at,
		"details" : json.dumps({"Version3.1":{"iad-attribution":"false"}}),
		"device_name" : device_data.get('device_platform'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : 0,
		"hardware_name" : device_data.get('hardware'),
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"install_receipt" : install_receipt(),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"language" : device_data.get('locale').get('language'),
		"last_interval" : get_lastInterval(app_data),
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : 1,
		"network_type" : "CTRadioAccessTechnologyHSDPA",
		"os_build" : device_data.get('build'),
		"os_name" : "ios",
		"os_version" : device_data.get('os_version'),
		"persistent_ios_uuid" : app_data.get('ios_uuid'),
		"sent_at" : sent_at,
		"session_count" : app_data.get('session_count',1),
		"session_length" : time_spent,
		"source" : source,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : 1,

		}
	if source=='iad3':
		if data.get('deeplink'):
			del data['deeplink']
		if data.get('click_time'):
			del data['click_time']
		data['details']=json.dumps({"Version3.1":{"iad-attribution":"false"}})

	if source=='deeplink' and click_time:
		if not data.get('deeplink'):
			data['deeplink']="CONSTANT"
			data['click_time']='sdk_clk_time'





	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'click')
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	



###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,t1=0,t2=0,initiated_by=''):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		# "Authorization" : CHECK AUTH,
		"Client-Sdk" : campaign_data.get('adjust').get('sdk'),
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"attribution_deeplink" : "1",
		"created_at" : created_at,
		"environment" : "production",
		"event_buffering_enabled" : 0,
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"initiated_by" : initiated_by,
		"needs_response_details" : 1,
		"sent_at" : sent_at,

		}
	data = {

		}

	
	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('idfa'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	time_spent=int(time.time())-app_data.get('adjust_call_time')
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Accept" : "*/*",
		"Accept-Encoding" : "br, gzip, deflate",
		"Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		# "Authorization" : CHECK AUTH,
		"Client-Sdk" : campaign_data.get('adjust').get('sdk'),
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

		}
	params = {

		}
	data = {
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_code'),
		"app_version_short" : campaign_data.get('app_version_name'),
		"attribution_deeplink" : 1,
		"bundle_id" : campaign_data.get('package_name'),
		"callback_params" : callback_params,
		"connectivity_type" : 2,
		"country" : device_data.get('locale').get('country'),
		"cpu_type" : device_data.get('cpu_type'),
		"created_at" : created_at,
		"device_name" : device_data.get('device_platform'),
		"device_type" : device_data.get('device_type'),
		"environment" : "production",
		"event_buffering_enabled" : 0,
		"event_count" : app_data.get('event_count'),
		"event_token" : event_token,
		"hardware_name" : device_data.get('hardware'),
		"idfa" : app_data.get('idfa_id'),
		"idfv" : app_data.get('idfv_id'),
		"install_receipt" : install_receipt(),
		"language" : device_data.get('locale').get('language'),
		"mcc" : device_data.get('mcc'),
		"mnc" : device_data.get('mnc'),
		"needs_response_details" : 1,
		"network_type" : "CTRadioAccessTechnologyHSDPA",
		"os_build" : device_data.get('build'),
		"os_name" : "ios",
		"os_version" : device_data.get('os_version'),
		"persistent_ios_uuid" : app_data.get('ios_uuid'),
		"sent_at" : sent_at,
		"session_count" : str(app_data.get('session_count')),
		"session_length" : time_spent,
		"subsession_count" : app_data.get('subsession_count'),
		"time_spent" : time_spent,
		"tracking_enabled" : "1",
		'partner_params':partner_params

		}

	app_data['session_Length']=time_spent

	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'event')

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':None, 'data': data}





	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('hex',64)



def get_auth(device_data,app_data,created_at,idfa,activity_type):
	data1= str(campaign_data['adjust']['secret_key']+activity_type+idfa+created_at)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'	

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date
# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0


	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')



def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		# app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		# app_data['user']['firstname'] 	= first_name
		# app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['password'] 		= util.get_random_string('char_all',6)+"@"+util.get_random_string('decimal',1)

def install_receipt():
	return 'MIISpwYJKoZIhvcNAQcCoIISmDCCEpQCAQExCzAJBgUrDgMCGgUAMIICSAYJKoZIhvcNAQcBoIICOQSCAjUxggIxMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMAwCAQ4CAQEEBAICAI0wDQIBCwIBAQQFAgMGb/MwDQIBDQIBAQQFAgMB1YgwDgIBAQIBAQQGAgQXiU7NMA4CAQkCAQEEBgIEUDI1MzAOAgEQAgEBBAYCBDGUmUswEAIBDwIBAQQIAgYe+SmyQXkwFAIBAAIBAQQMDApQcm9kdWN0aW9uMBYCAQMCAQEEDgwMMjAxOTA2MjcxNjMzMBYCARMCAQEEDgwMMjAxODA5MjUxMjIyMBgCAQQCAQIEEB4fI8j3NxTMMzJODMHePwIwHAIBBQIBAQQUV1xGqNhTelO/tBR1uYL+qIBc3pwwHgIBCAIBAQQWFhQyMDE5LTA3LTEwVDEwOjA2OjIxWjAeAgEMAgEBBBYWFDIwMTktMDctMTBUMTA6MDY6MjFaMB4CARICAQEEFhYUMjAxOC0wOS0yNlQwODozNjoyMFowHwIBAgIBAQQXDBVjb20ucHJpdmFsaWEucHJpdmFsaWEwSwIBBwIBAQRDg89I9/Ghq4DbXYLZSMMMqY4s/b8qapVf3+yIWf8hH1Mfm5IoEFAaIQ6xTG8Xze8WuRyu7n/kP5X2sLqkryvkCb+5STBOAgEGAgEBBEaAajd8wfuqtSBS5SkIisusjt6v7XRZ52M0KQd8UNVuU1qQBry4kEOOQs5gxQ5X7bgCsg72CLWCABiaT6ARBcdIJZRmaYc6oIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQBl2kqHlGHJ6vM/f4iKDEfn5vLz4KmNkhyx0JiQBLZnuwwgSQ943TrBpFc+7dIX7ZeaDUT9q3bqO2CCnY+/3LNOM8e+ctCy0csWsEP1gnyUP64gE3Cnud4Lp/9jMt+c21t9zv72M48Q3/j/qspISmK41Y5QBE5oByNhXuebt+ej0hgjnkMBQ++qbrFV2jm1B+sm/XGhchJPwNgwbv8AaHvk/qMlJCb6TDM3M68t/io5NgFVNOHKt79bbcilIXOrlnUX2k2+rRl01MJ0u1DjDFfIs4zgxkt38g3xPabo0lH7CCCOIluoR4/IG55QbGN8hWRh7J1V0EqmwfLlWxv4fGDN'
