# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import util,purchase
from sdk import NameLists,installtimenew
from sdk import getsleep
import uuid, time, binascii, json, random, urllib, hashlib, string, datetime
import Config, clicker

campaign_data = {
	'app_size'			 : 16.0,#17.0,#19.0,#18.0,#17.0,
	'package_name' : 'br.com.sbf.centauro',
	'app_name': 'Centauro',
	'app_version_code' : '11099',#'11087',#'11080',#'11077',#'11074',#'11071',#'11059',#'11046',#'11038',#'11025',#'10660',#'10514',
	'app_version_name': '1.9.21',#'1.9.20',#'1.9.19',#'1.9.18',#'1.9.17',#'1.9.16',#'1.9.15',#'1.9.14',#'1.9.13',#'1.9.12',#'1.6.0',#'1.5.1',
	'supported_countries': 'WW',
	'device_targeting':True,
	'CREATE_DEVICE_MODE':3,
	'ctr':6,
	'supported_os':'4.4',
	'no_referrer': True,
	'tracker'	:'appsflyer',
	'appsflyer':{
		'key': '2fgWyXjcGYuK3in5PVsQZC',
		'buildnumber': '4.10.3',#'4.10.2',#'4.8.20',#'4.8.12',#'6.0',
		'version'	 : 'v4',
		'dkh' : '2fgWyXjc',
	},
	'api_centauro':{
		'x-cv-id': '14',
		'Authorization':'Basic TW9iaWxlQXBwTTpjN2I1OTJhNg==',
		'x-client-token': 'db0cb287-a4ec-448e-8b26-81fbf1699de1',
		'CPF':'293.511.106-05',
		'ua':'okhttp/2.5.0'
	},
	'accengage':
			{
			'version':	'A3.5.4',
			'partnerId':'centauro15c440abfe64dc2'
			},

	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention':{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:20,
		21:19,
		22:18,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5,
	}		
}
		
def install(app_data, device_data, day=1):
	print "please wait..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	time.sleep(random.randint(15,25))

	#################INITIALIZE################
	app_data['registeredFlag'] = False
	app_data['InitialFlag'] = False
	app_data['cart'] = "["
	app_data['query'] = random.choice(['puma','adidas','Nike', 'Asics', 'Oakley', 'Mizuno', 'Converse', 'Olympikus', 'Reebok', 'Lacoste', 'Timberland'])
	battery_level(app_data)
	def_deviceFingerPrintId(app_data)
	def_installTime(app_data)
	def_launchCounter(app_data)
	def_googleToken(app_data)
	purchaseInitiated = False
	
	if not app_data.get('insdate'):
		app_data['insdate']=str(int(time.time())-random.randint(5,1000))	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	if not app_data.get('matId'):	
		app_data['matId']=str(uuid.uuid4())
			
	if not app_data.get('uid'):
		app_data['uid']=str(int(time.time()*1000))+'-'+str(random.randint(1000000000000000000,9999999999999999999))
		
	if not app_data.get('UserId'):
		app_data['UserId']='3176'+str(random.randint(400000000000000,800000000000000))
		
		

	#################*******CALLS*******################
	
	# print '--------------------------Accengage Initial---------------------'
	# appsflyerTrack=initial_accengage_self(campaign_data, app_data, device_data)
	# util.execute_request(**appsflyerTrack)	

	# print '--------------------------Accengage access---------------------'
	# appsflyerTrack=access_accengage_self(campaign_data, app_data, device_data)
	# util.execute_request(**appsflyerTrack)

	# print '-------------------------Api self configuracoes-------------------------------------'
	# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2.1/configuracoes")
	# result=util.execute_request(**appsflyerApi)
	# app_data['InitialFlag'] = False

	# print '-------------------------Api self /homes/android-------------------------------------'
	# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v3/homes/android")
	# result=util.execute_request(**appsflyerApi)
	get_codigosModelosCores(app_data,type="home")


	print '------------------------Track Appsflyer---------------------'
	appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true")
	app_data['api_hit_time'] = time.time()
	util.execute_request(**appsflyerTrack)

	print '------------------------Track Appsflyer---------------------'
	appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**appsflyerTrack)
	
	print '-------------------------Api Appsflyer-------------------------------------'
	appsflyerApi=appsflyer_api(campaign_data, app_data, device_data)
	result=util.execute_request(**appsflyerApi)

	try:
		app_data['installAttribution']=result.get('data')
	except:
		print "Error: Installation Attribute not present"

	# print '-------------------------Api self precos-------------------------------------'
	# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v3/precos")
	# result=util.execute_request(**appsflyerApi)
	fetchproductList(device_data,app_data)

	print '-------------------------Register Appsflyer-------------------------------------'
	appsflyerApi=appsflyer_register(campaign_data,app_data,device_data)
	result=util.execute_request(**appsflyerApi)

	# print '-------------------------Api self configuracoes-------------------------------------'
	# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2.1/configuracoes")
	# result=util.execute_request(**appsflyerApi)

	if random.randint(1,100)<=90:

		print '--------------------------Events Appsflyer af_search---------------------'
		eventValue='{"af_content_type":"search","af_success":true,"af_date_a":"'+get_params_date(app_data,device_data)+'","af_search_string":"'+str(app_data.get('query'))+'"}'
		appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_search',eventValue)
		util.execute_request(**appsflyerTrack)	

		# print '-------------------------centauro_SEARCH-------------------------------------'
		# appsflyerApi=centauro_search(campaign_data, app_data, device_data)
		# result=util.execute_request(**appsflyerApi)
		get_codigosModelosCores(app_data,type="search")

		# print '-------------------------Api self precos-------------------------------------'
		# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v3/precos")
		# result=util.execute_request(**appsflyerApi)
		fetchproductList(device_data,app_data)				

		#########   af_list_view
		print '--------------------------Events Appsflyer af_list_view---------------------'
		eventValue='{"af_currency":"BRL","product":'+str(app_data.get('productList')).replace("'", '"')+'}'
		appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_list_view',eventValue)
		util.execute_request(**appsflyerTrack)	
		
		# print '-------------------------Api self selos-------------------------------------'
		# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2.2/selos")
		# result=util.execute_request(**appsflyerApi)
		
		# print '-------------------------Api self produtos-------------------------------------'
		choice = str(random.choice(app_data.get('productList'))).replace("'", '"')
		# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v4/produtos/"+choice[:len(choice)-2],choice=choice)
		# result1=util.execute_request(**appsflyerApi)
		fetch_product(app_data,choice=choice)

		if random.randint(1,100)<=70:
			print '------------------------Track Appsflyer---------------------'
			appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**appsflyerTrack)


		###########   af_content_view	
		print '--------------------------Events Appsflyer af_content_view---------------------'
		eventValue=app_data.get('contentView')
		appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_content_view',eventValue)
		util.execute_request(**appsflyerTrack)

		if random.randint(1,100)<=67:

			time.sleep(random.randint(3,7))

			print '------------------------Track Appsflyer---------------------'
			appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**appsflyerTrack)

			#########  af_cart
			print '--------------------------Events Appsflyer af_cart---------------------'
			eventValue='{"product":'+app_data.get('cart')+'}'
			appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_cart',eventValue)
			util.execute_request(**appsflyerTrack)

			# print '-------------------------Api self-------------------------------------'
			# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2/validacao/cpf",method="post")
			# result=util.execute_request(**appsflyerApi)

			# print '-------------------------Api self consultacep-------------------------------------'
			# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2/consultacep/41180095")
			# result=util.execute_request(**appsflyerApi)
			# try:
			# 	d=json.loads(result.get('data'))
			# 	app_data['cidade'] = str(d.get('cidade'))
			# 	app_data['bairro'] = str(d.get('bairro'))
			# 	app_data['cep'] = str(d.get('cep'))
			# 	app_data['logradouro'] = str(d.get('logradouro'))
			# 	app_data['uf'] = str(d.get('uf'))
			# except:
			app_data['cidade'] = "SALVADOR"
			app_data['bairro'] = "SABOEIRO"
			app_data['cep'] = "41180095"
			app_data['logradouro'] = "TRAVESSA 8 DE ABRIL"
			app_data['uf'] = "BA"
			check(app_data)

			# print '-------------------------Api self validacao/email-------------------------------------'
			# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2/validacao/email",method="post")
			# result=util.execute_request(**appsflyerApi)

			# ####REGISTERR SELF
			# print '-------------------------Api REGISTERR SELF-------------------------------------'
			# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v3/clientes",method="post")
			# result=util.execute_request(**appsflyerApi)
			

			# ######LOGIN SELF
			# print '-------------------------Api LOGIN SELF-------------------------------------'
			# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2.1/clientes/login",method="post")
			# result=util.execute_request(**appsflyerApi)

			# print '-------------------------Api self clientes-------------------------------------'
			# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v3/clientes")
			# result=util.execute_request(**appsflyerApi)
			
			
			##############      af_complete_registration
			print '--------------------------Events Appsflyer af_complete_registration---------------------'
			eventValue='{"af_registration_screen":""}'
			appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_complete_registration',eventValue)
			util.execute_request(**appsflyerTrack)

			###############     af_cart
			print '--------------------------Events Appsflyer af_cart---------------------'
			eventValue='{"product":'+app_data.get('cart')+'}'
			appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_cart',eventValue)
			util.execute_request(**appsflyerTrack)
			app_data['registeredFlag'] = True

			# print '-------------------------Api self endereco-------------------------------------'
			# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2/endereco")
			# result=util.execute_request(**appsflyerApi)
			# try:
			# 	app_data['EID'] = str(json.loads(result.get('data'))[len(json.loads(result.get('data')))-1].get('id'))
			# except:
			# 	app_data['EID'] = "22792042"

			if not app_data.get('EID') or app_data.get('EID')==None or app_data.get('EID')=="":
				app_data['EID'] = "22792042"

			if random.randint(1,100)<=87:


				#####################    af_initiated_checkout
				print '--------------------------Events Appsflyer af_initiated_checkout---------------------'
				getTotalCartPrice(app_data)
				eventValue='{"af_currency":"BRL","product":'+app_data.get('cart')+',"af_price":'+app_data.get('cartTotalSum')+'}'
				appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_initiated_checkout',eventValue)
				util.execute_request(**appsflyerTrack)

				# print '-------------------------Api self checkout/entregas-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v4/checkout/entregas")
				# result=util.execute_request(**appsflyerApi)

				# print '-------------------------Api self checkout/entregas-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v4/checkout/entregas",method="post")
				# result=util.execute_request(**appsflyerApi)

				# print '-------------------------Api self checkout/pagamentos-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v4/checkout/pagamentos")
				# util.execute_request(**appsflyerApi)
				# util.execute_request(**appsflyerApi)

				# print '-------------------------Api self checkout/processamentopedido-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v4/checkout/processamentopedido",method="post")
				# result=util.execute_request(**appsflyerApi)
				purchaseInitiated = True

	if purchaseInitiated == True:
		##############   af_purchase
		if purchase.isPurchase(app_data,day,advertiser_demand=10):
			make_purchase(campaign_data, app_data, device_data)
					
					
	print '--------------------------Stats Appsflyer---------------------'
	appsflyerTrack=appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**appsflyerTrack)
	
	return {'status':True}
	
def open(app_data, device_data, day=1):

	#################INITIALIZE################
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	if not app_data.get('registeredFlag'):
		app_data['registeredFlag'] = False
	if not app_data.get('InitialFlag'):
		app_data['InitialFlag'] = False
	if not app_data.get('cart'):
		app_data['cart'] = "["
	if not app_data.get('query'):
		app_data['query'] = random.choice(['puma','adidas','Nike', 'Asics', 'Oakley', 'Mizuno', 'Converse', 'Olympikus', 'Reebok', 'Lacoste', 'Timberland'])
	purchaseInitiated = False
	battery_level(app_data)
	def_deviceFingerPrintId(app_data)
	def_installTime(app_data)
	def_launchCounter(app_data)
	def_googleToken(app_data)
	
	if not app_data.get('insdate'):
		app_data['insdate']=str(int(time.time())-random.randint(5,1000))	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	if not app_data.get('matId'):	
		app_data['matId']=str(uuid.uuid4())
			
	if not app_data.get('uid'):
		app_data['uid']=str(int(time.time()*1000))+'-'+str(random.randint(1000000000000000000,9999999999999999999))
		
	if not app_data.get('UserId'):
		app_data['UserId']='3176'+str(random.randint(400000000000000,800000000000000))
		

	#################*******CALLS*******################
	
	# if app_data.get('InitialFlag') == False:

	# 	print '--------------------------Accengage Initial---------------------'
	# 	appsflyerTrack=initial_accengage_self(campaign_data, app_data, device_data)
	# 	util.execute_request(**appsflyerTrack)	

	# 	print '--------------------------Accengage access---------------------'
	# 	appsflyerTrack=access_accengage_self(campaign_data, app_data, device_data)
	# 	util.execute_request(**appsflyerTrack)

		# print '-------------------------Api self configuracoes-------------------------------------'
		# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2.1/configuracoes")
		# result=util.execute_request(**appsflyerApi)
		# app_data['InitialFlag'] = False

	# print '-------------------------Api self /homes/android-------------------------------------'
	# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v3/homes/android")
	# result=util.execute_request(**appsflyerApi)
	get_codigosModelosCores(app_data,type="home")

	print '------------------------Track Appsflyer---------------------'
	appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**appsflyerTrack)

	# print '-------------------------Api self precos-------------------------------------'
	# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v3/precos")
	# result=util.execute_request(**appsflyerApi)
	fetchproductList(device_data,app_data)

	# print '-------------------------Api self configuracoes-------------------------------------'
	# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2.1/configuracoes")
	# result=util.execute_request(**appsflyerApi)

	if random.randint(1,100)<=90:
		print '--------------------------Events Appsflyer af_search---------------------'
		eventValue='{"af_content_type":"search","af_success":true,"af_date_a":"'+get_params_date(app_data,device_data)+'","af_search_string":"'+str(app_data.get('query'))+'"}'
		appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_search',eventValue)
		util.execute_request(**appsflyerTrack)	

		# print '-------------------------centauro_SEARCH-------------------------------------'
		# appsflyerApi=centauro_search(campaign_data, app_data, device_data)
		# result=util.execute_request(**appsflyerApi)
		get_codigosModelosCores(app_data,type="search")

		# print '-------------------------Api self precos-------------------------------------'
		# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v3/precos")
		# result=util.execute_request(**appsflyerApi)
		fetchproductList(device_data,app_data)				

		#########   af_list_view
		print '--------------------------Events Appsflyer af_list_view---------------------'
		eventValue='{"af_currency":"BRL","product":'+str(app_data.get('productList')).replace("'", '"')+'}'
		appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_list_view',eventValue)
		util.execute_request(**appsflyerTrack)	
		
		# print '-------------------------Api self selos-------------------------------------'
		# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2.2/selos")
		# result=util.execute_request(**appsflyerApi)
		
		# print '-------------------------Api self produtos-------------------------------------'
		choice = str(random.choice(app_data.get('productList'))).replace("'", '"')
		# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v4/produtos/"+choice[:len(choice)-2],choice=choice)
		# result1=util.execute_request(**appsflyerApi)
		fetch_product(app_data,choice=choice)

		if random.randint(1,100)<=70:
			print '------------------------Track Appsflyer---------------------'
			appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**appsflyerTrack)


		###########   af_content_view	
		print '--------------------------Events Appsflyer af_content_view---------------------'
		eventValue=app_data.get('contentView')
		appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_content_view',eventValue)
		util.execute_request(**appsflyerTrack)

		if random.randint(1,100)<=67:

			print '------------------------Track Appsflyer---------------------'
			appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**appsflyerTrack)

			#########  af_cart
			print '--------------------------Events Appsflyer af_cart---------------------'
			eventValue='{"product":'+app_data.get('cart')+'}'
			appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_cart',eventValue)
			util.execute_request(**appsflyerTrack)

			# print '-------------------------Api self-------------------------------------'
			# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2/validacao/cpf",method="post")
			# result=util.execute_request(**appsflyerApi)

			if app_data.get('registeredFlag')==False:

				time.sleep(random.randint(3,7))

				# print '-------------------------Api self consultacep-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2/consultacep/41180095")
				# result=util.execute_request(**appsflyerApi)
				# try:
				# 	d=json.loads(result.get('data'))
				# 	app_data['cidade'] = str(d.get('cidade'))
				# 	app_data['bairro'] = str(d.get('bairro'))
				# 	app_data['cep'] = str(d.get('cep'))
				# 	app_data['logradouro'] = str(d.get('logradouro'))
				# 	app_data['uf'] = str(d.get('uf'))
				# except:
				app_data['cidade'] = "SALVADOR"
				app_data['bairro'] = "SABOEIRO"
				app_data['cep'] = "41180095"
				app_data['logradouro'] = "TRAVESSA 8 DE ABRIL"
				app_data['uf'] = "BA"
				check(app_data)

				# print '-------------------------Api self validacao/email-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2/validacao/email",method="post")
				# result=util.execute_request(**appsflyerApi)

				# ####REGISTERR SELF
				# print '-------------------------Api REGISTERR SELF-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v3/clientes",method="post")
				# result=util.execute_request(**appsflyerApi)
				

				# ######LOGIN SELF
				# print '-------------------------Api LOGIN SELF-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2.1/clientes/login",method="post")
				# result=util.execute_request(**appsflyerApi)

				# print '-------------------------Api self clientes-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v3/clientes")
				# result=util.execute_request(**appsflyerApi)
				
				
				##############      af_complete_registration
				print '--------------------------Events Appsflyer af_complete_registration---------------------'
				eventValue='{"af_registration_screen":""}'
				appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_complete_registration',eventValue)
				util.execute_request(**appsflyerTrack)

				###############     af_cart
				print '--------------------------Events Appsflyer af_cart---------------------'
				eventValue='{"product":'+app_data.get('cart')+'}'
				appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_cart',eventValue)
				util.execute_request(**appsflyerTrack)
				app_data['registeredFlag'] = True

			# print '-------------------------Api self endereco-------------------------------------'
			# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v2/endereco")
			# result=util.execute_request(**appsflyerApi)
			# try:
			# 	app_data['EID'] = str(json.loads(result.get('data'))[len(json.loads(result.get('data')))-1].get('id'))
			# except:
			# 	app_data['EID'] = "22792042"
			if not app_data.get('EID') or app_data.get('EID')==None or app_data.get('EID')=="":
				app_data['EID'] = "22792042"

			if random.randint(1,100)<=87:


				#####################    af_initiated_checkout
				print '--------------------------Events Appsflyer af_initiated_checkout---------------------'
				getTotalCartPrice(app_data)
				eventValue='{"af_currency":"BRL","product":'+app_data.get('cart')+',"af_price":'+app_data.get('cartTotalSum')+'}'
				appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_initiated_checkout',eventValue)
				util.execute_request(**appsflyerTrack)

				# print '-------------------------Api self checkout/entregas-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v4/checkout/entregas")
				# result=util.execute_request(**appsflyerApi)

				# print '-------------------------Api self checkout/entregas-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v4/checkout/entregas",method="post")
				# result=util.execute_request(**appsflyerApi)

				# print '-------------------------Api self checkout/pagamentos-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v4/checkout/pagamentos")
				# util.execute_request(**appsflyerApi)
				# util.execute_request(**appsflyerApi)

				# print '-------------------------Api self checkout/processamentopedido-------------------------------------'
				# appsflyerApi=api_centauro_appsbnet_br(campaign_data, app_data, device_data,url="v4/checkout/processamentopedido",method="post")
				# result=util.execute_request(**appsflyerApi)
				purchaseInitiated = True

	if purchaseInitiated == True:
		##############   af_purchase
		if purchase.isPurchase(app_data,day,advertiser_demand=10):
			make_purchase(campaign_data, app_data, device_data)

	print '--------------------------Stats Appsflyer---------------------'
	appsflyerTrack=appsflyer_stats(campaign_data, app_data, device_data)
	util.execute_request(**appsflyerTrack)
		
	
	return {'status':True}


#######################################################
#
#
#			Purchase
#
#######################################################

def make_purchase(campaign_data, app_data, device_data):

	print '--------------------------Events Appsflyer af_complete_registration---------------------'
	eventValue='{"af_success":true}'
	appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_add_payment_info',eventValue)
	util.execute_request(**appsflyerTrack)

	randomP = random.randint(1,6)

	if randomP<=4:
		print '--------------------------Events Appsflyer Purchase---------------------'
		eventValue='{"af_payment_method":"MASTERCARD (1x)","af_receipt_id":'+"9132"+util.get_random_string('decimal',5)+',"af_currency":"BRL","product":'+app_data.get('cart')+',"af_revenue":'+app_data.get('cartTotalSum')+"00000000"+str(random.randint(0,9))+'}'
		appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_purchase',eventValue)
		util.execute_request(**appsflyerTrack)
		clearCart(app_data)

	elif randomP>4 and randomP<=10:
		print '--------------------------Events Appsflyer Purchase---------------------'
		eventValue=u'{"af_payment_method":"BOLETO BANCÁRIO (1x)",'.encode('utf-8', 'ignore').decode('utf-8')+'"af_receipt_id":'+"9132"+util.get_random_string('decimal',5)+',"af_currency":"BRL","product":'+app_data.get('cart')+',"af_revenue":'+app_data.get('cartTotalSum')+'}'
		appsflyerTrack=appsflyer_event(campaign_data, app_data, device_data,'af_purchase',eventValue)
		util.execute_request(**appsflyerTrack)
		clearCart(app_data)


#######################################################
#
#
#			Self Calls
#
#######################################################


def api_centauro_appsbnet_br(campaign_data, app_data, device_data,url=None,choice=None,method="get"):
	url1 = "http://api.centauro.appsbnet.com.br/"+url  #https
	header={
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' ('+device_data.get('model')+'; '+device_data.get('os_version')+' API '+device_data.get('sdk')+')',
		'x-client-UserAgent': 'android',
		'x-cv-id': campaign_data.get('api_centauro').get('x-cv-id'),
		'Authorization': campaign_data.get('api_centauro').get('Authorization'),
		'x-client-token': campaign_data.get('api_centauro').get('x-client-token'),
		'Accept-Encoding': 'gzip',
		'Content-Type': 'application/json; charset=UTF-8',
		'Connection': 'Keep-Alive'

	}
	data = {}
	params=None
	if url == "v3/precos":
		header['x-coupon-token'] = ""
		params = {
			'withPromotion':	'true',
			'padraoApenas':	'true',
			'codigosModelosCores':app_data.get('codigosModelosCores'),
			'codigoCupomDesconto': ""	
		}
	if url=="v2.2/selos":
		params = {
		'codigosModelosCor':app_data.get('codigosModelosCores'),
		}
	if url.split('/')[1] =="produtos":
		header['x-coupon-token'] = ""
		params={
				'codigoCupomDesconto':"",	
				'cor': choice[len(choice)-2:len(choice)]
				}
	if url=="v2/validacao/email":
		register_user(app_data,country='united states')
		data=json.dumps({"Email":app_data.get('user').get('email')})

	if url=="v2/validacao/cpf":
		data=json.dumps({"CPF":campaign_data.get('api_centauro').get('CPF')})

	if url=="v3/clientes" and method=="post":
		data = json.dumps({"Endereco":{"TelefoneAdicional":{"Numero":app_data.get('user').get('Numero'),"DDD":app_data.get('user').get('Numero')[0:2]},"Cidade":app_data.get('cidade'),"Complemento":"","Bairro":app_data.get('bairro'),"TelefoneFixo":{"Numero":"","DDD":""},"Numero":util.get_random_string('decimal',4),"NomeDeQuemIraReceber":"","CEP":app_data.get('cep'),"UF":app_data.get('uf'),"referencia":app_data.get('user').get('firstname'),"Logradouro":app_data.get('logradouro')},"DataDeNascimento":app_data.get('user').get('dob'),"Email":app_data.get('user').get('email'),"Sexo":app_data.get('user').get('sex'),"Sobrenome":"kunamzh","Nome":app_data.get('user').get('firstname'),"Newsletter":'true',"Senha":app_data.get('user').get('senha'),"CPF":campaign_data.get('api_centauro').get('CPF').strip('.').strip('-'),"RG":util.get_random_string("decimal",9),"IpOrigem":"","UrlOrigem":"","TipoCliente":"PF"})

	if url=="v2.1/clientes/login":
		data = json.dumps({"usuario":app_data.get('user').get('email'),"ManterLogado":'true',"senha":app_data.get('user').get('senha')})

	if url=="v4/checkout/entregas":
		if method=="post":
			data = json.dumps({"EnderecoId":app_data.get('EID'),"GruposEndereco":[{"TipoEntrega":'1',"DiasDisponibilidadeRetira":'0',"PeriodoId":'0',"Itens":[{"ItemCarrinhoId":app_data.get('af_content_color'),"Sku":app_data.get('sku')}]}]})
		del header['Content-Type']
	
	if url=="v4/checkout/pagamentos":
		params = {'habilitarcompraRapida':'true'}
		del header['Content-Type']

	if url == "v4/checkout/processamentopedido" and method=="post":
		data = json.dumps({"TesteAbDescontoBoleto":'true',"MesCartao":'0',"AnoCartao":'0',"NomeCartao":"boleto","Newsletter":'false',"UtilizarPagamentoOnline":'true',"ParceiroId":0,"OpcaoPagamento":"boleto","CodigoEspecie":"BO","UtilizarCartaoCompraRapida":'false',"UtilizarCartao":'false',"MesCartaoCliente":'0',"AnoCartaoCliente":'0'})

	return {'url': url1, 'httpmethod': method, 'headers': header, 'params': params, 'data': data}




def initial_accengage_self(campaign_data, app_data, device_data):
	url1 = "http://api1.accengage.com/routes"   #https
	header={
		'Accept-Encoding': 'gzip',
		'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('brand')+' '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
		'Content-Type': 'text/xml;charset=utf-8'
	}
	data = {}
	params = {
		'version':campaign_data.get('accengage').get('version'),
		'sharedId':app_data.get('matId'),
		'partnerId':campaign_data.get('accengage').get('partnerId'),	
	}
	return {'url': url1, 'httpmethod': 'get', 'headers': header, 'params': params, 'data': data}


def access_accengage_self(campaign_data, app_data, device_data):
	url1 = "http://api.accengage.com/v1/sdk_access_token"  #https
	header={
		'Accept-Encoding': 'gzip',
		'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('brand')+' '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
		'Content-Type': 'application/json;charset=utf-8',
		'Accengage-Signature': util.get_random_string('hex',40),
		'Accengage-Time': str(int(time.time()))	
	}
	data = {"partnerId":campaign_data.get('accengage').get('partnerId'),"deviceId":{"androidid":device_data.get('android_id'),"idfv":app_data.get('matId')},"sharedId":app_data.get('matId')}
	params = None
	return {'url': url1, 'httpmethod': 'post', 'headers': header, 'params': params, 'data': json.dumps(data)}


def centauro_search(campaign_data, app_data, device_data):
	url = "http://centauro.resultspage.com/search"  #https
	header={
		'Accept-Encoding': 'gzip',
		'User-Agent': campaign_data.get('api_centauro').get('ua')

	}
	data = {}
	params = {
		'p':'Q',
		'cnt':'20',
		'rk':'1',
		'w':app_data.get('query'),
		'af':'',	
		'srt':'0',
		'showmarketplace':'1',
		'ts':'json-full'	
	}
	return {'url': url, 'httpmethod': 'get', 'headers': header, 'params': params, 'data': data}

###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch') and type(app_data.get('timepassedsincelastlaunch'))==int:
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "no",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"sensors" : [{u'sN': u'LSM6DB0 Gyroscope Sensor', u'sVE': [1.719168, -0.318681, 0.735042], u'sV': u'STMicroelectronics', u'sVS': [-0.19658099, -0.888888, -0.694749], u'sT': 4}, {u'sN': u'LSM6DB0 Magnetometer Sensor', u'sVE': [14.64, -94.86001, -11.580001], u'sV': u'STMicroelectronics', u'sVS': [23.04, -86.58, -13.68], u'sT': 2}, {u'sN': u'LSM6DB0 Accelerometer Sensor', u'sVE': [-2.0379841, 10.899148, 14.09486], u'sV': u'STMicroelectronics', u'sVS': [0.997464, 6.7298923, 7.844564], u'sT': 1}],
		},
		# "deviceRm" : device_data.get('build'),
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		# "installer_package" : "com.android.chrome",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"open_referrer" : 'android-app://com.android.vending',
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"registeredUninstall" : False,
		"rfr" : {
								"clk" : str(int(app_data.get("times").get("click_time"))),
								"code" : "0",
								"install" : "0",
								"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",
		},
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"tokenRefreshConfigured" : False,
		"uid" : app_data.get('uid'),
		"p_receipt" : util.get_random_string('google_token',571).replace('-','+').replace('_','/')+"=",
		'sc_o':'p',
		"installer_package":"com.android.vending",
		}			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
		data["user_emails"] = json.dumps({"plain_el_arr":[""]})	
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)

	if app_data.get('counter')>2:
		if data.get('rfr'):
			del data['rfr']
		if data.get('deviceData').get('sensors'):
			del data['deviceData']['sensors']	
		if data.get('p_receipt'):
			del data['p_receipt']	
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))	
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
 	def_(app_data,'counter')
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"app_name" : campaign_data.get('app_name'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand').upper(),
		"carrier" : device_data.get('carrier'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"device_id" : app_data.get('uid'),
		"devkey" : campaign_data.get('appsflyer').get('key'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,eventName="",eventValue=""):
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		# "deviceRm" : device_data.get('build'),
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		# "installer_package" : "com.android.chrome",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"registeredUninstall" : True,
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),
		"user_emails" : json.dumps({"plain_el_arr":[""]}),
		'sc_o':'p',
		"installer_package":"com.android.vending",
		"tokenRefreshConfigured" : False,
		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
	
def appsflyer_stats(campaign_data, app_data, device_data):

	url='https://stats.appsflyer.com/stats'   
	
	header={
		'Content-Type': 'application/json',
		'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('brand')+' '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
		'Accept-Encoding': 'gzip'
	}
	params={}	
	
	data = {
		'advertiserId': device_data.get('adid'),
		'app_id': campaign_data.get('package_name'),
		'channel':'(null)',
		'deviceFingerPrintId': app_data.get('deviceFingerPrintId'),
		'devkey': campaign_data.get('appsflyer').get('key'),
		'gcd_conversion_data_timing':'7',
		'launch_counter': str(app_data.get('launch_counter')),
		'originalAppsflyerId': app_data.get('uid'),
		'platform': 'Android',
		'statType': 'user_closed_app',
		'time_in_app': str(int(str(int((time.time()*1000)))[:11])-int(app_data.get('install_time')[:11])),
		'uid':app_data.get('uid')
	}
	
	app_data['launch_counter'] = app_data.get('launch_counter')+2
	
	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
	
def diet_dietaesaude(campaign_data, app_data, device_data):
	url='http://api.dietaesaude.com.br/tokenissuer/v2/dietaesaude'  #https
	method='get'
	headers = {
		'Authorization': campaign_data.get('dietandhealth').get('authorization'),
		'Accept-Language': device_data.get('locale').get('language'),
		'User-Agent': 'Android/'+device_data.get('os_version'),
		'App-Name': campaign_data.get('dietandhealth').get('app_name'),
		'App-Version': campaign_data.get('app_version_name'),
		'Content-Type': 'application/json',
		'Accept-Encoding': 'gzip'
	}
	params = {}
	data = {}
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################
def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
		
def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100

	if n3<10:
		n3 = '0'+str(n3)
		
	sb = insert_char(sb,23,str(n3))
	return sb



###################################################################
# appsflyer_kef_key and value generator
# parameter 			: brand, sdk, lang, af_ts, buildnumber, etc
#
# Generates kef key and value for appsflyer.
###################################################################
global temp, counter
temp = 0
counter = 1

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
	if buildnumber=="4.8.18":
		return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
	else:
		c = '\u0000'
		obj2 = af_ts[::-1]
		out = calculate(sdk, obj2, brand)
		i = len(out)
		if i > 4:
			i2 = globals()['counter']+65
			globals()['temp'] = i2 % 128
			if (1 if i2 % 2 != 0 else None) != 1:
				out.delete(4, i)
			else:
				out.delete(5, i)
		else:
			while i < 4:
				i3 = globals()['counter'] + 119
				globals()['temp'] = i3 % 128
				if (16 if i3 % 2 != 0 else 93) != 16:
					i += 1
					out+='1'
				else:
					i += 66
					out+='H'
				i3 = globals()['counter'] + 109
				globals()['temp'] = i3 % 128
				i3 %= 2
		return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
	ts = ts[::-1]
	appends = [sdk,lang,ts]
	lengths = [len(sdk),len(lang),len(ts)]
	l = sorted(lengths)
	least = l[0]
	out = ""
	for x in range(least):
		numb = None
		for y in range(len(appends)):
			charAt = ord(appends[y][x])
			if numb:
				charAt = int((charAt ^ int(numb)))

			numb = charAt
			
		out+=str(hex(numb))[2:]

	if len(out)>4:
		out = out[:4]
	elif len(out)<4:
		while len(out)<4:
			out+="1"
	
	return out

"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""

def generateValue(b,x,s,p,ts,fl,buildnumber):
	part1=generateValue1get(ts,fl,buildnumber)
	str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p,"utf-8")
	for i in range(len(str_)):
		str_[i] = int((str_[i] ^ ((i % 2) + 42)))

	stringBuilder = ""
	for toHexString in str_:
		toHexString2 = str(hex(int(toHexString)))[2:]
		if 1 == len(toHexString2):
			toHexString2 = "0"+str(toHexString2)
		stringBuilder+=toHexString2
	part2 = stringBuilder.__str__()
	return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
	gethash = sha256(ts+firstLaunch+buildnumber)
	return gethash[:16]

"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""

def calculate(sdk, obj, brand):
	allList = [sdk, obj, brand]
	arrayList = []
	i = 0
	while True:
		flag = 1
		if i >= 3:
			break
		i2 = globals()['temp'] + 63
		globals()['counter'] = i2 % 128
		if i2 % 2 != 0:
			flag = None
		if flag != None:
			arrayList.append(len(allList[i]))
			i += 31
		else:
			arrayList.append(len(allList[i]))
			i += 1
	sorted(arrayList)
	intValue = int(arrayList[0])
	out = ""
	i3 = 0
	while i3 < intValue:
		i4 = globals()['counter']+99
		globals()['temp'] = i4 % 128
		i5 =  globals()['temp']+67
		globals()['counter'] = i5 % 128
		i5 %= 2
		number = None
		if i4 % 2 != 0:
			a = 57
		else:
			a = 83
		if a != 83:
			i4 = 1
		else:
			i4 = 0
		while i4 < 3:
			i5 = globals()['temp'] + 87
			globals()['counter'] = i5 % 128
			i5 %= 2
			i5 = ord(allList[i4][i3])
			if (92 if number == None else 39) != 39:
				i6 = globals()['temp'] + 55
				globals()['counter'] = i6 % 128
				i6 %= 2
				i6 = globals()['counter'] + 39
				globals()['temp'] = i6 % 128
				i6 %= 2
			else:
				i5 ^= int(number)
			number = i5
			i4 += 1
		out+=str(hex(int(number)))[2:]
		i3 += 1
	return out


###############################################################
#
#				IMPORTANT METHODS
#		
#
##############################################################
	
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)
	
def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

###############################################################
#
#					UTILITY METHODS
#		
#
##############################################################
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def timestamp():
	return str(int(time.time()*1000))
	
def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def timeSinceLastLaunch_return(app_data):
	
	if not app_data.get('timepassedsincelastlaunch'):
		app_data['timepassedsincelastlaunch']=str(0)
		app_data['open_time']=int(time.time()*100)
	else:
		app_data['timepassedsincelastlaunch']=str(int(time.time()*100)-app_data.get('open_time'))
		app_data['open_time']=int(time.time()*100)
		
	return {'status':True}
	
def battery_level(app_data):
	if not app_data.get('batteryLevel'):
		app_data['batteryLevel'] = str(random.randint(15,100))+'.0'
	
def random_string(len):
	return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		num1 = random.randint(1,2)
		if num1==1:
			app_data['deviceFingerPrintId'] = 'ffffffff-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)
		if num1==2:
			app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-0000-0000'+random_string(8)

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"		


def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)

def time_Stamp():
	return str(int(time.time()*1000))
	
def def_installTime(app_data):
	if not app_data.get('install_time'):
		app_data['install_time'] = time_Stamp()
		
def def_launchCounter(app_data):
	if not app_data.get('launch_counter'):
		app_data['launch_counter'] = 0
		
def def_googleToken(app_data):
	if not app_data.get('google_token'):
		app_data['google_token'] = util.get_random_string('google_token',11)

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')



def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		if gender=="male":
			g ='Masculino'
		else:
			g =''
		app_data['user']				={}
		app_data['user']['sex'] 		= g
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['Numero']		= '8'+util.get_random_string('decimal',7)
		app_data['user']['senha']		= app_data.get('user').get('firstname')+util.get_random_string('decimal',4)

###############################################################
#
#				FETCHING DATA METHODS
#		
#
##############################################################

def get_codigosModelosCores(app_data,r='',type='home'):
	eflag=False
	# try:
	# 	app_data['codigosModelosCores'] = ""
	# 	if type=="search":
	# 		a = json.loads(r.get('data')).get('results')
	# 		data=[]
	# 		for i in range(len(a)):
	# 			data.append(a[i].get('sku'))
	# 		codigosModelosCores = data
	# 	if type=="home":
	# 		a = json.loads(r.get('data')).get('conteudoJson')
	# 		mCores = []
	# 		for i in range(len(a)):
	# 			if a[i].get('produtosVitrine'):
	# 				p=a[i].get('produtosVitrine')
	# 		for j in range(len(p)):
	# 			mCores.append(p[j].get('codigoModeloCor'))
	# 		codigosModelosCores = mCores
	# 	for k in range(len(codigosModelosCores)):
	# 		comma=","
	# 		if k==len(codigosModelosCores)-1:
	# 			comma=""
	# 		app_data['codigosModelosCores'] = app_data['codigosModelosCores']+codigosModelosCores[k]+comma
	# except:
	print "Exception data fetched"
	app_data['codigosModelosCores'] = random.choice(['88871129,894329I3,89799002,81891603,8904543X,8860795G,88380802,89372245,91069502','897990NF,91747562,91772731,9174752V,858039OX,89799203,88595463,89799002,91854931,918723XR,88502805,88598683,91877962,917030KM,8580393V,89347003,88678731,917267LX,884089OX,88258483','918516RQ,918481RQ,918792RQ,919553RQ,9185143U,9184503U,9187913U,918512RQ,918482RQ,918770RQ,918794RQ,918483RQ,9184833U,918727RQ,918796RQ,91873602,91873104,91873107,91851704,9187933U','89338386,89338212,M00PLV32,88200819,91027912,89338315,89525102,88553083,88214206,882138BL,88553202,88811505,89338302,88814902,89117202,88214405,88553284,88553212,89338284,87614402'])
	eflag=True

	if type=="home":
		if len(app_data.get('codigosModelosCores'))<1 or eflag==True:
			print "Exception data fetched"
			app_data['codigosModelosCores'] = '91765386,91981302,91257231,91921504,881343U0,919724WN,82122902,891759J4,89447404'

	if type=="search":
		if not app_data.get('codigosModelosCores') or len(app_data.get('codigosModelosCores'))<1:
			print "Exception data fetched"
			app_data['codigosModelosCores'] = random.choice(['88871129,894329I3,89799002,81891603,8904543X,8860795G,88380802,89372245,91069502','897990NF,91747562,91772731,9174752V,858039OX,89799203,88595463,89799002,91854931,918723XR,88502805,88598683,91877962,917030KM,8580393V,89347003,88678731,917267LX,884089OX,88258483','918516RQ,918481RQ,918792RQ,919553RQ,9185143U,9184503U,9187913U,918512RQ,918482RQ,918770RQ,918794RQ,918483RQ,9184833U,918727RQ,918796RQ,91873602,91873104,91873107,91851704,9187933U','89338386,89338212,M00PLV32,88200819,91027912,89338315,89525102,88553083,88214206,882138BL,88553202,88811505,89338302,88814902,89117202,88214405,88553284,88553212,89338284,87614402'])

def fetchproductList(device_data,app_data,r=''):
	# try:
	# 	l=json.loads(r.get('data'))
	# 	product_list = []
	# 	for i in range(len(l)):
	# 		if l[i].get('codigoModeloCor'):
	# 			product_list.append(l[i].get('codigoModeloCor').encode('utf-8'))	
	# except:
	product_list = ['91765386', '91981302', '91257231', '91921504', '881343U0', '919724WN', '82122902', '891759J4', '89447404']
	if len(product_list)<3:
		product_list = ['91765386', '91981302', '91257231', '91921504', '881343U0', '919724WN', '82122902', '891759J4', '89447404']
	app_data['productList'] = [random.choice(product_list),random.choice(product_list),random.choice(product_list)]

def fetch_product(app_data,choice,r=''):
	# try:
	# 	l=json.loads(r.get('data'))
	# 	app_data['productName'] = l.get('disponibilidade').get('nome')
	# 	app_data['productValue']  = l.get('precos')[0].get('valor').replace(',','.')
	# 	app_data['sku'] = l.get('disponibilidade').get('cores')[0].get('sku')
	# except:
	# 	app_data['productName'] = "Tênis Oakley Modoc - Masculino"
	# 	app_data['productValue']  = "389.99"
	# 	app_data['sku'] = "885532020395"
	# try:
	# 	app_data['productName'] = unicode(app_data['productName'],'utf-8')
	# except:
	# app_data['productName'] = app_data['productName'].encode('utf-8', 'ignore').decode('utf-8')
	if not app_data.get('productName') or app_data.get('productName')=="" or app_data.get('productName')==None:
		app_data['productName'] = "Tênis Oakley Modoc - Masculino"
		app_data['productValue']  = "389.99"
		app_data['sku'] = "885532020395"

 	if not app_data.get('cart')=="[":
		app_data['cart'] = app_data['cart'][:len(app_data.get('cart'))-1]+','
	app_data['contentView'] = '{"af_content_type":'+app_data.get('productName')+',"af_content_id":"'+choice[:len(choice)-2]+'","af_currency":"BRL","af_content_color":"'+choice+'","af_price":'+app_data.get('productValue')+'}'
	app_data['cart'] = app_data['cart'].encode('utf-8') +app_data.get('contentView').encode('utf-8')+']'
	app_data['af_content_color'] = choice

def getTotalCartPrice(app_data):
	totalPrice=0
	a = app_data.get('cart').split('"')
	for i in range(len(a)):
		if a[i]=='af_price':
			b=i
			price = a[b+1][1:len(a[b+1])-3]
			totalPrice=totalPrice+float(price)
	round(totalPrice,2)
	app_data['cartTotalSum'] = str(totalPrice)
	if not app_data.get('cartTotalSum'):
		app_data['cartTotalSum'] = "389.99"

def clearCart(app_data):
	app_data['cart'] = "["
	app_data['cartTotalSum'] = "0"

def check(app_data):
	if not app_data.get('cidade') or app_data.get('cidade')==None or app_data.get('cidade')=="":
		app_data['cidade'] = "SALVADOR"
		app_data['bairro'] = "SABOEIRO"
		app_data['cep'] = "41180095"
		app_data['logradouro'] = "TRAVESSA 8 DE ABRIL"
		app_data['uf'] = "BA"

def get_params_date(app_data,device_data):
	d=time.time()
	return str(datetime.datetime.utcfromtimestamp(d).strftime('%a %B %d %H:%M:%S')+' GMT '+device_data.get('timezone')+' '+ datetime.datetime.utcfromtimestamp(time.time()).strftime('%Y'))