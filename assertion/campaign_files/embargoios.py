import random,time,datetime,uuid, json
from sdk import NameLists
from sdk import getsleep
from sdk import util,installtimenew
import urllib
import clicker,Config

campaign_data = {
	'package_name' : 'com.embargoapp.embargo.ios',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'ctr':6,
	'app_name' : 'Embargo',
	'device_targeting' : True,
	'app_id':'1205758388',
	'app_version_code': '255',#'240',#'237',#'233',#'225',#'218',#'213',220
	'app_version_name':'4.1.1',#'4.0.4',#'4.0.2',#'4.0.1',#'3.2.5',#'3.2',#'3.1.1',3.2.1
	'supported_os':'10.0',
	'supported_countries': 'WW',
	'sdk' : 'ios',
	'tracker': 'adjust',
	'app_size' : 68.9,#68.7,#66.1,
	'adjust':{
				'app_token': 'gs3brfgng9vk',
				'app_secret': '4822356814742469981715364538223199297',#'112971750619090422331377114671853551437',
				'sdk': 'ios4.18.3',#'ios4.17.2',#'ios4.15.0',#'ios4.14.1',
				'app_updated_at': '2019-10-04T23:36:53.000Z',#'2019-07-17T21:46:40.000Z',#'2019-06-18T19:49:14.000Z',#'2019-05-17T03:07:42.000Z',#'2019-04-08T17:05:29.000Z',#2018-12-20T23:21:05.000Z,'2018-12-19T03:51:52.000Z',#'2018-09-12T16:27:56.000Z',
				'signature_id': '4',#'1'
			},
	'retention':{
					1:71,
					2:69,
					3:67,
					4:65,
					5:63,
					6:61,
					7:57,
					8:55,
					9:53,
					10:51,
					11:48,
					12:45,
					13:42,
					14:39,
					15:37,
					16:35,
					17:33,
					18:32,
					19:31,
					20:30,
					21:30,
					22:29,
					23:28,
					24:27,
					25:26,
					26:25,
					27:25,
					28:24,
					29:23,
					30:22,
					31:20,
					32:15,
					33:10,
					34:7,
					35:5,
	}
}


# def make_sec(app_data,device_data):	
# 	timez = device_data.get('timezone')
# 	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
# 	if int(timez) < 0:
# 		sec = (-1) * sec
# 	if not app_data.get('sec'):
# 		app_data['sec'] = sec;
		
# 	return app_data.get('sec')	
	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')
		
# def make_subsession_count(app_data,device_data):
# 	if not app_data.get('subsession_count'):
# 		app_data['subsession_count'] = 1
# 	else:
# 		app_data['subsession_count'] += 1
		
# 	return app_data.get('subsession_count')

def get_install_reciept():
	install_receipt='MIISlwYJKoZIhvcNAQcCoIISiDCCEoQCAQExCzAJBgUrDgMCGgUAMIICOAYJKoZIhvcNAQcBoIICKQSCAiUxggIhMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgCNMA0CAQMCAQEEBQwDMjU1MA0CAQoCAQEEBRYDMTcrMA0CAQ0CAQEEBQIDAdWIMA0CARMCAQEEBQwDMjE4MA4CAQECAQEEBgIER95ptDAOAgEJAgEBBAYCBFAyNTMwDgIBCwIBAQQGAgQHEShSMA4CARACAQEEBgIEMadlATAQAgEPAgEBBAgCBjlZwJMU7jAUAgEAAgEBBAwMClByb2R1Y3Rpb24wGAIBBAIBAgQQR4+e2nbFN8dhf/I39wwvPTAcAgEFAgEBBBQ39ulLFr3rPlxyrOhxpexBUDOJSTAeAgEIAgEBBBYWFDIwMTktMTAtMDlUMDY6NDM6NThaMB4CAQwCAQEEFhYUMjAxOS0xMC0wOVQwNjo0Mzo1OFowHgIBEgIBAQQWFhQyMDE4LTEyLTIxVDA1OjA2OjE1WjAkAgECAgEBBBwMGmNvbS5lbWJhcmdvYXBwLmVtYmFyZ28uaW9zMEACAQcCAQEEOEL+cjHsXz/VhwN0axjwz4cFp+6TLFV9VKdx4Bzv0M8TShn6Ur3+tYPSzrmlFD97YKUzzuPl1iuMMFQCAQYCAQEETJcs3HsF1xT26XYsDgYfEi3810av38Gi8+We21g+h8b7PA0e5dFQ6f3L/jxuPNjXORGoxGuAUw0VQPv5Zus3Stge3JEXRFzWk2WAJKyggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAGQEONUncoKIlWYqmFAcmnM71JGKKrlVoX4AgFU6jGpIxcrzUnYElrFArlV/CHOgXbzSq4PtuSXLOuscdN/iU6MbmANNqPNE3/aF5PG8g0Qu7Ns0SGMDQD/Db0QpgLDhClz5ioeeDSB+NMkt4VpoKWQvDPeZqN+def5tnObQY5bxmM/xVNps7IupbqXj1dmkKbWeo/oOs16bd0QhKsnQVuwiZpSjT3Z6AmEd/c3IlRbliDpBBysfh0aqNg6PlZAvCXlT7KKbevapO+BQpve4vPbGJrdLvh65oIhsmVte0ql+eNivE+HeGUz1R5GqvIEt6ubXz9Tz/ktGX9ueFmOWSMI='
	return install_receipt

def install(app_data, device_data):

	print 'plz wait...'
	print '*************Install**********************'

	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")
	def_sec(app_data,device_data)
	
	get_install_reciept()
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	app_data['user_registered_successfully'] = False


	time.sleep(random.randint(35,37))

	app_data['session_start_timestamp']=int(time.time())

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)


	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)	

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,t1=5,t2=7)
	util.execute_request(**adjustSession)


	if random.randint(1,100) <= 90:

		if random.randint(1,100) <= 55:

			# sign up  or fb login
			time.sleep(random.randint(50,90))
			print '-----------------------ADJUST Event------------sign up  or fb login-----------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'nfpvll')  
			util.execute_request(**adjust_event_req)

		else:

			# google login
			time.sleep(random.randint(50,90))
			print '-----------------------ADJUST Event------------google login-----------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'tqlvd8')  
			util.execute_request(**adjust_event_req)

			time.sleep(random.randint(0,2))
			print '-----------------------ADJUST Event------------google login-----------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'kgisqh')  
			util.execute_request(**adjust_event_req)
		
		app_data['user_registered_successfully'] = True


		if random.randint(1,100)<=55:
			time.sleep(random.randint(45,60))
			print '-----------------------ADJUST Event------------reward_claimed-----------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'meqzz8')  
			util.execute_request(**adjust_event_req)

			if random.randint(1,100)<=45:
				time.sleep(random.randint(10,30))
				print '-----------------------ADJUST Event------------promo applied-----------------'
				adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '2078sn')  
				util.execute_request(**adjust_event_req)


	app_data['session_end_timestamp']=int(time.time())
		
	return {'status':True}


###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):

	print '*************open**********************'

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")

	def_sec(app_data,device_data)

	get_install_reciept()

	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		if not device_data.get('idfv_id'):
			app_data['idfv_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfv_id'] = device_data.get('idfv_id')
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('session_start_timestamp'):
		app_data['session_start_timestamp']=int(time.time())

	if not app_data.get('session_end_timestamp'):
		app_data['session_end_timestamp']=int(time.time())

	if not app_data.get('user_registered_successfully'):
		app_data['user_registered_successfully'] = False


	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data, is_type = 'open')
	util.execute_request(**adjustSession)

	app_data['session_start_timestamp']=int(time.time())


	if app_data.get('user_registered_successfully')==False:

		if random.randint(1,100) <= 55:

			# sign up  or fb login
			time.sleep(random.randint(50,90))
			print '-----------------------ADJUST Event------------sign up  or fb login-----------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'nfpvll')  
			util.execute_request(**adjust_event_req)

		else:

			# google login
			time.sleep(random.randint(50,90))
			print '-----------------------ADJUST Event------------google login-----------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'tqlvd8')  
			util.execute_request(**adjust_event_req)

			time.sleep(random.randint(0,2))
			print '-----------------------ADJUST Event------------google login-----------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'kgisqh')  
			util.execute_request(**adjust_event_req)
		
		app_data['user_registered_successfully'] = True


		if random.randint(1,100)<=55:
			time.sleep(random.randint(45,60))
			print '-----------------------ADJUST Event------------reward_claimed-----------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'meqzz8')  
			util.execute_request(**adjust_event_req)

			if random.randint(1,100)<=45:
				time.sleep(random.randint(10,30))
				print '-----------------------ADJUST Event------------promo applied-----------------'
				adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = '2078sn')  
				util.execute_request(**adjust_event_req)

	else:
		if random.randint(1,100)<=25:
			time.sleep(random.randint(45,60))
			print '-----------------------ADJUST Event------------promo applied-----------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,event_token = 'meqzz8')  
			util.execute_request(**adjust_event_req)


	time.sleep(random.randint(0,5*60))
	

	app_data['session_end_timestamp']=int(time.time())

	return {'status':True}



###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,t1=0,t2=0, is_type = 'install'):
	
	
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	data = {
			 'app_token': campaign_data.get('adjust').get('app_token'),
			 'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			 'app_version':	campaign_data.get('app_version_code'),
			 'app_version_short': campaign_data.get('app_version_name'),
			 'attribution_deeplink':	1,
			 'bundle_id': campaign_data.get('package_name'),
			 'connectivity_type' : 2,
			 'country':device_data.get('locale').get('country').upper(),
			 'cpu_type':	device_data.get('cpu_type'),
			 'created_at': created_at,
			 'device_name': device_data.get('device_platform'),
			 'device_type': device_data.get('device_type'),
			 'environment': 'production',
			 'event_buffering_enabled': 0,
			 'hardware_name': device_data.get('hardware'),
			 'idfa': app_data.get('idfa_id'),
			 'idfv':	app_data.get('idfv_id'),
			 'install_receipt': get_install_reciept(),
			 'installed_at' : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			 'language':	device_data.get('locale').get('language'),
			 'needs_response_details': 1,
			 'os_build':	device_data.get('build'),
			 'os_name': 'ios',
			 'os_version': device_data.get('os_version'),
			 'persistent_ios_uuid': app_data.get('ios_uuid'),
			# 'queue_size': 1,
			 'sent_at': sent_at,
			 'session_count': make_session_count(app_data,device_data),
			 'tracking_enabled':	1,
			 'network_type'	: 'CTRadioAccessTechnologyLTE',
			 'mcc'	: device_data.get('mcc'),
			 'mnc'	: device_data.get('mnc')

			}

	if is_type == "open":
		def_(app_data,'subsession_count')
		data['last_interval'] = int(time.time())-app_data.get('session_end_timestamp')
		data['session_length'] = app_data.get('session_end_timestamp')-app_data.get('session_start_timestamp')
		data['subsession_count'] = app_data.get('subsession_count')+random.randint(4,8)
		data['time_spent'] = app_data.get('session_end_timestamp')-app_data.get('session_start_timestamp')

		
	app_data['time_passes'] = int(time.time())

	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'session')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	



def adjust_sdkclick(campaign_data, app_data, device_data,t1=0,t2=0,callback_params=False,partner_params=False, type = 'install'):
	
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	
	params={}

	created_at=get_date(app_data,device_data)
	created_at_time = int(time.time())
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	data = {
			 'app_token': campaign_data.get('adjust').get('app_token'),
			 'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			 'app_version':	campaign_data.get('app_version_code'),
			 'app_version_short': campaign_data.get('app_version_name'),
			 'attribution_deeplink':	1,
			 'bundle_id': campaign_data.get('package_name'),
			 'connectivity_type' : 2,
			 'country':device_data.get('locale').get('country').upper(),
			 'cpu_type':	device_data.get('cpu_type'),
			 'created_at': created_at,
			 'details': json.dumps({"Version3.1":{"iad-attribution":"false"}}),
			 'device_name': device_data.get('device_platform'),
			 'device_type': device_data.get('device_type'),
			 'environment': 'production',
			 'event_buffering_enabled': 0,
			 'hardware_name': device_data.get('hardware'),
			 'idfa': app_data.get('idfa_id'),
			 'idfv':	app_data.get('idfv_id'),
			 'install_receipt': get_install_reciept(),
			 'installed_at' : datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			 'language':	device_data.get('locale').get('language'),
			 'last_interval' : 1,
			 'needs_response_details': 1,
			 'os_build':	device_data.get('build'),
			 'os_name': 'ios',
			 'os_version': device_data.get('os_version'),
			 'persistent_ios_uuid': app_data.get('ios_uuid'),
			 'sent_at': sent_at,
			 'session_count':str(app_data.get('session_count',0)),
			 'source': 'iad3',
			 'subsession_count': 1,
			 'tracking_enabled': 1,
			 'time_spent':0,
			 'session_length':0,
			 'network_type'	: 'CTRadioAccessTechnologyLTE',
			 'mcc'	: device_data.get('mcc'),
			 'mnc'	: device_data.get('mnc')

			}

	# if app_data.get('time_passes'):
	# 	data['session_length'] = created_at_time - app_data.get('time_passes')
	# 	data['time_spent'] = data['session_length']
	# else:
	# 	data['time_spent'] = 0
	# 	data['session_length'] = 0

	if type == "open":
		def_(app_data,'subsession_count')
		# data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = int(time.time())-app_data.get('session_start_timestamp')
		data['subsession_count'] = app_data.get('subsession_count')+random.randint(0,2)
		data['time_spent'] = int(time.time())-app_data.get('session_start_timestamp')
		data['persistent_ios_uuid']= app_data.get('ios_uuid'),
		data['session_count']=str(app_data.get('session_count',0)),
	
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'click')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	

def adjust_attribution(campaign_data, app_data, device_data,t1=0,t2=0):

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	
	data={}

	params = {
			 'app_token': campaign_data.get('adjust').get('app_token'),
			 'attribution_deeplink' : 1,
			 'created_at': created_at,
			 'environment' : 'production',
			 'event_buffering_enabled': 0,
			 'idfa': app_data.get('idfa_id'),
			 'idfv':	app_data.get('idfv_id'),
			 'initiated_by' : 'backend',
			 'needs_response_details': 1,
			 'sent_at': sent_at,
			 'app_version':	campaign_data.get('app_version_code'),
			 'app_version_short': campaign_data.get('app_version_name'),
			 'bundle_id': campaign_data.get('package_name'),
			 'device_name': device_data.get('device_platform'),
			 'device_type': device_data.get('device_type'),
			 'os_build':	device_data.get('build'),
			 'os_name': 'ios',
			 'os_version': device_data.get('os_version'),
			 'persistent_ios_uuid': app_data.get('ios_uuid'),
			}

	headers['Authorization']=get_auth(device_data,app_data,params.get('created_at'),params.get('idfa'),'attribution')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	


def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=False, partner_params=False, purchase=False,t1=0,t2=0):
	
	created_at_event=get_date(app_data,device_data)
	created_at_time = int(time.time())
	time.sleep(random.randint(t1,t2))
	sent_at_event=get_date(app_data,device_data)
	inc_(app_data, 'event_count')	
		
	url = 'http://app.adjust.com/event'
	
	headers = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}

	data = {
			 'app_token':campaign_data.get('adjust').get('app_token'),
			 'app_version':	campaign_data.get('app_version_code'),
			 'app_version_short':campaign_data.get('app_version_name'),
			 'attribution_deeplink': 1,
			 'bundle_id':campaign_data.get('package_name'),
			 'connectivity_type' : 2,
			 'country':device_data.get('locale').get('country'),
			 'cpu_type':device_data.get('cpu_type'),
			 'created_at': created_at_event,
			 'device_name':device_data.get('device_platform'),
			 'device_type':device_data.get('device_type'),
			 'environment':'production',
			 'event_buffering_enabled':	0,
			 'event_count':app_data.get('event_count'),
			 'event_token':event_token,
			 'hardware_name': device_data.get('hardware'),	
			 'idfa':app_data.get('idfa_id'),
			 'idfv':app_data.get('idfv_id'),
			 'install_receipt': get_install_reciept(),
			 'language':	device_data.get('locale').get('language'),
			 'needs_response_details':1,
			 'os_build':	device_data.get('build'),
			 'os_name': 'ios',
			 'os_version': device_data.get('os_version'),
			 'persistent_ios_uuid': app_data.get('ios_uuid'),
			# 'queue_size': 1,
			 'sent_at': sent_at_event,
			 'session_count':str(app_data.get('session_count',0)),
			 'subsession_count': 3,
			 'tracking_enabled': 1,
			 'network_type'	: 'CTRadioAccessTechnologyLTE',
			 'mcc'	: device_data.get('mcc'),
			 'mnc'	: device_data.get('mnc')

			}

	# if app_data.get('time_passes'):
	# 	data['session_length'] = created_at_time - app_data.get('time_passes')
	# 	data['time_spent'] = data['session_length'] - random.randint(2,6)
	# else:
	# 	data['time_spent'] = 0
	# 	data['session_length'] = 0

	data['session_length']=int(time.time())-app_data.get('session_start_timestamp')
	data['time_spent']=data['session_length']

	if partner_params:
		data['partner_params']=partner_params	
	if callback_params:
		data['callback_params'] = callback_params
		
	headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'event')

	return {'url': url, 'httpmethod': 'post', 'headers': headers, 'params':None, 'data': data}	


def adjust_sdkinfo(campaign_data, app_data, device_data, t1 = 0, t2 = 0):
	pushtoken(app_data)

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	url = 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent':urllib.pathname2url(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}

	params={}

	data = {

		'app_token': campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink' : 1,
		'created_at': created_at,
		'environment' : 'production',
		'event_buffering_enabled': 0,
		'idfa': app_data.get('idfa_id'),
		'idfv':	app_data.get('idfv_id'),
		'needs_response_details': 1,
		'persistent_ios_uuid': app_data.get('ios_uuid'),
		'push_token':app_data.get('pushtoken'),
		'sent_at': sent_at,
		'source' :'push'
	}
	headers['Authorization']=headers['Authorization']=get_auth(device_data,app_data,data.get('created_at'),data.get('idfa'),'info')

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###########################################################
#						UTIL							  #
###########################################################

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

	
def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

# def set_sessionLength(app_data,forced=False,length=0):
# 	def_sessionLength(app_data,forced)
# 	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return str(date)

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('hex',64)

# def device_id(app_data):
# 	if not app_data.get('x-device-id'):
# 		app_data['x-device-id']=str(uuid.uuid4()).upper()

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"
# 		app_data['user']['phone'] 		="+971"+'-50-'+util.get_random_string('decimal',7)
# 		app_data['user']['password'] 	= app_data['user']['lastname']+random.choice(["@","!"])+app_data['user']['dob'].replace("-","") 

def get_auth(device_data,app_data,created_at,idfa,activity_type):
	data1= str(campaign_data['adjust']['app_secret']+activity_type+idfa+created_at)
	sign= util.sha256(data1)
	return 'Signature secret_id="'+campaign_data.get('adjust').get('signature_id')+'",signature="'+sign+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'	
