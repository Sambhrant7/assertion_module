import random,time,datetime,uuid,json,urllib
from sdk import util,installtimenew
import hmac,hashlib,base64,urllib
from sdk import getsleep
import clicker,Config

campaign_data = {
	'package_name' : 'ru.AutoRu',
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'ctr':6,
	'app_name' : 'AutoRu',
	'app_id':'507760450',
	'app_version_code': '12347',#'12191',#'12017',#11783, '11706',#'11579',#'11428',#'11220',#'11061',#'10897',#10802, 10635, '10402',#'10202',#'10056',#'9487',
	'app_version_name': '9.18.0',#'9.17.0',#'9.16.0',#9.14.0, '9.13.0',#'9.12.0',#'9.11.0',#'9.10.1',#'9.10.0',#'9.9.1',#9.9.0, 9.8.1, '9.7.0',#'9.6.0',#'9.5.1',#'9.3.1',
	'supported_os':'10.0',
	'app_size':236.0,#233.8,#228.3, 227.0,#226.7,#225.3,#207.3,#199.5, #198.0
	'supported_countries': 'WW',
	'device_targeting':True,
	'no_referrer' : False,
	'sdk' : 'ios',
	'tracker' : 'adjust',
	'adjust':{
				'app_token': '3xedbfy9nakv',
				'sdk': 'ios4.18.1',#'ios4.17.1',#'ios4.16.0',
				'app_updated_at' : '2019-09-18T00:16:34.000Z',#'2019-09-05T19:23:03.000Z',#'2019-08-26T16:46:06.000Z',#2019-08-08T21:15:25.000Z, '2019-08-01T20:10:23.000Z',#'2019-07-25T15:36:46.000Z',#'2019-07-15T19:45:39.000Z',#'2019-06-28T22:31:38.000Z',#'2019-06-14T22:53:37.000Z',#'2019-05-30T23:06:07.000Z',#2019-05-20T15:13:02.000Z, 2019-04-25T13:50:11.000Z, '2019-04-09T22:57:05.000Z',#'2019-03-25T21:14:17.000Z',#'2019-03-13T17:08:39.000Z',#'2019-02-04T18:22:15.000Z',
				'secret_key': '148495083913754741766088309221891767379',
				'secret_id':'1',
			},
	'yandex':{
				'version_name':'3.6.0',#'3.5.0',#'2.98',
				'version_code':'298',
				'sdk':'13847',#'11459',
	},		
	'retention':{
					1:65,
					2:63,
					3:60,
					4:58,
					5:55,
					6:53,
					7:50,
					8:48,
					9:45,
					10:43,
					11:40,
					12:38,
					13:35,
					14:32,
					15:30,
					16:28,
					17:26,
					18:23,
					19:21,
					20:20,
					21:19,
					22:18,
					23:17,
					24:16,
					25:15,
					26:14,
					27:13,
					28:12,
					29:11,
					30:10,
					31:9,
					32:8,
					33:7,
					34:6,
					35:5,
	}
}

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	
	
def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')
		
def make_subsession_count(app_data,device_data):
	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 1
	else:
		app_data['subsession_count'] += 1
		
	return app_data.get('subsession_count')

###########################################################
#						INSTALL							  #
###########################################################
def install(app_data, device_data):
	print '*************Install**********************'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios',min_sleep=0)
	pushtoken(app_data)
	# apple_receipt(app_data)

	###########		Initialize		############
	
	# if not app_data.get('installed_at'):
	# 	app_data['installed_at'] = get_date(app_data,device_data)

	# time.sleep(random.randint(60,100))	
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)	

	

	app_data['adjust_call_time']=int(time.time())	

	###########			Calls		############   

	global cars_list

	cars_list=[['1083273870-3d4b4c2c', '3790000.0', False],['1085292876-3fbb42cf', '2498570.0', False],['1085099784-613cb6ab', '667900.0', False],['1085910784-81007f64', '900000.0', False],['1084991042-9b1ea244', '5280000.0', False],['1085030512-162351c9', '1936900.0', False],['1085754822-49d66e9c', '2890000.0', False],['1085905046-4a1f6a16', '9490893.0', False],['1085863558-dd830a0a', '1146000.0', False],['1085561322-db654632', '2814000.0', False],['1084835808-09b3190c', '1919000.0', False],['1085554938-a93d689a', '1719900.0', False],['1084178949-6f63ec8b', '373410.0', False],['1085524408-3202393d', '445000.0', False],['1085906920-fd4c627b', '2407700.0', False],['1088539074-db9c7165', '8478000.0', False],['1088545146-3f3b9ccb', '6650000.0', False],['1088764250-1288adaf', '6550000.0', False],['1090797568-f427df5f', '5900000.0', False],['1089934236-8b37da50', '5900000.0', False],['1090796416-782a8e75', '5750000.0', False],['1085805988-25d3de30', '5700000.0', False],['1088529852-4df06c82', '5650000.0', False],['1089947884-004ae098', '5050000.0', False],['1077585817-fad39', '5000000.0', False],['1091380542-fe6c502b', '4950000.0', False],['1088721614-a23b9258', '4950000.0', False],['1090020082-fe1fbd85', '4800000.0', False],['1072232246-6c838d4b', '4600000.0', False],['1088635594-65ec690d', '4500000.0', False],['1081272560-bc0df17c', '4500000.0', False],['1088643098-38cbe63d', '4295000.0', False],['1075982405-8babc8', '4200000.0', False],['1090955034-e8092fe5', '4200000.0', False],['1089496546-71ebf105', '4200000.0', False],['1085648850-a9a1fd44', '4200000.0', False],['1091329168-26e92250', '4100000.0', False],['1088853354-d925922d', '4099000.0', False]]

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,t1=0,t2=0)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)

	time.sleep(random.randint(1,2))

	print "\n----------------------------SELF CALL ----------------------------------------"
	request = startup_mobile(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(7,10))

	print "\n----------------------------ADJUST Attribution ------------------------------------"
	adjustSession = adjust_attribution(campaign_data, app_data, device_data,t1=0,t2=0)
	util.execute_request(**adjustSession)
	
	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data,t1=0,t2=0)
	util.execute_request(**adjustSession)

	print "\n----------------------------ADJUST sdk INFO ------------------------------------"
	adjustSession = adjust_sdkinfo(campaign_data, app_data, device_data,t1=0,t2=0)
	util.execute_request(**adjustSession)

	time.sleep(random.randint(8,10))
	
	app_start(campaign_data, app_data, device_data)	

	if random.randint(1,100)<=50:
		app_data['customer_id']="44"+util.get_random_string('decimal',6)
		app_data['registration']=True

	global val,cars_id,cars_price,talked	

	if app_data.get('registration')==True and random.randint(1,100)<=40:
		val=random.randint(0,len(cars_list)-1)
		cars_id=cars_list[val][0]

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			car_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			bike_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			commercial_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			car_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			bike_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			commercial_ad(campaign_data, app_data, device_data,cars_id=cars_id)	

	if random.randint(1,100) <= 50:

		time.sleep(random.randint(30,120))
		if random.randint(1,100)<=20:
			car_search(campaign_data, app_data, device_data)
		car_search(campaign_data, app_data, device_data)

		global new_car_list,new_carid_list
		new_car_list=[]
		new_carid_list=[]
		for i in range(random.randint(3,5)):
			temp=random.choice(cars_list)
			if temp not in new_car_list:
				new_car_list.append(temp)
				new_carid_list.append(temp[0])

		new_car_search(campaign_data, app_data, device_data,new_carid_list)

		if random.randint(1,100)<=20:
			new_car_search(campaign_data, app_data, device_data,new_carid_list)

		if random.randint(1,100)<=20:
			new_car_search(campaign_data, app_data, device_data,new_carid_list)
		if random.randint(1,100)<=20:
			car_search(campaign_data, app_data, device_data)

		if random.randint(1,100)<=95:

			val=random.randint(0,len(new_car_list)-1)
			cars_id=new_car_list[val][0]
			cars_price=new_car_list[val][1]
			talked=new_car_list[val][2]						

			time.sleep(random.randint(30,120))
			content_view(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)

			if random.randint(1,100)<=20:
				content_view(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)
		
			if random.randint(1,100)<=30:				

				time.sleep(random.randint(30,120))
				add_to_fav(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)

			if random.randint(1,100)<=15:
				time.sleep(random.randint(10,120))
				call_dealer(campaign_data, app_data, device_data)

			if random.randint(1,100)<=25 and talked==False:
				time.sleep(random.randint(10,120))
				msgg_dealer_first(campaign_data, app_data, device_data,cars_id=cars_id)
				cars_list[val][2]=True

		if random.randint(1,100)<=25:	

			val=random.randint(0,len(cars_list)-1)
			cars_id=cars_list[val][0]
			cars_price=cars_list[val][1]
			talked=cars_list[val][2]			

			time.sleep(random.randint(30,120))
			add_to_fav(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)

	if random.randint(1,100) <= 40:

		time.sleep(random.randint(30,120))
		car_search(campaign_data, app_data, device_data)

		global new_car_list,new_carid_list
		new_car_list=[]
		new_carid_list=[]
		for i in range(random.randint(3,5)):
			temp=random.choice(cars_list)
			if temp not in new_car_list:
				new_car_list.append(temp)
				new_carid_list.append(temp[0])

		new_car_search(campaign_data, app_data, device_data,new_carid_list)

		if random.randint(1,100)<=60:

			val=random.randint(0,len(new_car_list)-1)
			cars_id=new_car_list[val][0]
			cars_price=new_car_list[val][1]
			talked=new_car_list[val][2]						

			time.sleep(random.randint(30,120))
			content_view(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)
		
			if random.randint(1,100)<=10:				

				time.sleep(random.randint(30,120))
				add_to_fav(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)

			if random.randint(1,100)<=15:
				time.sleep(random.randint(10,120))
				call_dealer(campaign_data, app_data, device_data)

			if random.randint(1,100)<=10:				

				time.sleep(random.randint(30,120))
				add_to_fav(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)

			if random.randint(1,100)<=5:
				time.sleep(random.randint(10,120))
				call_dealer(campaign_data, app_data, device_data)

			if random.randint(1,100)<=25 and talked==False:
				time.sleep(random.randint(10,120))
				msgg_dealer_first(campaign_data, app_data, device_data,cars_id=cars_id)
				cars_list[val][2]=True

		if random.randint(1,100)<=25:	

			val=random.randint(0,len(cars_list)-1)
			cars_id=cars_list[val][0]
			cars_price=cars_list[val][1]
			talked=cars_list[val][2]			

			time.sleep(random.randint(30,120))
			add_to_fav(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)




	if app_data.get('registration')==True and random.randint(1,100)<=70:
		val=random.randint(0,len(cars_list)-1)
		cars_id=cars_list[val][0]

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			car_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			bike_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			commercial_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			car_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			bike_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			commercial_ad(campaign_data, app_data, device_data,cars_id=cars_id)

	
	###########		Finalize	############
	set_appCloseTime(app_data)

	return {'status':True}

###########################################################
#						OPEN							  #
###########################################################
def open(app_data, device_data, day=1):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios',min_sleep=0)

	pushtoken(app_data)	
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)	

	

	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())

	if not app_data.get('registration'):
		app_data['registration']=False


	global cars_list

	cars_list=[['1083273870-3d4b4c2c', '3790000.0', False],['1085292876-3fbb42cf', '2498570.0', False],['1085099784-613cb6ab', '667900.0', False],['1085910784-81007f64', '900000.0', False],['1084991042-9b1ea244', '5280000.0', False],['1085030512-162351c9', '1936900.0', False],['1085754822-49d66e9c', '2890000.0', False],['1085905046-4a1f6a16', '9490893.0', False],['1085863558-dd830a0a', '1146000.0', False],['1085561322-db654632', '2814000.0', False],['1084835808-09b3190c', '1919000.0', False],['1085554938-a93d689a', '1719900.0', False],['1084178949-6f63ec8b', '373410.0', False],['1085524408-3202393d', '445000.0', False],['1085906920-fd4c627b', '2407700.0', False],['1088539074-db9c7165', '8478000.0', False],['1088545146-3f3b9ccb', '6650000.0', False],['1088764250-1288adaf', '6550000.0', False],['1090797568-f427df5f', '5900000.0', False],['1089934236-8b37da50', '5900000.0', False],['1090796416-782a8e75', '5750000.0', False],['1085805988-25d3de30', '5700000.0', False],['1088529852-4df06c82', '5650000.0', False],['1089947884-004ae098', '5050000.0', False],['1077585817-fad39', '5000000.0', False],['1091380542-fe6c502b', '4950000.0', False],['1088721614-a23b9258', '4950000.0', False],['1090020082-fe1fbd85', '4800000.0', False],['1072232246-6c838d4b', '4600000.0', False],['1088635594-65ec690d', '4500000.0', False],['1081272560-bc0df17c', '4500000.0', False],['1088643098-38cbe63d', '4295000.0', False],['1075982405-8babc8', '4200000.0', False],['1090955034-e8092fe5', '4200000.0', False],['1089496546-71ebf105', '4200000.0', False],['1085648850-a9a1fd44', '4200000.0', False],['1091329168-26e92250', '4100000.0', False],['1088853354-d925922d', '4099000.0', False]]

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data,type='open',t1=0,t2=0)
	util.execute_request(**adjustSession)

	app_data['adjust_call_time']=int(time.time())

	app_start(campaign_data, app_data, device_data)	

	if random.randint(1,100)<=50 and app_data.get('registration')==False:
		app_data['customer_id']="44"+util.get_random_string('decimal',6)
		app_data['registration']=True

	global val,cars_id,cars_price,talked		

	if app_data.get('registration')==True and random.randint(1,100)<=30:
		val=random.randint(0,len(cars_list)-1)
		cars_id=cars_list[val][0]

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			car_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			bike_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			commercial_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			car_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			bike_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			commercial_ad(campaign_data, app_data, device_data,cars_id=cars_id)	

	if random.randint(1,100) <= 30:

		time.sleep(random.randint(30,120))
		if random.randint(1,100)<=20:
			car_search(campaign_data, app_data, device_data)
		car_search(campaign_data, app_data, device_data)

		global new_car_list,new_carid_list
		new_car_list=[]
		new_carid_list=[]
		for i in range(random.randint(3,5)):
			temp=random.choice(cars_list)
			if temp not in new_car_list:
				new_car_list.append(temp)
				new_carid_list.append(temp[0])

		new_car_search(campaign_data, app_data, device_data,new_carid_list)

		if random.randint(1,100)<=20:
			new_car_search(campaign_data, app_data, device_data,new_carid_list)
		if random.randint(1,100)<=20:
			car_search(campaign_data, app_data, device_data)

		if random.randint(1,100)<=20:
			new_car_search(campaign_data, app_data, device_data,new_carid_list)

		if random.randint(1,100)<=95:

			val=random.randint(0,len(new_car_list)-1)
			cars_id=new_car_list[val][0]
			cars_price=new_car_list[val][1]
			talked=new_car_list[val][2]						

			time.sleep(random.randint(30,120))
			content_view(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)

			if random.randint(1,100)<=20:
				content_view(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)
		
			if random.randint(1,100)<=30:				

				time.sleep(random.randint(30,120))
				add_to_fav(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)

			if random.randint(1,100)<=15:
				time.sleep(random.randint(10,120))
				call_dealer(campaign_data, app_data, device_data)

			if random.randint(1,100)<=25 and talked==False:
				time.sleep(random.randint(10,120))
				msgg_dealer_first(campaign_data, app_data, device_data,cars_id=cars_id)
				cars_list[val][2]=True

		if random.randint(1,100)<=25:	

			val=random.randint(0,len(cars_list)-1)
			cars_id=cars_list[val][0]
			cars_price=cars_list[val][1]
			talked=cars_list[val][2]			

			time.sleep(random.randint(30,120))
			add_to_fav(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)

	if random.randint(1,100) <= 30:

		time.sleep(random.randint(30,120))
		car_search(campaign_data, app_data, device_data)

		global new_car_list,new_carid_list
		new_car_list=[]
		new_carid_list=[]
		for i in range(random.randint(3,5)):
			temp=random.choice(cars_list)
			if temp not in new_car_list:
				new_car_list.append(temp)
				new_carid_list.append(temp[0])

		new_car_search(campaign_data, app_data, device_data,new_carid_list)

		if random.randint(1,100)<=60:

			val=random.randint(0,len(new_car_list)-1)
			cars_id=new_car_list[val][0]
			cars_price=new_car_list[val][1]
			talked=new_car_list[val][2]						

			time.sleep(random.randint(30,120))
			content_view(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)
		
			if random.randint(1,100)<=10:				

				time.sleep(random.randint(30,120))
				add_to_fav(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)

			if random.randint(1,100)<=15:
				time.sleep(random.randint(10,120))
				call_dealer(campaign_data, app_data, device_data)

			if random.randint(1,100)<=10:				

				time.sleep(random.randint(30,120))
				add_to_fav(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)

			if random.randint(1,100)<=5:
				time.sleep(random.randint(10,120))
				call_dealer(campaign_data, app_data, device_data)

			if random.randint(1,100)<=25 and talked==False:
				time.sleep(random.randint(10,120))
				msgg_dealer_first(campaign_data, app_data, device_data,cars_id=cars_id)
				cars_list[val][2]=True

		if random.randint(1,100)<=25:	

			val=random.randint(0,len(cars_list)-1)
			cars_id=cars_list[val][0]
			cars_price=cars_list[val][1]
			talked=cars_list[val][2]			

			time.sleep(random.randint(30,120))
			add_to_fav(campaign_data, app_data, device_data,cars_id=cars_id,cars_price=cars_price)


	if app_data.get('registration')==True and random.randint(1,100)<=50:
		val=random.randint(0,len(cars_list)-1)
		cars_id=cars_list[val][0]

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			car_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			bike_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			commercial_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=10:
			time.sleep(random.randint(180,600))
			car_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			bike_ad(campaign_data, app_data, device_data,cars_id=cars_id)

		if random.randint(1,100)<=20:
			time.sleep(random.randint(180,600))
			commercial_ad(campaign_data, app_data, device_data,cars_id=cars_id)

	
	###########		Finalize	############
	set_appCloseTime(app_data)

	return {'status':True}
	
###########################################################
#         Event Definition                                #
###########################################################	

def app_start(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT____________________________APP OPEN'
	request=adjust_event(campaign_data, app_data, device_data,'gvp54r',t1=0,t2=0)
	util.execute_request(**request)

def car_search(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT____________________________car_search'
	request=adjust_event(campaign_data, app_data, device_data,'se4hby',t1=2,t2=3)
	util.execute_request(**request)

def new_car_search(campaign_data, app_data, device_data,new_carid_list):
	print '\nAdjust : EVENT____________________________new_car_search'

	partner_params={"criteo_p":urllib.quote(str(new_carid_list))}

	if app_data.get('customer_id'):
		partner_params['customer_id']=app_data.get('customer_id')
	request=adjust_event(campaign_data, app_data, device_data,'vip020',partner_params=json.dumps(partner_params),t1=2,t2=3)
	util.execute_request(**request)

def content_view(campaign_data, app_data, device_data,cars_id,cars_price):
	print '\nAdjust : EVENT____________________________content_view'

	partner_params={"fb_content_id":str(cars_id),"_valueToSum":cars_price,"trg_feed":"3060267_4","fb_currency":"RUB","criteo_p":str(cars_id),"item_id":str(cars_id),"fb_content_type":"product"}

	if app_data.get('customer_id'):
		partner_params['customer_id']=app_data.get('customer_id')

	request=adjust_event(campaign_data, app_data, device_data,'76cyrr',partner_params=json.dumps(partner_params),t1=2,t2=3)
	util.execute_request(**request)

def add_to_fav(campaign_data, app_data, device_data,cars_id,cars_price):
	print '\nAdjust : EVENT____________________________add_to_fav'
	# partner_params=json.dumps({"customer_id":app_data.get('customer_id'),"valueToSum":cars_price,"fb_currency":"RUB","fb_content_type":"product","fb_content_id":cars_id,"criteo_p":urllib.quote(str([{"i":cars_id,"pr":float(cars_price),"q":random.randint(1,2)}]))})

	partner_params={"fb_content_id":cars_id,"_valueToSum":cars_price,"trg_feed":"3060267_4","fb_currency":"RUB","criteo_p":urllib.quote(str([{"i":cars_id,"pr":float(cars_price),"q":random.randint(1,2)}])),"item_id":cars_id,"fb_content_type":"product"}

	if app_data.get('customer_id'):
		partner_params['customer_id']=app_data.get('customer_id')

	request=adjust_event(campaign_data, app_data, device_data,'9r8f3c',partner_params=json.dumps(partner_params),t1=2,t2=3)
	util.execute_request(**request)

def call_dealer(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT____________________________call_dealer'

	partner_params={"trg_feed":"3060267_4","fb_currency":"RUB","fb_content_type":"product","_valueToSum":cars_price,"fb_content_id":cars_id}
	if app_data.get('customer_id'):
		partner_params['customer_id']=app_data.get('customer_id')
	request=adjust_event(campaign_data, app_data, device_data,'d3hyfa',partner_params=json.dumps(partner_params),t1=2,t2=3)
	util.execute_request(**request)

def msgg_dealer_first(campaign_data, app_data, device_data,cars_id):
	partner_params=json.dumps({"item_id":cars_id})
	print '\nAdjust : EVENT____________________________msgg_dealer_first'
	request=adjust_event(campaign_data, app_data, device_data,'lqz6i8',partner_params=partner_params,t1=2,t2=3)
	util.execute_request(**request)

def car_ad(campaign_data, app_data, device_data,cars_id):
	partner_params=json.dumps({"item_id":cars_id})
	print '\nAdjust : EVENT____________________________car_ad'
	request=adjust_event(campaign_data, app_data, device_data,'jmol1d',partner_params=partner_params,t1=2,t2=3)
	util.execute_request(**request)

	request=adjust_event(campaign_data, app_data, device_data,'483mep',partner_params=partner_params,t1=2,t2=3)
	util.execute_request(**request)

def commercial_ad(campaign_data, app_data, device_data,cars_id):
	partner_params=json.dumps({"item_id":cars_id})
	print '\nAdjust : EVENT____________________________commercial_ad'
	request=adjust_event(campaign_data, app_data, device_data,'jmol1d',partner_params=partner_params,t1=2,t2=3)
	util.execute_request(**request)

	request=adjust_event(campaign_data, app_data, device_data,'71jkmk',partner_params=partner_params,t1=2,t2=3)
	util.execute_request(**request)

def bike_ad(campaign_data, app_data, device_data,cars_id):
	partner_params=json.dumps({"item_id":cars_id})
	print '\nAdjust : EVENT____________________________bike_ad'
	request=adjust_event(campaign_data, app_data, device_data,'jmol1d',partner_params=partner_params,t1=2,t2=3)
	util.execute_request(**request)

	request=adjust_event(campaign_data, app_data, device_data,'3p8mwr',partner_params=partner_params,t1=2,t2=3)
	util.execute_request(**request)

###########################################################
#						ADJUST							  #
###########################################################

def adjust_session(campaign_data, app_data, device_data,type='install',t1=0,t2=0):
	app_data['subsession_count'] = 1
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	
	params={}

	data = {
			# 'app_secret' : campaign_data.get('adjust').get('secret_key'),
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'connectivity_type':	'2',
			'created_at': created_at,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'sent_at': sent_at,
			'session_count': make_session_count(app_data,device_data),
			'tracking_enabled':	0,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'install_receipt': apple_receipt(),
			'os_build':	device_data.get('build'),
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'mcc': device_data.get('mcc'),
			'mnc': device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyLTE',
			# 'tce':	1,
			}
	
	if type == "open":
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = int(time.time()) - app_data.get('adjust_call_time'),
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = int(time.time()) - app_data.get('adjust_call_time'),

		
	
	app_data['time_passes'] = int(time.time()*1000)

	# message = bytearray(json.dumps(data))
	# str_adj = json.dumps(data)+adj1(app_data,device_data,message)+str(time.time())+campaign_data.get('sdk')+"session"+campaign_data.get('adjust').get('sdk')
	# sign= (util.sha256(str_adj)).upper()
	# headers['Authorization']= 'Signature signature="'+sign+'",secret_id="1",algorithm="adj1",headers_id="2"'
	#headers['Authorization']=get_auth(campaign_data,'session',data.get('idfa'),data.get('created_at'))

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adjust_sdkclick(campaign_data, app_data, device_data,t1=0,t2=0):
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}

	created_at=get_date(app_data,device_data,early=random.randint(7,10))
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	
	params={}

	data = {'persistent_ios_uuid':app_data.get('ios_uuid'),
			# 'app_secret' : campaign_data.get('adjust').get('secret_key'),
			'details': '{"Version3.1":{"iad-attribution":"false"}}',
			'source': 'iad3',
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': created_at,
			'connectivity_type':	'2',
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'sent_at': sent_at,
			'session_count': app_data.get('session_count'),
			'tracking_enabled':	0,
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'installed_at':datetime.datetime.utcfromtimestamp((app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'install_receipt': apple_receipt(),
			'os_build':	device_data.get('build'),
			# 'tce':	1,
			'last_interval': 2,
			'subsession_count': 1,
			'session_length': int(time.time()) - app_data.get('adjust_call_time'),
			'time_spent' : int(time.time()) - app_data.get('adjust_call_time'),
			'push_token':app_data.get('pushtoken'),
			'mcc': device_data.get('mcc'),
			'mnc': device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyLTE',
			}

	#headers['Authorization']=get_auth(campaign_data,'click',data.get('idfa'),data.get('created_at'))

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
def adjust_attribution(campaign_data, app_data, device_data,t1=0,t2=0):
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}
	data={}

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	params = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink' : 1,
			'bundle_id': campaign_data.get('package_name'),
			'created_at': created_at,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'initiated_by':'sdk',
			'os_build':	device_data.get('build'),
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid' :app_data.get('ios_uuid'),
			'needs_response_details': 1,
			'sent_at': sent_at,
			# 'app_secret' : campaign_data.get('adjust').get('secret_key'),
			# 'headers_id':'2',
			# 'secret_id':campaign_data.get('adjust').get('secret_id'),
			}

	# message = bytearray(json.dumps(params))
	# str_adj = json.dumps(params)+adj1(app_data,device_data,message)+str(time.time())+campaign_data.get('sdk')+"session"+campaign_data.get('adjust').get('sdk')
	# sign= (util.sha256(str_adj)).upper()
	# headers['Authorization']= 'Signature signature="'+sign+'",secret_id="1",algorithm="adj1",headers_id="2"'

	# params['signature'] = sign			
	#headers['Authorization']=get_auth(campaign_data,'attribution',data.get('idfa'),data.get('created_at'))
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		

def adjust_sdkinfo(campaign_data, app_data, device_data,t1=0,t2=0):
	url = 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Content-Type': 'application/x-www-form-urlencoded',
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}

	created_at=get_date(app_data,device_data,early=random.randint(7,10))
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	params={}

	data = {
		# 'app_secret' : campaign_data.get('adjust').get('secret_key'),
		'app_token': campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink' : 1,
		'created_at': created_at,
		'environment' : 'production',
		'event_buffering_enabled': 0,
		'idfa': app_data.get('idfa_id'),
		'idfv':	app_data.get('idfv_id'),
		'needs_response_details': 1,
		'sent_at': sent_at,
		'persistent_ios_uuid': app_data.get('ios_uuid'),
		'push_token':app_data.get('pushtoken'),
		'source' :'push'
	}

	#headers['Authorization']=get_auth(campaign_data,'info',data.get('idfa'),data.get('created_at'))

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}
	
def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=None,partner_params=None,t1=0,t2=0):
	def_(app_data,'subsession_count')
		
	url = 'http://app.adjust.com/event'
	method = 'post'
	headers = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}
			
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	data = {
			# 'app_secret' : campaign_data.get('adjust').get('secret_key'),
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'hardware_name':'D101AP',	
			'event_buffering_enabled':	0,
			'cpu_type':'CPU_SUBTYPE_ARM64_V8',
			'attribution_deeplink': '1',
			'os_name':'ios',
			'environment':'production',
			'needs_response_details':1,
			'event_count':app_data.get('event_count'),
			'session_count':str(app_data.get('session_count')),
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			'created_at': created_at,
			'connectivity_type':	'2',
			'event_token':event_token,
			'bundle_id':campaign_data.get('package_name'),
			'subsession_count':str(app_data.get('session_count')),
			'os_version':device_data.get('os_version'),
			'app_version': campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country'),
			'language':	device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'session_length':int(time.time()) - app_data.get('adjust_call_time'),
			'time_spent':int(time.time()) - app_data.get('adjust_call_time'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': '0',
			'device_name':device_data.get('device_platform'),
			'sent_at': sent_at,
			'os_build':	device_data.get('build'),
			'install_receipt': apple_receipt(),
			'push_token':app_data.get('pushtoken'),
			'mcc': device_data.get('mcc'),
			'mnc': device_data.get('mnc'),
			'network_type':'CTRadioAccessTechnologyLTE',
			}
			
	if callback_params:
		data['callback_params']	= callback_params
	if partner_params:
		data['partner_params']	= partner_params

	#headers['Authorization']=get_auth(campaign_data,'event',data.get('idfa'),data.get('created_at'))
		
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':None, 'data': data}	

################################################################
#
#
#     SELF CALL   (YANDEX)
#
#
################################################################	
def startup_mobile(campaign_data, app_data, device_data):
	url = 'http://startup.mobile.yandex.net/analytics/startup'
	method = 'get'
	headers = {
				"Accept": "application/json",
				"Accept-Language": device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				"Accept-Encoding": 'encrypted',#"br, gzip, deflate",
				"User-Agent": "com.yandex.mobile.metrica.sdk/"+campaign_data.get('yandex').get('version_name')+"."+campaign_data.get('yandex').get('sdk')+" (Apple "+device_data.get('device_platform')+"; iOS "+device_data.get('os_version')+")",
			}

	data = None

	params = {
			# 'analytics_sdk_version'	: campaign_data.get('yandex').get('version_code'),
			'analytics_sdk_version_name' :	campaign_data.get('yandex').get('version_name'),
			'app_debuggable' :	0,
			'app_id' :	campaign_data.get('package_name'),
			'app_platform' :	'iphone',
			'browsers' :	1,
			'device_type' :	device_data.get('device_type'),
			'detect_locale'	:'1',
			'extensions_collecting':'1',
			'queries':'1',
			'requests':'1',
			'deviceid' :	app_data.get('idfv_id'),
			'easy_collecting' :	1,
			'features' :	'easy_attribution,easy_collecting,extensions_collecting,sdk_list,socket,socket_closing',
			'ifv' :	app_data.get('idfv_id'),
			'locale' :	device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
			'manufacturer' :	'Apple',
			'model' :	device_data.get('device_platform'),
			'os_version' :	device_data.get('os_version'),
			'protocol_version' :	2,
			'query_hosts' :	2,
			'scalefactor' :	2.00,
			'screen_dpi' :	device_data.get('dpi'),
			'screen_height' :	device_data.get('resolution').split('x')[1],
			'screen_width' :	device_data.get('resolution').split('x')[0],
			'socket' :	1,
			'sdk_list':	1,
			'stat_sending'	:1,
			'uuid'	:str(uuid.uuid4()).replace('-','')

			}


		
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':params, 'data': data}	
	
	
###########################################################
#						UTIL							  #
###########################################################

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	#st = str(int(time.time()*1000))
	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('app_id'))
	
def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length
	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0
		
def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(timestamp())	
	
def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(timestamp()) - app_data.get('appCloseTime')
	
def user_id(app_data):
	if not app_data.get('user_id'):
		app_data['user_id']= '123'+util.get_random_string('decimal',4)	
	
def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def pushtoken(app_data):
	if not app_data.get('pushtoken'):
		app_data['pushtoken']=util.get_random_string('hex',64)	

def get_auth(campaign_data,activity_kind,idfa,created_at):
	final_str=campaign_data.get('adjust').get('secret_key')+str(activity_kind)+str(idfa)+str(created_at)
	sign=util.sha256(final_str)
	auth='Signature secret_id="'+str(campaign_data.get('adjust').get('secret_id'))+'",signature="'+str(sign)+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'
	return auth	

def apple_receipt():
	
		return 'MIISkQYJKoZIhvcNAQcCoIISgjCCEn4CAQExCzAJBgUrDgMCGgUAMIICMgYJKoZIhvcNAQcBoIICIwSCAh8xggIbMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMAwCAQ4CAQEEBAICAI0wDQIBCwIBAQQFAgMHbnQwDQIBDQIBAQQFAgMB1YgwDgIBAQIBAQQGAgQeQ89CMA4CAQkCAQEEBgIEUDI1MzAOAgEQAgEBBAYCBDGhNgMwDwIBAwIBAQQHDAUxMjE5MTAPAgETAgEBBAcMBTEyMTkxMBACAQ8CAQEECAIGS4iJQPMjMBMCAQICAQEECwwJcnUuQXV0b1J1MBQCAQACAQEEDAwKUHJvZHVjdGlvbjAYAgEEAgECBBAnzs/AupC942JBER3RbGThMBwCAQUCAQEEFEXeKjaOMtrL3QISYjnXV308CDI7MB4CAQgCAQEEFhYUMjAxOS0wOS0wN1QwOTo1ODo1OFowHgIBDAIBAQQWFhQyMDE5LTA5LTA3VDA5OjU4OjU5WjAeAgESAgEBBBYWFDIwMTktMDktMDdUMDk6NTg6NTlaMEkCAQcCAQEEQbDx7N58KceCFKq3aZWOUd3WdSY40x7DbdCObs9JQESHxwpGbuTLcHTDO85MdJY7ArsrypklyrK6fszi7jWAZIgAMFQCAQYCAQEETP3DXNgVBlsjzh+OOr+SAV4MMIDzZ92VPiHjFQbPMNcjBxoWuu9iv7IHzBl0XHFI5ouFkeGTC/5dgS5bVSjiAQsjKnBBw1gh4ke/S4uggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAKHr2HoeMvBkMMZQj4MKIsos0hslX8rvoUR/k0Js5H3j6gJMt62lSqkWUIqkxt9MrJS1onVpujOTM5C2feOdMbObNz7jInbua1uAR9acbVBpUFxVw7CPk1/2q0PZq2FIsc/XTxMmWVTuGW64Okn07GKUnVCpfQK9xO5NMmk0Z3Gi6xwmdwGu93vEeDKRp3aoRU8nlMOirB5NP9Fua9lahEmIWMrMpN/QBEJyDndyKLnlWP9V/jCQ0nCZcWCwCV78dmIf4dp2wPLjwn6y93+D2y7iydLNjmp+gza+1BrJMxWzN0RpI76jtYmr/0OATujoQuj0BX8RzVLU9MoFveRafsk='


####################################################
#
# 		ADJ 1
#
#########################################################
def adj1(app_data,device_data,message):
	digest = hmac.new(campaign_data['adjust']['secret_key'], msg=message, digestmod=hashlib.sha256).digest()
	signature = base64.b64encode(digest).decode()

	return signature	