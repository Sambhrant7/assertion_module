# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import util, purchase
from sdk import NameLists
from sdk import getsleep
import hmac,hashlib,base64
import time
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.q8.flowers.app',
	'app_name' 			 :'Floward Online flowers &amp; gifts delivery in Kuwait',
	'app_size'			 : 16.0,
	'app_version_name' 	 :'5.6.0',#'5.5.6',#5.5.5,'5.2.0',#'5.1.3',
	'app_version_code'	 : '68',#'67',
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Adjust',
	'adjust'		 : {
		'app_token'	 : 'ngseedw0iha8',
		'sdk'		 : 'android4.17.0',
		'secret_key' : '4475053202073692481134169147300204411',
		'secret_id'	 : '1',
		},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	set_androidUUID(app_data)

	################generating_realtime_differences##################
	click_time1=get_date(app_data,device_data)
	created_at_session=get_date(app_data,device_data)
	created_at_sdk1=get_date(app_data,device_data)
	time.sleep(random.randint(2,3))
	sent_at_sdk1=get_date(app_data,device_data)
	sent_at_session=get_date(app_data,device_data)
	t1=1
	t2=3

	app_data['distinct_id']=str(uuid.uuid4())

	if device_data.get('locale').get('country')=='SA':
		app_data['currency']='SAR'

	else:
		app_data['currency']='KWD'

	###########		CALLS		############	
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)

	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,created_at=created_at_sdk1,sent_at=sent_at_sdk1,click_time=click_time1,source='reftag')
	util.execute_request(**request)

	time.sleep(random.randint(5,10))

	app_open(campaign_data, app_data, device_data,t1,t2)
	time.sleep(random.randint(3,5))

	created_at_attribution=get_date(app_data,device_data)
	sent_at_attribution=get_date(app_data,device_data)
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,created_at=created_at_attribution,sent_at=sent_at_attribution)
	util.execute_request(**request)

	if random.randint(1,100)<=60: #60
		time.sleep(random.randint(30,120))
		request=signup( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			output=json.loads(response.get('data')).get('User')		
			app_data['user_id']=str(output.get('Id'))

		except:
			print 'exception'
			app_data['user_id']='54570'

		registration(campaign_data, app_data, device_data, t1=2,t2=4)

		app_data['registration_complete']=True

	request=get_category_id( campaign_data, device_data, app_data )
	response=util.execute_request(**request)
	try:
		output=json.loads(response.get('data')).get('CategoryObj')
		app_data['catId']=output.get('Id')
	except:
		print 'Exception'
		app_data['catId']='3099'

	global proId_list, proPrice_list, availabe_quantity_list, proName_list

	proId_list=[]
	proPrice_list=[]
	availabe_quantity_list=[]
	proName_list=[]

	request=get_products( campaign_data, device_data, app_data )
	response=util.execute_request(**request)
	try:
		output=json.loads(response.get('data')).get('listOfData')

		for i in range(len(output)):
			proId_list.append(output[i].get('Id'))
			proPrice_list.append(output[i].get('Price'))
			availabe_quantity_list.append(output[i].get('AvailableQTY'))
			proName_list.append(str(output[i].get('Name')))

		if len(proId_list) == 0 or len(proPrice_list) == 0 or len(availabe_quantity_list) == 0 or len(proName_list) == 0:
			proId_list=[9053, 9146, 9270, 9055, 9219, 9272, 9275, 9278, 10006, 9274, 9686, 10007]
			proPrice_list=[140, 150, 150, 160, 200, 200, 200, 200, 200, 220, 220, 220]
			availabe_quantity_list=[45, 187, 3, 14, 3, 250, 3, 283, 3, 13, 45, 3]
			proName_list=['FL2008', 'FL2538', 'FL2594', 'FL2011', 'FL2574', 'FL2595', 'FL2598', ' FL2601', 'FL2634', 'FL2597', 'FL2613', 'FL2632']

	except:
		print 'Exception'

		proId_list=[9053, 9146, 9270, 9055, 9219, 9272, 9275, 9278, 10006, 9274, 9686, 10007]
		proPrice_list=[140, 150, 150, 160, 200, 200, 200, 200, 200, 220, 220, 220]
		availabe_quantity_list=[45, 187, 3, 14, 3, 250, 3, 283, 3, 13, 45, 3]
		proName_list=['FL2008', 'FL2538', 'FL2594', 'FL2011', 'FL2574', 'FL2595', 'FL2598', ' FL2601', 'FL2634', 'FL2597', 'FL2613', 'FL2632']

	r=random.randint(1,100)
	if r<=80:
		max_price=150

	elif r<=95:
		max_price=200

	else:
		max_price=220

	max_price=10

	for _ in range(random.randint(2,3)):

		if random.randint(1,100)<=60:			

			count=0

			while True:
				app_data['index']=random.randint(0, len(proId_list)-1)
				if proPrice_list[app_data.get('index')]<=max_price:
					break

				if count>=len(proId_list):
					app_data['index']=random.randint(0, len(proId_list)-1)
					break

				count+=1


			time.sleep(random.randint(30,120))
			view_product(campaign_data, app_data, device_data,t1=1,t2=3)

			if random.randint(1,100)<=70:

				time.sleep(random.randint(30,120))
				add_to_cart(campaign_data, app_data, device_data,t1=1,t2=3)

				if random.randint(1,100)<=35:
					if app_data.get('registration_complete')==False:
						time.sleep(random.randint(30,120))
						request=signup( campaign_data, device_data, app_data )
						response=util.execute_request(**request)
						try:
							output=json.loads(response.get('data')).get('User')		
							app_data['user_id']=str(output.get('Id'))

						except:
							print 'exception'
							app_data['user_id']='54570'

						registration(campaign_data, app_data, device_data, t1=2,t2=4)

						app_data['registration_complete']=True

					time.sleep(random.randint(30,120))
					checkout_initiated(campaign_data, app_data, device_data,t1=1,t2=3)


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	if not app_data.get('times'):
		print "Please wait installing..."
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	set_androidUUID(app_data)

	if not app_data.get('distinct_id'):
		app_data['distinct_id']=str(uuid.uuid4())

	if not app_data.get('currency'):
		if device_data.get('locale').get('country')=='SA':
			app_data['currency']='SAR'

		else:
			app_data['currency']='KWD'

	if not app_data.get('registration_complete'):
		app_data['registration_complete']=False

	################# calls ###################

	created_at_session=get_date(app_data,device_data)
	time.sleep(random.randint(2,3))
	sent_at_session=get_date(app_data,device_data)

	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session, isOpen=True)
	util.execute_request(**request)

	created_at_attribution=get_date(app_data,device_data)
	sent_at_attribution=get_date(app_data,device_data)

	time.sleep(random.randint(6,8))
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,created_at=created_at_attribution,sent_at=sent_at_attribution)
	util.execute_request(**request)



	time.sleep(random.randint(1,2))
	app_open(campaign_data, app_data, device_data,t1=1,t2=2)

	if random.randint(1,100)<=60 and app_data.get('registration_complete')==False: 
		time.sleep(random.randint(30,120))
		request=signup( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			output=json.loads(response.get('data')).get('User')		
			app_data['user_id']=str(output.get('Id'))

		except:
			print 'exception'
			app_data['user_id']='54570'

		registration(campaign_data, app_data, device_data, t1=2,t2=4)

		app_data['registration_complete']=True

	if not app_data.get('catId'):
		request=get_category_id( campaign_data, device_data, app_data )
		response=util.execute_request(**request)
		try:
			output=json.loads(response.get('data')).get('CategoryObj')
			app_data['catId']=output.get('Id')
		except:
			print 'Exception'
			app_data['catId']='3099'

	global proId_list, proPrice_list, availabe_quantity_list, proName_list

	proId_list=[]
	proPrice_list=[]
	availabe_quantity_list=[]
	proName_list=[]

	request=get_products( campaign_data, device_data, app_data )
	response=util.execute_request(**request)
	try:
		output=json.loads(response.get('data')).get('listOfData')

		for i in range(len(output)):
			proId_list.append(output[i].get('Id'))
			proPrice_list.append(output[i].get('Price'))
			availabe_quantity_list.append(output[i].get('AvailableQTY'))
			proName_list.append(str(output[i].get('Name')))

		if len(proId_list) == 0 or len(proPrice_list) == 0 or len(availabe_quantity_list) == 0 or len(proName_list) == 0:
			proId_list=[9053, 9146, 9270, 9055, 9219, 9272, 9275, 9278, 10006, 9274, 9686, 10007]
			proPrice_list=[140, 150, 150, 160, 200, 200, 200, 200, 200, 220, 220, 220]
			availabe_quantity_list=[45, 187, 3, 14, 3, 250, 3, 283, 3, 13, 45, 3]
			proName_list=['FL2008', 'FL2538', 'FL2594', 'FL2011', 'FL2574', 'FL2595', 'FL2598', ' FL2601', 'FL2634', 'FL2597', 'FL2613', 'FL2632']

	except:
		print 'Exception'

		proId_list=[9053, 9146, 9270, 9055, 9219, 9272, 9275, 9278, 10006, 9274, 9686, 10007]
		proPrice_list=[140, 150, 150, 160, 200, 200, 200, 200, 200, 220, 220, 220]
		availabe_quantity_list=[45, 187, 3, 14, 3, 250, 3, 283, 3, 13, 45, 3]
		proName_list=['FL2008', 'FL2538', 'FL2594', 'FL2011', 'FL2574', 'FL2595', 'FL2598', ' FL2601', 'FL2634', 'FL2597', 'FL2613', 'FL2632']

	if day<4:
		no_of_times=random.randint(2,3)

	elif day>=4 and day<=5:
		no_of_times=random.randint(1,2)

	else:
		no_of_times=random.randint(0,1)

	r=random.randint(1,100)
	if r<=80:
		max_price=150

	elif r<=95:
		max_price=200

	else:
		max_price=220

	max_price=10

	for _ in range(no_of_times):
		if random.randint(1,100)<=60:
			count=0

			while True:
				app_data['index']=random.randint(0, len(proId_list)-1)
				if proPrice_list[app_data.get('index')]<=max_price:
					break

				if count>=len(proId_list):
					app_data['index']=random.randint(0, len(proId_list)-1)
					break

				count+=1

			time.sleep(random.randint(30,120))
			view_product(campaign_data, app_data, device_data,t1=1,t2=3)

			if random.randint(1,100)<=70:

				time.sleep(random.randint(30,120))
				add_to_cart(campaign_data, app_data, device_data,t1=1,t2=3)

				if random.randint(1,100)<=35:
					if app_data.get('registration_complete')==False:
						time.sleep(random.randint(30,120))
						request=signup( campaign_data, device_data, app_data )
						response=util.execute_request(**request)
						try:
							output=json.loads(response.get('data')).get('User')		
							app_data['user_id']=str(output.get('Id'))

						except:
							print 'exception'
							app_data['user_id']='54570'

						registration(campaign_data, app_data, device_data, t1=2,t2=4)

						app_data['registration_complete']=True

					time.sleep(random.randint(30,120))
					checkout_initiated(campaign_data, app_data, device_data,t1=1,t2=3)

					if purchase.isPurchase(app_data,day=day,advertiser_demand=10): #5
						time.sleep(random.randint(50,60))
						order(campaign_data, app_data, device_data)

	return {'status':'true'}


def test(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	set_androidUUID(app_data)

	################generating_realtime_differences##################
	click_time1=get_date(app_data,device_data)
	created_at_session=get_date(app_data,device_data)
	created_at_sdk1=get_date(app_data,device_data)
	time.sleep(random.randint(2,3))
	sent_at_sdk1=get_date(app_data,device_data)
	sent_at_session=get_date(app_data,device_data)
	t1=1
	t2=3

	###########		CALLS		############	
		
	print '\n'+'Adjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,created_at=created_at_session,sent_at=sent_at_session)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)

	print '\n'+'Adjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,created_at=created_at_sdk1,sent_at=sent_at_sdk1,click_time=click_time1,source='reftag')
	util.execute_request(**request)

	time.sleep(random.randint(5,10))

	app_open(campaign_data, app_data, device_data,t1,t2)
	time.sleep(random.randint(3,5))

	created_at_attribution=get_date(app_data,device_data)
	sent_at_attribution=get_date(app_data,device_data)
		
	print '\n'+'Adjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,created_at=created_at_attribution,sent_at=sent_at_attribution)
	util.execute_request(**request)

	time.sleep(random.randint(20,40))

	view_product(campaign_data, app_data, device_data,t1=1,t2=3)

	time.sleep(random.randint(30,40))

	add_to_cart(campaign_data, app_data, device_data,t1=1,t2=3)

	return {'status':'true'}

def signup( campaign_data, device_data, app_data ):
	register_user(app_data)
	url= "https://api.floward.com/API/Apps/v1/api.svc/SingUp" # not changing to http as they are redirected
	method= "post"
	headers= {       "API-Version": "1.0",
        "Accept-Encoding": "gzip",
        "Authorization": "Basic UTNqQVR1U3VoSzlHbkMyeWtkZmhuUThRMjREOGdqTkpwOjVlckFMSmtaQkRNWVFjeVhUOEM=",
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": "okhttp/3.12.0",
        "build": campaign_data.get('app_version_code'),
        "device": "1",
        "lang": "1",
        "mxDistinctId": app_data.get('distinct_id'),
        "opsId": "2",
        "uniqID": "T0RGWW9IRFo0cmNx",
        "version": campaign_data.get('app_version_name')}

	params= {       "accessKey": "ETj5DMaTnej42fE66USLTvg",
        "birthDate": app_data.get('user').get('dob'),
        "country": "21",
        "device": "1",
        "fName": app_data.get('user').get('firstname'),
        "gender": "0",
        "lName": app_data.get('user').get('last_name'),
        "lang": "1",
        "mail": app_data.get('user').get('email'),
        "mxDistinctId": "",
        "password": app_data.get('user').get('password'),
        "phone": app_data.get('user').get('mobile_number'),
        "uniqID": "T0RGWW9IRFo0cmNx"}

	data= {       "accessKey": "ETj5DMaTnej42fE66USLTvg",
        "birthDate": app_data.get('user').get('dob'),
        "country": "21",
        "device": "1",
        "fName": app_data.get('user').get('firstname'),
        "gender": "0",
        "lName": app_data.get('user').get('lastname'),
        "lang": "1",
        "mail": app_data.get('user').get('email'),
        "mxDistinctId": "",
        "password": app_data.get('user').get('password'),
        "phone": app_data.get('user').get('mobile_number'),
        "uniqID": "T0RGWW9IRFo0cmNx"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def get_category_id( campaign_data, device_data, app_data ):
	url= "https://api.floward.com/API/Apps/v1/api.svc/GetCategoryByKey" # not changing to http as they are redirected
	method= "get"
	headers= {       "API-Version": "1.0",
        "Accept-Encoding": "gzip",
        "Authorization": "Basic UTNqQVR1U3VoSzlHbkMyeWtkZmhuUThRMjREOGdqTkpwOjVlckFMSmtaQkRNWVFjeVhUOEM=",
        "User-Agent": "okhttp/3.12.0",
        "build": campaign_data.get('app_version_code'),
        "device": "1",
        "lang": "1",
        "mxDistinctId": app_data.get('distinct_id'),
        "opsId": "2",
        "uniqID": "T0RGWW9IRFo0cmNx",
        "version": campaign_data.get('app_version_name')}

	params= {       "accessKey": "ETj5DMaTnej42fE66USLTvg",
        "catKey": "Flowers",
        "device": "1",
        "lang": "1",
        "mxDistinctId": "",
        "uniqID": "T0RGWW9IRFo0cmNx"}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def get_products( campaign_data, device_data, app_data ):
	url= "https://api.floward.com/API/Apps/v1/api.svc/GetProducts" # not changing to http as they are redirected
	method= "get"
	headers= {       "API-Version": "1.0",
        "Accept-Encoding": "gzip",
        "Authorization": "Basic UTNqQVR1U3VoSzlHbkMyeWtkZmhuUThRMjREOGdqTkpwOjVlckFMSmtaQkRNWVFjeVhUOEM=",
        "User-Agent": "okhttp/3.12.0",
        "build": campaign_data.get('app_version_code'),
        "device": "1",
        "lang": "1",
        "mxDistinctId": app_data.get('distinct_id'),
        "opsId": "2",
        "uniqID": "T0RGWW9IRFo0cmNx",
        "version": campaign_data.get('app_version_name')}

	params= {       "accessKey": "ETj5DMaTnej42fE66USLTvg",
        "catId": app_data.get('catId'),
        "device": "1",
        "filter": "",
        "lang": "1",
        "mxDistinctId": "",
        "occasions": "",
        "page": "1",
        "priceRang": "",
        "searchKey": "",
        "sortBy": "1",
        "uniqID": "T0RGWW9IRFo0cmNx"}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def app_open(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________zd1ot3'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='zd1ot3',t1=t1,t2=t2)
	util.execute_request(**request)

def view_product(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________i8gt0b'
	try:
		proId=str(proId_list[app_data.get('index')])
		proPrice=str(proPrice_list[app_data.get('index')])+'.0'
		proName=str(proName_list[app_data.get('index')])
		qty=str(availabe_quantity_list[app_data.get('index')])

	except:
		print 'Exception'
		proId='9274'
		proPrice='220.0'
		proName='FL2597'
		qty='13'

	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"proId":proId,"proPrice":proPrice,"currency":app_data.get('currency'),"qty":qty,"product":"product","proName":proName,"ScreenName":"ProductView"})),partner_params=str(json.dumps({"criteo_p":proId,"customer_id":"0","proId":proId,"proPrice":proPrice,"currency":app_data.get('currency'),"qty":qty,"product":"product","proName":proName,"ScreenName":"ProductView"})),event_token='i8gt0b',t1=t1,t2=t2)
	util.execute_request(**request)



def add_to_cart(campaign_data, app_data, device_data,t1=0,t2=0):
	try:
		proId=str(proId_list[app_data.get('index')])
		proPrice=str(proPrice_list[app_data.get('index')])+'.0'
		proName=str(proName_list[app_data.get('index')])

	except:
		print 'Exception'
		proId='9274'
		proPrice='220.0'
		proName='FL2597'

	print 'Adjust : EVENT______________________________90v6si'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=str(json.dumps({"proId":proId,"proPrice":proPrice,"currency":app_data.get('currency'),"qty":"1","product":"product","proName":proName})),partner_params=str(json.dumps({"criteo_p":urllib.quote(str([{"i":proId,"pr":float(proPrice),"q":1}])),"customer_id":"0","proId":proId,"proPrice":proPrice,"currency":app_data.get('currency'),"qty":"1","product":"product","proName":proName})),event_token='90v6si',t1=t1,t2=t2)
	util.execute_request(**request)

def registration(campaign_data, app_data, device_data, t1=0,t2=0):
	try:
		country=util.country_list.get(device_data.get('locale').get('country')).get('apolitical_name')

	except:
		print 'Exception'
		country='United States'

	callback_params=json.dumps({"email":app_data.get('user').get('email'),"id":app_data.get('user_id'),"country":country})
	print 'Adjust : EVENT______________________________v6ebqn'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=callback_params,partner_params=None,event_token='v6ebqn',t1=t1,t2=t2)
	util.execute_request(**request)

def checkout_initiated(campaign_data, app_data, device_data,t1=0,t2=0):
	try:	
		proPrice=str(proPrice_list[app_data.get('index')])+'.0'

	except:
		print 'Exception'
		proPrice='220.0'

	callback_params=json.dumps({"amount":proPrice})
	print 'Adjust : EVENT______________________________68gigw'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=callback_params,partner_params=None,event_token='68gigw',t1=t1,t2=t2)
	util.execute_request(**request)

def order(campaign_data, app_data, device_data,t1=0,t2=0):
	print 'Adjust : EVENT______________________________thg4hs'
	request=adjust_event(campaign_data, app_data, device_data,callback_params=None,partner_params=None,event_token='thg4hs',t1=t1,t2=t2)
	util.execute_request(**request)


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,created_at,sent_at,click_time='',source='',callback_params=None,partner_params=None):
	inc_(app_data,'subsession_count')
	reftag = ''
	temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
	if temp_b.get('adjust_reftag'):
		reftag = temp_b.get('adjust_reftag')[0]
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	app_data['sessionLength']=time_spent
	method = "post"
	url = 'http://app.adjust.com/sdk_click'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		"Content-Type" : "application/x-www-form-urlencoded",

		}
	params = {

		}
	data = {
		"event_buffering_enabled" : "0",
		"app_secret" : campaign_data.get('adjust').get('secret_key'),
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"needs_response_details" : "1",
		"time_spent" : time_spent,
		"os_name" : "android",
		"device_type" : device_data.get('device_type'),
		"device_manufacturer" : device_data.get('manufacturer'),
		"last_interval" : "0",
		"connectivity_type" : "1",
		"package_name" : campaign_data.get('package_name'),
		"os_version" : device_data.get('os_version'),
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("install_complete_time")),
		"environment" : "production",
		"source" : source,
		"android_uuid" : app_data.get('android_uuid'),
		"display_width" : device_data.get('resolution').split('x')[1],
		"screen_density" : get_screen_density(device_data),
		"raw_referrer" : "utm_source=(not%20set)&utm_medium=(not%20set)",
		"subsession_count" : app_data.get('subsession_count'),
		"session_length" : session_length,
		"gps_adid" : device_data.get('adid'),
		"screen_size" : get_screen_size(device_data),
		"tracking_enabled" : "1",
		"gps_adid_src" : "service",
		"screen_format" : get_screen_format(device_data),
		"display_height" : device_data.get('resolution').split('x')[0],
		"hardware_name" : device_data.get('hardware'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"sent_at" : sent_at,
		"attribution_deeplink" : "1",
		"app_version" : campaign_data.get('app_version_name'),
		"language" : device_data.get('locale').get('language'),
		"referrer" : "utm_source=(not set)&utm_medium=(not set)",
		"created_at" : created_at,
		"session_count" : 1,
		"device_name" : device_data.get('model'),
		"cpu_type" : device_data.get('cpu_abi'),
		"country" : device_data.get('locale').get('country'),
		"network_type" : "0",
		"api_level" : device_data.get('sdk'),
		"os_build" : device_data.get('build'),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if source=='reftag':
		data['referrer']=urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else 'utm_source=(not set)&utm_medium=(not set)'
		data['raw_referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		data['reftag']=reftag
		if click_time:
			data['click_time']=click_time
		elif data.get("click_time"):
			del data['click_time']
		# del data['session_length']
		# del data['subsession_count']
		# del data['time_spent']

	if source=='install_referrer':
		data['referrer']=app_data.get('referrer') or 'utm_source=(not%20set)&utm_medium=(not%20set)'
		if click_time:
			data['click_time']=click_time
		elif data.get("click_time"):
			del data['click_time']
		
		if data.get('raw_referrer'):
			del data['raw_referrer']
		
		data['install_begin_time']=get_date_by_ts(app_data,device_data,ts=app_data.get("times").get("download_end_time"))
		# del data['last_interval']


	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,created_at,sent_at,isOpen=False,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	inc_(app_data,'session_count')
	inc_(app_data,'subsession_count')
	if not app_data.get('sess_len'):
		app_data['sess_len']=int(time.time())
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	app_data['sessionLength']=time_spent
	method = "post"
	url = 'http://app.adjust.com/session'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/x-www-form-urlencoded",
		"User-Agent" : get_ua(device_data),
		}

	params = {

		}
	data = {
		"package_name" : campaign_data.get('package_name'),
		"app_secret" : campaign_data.get('adjust').get('secret_key'),
		"updated_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"needs_response_details" : "1",
		"os_name" : "android",
		"device_type" : device_data.get('device_type'),
		"device_manufacturer" : device_data.get('manufacturer'),
		"connectivity_type" : "1",
		"event_buffering_enabled" : "0",
		"installed_at" : get_date_by_ts(app_data,device_data,ts=app_data.get('times').get('install_complete_time')),
		"environment" : "production",
		"os_version" : device_data.get('os_version'),
		"android_uuid" : app_data.get('android_uuid'),
		"display_width" : device_data.get('resolution').split('x')[1],
		"screen_density" : get_screen_density(device_data),
		"gps_adid" : device_data.get('adid'),
		"screen_size" : get_screen_size(device_data),
		"tracking_enabled" : "1",
		"gps_adid_src" : "service",
		"screen_format" : get_screen_format(device_data),
		"display_height" : device_data.get('resolution').split('x')[0],
		"hardware_name" : device_data.get('hardware'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"sent_at" : sent_at,
		"attribution_deeplink" : "1",
		"app_version" : campaign_data.get('app_version_name'),
		"language" : device_data.get('locale').get('language'),
		"country" : device_data.get('locale').get('country'),
		"created_at" : created_at,
		"session_count" : app_data.get('session_count'),
		"device_name" : device_data.get('model'),
		"cpu_type" : device_data.get('cpu_abi'),
		"network_type" : "0",
		"api_level" : device_data.get('sdk'),
		"os_build" : device_data.get('build'),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if isOpen:
		def_(app_data,'subsession_count')
		data['created_at'] 		= get_date(app_data,device_data)
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('sessionLength')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('sessionLength')
		app_data['sess_len']=int(time.time())


	
	message = bytearray(json.dumps(data))
	str_adj = json.dumps(data)+adj1(app_data,device_data,message)+str(time.time())+device_data.get('sdk')+"session"+campaign_data.get('adjust').get('sdk')
	sign= (util.sha256(str_adj)).upper()
	headers['Authorization']= 'Signature signature="'+sign+'",secret_id='+campaign_data.get('adjust').get('secret_id')+',algorithm="adj1",headers_id="1"'	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,callback_params=None,partner_params=None):
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	session_length=int(time.time())-app_data['sess_len']
	time_spent=session_length
	app_data['sessionLength']=time_spent
	method = "post"
	url = 'http://app.adjust.com/event'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		"Content-Type" : "application/x-www-form-urlencoded",
		}
	params = {

		}
	data = {
		"event_buffering_enabled" : "0",
		"app_secret" : campaign_data.get('adjust').get('secret_key'),
		"event_token" : event_token,
		"tracking_enabled" : "1",
		"needs_response_details" : "1",
		"time_spent" : time_spent,
		"os_name" : "android",
		"device_type" : device_data.get('device_type'),
		"device_manufacturer" : device_data.get('manufacturer'),
		"connectivity_type" : "1",
		"package_name" : campaign_data.get('package_name'),
		"environment" : "production",
		"os_version" : device_data.get('os_version'),
		"android_uuid" : app_data.get('android_uuid'),
		"display_width" : device_data.get('resolution').split('x')[1],
		"screen_density" : get_screen_density(device_data),
		"subsession_count" : app_data.get('subsession_count'),
		"session_length" : session_length,
		"gps_adid" : device_data.get('adid'),
		"screen_size" : get_screen_size(device_data),
		"hardware_name" : device_data.get('hardware'),
		"gps_adid_src" : "service",
		"screen_format" : get_screen_format(device_data),
		"display_height" : device_data.get('resolution').split('x')[0],
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"sent_at" : sent_at,
		"attribution_deeplink" : "1",
		"app_version" : campaign_data.get('app_version_name'),
		"language" : device_data.get('locale').get('language'),
		"event_count" : app_data.get('event_count'),
		"country" : device_data.get('locale').get('country'),
		"created_at" : created_at,
		"session_count" : app_data.get('session_count'),
		"device_name" : device_data.get('model'),
		"cpu_type" : device_data.get('cpu_abi'),
		"network_type" : "0",
		"api_level" : device_data.get('sdk'),
		"os_build" : device_data.get('build'),

		}
	if callback_params:
		data["callback_params"] = callback_params
	
	if partner_params:
		data["partner_params"] = partner_params

	if event_token=='thg4hs':
		try:
			proPrice=str(proPrice_list[app_data.get('index')])

		except:
			proPrice='220'

		data['revenue']= str(proPrice)+'.00000'
		data['currency']=app_data.get('currency')



	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data,device_data,created_at,sent_at):
	inc_(app_data,'subsession_count')
	method = "head"
	url = 'http://app.adjust.com/attribution'
	headers = {
		"Client-SDK" : campaign_data.get('adjust').get('sdk'),
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		}
	params = {
		"attribution_deeplink" : "1",
		"gps_adid" : device_data.get('adid'),
		"package_name" : campaign_data.get('package_name'),
		"event_buffering_enabled" : "0",
		"initiated_by" : "backend",
		"app_secret" : campaign_data.get('adjust').get('secret_key'),
		"tracking_enabled" : "1",
		"needs_response_details" : "1",
		"device_name" : device_data.get('model'),
		"environment" : "production",
		"os_version" : device_data.get('os_version'),
		"os_name" : "android",
		"gps_adid_src" : "service",
		"android_uuid" : app_data.get('android_uuid'),
		"device_type" : device_data.get('device_type'),
		"app_token" : campaign_data.get('adjust').get('app_token'),
		"app_version" : campaign_data.get('app_version_name'),
		"created_at" : created_at,
		"api_level" : device_data.get('sdk'),
		"sent_at" : sent_at,

		}
	data = {

		}

	
	message = bytearray(json.dumps(data))
	str_adj = json.dumps(data)+adj1(app_data,device_data,message)+str(time.time())+device_data.get('sdk')+"session"+campaign_data.get('adjust').get('sdk')
	sign= (util.sha256(str_adj)).upper()
	headers['Authorization']= 'Signature signature="'+sign+'",secret_id='+campaign_data.get('adjust').get('secret_id')+',algorithm="adj1",headers_id="1"'	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}




###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def get_screen_size(device_data):
	dots = device_data.get('dpi')
	if (dots <= 120):
		screen_size = "small"
	elif (dots <= 160):
		screen_size = "normal"
	elif (dots <= 240):
		screen_size = "large"
	else:
		screen_size ="xlarge"

	return screen_size

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())
	return app_data.get('android_uuid')


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def get_date(app_data,device_data):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_date_by_ts(app_data,device_data,ts):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('01/01/1975', '01/01/2000', random.random(), format='%d/%m/%Y')
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		# app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['mobile_number'] = random.choice(['6','7','8','9'])+util.get_random_string('decimal',9)
		app_data['user']['password']    = first_name+last_name+util.get_random_string('decimal',4)
	

####################################################
#
# 		ADJ 1
#
#########################################################
def adj1(app_data,device_data,message):
	digest = hmac.new(campaign_data['adjust']['secret_key'], msg=message, digestmod=hashlib.sha256).digest()
	signature = base64.b64encode(digest).decode()

	return signature	