# -*- coding: utf-8 -*-
import sys
from sdk import getsleep
reload(sys)
sys.setdefaultencoding('utf-8')

import random,time,datetime,uuid,json,urllib
from sdk import util, installtimenew, purchase
from sdk import NameLists
import Config, clicker

campaign_data = {
			'app_size' : 84.0,#72.0,#71.8,#71.7,#70.3, 70.2,#69.8,#71.1,#60.7,#60.8,
			'package_name' : 'jp.co.i-bec.happyhills',
			'app_version_code' : '9.6.1.0',#'9.6.0.0',#'9.5.1.0',#'9.5.0.0',#9.4.2.0, '9.4.1.0',#'9.4.0.0',#'9.3.4.0',#'9.3.3.0',#'9.3.2.0',#'9.3.1.0',#'9.3.0.3',#'9.2.4.0',#'9.2.3.0',#9.2.2.0, '9.2.1.0',#'9.1.1.0',#'9.1.0.0',#'9.0.3.0',#'9.0.2.0',#'9.0.1.0',#'8.12.2.0',#'8.11.0.0',
			'app_version_name' : '9.6.1',#'9.6.0',#'9.5.1',#'9.5.0',#9.4.2, '9.4.1',#'9.4.0',#'9.3.0',#'9.3.3',#'9.3.2',#'9.3.1',#'9.3.0',#'9.2.4',#'9.2.3',#9.2.2, '9.2.1',#'9.1.1',#'9.1.0',#'9.0.3',#'9.0.2',#'9.0.1',#'8.12.2',#'8.11.0',
			'app_name':u'ハッピーメール'.encode('utf-8'),
			'app_id':'521055533',
			'ctr':6,
			'tracker':'adjust',
			'adjust':
						{
							'app_token'     : 'wac6orlg4cg0',
							'sdk'           : 'ios4.18.1',#'ios4.17.1',#'ios4.17.3',#'ios4.17.1',#'ios4.17.0',#'ios4.15.0',
							'secret_key'	: '96020390713823239877622891481421110226',#'22196852671793379015241148061963389614',#'1284416034366890345373459983183309160',#'60916201213928921185571005363890318',#'1567443971674945394913381071164321829',#'18645177271709117507702105686188429987',
							'secret_id'		: '18',#'16',#'14',#'12',#'10',#'6',
							'app_updated_at': '2019-09-18T14:03:15.000Z',#'2019-09-02T08:25:52.000Z',#'2019-07-22T16:55:40.000Z',#'2019-07-09T15:38:29.000Z',#2019-06-19T16:36:38.000Z, '2019-06-03T15:00:02.000Z',#'2019-05-22T15:32:24.000Z',#'2019-05-15T15:05:01.000Z',#'2019-05-08T09:17:11.000Z',#'2019-04-24T13:34:24.000Z',#'2019-04-10T15:56:50.000Z',#'2019-03-25T16:11:37.000Z',#'2019-03-13T13:33:18.000Z',#'2019-03-05T08:00:48.000Z',#2019-02-25T16:15:59.000Z, '2019-02-19T13:11:59.000Z',#'2019-01-24T01:09:36.000Z',#'2019-01-15T07:49:20.000Z',#'2018-12-17T14:40:02.000Z',#'2018-12-10T08:57:29.000Z',#'2018-11-07T12:53:22.000Z+0530',#'2018-09-19T17:21:41.000Z',#'2018-07-30T11:20:15.000Z',
						},
			'purchase':{
						'1':'500.000',
						'2':'1000.000'
					},
			'supported_os':'9.0',
			'supported_countries': 'WW',
			'device_targeting':True,
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention':{
							1:70,
							2:68,
							3:66,
							4:64,
							5:61,
							6:59,
							7:57,
							8:52,
							9:50,
							10:47,
							11:45,
							12:43,
							13:40,
							14:37,
							15:35,
							16:31,
							17:30,
							18:28,
							19:26,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
			}
}


###########################################################
#														  #
#						INSTALL							  #
#														  #
###########################################################
def install(app_data, device_data):
	print '---------------------------Install---------------------------------'
	print "installing please wait"

	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")
	###########		Initialize		############
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()

	app_data['adjust_call_time']=int(time.time())	

	app_data['device_id']=str(uuid.uuid4())

	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	app_data['created_at'] = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	if not app_data.get('idfa_id') :
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else :
			app_data['idfa_id'] = device_data.get('idfa_id')
		
	
		
	if not app_data.get('install_receipt'):
		app_data['install_receipt']='MIISlgYJKoZIhvcNAQcCoIIShzCCEoMCAQExCzAJBgUrDgMCGgUAMIICNwYJKoZIhvcNAQcBoIICKASCAiQxggIgMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgCNMA0CAQoCAQEEBRYDMTcrMA0CAQsCAQEEBQIDBzSgMA0CAQ0CAQEEBQIDAdWIMA4CAQECAQEEBgIEHw6tLTAOAgEJAgEBBAYCBFAyNTMwDgIBEAIBAQQGAgQxo8RnMBACAQ8CAQEECAIGSbf0n/ZuMBECAQMCAQEECQwHOS42LjEuMDARAgETAgEBBAkMBzkuMS4wLjAwFAIBAAIBAQQMDApQcm9kdWN0aW9uMBgCAQQCAQIEEIBFtdZ5S7kRkPtXXgR9JdAwHAIBBQIBAQQUrwCm8nEF94YfIhz6qXgA2zjqISgwHgIBCAIBAQQWFhQyMDE5LTA5LTMwVDA1OjEyOjQwWjAeAgEMAgEBBBYWFDIwMTktMDktMzBUMDU6MTI6NDBaMB4CARICAQEEFhYUMjAxOS0wMS0xN1QwODoxNjo0OVowIAIBAgIBAQQYDBZqcC5jby5pLWJlYy5oYXBweWhpbGxzMEECAQcCAQEEOWFLL/bRWbRdnvbddfspCqAwd87x6wtvziEQ6MPE2WcTHDoRb/EjXAQM+xgFNkpGrWJMzn76dPgn2jBPAgEGAgEBBEfGAPQDKNuinPFj8rUS155sSlMegJE1OGLjYrhfc7gRRhsFXMKGEOU/B691x9IPdWL98A6my5bLTERlYvo0fJo2pGOLX2JEz6CCDmUwggV8MIIEZKADAgECAggO61eH554JjTANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xNTExMTMwMjE1MDlaFw0yMzAyMDcyMTQ4NDdaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClz4H9JaKBW9aH7SPaMxyO4iPApcQmyz3Gn+xKDVWG/6QC15fKOVRtfX+yVBidxCxScY5ke4LOibpJ1gjltIhxzz9bRi7GxB24A6lYogQ+IXjV27fQjhKNg0xbKmg3k8LyvR7E0qEMSlhSqxLj7d0fmBWQNS3CzBLKjUiB91h4VGvojDE2H0oGDEdU8zeQuLKSiX1fpIVK4cCc4Lqku4KXY/Qrk8H9Pm/KwfU8qY9SGsAlCnYO3v6Z/v/Ca/VbXqxzUUkIVonMQ5DMjoEC0KCXtlyxoWlph5AQaCYmObgdEHOwCl3Fc9DfdjvYLdmIHuPsB8/ijtDT+iZVge/iA0kjAgMBAAGjggHXMIIB0zA/BggrBgEFBQcBAQQzMDEwLwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcjA0MB0GA1UdDgQWBBSRpJz8xHa3n6CK9E31jzZd7SsEhTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHgYDVR0gBIIBFTCCAREwggENBgoqhkiG92NkBQYBMIH+MIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDYGCCsGAQUFBwIBFipodHRwOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQANphvTLj3jWysHbkKWbNPojEMwgl/gXNGNvr0PvRr8JZLbjIXDgFnf4+LXLgUUrA3btrj+/DUufMutF2uOfx/kd7mxZ5W0E16mGYZ2+FogledjjA9z/Ojtxh+umfhlSFyg4Cg6wBA3LbmgBDkfc7nIBf3y3n8aKipuKwH8oCBc2et9J6Yz+PWY4L5E27FMZ/xuCk/J4gao0pfzp45rUaJahHVl0RYEYuPBX/UIqc9o2ZIAycGMs/iNAGS6WGDAfK+PdcppuVsq1h1obphC9UynNxmbzDscehlD86Ntv0hgBgw2kivs3hi1EdotI9CO/KBpnBcbnoB7OUdFMGEvxxOoMIIEIjCCAwqgAwIBAgIIAd68xDltoBAwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTEzMDIwNzIxNDg0N1oXDTIzMDIwNzIxNDg0N1owgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjgaYwgaMwHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwDgYDVR0PAQH/BAQDAgGGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQBPz+9Zviz1smwvj+4ThzLoBTWobot9yWkMudkXvHcs1Gfi/ZptOllc34MBvbKuKmFysa/Nw0Uwj6ODDc4dR7Txk4qjdJukw5hyhzs+r0ULklS5MruQGFNrCk4QttkdUGwhgAqJTleMa1s8Pab93vcNIx0LSiaHP7qRkkykGRIZbVf1eliHe2iK5IaMSuviSRSqpd1VAKmuu0swruGgsbwpgOYJd+W+NKIByn/c4grmO7i77LpilfMFY0GCzQ87HUyVpNur+cmV6U/kTecmmYHpvPm0KdIBembhLoz2IYrF+Hjhga6/05Cdqa3zr/04GpZnMBxRpVzscYqCtGwPDBUfMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCA7rV4fnngmNMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEAoZBrGInny+kyyBMRoTfAupjYAytfLi0QpbJYSdcmqQ+IBeZ5bj3TMevXOOSXOYBLOyQS8IaRX4XsS5ozg4H0CXh4gI5rpNmdFl3mw1qwU4Yhs4m+AfXxV+428KJfzzWR0bt2eUZ73pInC8dhZmqFeD6eJZ69Fab0uFHgINbgmdLDO2hh9SgewCuTd35/PS7FELP97GD3FwX3LX0aRB0biNwTSjhqffiiYT2AQaQ1zTrp4gK5tFrz98T/qwJfvFv4s9eLWmwxWbZdZxtpBjRrMIC3dpahGcppcq4myHdoGlLzsvVkHjdUY0OEQta93IWDB6Iw2rVIEOCwxpVaE3S46w=='

	if not app_data.get('registration_flag'):
		app_data['registration_flag']=False

	if not app_data.get('unsubscribe_flag'):
		app_data['unsubscribe_flag']=False

	###########			Calls		############	
	print '\n\n-----------------------api_reproio----------------------------'
	adjust1=api_reproio(campaign_data, app_data, device_data)
	util.execute_request(**adjust1)

	time.sleep(random.randint(1,2))
	
	print '\n\n-----------------------ADJUST SESSION-----------------------------'
	adjust1=adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjust1)


	print '\n\n-----------------------ADJUST SDK-----------------------------'
	a1 = adjust_sdkClick(campaign_data, app_data, device_data)
	util.execute_request(**a1)

	time.sleep(random.randint(5,7))
	print "adjust attribution"
	request = adjust_attribution(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(5,7))
	print "adjust attribution"
	request = adjust_attribution(campaign_data, app_data, device_data,initiated_by='sdk')
	util.execute_request(**request)

	print "sdk_info"
	req=adjust_sdkinfo(campaign_data, app_data, device_data)
	util.execute_request(**req)

	if random.randint(1,100)<=90:
		time.sleep(random.randint(50,60))
		register_user(app_data,country='united states')
		print '\n\n-----------------------selfcall/register----------------------------'
		adjust1=selfcall_register(campaign_data, app_data, device_data)
		util.execute_request(**adjust1)

		print '\n\n-----------------------ADJUST EVENT-----------------------------'
		adjust4=adjust_event(campaign_data, app_data, device_data,'563slt')
		util.execute_request(**adjust4)
		app_data['registration_flag']=True

	if random.randint(1,100)<=5:
		time.sleep(random.randint(20,30))
		print '\n\n-----------------------ADJUST EVENT-----------------------------'
		adjust4=adjust_event(campaign_data, app_data, device_data,'pgip36')
		util.execute_request(**adjust4)
		app_data['unsubscribe_flag']=True
				

	###########		Finalize		############
	set_appCloseTime(app_data)
	return {'status':True}

def open(app_data, device_data,day=1):

	if not app_data.get('times'):
		print "installing please wait"
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")
	

	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()

	if not app_data.get('device_id'):
		app_data['device_id']=str(uuid.uuid4())

	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;

	if not app_data.get('install_receipt'):
		app_data['install_receipt']='MIISlgYJKoZIhvcNAQcCoIIShzCCEoMCAQExCzAJBgUrDgMCGgUAMIICNwYJKoZIhvcNAQcBoIICKASCAiQxggIgMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgCNMA0CAQoCAQEEBRYDMTcrMA0CAQsCAQEEBQIDBzSgMA0CAQ0CAQEEBQIDAdWIMA4CAQECAQEEBgIEHw6tLTAOAgEJAgEBBAYCBFAyNTMwDgIBEAIBAQQGAgQxo8RnMBACAQ8CAQEECAIGSbf0n/ZuMBECAQMCAQEECQwHOS42LjEuMDARAgETAgEBBAkMBzkuMS4wLjAwFAIBAAIBAQQMDApQcm9kdWN0aW9uMBgCAQQCAQIEEIBFtdZ5S7kRkPtXXgR9JdAwHAIBBQIBAQQUrwCm8nEF94YfIhz6qXgA2zjqISgwHgIBCAIBAQQWFhQyMDE5LTA5LTMwVDA1OjEyOjQwWjAeAgEMAgEBBBYWFDIwMTktMDktMzBUMDU6MTI6NDBaMB4CARICAQEEFhYUMjAxOS0wMS0xN1QwODoxNjo0OVowIAIBAgIBAQQYDBZqcC5jby5pLWJlYy5oYXBweWhpbGxzMEECAQcCAQEEOWFLL/bRWbRdnvbddfspCqAwd87x6wtvziEQ6MPE2WcTHDoRb/EjXAQM+xgFNkpGrWJMzn76dPgn2jBPAgEGAgEBBEfGAPQDKNuinPFj8rUS155sSlMegJE1OGLjYrhfc7gRRhsFXMKGEOU/B691x9IPdWL98A6my5bLTERlYvo0fJo2pGOLX2JEz6CCDmUwggV8MIIEZKADAgECAggO61eH554JjTANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xNTExMTMwMjE1MDlaFw0yMzAyMDcyMTQ4NDdaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClz4H9JaKBW9aH7SPaMxyO4iPApcQmyz3Gn+xKDVWG/6QC15fKOVRtfX+yVBidxCxScY5ke4LOibpJ1gjltIhxzz9bRi7GxB24A6lYogQ+IXjV27fQjhKNg0xbKmg3k8LyvR7E0qEMSlhSqxLj7d0fmBWQNS3CzBLKjUiB91h4VGvojDE2H0oGDEdU8zeQuLKSiX1fpIVK4cCc4Lqku4KXY/Qrk8H9Pm/KwfU8qY9SGsAlCnYO3v6Z/v/Ca/VbXqxzUUkIVonMQ5DMjoEC0KCXtlyxoWlph5AQaCYmObgdEHOwCl3Fc9DfdjvYLdmIHuPsB8/ijtDT+iZVge/iA0kjAgMBAAGjggHXMIIB0zA/BggrBgEFBQcBAQQzMDEwLwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcjA0MB0GA1UdDgQWBBSRpJz8xHa3n6CK9E31jzZd7SsEhTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHgYDVR0gBIIBFTCCAREwggENBgoqhkiG92NkBQYBMIH+MIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDYGCCsGAQUFBwIBFipodHRwOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQANphvTLj3jWysHbkKWbNPojEMwgl/gXNGNvr0PvRr8JZLbjIXDgFnf4+LXLgUUrA3btrj+/DUufMutF2uOfx/kd7mxZ5W0E16mGYZ2+FogledjjA9z/Ojtxh+umfhlSFyg4Cg6wBA3LbmgBDkfc7nIBf3y3n8aKipuKwH8oCBc2et9J6Yz+PWY4L5E27FMZ/xuCk/J4gao0pfzp45rUaJahHVl0RYEYuPBX/UIqc9o2ZIAycGMs/iNAGS6WGDAfK+PdcppuVsq1h1obphC9UynNxmbzDscehlD86Ntv0hgBgw2kivs3hi1EdotI9CO/KBpnBcbnoB7OUdFMGEvxxOoMIIEIjCCAwqgAwIBAgIIAd68xDltoBAwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTEzMDIwNzIxNDg0N1oXDTIzMDIwNzIxNDg0N1owgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjgaYwgaMwHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwDgYDVR0PAQH/BAQDAgGGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQBPz+9Zviz1smwvj+4ThzLoBTWobot9yWkMudkXvHcs1Gfi/ZptOllc34MBvbKuKmFysa/Nw0Uwj6ODDc4dR7Txk4qjdJukw5hyhzs+r0ULklS5MruQGFNrCk4QttkdUGwhgAqJTleMa1s8Pab93vcNIx0LSiaHP7qRkkykGRIZbVf1eliHe2iK5IaMSuviSRSqpd1VAKmuu0swruGgsbwpgOYJd+W+NKIByn/c4grmO7i77LpilfMFY0GCzQ87HUyVpNur+cmV6U/kTecmmYHpvPm0KdIBembhLoz2IYrF+Hjhga6/05Cdqa3zr/04GpZnMBxRpVzscYqCtGwPDBUfMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCA7rV4fnngmNMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEAoZBrGInny+kyyBMRoTfAupjYAytfLi0QpbJYSdcmqQ+IBeZ5bj3TMevXOOSXOYBLOyQS8IaRX4XsS5ozg4H0CXh4gI5rpNmdFl3mw1qwU4Yhs4m+AfXxV+428KJfzzWR0bt2eUZ73pInC8dhZmqFeD6eJZ69Fab0uFHgINbgmdLDO2hh9SgewCuTd35/PS7FELP97GD3FwX3LX0aRB0biNwTSjhqffiiYT2AQaQ1zTrp4gK5tFrz98T/qwJfvFv4s9eLWmwxWbZdZxtpBjRrMIC3dpahGcppcq4myHdoGlLzsvVkHjdUY0OEQta93IWDB6Iw2rVIEOCwxpVaE3S46w=='
		
	app_data['created_at'] = datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	if not app_data.get('idfa_id') :
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else :
			app_data['idfa_id'] = device_data.get('idfa_id')
		
	

	if not app_data.get('unsubscribe_flag'):
		app_data['unsubscribe_flag'] = False

	if not app_data.get('registration_flag'):
		app_data['registration_flag']=False

	###########################open calls##############################

	
	print '\n\n-----------------------ADJUST SESSION-----------------------------'
	adjust1=adjust_session(campaign_data, app_data, device_data,event='open')
	util.execute_request(**adjust1)
	app_data['adjust_call_time']=int(time.time())

	if app_data.get('registration_flag')==False and random.randint(1,100)<=90:
		time.sleep(random.randint(50,60))
		register_user(app_data,country='united states')
		print '\n\n-----------------------selfcall/register----------------------------'
		adjust1=selfcall_register(campaign_data, app_data, device_data)
		util.execute_request(**adjust1)

		print '\n\n-----------------------ADJUST EVENT-----------------------------'
		adjust4=adjust_event(campaign_data, app_data, device_data,'563slt')
		util.execute_request(**adjust4)
		app_data['registration_flag']=True


	if app_data.get('registration_flag') == True and app_data.get('unsubscribe_flag') == False:

		if purchase.isPurchase(app_data,day,advertiser_demand=15):

			time.sleep(random.randint(20,30))
			randompurchase = random.randint(1,100)
			if randompurchase <= 90:
				num = "1"
			else:
				num = "2"
			
			print '\nAdjust : Event____________________________________'
			request=adjust_event(campaign_data, app_data, device_data,event_token='8s7qtx',purchase=num)
			util.execute_request(**request)




	if app_data.get('unsubscribe_flag')==False:
		if random.randint(1,100)<=10:
			time.sleep(random.randint(20,30))
			print '\n\n-----------------------ADJUST EVENT-----------------------------'
			adjust4=adjust_event(campaign_data, app_data, device_data,'pgip36')
			util.execute_request(**adjust4)
			app_data['unsubscribe_flag']=True
	set_appCloseTime(app_data)
	return {'status':True}

###########################################################
#														  #
#						ADJUST							  #
#														  #
###########################################################
def adjust_session(campaign_data, app_data, device_data,event='install'):
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding': 'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'User-Agent' : urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/758.3.15 Darwin/15.4.0' ,
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				
			  }
	params={}

	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1

	if not app_data.get('ios_UUID'):
		app_data['ios_UUID'] = str(uuid.uuid4())

	data = {
			'app_token':campaign_data.get('adjust').get('app_token'),
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'app_version': campaign_data.get('app_version_code'),
			'attribution_deeplink':1,
			'cpu_type':'CPU_SUBTYPE_ARM64_V8',
			'app_version_short': campaign_data.get('app_version_name'),
			'bundle_id':campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country'),
			'created_at': datetime.datetime.utcfromtimestamp((time.time()-0.04)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'device_name':device_data.get('device_platform'),
			'device_type':device_data.get('device_type'),
			'environment':'production',
			'event_buffering_enabled':	0,
			'hardware_name': device_data.get('build'),
			# 'ios_uuid':app_data.get('ios_UUID'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'language':device_data.get('locale').get('language'),
			'needs_response_details':'1',
			'os_name': 'ios',
			'os_version':device_data.get('os_version'),
			'sent_at': datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'session_count':str(app_data.get('session_count')),
			'tracking_enabled': '0',
			'persistent_ios_uuid':app_data.get('ios_UUID'),
			'os_build':device_data.get('build'),
			'connectivity_type':'2',
			'installed_at':datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'install_receipt': app_data.get('install_receipt'),
			'mcc'	: device_data.get('mcc'),
			'mnc'	: device_data.get('mnc'),
			'network_type'	: 'CTRadioAccessTechnologyLTE'
			}

	if event=='open':
		# timeSpent = int(time.time()*1000)-app_data.get('adjust_call_time')
		# if not app_data.get('timeSpent'):
		# 	app_data['timeSpent'] = timeSpent
		# else:
		# 	app_data['timeSpent'] = timeSpent

		if not app_data.get('subsession_count'):
			app_data['subsession_count'] = 1
		else:
			app_data['subsession_count'] += 1
		if app_data.get('Spent'):
			data['time_spent']=app_data.get('Spent')
			data['session_length']=app_data.get('Spent')

		else:
			data['time_spent']=0
			data['session_length']=0
		data['subsession_count']=app_data.get('subsession_count')
		data['last_interval']= get_lastInterval(app_data)
		

	headers['Authorization']= get_auth(app_data,activity_kind="session",idfa=data["idfa"],created_at=data["created_at"])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_sdkClick(campaign_data, app_data, device_data):
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	header = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}
	if not app_data.get('ios_UUID'):
		app_data['ios_UUID'] = str(uuid.uuid4())

	timeSpent = int(time.time())-app_data.get('adjust_call_time')

	data = {
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'details': '{"Version3.1":{"iad-attribution":"false"}}',
			'source': 'iad3',
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': get_date(app_data,device_data,early=random.uniform(1,3)),
			'connectivity_type':'2',
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'sent_at': get_date(app_data,device_data),
			'session_count': str(app_data.get('session_count')),
			'tracking_enabled':	0,
			'install_receipt': app_data.get('install_receipt'),
			'os_build':	device_data.get('build'),
			'last_interval': 1,
			'session_length': timeSpent,
			'subsession_count': 1,
			'time_spent' : timeSpent,
			'persistent_ios_uuid':app_data.get('ios_UUID'),
			'installed_at':datetime.datetime.utcfromtimestamp(int(app_data.get('times').get('install_complete_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'mcc'	: device_data.get('mcc'),
			'mnc'	: device_data.get('mnc'),
			'network_type'	: 'CTRadioAccessTechnologyLTE'

			
			}
	# if app_data.get('referrer'):
	# 	data['referrer'] = app_data.get('referrer')
	header['Authorization']= get_auth(app_data,activity_kind="click",idfa=data["idfa"],created_at=data["created_at"])
	return {'url':url, 'httpmethod':method, 'headers':header, 'params':None, 'data':data}	

def adjust_attribution(campaign_data, app_data, device_data, initiated_by='backend'):
	url = 'http://app.adjust.com/attribution'
	method='head'
	headers = {
			   'Client-Sdk':campaign_data.get('adjust').get('sdk'),
			   'User-Agent' : urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/758.3.15 Darwin/15.4.0',#'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
				'Accept-Encoding' : 'br, gzip, deflate',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
			   }
	params = {
			'app_token':campaign_data.get('adjust').get('app_token'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':1,
			'created_at':app_data.get('created_at'),
			'environment':'production',
			'event_buffering_enabled':0,
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'needs_response_details':'1',
			'initiated_by':initiated_by,
			'sent_at':datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'bundle_id': campaign_data.get('package_name'),
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'os_build':	device_data.get('build'),
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid':app_data.get('ios_UUID'),
			}
	headers['Authorization']= get_auth(app_data,activity_kind="attribution",idfa=params["idfa"],created_at=params["created_at"])
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

def adjust_event(campaign_data, app_data, device_data,event_token,event='install', purchase = None):
	url = 'http://app.adjust.com/event'
	method = 'post'
	headers = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Content-Type': 'application/x-www-form-urlencoded',
				'User-Agent' : urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/758.3.15 Darwin/15.4.0' ,
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
			  }
	params={}

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	if not app_data.get('subsession_count'):
		app_data['subsession_count'] = 1
	else:
		app_data['subsession_count'] += 1

	timeSpent = int(time.time())-app_data.get('adjust_call_time')

	


	# if not app_data.get('timeSpent'):
	# 	app_data['timeSpent'] = timeSpent
	# else:
	# 	app_data['timeSpent'] = timeSpent
	# if event=="open":
	# 	app_data['timeSpent'] = random.randint(50,100)

	data = {
			'attribution_deeplink':1,
			'cpu_type':'CPU_SUBTYPE_ARM64_V8',
			'app_token':campaign_data.get('adjust').get('app_token'),
			'app_version': campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'bundle_id':campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country'),
			'created_at': datetime.datetime.utcfromtimestamp((time.time()-0.04)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'device_name':device_data.get('device_platform'),
			'device_type':device_data.get('device_type'),
			'environment':'production',
			'hardware_name': device_data.get('build'),
			# 'ios_uuid':app_data.get('ios_UUID'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'language':device_data.get('locale').get('language'),
			'needs_response_details':'1',
			'os_name': 'ios',
			'os_version':device_data.get('os_version'),
			'sent_at': datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'session_count':str(app_data.get('session_count')),
			'tracking_enabled': '0',
			'time_spent':timeSpent-random.randint(2,5),
			'subsession_count':app_data.get('subsession_count'),
			'session_length': timeSpent,
			'event_count':app_data.get('event_count'),
			'event_token':event_token,
			'persistent_ios_uuid':app_data.get('ios_UUID'),
			'event_buffering_enabled':0,
			'os_build':device_data.get('build'),
			'connectivity_type':'2',
			'install_receipt': app_data.get('install_receipt'),
			'mcc'	: device_data.get('mcc'),
			'mnc'	: device_data.get('mnc'),
			'network_type'	: 'CTRadioAccessTechnologyLTE'

			}

	# if app_data.get('push_token'):
	# 	data['push_token']=app_data.get('push_token')

	if purchase:
		data['currency']='JPY'
		data['revenue']=campaign_data.get('purchase').get(purchase)

	app_data['Spent']=data['session_length']

	headers['Authorization']= get_auth(app_data,activity_kind="event",idfa=data["idfa"],created_at=data["created_at"])
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':params, 'data': data}

###################################################################
#
# 				DELIVER YOGI-----SELF CALLS
#
###################################################################


def api_reproio(campaign_data, app_data, device_data):
	url 	= 'http://api.reproio.com/v1/insights/config'
	method 	= 'post'
	headers = {
		'Content-Type': 'application/json',
		'Accept': 'application/json',
		'Accept-Encoding'	:'br, gzip, deflate',
		'User-Agent' : urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/897.15 Darwin/17.5.0',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		 }
	params= None
	data = {"device":device_data.get('device_platform'),
			"os":"ios",
			"locale":device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
			"is_first_launch":True,
			"idfv":app_data.get('idfv_id'),
			"os_version":device_data.get('os_version'),
			"token":"8942c977-15dc-4bf6-9d49-5c206525deb1",
			"sdk_version":"2.9.11",
			"user_annotation":""}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def selfcall_register(campaign_data, app_data, device_data):
	url 	= 'http://api.happymail.co.jp/happymailapp/hmap/html/9.0.0/register.php'
	method 	= 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		# 'Accept': 'application/json',
		'Accept-Encoding'	:'br, gzip, deflate',
		'User-Agent' : "%E3%83%8F%E3%83%83%E3%83%94%E3%83%BC%E3%83%A1%E3%83%BC%E3%83%AB/"+campaign_data.get('app_version_code')+" CFNetwork/"+device_data.get('cfnetwork').split('_')[0]+" Darwin/"+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'x-happy-user-agent': 'HappyHills/iOS/'+campaign_data.get('app_version_name')
		 }
	if app_data.get('user').get('sex')=='male':
		sex='1'
	else:
		sex='2'
	params= None
	data = {
			'device_id':app_data.get('device_id').upper(),
			'password':app_data.get('user').get('password'),
			'member_nickname':app_data.get('user').get('nickname'),
			'member_sex':sex,
			'member_residence':str(random.randint(1,9)),
			'member_age':str(random.randint(1,9)),
			'country_code':'0',
			'device_name':app_data.get('user').get('firstname')+"'s iPhone",
			'device_model':device_data.get('device_type'),
			'device_version':device_data.get('os_version')
			}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_sdkinfo(campaign_data, app_data, device_data):

	make_push_token(app_data)
	
	url = 'http://app.adjust.com/sdk_info'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding': 'br,gzip,deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'User-Agent': urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	
	params={}

	data = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink':	1,
			'created_at': get_date(app_data,device_data,early=random.uniform(1,2)),
			'environment':'production' ,			
			'event_buffering_enabled': 0,
			'gps_adid':device_data.get('adid'),
			'needs_response_details': 1,
			'push_token':make_push_token(app_data),
			'sent_at':	datetime.datetime.utcfromtimestamp((time.time())-app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'source':'push',
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'persistent_ios_uuid':app_data.get('ios_UUID'),
			#'tracking_enabled':	1,
			
			}

	
	headers['Authorization']= get_auth(app_data,activity_kind="info",idfa=data["idfa"],created_at=data["created_at"])

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

# def selfcall_login(campaign_data, app_data, device_data):
# 	url 	= 'https://api.happymail.co.jp/happymailapp/hmap/html/9.0.0/login.php'
# 	method 	= 'post'
# 	headers = {
# 		'Content-Type': 'application/x-www-form-urlencoded',
# 		# 'Accept': 'application/json',
# 		'Accept-Encoding'	:'br, gzip, deflate',
# 		'User-Agent' : "%E3%83%8F%E3%83%83%E3%83%94%E3%83%BC%E3%83%A1%E3%83%BC%E3%83%AB/"+campaign_data.get('app_version_code')+" CFNetwork/"+device_data.get('cfnetwork').split('_')[0]+" Darwin/"+device_data.get('cfnetwork').split('_')[1],
# 		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
# 		'x-happy-user-agent': 'HappyHills/iOS/'+campaign_data.get('app_version_name')
# 		 }
# 	params= None
# 	data = {
# 			'app_name' : campaign_data.get('app_name'),
# 			'app_version' : campaign_data.get('app_version_name'),
# 			'device_id':app_data.get('device_id').upper(),
# 			'device_ids' : '{"ios":{"idfv":"'+app_data.get('idfv_id')+'","idfa":"'+app_data.get('idfv_id')+'","adid":"'+app_data.get('aid')+'"}}',
# 			# json.dumps({"ios":{"idfv":app_data.get('idfv_id'),"idfa":app_data.get('idfa_id'),"adid":app_data.get('aid')}}),
# 			'password':app_data.get('user').get('password'),
# 			'device_name':app_data.get('user').get('firstname')+"'s iPhone",
# 			'device_push' : 1,
# 			'device_model':device_data.get('device_type'),
# 			'device_version':device_data.get('os_version'),
# 			'log' : '',
# 			'phone_no' : app_data.get('user').get('phone'),
# 			'resolution' : device_data.get('device_platform'),
# 			'sns_type' : 0
# 			}
# 	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}



###########################################################
#														  #
#						UTIL							  #
#														  #
###########################################################


def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((timestamp()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def timestamp():
	return time.time()

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_auth(app_data,activity_kind,idfa,created_at):   #activity_kind idfa app_secret created_at
	data1= campaign_data.get('adjust').get('secret_key')+activity_kind+idfa+created_at
	sign= util.sha256(data1)
	auth= 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+sign+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'
	return auth

#############################################
# 											#
# 				Extra Funcations 			#
# 											#
#############################################
	
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	# serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	device_ua = device_data.get('User-Agent')
	device_data['User-Agent'] = device_ua[:device_ua.find('Safari/')].strip()

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,gaid=device_data.get('idfa_id'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('app_id'))

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		# app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		# app_data['user']['lastname'] 	= last_name
		# app_data['user']['username'] 	= username
		# app_data['user']['email'] 		= username+"@gmail.com"
		app_data['user']['password'] 	= util.get_random_string(type='decimal',size=4)
		app_data['user']['nickname'] 	= util.get_random_string(type='decimal',size=9)
		# app_data['user']['phone']		= '5000'+util.get_random_string(type='decimal',size=7)


def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def make_push_token(app_data):
	if not app_data.get('push_token'):
		app_data['push_token']=str(util.get_random_string('hex',64))
	return app_data.get('push_token')