# -*- coding: utf-8 -*-
import sys
from sdk import getsleep
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import util,installtimenew
from sdk import NameLists
import time
import random
import json
import datetime
import urllib
import urlparse
import uuid
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'com.ebates',
	'ctr' :6,
	'app_name' 			 :'Ebates',
	'app_version_code'	 :'6040001',#'6030003',#5130001, '5090001',#'5080100', #5050001
	'app_version_name'	 :'6.4.0',#'6.3.0',#5.13.0, '5.11.0',#'5.9.0',#'5.8.1',#5.7.1, '5.7.0',#'5.6.0',#'5.5.0',#'4.36.0', #4.22.0
	'no_referrer'		 : True,
	'app_size'			: 32.0,#31.0, #34.0
	'supported_os'		 :'5.0',		
	'supported_countries':'WW',
	'device_targeting':True,
	'tracker':'adjust',
	'adjust':{
		'app_token'	: 'nyr2ptajub2g',
		'sdk'		: 'android4.14.0',	#android4.12.1
		'secret_key': '1186331776204248755719047075801777709832',
		'secret_id' : '1'

	},
	'branch':{
		'key':'key_live_pkg8pcYHl4Zj0WRqC6oEJacnrziifxxm',
		'sdk': 'android2.19.2',
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5,

	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############
	installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os='android',min_sleep=0)	
	set_sessionLength(app_data,forced=True,length=0)

	###########		CALLS		################	
	print 'please wait____________________'
	
	time.sleep(random.randint(60,100))

	session_createdAt=get_date(app_data,device_data)
	click_createdAt=get_date(app_data,device_data)
	attribution_createdAt=get_date(app_data,device_data)
	time.sleep(random.randint(1,2))
	session_sentAt=get_date(app_data,device_data)
	time.sleep(random.randint(1,2))
	click_sentAt=get_date(app_data,device_data)
	time.sleep(random.randint(1,2))
	attribution_sentAt=get_date(app_data,device_data)

	app_data['signup']=False
	app_data['login']=False

	if not app_data.get('user_id'):
		app_data['user_id']=util.get_random_string('hex',23).upper()

	print '\nBranch : INSTALL____________________________________'
	request=branch_install(campaign_data, app_data, device_data)
	util.execute_request(**request)

	app_data['push_token']=util.get_random_string('google_token',11)+':APA91B' +util.get_random_string('google_token',134)
	
	if app_data.get('referrer'):
		print '\nAdjust : SDK CLICK____________________________________'
		request=adjust_sdkclick(campaign_data, app_data, device_data, click_createdAt, click_sentAt,source='reftag')
		app_data['api_hit_time'] = time.time()
		util.execute_request(**request)

		print '\nAdjust : SDK CLICK____________________________________'
		request=adjust_sdkclick(campaign_data, app_data, device_data, click_createdAt, click_sentAt)
		app_data['api_hit_time'] = time.time()
		util.execute_request(**request)

	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data, session_createdAt, session_sentAt)
	util.execute_request(**request)

	time.sleep(random.randint(8,20))

	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data, attribution_createdAt, attribution_sentAt)
	util.execute_request(**request)

	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data, attribution_createdAt, attribution_sentAt, initiated_by = 'sdk')
	util.execute_request(**request)

	print '\nAdjust : sdk_info____________________________________'
	request=adjust_sdkinfo(campaign_data, app_data, device_data,t1=0,t2=0)
	util.execute_request(**request)

	request=measurement(campaign_data, app_data, device_data)
	util.execute_request(**request)

	

	if random.randint(1,100)<= 90:
		signup(campaign_data, app_data, device_data)
		app_data['signup']=True

		if random.randint(1,100)<= 95:
			for i in range(1):
				print "\nself_call_apituner_ecbsn\n"
				request=apituner_ecbsn( campaign_data, device_data, app_data )
				result=util.execute_request(**request)				
				global ids,names
				try:
					ids=[]
					names=[]
					store=json.loads(result.get('data')).get('store')
					for i in range(len(store)):
						if store[i]['id'] or store[i]['name']:											
							ids.append(store[i].get('id'))
							names.append(store[i].get('name'))
								
						else:
							print "else"
							ids=[1, 10000, 10006, 10014, 10017, 10019, 10021, 10027, 10028, 10030, 10045, 10056, 10061, 10064, 10067, 10071, 10079, 10080, 10086, 10087, 10088, 10089, 10093, 10097, 10101, 10103, 10104, 10105, 10107, 10123, 10124]
							names=['Amazon', 'Fairfield Inn', 'JW Marriott', 'Pottery Barn', 'Tre Religion', 'Backcontry.com', 'Betsey Johnson', 'The North Face', 'bebe', 'Virgin Atlantic Airways', 'Smashbox', 'Natralizer', 'HisRoom', 'Magazine Line', 'Cafe Britt', 'Bobbi Brown Cosmetics', 'Baseball Savings', 'PsPrint', 'Sams Clb', 'Holabird Sports', 'EXPRESS', 'Lord + Taylor', 'Mens Wearhose', 'Bowflex Shop', 'Natica', 'Microsoft Store', '24 Hor Fitness', 'TOMS', 'Cafe Press', 'Jelly Belly', 'Reebok']


					# print "-------------------------------------"
					# print ids
					# print "-------------------------------------"
					# print names
					print "-------------------------------------"
					print len(ids)
					print len(names)
					val=random.randint(0,len(ids)-1)
					print val
					app_data['id']=ids[val]
					app_data['name']=names[val]

					if app_data.get('id')==None or app_data.get('name')==None:
						ids=[1, 10000, 10006, 10014, 10017, 10019, 10021, 10027, 10028, 10030, 10045, 10056, 10061, 10064, 10067, 10071, 10079, 10080, 10086, 10087, 10088, 10089, 10093, 10097, 10101, 10103, 10104, 10105, 10107, 10123, 10124]
						names=['Amazon', 'Fairfield Inn', 'JW Marriott', 'Pottery Barn', 'Tre Religion', 'Backcontry.com', 'Betsey Johnson', 'The North Face', 'bebe', 'Virgin Atlantic Airways', 'Smashbox', 'Natralizer', 'HisRoom', 'Magazine Line', 'Cafe Britt', 'Bobbi Brown Cosmetics', 'Baseball Savings', 'PsPrint', 'Sams Clb', 'Holabird Sports', 'EXPRESS', 'Lord + Taylor', 'Mens Wearhose', 'Bowflex Shop', 'Natica', 'Microsoft Store', '24 Hor Fitness', 'TOMS', 'Cafe Press', 'Jelly Belly', 'Reebok']

						print len(ids)
						print len(names)
						val=random.randint(0,len(ids)-1)
						print val
						app_data['id']=ids[val]
						app_data['name']=names[val]


				except:
					print "except"
					ids=[1, 10000, 10006, 10014, 10017, 10019, 10021, 10027, 10028, 10030, 10045, 10056, 10061, 10064, 10067, 10071, 10079, 10080, 10086, 10087, 10088, 10089, 10093, 10097, 10101, 10103, 10104, 10105, 10107, 10123, 10124]
					names=['Amazon', 'Fairfield Inn', 'JW Marriott', 'Pottery Barn', 'Tre Religion', 'Backcontry.com', 'Betsey Johnson', 'The North Face', 'bebe', 'Virgin Atlantic Airways', 'Smashbox', 'Natralizer', 'HisRoom', 'Magazine Line', 'Cafe Britt', 'Bobbi Brown Cosmetics', 'Baseball Savings', 'PsPrint', 'Sams Clb', 'Holabird Sports', 'EXPRESS', 'Lord + Taylor', 'Mens Wearhose', 'Bowflex Shop', 'Natica', 'Microsoft Store', '24 Hor Fitness', 'TOMS', 'Cafe Press', 'Jelly Belly', 'Reebok']

					print len(ids)
					print len(names)
					val=random.randint(0,len(ids)-1)
					print val
					app_data['id']=ids[val]
					app_data['name']=names[val]


			for i in range(random.randint(1,3)):

				val=random.randint(0,len(ids)-1)
				app_data['id']=ids[val]
				app_data['name']=names[val]

				browse_shop(campaign_data, app_data, device_data)

		if random.randint(1,100)<=35:
			for i in range(random.randint(1,3)):
				raf_share(campaign_data, app_data, device_data)
	if app_data.get('signup')==False and random.randint(1,100)<=50:
		signup(campaign_data, app_data, device_data,call=False)
		app_data['signup']=True
		if random.randint(1,100)<=95:
			for i in range(1):
				print "\nself_call_apituner_ecbsn\n"
				request=apituner_ecbsn( campaign_data, device_data, app_data )
				result=util.execute_request(**request)				
				
				try:
					ids=[]
					names=[]
					store=json.loads(result.get('data')).get('store')
					for i in range(len(store)):
						if store[i]['id'] or store[i]['name']:				
							ids.append(store[i].get('id'))
							names.append(store[i].get('name'))
						else:
							print "else"
							ids=[1, 10000, 10006, 10014, 10017, 10019, 10021, 10027, 10028, 10030, 10045, 10056, 10061, 10064, 10067, 10071, 10079, 10080, 10086, 10087, 10088, 10089, 10093, 10097, 10101, 10103, 10104, 10105, 10107, 10123, 10124]
							names=['Amazon', 'Fairfield Inn', 'JW Marriott', 'Pottery Barn', 'Tre Religion', 'Backcontry.com', 'Betsey Johnson', 'The North Face', 'bebe', 'Virgin Atlantic Airways', 'Smashbox', 'Natralizer', 'HisRoom', 'Magazine Line', 'Cafe Britt', 'Bobbi Brown Cosmetics', 'Baseball Savings', 'PsPrint', 'Sams Clb', 'Holabird Sports', 'EXPRESS', 'Lord + Taylor', 'Mens Wearhose', 'Bowflex Shop', 'Natica', 'Microsoft Store', '24 Hor Fitness', 'TOMS', 'Cafe Press', 'Jelly Belly', 'Reebok']

					# print "-------------------------------------"
					# print ids
					# print "-------------------------------------"
					# print names
					print "-------------------------------------"
					print len(ids)
					print len(names)
					val=random.randint(0,len(ids)-1)
					print val
					app_data['id']=ids[val]
					app_data['name']=names[val]

					if app_data.get('id')==None or app_data.get('name')==None:
						ids=[1, 10000, 10006, 10014, 10017, 10019, 10021, 10027, 10028, 10030, 10045, 10056, 10061, 10064, 10067, 10071, 10079, 10080, 10086, 10087, 10088, 10089, 10093, 10097, 10101, 10103, 10104, 10105, 10107, 10123, 10124]
						names=['Amazon', 'Fairfield Inn', 'JW Marriott', 'Pottery Barn', 'Tre Religion', 'Backcontry.com', 'Betsey Johnson', 'The North Face', 'bebe', 'Virgin Atlantic Airways', 'Smashbox', 'Natralizer', 'HisRoom', 'Magazine Line', 'Cafe Britt', 'Bobbi Brown Cosmetics', 'Baseball Savings', 'PsPrint', 'Sams Clb', 'Holabird Sports', 'EXPRESS', 'Lord + Taylor', 'Mens Wearhose', 'Bowflex Shop', 'Natica', 'Microsoft Store', '24 Hor Fitness', 'TOMS', 'Cafe Press', 'Jelly Belly', 'Reebok']
						print len(ids)
						print len(names)
						val=random.randint(0,len(ids)-1)
						print val
						app_data['id']=ids[val]
						app_data['name']=names[val]


				except:
					print "except"
					ids=[1, 10000, 10006, 10014, 10017, 10019, 10021, 10027, 10028, 10030, 10045, 10056, 10061, 10064, 10067, 10071, 10079, 10080, 10086, 10087, 10088, 10089, 10093, 10097, 10101, 10103, 10104, 10105, 10107, 10123, 10124]
					names=['Amazon', 'Fairfield Inn', 'JW Marriott', 'Pottery Barn', 'Tre Religion', 'Backcontry.com', 'Betsey Johnson', 'The North Face', 'bebe', 'Virgin Atlantic Airways', 'Smashbox', 'Natralizer', 'HisRoom', 'Magazine Line', 'Cafe Britt', 'Bobbi Brown Cosmetics', 'Baseball Savings', 'PsPrint', 'Sams Clb', 'Holabird Sports', 'EXPRESS', 'Lord + Taylor', 'Mens Wearhose', 'Bowflex Shop', 'Natica', 'Microsoft Store', '24 Hor Fitness', 'TOMS', 'Cafe Press', 'Jelly Belly', 'Reebok']

					print len(ids)
					print len(names)
					val=random.randint(0,len(ids)-1)
					print val
					app_data['id']=ids[val]
					app_data['name']=names[val]

			for i in range(random.randint(1,3)):

				val=random.randint(0,len(ids)-1)
				app_data['id']=ids[val]
				app_data['name']=names[val]

				browse_shop(campaign_data, app_data, device_data)
		if random.randint(1,100)<=35:
			for i in range(random.randint(1,3)):
				raf_share(campaign_data, app_data, device_data)

	if app_data.get('signup')==True and random.randint(1,100)<=10:
		login(campaign_data, app_data, device_data)
		app_data['login']=True


	###########		FINALIZE	################
	set_appCloseTime(app_data)
	
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############	
	
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data['app_size'],os='android',min_sleep=0)
	session_createdAt=get_date(app_data,device_data)
	click_createdAt=get_date(app_data,device_data)
	time.sleep(random.randint(1,2))
	session_sentAt=get_date(app_data,device_data)
	time.sleep(random.randint(1,2))
	click_sentAt=get_date(app_data,device_data)

	if not app_data.get('push_token'):
		app_data['push_token']=util.get_random_string('google_token',11)+':APA91B' +util.get_random_string('google_token',134)
	
	if not app_data.get('signup'):
		app_data['signup']=False
	if not app_data.get('login'):
		app_data['login']=False

	if not app_data.get('user_id'):
		app_data['user_id']=util.get_random_string('hex',23).upper()

	###########		CALLS		################	
	if app_data.get('referrer'):
		print '\nAdjust : SDK CLICK____________________________________'
		request=adjust_sdkclick(campaign_data, app_data, device_data, click_createdAt, click_sentAt,isOpen=True)
		util.execute_request(**request)

	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data, session_createdAt, session_sentAt,isOpen=True)
	util.execute_request(**request)
	set_sessionLength(app_data,forced=True,length=0)

	if app_data.get('signup')==False:
		signup(campaign_data, app_data, device_data)
		app_data['signup']=True

	if app_data.get('signup')==True:
		if random.randint(1,100)<=85:
			for i in range(1):
				print "\nself_call_apituner_ecbsn\n"
				request=apituner_ecbsn( campaign_data, device_data, app_data )
				result=util.execute_request(**request)
				
				global ids,names
				try:
					ids=[]
					names=[]
					store=json.loads(result.get('data')).get('store')
					for i in range(len(store)):
						if store[i]['id'] or store[i]['name']:				
							ids.append(store[i].get('id'))
							names.append(store[i].get('name'))
						else:
							print "else"
							ids=[1, 10000, 10006, 10014, 10017, 10019, 10021, 10027, 10028, 10030, 10045, 10056, 10061, 10064, 10067, 10071, 10079, 10080, 10086, 10087, 10088, 10089, 10093, 10097, 10101, 10103, 10104, 10105, 10107, 10123, 10124]
							names=['Amazon', 'Fairfield Inn', 'JW Marriott', 'Pottery Barn', 'Tre Religion', 'Backcontry.com', 'Betsey Johnson', 'The North Face', 'bebe', 'Virgin Atlantic Airways', 'Smashbox', 'Natralizer', 'HisRoom', 'Magazine Line', 'Cafe Britt', 'Bobbi Brown Cosmetics', 'Baseball Savings', 'PsPrint', 'Sams Clb', 'Holabird Sports', 'EXPRESS', 'Lord + Taylor', 'Mens Wearhose', 'Bowflex Shop', 'Natica', 'Microsoft Store', '24 Hor Fitness', 'TOMS', 'Cafe Press', 'Jelly Belly', 'Reebok']

					# print "-------------------------------------"
					# print ids
					# print "-------------------------------------"
					# print names
					print "-------------------------------------"
					print len(ids)
					print len(names)
					val=random.randint(0,len(ids)-1)
					print val
					app_data['id']=ids[val]
					app_data['name']=names[val]

					if app_data.get('id')==None or app_data.get('name')==None:
						ids=[1, 10000, 10006, 10014, 10017, 10019, 10021, 10027, 10028, 10030, 10045, 10056, 10061, 10064, 10067, 10071, 10079, 10080, 10086, 10087, 10088, 10089, 10093, 10097, 10101, 10103, 10104, 10105, 10107, 10123, 10124]
						names=['Amazon', 'Fairfield Inn', 'JW Marriott', 'Pottery Barn', 'Tre Religion', 'Backcontry.com', 'Betsey Johnson', 'The North Face', 'bebe', 'Virgin Atlantic Airways', 'Smashbox', 'Natralizer', 'HisRoom', 'Magazine Line', 'Cafe Britt', 'Bobbi Brown Cosmetics', 'Baseball Savings', 'PsPrint', 'Sams Clb', 'Holabird Sports', 'EXPRESS', 'Lord + Taylor', 'Mens Wearhose', 'Bowflex Shop', 'Natica', 'Microsoft Store', '24 Hor Fitness', 'TOMS', 'Cafe Press', 'Jelly Belly', 'Reebok']
						print len(ids)
						print len(names)
						val=random.randint(0,len(ids)-1)
						print val
						app_data['id']=ids[val]
						app_data['name']=names[val]


				except:
					print "except"
					ids=[1, 10000, 10006, 10014, 10017, 10019, 10021, 10027, 10028, 10030, 10045, 10056, 10061, 10064, 10067, 10071, 10079, 10080, 10086, 10087, 10088, 10089, 10093, 10097, 10101, 10103, 10104, 10105, 10107, 10123, 10124]
					names=['Amazon', 'Fairfield Inn', 'JW Marriott', 'Pottery Barn', 'Tre Religion', 'Backcontry.com', 'Betsey Johnson', 'The North Face', 'bebe', 'Virgin Atlantic Airways', 'Smashbox', 'Natralizer', 'HisRoom', 'Magazine Line', 'Cafe Britt', 'Bobbi Brown Cosmetics', 'Baseball Savings', 'PsPrint', 'Sams Clb', 'Holabird Sports', 'EXPRESS', 'Lord + Taylor', 'Mens Wearhose', 'Bowflex Shop', 'Natica', 'Microsoft Store', '24 Hor Fitness', 'TOMS', 'Cafe Press', 'Jelly Belly', 'Reebok']

					print len(ids)
					print len(names)
					val=random.randint(0,len(ids)-1)
					print val
					app_data['id']=ids[val]
					app_data['name']=names[val]

			for i in range(random.randint(1,3)):

				val=random.randint(0,len(ids)-1)
				app_data['id']=ids[val]
				app_data['name']=names[val]

				browse_shop(campaign_data, app_data, device_data)
		if random.randint(1,100)<=25:
			for i in range(random.randint(1,3)):
				raf_share(campaign_data, app_data, device_data)

	if app_data.get('signup')==True and app_data.get('login')==False and random.randint(1,100)<=10:
		login(campaign_data, app_data, device_data)
		app_data['login']=True



	###########		FINALIZE	################
	set_appCloseTime(app_data)
	
	return {'status':'true'}

def apituner_ecbsn( campaign_data, device_data, app_data ):
	url= "https://apituner.ecbsn.com/apituner/v1/store/reward/list"
	method= "get"
	headers= {       'Accept': 'application/json',
        'Accept-Encoding': 'gzip',
        'Cookie': 'AWSALB='+util.get_random_string('all',29)+util.get_random_string('all',94),
        'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+'.1 (Android '+device_data.get('os_version')+' Phone; '+device_data.get('sdk')+')',
        'X-Eb-Key': util.get_random_string('all',15)+'+'+util.get_random_string('char_small',2)+'+'+util.get_random_string('char_all',3)+'/'+util.get_random_string('all',20)+'=',
        'X-Eb-TSync': util.base64(str(int(time.time())))}

	params= {       'channel': '3'}

	data= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################
def signup(campaign_data, app_data, device_data,call=True):
	print '\nAdjust : EVENT____________________________SIGN_UP'
	interval 		= random.randint(30,60)
	time.sleep(interval)
	set_sessionLength(app_data,length=interval)
	if call==True:
		Source="Onboarding_Tutorial"
		app_data['During_Tutorial']="Yes"
	else:
		Source="Settings"
		app_data['During_Tutorial']="No"
	app_data['method']=random.choice(['Regular','Facebook','Google'])
	callback_params = json.dumps({"Source":Source,"UserGUID":app_data.get('user_id'),"Tenant":device_data.get('locale').get('country').lower(),"Method":app_data.get('method'),"Has_Referrer":"false","During_Tutorial":app_data.get('During_Tutorial')})

	request=adjust_event(campaign_data, app_data, device_data,'e9exxu',callback_params, t1=15,t2=20)
	util.execute_request(**request)

	

def browse_shop(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT____________________________BROWSE_SHOP'
	interval 		= random.randint(15,30)
	time.sleep(interval)
	set_sessionLength(app_data,length=interval)
	
	callback_params = {"browser_agent":device_data.get('User-Agent'),"Preceding_Source":"Holiday_Mode_Featured_Content","tracking_ticket":"ebs22905955"+util.get_random_string('hex',5),"UserGUID":app_data.get('user_id'),"Source":"Store_Details","Tenant":device_data.get('locale').get('country').lower(),"Store_ID":str(app_data.get('id')).encode('utf-8')}

	if app_data.get('name'):
		if not app_data.get('name')[0]==" ":
			callback_params["Store_Name"]=app_data.get('name').decode('latin-1').encode('utf-8').strip()
	else:
		callback_params["Store_Name"]=app_data.get('name').decode('latin-1').encode('utf-8').strip()


	request=adjust_event(campaign_data, app_data, device_data,'hzr9l4',json.dumps(callback_params), t1=15,t2=20)
	util.execute_request(**request)


def raf_share(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT____________________________RAF_SHARE'
	interval 		= random.randint(15,20)
	time.sleep(interval)
	set_sessionLength(app_data,length=interval)
	shareby=random.choice(['share_facebook','share_sms','share_email'])
	callback_params = json.dumps({"Tenant":device_data.get('locale').get('country').lower(),"share_type":shareby})

	request=adjust_event(campaign_data, app_data, device_data,'5i4cjp',callback_params, t1=15,t2=20)
	util.execute_request(**request)


def login(campaign_data, app_data, device_data):
	print '\nAdjust : EVENT____________________________login'
	interval 		= random.randint(20,25)
	time.sleep(interval)
	set_sessionLength(app_data,length=interval)
	callback_params = json.dumps({"UserGUID":app_data.get('user_id'),"Method":app_data.get('method'),"Tenant":device_data.get('locale').get('country').lower(),"During_Tutorial":app_data.get('During_Tutorial')})

	request=adjust_event(campaign_data, app_data, device_data,'ficwz9',callback_params, t1=15,t2=20)
	util.execute_request(**request)
	


###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,created_at, sent_at,isOpen=False,callback_params=None):
	set_androidUUID(app_data)
	# inc_(app_data,'session_count')

	url 	= 'http://app.adjust.com/session'
	method 	= 'post'
	headers = {
		'Client-SDK'		: campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/x-www-form-urlencoded',
		'User-Agent'		: get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'connectivity_type'		:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'1',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:get_date_ts(app_data,device_data,get_ts=app_data.get('times').get('install_complete_time')),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'tracking_enabled'		:'1',
		'updated_at'			:get_date_ts(app_data,device_data,get_ts=app_data.get('times').get('install_complete_time')),
		'vm_isa'				:'arm',
		# 'mcc'                   :device_data.get('mcc'),
		# 'mnc'                   :device_data.get('mnc'),
		# 'push_token'            :app_data.get('push_token')
		}
	if isOpen:
		def_(app_data,'subsession_count')
		def_sessionLength(app_data)
		data['created_at'] 		= created_at
		data['last_interval'] 	= get_lastInterval(app_data)
		data['session_length'] 	= app_data.get('sessionLength')
		data['subsession_count']= app_data.get('subsession_count')
		data['time_spent'] 		= app_data.get('sessionLength')
		if app_data.get('push_token'):
			data['push_token']=app_data.get('push_token')

	headers['Authorization']= get_auth(app_data,activitykind='session',created_at=data['created_at'],gps_adid=data['gps_adid'])	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,callback_params=None,partner_params=None,t1=0,t2=0):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	set_androidUUID(app_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'event_count')
	def_sessionLength(app_data)

	url 	= 'http://app.adjust.com/event'
	method 	= 'post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'country'				:device_data.get('locale').get('country'),
		'connectivity_type'		:'1',
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			: created_at,
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'event_count'			:app_data.get('event_count'),
		'event_token'			:event_token,
		'environment'			:'production',
		'event_buffering_enabled':'1',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'session_length'		:app_data.get('sessionLength'),
		'subsession_count'		:app_data.get('subsession_count'),
		'tracking_enabled'		:'1',
		'time_spent'			:app_data.get('sessionLength'),
		'vm_isa'				:'arm',
		# 'mcc'                   :device_data.get('mcc'),
		# 'mnc'                   :device_data.get('mnc'),
		'push_token'            :app_data.get('push_token')
		}
	if callback_params:
		data['callback_params']	= callback_params

	headers['Authorization']= get_auth(app_data,activitykind='event',created_at=data['created_at'],gps_adid=data['gps_adid'])
		
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_sdkinfo(campaign_data, app_data, device_data,t1=0,t2=0):
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
	# def_ua(app_data,device_data)
	set_androidUUID(app_data)

	url = 'http://app.adjust.com/sdk_info'
	method='post'
	headers = {
		'Client-SDK':campaign_data.get('adjust').get('sdk'),
		'User-Agent':get_ua(device_data),
		'Connection':'Keep-Alive',
		'Accept-Encoding':'gzip',	
		'Content-Type': 'application/x-www-form-urlencoded',
		 }
	data = {
		'app_token': campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink': '1',
		'created_at': created_at,
		'environment':	'production',
		'event_buffering_enabled': '1',
		'gps_adid':	device_data.get('adid'),
		'needs_response_details':	'1',
		'push_token': app_data.get('push_token'),
		'sent_at':	sent_at,
		'source':	'push',
		'tracking_enabled':	'1'

		}
	headers['Authorization'] = get_auth(app_data,activitykind='info',created_at=data['created_at'],gps_adid=data['gps_adid'])
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data, created_at, sent_at,source='install_referrer',isOpen=''):
	set_androidUUID(app_data)
	# set_installedAT(app_data,device_data)
	def_(app_data,'subsession_count')
	inc_(app_data,'session_count')
	def_sessionLength(app_data)
	reftag = ''
	temp_b = urlparse.parse_qs(urllib.unquote(app_data.get('referrer')))
	if temp_b.get('adjust_reftag'):
		reftag = temp_b.get('adjust_reftag')[0]

	url 	= 'http://app.adjust.com/sdk_click'
	method 	='post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'click_time'			:get_date_ts(app_data,device_data,get_ts=app_data.get('times').get('click_time')),
		'country'				:device_data.get('locale').get('country'),
		'connectivity_type'		:'1',
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'1',
		'gps_adid'				:device_data.get('adid'),
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:get_date_ts(app_data,device_data,get_ts=app_data.get('times').get('install_complete_time')),
		'install_begin_time'	:get_date_ts(app_data,device_data,get_ts=app_data.get('times').get('download_end_time')),
		'language'				:device_data.get('locale').get('language'),
		'needs_response_details':'1',
		'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'referrer'				:urllib.unquote(app_data.get('referrer')),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'session_length'		:app_data.get('sessionLength'),
		'subsession_count'		:app_data.get('subsession_count'),
		'source'				:source,
		'time_spent'			:app_data.get('sessionLength'),
		'tracking_enabled'		:'1',
		'updated_at'			:get_date_ts(app_data,device_data,get_ts=app_data.get('times').get('install_complete_time')),
		'vm_isa'				:'arm',
		# 'mcc'                   :device_data.get('mcc'),
		# 'mnc'                   :device_data.get('mnc'),
		# 'push_token'            :app_data.get('push_token')
		}
	if isOpen:
		#del data['click_time']
		del data['install_begin_time']
		if app_data.get('push_token'):
			data['push_token']=app_data.get('push_token')

	if source=='reftag':
		del data['install_begin_time']
		data['last_interval']='0'
		data['referrer']= urllib.unquote(app_data.get('referrer'))
		data['raw_referrer']= app_data.get('referrer')
		data['click_time']=get_date(app_data,device_data)

	headers['Authorization']= get_auth(app_data,activitykind='click',created_at=data['created_at'],gps_adid=data['gps_adid'])
		
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data, device_data, created_at, sent_at, initiated_by = 'backend'):
	url 	= 'http://app.adjust.com/attribution'
	method 	='head'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	params = {
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:created_at,
		'environment'			:'production',
		'event_buffering_enabled':'1',
		'gps_adid'				:device_data.get('adid'),
		'needs_response_details':'1',
		'sent_at'				:sent_at,
		'tracking_enabled'		:'1',
		'initiated_by'          : initiated_by,
		}

	headers['Authorization']= get_auth(app_data,activitykind='attribution',created_at=params['created_at'],gps_adid=params['gps_adid'])
		
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}

###################################################################
# branch_install()		: method
# parameter 			: campaign_data, app_data, device_data
#
###################################################################
def branch_install(campaign_data, app_data, device_data):

	
	url = 'http://api.branch.io/v1/install'
	method = 'post'
	headers = {
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'Accept-Encoding':'gzip',
		'User-Agent':get_ua(device_data),
		}
	data_value = {
		'screen_dpi': int(device_data.get('dpi')),
		'branch_key': campaign_data.get('branch').get('key'),
		'is_hardware_id_real': True,
		# 'external_intent_extra':'{"profile":0}',
		'hardware_id': device_data.get('android_id'),
		'os_version': int(device_data.get('sdk')),
		'app_version': campaign_data.get('app_version_name'),
		'brand': device_data.get('brand'),
		'screen_width': int(device_data.get('resolution').split('x')[1]),
		'is_referrable': 1,
		'update': 0,
		'sdk': campaign_data.get('branch').get('sdk'),
		'wifi': True,
		'retryNumber': 0,
		'lat_val': 0,
		'debug': False,
		'metadata':{},
		'model': device_data.get('model'),
		'os': 'Android',
		'screen_height': int(device_data.get('resolution').split('x')[0]),
		'google_advertising_id':device_data.get('adid'),
		'cd':{'mv':-1,'pn':campaign_data.get('package_name')},
		'country':device_data.get('locale').get('country'),
		'facebook_app_link_checked':False,
		'instrumentation':{'v1/install-qwt':'0'},
		'language':device_data.get('locale').get('language'),
		'local_ip':device_data.get('private_ip'),
		'clicked_referrer_ts':int(app_data.get('times').get('click_time')),
		'environment':'FULL_APP',
		'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'install_begin_ts':int(app_data.get('times').get('download_end_time')),
		'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
		'previous_update_time':0,
		'ui_mode':'UI_MODE_TYPE_NORMAL'


		}
	data = json.dumps(data_value)
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params': None, 'data': data}

def measurement(campaign_data, app_data, device_data):
	url='http://app-measurement.com/config/app/1%3A1024842314514%3Aandroid%3A86862844cf42b93c'
	method='head'
	headers={

	'Accept-Encoding':'gzip',
		'User-Agent':get_ua(device_data),
	}
	params={
		'app_instance_id':'a5908813695bd34b56f9b2e6284b87cc',
		'platform':'android',
		'gmp_version':13280
	}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params': params, 'data': None}

#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(user_name_list)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def set_installedAT(app_data,device_data):
	if not app_data.get('installed_at'):
		app_data['installed_at'] =  get_date(app_data,device_data,early=random.uniform(5,10))

def get_auth(app_data,activitykind,created_at,gps_adid):
	data1= str(created_at+campaign_data['adjust']['secret_key']+gps_adid+activitykind)	
	sign= util.sha256(data1)
	auth= 'Signature secret_id="'+campaign_data['adjust']['secret_id']+'",signature="'+sign+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind"'	
	return auth	

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_date_ts(app_data,device_data,get_ts=''):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp(get_ts+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date
