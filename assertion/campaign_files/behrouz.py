# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import util
from sdk import NameLists
from sdk import getsleep
import time
import random
import json
import uuid
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.behrouzbiryani',
	'app_size'		     : 22.0,
	'app_name' 			 :'Behrouz Biryani - Order Biryani Online',
	'app_version_name' 	 :'2.15',
	'ctr' 				 : 6,
	'tracker'			 : 'branch',
	'no_referrer' 		 : True,
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '5.0',
	'api_branch':{
	    'branchkey'  :'key_live_piw6ZGhJfrVXT3UboP85cddfzCm3Z375',
	    'sdk'        :'android3.2.0',
	    'sdk1'		 : 'web2.51.0'
	},	
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	

	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	app_data['identity']='96'+util.get_random_string('decimal',8)
	register_user(app_data)

	################generating_realtime_differences##################

	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	app_data['api_hit_time']=time.time()
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')

		if not app_data.get('device_fingerprint_id1') or not app_data.get('device_fingerprint_id1') or not app_data.get('device_fingerprint_id1'):
			app_data['device_fingerprint_id1']='613284344550945604'
			app_data['identity_id']='641581897171978611'
			app_data['session_id']='641581897189090110'	

	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'	

	item_list = ['King Size Biryani (Serves 2)', 'Kilo Biryani (Serves 4-5)', 'Kebab Koobideh', 'Biryani Combos', 'Shirin & Sherbet', 'Kormas & Curries']


	if random.randint(1,100) <= 95:

		for _ in range(random.randint(1,5)):

			time.sleep(random.randint(26,35))
			request=branch_event_standard( campaign_data, device_data, app_data , eventName='VIEW_ITEM', collection_name=random.choice(item_list))
			util.execute_request(**request)


		if random.randint(1,100) <= 45:

			time.sleep(random.randint(26,35))
			request=branch_event_standard( campaign_data, device_data, app_data , eventName='ADD_TO_CART')
			util.execute_request(**request)


		if random.randint(1,100) <= 20:

			time.sleep(random.randint(47,56))
			request=branch_open( campaign_data, device_data, app_data, call_type = 'short')
			util.execute_request(**request)

			time.sleep(random.randint(2,4))
			request=branch_pageView( campaign_data, device_data, app_data )
			util.execute_request(**request)


		if not app_data.get('registration') and random.randint(1,100) <= 70:

			time.sleep(random.randint(107,120))
			request=branch_event_standard( campaign_data, device_data, app_data , eventName='COMPLETE_REGISTRATION', cal_seq=1)
			util.execute_request(**request)

			time.sleep(random.randint(2,3))
			request=branch_custom( campaign_data, device_data, app_data )
			util.execute_request(**request)

			request=api_branch_profile( campaign_data, device_data, app_data )
			util.execute_request(**request)

			request=branch_event_standard( campaign_data, device_data, app_data , eventName='COMPLETE_REGISTRATION', cal_seq=2)
			util.execute_request(**request)

			app_data['registration'] = True


		if random.randint(1,100) <= 70:

			for _ in range(random.randint(1,2)):

				if app_data.get('registration'):

					item = random.choice(item_list)

					for _ in range(3):

						time.sleep(random.randint(26,35))
						request=branch_event_standard( campaign_data, device_data, app_data , eventName='VIEW_ITEM', collection_name=item)
						util.execute_request(**request)

				else:

					time.sleep(random.randint(26,35))
					request=branch_event_standard( campaign_data, device_data, app_data , eventName='VIEW_ITEM', collection_name=random.choice(item_list))
					util.execute_request(**request)


			if random.randint(1,100) <= 50:

				time.sleep(random.randint(26,35))
				request=branch_event_standard( campaign_data, device_data, app_data , eventName='ADD_TO_CART')
				util.execute_request(**request)


		if random.randint(1,100) <= 50:

			time.sleep(random.randint(47,56))
			request=branch_open( campaign_data, device_data, app_data )
			util.execute_request(**request)

	time.sleep(random.randint(10,30))
	request = api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)

	###########		CALLS		############	
	
	return {'status':'true'}

#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):

	return {'status':'true'}

def test(app_data, device_data):	
		###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	app_data['identity']='96'+util.get_random_string('decimal',8)
	register_user(app_data)

	################generating_realtime_differences##################

	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	app_data['api_hit_time']=time.time()
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'	

	time.sleep(random.randint(26,35))
	request=branch_event_standard( campaign_data, device_data, app_data , eventName='VIEW_ITEM', collection_name='King Size Biryani (Serves 2)')
	util.execute_request(**request)

	time.sleep(random.randint(13,17))
	request=branch_event_standard( campaign_data, device_data, app_data , eventName='VIEW_ITEM', collection_name='Kilo Biryani (Serves 4-5)')
	util.execute_request(**request)

	time.sleep(random.randint(2,4))
	request=branch_event_standard( campaign_data, device_data, app_data , eventName='VIEW_ITEM', collection_name='Kebab Koobideh')
	util.execute_request(**request)

	time.sleep(random.randint(38,43))
	request=branch_event_standard( campaign_data, device_data, app_data , eventName='VIEW_ITEM', collection_name='Biryani Combos')
	util.execute_request(**request)

	time.sleep(random.randint(4,8))
	request=branch_event_standard( campaign_data, device_data, app_data , eventName='VIEW_ITEM', collection_name='Shirin & Sherbet')
	util.execute_request(**request)

	time.sleep(random.randint(47,56))
	request=branch_open( campaign_data, device_data, app_data )
	util.execute_request(**request)

	time.sleep(random.randint(2,4))
	request=branch_pageView( campaign_data, device_data, app_data )
	util.execute_request(**request)

	time.sleep(random.randint(107,120))
	request=branch_event_standard( campaign_data, device_data, app_data , eventName='COMPLETE_REGISTRATION', cal_seq=1)
	util.execute_request(**request)

	time.sleep(random.randint(2,3))
	request=branch_custom( campaign_data, device_data, app_data )
	util.execute_request(**request)

	request=api_branch_profile( campaign_data, device_data, app_data )
	util.execute_request(**request)

	request=branch_event_standard( campaign_data, device_data, app_data , eventName='COMPLETE_REGISTRATION', cal_seq=2)
	util.execute_request(**request)
	
	return {'status':'true'}


###################################################################
# BRANCH CALLS
# 
###################################################################

def api_branch_install(campaign_data,app_data,device_data):
	method='post'
	url='http://api2.branch.io/v1/install'

	headers = {
				'User-Agent'		:get_ua(device_data),
				'Accept-Encoding': 'gzip',
				'Content-Type':'application/json',
				'Accept':'application/json',
			}

	data={"app_version":campaign_data.get('app_version_name'),
		"facebook_app_link_checked":False,
		"is_referrable":1,
		"update":0,
		"debug":False,
		"metadata":{},
		"hardware_id":device_data.get('android_id'),
		# 'clicked_referrer_ts':int(app_data.get('times').get('click_time')),
		# 'install_begin_ts':int(app_data.get('times').get('download_begin_time')),
		"is_hardware_id_real":True,
		"brand":device_data.get('brand').upper(),
		"model":device_data.get('model'),
		"screen_dpi":int(device_data.get('dpi')),
		"screen_height":int(device_data.get('resolution').split('x')[0]),
		"screen_width":int(device_data.get('resolution').split('x')[1]),
		"wifi":True,
		"os":"Android",
		"os_version":int(device_data.get('sdk')),
		# "country":device_data.get('locale').get('country'),
		"language":device_data.get('locale').get('language'),
		"local_ip":device_data.get('private_ip'),
		"cd":{"mv":"-1","pn":campaign_data.get('package_name')},
		"google_advertising_id":device_data.get('adid'),
		"lat_val":0,
		"instrumentation":{"v1/install-qwt":"0"},
		"sdk":campaign_data.get('api_branch').get('sdk'),
		"retryNumber":0,
		"branch_key":campaign_data.get('api_branch').get('branchkey'),
		'environment':'FULL_APP',
		'latest_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'latest_update_time':int(app_data.get('times').get('install_complete_time')*1000),
		'first_install_time':int(app_data.get('times').get('install_complete_time')*1000),
		'ui_mode':'UI_MODE_TYPE_NORMAL',
		'previous_update_time':0
		} 

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}


def api_branch_logout( campaign_data, device_data, app_data ):
	url= "http://api2.branch.io/v1/logout"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data),}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand').upper(),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {"v1\/logout-qwt":"3","v1\/install-brtt":"4184"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {   },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk":  campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        # 'country': device_data.get('locale').get('country'),
        "wifi": True}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def api_branch_profile( campaign_data, device_data, app_data ):
	url= "http://api2.branch.io/v1/profile"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": get_ua(device_data),}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand').upper(),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity": app_data.get('identity'),
        "identity_id": app_data.get('identity_id'),

        "instrumentation": {"v2/event/custom-brtt":"331","v1/profile-qwt":"57"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": { },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),		
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        # 'country': device_data.get('locale').get('country'),
        "wifi": True}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def branch_event_standard( campaign_data, device_data, app_data, eventName , collection_name='', cal_seq=1):
	url= "http://api2.branch.io/v2/event/standard"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "content_items": [       {       "$creation_timestamp": int(time.time()*1000),
                                         "$locally_indexable": True,
                                         "$og_title": "COLLECTION VIEWED",
                                         "$publicly_indexable": True,
                                         "BRAND NAME": "Behrouz Biryani",
                                         "CLIENT SOURCE": "behrouz_biriyani",
                                         "COLLECTION NAME": collection_name}],
        "instrumentation": {       "v1/install-brtt": "437",
                                   "v2/event/standard-qwt": "1"},
        "metadata": {       },
        "name": eventName,
        "retryNumber": 0,
        "user_data": {       "aaid": device_data.get('adid'),
                             "android_id": device_data.get('android_id'),
                             "app_version": campaign_data.get('app_version_name'),
                             "brand": device_data.get('brand'),
                             "developer_identity": "bnc_no_value",
                             "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
                             "environment": "FULL_APP",
                             "language": device_data.get('locale').get('language'),
                             "limit_ad_tracking": 0,
                             "local_ip": device_data.get('private_ip'),
                             "model": device_data.get('model'),
                             "os": "Android",
                             "os_version": int(device_data.get('sdk')),
                             "screen_dpi": int(device_data.get('dpi')),
                             "screen_height": int(device_data.get('resolution').split('x')[0]),
                             "screen_width": int(device_data.get('resolution').split('x')[1]),
                             "sdk": "android",
                             "sdk_version": campaign_data.get('api_branch').get('sdk').split('android')[1],
                             "user_agent": device_data.get('User-Agent')}}


	if eventName == 'COMPLETE_REGISTRATION':
		del data['content_items'][0]['$og_title']
		del data['content_items'][0]['BRAND NAME']
		del data['content_items'][0]['CLIENT SOURCE']
		del data['content_items'][0]['COLLECTION NAME']

		data['instrumentation']={"v2/event/standard-brtt":"365","v2/event/standard-qwt":"0"}

		if cal_seq ==1:

			data['content_items'][0]['PROFILE_DATA']='LOGIN'

		if cal_seq==2:
			data['content_items'][0]['$canonical_identifier']='profile/'+str(app_data.get('identity'))
			data['content_items'][0]['$og_title']='PROFILE'
			data['content_items'][0]['EMAIL']=app_data.get('user').get('email')
			data['content_items'][0]['MOBILE NO']=app_data.get('identity')
			data['content_items'][0]['NAME']=app_data.get('user').get('firstname')

			data['instrumentation']={"v1/profile-brtt":"312","v2/event/standard-qwt":"342"}

	if eventName == 'ADD_TO_CART':
		if data.get('content_items'):
			del data['content_items']
		data['instrumentation']={"v2/event/standard-brtt":"298","v2/event/standard-qwt":"2"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def branch_custom( campaign_data, device_data, app_data ):
	url= "http://api2.branch.io/v2/event/custom"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent":  'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "custom_data": {       "LOGIN": app_data.get('identity')},
        "instrumentation": {       "v2/event/custom-qwt": "354",
                                   "v2/event/standard-brtt": "372"},
        "metadata": {       },
        "name": "STATUS",
        "retryNumber": 0,
        "user_data": {       "aaid": device_data.get('adid'),
                             "android_id":  device_data.get('android_id'),
                             "app_version": campaign_data.get('app_version_name'),
                             "brand":device_data.get('brand'),
                             "developer_identity": "bnc_no_value",
                             "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
                             "environment": "FULL_APP",
                             "language": device_data.get('locale').get('language'),
                             "limit_ad_tracking": 0,
                             "local_ip": device_data.get('private_ip'),
                             "model": device_data.get('model'),
                             "os": "Android",
                             "os_version": int(device_data.get('sdk')),
                             "screen_dpi":  int(device_data.get('dpi')),
                             "screen_height": int(device_data.get('resolution').split('x')[0]),
                             "screen_width": int(device_data.get('resolution').split('x')[1]),
                             "sdk": "android",
                              "sdk_version": campaign_data.get('api_branch').get('sdk').split('android')[1],
                             "user_agent": device_data.get('User-Agent')}}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def api_branch_close(campaign_data, app_data, device_data):	
	
	url='http://api2.branch.io/v1/close'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"session_id":app_data.get('session_id'),
			"app_version": campaign_data.get('app_version_name'),
			'google_advertising_id': device_data.get('adid'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand').upper(),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			# "country":device_data.get('locale').get('country'),
			"lat_val":0,
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/event-brtt":"343","v1/close-qwt":"1"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
			# 'country': device_data.get('locale').get('country'),
	}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

def branch_open( campaign_data, device_data, app_data, call_type = '' ):
	url= "http://api2.branch.io/v1/open"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        #"Accept-Language": device_data.get('locale').get('language')+","+device_data.get('locale').get('language')+"-"+device_data.get('locale').get('country')+";q=0.9",
        "Content-Type": "application/json",
        "User-Agent": 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
        }

	params= None

	if not app_data.get('browser_fingerprint_id'):
		app_data['browser_fingerprint_id'] = util.get_random_string('decimal',18)


	data = {
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"session_id":app_data.get('session_id'),
			"app_version": campaign_data.get('app_version_name'),
			'google_advertising_id': device_data.get('adid'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand').upper(),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			# "country":device_data.get('locale').get('country'),
			"lat_val":0,
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v1/event-brtt":"343","v1/close-qwt":"1"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
			# 'country': device_data.get('locale').get('country'),
			"cd":{"mv":"-1","pn":"com.behrouzbiryani"},
	}

	data = json.dumps(data)

	if call_type == 'short':

		headers['Origin'] = "https://www.behrouzbiryani.com"
		headers['Referer'] = "https://www.behrouzbiryani.com/the-story?order_source=behrouz_android"
		headers['Sec-Fetch-Mode'] = "cors"
		headers['Sec-Fetch-Site'] = "cross-site"
		headers['User-Agent'] = device_data.get('User-Agent')
		headers['X-Requested-With'] = "com.behrouzbiryani"
		headers['Accept-Language'] = device_data.get('locale').get('language')+","+device_data.get('locale').get('language')+"-"+device_data.get('locale').get('country')+";q=0.9"
		headers['Accept-Encoding'] = "gzip, deflate"
		headers['Content-Type'] = "application/x-www-form-urlencoded"
		headers['Accept'] = "*/*"

		data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
	        "browser_fingerprint_id": app_data.get('browser_fingerprint_id'),
	        "current_url": "https://www.behrouzbiryani.com/the-story?order_source=behrouz_android",
	        "options": "{}",
	        "sdk": campaign_data.get('api_branch').get('sdk1')}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

def branch_pageView( campaign_data, device_data, app_data ):
	url= "http://api2.branch.io/v1/pageview"
	method= "post"
	headers= {       "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language":device_data.get('locale').get('language')+","+device_data.get('locale').get('language')+"-"+device_data.get('locale').get('country')+";q=0.9",
        "Content-Type": "application/x-www-form-urlencoded",
        "Origin": "https://www.behrouzbiryani.com",
        "Referer": "https://www.behrouzbiryani.com/the-story?order_source=behrouz_android",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "cross-site",
        "User-Agent":   device_data.get('User-Agent'),
        "X-Requested-With": "com.behrouzbiryani"}

	params= None

	data= {       "branch_key":campaign_data.get('api_branch').get('branchkey'),
        "browser_fingerprint_id": app_data.get('browser_fingerprint_id'),
        "callback_string": "branch_view_callback__1",
        "data": '{"$canonical_url":"https://www.behrouzbiryani.com/the-story?order_source=behrouz_android","$og_title":"Behrouz Biryani - The Story","$og_description":"Order from a range of Royal Biryanis at Behrouz. Chicken Biryani, Veg Biryani & More. Order online and get it home delivered. Best dum Biryani near you","$og_image_url":"https://assets.faasos.io/behrouzbiryani-react/production/meta-img.jpg","$og_video":null,"$og_type":"website"}',
        "event": "pageview",
        "feature": "journeys",
        "has_app_websdk": "false",
        "identity_id": app_data.get('identity_id'),
        "metadata": '{"url":"https://www.behrouzbiryani.com/the-story?order_source=behrouz_android","user_agent":"'+device_data.get('User-Agent')+'","language":"'+device_data.get('locale').get('language')+'","screen_width":'+device_data.get('resolution').split('x')[1]+',"screen_height":'+device_data.get('resolution').split('x')[0]+',"window_device_pixel_ratio":2,"og_data":{"$og_title":"Behrouz Biryani - The Story","$og_description":"Order from a range of Royal Biryanis at Behrouz. Chicken Biryani, Veg Biryani & More. Order online and get it home delivered. Best dum Biryani near you","$og_image_url":"https://assets.faasos.io/behrouzbiryani-react/production/meta-img.jpg","$og_type":"website"},"title":"Behrouz Biryani - The Story","description":"Order from a range of Royal Biryanis at Behrouz. Chicken Biryani, Veg Biryani & More. Order online and get it home delivered. Best dum Biryani near you","canonical_url":"https://www.behrouzbiryani.com/"}',
        "open_app": "false",
        "sdk": campaign_data.get('api_branch').get('sdk1'),
        "session_id": app_data.get('session_id'),
        "source": "web-sdk",
        "user_language": device_data.get('locale').get('language')}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	
def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= first_name+"@gmail.com"
		app_data['user']['password']	= util.get_random_string('decimal',6)

def current_location(device_data):
	url='http://lumtest.com/myip.json'
	header={
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
			'Accept-Encoding': 'gzip, deflate',
			'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+','+device_data.get('locale').get('language')+';q=0.9',
			'Cache-Control': 'max-age=0',
			'Upgrade-Insecure-Requests': '1',
			'User-Agent': device_data.get('User-Agent')
	}
	params={}
	data={}
		
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}