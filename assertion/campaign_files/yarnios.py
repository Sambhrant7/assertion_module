# -*- coding: utf-8 -*-

import uuid,json,random,urlparse,time,datetime, urllib, string
from sdk import util,purchase, installtimenew, getsleep
import clicker,Config


campaign_data = {
			'package_name':'com.science-inc.Yarn',
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'ctr':6,
			'device_targeting':True,
			'app_name' :'Yarn',
			'app_id':'1195233335',
			'app_version_code': '1',#'91', 
			'app_version_name': '7.2.2',#'6.2.0',  
			'supported_countries': 'WW',
			'supported_os':'7.0',
			'app_size' : 99.2,
			'sdk' : 'ios',
			'tracker' : 'adjust',
			'adjust':{
				'app_token': 'rm42o2aj4u0w',
				'sdk': 'ios4.16.0',#'ios4.14.3',
				'app_updated_at' : '2019-10-11T03:36:05.000Z',#'2018-11-14T04:38:38.000Z',
	
			},
			
			'retention':{
							1:70,
							2:68,
							3:66,
							4:64,
							5:61,
							6:59,
							7:57,
							8:52,
							9:50,
							10:47,
							11:45,
							12:43,
							13:40,
							14:37,
							15:35,
							16:31,
							17:30,
							18:28,
							19:26,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5
						}
		}
def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	

def install(app_data, device_data):	

	print 'plz wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios',min_sleep=0)

	install_receipt()
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not device_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfv_id'] = device_data.get('idfv_id')

	app_data['user_id']=str(uuid.uuid4()).upper()

	def_(app_data,'session_count')
	def_(app_data,'subsession_count')
		
	make_sec(app_data,device_data)

	session_createdAt=get_date(app_data,device_data)
	time.sleep(random.randint(1,2))
	attribution_createdAt=get_date(app_data,device_data)	
	session_sentAt=get_date(app_data,device_data)	

	time.sleep(random.randint(4,8))
	attribution_sentAt=get_date(app_data,device_data)

	app_data['adjust_call_time']=int(time.time())

	print '\n\n-----------------------ADJUST SESSION-----------------------------'	
	adjust1=adjust_session(campaign_data, app_data, device_data,session_createdAt, session_sentAt)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjust1)

	time.sleep(random.randint(0,1))

	print '\n\n-----------------------ADJUST SDK CLICK-----------------------------'
	adjust2=adjust_sdkClick(campaign_data, app_data, device_data, source='deeplink')
	util.execute_request(**adjust2)	


	print '\n\n-----------------------ADJUST SDK CLICK-----------------------------'
	adjust2=adjust_sdkClick(campaign_data, app_data, device_data, source='iad3')
	util.execute_request(**adjust2)	


	time.sleep(random.randint(2,5))
	print "adjust attribution"
	adjust_attribution_1 = adjust_attribution(campaign_data, app_data, device_data,attribution_createdAt, attribution_sentAt)
	util.execute_request(**adjust_attribution_1)

	for i in range(0,4):
		time.sleep(random.randint(15,30))
		print '\n\n-----------------------ADJUST SDK CLICK-----------------------------'
		adjust2=adjust_sdkClick(campaign_data, app_data, device_data, source='deeplink')
		util.execute_request(**adjust2)	


	app_data['app_close_time'] = int(time.time())
	
	
	return {"status" : True}
	
def open(app_data,device_data,day=1):
	
	print 'plz wait...'
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios',min_sleep=0)

	install_receipt()
		
	if not app_data.get('idfa_id'):
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		if not device_data.get('idfv_id'):
			app_data['idfv_id'] = str(uuid.uuid4()).upper()
		else:
			app_data['idfv_id'] = device_data.get('idfv_id')

	if not app_data.get('user_id'):
		app_data['user_id']=str(uuid.uuid4()).upper()

	def_(app_data,'session_count')
	def_(app_data,'subsession_count')

	if not app_data.get('app_close_time'):
		app_data['app_close_time'] = int(time.time())

	if not app_data.get('adjust_call_time'):
		app_data['adjust_call_time']=int(time.time())


	session_createdAt=get_date(app_data,device_data)
	time.sleep(random.randint(1,2))
	attribution_createdAt=get_date(app_data,device_data)	
	session_sentAt=get_date(app_data,device_data)	

	time.sleep(random.randint(4,8))
	attribution_sentAt=get_date(app_data,device_data)


	print '\n\n-----------------------ADJUST SESSION-----------------------------'	
	adjust1=adjust_session(campaign_data, app_data, device_data,session_createdAt, session_sentAt, isOpen = True)
	util.execute_request(**adjust1)

	app_data['adjust_call_time']=int(time.time())

	print '\n\n-----------------------ADJUST SDK CLICK-----------------------------'
	adjust2=adjust_sdkClick(campaign_data, app_data, device_data, source='deeplink')
	util.execute_request(**adjust2)	


	if random.randint(1,100) <= 45:

		time.sleep(random.randint(20,60))
		print '\n\n Adjust Event : click on subscription'
		request = adjust_event(campaign_data, app_data, device_data,event_token = 'o955hz')
		util.execute_request(**request)


		if purchase.isPurchase(app_data,day,advertiser_demand=10): # to make overall purchase 10%

			time.sleep(random.randint(20,60))
			print '\n\n Adjust Event : subscription'
			request = adjust_event(campaign_data, app_data, device_data,event_token = 's2tmww')
			util.execute_request(**request)

			print '\n\n Adjust Event : subscription'
			request = adjust_event(campaign_data, app_data, device_data,event_token = 'dmi6nm')
			util.execute_request(**request)


	time.sleep(random.randint(0*60,5*60))

	app_data['app_close_time'] = int(time.time())
	
	return {"status" : True}


###########################################################
#														  #
#						ADJUST							  #
#														  #
###########################################################
def adjust_session(campaign_data, app_data, device_data,created_at, sent_at, isOpen = False):

	make_sec(app_data,device_data)

	inc_(app_data,'session_count')

	if not app_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	url = 'http://app.adjust.com/session'
	method = 'post'
	header = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'gzip, deflate, br',
		'User-Agent': campaign_data.get('app_name').replace(' ','%20')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	params={}

	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())		

	data = {			
			'app_token':campaign_data.get('adjust').get('app_token'),
			'app_updated_at' :campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'app_version':campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	'1',
			'bundle_id': campaign_data.get('package_name'),
			'connectivity_type' :2,
			'country': device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type') ,
			'created_at':created_at,
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment':	'production',
			'event_buffering_enabled':0,
			'hardware_name': device_data.get('build'),
			'idfa':	app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'installed_at' : datetime.datetime.fromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details':	'1',
			'os_build':	device_data.get('build'),
			'os_name': 'ios',
			'os_version':device_data.get('os_version'),
			'persistent_ios_uuid':	app_data.get('android_uuid'),
			'sent_at':sent_at,
			'session_count': app_data.get('session_count'),
			'tracking_enabled':	'1',
			'install_receipt' : install_receipt(),
			# 'queue_size':1,
			}

	data['callback_params'] = str(json.dumps({"localId":app_data.get('user_id'),"userId":app_data.get('user_id')}))


	if isOpen:
		data['session_length'] = app_data.get('app_close_time') - app_data.get('adjust_call_time')
		data['time_spent'] = app_data.get('app_close_time') - app_data.get('adjust_call_time')
		data['last_interval'] = int(time.time()) - app_data.get('app_close_time')
		data['subsession_count'] = app_data.get('subsession_count')

	
	
	return {'url':url, 'httpmethod':method, 'headers':header, 'params':params, 'data':data}	

def adjust_event(campaign_data, app_data, device_data,event_token,t1=0,t2=0,purchase_type=False):
	make_sec(app_data,device_data)

	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)
		
	url = 'http://app.adjust.com/event'
	method = 'post'
	header = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'gzip, deflate, br',
		'User-Agent': campaign_data.get('app_name').replace(' ','%20')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
	}

	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1	

	data = {
		'app_token': campaign_data.get('adjust').get('app_token'),
		# 'app_updated_at' :campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
		'app_version': campaign_data.get('app_version_code'),
		'app_version_short': campaign_data.get('app_version_name'),
		'attribution_deeplink':	'1',
		'bundle_id': campaign_data.get('package_name'),
		'connectivity_type' :2,
		'country': device_data.get('locale').get('country'),
		'cpu_type':	device_data.get('cpu_type'),
		'created_at':created_at,		
		'device_name':device_data.get('device_platform'),
		'device_type':	device_data.get('device_type'),
		'environment':	'production',
		'event_buffering_enabled':0,
		'event_count':	app_data.get('event_count'),
		'event_token':	event_token,
		'hardware_name': device_data.get('build'),
		'idfa':	app_data.get('idfa_id'),
		'idfv':	app_data.get('idfv_id'),
		'language':	device_data.get('locale').get('language'),
		'needs_response_details':	'1',
		'os_build': device_data.get('build'),
		'os_name':	'ios',
		'os_version': device_data.get('os_version'),
		'persistent_ios_uuid':	app_data.get('android_uuid'),
		'sent_at':sent_at,
		'session_count': str(app_data.get('session_count')),
		'session_length':int(time.time())- app_data.get('adjust_call_time'),
		'subsession_count':str(app_data.get('subsession_count')),
		'time_spent': int(time.time())- app_data.get('adjust_call_time'),
		'tracking_enabled':	'1',
		'install_receipt' : install_receipt(),
		
		}

	data['callback_params'] = str(json.dumps({"localId":app_data.get('user_id'),"userId":app_data.get('user_id')}))

	
	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':None, 'data': data}
	
def adjust_sdkClick(campaign_data, app_data, device_data, source='iad3'):

	inc_(app_data,'subsession_count')

	created_at=get_date(app_data,device_data)
	if source=='deeplink':
		click_time=get_date(app_data,device_data)

	sent_at=get_date(app_data,device_data)

	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	header = {				
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'gzip, deflate, br',
		'User-Agent': campaign_data.get('app_name').replace(' ','%20')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
	}

	data = {
		'app_token':campaign_data.get('adjust').get('app_token'),
		'app_updated_at' :campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'app_version':campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	'1',
			'bundle_id': campaign_data.get('package_name'),
			'connectivity_type' :2,
			'country': device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': created_at,
			'details' :'{"Version3.1":{"iad-attribution":"false"}}',
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment':	'production',
			'event_buffering_enabled':0,
			'hardware_name': device_data.get('build'),
			'idfa':	app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'installed_at': datetime.datetime.fromtimestamp(app_data.get('times').get('install_complete_time')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'last_interval':1,
			'language':	device_data.get('locale').get('language'),
			'needs_response_details':	'1',
			'os_build':	device_data.get('build'),
			'os_name': 'ios',
			'os_version':device_data.get('os_version'),
			'persistent_ios_uuid':	app_data.get('android_uuid'),
			'sent_at':sent_at,
			'session_count': app_data.get('session_count'),
			'tracking_enabled':	'1',
			'install_receipt' : install_receipt(),
			'source' :source,
			'session_length':0,
			'subsession_count' :app_data.get('subsession_count'),
			'time_spent':0
	}
	data['callback_params'] = str(json.dumps({"localId":app_data.get('user_id'),"userId":app_data.get('user_id')}))

	if source=='deeplink':
		data['click_time']=click_time
		data['deeplink']= 'yarn://google/link/?dismiss=1&is_weak_match=1'

		del data['details']

	
	return {'url':url, 'httpmethod':'post', 'headers':header, 'params':None, 'data':data}	

def adjust_attribution(campaign_data, app_data, device_data, created_at, sent_at):

	url = 'http://app.adjust.com/attribution'
	method = 'head'
	header = {
		'Client-Sdk':campaign_data.get('adjust').get('sdk'),
		'User-Agent': campaign_data.get('app_name').replace(' ','%20')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'gzip, deflate, br',
	}
	
	params= {       "app_token": campaign_data.get('adjust').get('app_token'),
        "app_version": campaign_data.get('app_version_code'),
        "app_version_short": campaign_data.get('app_version_name'),
        "attribution_deeplink": "1",
        "bundle_id": campaign_data.get('package_name'),
        "created_at": created_at,
        "device_name": device_data.get('device_platform'),
        "device_type": "iPhone",
        "environment": "production",
        "event_buffering_enabled": "0",
        "idfa": app_data.get('idfa_id'),
        "idfv": app_data.get('idfv_id'),
        "initiated_by": "backend",
        "needs_response_details": "1",
        "os_build": device_data.get('build'),
        "os_name": "ios",
        "os_version": device_data.get('os_version'),
        "persistent_ios_uuid": app_data.get('android_uuid'),
        "sent_at": sent_at}

	return {'url':url, 'httpmethod':method, 'headers':header, 'params':params, 'data':None}
	
############################################################
				# Extra Function								
############################################################

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = str(int(time.time()*1000))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = random.randint(1,5)	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def get_date(app_data,device_data,early=0):
	make_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def install_receipt():
	return 'MIISpwYJKoZIhvcNAQcCoIISmDCCEpQCAQExCzAJBgUrDgMCGgUAMIICSAYJKoZIhvcNAQcBoIICOQSCAjUxggIxMAoCARQCAQEEAgwAMAsCAQMCAQEEAwwBMTALAgETAgEBBAMMATEwCwIBGQIBAQQDAgEDMAwCAQ4CAQEEBAICAIswDQIBCgIBAQQFFgMxNyswDQIBDQIBAQQFAgMB/DYwDgIBAQIBAQQGAgRHPdA3MA4CAQkCAQEEBgIEUDI1MzAOAgELAgEBBAYCBAb+UagwDgIBEAIBAQQGAgQxqR7xMBACAQ8CAQEECAIGOVnfJ4mNMBQCAQACAQEEDAwKUHJvZHVjdGlvbjAYAgEEAgECBBCnDDFn/Gqp5AO90EysnrJdMBwCAQUCAQEEFM++fWo3bEaa5lrUqe4on2znh4EFMB4CAQICAQEEFgwUY29tLnNjaWVuY2UtaW5jLllhcm4wHgIBCAIBAQQWFhQyMDE5LTEwLTE3VDEyOjE1OjI1WjAeAgEMAgEBBBYWFDIwMTktMTAtMTdUMTI6MTU6MjVaMB4CARICAQEEFhYUMjAxOS0xMC0xN1QxMjoxNToyNVowTQIBBwIBAQRFacKbHSHYxN4XH8vOvveAjKt4q2Mjpmri0YGn9ioHmi4yDFfVcEeZjofkQRllNwFniynWvGbLsnpH7uPPuGphfWbPbyA9MGECAQYCAQEEWRnTZgglmSjTSxaUGhCY9sGseBkbwT55MkLKOReFdOedb6nOXRY/HZEO1pMZWsGbEktgE4sWIP9HOkKk5Z13xDa7wccuNUm04yFn+AwJgJ+9Jgt1iVYenRMmoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQCPHW94lm500HVLOTcZZuGtb99Na9VvWZ1KYM2gCgJFn57Rkl0DjNnc7nJbdYAZj4s6YYq+7puUGlChfWevzql8YB4Ai9N7UMkuzjBHpYpktCKeeA4is1LGWf4HBPM2obeHn7ZQ68AZLwRFnUpec+wH2eNPTsdyvuZaJWBvOfY07gjXSuLEebWgaUy4r2Y33eC84RjcdxxnVp6HavTpckf4Mm4tnRCuUIFfw6VIZ3AIxP0dB9Xvsjyh0qGaPu4zDRhkbdbMJeFKKzA0HL6FkhVzjQuDdplhnED+P2canYm+wYEteiS0cimdyFxz0mN248ZZh4f8GxVI04oXPWGtJ3/X'

def get_signature(campaign_data, app_data, device_data,app_secret=False,action=False,idfa=False,created_at=False):
	app_data['header_authorization']= 'Signature secret_id="'+campaign_data.get('adjust').get('secret_id')+'",signature="'+util.sha256(str(app_secret)+str(action)+str(idfa)+str(created_at))+'",algorithm="sha256",headers=" app_secret activity_kind idfa created_at"'
	return app_data.get('header_authorization')

