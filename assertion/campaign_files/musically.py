# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util
from sdk import NameLists
import time
import random
import base64
import json
import string
import urllib
import datetime
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.zhiliaoapp.musically',
	'app_name' 			 :'musically',
	'app_version_name' 	 : '13.2.11',#'13.1.3',#13.0.3, '12.8.3',#12.7.3, '12.6.5',#'12.4.5',#'12.3.6',#'12.3.5',#'12.2.4',#'12.1.5',#'12.0.3',#'11.9.4',#'11.9.3',#'11.8.3',#'11.8.2',#'11.8.1',#'11.6.4',#'11.2.3',#11.1.5,'11.1.3',#11.0.3
	'app_version_code' 	 : '2019092901',#'2019091803',#2019090808, '2019082609',#2019081910, '2019081314',#'2019080210',#'2019072516',#'2019072502',#'2019071548',#'2019071038',#'2019070139',#'2019062716',#'2019062627',#'2019062101',#'2019061824',#'2019061630',#'2019060448',#'2019051203',#2019050402, '2019043025', #2019042244
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'   : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'XY8Lpakui8g4kBcposRgxA',
		'dkh'		 : 'XY8Lpaku',
		'buildnumber': '4.8.20',
		'version'	 : 'v4',
	},
	'app_size':82.0,#83.0, 90.0,#93.0, 98.0,#100.0,#83.0,#88.0,#84.0,#83.0,#85.0,#79.0, #77.65, 80.0
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	################generating_realtime_differences##################
	
	global ts1, ts2

	ts1=int(time.time()*1000)
	time.sleep(1)
	ts2=int(time.time()*1000)

	def_firstLaunchDate(app_data,device_data)

	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])

	app_data['video_watched']=0
	app_data['liked_videos']=0
	app_data['login']=False
	app_data['user_id']='0'

	###########		CALLS		############
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)

	time.sleep(random.randint(1,2))
	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	response = util.execute_request(**request)
	catch(response,app_data,"appsflyer")

	time.sleep(random.randint(0,1))
	request=app_measurement( campaign_data, device_data, app_data )
	util.execute_request(**request)

	time.sleep(random.randint(0,1))
	mus_af_report_start_up(campaign_data, app_data, device_data)
	
	time.sleep(random.randint(4,8))
	mus_af_impression_1(campaign_data, app_data, device_data)

	if random.randint(1,100)<=75 and app_data.get('login')==False:
		time.sleep(random.randint(40,60))
		mus_af_onboarding_success(campaign_data, app_data, device_data)
		app_data['login']=True

	if random.randint(1,100)<=50:
		print '\n'+'Appsflyer : Track____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false", isOpen=True)
		util.execute_request(**request)

	for i in range(random.randint(2,5)):
		event=random.choice(['mus_af_follow','mus_af_comment','mus_af_post_video','mus_af_share_video_more'])
		if event == 'mus_af_follow':
			video_watch_event(campaign_data, app_data, device_data)

			video_like_event(campaign_data, app_data, device_data)

			if random.randint(1,100)<=80 and app_data['login']==True:
				time.sleep(random.randint(5,15))
				mus_af_follow(campaign_data, app_data, device_data)

		elif event == 'mus_af_comment':
			video_watch_event(campaign_data, app_data, device_data)

			video_like_event(campaign_data, app_data, device_data)

			if random.randint(1,100)<=73 and app_data['login']==True:
				time.sleep(random.randint(12,15))
				mus_af_comment(campaign_data, app_data, device_data)

		elif event == 'mus_af_post_video':
			video_watch_event(campaign_data, app_data, device_data)

			video_like_event(campaign_data, app_data, device_data)

			if random.randint(1,100)<=72 and app_data['login']==True:
				time.sleep(random.randint(15,180))
				mus_af_post_video(campaign_data, app_data, device_data)

		else:
			video_watch_event(campaign_data, app_data, device_data)

			video_like_event(campaign_data, app_data, device_data)

			if random.randint(1,100)<=53 and app_data['login']==True:
				time.sleep(random.randint(35,45))
				mus_af_share_video_more(campaign_data, app_data, device_data)

	if random.randint(1,100)<=50:
		print '\n'+'Appsflyer : Track____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false", isOpen=True)
		util.execute_request(**request)

	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	###########		INITIALIZE		############	
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)
	
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	################generating_realtime_differences##################

	global ts1, ts2, video_watched

	ts1=int(time.time()*1000)
	time.sleep(1)
	ts2=int(time.time()*1000)

	def_firstLaunchDate(app_data,device_data)

	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))

	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])

	app_data['video_watched']=0

	if not app_data.get('login'):
		app_data['login']=False

	app_data['user_id']='66852905630690'+util.get_random_string('decimal',5)

	###########		CALLS		############
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false", isOpen=True)
	util.execute_request(**request)

	time.sleep(random.randint(1,2))
	mus_af_report_start_up(campaign_data, app_data, device_data)

	if random.randint(1,100)<=75 and app_data.get('login')==False:
		time.sleep(random.randint(40,60))
		mus_af_onboarding_success(campaign_data, app_data, device_data)
		app_data['login']=True

	if random.randint(1,100)<=50:
		print '\n'+'Appsflyer : Track____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false", isOpen=True)
		util.execute_request(**request)

	for i in range(random.randint(2,5)):
		event=random.choice(['mus_af_follow','mus_af_comment','mus_af_post_video','mus_af_share_video_more'])
		if event == 'mus_af_follow':
			video_watch_event(campaign_data, app_data, device_data)

			if random.randint(1,100)<=73 and app_data['login']==True:
				time.sleep(random.randint(5,15))
				mus_af_follow(campaign_data, app_data, device_data)

		elif event == 'mus_af_comment':
			video_watch_event(campaign_data, app_data, device_data)

			if random.randint(1,100)<=66 and app_data['login']==True:
				time.sleep(random.randint(12,15))
				mus_af_comment(campaign_data, app_data, device_data)

		elif event == 'mus_af_post_video':
			video_watch_event(campaign_data, app_data, device_data)

			if random.randint(1,100)<=66 and app_data['login']==True:
				time.sleep(random.randint(15,180))
				mus_af_post_video(campaign_data, app_data, device_data)

		else:
			video_watch_event(campaign_data, app_data, device_data)

			if random.randint(1,100)<=46 and app_data['login']==True:
				time.sleep(random.randint(35,45))
				mus_af_share_video_more(campaign_data, app_data, device_data)

	if random.randint(1,100)<=50:
		print '\n'+'Appsflyer : Track____________________________________'
		request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="false", isOpen=True)
		util.execute_request(**request)

	return {'status':'true'}

def app_measurement( campaign_data, device_data, app_data ):
	url= "http://app-measurement.com/config/app/1%3A340331662088%3Aandroid%3A3c6c52c4762af402"
	method= "head"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent": get_ua(device_data),}

	params= {       "app_instance_id": "5929eb28c99b80c4b1858d1669fa9933",
        "gmp_version": "16091",
        "platform": "android"}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def mus_af_report_start_up(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_report_start_up'
	eventName		  = 'mus_af_report_start_up'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def mus_af_impression_1(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_impression_1'
	eventName		  = 'mus_af_impression_1'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def mus_af_onboarding_success(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_onboarding_success'
	eventName		  = 'mus_af_onboarding_success'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def mus_af_vv_5_uv(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_vv_5_uv'
	eventName		  = 'mus_af_vv_5_uv'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def mus_af_vv_10_uv(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_vv_10_uv'
	eventName		  = 'mus_af_vv_10_uv'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def mus_af_vv_20_uv(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_vv_20_uv'
	eventName		  = 'mus_af_vv_20_uv'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def mus_af_vv_30_uv(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_vv_30_uv'
	eventName		  = 'mus_af_vv_30_uv'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def mus_af_vv_40_uv(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_vv_40_uv'
	eventName		  = 'mus_af_vv_40_uv'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def mus_af_vv_50_uv(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_vv_50_uv'
	eventName		  = 'mus_af_vv_50_uv'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def mus_af_follow(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_follow'
	eventName		  = 'mus_af_follow'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def mus_af_like_video_1(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_like_video_1'
	eventName		  = 'mus_af_like_video_1'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def mus_af_comment(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_comment'
	eventName		  = 'mus_af_comment'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def mus_af_post_video(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_post_video'
	eventName		  = 'mus_af_post_video'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def mus_af_share_video_more(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mus_af_share_video_more'
	eventName		  = 'mus_af_share_video_more'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def mus_af_like_video_5(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________mmus_af_like_video_5'
	eventName		  = 'mus_af_like_video_5'
	eventValue		  = '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def video_watch_event(campaign_data, app_data, device_data):
	ch=random.randint(1,100)
	if ch<=85 and app_data.get('video_watched')==0:
		time.sleep(random.randint(30,60))
		mus_af_vv_5_uv(campaign_data, app_data, device_data)
		app_data['video_watched']=5

	if ch<=80 and app_data.get('video_watched')==5:
		time.sleep(random.randint(30,60))
		mus_af_vv_10_uv(campaign_data, app_data, device_data)
		app_data['video_watched']=10

	if ch<=75 and app_data.get('video_watched')==10:
		time.sleep(random.randint(30,60))
		mus_af_vv_20_uv(campaign_data, app_data, device_data)
		app_data['video_watched']=20

	if ch<=70 and app_data.get('video_watched')==20:
		time.sleep(random.randint(30,60))
		mus_af_vv_30_uv(campaign_data, app_data, device_data)
		app_data['video_watched']=30

	if ch<=65 and app_data.get('video_watched')==30:
		time.sleep(random.randint(30,60))
		mus_af_vv_40_uv(campaign_data, app_data, device_data)
		app_data['video_watched']=40

	if ch<=60 and app_data.get('video_watched')==40:
		time.sleep(random.randint(30,60))
		mus_af_vv_50_uv(campaign_data, app_data, device_data)
		app_data['video_watched']=50

def video_like_event(campaign_data, app_data, device_data):
	ch=random.randint(1,100)
	if app_data.get('login')==True:
		if ch<=93 and app_data.get('liked_videos')==0:
			time.sleep(random.randint(6,10))
			mus_af_like_video_1(campaign_data, app_data, device_data)
			app_data['liked_videos']=1

		if ch<=53 and app_data.get('liked_videos')==1:
			time.sleep(random.randint(10,15))
			mus_af_like_video_5(campaign_data, app_data, device_data)
			app_data['liked_videos']=5
	
###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"android_id" : device_data.get('android_id'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		# "cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		"deviceRm" : device_data.get('build'),
		"deviceType" : "user",
		"extraReferrers" : json.dumps({app_data.get('referrer'):"["+str(ts1)+","+str(ts2)+"]"}) if app_data.get('referrer') else None,
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"installer_package" : "com.android.vending",
		"isFirstCall" : "true",
		"isGaidWithGps" : "true",
		"ivc" : False,
		# "kef7368" : "Unknown Key",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_reactNative",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}

	# if app_data.get('referrer'):
	# 	data['extraReferrers']=json.dumps({urllib.unquote(app_data.get('referrer')):"["+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+"]","af_tranid=eABAclzsUxEdUzvHPmFjzA&af_siteid=1279_500_abc&pid=gameberry_int&c=VG_KR&af_click_lookback=7d&CLICK_ID=5b7d42ca73fba30001ade2a8":"["+ref_time()+"]","referrer=af_tranid%3DK1SbsSqwy9NT7pT8wa7qXA%26clickid%3D5b7d43a5a6ac77af0f44dc29%26af_siteid%3DAdscreen_DSPMV01ST%26af_c_id%3DVGG_AOS_KR%26af_ad%3Dmobdf407d7001b0543c%26pid%3Dadscreen_int%26af_click_lookback%3D7d%26af_adset%3Dmb_videogame_kr":"["+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+"]"})	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	# if app_data.get('installAttribution'):
	# 	data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)

	data['cksm_v1']=cksm_v1(data.get('installDate'), data.get('af_timestamp'))

	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey=get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))


	data["kef"+kefKey] = kefVal	

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_firstLaunchDate(app_data,device_data)
	def_appsflyerUID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())

 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"android_id" : device_data.get('android_id'),
		"appUserId" : "0",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		# "cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "no",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		"deviceRm" : device_data.get('build'),
		"deviceType" : "user",
		"extraReferrers" : json.dumps({app_data.get('referrer'):"["+str(ts1)+","+str(ts2)+"]"}) if app_data.get('referrer') else None,
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+"+0000",
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"ivc" : False,
		# "kef7d69" : "Unknown Key",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"open_referrer" : 'android-app://com.android.vending',
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_reactNative",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"rfr" : {
								"clk" : str(int(app_data.get("times").get("click_time"))),
								"code" : "0",
								"install" : str(int(app_data.get("times").get("download_begin_time"))),
								"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",
		},
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),

		}

	if app_data.get('counter')==2:
		data['gcd_timing']='6091'

	# if app_data.get('referrer'):
	# 	data['extraReferrers']=json.dumps({urllib.unquote(app_data.get('referrer')):"["+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+","+ref_time()+"]","af_tranid=eABAclzsUxEdUzvHPmFjzA&af_siteid=1279_500_abc&pid=gameberry_int&c=VG_KR&af_click_lookback=7d&CLICK_ID=5b7d42ca73fba30001ade2a8":"["+ref_time()+"]","referrer=af_tranid%3DK1SbsSqwy9NT7pT8wa7qXA%26clickid%3D5b7d43a5a6ac77af0f44dc29%26af_siteid%3DAdscreen_DSPMV01ST%26af_c_id%3DVGG_AOS_KR%26af_ad%3Dmobdf407d7001b0543c%26pid%3Dadscreen_int%26af_click_lookback%3D7d%26af_adset%3Dmb_videogame_kr":"["+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+','+ref_time()+"]"})			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	# if app_data.get('installAttribution'):
	# 	data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)

	if app_data.get('counter')==1:
		data['deviceData']['sensors']=[{"sT":1,"sV":"STMicroelectronics","sVE":[-0.15690613,-0.11767578,9.806656],"sVS":[-0.11767578,0.15690613,9.885101],"sN":"LIS3DH Accelerometer -Wakeup Secondary"},{"sT":2,"sV":"AKM","sVE":[39.48,25.439999,-102],"sVS":[40.739998,22.199999,-101.939995],"sN":"AK09911 Magnetometer"},{"sT":1,"sV":"STMicroelectronics","sVE":[-0.15690613,-0.11767578,9.806656],"sVS":[-0.11767578,0.15690613,9.885101],"sN":"LIS3DH Accelerometer"},{"sT":4,"sV":"akm","sVE":[0.003908246,0.020525152,-0.010573874],"sVS":[0,0,0],"sN":"AK09911-pseudo-gyro"}]

	data['cksm_v1']=cksm_v1(data.get('installDate'), data.get('af_timestamp'))

	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey=get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))

	data["kef"+kefKey] = kefVal	

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"device_id" : app_data.get('uid'),
		"devkey" : campaign_data.get('appsflyer').get('key'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def ref_time():
	#time.sleep(random.randint(10,15))
	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"


def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()

def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100
		
	sb = insert_char(sb,23,str(n3))
	return sb

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""
global temp, counter
temp=0
counter=1
def get_key_half(brand, sdk, lang, af_ts, buildnumber):
	if buildnumber=="4.8.18":
		return getKeyHalf_4_8_18(sdk, lang, af_ts)
	else:
	    c = '\u0000'
	    obj2 = af_ts[::-1]
	    out = calculate(sdk, obj2, brand)
	    i = len(out)
	    if i > 4:
	        i2 = globals()['counter']+65
	        globals()['temp'] = i2 % 128
	        if (1 if i2 % 2 != 0 else None) != 1:
	            out.delete(4, i)
	        else:
	            out.delete(5, i)
	    else:
	        while i < 4:
	            i3 = globals()['counter'] + 119
	            globals()['temp'] = i3 % 128
	            if (16 if i3 % 2 != 0 else 93) != 16:
	                i += 1
	                out+='1'
	            else:
	                i += 66
	                out+='H'
	            i3 = globals()['counter'] + 109
	            globals()['temp'] = i3 % 128
	            i3 %= 2
	    return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
    ts = ts[::-1]
    appends = [sdk,lang,ts]
    lengths = [len(sdk),len(lang),len(ts)]
    l = sorted(lengths)
    least = l[0]
    out = ""
    for x in range(least):
        numb = None
        for y in range(len(appends)):
            charAt = ord(appends[y][x])
            if numb:
                charAt = int((charAt ^ int(numb)))

            numb = charAt
            
        out+=str(hex(numb))[2:]

    if len(out)>4:
    	out = out[:4]
    elif len(out)<4:
    	while len(out)<4:
    		out+="1"
    
    return out

"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""

def generateValue(b,x,s,p,ts,fl,buildnumber):
    part1=generateValue1get(ts,fl,buildnumber)
    str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p,'utf-8')
    for i in range(len(str_)):
        str_[i] = int((str_[i] ^ ((i % 2) + 42)))

    stringBuilder = ""
    for toHexString in str_:
        toHexString2 = str(hex(int(toHexString)))[2:]
        if 1 == len(toHexString2):
            toHexString2 = "0"+str(toHexString2)
        stringBuilder+=toHexString2
    part2 = stringBuilder.__str__()
    return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
    gethash = sha256(ts+firstLaunch+buildnumber)
    return gethash[:16]

"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""

def calculate(sdk, obj, brand):
    allList = [sdk, obj, brand]
    arrayList = []
    i = 0
    while True:
        flag = 1
        if i >= 3:
            break
        i2 = globals()['temp'] + 63
        globals()['counter'] = i2 % 128
        if i2 % 2 != 0:
            flag = None
        if flag != None:
            arrayList.append(len(allList[i]))
            i += 31
        else:
            arrayList.append(len(allList[i]))
            i += 1
    sorted(arrayList)
    intValue = int(arrayList[0])
    out = ""
    i3 = 0
    while i3 < intValue:
        i4 = globals()['counter']+99
        globals()['temp'] = i4 % 128
        i5 =  globals()['temp']+67
        globals()['counter'] = i5 % 128
        i5 %= 2
        number = None
        if i4 % 2 != 0:
            a = 57
        else:
            a = 83
        if a != 83:
            i4 = 1
        else:
            i4 = 0
        while i4 < 3:
            i5 = globals()['temp'] + 87
            globals()['counter'] = i5 % 128
            i5 %= 2
            i5 = ord(allList[i4][i3])
            if (92 if number == None else 39) != 39:
                i6 = globals()['temp'] + 55
                globals()['counter'] = i6 % 128
                i6 %= 2
                i6 = globals()['counter'] + 39
                globals()['temp'] = i6 % 128
                i6 %= 2
            else:
                i5 ^= int(number)
            number = i5
            i4 += 1
        out+=str(hex(int(number)))[2:]
        i3 += 1
    return out