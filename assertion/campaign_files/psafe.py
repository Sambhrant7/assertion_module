# -*- coding: utf8 -*-
from sdk import util, installtimenew
from sdk import getsleep
from sdk import NameLists
import time
import random
import json
import string
import datetime
import urllib
import uuid, urlparse
import clicker
import Config


#.................CAMPAIGN_DATA DICT....#
#.................values are already mentioned.........#

# timezone="0000"
campaign_data = {

	'package_name'		 :'com.psafe.msuite',
	'app_name' 			 :'dfndr security',
	'app_version_code'	 :'53402',#53304,'52802',#52303, '51301', #51008
	'CREATE_DEVICE_MODE' : 3,
	'ctr'					: 6,
	'device_targeting'		: True,
	'tracker'			:'appsflyer',
	'app_version_name'	 :'5.34.2',#5.33.4, '5.28.2',#5.23.3, '5.13.1', #5.10.8
	'no_referrer'		 : True,
	'supported_os'		 :'4.4',		
	'supported_countries':'WW',
	'app_size' : 16.0,
	'appsflyer':{
		'key'		 : 'eFfrXRZeS6gmsxrFopAyrE',
		'dkh'		 : 'eFfrXRZe',
		'buildnumber': '4.8.19',#'4.8.13', #4.8.9
		'version'	 : 'v4',
	},
	'gmp_version':'15090',#'12872',
	'adserverapi_psafe':{
		'cid':'104488',
		'm':'34e68bc9de3180acd061d49e5865b708',
		'version':'1.5',
	},
	'mopub' : {
		'id' : '99c5e7c043174f57a85c41c369ffdd79',
		'nv' : '5.4.0',
		'sc' : '1.5'
	},	
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5,

	},
}



def install(app_data,device_data):
	
	print"\n PLEASE WAIT INSTALLLING__________________"
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	def_eventsRecords(app_data)

	######### Tracking for first tym	###########

	globals()['af_gcm_timestamp_t1'] = str(int(time.time()*1000))

	def_firstLaunchDate(app_data,device_data)

	print "gcm token call"
	req=gcmToken_call(campaign_data, app_data, device_data)
	res=util.execute_request(**req)
	try:
		app_data['gcm_token']=res.get('data').split('token=')[1]
		print app_data['gcm_token']
	except:
		app_data['gcm_token']='ePcACG-j7FQ:APA91bGdESSfn6uicbYcBhk97SkXdiR_VPJY1nzW9lyjmfpYNmzsgnZV38_23jcP6zCyOPxdLkJgvO3R9xf9Wz8Tb9dLJNFuPMdxSSqw-M0TtZ331kQxiCU0-3gJi-c5TkVSOnPAEy59'

	print"------------------------------------------------------"
	r=getpromotion(app_data,campaign_data,device_data)
	util.execute_request(**r)

	print"\n\n------------------------------------------------------"
	r=selfcall(app_data,campaign_data,device_data,calltype='updatecfgtime')
	util.execute_request(**r)

	print"\n\n------------------------------------------------------"
	r=selfcall(app_data,campaign_data,device_data,calltype='updatecfg')
	util.execute_request(**r)

	print"\n\n------------------------------------------------------"
	r=selfcall(app_data,campaign_data,device_data,calltype='dlen')
	util.execute_request(**r)

	print"\n\n------------------------------------------------------"
	r=selfcall(app_data,campaign_data,device_data,calltype='dlpt')
	util.execute_request(**r)

	print"\n\n------------------------------------------------------"
	r=selfcall(app_data,campaign_data,device_data,calltype='cardlist')
	util.execute_request(**r)

	print"\n\n------------------------------------------------------"
	r=selfcall(app_data,campaign_data,device_data,calltype='dialog_les')
	util.execute_request(**r)

	print '----------Crashlytics-----------------------'
	request = crashlytics(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(5,8))
	print "\n\n---------------------------------------------------------"
	r=userlogin(app_data,campaign_data,device_data)
	result=util.execute_request(**r)
	try:
		access_token=json.loads(result.get('data')).get('access_token')
	except:
		access_token='m5dla6v69384kq5j8rl39tvt5ju2r0duagvlf71nggkkbcgh9eh'

	print "\n\n---------------------------------------------------------"
	r=get_user_info(app_data,campaign_data,device_data,access_token)
	util.execute_request(**r)

	print "\n\n---------------------------------------------------------"
	r=ad_mopub_ad(campaign_data, app_data, device_data)
	resp=util.execute_request(**r)
	try:
		app_data['resp_decode']=json.loads(resp.get('data')).get('ad-responses')[0].get('metadata').get('x-before-load-url')
		# print "======================="
		# print app_data['resp_decode']
		# print "======================="
		# time.sleep(10)
	except:
		app_data['resp_decode']='https://ads.mopub.com/m/attempt?cid=3de6c2981bf5408fa003cd45a84e425f&country_code=IN&cppck=7D756&current_consent_status=unknown&gdpr_applies=0&id=99c5e7c043174f57a85c41c369ffdd79&logged_server_side=0&pf=1&pfs=&req=c14b93e7422a46239717dba668f2f7cd&reqt=1553848849.0&rtc=0'

	print "\n\n---------------------------------------------------------"
	r=ad_mopub_attempt(campaign_data, app_data, device_data)
	resp=util.execute_request(**r)

	time.sleep(random.randint(10,18))
	print"\n-------------------------------------------------------------------"
	r=configcall(app_data,campaign_data,device_data)
	util.execute_request(**r)

	# time.sleep(random.randint(10,18))
	# print"------------------------------------------------------"
	# r=first_run_psafe(app_data,campaign_data,device_data)
	# util.execute_request(**r)

	time.sleep(random.randint(10,18))
	print"------------------------------------------------------"
	r=getData_profile_psafe(app_data,campaign_data,device_data)
	util.execute_request(**r)

	print '\n Appsflyer :_______TRACK________'
	request=appsflyer_trackrequestt(campaign_data,app_data,device_data, isFirstCall = 'true')
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)

	time.sleep(random.randint(2,5))
	if random.randint(1,100)<=100:
		eventappopen(app_data,campaign_data,device_data)

		eventacceptterms(app_data,campaign_data,device_data)

		eventbackgroundsession(app_data,campaign_data,device_data)

		eventappalive(app_data,campaign_data,device_data)

		eventadview(app_data,campaign_data,device_data)

		if random.randint(1,100)<=100:
			time.sleep(random.randint(1,2))

			print "\n Appsflyer :____________________Register_________"
			request=appsflyer_register(campaign_data,app_data,device_data,count=1)
			util.execute_request(**request)

			print "\n Appsflyer :____________________Register_________"
			request=appsflyer_register(campaign_data,app_data,device_data,count=2)
			util.execute_request(**request)

			print "\n Appsflyer :____________________Register_________"
			request=appsflyer_register(campaign_data,app_data,device_data,count=3)
			util.execute_request(**request)

			print "\n Appsflyer :____________________Register_________"
			request=appsflyer_register(campaign_data,app_data,device_data,count=4)
			util.execute_request(**request)

			for i in range(0,2):
				print '\n Appsflyer :_______TRACK________'
				request=appsflyer_trackrequestt(campaign_data,app_data,device_data)
				util.execute_request(**request)

		if random.randint(1,100)<=100:
			for i in range(0,2):
				time.sleep(random.randint(5,10))
				eventscan(app_data,campaign_data,device_data)

	return{"status":True}


def open(app_data, device_data,day):	

	if not app_data.get('times'):
		print"\n PLEASE WAIT INSTALLLING__________________"
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	if not app_data.get('day1_retention'):
		app_data['day1_retention']= False

	if not app_data.get('day3_retention'):
		app_data['day3_retention']=False

	if not app_data.get('day7_retention'):
		app_data['day7_retention']=False


	print '\n Appsflyer :_______TRACK________'
	request=appsflyer_trackrequestt(campaign_data,app_data,device_data)
	util.execute_request(**request)

	if random.randint(1,100)<=90:

		eventappopen(app_data,campaign_data,device_data)

		eventappalive(app_data,campaign_data,device_data)

		eventadview(app_data,campaign_data,device_data)

		if app_data.get('day1_retention')==False:
			eventRetention1(app_data,campaign_data,device_data)
			app_data['day1_retention']=True

		eventbackgroundsession(app_data,campaign_data,device_data)

		if random.randint(1,100)<=70:			

			for i in range(0,2):
				time.sleep(random.randint(1,2))
				print '\n Appsflyer :_______TRACK________'
				request=appsflyer_trackrequestt(campaign_data,app_data,device_data)
				util.execute_request(**request)

		if random.randint(1,100)<=80:
			time.sleep(random.randint(5,10))
			eventscan(app_data,campaign_data,device_data)

		if random.randint(1,100)<=50:
			time.sleep(random.randint(15,20))
			eventsetupantitheft(app_data,campaign_data,device_data)		

		if day>=3 and app_data.get('day3_retention')==False:
			time.sleep(random.randint(1,2))
			eventRetention3(app_data,campaign_data,device_data)
			app_data['day3_retention']=True

		if day>=7 and app_data.get('day7_retention') == False:
			time.sleep(random.randint(1,2))
			eventRetention7(app_data,campaign_data,device_data)
			app_data['day7_retention']=True

	
	return {'status':True}	

##########################################################################################################################################
def eventappopen(app_data,campaign_data,device_data):
	print"\n\n Event app open----------"
	eventname="APP_OPEN"
	eventval='{}'
	request=appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName=eventname,eventValue=eventval,isFirstCall='true')
	util.execute_request(**request)


def eventacceptterms(app_data,campaign_data,device_data):
	print"\n\n Event ACCEPT_TERMS----------"
	eventname="ACCEPT_TERMS"
	eventval='{}'
	request=appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName=eventname,eventValue=eventval,isFirstCall='true')
	util.execute_request(**request)


def eventbackgroundsession(app_data,campaign_data,device_data):
	print"\n\n Event BACKGROUND_SESSION----------"
	eventname="BACKGROUND_SESSION"
	eventval='{}'
	request=appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName=eventname,eventValue=eventval)
	util.execute_request(**request)

def eventadview(app_data,campaign_data,device_data):
	print"\n\n Event Ad VIEW----------"
	eventname="af_ad_view"
	eventval=json.dumps({"ad_network":"null","af_adrev_ad_type":"null"})
	request=appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName=eventname,eventValue=eventval)
	util.execute_request(**request)

def eventappalive(app_data,campaign_data,device_data):
	print"\n\n Event APP ALIVE----------"
	eventname="APP_ALIVE"
	eventval='{}'
	request=appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName=eventname,eventValue=eventval)
	util.execute_request(**request)

def eventscan(app_data,campaign_data,device_data):
	print"\n\n Event scan----------"
	eventname="SCAN"
	eventval='{}'
	request=appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName=eventname,eventValue=eventval)
	util.execute_request(**request)	

def eventsetupantitheft(app_data,campaign_data,device_data):
	print"\n\n Event SETUP_ANTITHEFT----------"
	eventname="SETUP_ANTITHEFT"
	eventval='{}'
	request=appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName=eventname,eventValue=eventval)
	util.execute_request(**request)	

def eventRetention1(app_data,campaign_data,device_data):
	print"\n\n Event RETENTION 24 HOURS ----------"
	eventname="RETAINED_24HOURS"
	eventval='{}'
	request=appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName=eventname,eventValue=eventval,isFirstCall='true')
	util.execute_request(**request)
	

def eventRetention3(app_data,campaign_data,device_data):
	print"\n\n Event RETENTION 3 DAYS ----------"
	eventname="RETAINED_3DAYS"
	eventval='{}'
	request=appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName=eventname,eventValue=eventval,isFirstCall='true')
	util.execute_request(**request)
	

def eventRetention7(app_data,campaign_data,device_data):
	print"\n\n Event RETENTION 7 DAYS ----------"
	eventname="RETAINED_7DAYS"
	eventval='{}'
	request=appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName=eventname,eventValue=eventval,isFirstCall='true')
	util.execute_request(**request)
	
def ad_mopub_ad(campaign_data, app_data, device_data):
	url = 'https://ads.mopub.com/m/ad'
	method = 'post'
	headers = {
			'User-Agent': device_data.get('User-Agent'),
			'Accept-Encoding' : 'gzip',
			'accept-language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
			'Content-Type'	  : 'application/json; charset=UTF-8'
			
	}

	params = None

	data = {"cn":device_data.get('carrier'),
		"z":device_data.get('timezone'),
		"dnt":"0",
		"assets":"title,text,iconimage,mainimage,ctatext,starrating",
		"ct":"2",
		"gdpr_applies":"0",
		"h":device_data.get('resolution').split('x')[0],
		"current_consent_status":"unknown",
		"w":device_data.get('resolution').split('x')[1],
		"av":campaign_data.get('app_version_name'),
		"dn":device_data.get('brand').upper()+","+device_data.get('model')+","+device_data.get('product'),
		"mnc":device_data.get('mnc'),
		"bundle":campaign_data.get('package_name'),
		"o":"p",
		"id":campaign_data.get('mopub').get('id'),
		"udid":"ifa:"+device_data.get('adid'),
		"mcc":device_data.get('mcc'),
		"nv":campaign_data.get('mopub').get('nv'),
		"sc":campaign_data.get('mopub').get('sc'),
		"force_gdpr_applies":"0"}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def ad_mopub_attempt(campaign_data, app_data, device_data):
	url = 'https://ads.mopub.com/m/attempt'
	method = 'post'
	headers = {
			'User-Agent': device_data.get('User-Agent'),
			'Accept-Encoding' : 'gzip',
			'accept-language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
			'Content-Type'	  : 'application/json; charset=UTF-8'
			
	}
	params = None

	data = {"pfs":"",
		"country_code":app_data.get('resp_decode').split('country_code=')[1].split('&')[0],
		"req":app_data.get('resp_decode').split('req=')[1].split('&')[0],
		"id":app_data.get('resp_decode').split('id=')[1].split('&')[0],
		"rtc":app_data.get('resp_decode').split('rtc=')[1].split('&')[0],
		"cid":app_data.get('resp_decode').split('cid=')[1].split('&')[0],
		"reqt":app_data.get('resp_decode').split('reqt=')[1].split('&')[0],
		"pf":app_data.get('resp_decode').split('pf=')[1].split('&')[0],
		"gdpr_applies":app_data.get('resp_decode').split('gdpr_applies=')[1].split('&')[0],
		"logged_server_side":app_data.get('resp_decode').split('logged_server_side=')[1].split('&')[0],
		"current_consent_status":app_data.get('resp_decode').split('current_consent_status=')[1].split('&')[0],
		"cppck":app_data.get('resp_decode').split('cppck=')[1].split('&')[0]}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def crashlytics(campaign_data, app_data, device_data):
	url = 'https://settings.crashlytics.com/spi/v2/platforms/android/apps/'+campaign_data.get('package_name')+'/settings'
	method = 'get'
	headers = {
			'User-Agent': 'Crashlytics Android SDK/1.4.8.32',
			'X-CRASHLYTICS-DEVELOPER-TOKEN': '470fa2b4ae81cd56ecbcda9735803434cec591fa',
			'X-CRASHLYTICS-API-KEY': '8e3eba206ee199fd43feeb386bf35d374e3c0fd5',
			'X-CRASHLYTICS-API-CLIENT-TYPE': 'android',
			'X-CRASHLYTICS-API-CLIENT-VERSION': '1.4.8.32',
			'Accept': 'application/json',
			'X-CRASHLYTICS-DEVICE-MODEL': device_data.get('brand')+'/'+device_data.get('model'),
			'X-CRASHLYTICS-OS-BUILD-VERSION': '1456818039',
			'X-CRASHLYTICS-OS-DISPLAY-VERSION': device_data.get('os_version'),
			'X-CRASHLYTICS-INSTALLATION-ID': '7690f9ec97ea49ceb7e4c5c3da03d562',
			'Accept-Encoding': 'gzip',
			# 'X-CRASHLYTICS-ADVERTISING-TOKEN': device_data.get('adid'),
			# 'X-CRASHLYTICS-ANDROID-ID': device_data.get('android_id'),
	}

	params = 'icon_hash=d691ea02ea30d48e91ede52b00a942f0b2240012&display_version='+campaign_data.get('app_version_name')+'&source=4&instance=47393fdb6c9f85426bec641256a7e67a46de5aeb&build_version='+campaign_data.get('app_version_code')

	data = None

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def gcmToken_call(campaign_data, app_data, device_data):
	url = 'https://android.clients.google.com/c2dm/register3'
	method = 'post'
	headers = {
	'Accept-Encoding' : 'gzip',
	'User-Agent'	 : 'Android-GCM/1.5 ('+device_data.get('product')+' '+device_data.get('build')+')',
	'Content-Type'	 : 'application/x-www-form-urlencoded',
	'app'	 : campaign_data.get('package_name'),
	'gcm_ver'	 : '19275019',
	'Authorization'	 : 'AidLogin 4297219205550664145:6860546673115293611',
	}
	params = None
	data = {
	'X-subtype':'287695367178',
	'sender':'287695367178',
	'X-app_ver':campaign_data.get('app_version_code'),
	'X-osv':device_data.get('sdk'),
	'X-cliv':'fiid-'+util.get_random_string('decimal',8),
	'X-gmsv':'19275019',
	'X-appid':util.get_random_string('char_all',11),
	'X-scope':'*',
	'X-gmp_app_id':'1:287695367178:android:'+util.get_random_string('hex',16),
	'X-app_ver_name':campaign_data.get('app_version_name'),
	'app':campaign_data.get('package_name'),
	'device':'4297219205550664145',
	'cert':util.get_random_string('hex',40),
	'app_ver':campaign_data.get('app_version_code'),
	'info':'I_'+util.get_random_string('all',18)+'_'+util.get_random_string('all',10),
	'gcm_ver':'19275019',
	'plat':'0',
	}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


#################################### tacking the app ##########################################
def appsflyer_trackrequestt(campaign_data,app_data,device_data,isFirstCall='false'):
	def_firstLaunchDate(app_data,device_data)
	get_afGoogleInstanceID(app_data)
	get_gcmToken(app_data)
	# def_deviceFingerPrintId(app_data)	
	def_appsflyerUID(app_data)
	appuserid(app_data)
	gcm_timetoken(app_data)
	def_(app_data,"iaecounter")
	inc_(app_data,'counter')

	method 	= 'post'
	url 	= 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  :	get_ua(device_data)
		}

	params={
		'buildnumber':campaign_data.get('appsflyer').get('buildnumber'),
		'app_id':campaign_data.get('package_name')
	}

	data={
		"advertiserId"			: device_data.get('adid'),
		"advertiserIdEnabled"	: "true",
		
		"af_preinstalled"		: "false",
		'af_gcm_token'			:app_data.get('gcm_token'),
		"af_timestamp"			: timestamp(),
		"iaecounter"			:str(app_data['iaecounter']),
		"app_version_code"		: str(campaign_data.get('app_version_code')),
		"app_version_name"		: str(campaign_data.get('app_version_name')),
		"appsflyerKey"			: campaign_data.get('appsflyer').get('key'),
		"brand"					: device_data.get('brand'),
		"carrier"				: device_data.get('carrier'),
		"country"				: device_data.get('locale').get('country'),
		'appUserId'				:app_data.get('appuserid'),
		# "currency"				:"EUR",
		"date1"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone'),
		"android_id"			: device_data.get('android_id'),

		"date2"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone'),
		"device"				: device_data.get('device'),
		"deviceType"			: "user",
		"firstLaunchDate"		: app_data.get('firstLaunchDate'),
		"installDate"			: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone'),
		"isFirstCall"			: isFirstCall,
		"lang"					: util.get_language_name(device_data.get('locale').get('language')),
		"lang_code"				: device_data['locale']['language'],
		"model"					: device_data.get('model'),
		"network"				: device_data.get('network').upper(),
		"operator"				: device_data.get('carrier'),
		"product"				: device_data.get('product'),
		"sdk"					: device_data.get('sdk'),	
		"uid"					: app_data.get('uid'),
		"af_v"					: app_data.get('af_v'),
		"af_events_api"			:"1",
		"deviceData"			: {
									"arch": "",
									'btch':'ac',
									"btl":get_batteryLevel(app_data),
									"build_display_id": device_data.get('build'),
									"cpu_abi": device_data.get('cpu_abi'),
									"cpu_abi2": device_data.get('cpu_abi'),
									"dim"	:{
												"d_dpi":device_data['dpi'],
												"size":"2",
												"x_px":device_data['resolution'].split('x')[1],
												"xdp":"268.941",
												"y_px":device_data['resolution'].split('x')[0],									
												"ydp":"268.694"
											},
									},
		"deviceRm" : device_data.get('build'),
		"installer_package"		:"com.android.vending",							
		# "deviceFingerPrintId"	: app_data.get('deviceFingerPrintId'),
		# "installer_package"		: campaign_data.get('package_name'),
		"isGaidWithGps"			:"true",
		"platformextension"		:"android_native",
		
		# "tokenRefreshConfigured" :"False",
		# "uid"	:app_data['uid'],
		"cksm_v1":util.get_random_string('hex',34),
		"open_referrer": "android-app://" + campaign_data.get('package_name'),
		 "ivc": False,
		 "kef4" + util.get_random_string('hex',3) : util.get_random_string('hex',46) ,
		"registeredUninstall":False,
		"counter":str(app_data['counter']),
		'af_sdks':'0000000000',
	 	"batteryLevel":get_batteryLevel(app_data),	
	 	'rfr':{
	 			'clk':str(int(app_data.get('times').get('click_time'))),
	 			'code':'0',
	 			'install':str(int(app_data.get('times').get('install_complete_time'))),
	 			'val':'',

	 	}, 	
	}

	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+\
						data.get('uid')[0:7]+\
						data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+\
						data.get('af_timestamp')+\
						data.get('uid')+\
						data.get('installDate')+\
						str(data.get('counter'))+\
						data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))


	if app_data.get('referrer'):
		data['referrer'] = urllib.unquote(app_data.get('referrer'))
		data['rfr']['val']=urllib.unquote(app_data.get('referrer'))
	else:
		data['referrer']="utm_source=(not%20set)&utm_medium=(not%20set)"
		data['rfr']['val']="utm_source=(not%20set)&utm_medium=(not%20set)"



	if app_data.get('counter')==1:
		# data['af_gcm_token']=get_afGoogleInstanceID(app_data)+':'+get_gcmToken(app_data)
		data['timepassedsincelastlaunch']="0"
	else:
		data['timepassedsincelastlaunch']= str(int(time.time()) - app_data.get('timepassedsincelastlaunch'))

	if app_data.get('af_gcm_timing_t1'):
		data['af_gcm_token'] = app_data.get('af_gcm_timing_t1')
		data['registeredUninstall'] = True

	if app_data.get('counter')>=2:
		del data['af_sdks']
		del data['batteryLevel']		
		# data['af_gcm_token']=app_data.get('gcm_timetoken')

	if not app_data.get('timepassedsincelastlaunch'):
		app_data['timepassedsincelastlaunch'] = int(time.time())
			

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

########################################
#		Call--
#
#
#######################################



###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)

	url 	= 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	method 	= 'get'
	headers = {
		'Accept-Encoding' : 'gzip',
		'User-Agent'	  : get_ua(device_data),
		}
	params 	= {
		'devkey'		  : campaign_data.get('appsflyer').get('key'),
		'device_id' 	  : app_data.get('uid')
		}
	data 	= None
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data,count=None):
	# def_deviceFingerPrintId(app_data)
	gcm_timetoken(app_data)
	tm1(app_data)
	tm2(app_data)
	tm3(app_data)
	tm4(app_data)
	def_appsflyerUID(app_data)
 	def_(app_data,'launch_counter')
 	inc_(app_data,'launch_counter')
	appuserid(app_data)
		
	method 	= 'post'
	url 	= 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent' 
	headers = {	
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}	
	params = {
		'app_id'		  : campaign_data.get('package_name'),
		'buildnumber'	  : campaign_data.get('appsflyer').get('buildnumber'),
		}
	data1 = {
			'advertiserId' 			: device_data.get('adid'),	
			# 'af_google_instance_id'	: afGoogleInstanceID,
			# 'channel'				: 'null',
			'app_version_code'		:campaign_data.get('app_version_code'),
			'app_version_name'		:campaign_data.get('app_version_name'),
			'app_name'				:campaign_data.get('app_name'),
			'carrier'				:device_data.get('carrier'),
			'appUserId'				:app_data.get('appuserid'),
			# 	'appUserId'				:'0',
			'brand'					:device_data.get('brand'),
			# 'deviceFingerPrintId'	: app_data.get('deviceFingerPrintId'),
			'devkey'				: campaign_data.get('appsflyer').get('key'),
			'installDate'			: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone'),
			'launch_counter'		: "0",
			'sdk' 					: device_data.get('sdk'),
			"model"					: device_data.get('model'),
			"network"				: "WIFI",
			"operator"				: device_data.get('carrier'),	
			'uid' 					: app_data.get('uid'),
			'af_gcm_token'			: get_afGoogleInstanceID(app_data)+':'+get_gcmToken(app_data),
		}

	if count==1:
		data1['af_gcm_token']=str(app_data.get('tm1'))+','+ app_data.get('gcm_token')
	elif count==2:
		data1['af_gcm_token']=str(app_data.get('tm2'))+','+str(app_data.get('tm1'))+','+ app_data.get('gcm_token')
	elif count==3:
		data1['af_gcm_token']=str(app_data.get('tm3'))+','+str(app_data.get('tm2'))+','+str(app_data.get('tm1'))+','+ app_data.get('gcm_token')
	else:
		data1['af_gcm_token']=str(app_data.get('tm4'))+','+str(app_data.get('tm3'))+','+str(app_data.get('tm2'))+','+str(app_data.get('tm1'))+','+ app_data.get('gcm_token')
		app_data['af_gcm_timing_t1'] = str(app_data.get('tm4'))


	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data1)}


###################################################################
# appsflyer_stats()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Appsflyer's behaviour whenever user leaves app's context
# example : User drags notification bar,
#			Facebook SignUp page opens in app, etc
###################################################################
def appsflyer_stats(campaign_data, app_data, device_data):
	# def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	inc_(app_data,'launchCounter')

	method 	= 'post'
	url 	= 'https://stats.appsflyer.com/stats' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}
	params = None
	data   = {
		"advertiserId"			: device_data.get('adid'),
		"app_id"				: campaign_data.get('package_name'),
		"channel"				: 'null',
		"deviceFingerPrintId"	: app_data.get('deviceFingerPrintId'),
		"devkey"				: campaign_data.get('appsflyer').get('key'),
		"gcd_conversion_data_timing": "0",
		"launch_counter"		: str(app_data.get('launchCounter')),
		"originalAppsflyerId"	: app_data.get('uid'),
		"platform"				: "Android",
		"statType"				: "user_closed_app",
		"time_in_app"			: str(int(random.randint(20,60))),
		"uid"					: app_data.get('uid'),
		}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################
def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue="",isFirstCall='false'):
	gcm_timetoken(app_data)
	def_firstLaunchDate(app_data,device_data)
	# def_deviceFingerPrintId(app_data)
	appuserid(app_data)
	def_appsflyerUID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')

	method 	= 'post'
	url 	= 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent' 
	headers = {
		'Accept-Encoding' : 'gzip',
		'Content-Type'	  : 'application/json',
		'User-Agent'	  : get_ua(device_data),
		}
	params  = {
		'app_id'		  : campaign_data.get('package_name'),
		'buildnumber'	  : campaign_data.get('appsflyer').get('buildnumber'),
		}	
	data 	= {
		"advertiserId"			: device_data.get('adid'),
		"advertiserIdEnabled"	: "true",
		"af_events_api"			: "1",
		"af_preinstalled"		: "false",
		"af_timestamp"			: timestamp(),
		"android_id"			: device_data.get('android_id'),
		"app_version_code"		: campaign_data.get('app_version_code'),
		"app_version_name"		: campaign_data.get('app_version_name'),
		"appsflyerKey"			: campaign_data.get('appsflyer').get('key'),
		"brand"					: device_data.get('brand'),
		"carrier"				: device_data.get('carrier'),
		"counter"				: str(app_data.get('counter')),
		"cksm_v1"				:util.get_random_string('hex',34),
		'appUserId'				:app_data.get('appuserid'),
		"country"				: device_data.get('locale').get('country'),
		"date1"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone'),
		"date2"					: datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone'),
		"device"				: device_data.get('device'),
		"deviceData"			:  {
									"arch": "",
									"build_display_id": device_data.get('build'),
									"cpu_abi": device_data.get('cpu_abi'),
									"cpu_abi2": device_data.get('cpu_abi'),
									"dim"	:{
												"d_dpi":device_data['dpi'],
												"size":"2",
												"x_px":device_data['resolution'].split('x')[1],
												"xdp":"268.941",
												"y_px":device_data['resolution'].split('x')[0],									
												"ydp":"268.694"
											},
									},
		# "deviceFingerPrintId"	: app_data.get('deviceFingerPrintId'),
		"deviceType"			: "user",
		"deviceRm" : device_data.get('build'),
		"ivc": False,
		"kef4" + util.get_random_string('hex',3): util.get_random_string('hex',46),
		"firstLaunchDate"		: app_data.get('firstLaunchDate'),
		"iaecounter"			: str(app_data.get('iaecounter')),
		"installDate"			:datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone'),
		"installer_package"		: "com.android.vending",
		"isFirstCall"			: isFirstCall,
		"isGaidWithGps"			: "true",
		"lang"					: util.get_language_name(device_data.get('locale').get('language')),
		"lang_code"				: device_data.get('locale').get('language'),
		"model"					: device_data.get('model'),
		"network"				: "WIFI",
		"operator"				: device_data.get('carrier'),
		"platformextension"		: "android_native",
		"product"				: device_data.get('product'),
		'registeredUninstall':False,
		"referrer"				: "",
		"sdk"					: device_data.get('sdk'),	
		# "timepassedsincelastlaunch":"",
		"uid"					: app_data.get('uid'),
		'af_gcm_token': app_data.get('gcm_token'),


	}	

	if app_data.get('referrer'):
		data['referrer']=urllib.unquote(app_data.get('referrer'))
	else:
		data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'


	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+\
						data.get('uid')[0:7]+\
						data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+\
						data.get('af_timestamp')+\
						data.get('uid')+\
						data.get('installDate')+\
						data.get('counter')+\
						data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and \
			app_data.get('prev_event_value') and \
			app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
	update_eventsRecords(app_data,eventName,eventValue)

	if  app_data.get('iaecounter')>2:
		data['af_gcm_token']=str(int(time.time()*1000))

	if app_data.get('af_gcm_timing_t1'):
		data['af_gcm_token'] = app_data.get('af_gcm_timing_t1')
		data['registeredUninstall'] = True
	
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
#**************************************************************************************************************************************#

def getpromotion(app_data,campaign_data,device_data):
	url="http://adserverapi.psafe.com/GetPromotionConfig"
	method="post"
	headers={
	'Accept-Encoding':'gzip',
	'User-Agent':get_ua(device_data),
	'Content-Type':'application/json',
	}
	params={}
	data={"header":{"connectionType":device_data.get('network').upper(), 
		"packageName":campaign_data.get('package_name'), 
		"protocolVersion":"2.0", 
		"timeZoneOffset":"19800000", 
		"data":"JpusKGOPchRntrz%2B7SDJvJJuwd9XgtFrvqVrFWRY9zMVCeLYD3TY6iFF3zPxNX6668tzUXnuH2dPlvIV0LuISnp%2BLQKgdeZ22%2BVI8aYy3Qqo8ln6xF88kWyhE1MFPbjcwkfhnx2PYU5ZiBCviHOpKJp1rLXi2v7xXdTadraRNi%2F51D5NyIjSjwVzJ1ABcvqmiad4yaqzU2%2FyKESRy5ij0UccYSXyiT5tdbGIJ46ShHNORA9kNfWj7ntTBqdkqaLjR5ZRtikRcxldnEPiSf2Ql%2BE25tBgiC4jzizWZN8xw2AScLXx%2Bed%2FXChj43PYIrYIdbFYTk9A5tcmomt0%2BFxwzQoknIetyL74WPCVrG%2B7Hl2N5Tq%2FatH9w10xBhrDAd6TmP5YZdr9OWa2iOvARHBbAAnYIc5U0YnlmfAipytkcHESapdl16nxDnR4a99%2ByOg9wZKspnGxDB3wYp%2BDCictmDB1vLgn8vV1szzj9Tyo6aakOhyNLJyy4wdnKbedYd700V1dKpV3J%2FSHtcKjZZo9Jm%2BFKYZmgCm%2FcvGNwJ50lAxzjJc%2FqbTJzr5JmoPKThvt5j5l5p8SsmqPZ%2B5665%2FOCoJUm50S4qPMAt94F6saWWXSCx%2BgTGCjirsHIz51juNV8c2Z8U5jsU3oClipaJwZGNtBpJ%2BUTF8Ik4dfneKMI7WrVNjPuiag4AIr1idIxgqomK7HlJrMyDR%2BRkzrTA17N%2FGFBxAdf%2B80ZtIVg4Jc%2BAqbdnR4j4O5Lms1c2LgqSG1q%2BDwX9RQKlVdF2k18kxOiUeMYnjcbrsLk9w1KjvueSA1pmv5B2eIPHqzZAn%2BII33D%2BkTuJMvsFoUgzPbuI1GZvKpZ9L5mYsQx8FRP%2B%2FuTQzQaIkARixY58P6H8c4HZFv1selk7GZIORMhgAl2xMTwnTD2xNGcCodUIHSuYt1QgfNLqQ5OefDt6UNBPaonJR2Xkn2H4F7yTPObp8hMK%2FrypmRuEOKPVxn48X70eXEtFR8V3l%2Be2vRLH00%2BvuBeybANy9Eo%2FcmZuwYflAHFHXHsyOaOdYfrWWyI4Yk1Sf181BtmTCNk386oueh%2BpXrogmJHA2mgxgsiZel2K%2F1dCD3qAqZlc0vHV1oSWUByAKYXFSRjYqSbMMxtNiTryxD89d00N2iKAeKotuM16LvorYP9V7xTRMDl2yT6gOVy9FRylJ6yyslEr7jRgjcw%2BBZ1NiKkDwagk6spJBNHJUueoZvlFkkyGmI7ZRaTmNjgEdIE4px5wuDhvE7t1I8xjb5X6o4QHFE7i04jQ1k43XL3KA2x1Mj6sg5Gy9namgPFRsmKEEK92FbGd%2FLro3WjC9cKNr5aAmPoVGlyrTckDaWILPbYkF6RazJ%2Feh0mGyXWB7JAHkC4eMDufSFcWD90%2FIQWwzrJDdXBLPfCYSOCUxp%2FdbgPU4wVbE%2BbxCyq7rzKkIjVZSn%2BPK3OH98UxkQwjHyf0Sjdwzg86%2F%2Bt7faTXgr4qI4e3n%2FpNfimUD7m0%2FyF%2FjsINfcU%2BUr9zpVS1NwhLaxRHcqnyaDObUWmOFoV7vvw44HERWt8J0JBTRqe4BlDyMslML0kcggGPMch66LbGkHalXupjzvF%2BJrWwtclzp%2BKx4BHRZWFM%2FBxMrd17RT15qqoYtEndaFUEQ1%2BHXKHxLgh9YIkIlODAdrlInInIH3z9EIFZa5r3dAaLi5w6Del01591tNjG45%2Fb0ZBWokLvMlHWJJy%2BvpdSWnNez3b%2FXV3%2FxR309Ul08YrBD%2B6Y9X5zY%2F1iiLNKL%2BkpWIypiAa%2FRmXWrRnTUzHhphRg4U1ZjDaVbM3vICGm0WPeP71oUGz4dMi99H3DX12sGYHK7RlCdAeYsDb39iTyLb8BZ6%2F82Dnb4LAfZrmjUchKys544nVr3X%2BHt5wlmAHvndoirNAvfqev2SpM1MitcAFsKeo5fjhxvpVnzBuFffo7wnnByLGHX70fJyyxeXGQ8lyiQT6qw6k8tje43CzRHLgS0tcOrgSu4YgxxJD7tik8W58DNwS1TgFO9DaGrj%2FqeE9WjLsUx2JXPqbOBxWPFfkJ36Qv%2FPwdDVZO1Ulg43XX8ffs5Qn6lPJTE%2Bsp31lcTQEwpGnuUGXT0KbRcatLeiRb08gegqohto%2Fdk8tkG93mayamOcw55I%2F%2FT4EJ%2BUg%2BCJI9kDFrNTs32gOAFjjdOq2l0Q4%2F1Vwj6CnH39WeAX%2FWWIsog%2F6il1BNQNsRGjqUbfSixOSxb%2BDxLv5J1J9DLPkYOxk%2F5SHoV7NImBTYRG384rTP8elmwFGWS8peDj1Y%2FCFDfx7VLwuptBD68zF03aje61b%2BEMuyR3x1RsHNxYkoSIvPDo1CxMaYnJFb2xNZoZ5uJLe3EB65KDu%2FxZ0JDHPyfR44OnlayPgQgwPh8erGYg%2Bh1jdMebLxpZ%2FxkjvN2XcbXBoyxx4tbLUN%2B9vfgmPbZ7aI3kbxLVlDjlVXwzg0wYMKG1k0E82VmXx%2BvYfNdw3YLQ%2B4Aj6zqIU95JRsaLHzmdnJIUflxx%2B4IFGIvz0gYfLVcg6RZNPWDCgczrsAvpx1svXPCdz3b3apl51i0YQ0onfOf5FVyOnyc2X9wiyx93ktYjdWPtQUXRE2wc3oSoVfmee%2F5ymkh6zksROjaZx6GvXJfvJCGFZDdWvHXbKx46FjP9nBZvxK6le01NX7thtrX2itxi6xQz5lkVRYdUmZwrut8ZAe18na2Z8RoHPI7Y9CuPC0sfHNAdzNHXY5sfP2SqBip89r1EB85uAGj5QCAmwTiF0hpbYPXgDvGrKXDR1cbNxGpR6oVhf6bOE9gAC1kgKRE7oKdUeoOUaEbhP7mTCL%2BV1lN1x6x7Rjq1JpSZj7NjP%2F3GOZEoY5moJXx7REqZa9nizmiNrCJRlhZxi9l7Xb%2BaaMdM%2FXV5PZAy1XIvq3dnjOySV9UO%2BrlE0M401kV70s3E6QtRuNBairz2r45n0Lvx4fD44suWznKa6C8HYos%2FgMrW2mWz3sD%2FN81LC4TMhwyYokrlfjgeLIXilOzKoCLbxLDMAeXr34kK7WD97oWJAK%2Fjzp9VFuSUth7Q4cqHxCNYxW%2BLwm5LCCpIVR0p8W5hfEJTTXthSJUFozzbm1IC6qOQWEaBmlMDvImTde9OZk%2FAGEtFWMS02xVj0kArxP3zGc0shM9UDL%2FYDho%2BxrT5bCQ1w0uiJS6og8Of%2BpVDxmMRUaAXHbqwBC1mI9%2F7Q2DZHDJawiMy91084gjznQmj%2F7OjOyefb13KgUI0li%2FFES7lgwd14z9DzwjenKbJU4QjuCC6gvT8ySX4kR1e3ABCwlVg%2BzLf7AuCTxnPiwVCWXjz2wvZx%2Fl3iO3GD1pcuzx2d9%2Bvwvb7wfKzyKjLEWEzO%2F2O3iFXMruicdVThvXpMry6hreRb6idpLBLMqUQKop%2Bo1HJVPoHuHpxYL2p8jX2c6s5cIAseuVfOekdsHGHtwELfSnwss36HUwFVfEQIA4Xxhpvuoyt8gIfCUnG37sq0Fj7gecOBSTQ7zRpnshGkSkCobLC6nOtj%2BpuNnNIKt5fyy0abnW2z%2FkzjtC%2BBIZUGX9T1EH1v1WYqC1ywEJZFHA2FIJu1IY95d8Ukz6ro67s7FH0MInTowRStllGGXyGFfGkDhtV%2BXKUmvhSIZGg6EC9N%2FnNYw8GSQP4c62KGcRBnIEsxk92iPAnKKASKRtj8c9UNhX6PWkuIt0Itbcvp%2F8B%2BYUdqKss522BtLiMv00f%2F4FdIo2bCnv6w%2BmImM3xi7nWV%2BpMldxtuVytc7bXlLaBHVjFzjsUB0FWMC25Aq38FobjJsCinhOyVcWiYbJesuABIaopolnAR1IYMgNz%2FEympyzAcweR52See0MMUdEbd3mKiuyi64KWZ0R99UXxaOJpw9eJJy6zlWWwtshsRhdRJb%2Fq5RnUKfKc5loK8XhNEf93qwyLKfSiYny%2B5JpBuChr6INcbv5oy6Ro1X9P8plW%2BlLzomGXiBuNiycLBjv6ZZAHrFYMI3Il11kML%2B2GlWYSbJEvRrAh89oEQca%2FzZvp%2Fa6VgV7UpKC226UHtwS1neqmtdsJDjliZa7%2BhBMzEnkjzPEeLC3sqW972yfaE8wt0PRj5ft%2BS%2Fv5LwlR2aEGFP4hs6StX6AnBlMg7mencWkmblvkapHRaSmp6BaUGHVbSRqkc4A9nh81Qe8WP0duSkt8X%2BnU2tErB4M0wSpc8HD%2BJVgTgTfnXGg66d01dDSbz5FPRFjp5l7nNO9VaL%2Fk4fzws8h1M2Bq416%2B6eeuWUMKblkjpqj0oomZJxiwPaclechgvdT%2BQrqQA4BdHf5OjJ9WQmp5l4Ngi1aVQZUtc0HxM9FhiNaXqa%2FKnn2p2Vtw%2BBxqXfjNMicAnnJ8X3qwxC0L%2F%2FK3tj6Jmi2yAUrnp2HIzRx00zeDsn9PGf8wH1YxDr%2Fhs%2Fq1b6s6HGF7R8NDpSLRfUVY3zmUdegzOFsErnTucaBnQ%2F0koEhP89ktXeGghGXy9ZDupy1G7wP5SkmeFZFIYctDQQclT58I2aRNWANMVBY7LBmvEgTyQnB9VbAFAk4QTFbqnqW7yID4D7cCnAu0ez91gFVny9ftuI3UZWJQERjOgF3FqEp981ia5%2BT1TMiMnaQu6i1DKRjGB1wqouFpbCAQagSkLDGMzZC%2FFrD9pPP8nTIHU4Co5CbTq9Dp54ylD5ZmU34i92%2BwXogHR%2F8aqyn1gpxT%2FcZs2OMet75zGWm64er8LDJDZB8oS2nAzv%2F5M1%2Fy5x7Fn9XbxRkayn0XOerTxUhG7qkimitJAWaKGR2osPL8jUuLpnkbnoT8s2SIPpa52DDlTq46rdrzjd8j6HvR0sEaoCcE0LkdXhCEhzV7bLAcHZ7C3Tt3tR0mMVj6UDNeGwTsAyjMGYDFubb%2FLXw2IpN67WgBCj2czkcqdHQ5kftJ3AlfzFIT%2FCNO9h5RGOIV9boSv0aLXHZ6cbzlX%2F2Ti45E8pfq6PL2CHgf7mimGNg06utgsSSwE0HC5vCut%2ByrlTnulqii26carA1LRbyiIyCxybWCdr2Jg2YpPMBHtocYxvzpgXUCywIqncPX5HnK%2FofMmYCwvnPLgc9XijTUJfFkkd0HhRD6WWVSlFcILPnj3GW%2BtwyUo5Iw1X8xs%2FE0r%2FWgYp2o9Up0e1K2Q6WpGNE0ekVrMcyrn5ljBrzW9bds%2Fe8b9IJsDSqBI%2FDWMMkjF3rOOWH76AFuaFfxNRmrUW16crgvl0w1n4XaMkcNs%2FgDYI29NgQsg1dCyvAhrXAHWkWg8hd1ODrZzdO7csu8qeafz8KC5THq53A0Lg%2BlaQ2C65V35QlsCXaDSAbMfJuMbOqIdu2osNEoCfBiRwDpt8ZClh6l71xVPbJu1QGpN31zspHIH6%2FqvLCXvoND0E34YeGcbdmG44v5AXstEYWbC47bRTh4Wxk7Q%2FOClZNgEFOk5VkP6ZqtUCKugxEvFTX4M%2BNgLYpBNnXZ9Q7i9%2BufYzilHFKFNoZfo0jXzjf5B5VEdPZrG7rPa9PpRBh5j1SW8nWIcXoJSnNSw5MCVijlFq0Ifec%2BkFHR52UtOFMTnbpowNitTwZ3uv0TU1P%2F5lN1mKyInEQGQ9cXA0Go9fUkn95%2B7PXqGRBR08bnf2np1vrmQJSHw6NxaZDPfGgKsdmDFY2Ti0CMT1bKtDO%2BMpylbXpfP0egqML7DEkLL7FnLEYWS1CIaWbGLKXWWpsDGdEMMmUAXW2fquvNYFE%2Btz0tIsSyiUfnlkVmGEMygZZmn%2FKU924rhYpiCK20p5DlrFNZlfU6hjJgOGz7emOA1sBK0bFPWnYDTnPv6M8dGYHhVJuDgOrDmuSCcDBISt2WqBvj1WhCAiu8gIhFkwxNngk%2FNpTLRG%2FR0sdrtJToIcDwkGbQ2y7RmLOEcrR6QLcBdH7kEwetMa0xzqenUhpY%2B859uZ3jdpgmzP1Si9lqKW0SpRG1QVMXNKfCyYopo9UXKX4TZyvH7aegUliNuYjiEXis7hgIpqr8rjGThbLyJ9eafYNer927y71TVrhF%2BPn1U47NKjqMPOLaztEXvrlKqcL853Zla4cjjFtXPrcML1FDJuEa6Ll2pw4Ij%2FssGKwYbxFRkxhXgA7B1bkDDOae6WgzDvENoB8HSeAg3ZgIJtzk%2BA9yEyUGV5YKn0cDatOOYIpxbstB1D1YbGLZfuJtgWley1RFtE%2F8SXBQVK6VGAT10EBYS%2FwOqCz%2FEgbTFdwgZDOv9quD46FcMtgHD%2BIy9CunCkeYn98yhdqwTDwNUpB%2BkyA%2FZB5cEpegWQ9t%2Fqq7roTH%2FfpY%2BQ3g7NEN4YoVcxFUDXOGg29zSPIJxNWOJiGGLL8aQpPNgWlUbmkNUnU1qg4HTcrQ357Vy21h1vJFJQkNPK8Htk%2FvrUBYnLZubQNDJbf3GzZ7MJYfgdOUrHeZ0sJhLqGvO9b3smn67O1QqMB4MzTrCrKqBWQFxZznK%2FBcKx%2FmdGAzThhUEkfyQ4fT2ew7sm4LrK44iP0kMaSpqT6aBF6BGQKweRG2JaoVBWMQ3bggDGomrPFvXY7XiAdFB1TcnvR0D68VhNUXWT9hUcCgyddmE%2FMl6iUHXvcGDPUe0LFaNakBpIHSYv9mxf%2FrwENcqq%2BY4O00520ds9fLFouW1Q0g0jAyyIO%2Bldr%2BIkAAkwjdLduTtfoPSTiPloMZFKHi5Q7Jh5BDfzFquI8n2U3lY90On0amyLkhvUPqfsUwfoB%2BtNcIySLjVbbkONbdHhgUr0wcSzdqC8A%2Fm%2BMkfH0%2BfZpI6ADv63FqPitC3ORcmOBnwPdtP8%2FkIz5aqHGgWl3G0h18WvWGANudqNiXIMr36QJpCZ3KU3G6r8DP5f53X5e2QreQy28zjVMCwnlCQ%2FJbghrkDWvQIONiF5LXvz0a04yX%2FlESNNXwyXXg%2Bg%3D", 
		"clientKey":"f6ca13ff-d58e-4c8e-b665-09973e9a1b05", 
		"cid":campaign_data.get('adserverapi_psafe').get('cid'), 
		"m":campaign_data.get('adserverapi_psafe').get('m'),
		"appVersion":campaign_data.get('app_version_name'), 
		"country":device_data.get('locale').get('country').upper(), 
		"client":campaign_data.get('package_name'), 
		"imageSize":"hdpi", 
		"lang":device_data.get('locale').get('language'), 
		"version":campaign_data.get('app_version_name')}, 
		"requestData":{}}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


def selfcall(app_data,campaign_data,device_data,calltype=''):
	method="get"
	url=""
	headers={
		'Accept-Encoding':'gzip',
		'User-Agent':get_ua(device_data)
	}
	data={}
	params={}

	if calltype=='updatecfgtime':
		url='http://up2date.mob.psafe.com/psafe/update.cfg.timestamp'
	elif calltype=='updatecfg':
		url='http://up2date.mob.psafe.com/psafe/update.cfg'
	elif calltype=='dlen':
		url='http://up2date.mob.psafe.com/psafe/updateData/1531863099/dialog_len.cfg'
	elif calltype=='dlpt':
		url='http://up2date.mob.psafe.com/psafe/updateData/1531863097/dialog_lpt.cfg'	
	elif calltype=='cardlist':
		url='http://up2date.mob.psafe.com/psafe/updateData/1531516972/cardlist_lpt.cfg'
	else:
		url='http://up2date.mob.psafe.com/psafe/updateData/1531863099/dialog_les.cfg'


	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


def userlogin(app_data,campaign_data,device_data):
	method="post"
	url="https://d2sqz2p8ap3qea.cloudfront.net/user/login"
	headers={
		'Accept-Encoding':'gzip',
		'User-Agent':'okhttp/3.10.0'
	}
	device_id_string="psafe_dfndr_sec_"+device_data.get('android_id')
	params={}
	data={
			'device_type':'android',
			'device_id':util.base64(device_id_string),
			'tz':device_data.get('local_tz_name'),
			'device_name':device_data.get('brand')+' '+device_data.get('model'),
			'email':'',

			'idfa':device_data.get('adid'),
			'carrier_id':	'psafe_dfndr_sec',
			'auth_method'	:'anonymous',
			'mcc' : device_data.get('mcc'),
			'mnc' : device_data.get('mnc')
	}

				
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def get_user_info(app_data,campaign_data,device_data,access_token):
	method="get"
	url="https://d2sqz2p8ap3qea.cloudfront.net/user/current"
	headers={
		'Accept-Encoding':'gzip',
		'User-Agent':'okhttp/3.10.0'
	}
	params={
		'access_token':access_token
	}
	data=None

				
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def configcall(app_data,campaign_data,device_data):
	method="get"
	url="https://app-measurement.com/config/app/1%3A287695367178%3Aandroid%3A440b4fc2672913a4"
	headers={
		'Accept-Encoding':'gzip',
		'User-Agent':get_ua(device_data)
	}
	data={}
	params={
			'platform':'android',
			'gmp_version':campaign_data.get('gmp_version'),
			'app_instance_id':'1219ff8c2273461715fc3d7a4ca99906',
			
	}

				
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def getData_profile_psafe(app_data,campaign_data,device_data):
	method="post"
	url="https://profile.mob.psafe.com/getData"
	headers={
		'Accept-Encoding':'gzip',
		'User-Agent':get_ua(device_data),
		'Content-Type': 'application/json; charset=utf-8'
	}
	data={"m":campaign_data.get('adserverapi_psafe').get('m')}
	params=None
				
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

# def first_run_psafe(app_data,campaign_data,device_data):
# 	method="post"
# 	url="http://firstrun.psafe.com/ValidateNewInstallation"
# 	headers={
# 		'Accept-Encoding':'gzip',
# 		'Content-Type': 'application/json; charset=utf-8',
# 		'User-Agent':get_ua(device_data)
# 	}

# 	data={"packageName":campaign_data.get('package_name'),
# 	"m":campaign_data.get('adserverapi_psafe').get('m'),
# 	"googleAID":device_data.get('adid'),
# 	"psafeID":"09e723533bc4df53316961ffbb771acc",
# 	"legacyID":campaign_data.get('adserverapi_psafe').get('m'),
# 	"country":device_data.get('locale').get('country'),
# 	"version":campaign_data.get('app_version_name'),
# 	"protocolVersion":campaign_data.get('adserverapi_psafe').get('version'),
# 	"lang":device_data.get('locale').get('language'),
# 	"screenDensity":"xhdpi",
# 	"connectionType":"WIFI",
# 	"deviceType":"Phone",
# 	"campaign":"",
# 	"publisher":"",
# 	"content":get_conversion_data(app_data,need='utm_content'),
# 	"reftag":"",
# 	"referralParams":urllib.unquote(app_data.get('referrer')) if app_data.get('referrer') else "utm_source=(not%20set)&utm_medium=(not%20set)",
# 	"data":'JpusKGOPchRntrz%2B7SDJvJLO6foIPiECdv0E49LnaCd2zgo0lkXS7cv41ro09vj8sdEyMfL%2Fd1g8TZgoVU1MAPCc8QqrW7%2F7SAWcLpGHt8nKUmawtfoIrEIuExbjEgSgV4430TgSXBCbCPJwL0CurqYl5G%2FbycT7tzLIsDmIK9lvm7pdJ8lA0ir1EHTzDWRw0H7L6NifijQo3m1UWcPNj4REm1RKBTpfvkbs7kAsl3xgqNloVubLHI9bMU%2BrTCOy8q67Oe5nJA37Xa5vaaPDuAp9iKxtr%2BnZ7CHhl0C%2B3pkDUeVjsXNAJocdS3AvuBpuH3wQUMUlJhQNYmfZSJbL%2FDqYsp%2B5tBC8pX8i8bVEYi17cicZR99cM7AU3p8sPDlCI5tg76AFR0BUN2dpHrvEGp2iSxpiepfDSiGDEnAQt1krwI6GrzBlVljX0E1Rskj2lYLXsF1LgW%2FC6PlJYXxvNrBQPftyUX%2FddKaYQ7B21yQfbqAhY9bL3hHSykb4UKfzt6UDWNq82V5oFqdTsVnHfXZK05uQ7x3BOXiQOZGqQHrdHYl4ppUmU62VhGYQE01I7j9GaUqvh%2BZKkY5lODTIc5ZeILBoTugUNIbQtFw7tkSeQ2HHL7AVhpudxNFonw7XPVFsFYt%2BTmaWN%2BO5r5JmQeNIpBYcX%2Bamu0%2FH0dilJjqAeB5qJC%2BEFD0%2FNaTwgbMF6B%2B4yOkTjTRvEuhM5BvVGTbILNlSQ8E9%2FSQokANVGEmO5efYSIKRhIOFIriJ%2B%2B948scQ5S5hyn2TwBGhBN31n%2F4hafmeUvXmDvVWk3OTtqmoKcwgx5fFtLz4OBb8XCt7qxEV3yZ9bHi8N2fxMe%2Bo%2B07HOJysi4AM2BlkG7oL7f9QJKD9ybr9W9Pj5rWtagI6wtGhZedOeSs6RZDvBTrTLc7gh02OXcXYDSV0trozLi7zH6PCIH9a7i6glC9LUOwWlvBBx4I6NqVLv0N%2FNcYVmQB77AuAwjr40IK%2FLNBWclbIEWFeg%2BxOEiPg9Ko%2BW8dvphEZTEFkLmIgRkAKIcCLc2bOEROLImaAgn0NcP1hi8qkXxYbRkpy4zhMMWlCs0OabTPREJ%2FPAzYE5QRfpdD4KFD4q5iDTqJKlOvRWNkeAu3UGaeNF5sh9JaUOIIyfqaH7TWTKNnr5Lq8TpVC20W%2BE5id7cwDgovxcNtCs4EGlSjLmTv%2FCQwmjQaHKTqIPX3lo08im%2F5kmoq9Noorsvul3iw%2B4MCgQFaHVWJ%2BxwIeSey7c%2B4Oaf1pKstmdgj4Y66SRSA9GRj9XzaouKjNC0%2BzO%2FgaFytHo7nKsJrmZYbwh0tBi6zUklnXsHboRUSLCKN8sFbTOPtyuMDvndcFbRYKg9ASXR%2BqjQuhh1zyeGEQewmwwCdQOwJBf5hOdMciYKtJoYm9RYgdSZVNAxfqYYNV3blMNo7OdW1lI7hhOo8wU0lBbwMo0nj2GBzEOWAFLjgQCmHlZloZWRUFCw153Y6dT474rNIfyQXwBrQmqkiGT1Rp8QZWDjsHJKnkrDkCAYLQ2K4wNbui4%2FMn%2F2G11lJOC6lcRi5eLmEqxnAL9R%2BEdzgKsi5%2BXeRNo04w4cxaJjWkwDE%2BvIj8r%2Ffi2XmI%2FUi0A2iRmSZAx5XtJWUze82Ekfnh5fGIkwdAfY5zYHpsfkOvB01wI9ZPNUwntYesYtLL8gbu8%2BX6%2Fz1fpQXs2LHMc9W%2B4P3DB1dZiGnUEtRpJ4OAR%2B9jyWZNJYKlZ3%2BT2TqiZAk%2Ft6ZsWKQ8ptRSOQwyLZa2%2BHpasIbUfhRP%2B5%2B9JJXc%2Fwu9qsdYKda53%2FIIO8kG7uE65Jx%2BHFNMKqPo3hxR%2Bo1xU2ISFH3GjDod%2B0t9%2FPmOvCsTpc2Kmz%2FyQ8YA4RILLE1B%2Fxn4fuSNVm9NyhDTIgRDDYpVC%2BXRZ%2FZ5pdrty%2BD7zG41ewjpW8CV7o5Vto8StHHHDs9xQF4owrv%2BmRs3pKI8qKvgBDrbk%2Baj7pssEFuC4MyGGs14AAppw%2BDhUzTvuBzHVtujTFvYqgds5p7K0abYuO3EgCNyyAxlr6NawPzNjw5qdYmRARrd8gNlaCvPGm4JShCRoPm%2F70XCYXZFBf4rqrHLvVHnHiAQm55nwtyRWvR8rYl95WgkIDrYpVoOjyMeXJ42b8HLbpD2mcqQ0joqAerqh4VBJESRcsWhHi3ODnmtBvztnGMo757HTRYEAIPwNn7AQe3SlQpUSHLl4cwWVB7egllO%2FN2x5iJXI0rD4c36ReQrKGWQB7VOQpmut6h0nf1Mjp5u%2BnTIhhPHXvya44twZumTmun4eehwT2PDJ0gMVfNfIWUlQ1PDRkA37H66MnXUpKwxa%2BLS8y9fWRY0Hc55ryvgVpM%2FYshm1IqHZqkIGC7z4RcIu5NnSnBuhuez%2FczkCIA%2FWGrnEOB8hNjK2wf4erkkMOx3UI3242ze9ohyxzjc2GZeJpTQLP21zLAuBlWtnXCNNbt7S4HDD7LPQr5LY2F100wLHmA%2BM09Sxq1y61fK68KXKS64yw%2BaBngNlmUFfz7GePKyxyPiiLYHQEwkkPURStV6kyeMmnE7A9anr6VkM%2B8VdurLHTZciLOma3ta6NgkYJv8WvfB9ZNF%2F5KBgGdO4f3ADgeJBOE4VN%2FSAJ%2FjaaKz%2BpviGf9tzEL80hclL7nCbKLWQQwEbxa1F9VEs220JprEAnAcBue3vf121d0NLCdFC%2B07SQFwINT9wIaYhhqWyqx%2F3SDLn%2BA1u0HiChBKe3tws%2By%2FlwSkGVJi7G6wNll31Zuw%2BaB7LyPeL1IgW9UkWz1TWaDuQl0EOAyePzakpkqMnfxOaxtVXdDSjWdw85d0zCj1SvBzXXtdwFEbFbWtxtst6QW6SHOmVTMQgG2prtf2lYmctDB3XtNJWT7D0RPUcLdp5KPbhPA236RkTneoVXz%2B%2BBT2ARJjp8cyyTKdGHlYYyqcApg1IlxeCef3eqY1GhyNTXnkK2BUi8nYU8P%2FkPPV8XM4n4eO51M5z8CdjFGHo76bJqiKWNxGHGlzd9qhVGUQg7EFNY41JGhZQ9dthLAP4aEuOi5y3VUXT2OwD6aRIV0KjrEKDjoLl7%2BGoXUK2GYMmvVTYXJg9yaOJwZ2MulDKYsojnyuwlBnqu9Lhpps1iptwSL1BmN%2Fx%2FWN5kBxqVvoB74dRgczSbR63B7irkQbnZUQhf0mxOkDJlVnu%2BWV1JKoADwBmlokHMWdfa%2BfMrAtnYa3uQ65L1zbNp4yuFlAI7riL3IVp%2FvNvK3KlwGlEkCG%2FTTW014PVreSIgXwrsDKuHIV4AWsS0JwFrJvh693TfLx7XTKTC7U8HLtFU7SOImpN8pCiGTQxRmbxts1omBDru2Efjl%2FfL0N6Gz0nbi1jSeAse%2BHsHwMG1aGfYxdPlHBIBR%2F8u9sgC9q0HkMjVV5%2Bc68hIndc7jpUQzsC1EKezNCFtrQr5I0wAQ8UXgXLsrGnHYDNWQSWMKzSVfHWxWPyMo132GjPWjBimPjfmMFzORd%2B0LHHSh2B4w0QM0jOUi06DqSuoL5dSgWlkyxiVvH3A%2Bqa%2FOkSb87X6N3Hw2vSl8AKECe%2BjEBVoVT1B%2FE%2Bn18zgadx1lg4mOi4qCPKA5kU7VKhHwVfk9u2VWlPl0jnS4N6JzQD2tkFJJUQyr0MqQLbngyazZ%2B31Cjc4bcf%2FRwo6lRJRg4wvCJwjwt07jE4zx8fWglrYuRgQaiAdDL4yc%2F8nhtciPHLeq5O7Kq%2FfVhyV538FIPDeTDU99ZcTncT1Y8AoB00NI77EVt7GCuFwQLl4pNWqWn6O8L10BFXyJ%2FCkp0HLp0lubh4fIXc6PQgNUZj1V24lBEp4%2FEWC13wNpDDpfglgGZ0E%2FPRWK99f1n6kqqxOYFfc4XhIFNL95qV9xTBFS2klo%2FWjNqnD5q1xLDz3EYqdMS6FRxYsCEZXuigC0MCgTZXmIMBJye6Z21GLE71pOVRUbJf4xWUicnuqYPjiWzXbRIxE5%2BUQKraRe00fuQKULK2ql7sFPxsozw5cyn%2FrM9NKduQ2VuU4vzwcUaAxPkLHnc7wF9ggIteqz8rB7XPXm3z3K3iJyrbp9V6gxuxw417MyuoLvWzn11vSP3c18jlJDF6efwL4ba7R1NUvZA5Q%3D%3D',#"JpusKGOPchRntrz%2B7SDJvJJuwd9XgtFrvqVrFWRY9zMVCeLYD3TY6iFF3zPxNX66oXXm3SrZfHSAqq4BNF0VMx7cR40Um4fM0m7WxyqI4%2Bgwt9eXhEq2AEqSwYEPJcNCz5G%2BR%2FZNUTo4wL%2FmQAoHDoFW1QZCQMRwcXyaxcn%2FpVX%2BDZdq6oULuHLoAd4ihDgzWkvDTMReUXN1Nt8xptIssvljFzPvj1bqGeTrpuTehfxQ%2FBK%2BOJ78LNGG28K4pmDLeHqdRxHcvunenADW9nL9MpMA9HuzocO3jTSUK4JIWHLcQw%2BSiI9OirMktYDuSJCQPif637vyn5eEBjCarrTZnyEnFvga83G7wL60%2BMOJarOHk3eMPSzl%2FCnv8Utw%2BSTugb9KbMq1bKNhSaBdT3l6LJbxvgZIBsDKfdAftpjtl2lBgtoU%2BNTs7F8iGHRCozzBoeiu67Xt8EUmBTzu%2BBuaH9lZFjGcnIN9tqQqnMTrWp%2FTm3WsVx97IiwVZAEAZnZa8DoYkby6aTZvZ0jFpggANjiheozDfeLhIqulBhuDa2jDvtF%2FZOrkiqVfvLuVs7%2FCYptEv%2BcxMLyCQhp42BNAQjbz%2FIrbp0V4DMSci9%2F4pdsAKU8huRj7PtRFQf2cib9wLEY83p3Kud1xcSt8sUI2jtJt%2BquOsMbW7g1%2BbBpYpzJ6dlbmg4k7mOnZYJkKKKU%2BGDPgDioCfK3406YVw5FDLdn7nPkH7l651KLWkHU7BMQYi4rIZwDrziWeZMh7xwwoS%2BoNcHTNA0noycVzwTaCTEEjd7oS2f8RI8L0bxzcA6y4EZHsLx837Lv7D8c7om92xK%2FDiO3G3zfhB5tTpOXN9lmEUO%2FQr1W%2Bk6w%2FzgrzwbIGCrGYbNDi7XJAmQOuKkY7SU5QkwO4ghpeTTDVqmeo8awNHEmRLoJ5nWn6xy9PzddtK2kMEiF5C6yPWOzn%2F0Zj%2Fd1B44JfSwFklR1OfjWyqVh8wtghnKnjTjFyOIUL%2BGmDxWTQ1fpzaEem77YNQYTuMkqR1dbkcLOaIVv2dstXntgzAegbMTBCjcg7p4OlBRTnjFbXnDpGMQI4yBfYunBaqGLL4gc%2BYu1n4tndSU40txSOQ%2BRvegTcSs9RRJjZ9bjkDsJhnsl%2FkuCAogVbzQ09PHLYCdGWubPSX4SEPyyaJ5XItpL87Fpxe0MsgBUyQPhULtao2DtP2sG7iZIXXD85Ae7j%2BMm%2BguNSNQpWsC%2BJiUVZpjZXpzZlQ8xT%2BdhveZ6hGBXmq5vSPV3%2BNvbsWRsuoP%2BzgNQePmc0ZN8sya3BIWs9QZX2SbQY0MMIOI9tXkZHbKMzSgfzxS0cLx8csKE7GZCoq5ND57x21VNBLFyMJRY4Sa1m8Orb8bJJv21RxcnAGkvo5U5lYBbdeOu5L5BnZBV3jXY8GbAZLNrZtXRLm%2B%2FExvRFEEN4J7tV8a5boGmVh0U39EDiL%2FQp9aQ52dZDN75RvbzCG7UCSX8Mwvi7aiXspDSDmK4Ev9J06JaGSX4d5OQduHIuj6oY06%2FHUgqsyXKDk1z4R%2FvZ6vFn1R7QF2szAEtfV%2F%2BR1Pu%2FYzRf6YXBa7xBK7YFB8a2RWYC1iP%2FzIV%2F3eQkmq9yQKr7ehjS7YR7M1R5I0d38ib9Ny5sWpWe5aVUNpGnvfPjYUits001Xfna6cooo5KZMYeSWctaPZAbXCnlR2HTH9YuhT%2BbO9cD%2BQWuvPR3WW55AHeHL35A1K6h%2Fz7UyMPFI5fflH7TQ6OaC5PoYGTTX6Bz2vnxr0rbzeBViVj0yGuYALd4Z1q1yhy9s42395iT2EEEHAwhuAksiIedo26bGiHwyzsHA8f0RjnTy2m2hpbFxVNZ0O%2BdoYNTo5tkaf%2FjLU%2BLL0pjb8UNOLe2gq%2FFD12N27SrziN%2FhkKklOVgSz4KXGOCxCMMzmFYKmRQJ84IJaH5iB1%2FK3xNLSrHyUh06Kz%2F%2F7fm0%2Fk62gHVTESXJ8ziudxiUGpCd8O0pXRzlkOrLFY%2FqW017PsloVisLPCdN8ZgCDF%2FyAL%2FD0t9q5cpQJOXfmXdhdeFnl8g2H1n9s8nDeY%2BHbvXHf%2FrkONIeFvIMTflwGdolDBZ9H9ZTBAHP8d9%2BPUhWWl2Ba73fj%2FQXsV00I963Cj5SlxXSnlNdFOLtO7XBzkztmbMc25WVd%2FA%2FZ9g8HLGHIn5TwN0HUKK%2BztDfzC3i5Gw6Zustqyn1BNPeLnYa8qDgCUXMKjoxAGrXmQ1KYYko94%2BXadrbMXMssbSgKkOuFtKPyZ4wdStvo6EML2DmwhASqFjmzuBKGxqCRu4K%2BJdHLs%2BZXiBAhkxge5YDupNUe%2BZ4nKSKJSzEanyMSv8137cHYkM3B8ghwv%2ByUm6fcdvQuH5kg7NBJUD%2BNWWJMELLMtwSoPAA90CjfFLvDcNjS87YmuBKUSFEYt%2FLNOOmKw89RaD64YIZ2gHBVZ4SFDt2gBdtSfJ7F%2F2bKnWInSNZSmITux9tYEhosahvIfacBXxVZH5cezU7qi5DqLUh3xbFxxeCLnnxfprECw3eX85YGtqGa98enluTgy8sgpihXPWWUExuNbCKUnlV0NFN7vdgXgWQCsqXC1qudj5DM2q9niUqr7U57%2BQUi21oUhqMx0oDOQjWreSAyuea74oW8wwLM8IUeNuecBmXQbFcdWkUA2yXbaqcGmxgXCYCslyDwQj%2F1hXBnoIDUz7aZ2cTf3mRDo%2F7117xjzivw9JUaDWacn40%2BTmoXOVwy5oFpLUM%2BuReIF0O7yO8GH0k%2F6kLQWtd5qO88ES9CSaPjhNIXisyMlAAIhsRjelOUsWO0cuj%2B1nBE%2BoFldT%2BrAWpfLN3V9ubbphON5AWQGL3IwQNva0sion2%2FHXtW%2FMh3yEZ0%2Ft%2Bu8AjqlRw%2BLsXto1VjUEYfAhMSkts6eQYAyM6dLsSJYDMWQMPtzNHxHxSlvXvtLjmi%2FfyINpOHvBaxYN%2BCzt9VmC%2Fjg8uv2gv3QFodPSxEZZFxO0ors2iToBSz4ay4Bo0cuukCgaWMciD2%2FmlDUdjKqseOf7BzMGtdAyZi%2F9oDyvh%2FrPDz1Kw6rcEAphl%2BESWfymshmegjuZ%2BhKoUfyWJbYYbLJSaizb8DN5ZGexxNgln0KyX%2FXSZVyph5xX%2FMBj4Co9FgddmwsdJjjrWTCyV5M8eBzqyfJ8qdkdTI8FiOCIKTeN%2BVI1QQb4NJ4DjBNf0JAnS%2BZFjlImXqGw22MqI9jgdJzez1L2JSsUyq3Y50J3U1uyHjNrt8CrWgs3IvQTGx3qMBxxZd83Blzk4qYoJvatw8zwD2inMz7p%2FuQKrvQphHicMRleObyd7xrnX0VaNrgG1KpiYGQ9LtcYMSVSCwH2cnc0FuKoUA3iQu%2BDhh4wLc537ESiajF9QpErFSDqtfNCNohlBFJPH%2B%2BLubjgu0V3zb3WBnRYefnbosaVKYrDErvAxyOoIz0aQKRBMCxZFiA6fpcOYDsqqO7Icqu4TOB%2BRzC6u%2BW7adax0OluGLFuS2ETKl%2FmvhhTGEkMmrWAk%2Bt%2BTVh%2Bb2oI36A2OjpTYX62q6k8jTeMsvJZWRmNEunnsA9kUMf%2BhrfLEf6uQApIi%2F7WGSyCoJZFGaS3rBAqNMB4lmLCEnGpb8VkEuATnmss1UgUOmJr8ZyVg9GKP99Dz4RuNE%2BnNz9JjfjzPD8cVTMFjgBn89Neswr9052WGwpPDkqy8F0HbL49ER2WelGgnhm3ERGxYol3plNiv10s6O80uVCvDmYnpI7Od4i8stv0eUuapkop5e07nDVghHPbinp9GL4dhsBTtcwaV8JW4tqGd63qY8pv4wBmCE5l9ad9I%2BSpAL4zhsUFv1WProNp334rErpRmdBWYE95Rueau%2FNWWwkd9E4aGnZxk4hy%2Fje3Qduxv5ChFNtnDZaWQiHQbDKSdyvCFeb6FuqNoi0WCkVJnDGYmFOayrtoScf%2BnRb0LkmHQRQ1PWJ%2FdXsvW9bkaSQnlY7k10fwMBPjgO5%2B1OSx%2B%2Fg1tufKIIQIys4%2BqZXK4thKMUyKAmU6%2Fc2FgGFKPeVLDJrRC8JXYaT9a0%2B0rrLhfgqO4KR8yzo4AssjIvzQFPOVsc3G6%2F%2Fg9y32bJb9u5iz5kfgSkFyJRwymqLzLyyQLqhFY1cwDiu7TS1h5ruA93pemXH%2F%2F52nFeKBAM8Dugpwc4DUMJnzsoilhmVTlgzZ%2FJ%2Fk8Yut%2Fonbyi%2FzjsGDJ8%2FQUY6Y46QgbuGqb7jCWuOOKgqDO4iltbopgTXpOom2BDb5pagFTRhjLpF85QrjWJ1ZRHzxG%2FbUXPnDiRUH4wKGEGuI%2BY%2FC7rPoGfx0ALXzB1bZgBKfyQz1CZv8GoCmi47y2BuwJGXYrj%2Fh2VfwXNlH91ROpYUdDhv7F2zVeserzhLnDkTxnJLUUZqhG6m1nUPq8019fzN%2BQHaNkL%2BwWvZAC6hhOKFsJC1%2FqHDXBgRKB1MzbaSc1i4T%2FO6ltujaYmXjt1mVeBd4o%2BX%2BkoG6L28VYFFQxMXnWAHrALTl1gJ2mvkXAHq%2F%2F0uGZaEBe%2Bh%2FSpXLBdyZXE3iZTDWiCSPIqncDKPu85jSQWMITkrMpw93UP%2Fv6vM2lQy6CQapmvp0ncFMpZ2yK%2FZJfPYMHZ9Dvmo1aBAecABhi%2FWd6ySwUHnTN5hXe4Wjj%2BmtzOQJLxGH%2Bc2tE7pX9yEPTchaknYrWEw4ss6nGfzX4qucgidHjJDj7DHGTeoFx7EWGY1wYNOSi5BVc6z8YbprAApb%2BxWtDXeulgdFUQ58ZrHtj4BhmmcvD44QZfogFNKRGodUEFLS%2F7krbulPk7mJeKyx3LgPFcuqHuTyHIKobaVM6gMz14YiN2gR1IBfjb6x%2BQO61WK5QfiH6CCLQCecL5bi0WdNfh2furusSgy9dVmWRf1ICIzJcKo33HA1U8plybB5EP4A1hhPNdSa8VX1jIdpLZ6z3gxyPhCLwmqMIUf96g2z%2BBlPRl8Bt3uxNFI1qgbcrfGXerQoJ1rACSUPQNHMV0RabKGlvQ4cidnVYH2k%2FTs3Y5DHZ%2BJRtY00nEjjL8Y%2BNs4rYS06nBd7VbBjC0hhTTVFpUAtZBlfSFDxLlZ8ut9KcuneR%2BkaAA8B1VE8ZvO2qnr5HYdcgFkwuRbzuoVFJnoqjNqQKqxv%2FU3Bd0TB3WAwCIley6oCN4krQbau2oeWBJehuSkCmdzZiYwfBhgay%2B59M4gNU1q%2F2EnGHs1G%2FyOabAGlI2JONHIzzTwDTDaz0ZImUQHDX26%2B6rO%2F7b5oJ1X2o1%2BOKgib%2FIuHefe%2B9GUHbwB%2BybQ8Ba08eHQnJNEq4RcbEgbMG0IOB%2BXbb3FQa%2BS1Y3vy8EuEtb4Qngm1GnM1%2BgiUXJIthpK5DICcw59sBqwtllVJpk3CM0koZpNsL0oeNoFbPYosLtwGMPh4bVToT%2BPan74q8mmuQR%2BCLUiNVKR9ve9%2FV0BkX8zBIA3Ba3MqztpRPeCCNK2fZ33PC42Wj7rLJ9w%2B5eg3ocMxMlj7zCJdTJZBlwbZbqgcolWEZTkFY31hLveOEUBA%2Bkt3eZpBzq%2FqtLtWEFefTv8J6wbsRWiTZyFZ9uqfSlFzYQMSeaKdK%2FAassM3KoFwFhLatnWXG%2F%2FUm2xkRSkY3cOBOvyZNPof%2FlltKshlZdeXulN5zuWm5OYL%2BIuNVczEygtJmbGIyFjVb7Ceki0swG44q33ViymgB5cusFRHdPUFB7FFB%2BSZbFIqDoHWmocgokbyFNndlJHU8%2FPigBDnS4lu%2BJ%2FythpRgYwMKBRatjwZdXhY3J8uh4sKxzFp6pD7DzUh83mZIJpeIAyJ3zRVuAv1R%2B7sihfTgUMM6UyKJ8AoRxn7bVkKyO2PuDvTNQfRPAqmn%2BRiqdI0PNNV9jZ3TfsppU5rLHeBpF69C8IGcEcdq%2BYKp6zdZqdCzW9GuaajuIV%2FjE%2BbyrQMHtmQc9PjPT2jWQWRKqe6Ua%2FHPdx0NmkjCYhzqkEO7hMTBeBiv9lqHowGPXDLMRF4zt9aD%2FtNawLjFyi8Gfv0iPjH%2B%2BbJUcRfB4sQRE0kHaUXHr1WAv4DD1WXR%2Ff4Cvj0TUVZLkOrRmKd0o6gY053WrbE11wfxf4HeGF2BsSpvfaDB9oND4hai%2FlZmgfnRl5nMxbwG5216mzamchRwg76JMOFJ5iJG6L1ZgXnR%2BUc0YGK9EW7L%2FVeKaEFs9pmIriG1dZbTLEcmcxp0EMt71s%2FlLuq2pmh%2FTKQYoMiHtJwX7626tfDkpkSOGfLP6B5Uq%2BWFCBLQ7uDhuuhhkIjTNecMkLS%2FOaoWIlTzjICimKiKx9kAHP1tIy21jVglCWHOIOEW62nl2NFxg8v9%2FOZOwoVUBVIwhua0Q0IjOTSyJNVgvyYC1ClXQGiiUjdj9UU%2BUwTcBy4ekqPjQJz%2BxQ5QCYgub8oX1vzZI3Y%2Fj3fenHf8GpSD%2BsVRYncttOme0EVrurzVi0vk5JFHVBRTAANoHOdRVstOF2LL%2BmUOVAiSeglkIct8%2F315J6z%2F2GXgccFy8BkvAGAVoCIxmdshjmOTKdL5YBKCpcflLXObbgRJjts9EJ4jz%2FVKnalcWZ%2FfAOOofVr9Qs%2F8%2BgZTNHAOJ9ZFjjWOkk%2BgALFm6iT8mdb9C%2FHsyL3V99w9vNzq99jC%2F2MgUhXR0DAf7oNE7FeEGxWiXsfEM3H4hecIAPTYyfpFBREUtIOX%2F2N2weNu9EIBu%2Fv46asFawfvz8C6dhvtw6yWtAT4xQbUmx9l6RQz1ZWXiM%2FjcHk%2BZJVeBP4FaClIyB%2FyXhP969lz926ULZsursineJCzxectp5BuIgcM90ycr%2FkslcbtUuRxFgrXrPjYjYp4gnLFWSsSGV000zpNutVV2rb0FG5yA%2BNIXJpFrw7TJQl9Hmxd7eeWMMBiR7JAz2RjGNKU%2FEgiw%3D%3D"}
# 	}
# 	params=None

				
# 	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name = campaign_data.get('package_name'))

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(user_name_list)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"



def timestamp():
	return str(int(time.time()*1000))


def gcm_timetoken(app_data):
	if not app_data.get('gcm_timetoken'):
		app_data['gcm_timetoken']=str(int(time.time()*1000))




def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(util.get_random_string('decimal',19))


def def_installDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('installDate'):
		date = int(time.time()-random.randint(60,90))
		app_data['installDate'] =  datetime.datetime.utcfromtimestamp(date+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone')


def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date+app_data.get('sec')).strftime("%Y-%m-%d_%H%M%S")+device_data.get('timezone')



def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec


def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0


def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def get_afGoogleInstanceID(app_data):
	if not app_data.get('google_instance'):
		app_data['google_instance']=str(util.get_random_string('char_all',11))
	return app_data['google_instance']	


def get_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token']='APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))
	return app_data['af_gcm_token']



def get_batteryLevel(app_data):
	if not app_data.get('batteryLevel'):
		app_data['batteryLevel']=str(random.randint(10,100))+".0"
	return app_data['batteryLevel']


def unique_id(app_data):
	if not app_data.get('userID'):
		app_data['userID']=util.get_random_string('hex',16)
	return app_data['userID']	

def self_token(app_data):
	if not app_data.get('token'):
		app_data['token']=util.get_random_string('hex',32)

def self_fbid(app_data):
	if not app_data.get('fbid'):
		app_data['fbid']=util.get_random_string('decimal',16)






def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))			



def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))





def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])


# def def_deviceFingerPrintId(app_data):
# 	if not app_data.get('deviceFingerPrintId'):
# 		app_data['deviceFingerPrintId'] = 'ffffffff-'+random_string(4)+'-'+random_string(4)+'-0000-0000'+random_string(8)


def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] =jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"


def appuserid(app_data):
	if not app_data.get('appuserid'):
		app_data['appuserid']=util.get_random_string('hex',32)+'+'+util.get_random_string('hex',32)		


def tm1(app_data):
	if not app_data.get('tm1'):
		app_data['tm1']=int(time.time()*1000)


def tm2(app_data):
	if not app_data.get('tm2'):
		app_data['tm2']=app_data.get('tm1')+random.randint(3,6)#4

def tm3(app_data):
	if not app_data.get('tm3'):
		app_data['tm3']=app_data.get('tm2')+random.randint(60,90)#70

def tm4(app_data):
	if not app_data.get('tm4'):
		app_data['tm4']=app_data.get('tm3')+random.randint(300,360)#326


def get_conversion_data(app_data,need=False):
	conversion_data = "utm_medium:(not set)&utm_source:(not set)"
	if app_data.get('referrer'):
		referrer = urllib.unquote(app_data.get('referrer'))
		referrerpart = urlparse.parse_qs(referrer)
		utm_medium = referrerpart.get('utm_medium')[0] if referrerpart.get('utm_medium') else ''
		utm_content = referrerpart.get('utm_content')[0] if referrerpart.get('utm_content') else ''
		utm_term = referrerpart.get('utm_term')[0] if referrerpart.get('utm_term') else ''
		utm_source = referrerpart.get('utm_source')[0] if referrerpart.get('utm_source') else ''
		utm_campaign = referrerpart.get('utm_campaign')[0] if referrerpart.get('utm_campaign') else ''
		conversion_data = 'utm_medium:'+utm_medium+'&utm_content:'+utm_content+'&utm_term:'+utm_term+'&utm_source:'+utm_source+'&utm_campaign:'+utm_campaign
		if need==False:
			return conversion_data
		if need=='utm_content':
			return utm_content
	else:
		if need==False:
			return conversion_data
		if need=='utm_content':
			return "2125146_7958936368d17e161f1e2c6a1179"

# def get_data(device_data,campaign_data,app_data):
# 	dic={
# 		"androidVersion" : campaign_data.get('app_version_name'),
# 		"manufacturer"  : device_data.get('manufacturer'),
# 		"model" : device_data.get('model'),
# 		"installedApps" : campaign_data.get('package_name')
# 		}
# 	#============================
# 	dic_str=str(dic)
# 	print "in str",dic_str
# 	#============================
# 	dic_md5=util.md5(dic_str)
# 	print "in md5 ",dic_md5
# 	#============================
# 	dic_64=util.base64(dic_md5)
# 	print "in base64 ",dic_64
# 	#============================
# 	app_data['encode_data']=dic_64.encode()


