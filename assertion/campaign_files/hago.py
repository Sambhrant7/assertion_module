# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util, purchase
from sdk import NameLists
import time, base64
import random
import json
import string
import datetime
import urllib
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.yy.hiyo',
	'app_size'			 : 56.4, #48.0,
	'app_name' 			 :'Hago',
	'app_version_name' 	 : '3.3.5',#'3.2.8', #3.1.8
	'app_version_code' 	 : '11942',#'11115', #10563
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '4.4',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'tXsmvN55RtfUvDkCRmAGt9',
		'dkh'		 : 'tXsmvN55',
		'buildnumber': '4.8.11',
		'version'	 : 'v4',
	},
	'purchase_variations':{
		'1':0.99,
		'2':1.99,
		'3':4.99
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	

	###########		INITIALIZE		############

	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	################generating_realtime_differences##################

	def_firstLaunchDate(app_data,device_data)

	app_data['appUserId'] = util.get_random_string('hex', 32)

	if not app_data.get('games_completed'):
		app_data['games_completed']=0

	###########		CALLS		############
	print "\nSelf call : PushLogin\n"
	request= pushlogin_selfcall( campaign_data, device_data, app_data )
	util.execute_request(**request)

	req = android_clients_google( campaign_data, device_data, app_data )
	resp = util.execute_request(**req)
	try:
		resp_str=str(resp)
		app_data['gcm_token']=resp_str.split('token=')[1].split('\',')[0]
	except:
		app_data['gcm_token']=util.get_random_string('hex',11)+':'+get_gcmToken()

	print "_____________Register call________________"
	request= appsflyer_register( campaign_data, app_data, device_data )
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)
	
	time.sleep(random.randint(4,5))
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)	
	util.execute_request(**request)

	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	util.execute_request(**request)

	time.sleep(random.randint(1,2))	
	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(15,30))
	loginsuccess(campaign_data, app_data, device_data)

	# time.sleep(random.randint(5,7))
	# event_1(campaign_data, app_data, device_data, eventValue='{"gid":"","function_id":"phone_click","click_source":"1"}')

	time.sleep(random.randint(3,5))	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)

	# time.sleep(random.randint(15,20))
	# event_1(campaign_data, app_data, device_data, eventValue='{"function_id":"phone_send","phone_number":"IN  +91_7429598517_"}')

	# time.sleep(random.randint(1,2))
	# event_1(campaign_data, app_data, device_data, eventValue='{"function_id":"phone_show"}')

	# time.sleep(random.randint(40,50))
	# event_1(campaign_data, app_data, device_data, eventValue='{"function_id":"phone_send","phone_number":"IN  +91_7429581483_"}')

	# time.sleep(random.randint(12,16))
	# event_1(campaign_data, app_data, device_data, eventValue='{"function_id":"phone_login"}')

	# event_1(campaign_data, app_data, device_data, eventValue='{"function_id":"phone_vail_next"}')

	# time.sleep(random.randint(15,30))
	# loginsuccess(campaign_data, app_data, device_data)

	# event_1(campaign_data, app_data, device_data, eventValue='{"gid":"","function_id":"phone_login_success","login_time":"86835","click_source":"1"}')

	time.sleep(random.randint(3,5))
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)

	time.sleep(random.randint(20,30))
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)

	choice=['messaging', 'discover_people', 'enter_game', 'play_with_friends', 'challenge_game', 'visit_other_profile', 'login', 'visit_my_profile', 'gathering_game', 'enter_game', 'enter_game', 'challenge_game', 'gathering_game']	

	random.shuffle(choice)

	for i in range(random.randint(10,13)):

		if random.randint(1,100)<=10:
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)

		if choice[i]=='messaging' and random.randint(1,100)<=70:
			time.sleep(random.randint(5,10))
			Click_Messages(campaign_data, app_data, device_data)

			if random.randint(1,100)<=93:
				time.sleep(random.randint(5,10))
				Click_AddFriends(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				Click_Friends(campaign_data, app_data, device_data)

			if random.randint(1,100)<=43:
				time.sleep(random.randint(1,10))
				send_message(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				IM_Send_Message(campaign_data, app_data, device_data)

		elif choice[i]=='discover_people' and random.randint(1,100)<=80:
			time.sleep(random.randint(1,10))
			Click_DiscoverPeople(campaign_data, app_data, device_data)

		elif choice[i]=='enter_game' and random.randint(1,100)<=95:
			time.sleep(random.randint(5,10))
			entergame(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			Start_PK_Game(campaign_data, app_data, device_data)

			if random.randint(1,100)<=97:
				time.sleep(random.randint(90,180))
				CompleteGames(campaign_data, app_data, device_data)

				if random.randint(1,100)<=77:
					time.sleep(random.randint(1,5))
					add_friend(campaign_data, app_data, device_data)

					if random.randint(1,100)<=50:
						print 'after accepting the friend request'
						time.sleep(random.randint(1,10))
						send_message(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						IM_Send_Message(campaign_data, app_data, device_data)

		elif choice[i]=='play_with_friends' and random.randint(1,100)<=60:

			time.sleep(random.randint(1,10))
			Click_PlayWithFriends(campaign_data, app_data, device_data)

			if random.randint(1,100)<=92:
				time.sleep(random.randint(5,10))
				add_friend(campaign_data, app_data, device_data)
				time.sleep(random.randint(1,5))
				Add_Friend_Request(campaign_data, app_data, device_data)

				if random.randint(1,100)<=50:
					print 'after accepting the friend request'
					time.sleep(random.randint(1,10))
					send_message(campaign_data, app_data, device_data)
					time.sleep(random.randint(1,5))
					IM_Send_Message(campaign_data, app_data, device_data)

		elif choice[i]=='challenge_game' and random.randint(1,100)<=85:

			if random.randint(1,100)<=71:
				time.sleep(random.randint(1,5))
				Click_Challenge_Game(campaign_data, app_data, device_data)

			time.sleep(random.randint(5,10))
			Start_Challenge_Game(campaign_data, app_data, device_data)

			if random.randint(1,100)<=95:
				time.sleep(random.randint(90,180))
				CompleteGames(campaign_data, app_data, device_data)


		elif choice[i]=='visit_other_profile' and random.randint(1,100)<=65:
			time.sleep(random.randint(5,10))
			Visit_Other_Profile(campaign_data, app_data, device_data)

			if random.randint(1,100)<=92:
				time.sleep(random.randint(1,5))
				Add_Friend_Request(campaign_data, app_data, device_data)

				if random.randint(1,100)<=50:
					print 'after accepting the friend request'
					time.sleep(random.randint(1,10))
					send_message(campaign_data, app_data, device_data)
					time.sleep(random.randint(1,5))
					IM_Send_Message(campaign_data, app_data, device_data)

		elif choice[i]=='login' and random.randint(1,100)<=50:

			time.sleep(random.randint(10,45))
			event_1(campaign_data, app_data, device_data, eventValue='{"function_id":"phone_show"}')

			time.sleep(random.randint(1,5))
			loginsuccess(campaign_data, app_data, device_data)

		elif choice[i]=='visit_my_profile' and random.randint(1,100)<=45:
			time.sleep(random.randint(5,10))
			Visit_My_Profile(campaign_data, app_data, device_data)

		elif choice[i]=='gathering_game' and random.randint(1,100)<=45:

			time.sleep(random.randint(1,5))
			Click_Gathering_Game(campaign_data, app_data, device_data)

			if random.randint(1,100)<=94:
				time.sleep(random.randint(1,5))
				Start_Gathering_Game(campaign_data, app_data, device_data)

				if random.randint(1,100)<=96:

					time.sleep(random.randint(90,180))
					CompleteGames(campaign_data, app_data, device_data)


		if app_data.get('games_completed')==10:
			ten_games(campaign_data, app_data, device_data)


	# time.sleep(random.randint(30,35))
	# entergame(campaign_data, app_data, device_data)

	# time.sleep(random.randint(1,2))
	# start_pk_game(campaign_data, app_data, device_data)

	# time.sleep(random.randint(5,8))
	# completegames(campaign_data, app_data, device_data)
	

	return {'status':'true'}

#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):

	if not app_data.get('times'):	
		print "Please wait installing..."
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')

	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	################generating_realtime_differences##################

	def_firstLaunchDate(app_data,device_data)

	if not app_data.get('appUserId'):
		app_data['appUserId'] = util.get_random_string('hex', 32)

	if not app_data.get('games_completed'):
		app_data['games_completed']=0

	################ calls #####################

	if not app_data.get('gcm_token'):
		req = android_clients_google( campaign_data, device_data, app_data )
		resp = util.execute_request(**req)
		try:
			resp_str=str(resp)
			app_data['gcm_token']=resp_str.split('token=')[1].split('\',')[0]
		except:
			app_data['gcm_token']=util.get_random_string('hex',11)+':'+get_gcmToken()



	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data)
	util.execute_request(**request)

	choice=['messaging', 'discover_people', 'enter_game', 'play_with_friends', 'challenge_game', 'visit_other_profile', 'login', 'visit_my_profile', 'gathering_game', 'enter_game', 'enter_game', 'challenge_game', 'gathering_game']	

	random.shuffle(choice)

	if day<=3:
		no_of_times=random.randint(10,13)

	elif day<=5:
		no_of_times=random.randint(8,10)

	else:
		no_of_times=random.randint(6,8)

	for i in range(no_of_times):

		if random.randint(1,100)<=10:
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data)
			util.execute_request(**request)

		if choice[i]=='messaging' and random.randint(1,100)<=65:
			time.sleep(random.randint(5,10))
			Click_Messages(campaign_data, app_data, device_data)

			if random.randint(1,100)<=93:
				time.sleep(random.randint(5,10))
				Click_AddFriends(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				Click_Friends(campaign_data, app_data, device_data)

			if random.randint(1,100)<=43:
				time.sleep(random.randint(1,10))
				send_message(campaign_data, app_data, device_data)

				time.sleep(random.randint(1,5))
				IM_Send_Message(campaign_data, app_data, device_data)

		elif choice[i]=='discover_people' and random.randint(1,100)<=75:
			time.sleep(random.randint(1,10))
			Click_DiscoverPeople(campaign_data, app_data, device_data)

		elif choice[i]=='enter_game' and random.randint(1,100)<=90:
			time.sleep(random.randint(5,10))
			entergame(campaign_data, app_data, device_data)

			time.sleep(random.randint(1,5))
			Start_PK_Game(campaign_data, app_data, device_data)

			if random.randint(1,100)<=97:
				time.sleep(random.randint(90,180))
				CompleteGames(campaign_data, app_data, device_data)

				if random.randint(1,100)<=77:
					time.sleep(random.randint(1,5))
					add_friend(campaign_data, app_data, device_data)

					if random.randint(1,100)<=50:
						print 'after accepting the friend request'
						time.sleep(random.randint(1,10))
						send_message(campaign_data, app_data, device_data)

						time.sleep(random.randint(1,5))
						IM_Send_Message(campaign_data, app_data, device_data)

		elif choice[i]=='play_with_friends' and random.randint(1,100)<=55:

			time.sleep(random.randint(1,10))
			Click_PlayWithFriends(campaign_data, app_data, device_data)

			if random.randint(1,100)<=92:
				time.sleep(random.randint(5,10))
				add_friend(campaign_data, app_data, device_data)
				time.sleep(random.randint(1,5))
				Add_Friend_Request(campaign_data, app_data, device_data)

				if random.randint(1,100)<=50:
					print 'after accepting the friend request'
					time.sleep(random.randint(1,10))
					send_message(campaign_data, app_data, device_data)
					time.sleep(random.randint(1,5))
					IM_Send_Message(campaign_data, app_data, device_data)

		elif choice[i]=='challenge_game' and random.randint(1,100)<=80:

			if random.randint(1,100)<=71:
				time.sleep(random.randint(1,5))
				Click_Challenge_Game(campaign_data, app_data, device_data)

			time.sleep(random.randint(5,10))
			Start_Challenge_Game(campaign_data, app_data, device_data)

			if random.randint(1,100)<=95:
				time.sleep(random.randint(90,180))
				CompleteGames(campaign_data, app_data, device_data)


		elif choice[i]=='visit_other_profile' and random.randint(1,100)<=60:
			time.sleep(random.randint(5,10))
			Visit_Other_Profile(campaign_data, app_data, device_data)

			if random.randint(1,100)<=92:
				time.sleep(random.randint(1,5))
				Add_Friend_Request(campaign_data, app_data, device_data)

				if random.randint(1,100)<=50:
					print 'after accepting the friend request'
					time.sleep(random.randint(1,10))
					send_message(campaign_data, app_data, device_data)
					time.sleep(random.randint(1,5))
					IM_Send_Message(campaign_data, app_data, device_data)

		elif choice[i]=='login' and random.randint(1,100)<=45:

			time.sleep(random.randint(10,45))
			event_1(campaign_data, app_data, device_data, eventValue='{"function_id":"phone_show"}')

			time.sleep(random.randint(1,5))
			loginsuccess(campaign_data, app_data, device_data)

		elif choice[i]=='visit_my_profile' and random.randint(1,100)<=40:
			time.sleep(random.randint(5,10))
			Visit_My_Profile(campaign_data, app_data, device_data)

		elif choice[i]=='gathering_game' and random.randint(1,100)<=40:

			time.sleep(random.randint(1,5))
			Click_Gathering_Game(campaign_data, app_data, device_data)

			if random.randint(1,100)<=94:
				time.sleep(random.randint(1,5))
				Start_Gathering_Game(campaign_data, app_data, device_data)

				if random.randint(1,100)<=96:

					time.sleep(random.randint(90,180))
					CompleteGames(campaign_data, app_data, device_data)


		if app_data.get('games_completed')==10:
			time.sleep(random.randint(1,5))
			ten_games(campaign_data, app_data, device_data)

	if purchase.isPurchase(app_data,day,advertiser_demand=10):

		r= random.randint(1,100)

		if r<=50:
			choice='1'

		elif r<=80:
			choice='2'

		else:
			choice='3'

		revenue=campaign_data.get('purchase_variations').get(choice)

		time.sleep(random.randint(20,30))
		purchase_event(campaign_data, app_data, device_data, revenue)

	if app_data.get('flag_isPurchaseDone')==True and random.randint(1,100)<=20:
		time.sleep(random.randint(5,10))
		Click_Gifts(campaign_data, app_data, device_data)
		time.sleep(random.randint(1,5))
		Complete_Send_Gifts(campaign_data, app_data, device_data)


	return {'status':'true'}


#############################################
#	self calls
#############################################


def pushlogin_selfcall( campaign_data, device_data, app_data ):

	url= "http://short-yypush.yy.com/push/PushLogin"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Content-type": "application/x-www-form-urlencoded",
        "User-Agent": "HttpUrlConnection/Android"}

	params= None

	data= {       "appid": 1236618350,
        "cliType": 0,
        "deviceID": app_data.get('appUserId'),
        "deviceInfo": {       "android_sdk_ver": device_data.get('sdk'),
                              "app_ver": campaign_data.get('app_version_name'),
                              "brand": device_data.get('brand'),
                              "model": device_data.get('model'),
                              "sys_ver": device_data.get('os_version') },
        "hdid": app_data.get('appUserId'),
        "macAddr": urllib.quote(device_data.get('mac',"48:88:ca:68:08:1d")),
        "sdkVer": 3001008,
        "thirdtokenMask": "128",
        "tokenID": "",
        "verify": ""}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}


# def get_gcm_token( campaign_data, device_data, app_data ):

#         url     =       "http://android.clients.google.com/c2dm/register3"

#         method  =       "post"

#         headers =       {         'Accept-Encoding': 'gzip',
#           'Authorization': 'AidLogin 4038491838298919877:250389798860954024',
#           'User-Agent': 'Android-GCM/1.5 ('+device_data.get('product')+' '+device_data.get('build')+')',
#           'app': campaign_data.get('package_name'),
#           'content-type': 'application/x-www-form-urlencoded',
#           'gcm_ver': '17455012'}

#         params  =       None

#         data    =       {         
#         'X-app_ver': campaign_data.get('app_version_code'),
#           'X-app_ver_name': campaign_data.get('app_version_name'),
#           'X-appid': util.get_random_string('char_all',11),
#           'X-cliv': 'fiid-'+util.get_random_string('decimal',8),
#           'X-gmp_app_id': '1:167520552197:android:'+device_data.get('android_id'),
#           'X-gmsv': '17455012',
#           'X-osv': device_data.get('sdk'),
#           'X-scope': '*',
#           'X-subtype': '167520552197',
#           'app': campaign_data.get('package_name'),
#           'app_ver': campaign_data.get('app_version_code'),
#           'cert': util.get_random_string('hex',40),
#           'device': '4038491838298919877',
#           'gcm_ver': '17455012',
#           'info': util.get_random_string('all',20)+'_'+util.get_random_string('all',10),
#           'plat': '0',
#           'target_ver':27,
#           'sender': '167520552197'}

#         return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}

def app_measurement( campaign_data, device_data, app_data ):
	url= "http://app-measurement.com/config/app/1%3A167520552197%3Aandroid%3A929b0ccad4179fa2"
	method= "get"
	headers= {       "Accept-Encoding": "gzip",
        "User-Agent": get_ua(device_data),}

	params= {       "app_instance_id": "a4d8ee17b92d4faf99c7cdc54151212f",
        "gmp_version": "18382",
        "platform": "android"}

	data= None
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}

################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def event_1(campaign_data, app_data, device_data, eventValue):
	print 'Appsflyer : EVENT___________________________20023769'
	eventName			= '20023769'
	eventValue			= eventValue
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def loginsuccess(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________loginsuccess'
	eventName			= 'loginsuccess'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def Start_PK_Game(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Start_PK_Game'
	eventName			= 'Start_PK_Game'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def entergame(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________entergame'
	eventName			= 'entergame'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def CompleteGames(campaign_data, app_data, device_data):

	app_data['games_completed']+=1

	print 'Appsflyer : EVENT___________________________CompleteGames'
	eventName			= 'CompleteGames'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Click_Messages(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Click_Messages'
	eventName			= 'Click_Messages'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Click_AddFriends(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Click_AddFriends'
	eventName			= 'Click_AddFriends'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Click_Friends(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Click_Friends'
	eventName			= 'Click_Friends'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def send_message(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________send_message'
	eventName			= 'send_message'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def IM_Send_Message(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________IM_Send_Message'
	eventName			= 'IM_Send_Message'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Click_DiscoverPeople(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Click_DiscoverPeople'
	eventName			= 'Click_DiscoverPeople'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def add_friend(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________add_friend'
	eventName			= 'add_friend'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Click_PlayWithFriends(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Click_PlayWithFriends'
	eventName			= 'Click_PlayWithFriends'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Add_Friend_Request(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Add_Friend_Request'
	eventName			= 'Add_Friend_Request'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Start_Challenge_Game(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Start_Challenge_Game'
	eventName			= 'Start_Challenge_Game'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Visit_Other_Profile(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Visit_Other_Profile'
	eventName			= 'Visit_Other_Profile'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Visit_My_Profile(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Visit_My_Profile'
	eventName			= 'Visit_My_Profile'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def Click_Gathering_Game(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Click_Gathering_Game'
	eventName			= 'Click_Gathering_Game'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Start_Gathering_Game(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Start_Gathering_Game'
	eventName			= 'Start_Gathering_Game'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def ten_games(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ten_games'
	eventName			= 'ten_games'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Click_Challenge_Game(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Click_Challenge_Game'
	eventName			= 'Click_Challenge_Game'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def purchase_event(campaign_data, app_data, device_data, revenue):
	print 'Appsflyer : EVENT___________________________Purchase'
	eventName			= 'Purchase'
	eventValue			= json.dumps({"af_revenue":revenue,"af_currency":"USD"})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def Click_Gifts(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Click_Gifts'
	eventName			= 'Click_Gifts'
	eventValue			= '{}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def Complete_Send_Gifts(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________Complete_Send_Gifts'
	eventName			= 'Complete_Send_Gifts'
	eventValue			= json.dumps({"af_revenue":0.01})
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)



###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):

 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)

 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),
		}

	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),
		"isCachedRequest":"true",
		"timeincache":"8"
		}

	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_gcm_token" : app_data.get('gcm_token'),
		"af_preinstalled" : "false",
		"af_sdks" : "0000000000",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "no",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"sensors" : [{"sT":1,"sV":"STMicroelectronics","sVE":[-0.039230347,-0.07846069,10.002777],"sVS":[-0.15690613,-0.07846069,9.806656],"sN":"LIS3DH Accelerometer -Wakeup Secondary"},{"sT":2,"sV":"AKM","sVE":[28.5,47.16,-2.52],"sVS":[29.46,44.34,-2.3999999],"sN":"AK09911 Magnetometer"},{"sT":1,"sV":"STMicroelectronics","sVE":[-0.039230347,-0.07846069,10.002777],"sVS":[-0.15690613,-0.07846069,9.806656],"sN":"LIS3DH Accelerometer"},{"sT":4,"sV":"akm","sVE":[0,0,0],"sVS":[0,0,0],"sN":"AK09911-pseudo-gyro"}],
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"registeredUninstall" : True,
		"rfr" : {
								"clk" : str(int(app_data.get("times").get("click_time"))),
								"code" : "0",
								"install" : str(int(app_data.get("times").get("download_begin_time"))),
								"val": app_data.get("referrer") or "utm_source=(not%20set)&utm_medium=(not%20set)",
		},
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),
		"tokenRefreshConfigured":False
		}			

	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	if app_data.get('appUserId'):
		data["appUserId"] = app_data.get('appUserId')

	if app_data.get('counter')>=2:
		if app_data.get('counter')>2:
			del data['deviceData']['sensors']
			del data['rfr']
		data['reinstallCounter'] = "1"
		del params['isCachedRequest']
		del params['timeincache']

	if app_data.get("counter")==2:
		data['originalAppsflyerId'] = app_data.get('uid')
	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):

	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),
		}
	params = {
		"device_id" : app_data.get('uid'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		}
	data = {}
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_register()	: method
# parameter 			: campaign_data, app_data, device_data,
#
# Simulates Appsflyer's behaviour incase of new device registration.
###################################################################
def appsflyer_register(campaign_data,app_data,device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	def_(app_data,'counter')
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('gcm_token'),
		"app_name" : campaign_data.get('app_name'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand').upper(),
		"carrier" : device_data.get('carrier'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),
		'appUserId': app_data.get('appUserId')
		}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)

	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),
		}

	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),
		}

	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_gcm_token" : app_data.get('gcm_token'),
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_reactNative",
		"product" : device_data.get('product'),
		"registeredUninstall" : True,
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),
		"originalAppsflyerId" : app_data.get('uid'),
		"reinstallCounter" : '1',
		"tokenRefreshConfigured":"False"
		}	

	if app_data.get('appUserId'):
		data["appUserId"] = app_data.get('appUserId')

	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
			
	update_eventsRecords(app_data,eventName,eventValue)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


def android_clients_google( campaign_data, device_data, app_data ):
	url= "http://android.clients.google.com/c2dm/register3"
	method= "post"
	headers= {       "Accept-Encoding": "gzip",
        "Authorization": "AidLogin 4222865371253775297:8918866590732316351",
        "User-Agent": "Android-GCM/1.5 ("+device_data.get('model')+" "+device_data.get('build')+")",
        "app": campaign_data.get('package_name'),
        # "content-length": "361",
        "content-type": "application/x-www-form-urlencoded",
        "gcm_ver": "19056016"}

	params= None

	data= {       "X-app_ver": campaign_data.get('app_version_code'),
        "X-app_ver_name": campaign_data.get('app_version_name'),
        "X-appid": util.get_random_string('char_all',11),
        "X-cliv": "fiid-"+util.get_random_string('decimal',8),
        "X-gmp_app_id": "1:167520552197:android:"+util.get_random_string('hex',16),
        "X-gmsv": "19056016",
        "X-osv": device_data.get('sdk'),
        "X-scope": "*",
        "X-subtype": "167520552197",
        "app": campaign_data.get('package_name'),
        "app_ver": campaign_data.get('app_version_code'),
        "cert": util.get_random_string('hex',40),
        "device": "4222865371253775297",
        "X-Firebase-Client"	:"fire-android/ fire-iid/18.0.0 fire-core/17.0.0",
        "gcm_ver": "19056016",
        'info': util.get_random_string('all',20)+'_'+util.get_random_string('all',10),
        "plat": "0",
        "sender": "167520552197",
        "target_ver": "28"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": data}


###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################
def change_char(s, p, r):
	return s[:p]+r+s[p+1:]
		
def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100

	if n3<10:
		n3 = '0'+str(n3)
		
	sb = insert_char(sb,23,str(n3))
	return sb



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

# def get_afGoogleInstanceID():
# 	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

# def catch(response,app_data,paramName):
# 	try:
# 		jsonData = json.loads(response.get('data'))
# 		if paramName=="appsflyer":
# 			app_data['installAttribution'] = jsonData
# 	except:
# 		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

# def ref_time():
# 	time.sleep(random.randint(10,15))
# 	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"