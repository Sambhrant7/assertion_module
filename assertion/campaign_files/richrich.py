# -*- coding: utf-8 -*-
from sdk import installtimenew
from sdk import getsleep
from sdk import util, purchase
from sdk import NameLists
import time
import random
import json
import string
import datetime
import clicker
import Config


#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
	'package_name'		 :'com.ulugame.goldenkr.google',
	'app_size'			 : 48.0,#42.0,
	'app_name' 			 :'리치리치:부자되는 법',
	'app_version_name' 	 :'3.13',#'3.12',#'3.10',#'3.9',
	'app_version_code' 	 :'13',#'12',#'10',#'9',
	'CREATE_DEVICE_MODE' : 3,
	'ctr' 				 : 6,
	'no_referrer' 		 : False,
	'device_targeting'	 : True,
	'supported_countries': 'WW',
	'supported_os'		 : '5.1',
	'tracker'		 	 : 'Appsflyer',
	'appsflyer'		 	 : {
		'key'		 : 'ernx85LSi7VY3pH44rAiDW',
		'dkh'		 : 'ernx85LS',
		'buildnumber': '4.10.2',
		'version'	 : 'v4',
	},
	'purchase_var' 		: {
		'1' : { '$1.08' },	
		'2'	: { '$5.43' },
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'retention' :{			
				1:70,
				2:68,
				3:66,
				4:64,
				5:61,
				6:59,
				7:57,
				8:52,
				9:50,
				10:47,
				11:45,
				12:43,
				13:40,
				14:37,
				15:35,
				16:31,
				17:30,
				18:28,
				19:26,
				20:20,
				21:19,
				22:18,
				23:17,
				24:16,
				25:15,
				26:14,
				27:13,
				28:12,
				29:11,
				30:10,
				31:9,
				32:8,
				33:7,
				34:6,
				35:5,
			},
		}

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	

	###########		INITIALIZE		############	

	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	def_sec(app_data,device_data)
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)
	def_eventsRecords(app_data)

	################generating_realtime_differences##################

	def_firstLaunchDate(app_data,device_data)

 	###########	 CALLS		 ############
	
	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
	app_data['api_hit_time']=time.time()
	util.execute_request(**request)

	time.sleep(random.randint(2,3))
		
	print '\n'+'Appsflyer : API____________________________________'
	request = appsflyer_api(campaign_data, app_data, device_data)
	response=util.execute_request(**request)
	catch(response,app_data,"appsflyer")
	time.sleep(random.randint(5,10))

	print '\n'+'Appsflyer : attr____________________________________'
	request = appsflyer_attr(campaign_data, app_data, device_data)
	util.execute_request(**request)	
	time.sleep(random.randint(3,7))	

	print '\n'+'Appsflyer : Register____________________________________'
	request = appsflyer_register(campaign_data,app_data,device_data)
	util.execute_request(**request)


	if random.randint(1,100) <= 95:

		time.sleep(random.randint(60,120))
		open_complete(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,3))
		firstlogin_complete(campaign_data, app_data, device_data)
		app_data['firstlogin_complete'] = True


		if random.randint(1,100) <= 95:

			time.sleep(random.randint(30,60))
			require_permission(campaign_data, app_data, device_data)

			time.sleep(random.randint(10,15))
			agree_agreement(campaign_data, app_data, device_data)
			
			time.sleep(random.randint(5,10))
			enter_game(campaign_data, app_data, device_data)
			
			time.sleep(random.randint(5,10))
			bus_animation(campaign_data, app_data, device_data)
			
			time.sleep(random.randint(5,10))
			business_center(campaign_data, app_data, device_data)
			
			time.sleep(random.randint(5,10))
			question_complete(campaign_data, app_data, device_data)
			
			time.sleep(random.randint(10,15))
			name_gender(campaign_data, app_data, device_data)
			
			time.sleep(random.randint(30,60))
			currently_preparing(campaign_data, app_data, device_data)

			time.sleep(random.randint(10,15))
			create_character(campaign_data, app_data, device_data)
			
			time.sleep(random.randint(5,10))
			login_complete(campaign_data, app_data, device_data)

			time.sleep(random.randint(45,60))
			enterinstance(campaign_data, app_data, device_data)
			app_data['create_character'] = True 

			if random.randint(1,100) <= 95:

				call_pattern(campaign_data, app_data, device_data, call_type = 'install')


	return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):
	
	###########		INITIALIZE		############	

	print "Please wait ..."
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	def_sec(app_data,device_data)
	batteryChargingStatus(app_data)
	def_appsflyerUID(app_data)

	if not app_data.get('prev_event_name'):
		def_eventsRecords(app_data)

	################generating_realtime_differences##################

	def_firstLaunchDate(app_data,device_data)

 	###########	 CALLS		 ############

 	print '\n'+'Appsflyer : Track____________________________________'
	request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=True)
	util.execute_request(**request)


	if not app_data.get('firstlogin_complete'):

		time.sleep(random.randint(60,120))
		open_complete(campaign_data, app_data, device_data)

		time.sleep(random.randint(1,3))
		firstlogin_complete(campaign_data, app_data, device_data)
		app_data['firstlogin_complete'] = True


	if not app_data.get('create_character'):

		time.sleep(random.randint(30,60))
		require_permission(campaign_data, app_data, device_data)

		time.sleep(random.randint(10,15))
		agree_agreement(campaign_data, app_data, device_data)
		
		time.sleep(random.randint(5,10))
		enter_game(campaign_data, app_data, device_data)
		
		time.sleep(random.randint(5,10))
		bus_animation(campaign_data, app_data, device_data)
		
		time.sleep(random.randint(5,10))
		business_center(campaign_data, app_data, device_data)
		
		time.sleep(random.randint(5,10))
		question_complete(campaign_data, app_data, device_data)
		
		time.sleep(random.randint(10,15))
		name_gender(campaign_data, app_data, device_data)
		
		time.sleep(random.randint(30,60))
		currently_preparing(campaign_data, app_data, device_data)

		time.sleep(random.randint(10,15))
		create_character(campaign_data, app_data, device_data)
		
		time.sleep(random.randint(5,10))
		login_complete(campaign_data, app_data, device_data)

		time.sleep(random.randint(45,60))
		enterinstance(campaign_data, app_data, device_data)
		app_data['create_character'] = True 

	else:

		time.sleep(random.randint(5,10))
		enter_game(campaign_data, app_data, device_data)

		time.sleep(random.randint(5,10))
		login_complete(campaign_data, app_data, device_data)


	if random.randint(1,100) <= 90:
		call_pattern(campaign_data, app_data, device_data, call_type = 'open')


	if  purchase.isPurchase(app_data,day,advertiser_demand=10):

		random_number = random.randint(1,100)

		if random_number <= 90:
			purchase_item = 1
		else:
			purchase_item = 2

		app_data['custom_purchase_flag'] = True

		if purchase_item == 1:

			time.sleep(random.randint(30,60))
			first_charge(campaign_data, app_data, device_data)
			vip_purhchase(campaign_data, app_data, device_data, '1')
			recharge_purchase(campaign_data, app_data, device_data, '3')
			vip_purhchase(campaign_data, app_data, device_data, '1')
			purchase_proof(campaign_data, app_data, device_data, '1200')

		else:

			time.sleep(random.randint(30,60))
			first_charge(campaign_data, app_data, device_data)
			vip_purhchase(campaign_data, app_data, device_data, '2')
			recharge_purchase(campaign_data, app_data, device_data, '4')
			purchase_proof(campaign_data, app_data, device_data, '5900')


		if app_data.get('custom_purchase_flag') and random.randint(1,100) <= 70:

			if not app_data.get('vip_package_1') and app_data.get('vip_entered'):

				time.sleep(random.randint(30,60))
				vip_package(campaign_data, app_data, device_data, '1')

				app_data['vip_package_1'] = True


	return {'status':'true'}



def call_pattern(campaign_data, app_data, device_data, call_type = 'install'):

	if not app_data.get('level'):
		app_data['level'] = 1

	if call_type == 'install':
		play_upto = random.choice([10,20,30,30,30,30,30,30,30,30])
	else:
		play_upto = random.choice([10,10,10,20,20,20,20,20,30,30])

	for _ in range(play_upto):

		if app_data.get('level') <= 28:

			if app_data.get('level') not in [12,13]:

				print '\nPlaying : Novice : ' + str(app_data.get('level')) + '\n'

				if app_data.get('level') >= 1 and app_data.get('level') <= 28:
					time.sleep(random.randint(15,40))

				if app_data.get('level') in [6,18,21,27,28]:
					time.sleep(random.randint(40,90))

				novice(campaign_data, app_data, device_data, level = app_data.get('level'))

		if app_data.get('level') == 9:
			time.sleep(random.randint(5,10))
			level(campaign_data, app_data, device_data, level = '2')
			time.sleep(random.randint(10,15))
			talent(campaign_data, app_data, device_data, talent_number = '1045')

		if app_data.get('level') == 17:
			time.sleep(random.randint(5,10))
			level(campaign_data, app_data, device_data, level = '3')
			time.sleep(random.randint(30,45))
			talent(campaign_data, app_data, device_data, talent_number = '1046')

		if app_data.get('level') == 20:
			time.sleep(random.randint(5,10))
			level(campaign_data, app_data, device_data, level = '4')

		if app_data.get('level') == 27:
			time.sleep(random.randint(10,15))
			level(campaign_data, app_data, device_data, level = '5')

		if random.randint(1,100) <= 5:
			time.sleep(random.randint(5,10))
			print '\n'+'Appsflyer : Track____________________________________'
			request  = appsflyer_track(campaign_data, app_data, device_data,isFirstCall="true", isOpen=False)
			util.execute_request(**request)

		if random.randint(1,100) <= 3:
			# shop event
			time.sleep(random.randint(2,5))
			visit_shop(campaign_data, app_data, device_data)

		if random.randint(1,100) <= 10:
			if not app_data.get('vip_entered'):
				# vip package event
				time.sleep(random.randint(2,4))
				vip_package(campaign_data, app_data, device_data, '0')
				app_data['vip_entered'] = True

		app_data['level'] += 1


	if app_data.get('level') > 28:
		sub_call_pattern(campaign_data, app_data, device_data, call_type)


def sub_call_pattern(campaign_data, app_data, device_data, is_type = 'open'):

	if not app_data.get('sub_level'):
		app_data['sub_level'] = 6

	if is_type == 'install':
		play_upto = random.choice([1,2,3,3,3,4,4,4,4,4])
	else:
		play_upto = random.choice([1,2,2,3,3,4,4,4,4,4])

	for _ in range(play_upto):

		if app_data.get('sub_level') <= 11:

			time.sleep(random.randint(60,90))

			if app_data.get('sub_level') == 7:
				time.sleep(random.randint(2*60, 3*60))

			if app_data.get('sub_level') == 8:
				time.sleep(random.randint(3*60, 4*60))

			print '\nPlaying : Level : ' + str(app_data.get('sub_level')) + '\n'

			level(campaign_data, app_data, device_data, level = str(app_data.get('sub_level')))

		if app_data.get('sub_level') == 6:
			time.sleep(random.randint(60,90))
			talent(campaign_data, app_data, device_data, talent_number = '1041')

		if app_data.get('sub_level') == 8:
			time.sleep(random.randint(60,90))
			talent(campaign_data, app_data, device_data, talent_number = '1042')

		if app_data.get('sub_level') == 11:

			time.sleep(random.randint(60,90))
			talent(campaign_data, app_data, device_data, talent_number = '1043')

			time.sleep(random.randint(60,90))
			talent(campaign_data, app_data, device_data, talent_number = '1020')

		app_data['sub_level'] += 1


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def open_complete(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________open_complete'
	eventName			= 'open_complete'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def level(campaign_data, app_data, device_data, level = ''):
	print 'Appsflyer : EVENT___________________________level_' + str(level)
	eventName			= 'level_' + str(level)
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def firstlogin_complete(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________first login_complete'
	eventName			= 'first login_complete'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def create_character(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________create_character'
	eventName			= 'create_character'
	eventValue			= '{"value":"56'+ str(util.get_random_string('decimal',13)) +'"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def login_complete(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________login_complete'
	eventName			= 'login_complete'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def enterinstance(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________enterinstance'
	eventName			= 'enterinstance'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


def novice(campaign_data, app_data, device_data, level = 1):
	print 'Appsflyer : EVENT___________________________novice_' + str(level)
	eventName			= 'novice_' + str(level)
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def visit_shop(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________visit_shop'
	eventName			= 'visit_shop'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def vip_package(campaign_data, app_data, device_data, package_number):
	print 'Appsflyer : EVENT___________________________vip_package' + str(package_number)
	eventName			= 'vip_package' + str(package_number)
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def talent(campaign_data, app_data, device_data, talent_number):
	print 'Appsflyer : EVENT___________________________talent_' + str(talent_number)
	eventName			= 'talent_' + str(talent_number)
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def vip_purhchase(campaign_data, app_data, device_data, vip_number):
	print 'Appsflyer : EVENT___________________________vip_' + str(vip_number) + ' purchase'
	eventName			= 'vip_' + str(vip_number)
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def recharge_purchase(campaign_data, app_data, device_data, recharge_number):
	print 'Appsflyer : EVENT___________________________recharge_' + str(recharge_number) + ' purchase'
	eventName			= 'recharge_' + str(recharge_number)
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def first_charge(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________first_charge'
	eventName			= 'first_charge'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def purchase_proof(campaign_data, app_data, device_data, revenue):
	print 'Appsflyer : EVENT___________________________purchase_proof'
	eventName			= 'purchase_proof'
	eventValue			= '{"value":"'+ util.get_random_string('decimal', 17) +'","af_currency":"KRW","af_revenue":"'+ revenue +'"}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def require_permission(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________require_permission'
	eventName			= 'require_permission'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def agree_agreement(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ agree_agreement'
	eventName			= 'agree_agreement'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def enter_game(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ enter_game'
	eventName			= 'enter_game'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def bus_animation(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ bus_animation'
	eventName			= 'bus_animation'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def business_center(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ business_center'
	eventName			= 'business_center'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def question_complete(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ question_complete'
	eventName			= 'question_complete'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def name_gender(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ name_gender'
	eventName			= 'name_gender'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)

def currently_preparing(campaign_data, app_data, device_data):
	print 'Appsflyer : EVENT___________________________ currently_preparing'
	eventName			= 'currently_preparing'
	eventValue			= '{"value":0}'
	request	= appsflyer_event(campaign_data, app_data, device_data,eventName=eventName,eventValue=eventValue)
	util.execute_request(**request)


###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def appsflyer_track(campaign_data, app_data, device_data,timeSinceLastCall=0,isFirstCall="false",isOpen=True):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
 	app_data['time_in_app']=int(time.time())
 	if app_data.get('timepassedsincelastlaunch'):
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
 		app_data['timepassedsincelastlaunch']=int(time.time())
 	else:
 		app_data['timepassedsincelastlaunch']=int(time.time())
 		timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')

	method = "post"
	url = 'http://t.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time') + app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time') + app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "no",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"sensors" : [{u'sN': u'MAGNETOMETER', u'sVE': [7.35, 15.165, -75.602], u'sV': u'MTK', u'sVS': [6.3, 15.502, -78.229], u'sT': 2}, {u'sN': u'ACCELEROMETER', u'sVE': [-0.018, -0.168, 9.665], u'sV': u'MTK', u'sVS': [-0.474, -0.471, 9.753], u'sT': 1}, {u'sN': u'GYROSCOPE', u'sVE': [0, 0, -0.12280702], u'sV': u'MTK', u'sVS': [0, 0, 0], u'sT': 4}],
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : isFirstCall,
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"open_referrer" : 'android-app://com.android.vending',
		"operator" : device_data.get('carrier'),
		"p_receipt" : util.get_random_string('google_token',637).replace('-','+').replace('_','/')+'=',
		"platformextension" : "android_unity",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"sc_o" : "p",
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : str(timeSinceLastCall),
		"uid" : app_data.get('uid'),

		}			
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'

	if data.get('country') == 'KR':
		data['lang'] = '한국어'
	
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('af_gcm_token'):
		data['af_gcm_token']=app_data.get('af_gcm_token')

	if app_data.get('counter')>2:
		if data.get('rfr'):
			del data['rfr']
		if data.get('deviceData').get('sensors'):
			del data['deviceData']['sensors']
		if data.get("p_receipt"):
			del data["p_receipt"]

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))


	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	if isFirstCall=="false":
		if data.get('af_sdks'):
			del data['af_sdks']
		if data.get('batteryLevel'):
			del data['batteryLevel']
		if data.get('open_referrer'):
			data['open_referrer']="android-app://"+campaign_data.get("package_name")
			
	else:
		data['batteryLevel']=get_batteryLevel(app_data)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(8,15))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))	
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


###################################################################
# appsflyer_api()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# To acknowledge and check the nature of install.
###################################################################
def appsflyer_api(campaign_data, app_data, device_data):
	def_appsflyerUID(app_data)
	method = "get"
	url = 'http://api.appsflyer.com/install_data/v3/'+campaign_data.get('package_name')
	headers = {
		"Accept-Encoding" : "gzip",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"device_id" : app_data.get('uid'),
		"devkey" : campaign_data.get('appsflyer').get('key'),

		}
	data = {

		}

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


###################################################################
# appsflyer_event()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, eventName, eventValue
#
# Simulates Appsflyer's behaviour incase of an in-app event.
###################################################################

def appsflyer_event(campaign_data, app_data, device_data,timeSinceLastCall=0,eventName="",eventValue=""):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
 	def_(app_data,'counter')
 	inc_(app_data,'iaecounter')
 	get_deviceData(app_data, device_data)
	method = "post"
	url = 'http://events.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time') + app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time') + app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_unity",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"sc_o" : "p",
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)

	if data.get('country') == 'KR':
		data['lang'] = '한국어'

	if app_data.get('af_gcm_token'):
		data['af_gcm_token']=app_data.get('af_gcm_token')
		
	data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')

	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))

	data["eventValue"] 	= eventValue
	data["eventName"] 	= eventName

	if app_data.get('installAttribution'):
		data["installAttribution"] = str(json.dumps(app_data.get('installAttribution')))
	

	if app_data.get('user_id'):
		data["appUserId"] = app_data.get('user_id')

	if app_data.get('iaecounter')>1:
		if  app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({
									"prev_event_timestamp"	:app_data.get('prev_event_time'),
									"prev_event_value"		:app_data.get('prev_event_value'),
									"prev_event_name"		:app_data.get('prev_event_name')
									})
		if data['isFirstCall']=='true':
			data['isFirstCall']='false'
			
	update_eventsRecords(app_data,eventName,eventValue)
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(8,15))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def appsflyer_attr(campaign_data, app_data, device_data):
	def_firstLaunchDate(app_data,device_data)
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
 	inc_(app_data,'counter')
 	def_(app_data,'iaecounter')
 	get_google_gcmToken(app_data)
 	get_deviceData(app_data, device_data)
 	method = "post"
	url = 'http://attr.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "no",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
								},
						"sensors" : [{u'sN': u'MAGNETOMETER', u'sVE': [114.24, -52.8, 157.56], u'sV': u'MTK', u'sVS': [72, -90.42, 128.58], u'sT': 2}, {u'sN': u'ACCELEROMETER', u'sVE': [-0.086, 0, 9.864], u'sV': u'MTK', u'sVS': [-0.086, -0.009, 9.797], u'sT': 1}],
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"gcd_timing" : "6049",
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "false",
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name('en'),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : "WIFI",
		"open_referrer" : 'android-app://com.android.vending',
		"operator" : device_data.get('carrier'),
		"p_receipt" : "fHD0Ft4nHANhoZHzNPBI23mNc52pHe5fUunI9f025ABVQU62Vpoldes7K0TaHbEQcj5MZZpXE4W+tb/2FKkekA45Gefft97yuD40ltWOSRk3EAK0Es0awoGhjHZ7UN38kqGn8mF082oeAC4opSR2z7a1AnGvyYUpJel7zYFR34HXVlQOvev4+0BXezFJNdiKA/JsoxJUScPPjW/1h4OH6eeoUMpMr02RT8GxTlY9pbR3a65k9DRtoso9kJV2qvakexPgCmgqva1tKwJdOjeURfuVhXW4l9ZtCYe7tzwzSk5zszoDKDoWRcbhfHX6JO+XfQF+3+YZsQjitTc/CwL1sp4rsvPrRJqD8C55/Fmczl20ys1dxXQ1N85Fv64Vx2fEGCsKw/x/1FMWuTaRf0wXB9zAW3TYjtIpDlP25swo2vUtCxIRl0QObA3dJxZaseTvXfr7wSAVNlyKNi2mxTOlGClruN4KmYhP528/REdiNyw=",
		"platformextension" : "android_unity",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"sc_o" : "p",
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : 0,
		"uid" : app_data.get('uid'),

		}
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	else:
		data['referrer'] = 'utm_source=(not%20set)&utm_medium=(not%20set)'

	if app_data.get('timepassedsincelastlaunch'):
		data['timepassedsincelastlaunch'] = str(int(time.time()) - app_data.get('timepassedsincelastlaunch'))
		app_data['timepassedsincelastlaunch'] = int(time.time())
	else:
		data['timepassedsincelastlaunch'] = '0'
		app_data['timepassedsincelastlaunch'] = int(time.time())

	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("buildnumber"))	
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}


def appsflyer_register(campaign_data,app_data,device_data):
	def_deviceFingerPrintId(app_data)
	def_appsflyerUID(app_data)
	def_afGoogleInstanceID(app_data)
	get_google_gcmToken(app_data)
 	def_(app_data,'counter')
	method = "post"
	url = 'http://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : get_ua(device_data),

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('buildnumber'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"app_name" : campaign_data.get('app_name'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand').upper(),
		"carrier" : device_data.get('carrier'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),

		}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}



###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################
def change_char(s, p, r):
	return s[:p]+r+s[p+1:]
		
def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100

	if n3<10:
		n3 = '0'+str(n3)
		
	sb = insert_char(sb,23,str(n3))
	return sb



###################################################################
# appsflyer_kef_key and value generator
# parameter 			: brand, sdk, lang, af_ts, buildnumber, etc
#
# Generates kef key and value for appsflyer.
###################################################################
global temp, counter
temp = 0
counter = 1

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
	if buildnumber=="4.8.18":
		return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
	else:
		c = '\u0000'
		obj2 = af_ts[::-1]
		out = calculate(sdk, obj2, brand)
		i = len(out)
		if i > 4:
			i2 = globals()['counter']+65
			globals()['temp'] = i2 % 128
			if (1 if i2 % 2 != 0 else None) != 1:
				out.delete(4, i)
			else:
				out.delete(5, i)
		else:
			while i < 4:
				i3 = globals()['counter'] + 119
				globals()['temp'] = i3 % 128
				if (16 if i3 % 2 != 0 else 93) != 16:
					i += 1
					out+='1'
				else:
					i += 66
					out+='H'
				i3 = globals()['counter'] + 109
				globals()['temp'] = i3 % 128
				i3 %= 2
		return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
	ts = ts[::-1]
	appends = [sdk,lang,ts]
	lengths = [len(sdk),len(lang),len(ts)]
	l = sorted(lengths)
	least = l[0]
	out = ""
	for x in range(least):
		numb = None
		for y in range(len(appends)):
			charAt = ord(appends[y][x])
			if numb:
				charAt = int((charAt ^ int(numb)))

			numb = charAt
			
		out+=str(hex(numb))[2:]

	if len(out)>4:
		out = out[:4]
	elif len(out)<4:
		while len(out)<4:
			out+="1"
	
	return out

"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""

def generateValue(b,x,s,p,ts,fl,buildnumber):
	part1=generateValue1get(ts,fl,buildnumber)
	str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, "utf-8")
	for i in range(len(str_)):
		str_[i] = int((str_[i] ^ ((i % 2) + 42)))

	stringBuilder = ""
	for toHexString in str_:
		toHexString2 = str(hex(int(toHexString)))[2:]
		if 1 == len(toHexString2):
			toHexString2 = "0"+str(toHexString2)
		stringBuilder+=toHexString2
	part2 = stringBuilder.__str__()
	return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
	gethash = sha256(ts+firstLaunch+buildnumber)
	return gethash[:16]

"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""

def calculate(sdk, obj, brand):
	allList = [sdk, obj, brand]
	arrayList = []
	i = 0
	while True:
		flag = 1
		if i >= 3:
			break
		i2 = globals()['temp'] + 63
		globals()['counter'] = i2 % 128
		if i2 % 2 != 0:
			flag = None
		if flag != None:
			arrayList.append(len(allList[i]))
			i += 31
		else:
			arrayList.append(len(allList[i]))
			i += 1
	sorted(arrayList)
	intValue = int(arrayList[0])
	out = ""
	i3 = 0
	while i3 < intValue:
		i4 = globals()['counter']+99
		globals()['temp'] = i4 % 128
		i5 =  globals()['temp']+67
		globals()['counter'] = i5 % 128
		i5 %= 2
		number = None
		if i4 % 2 != 0:
			a = 57
		else:
			a = 83
		if a != 83:
			i4 = 1
		else:
			i4 = 0
		while i4 < 3:
			i5 = globals()['temp'] + 87
			globals()['counter'] = i5 % 128
			i5 %= 2
			i5 = ord(allList[i4][i3])
			if (92 if number == None else 39) != 39:
				i6 = globals()['temp'] + 55
				globals()['counter'] = i6 % 128
				i6 %= 2
				i6 = globals()['counter'] + 39
				globals()['temp'] = i6 % 128
				i6 %= 2
			else:
				i5 ^= int(number)
			number = i5
			i4 += 1
		out+=str(hex(int(number)))[2:]
		i3 += 1
	return out



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################

def batteryChargingStatus(app_data):
	app_data['btch'] = random.choice(["no","ac","usb"])

def def_afGoogleInstanceID(app_data):
	if not app_data.get('afGoogleInstanceID'):
		app_data['afGoogleInstanceID'] = util.get_random_string('char_all',11)

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))
	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : APPSFLYER
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Appsflyer 
###########################################################	

def def_firstLaunchDate(app_data,device_data):
	def_sec(app_data,device_data)
	if not app_data.get('firstLaunchDate'):
		date = int(time.time())
		app_data['firstLaunchDate'] =  datetime.datetime.utcfromtimestamp(date).strftime("%Y-%m-%d_%H%M%S")+"+0000"

def def_appsflyerUID(app_data):
	if not app_data.get('uid'):
		app_data['uid'] = timestamp()+'-'+str(random.getrandbits(64))

def def_eventsRecords(app_data):
	app_data['prev_event_name']  = ""
	app_data['prev_event_value'] = ""
	app_data['prev_event_time']  = str(int(time.time()*1000))

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name']  = eventName
	app_data['prev_event_value'] = eventValue
	app_data['prev_event_time']  = str(int(time.time()*1000))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"

def ref_time():
	time.sleep(random.randint(10,15))
	return str(int((time.time())*1000))

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()
#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1
	
# def register_user(app_data,country='united states'):
# 	if not app_data.get('user'):
# 		gender 		= random.choice(['male','female'])
# 		first_names = NameLists.NAMES[country][gender]
# 		first_name 	= random.choice(first_names)
# 		last_names 	= NameLists.NAMES[country]['lastnames']
# 		last_name 	= random.choice(last_names)
# 		number 		= str(random.randint(100, 9000))
# 		symbol 		= random.choice(['', '.', '_'])
# 		usernamelst = [first_name, last_name, number, symbol]
# 		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
# 			random.shuffle(usernamelst)
# 		username 	= ''.join(usernamelst)
# 		app_data['user']				={}
# 		app_data['user']['sex'] 		= gender
# 		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
# 		app_data['user']['firstname'] 	= first_name
# 		app_data['user']['lastname'] 	= last_name
# 		app_data['user']['username'] 	= username
# 		app_data['user']['email'] 		= username+"@gmail.com"