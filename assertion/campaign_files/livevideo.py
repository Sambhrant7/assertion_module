from sdk import util,purchase,installtimenew
from sdk import getsleep
import uuid, json, datetime, time, random, string,urllib
import clicker, Config

campaign_data = {
	'app_size'	 : 93.0,#92.0,#79.0,81.0, 91.0
	'package_name':'com.machipopo.media17',
	'ctr'         :6,
	'app_name':'LiveAF',
	'app_version_code': '2420',#'2418',#'2416',#2415, '2414',#'2413',#2383, '2379',#'2356',#'2351',#'2341',#'2339',#'2338',#'2332',#2295, 2328, 2330, 2331, 2335, 2336
	'app_version_name': '2.4.20.0',#'2.4.18.0',#'2.4.16.0',#2.4.15.0, '2.4.14.0',#'2.4.13.0',#2.3.83.0, '2.3.79.0',#'2.3.56.0',#'2.3.51.0',#'2.3.41.0',#'2.3.39.0',#'2.3.38.0',#'2.3.32.0', #2.2.95.0, 2.3.28.0, 2.3.30.0, 2.3.31.0, 2.3.35.0, 2.3.36.0
	'no_referrer': True,
	'tracker':'appsflyer',
	'supported_countries':'WW',
	'device_targeting':True,
	'CREATE_DEVICE_MODE':3,
	'supported_os':'4.4',
	'appsflyer':{
		'key':'4ih4DHjzd3CJVaDwLj4chX',
		'dkh':'4ih4DHjz',
		'build_number': '4.10.0',#'4.8.15',#4.8.12, 4.8.17
		'version':'v4'
	},
	'purchase':{
		'1':{
				'af_content_id':'media17_points_130',
				'af_content_type':'pointproduct',
				'af_revenue':'0.99'
			},
		'2':{
				'af_content_id':'media17_points_670',
				'af_content_type':'pointproduct',
				'af_revenue':'4.99'
			},
		'3':{
				'af_content_id':'media17_points_1380',
				'af_content_type':'pointproduct',
				'af_revenue':'9.99'
			},								
	},
	'country':[('USA',50),('India',3),('Malaysia',4),('Indonesia',1),('Thailand',2),('Egypt',1),('Russia',6),('USA',10),('SaudiArabia',1),('SouthAfrica',1),('Israel',2),('Kenya',1),('Nigeria',1),('Pakistan',2),('Qatar',1),('Brazil',3),('Mexico',4),('Canada',5),("UK",30),("HongKong",5),("Spain",4),("France",4),("Australia",5)],
	'retention':{
							1:70,
							2:68,
							3:66,
							4:64,
							5:61,
							6:59,
							7:57,
							8:52,
							9:50,
							10:47,
							11:45,
							12:43,
							13:40,
							14:37,
							15:35,
							16:31,
							17:30,
							18:28,
							19:26,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,

	}		

}

def timeSinceLastLaunch_return(app_data):
	
	if not app_data.get('timepassedsincelastlaunch'):
		app_data['timepassedsincelastlaunch']=str(0)
		app_data['open_time']=int(time.time())
	else:
		app_data['timepassedsincelastlaunch']=str(int(time.time())-app_data.get('open_time'))
		app_data['open_time']=int(time.time())

def make_battery_level(app_data):
	if not app_data.get('batteryLevel'):
		app_data['batteryLevel'] = str(random.randint(5,100))+'.0'
	return app_data.get('batteryLevel')

def get_game_card(campaign_data,app_data,device_data):
   
	params = {
		'g_id': campaign_data.get('game_id'),
		'm_id': app_data.get('m_id'),
		'p_name':'android',
		'limitcheck':'1',	# added by jitendra
		'version':'2'
	}
	
	return {'url': 'http://pay-fb.igg.com/api/get_game_card.php', 'httpmethod': 'get', 'headers': {'User-Agent':'Apache-HttpClient/UNAVAILABLE (java 1.4)'}, 'params': params, 'data': None}
	
def igg(campaign_data,app_data,device_data,param_1):
 
	data = {
		'b_key':param_1,
		's_type':'guest'
	}
	return {'url':'http://cgi.igg.com:9000/public/GetMIDBy3rdId', 'httpmethod':'post', 'headers':{'User-Agent': 'Apache-HttpClient/UNAVAILABLE (java 1.4)'}, 'params':None , 'data':data} 
	
def generate_gcm_id(app_data,device_data):
	app_data['gcm'] = 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))		
	return app_data.get('gcm')

def guest_user_login_igg(campaign_data,app_data,device_data):
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Charset': 'UTF-8',
		'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('brand')+' '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
		'Accept-Encoding': 'gzip'
	}
	params={
		'm_guest':'gaid='+device_data.get('adid'),
		'm_key':str(int(time.time()*1000)),
		'm_device_type':'',
		'keep_time':2592000,
		'm_game_id':campaign_data.get('game_id'),
	}
	
	m_data=device_data.get('adid')+"f6239975b2faae941ec24695e4db5bba"+params.get('m_key')
	params['m_data']=util.md5(m_data)
	
	return{'url':'http://cgi.igg.com:9000/public/guest_user_login_igg','httpmethod':'post','headers':headers,'params':params,'data':None}

def server_config_igg(campaign_data,app_data,device_data):

	return {'url':'http://config.igg.com/appconf/'+campaign_data.get('game_id')+'/server_config', 'httpmethod':'get', 'headers':{'User-Agent': 'Apache-HttpClient/UNAVAILABLE (java 1.4)'}, 'params':None , 'data':None}   

def register(campaign_data,app_data,device_data):
 
	data = {
		'd_adid':device_data.get('adid'),
		'd_android_id':device_data.get('android_id'),
		'd_country':device_data.get('locale').get('country').upper(),
		'd_imei':device_data.get('imei'),
		'd_language':device_data.get('locale').get('language').upper(),
		'd_manufacturer':device_data.get('manufacturer'),
		'd_model':device_data.get('model'),
		'd_operator_code':device_data.get('mcc')+device_data.get('mnc')[1:],	
		'd_operator_name':device_data.get('carrier'),   
		'd_os_version':device_data.get('os_version'),
		'd_regid':app_data.get('gcm'),
		'd_serial_id':device_data.get('serial'),
		'd_type':'3',
		'd_wifi_address':device_data.get('mac'),
		'g_id':campaign_data.get('game_id'),
		'iggid':app_data.get('m_id'),
		'nickname':app_data.get('m_id'),
		'tz_timezone':float(int(device_data.get('timezone'))/100),
	}
	
	return {'url': 'http://push.igg.com/api/register_device.php', 'httpmethod': 'post', 'headers': {'User-Agent':'Apache-HttpClient/UNAVAILABLE (java 1.4)'}, 'params': None, 'data': data}

def BindToIggByKey(campaign_data,app_data,device_data):
	
	params = {
		'm_key':int(time.time()*1000),
		'm_game_id':campaign_data.get('game_id'),
		'b_key':device_data.get('adid'),
		's_type':'guest',
		'signed_key':'9dafff5d921c5dc29ff392f9020f4439',
	}
	m_data = device_data.get('adid')+"f6239975b2faae941ec24695e4db5bba"+str(params.get('m_key'))
	params['m_data']=util.md5(m_data)
	
	return {'url':'http://cgi.igg.com:9000/public/BindToIggByKey', 'httpmethod':'post', 'headers':{'User-Agent': 'Apache-HttpClient/UNAVAILABLE (java 1.4)'}, 'params':params , 'data':None}
	
def make_appsflyer_device_fingerprint_id(app_data):
	if not app_data.get('appsflyer_deviceFingerPrintId'):
		num1 = random.randint(1,2)
		if num1==1:
			app_data['appsflyer_deviceFingerPrintId'] = 'ffffffff-'+random_string(4)+'-'+random_string(4)+'-ffff-ffff'+random_string(8)
		if num1==2:
			app_data['appsflyer_deviceFingerPrintId'] = '00000000-'+random_string(4)+'-'+random_string(4)+'-0000-0000'+random_string(8)
		
	return app_data.get('appsflyer_deviceFingerPrintId')
	
def random_string(len):
	return ''.join([random.choice("0123456789abcdef") for _ in range(len)])	
	

def install(app_data, device_data):
	
	print 'plz wait...'
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
	time.sleep(random.randint(60,100))

	
	app_data['af_gcm_token']=str(int(time.time()*1000))

	make_battery_level(app_data)
	make_appsflyer_device_fingerprint_id(app_data)
	app_userId(app_data)

	if not app_data.get('cksm_v1'):
		app_data['cksm_v1']= str(''.join(random.choice('abcdef0123456789') for i in range(32))) #3cccfc64045a0530cf2cde06efbf242e

	
	if not app_data.get('token_count'):
		app_data['token_count'] = 0
	#app_data['token_count']=0
	# if not app_data.get('insdate'):
	# 	app_data['insdate']=str(int(time.time())-random.randint(5,1000))
	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	if not app_data.get('uid'):
		app_data['uid']=str(int(time.time()*1000))+'-'+str(random.randint(1000000000000000000,9999999999999999999))
		
		
	if not app_data.get('firstLaunchDate'):
		app_data['firstLaunchDate']=datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%d_%I%M%S")+"+0000"	 
	if not app_data.get('m_id'):
		app_data['m_id'] = random.randint(350000000,380000000)
	
	app_data['login_flag']=False

	if not app_data.get('login_type'):
		app_data['login_type']=''

	print "Apps Flyer track call\n==========================================================\n"
	appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**appsflyerTrack)
	app_data['cal_open_time']=int(time.time())
	
	eventName="af_17_welcome_page"
	eventValue=json.dumps({})
	print "Apps Flyer Event Call\n==========================================================\n"
	appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
	util.execute_request(**appsflyer_event1)
	
	print "Apps Flyer register call\n==========================================================\n"
	appsflyerTrack=appsflyer_register(campaign_data, app_data, device_data)
	util.execute_request(**appsflyerTrack)
	app_data['cal_open_time']=int(time.time())

	print "Apps Flyer register call\n==========================================================\n"
	appsflyerTrack=appsflyer_register(campaign_data, app_data, device_data,temp=True)
	util.execute_request(**appsflyerTrack)
	app_data['cal_open_time']=int(time.time())

	request = appsflyer_attr(campaign_data, app_data, device_data)
	util.execute_request(**request)

	# app_data['afgcmtoken'] = app_data.get('af_gcm_token')
	
	# if random.randint(1,100)<=10:
	# 	return {'status':True}
		
	print "Apps Flyer track call\n==========================================================\n"
	appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data,True)
	util.execute_request(**appsflyerTrack)
	app_data['cal_open_time']=int(time.time())

	app_data['user_id'] = str(uuid.uuid4())
	
	if random.randint(1,100)<=70:
		
		print "Apps Flyer track call\n==========================================================\n"
		appsflyerTrack=appsflyer_stats(campaign_data, app_data, device_data)
		util.execute_request(**appsflyerTrack)
		app_data['cal_open_time']=int(time.time())	
		
		print "Apps Flyer track call\n==========================================================\n"
		appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data,True)
		util.execute_request(**appsflyerTrack)
		app_data['cal_open_time']=int(time.time())
		
	call_pattern(campaign_data, app_data, device_data)

	if app_data.get('login_flag')==True:
		time.sleep(random.randint(2,5))
		eventName="af_17_app_active_day1"
		eventValue=json.dumps({})
		print "Apps Flyer Event Call\n==========================================================\n"
		appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
		util.execute_request(**appsflyer_event1)

		eventName="af_17_app_active_day3"
		eventValue=json.dumps({})
		print "Apps Flyer Event Call\n==========================================================\n"
		appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
		util.execute_request(**appsflyer_event1)

		eventName="af_17_app_active_day7"
		eventValue=json.dumps({})
		print "Apps Flyer Event Call\n==========================================================\n"
		appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
		util.execute_request(**appsflyer_event1)

		eventName="af_17_app_active_day14"
		eventValue=json.dumps({})
		print "Apps Flyer Event Call\n==========================================================\n"
		appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
		util.execute_request(**appsflyer_event1)
		
		print "Apps Flyer track call\n==========================================================\n"
		appsflyerTrack=appsflyer_stats(campaign_data, app_data, device_data)
		util.execute_request(**appsflyerTrack)
		app_data['cal_open_time']=int(time.time())		
	
	return {'status':True}

	#========================================== Open Call ===========================================

def open(app_data, device_data,day=1):
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='android')
		time.sleep(random.randint(60,100))
	app_userId(app_data)
	make_battery_level(app_data)
	make_appsflyer_device_fingerprint_id(app_data)

	if not app_data.get('cksm_v1'):
		app_data['cksm_v1']= str(''.join(random.choice('abcdef0123456789') for i in range(32))) #3cccfc64045a0530cf2cde06efbf242e
	
	if not app_data.get('token_count'):
		app_data['token_count'] = 0
	#app_data['token_count']=0
	# if not app_data.get('insdate'):
	# 	app_data['insdate']=str(int(time.time())-random.randint(5,1000))
	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	if not app_data.get('uid'):
		app_data['uid']=str(int(time.time()*1000))+'-'+str(random.randint(1000000000000000000,9999999999999999999))
		
		
	if not app_data.get('firstLaunchDate'):
		app_data['firstLaunchDate']=datetime.datetime.utcfromtimestamp(time.time()).strftime("%Y-%m-%d_%I%M%S")+"0000"	 

	if not app_data.get('m_id'):
		app_data['m_id'] = random.randint(350000000,380000000)
	
	if not app_data.get('af_gcm_token'):	
		app_data['af_gcm_token']=str(int(time.time()*1000))
	
	if not app_data.get('login_flag'):		
		app_data['login_flag']=False

	if not app_data.get('login_type'):
		app_data['login_type']=''
		
	print "Apps Flyer track call\n==========================================================\n"
	appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data,True)
	util.execute_request(**appsflyerTrack)
	app_data['cal_open_time']=int(time.time())
	
	time.sleep(random.randint(10,20))
	
	call_pattern(campaign_data, app_data, device_data)

	if random.randint(1,100)<=70:
		
		print "Apps Flyer track call\n==========================================================\n"
		appsflyerTrack=appsflyer_stats(campaign_data, app_data, device_data)
		util.execute_request(**appsflyerTrack)
		app_data['cal_open_time']=int(time.time())	
		
		print "Apps Flyer track call\n==========================================================\n"
		appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data,True)
		util.execute_request(**appsflyerTrack)
		app_data['cal_open_time']=int(time.time())
	
	if app_data.get('login_flag')==True:

		if app_data.get('login_type')=='google':
			if random.randint(1,100)<=10:

				eventName="af_17_welcome_page"
				eventValue=json.dumps({})
				print "Apps Flyer Event Call\n==========================================================\n"
				appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
				util.execute_request(**appsflyer_event1)

				time.sleep(random.randint(5,10))
				eventName="af_17_login_button"
				eventValue=json.dumps({"af_17_reg_method":"af_17_reg_google"})
				print "Apps Flyer Event Call\n==========================================================\n"
				appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
				util.execute_request(**appsflyer_event1)

				eventName="af_17_3rd_auth"
				eventValue=json.dumps({"af_17_reg_method":"af_17_reg_google"})
				print "Apps Flyer Event Call\n==========================================================\n"
				appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
				util.execute_request(**appsflyer_event1)

				eventName="af_17_3rd_relogin"
				eventValue=json.dumps({"af_17_reg_method":"af_17_reg_google"})
				print "Apps Flyer Event Call\n==========================================================\n"
				appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
				util.execute_request(**appsflyer_event1)
		
		time.sleep(random.randint(2,5))
		eventName="af_17_app_active_day1"
		eventValue=json.dumps({})
		print "Apps Flyer Event Call\n==========================================================\n"
		appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
		util.execute_request(**appsflyer_event1)

		eventName="af_17_app_active_day3"
		eventValue=json.dumps({})
		print "Apps Flyer Event Call\n==========================================================\n"
		appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
		util.execute_request(**appsflyer_event1)

		eventName="af_17_app_active_day7"
		eventValue=json.dumps({})
		print "Apps Flyer Event Call\n==========================================================\n"
		appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
		util.execute_request(**appsflyer_event1)

		eventName="af_17_app_active_day14"
		eventValue=json.dumps({})
		print "Apps Flyer Event Call\n==========================================================\n"
		appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
		util.execute_request(**appsflyer_event1)

		print "--------- Purchase Call--------------"
		if purchase.isPurchase(app_data,day,advertiser_demand=10):
			flag = 0
			purchase_random = random.randint(1,100)
			
			if purchase_random <= 80:
				purchase_type = '1'
			
			elif purchase_random > 80 and purchase_random <= 95:
				purchase_type = '2'
				
			elif purchase_random > 95 and purchase_random <= 100:
				purchase_type = '3'
				
			else:
				flag = 1
				purchase_type = False
			
			if flag == 0:
				if not purchase_type == False:
					
					eventName="af_purchase"
					eventValue=json.dumps({"af_content_type":campaign_data.get('purchase').get(purchase_type).get('af_content_type'),"af_content_id":campaign_data.get('purchase').get(purchase_type).get('af_content_id'),"af_revenue":campaign_data.get('purchase').get(purchase_type).get('af_revenue'),"af_currency":"USD"})
					print "Apps Flyer Event Call\n==========================================================\n"
					appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
					util.execute_request(**appsflyer_event1)
					
					# eventName="af_purchase"
					# eventValue=json.dumps({"af_content_type":campaign_data.get('purchase').get(purchase_type).get('af_content_id'),"af_content_id":app_data.get('app_user_id'),"af_revenue":campaign_data.get('purchase').get(purchase_type).get('af_revenue'),"af_currency":"USD"})
					# print "Apps Flyer Event Call\n==========================================================\n"
					# appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
					# util.execute_request(**appsflyer_event1)
					
		time.sleep(random.randint(10,20))	
		print "Apps Flyer track call\n==========================================================\n"
		appsflyerTrack=appsflyer_stats(campaign_data, app_data, device_data)
		util.execute_request(**appsflyerTrack)
		app_data['cal_open_time']=int(time.time())	
	
	return {'status':True}

def call_pattern(campaign_data, app_data, device_data):
	
	if app_data.get('login_flag')==False:
		choice=random.choice([1,2,3,3,3,3,4,4,4,4,5,5,5,5,5,5,5,5,6,6])
		if choice==1:
			eventName="af_17_login_button"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_facebook"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			time.sleep(random.randint(10,20))
			
			eventName="af_17_3rd_auth"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_facebook"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)

			# eventName="af_17_3rd_relogin"
			# eventValue=json.dumps({"af_17_reg_method":"af_17_reg_google"})
			# print "Apps Flyer Event Call\n==========================================================\n"
			# appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			# util.execute_request(**appsflyer_event1)

			eventName="af_17_3rd_account"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_facebook"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			time.sleep(random.randint(10,20))
			
			eventName="af_complete_registration"
			eventValue=json.dumps({"af_registration_method":"fb_mobile_complete_registration"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			app_data['login_flag']=True
			
		elif choice==3:
			# eventName="af_17_login_button"
			# eventValue=json.dumps({"af_17_reg_method":"af_17_reg_line"})
			# print "Apps Flyer Event Call\n==========================================================\n"
			# appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			# util.execute_request(**appsflyer_event1)
			# time.sleep(random.randint(10,20))
			
			eventName="af_17_login_button"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_phone"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			
			eventName="af_17_phone_username"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_phone"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			time.sleep(random.randint(10,20))
			
			eventName="af_17_phone_password"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_phone"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			
			eventName="af_17_phone_auth"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_phone"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			time.sleep(random.randint(10,20))
			
			eventName="af_complete_registration"
			eventValue=json.dumps({"af_registration_method":"Phone"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			app_data['login_flag']=True
			
		elif choice==2:
			if random.randint(1,100)<=90:
				eventName="af_17_welcome_page"
				eventValue=json.dumps({})
				print "Apps Flyer Event Call\n==========================================================\n"
				appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
				util.execute_request(**appsflyer_event1)
			
			eventName="af_17_login_button"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_twitter"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			time.sleep(random.randint(10,20))

			eventName="af_17_3rd_account"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_twitter"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			time.sleep(random.randint(10,20))
			
			eventName="af_complete_registration"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_twitter"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)	
			app_data['login_flag']=True

		elif choice==4:
			
			eventName="af_17_login_button"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_line"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			time.sleep(random.randint(10,20))

			eventName="af_17_3rd_account"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_line"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			time.sleep(random.randint(10,20))
			
			eventName="af_complete_registration"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_line"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)	
			app_data['login_flag']=True

		elif choice==5:
			
			eventName="af_17_login_button"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_google"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			time.sleep(random.randint(10,20))

			eventName="af_17_3rd_auth"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_google"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)

			eventName="af_17_3rd_account"
			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_google"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)
			time.sleep(random.randint(10,20))
			
			eventName="af_complete_registration"
			eventValue=json.dumps({"af_registration_method":"Google"})
			print "Apps Flyer Event Call\n==========================================================\n"
			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
			util.execute_request(**appsflyer_event1)	
			app_data['login_flag']=True
			app_data['login_type']='google'
		
	print "Apps Flyer track call\n==========================================================\n"
	appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data,True)
	util.execute_request(**appsflyerTrack)
	app_data['cal_open_time']=int(time.time())

# def call_pattern(campaign_data, app_data, device_data):
	
# 	if app_data.get('login_flag')==False:
# 		choice=random.choice([1,1,1,1,2,2,2,3])
# 		if choice==1:
# 			eventName="af_17_login_button"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_facebook"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)
# 			time.sleep(random.randint(10,20))
			
# 			eventName="af_17_3rd_auth"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_facebook"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)

# 			eventName="af_17_3rd_relogin"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_google"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)

# 			eventName="af_17_3rd_account"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_facebook"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)
# 			time.sleep(random.randint(10,20))
			
# 			eventName="af_complete_registration"
# 			eventValue=json.dumps({"af_registration_method":"fb_mobile_complete_registration"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)
# 			app_data['login_flag']=True
			
# 		elif choice==2:
# 			eventName="af_17_login_button"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_line"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)
# 			time.sleep(random.randint(10,20))
			
# 			eventName="af_17_login_button"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_phone"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)
			
# 			eventName="af_17_phone_username"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_phone"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)
# 			time.sleep(random.randint(10,20))
			
# 			eventName="af_17_phone_password"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_phone"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)
			
# 			eventName="af_17_phone_auth"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_phone"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)
# 			time.sleep(random.randint(10,20))
			
# 			eventName="af_complete_registration"
# 			eventValue=json.dumps({"af_registration_method":"Phone"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)
# 			app_data['login_flag']=True
			
# 		elif choice==3:
# 			if random.randint(1,100)<=90:
# 				eventName="af_17_welcome_page"
# 				eventValue=json.dumps({})
# 				print "Apps Flyer Event Call\n==========================================================\n"
# 				appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 				util.execute_request(**appsflyer_event1)
			
# 			eventName="af_17_login_button"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_twitter"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)
# 			time.sleep(random.randint(10,20))
			
# 			eventName="af_17_phone_username"
# 			eventValue=json.dumps({"af_17_reg_method":"af_17_reg_line"})
# 			print "Apps Flyer Event Call\n==========================================================\n"
# 			appsflyer_event1 = appsflyer_event(campaign_data, app_data, device_data,eventName,eventValue)
# 			util.execute_request(**appsflyer_event1)	
# 			app_data['login_flag']=True
		
# 	print "Apps Flyer track call\n==========================================================\n"
# 	appsflyerTrack=appsflyer_track(campaign_data, app_data, device_data,True)
# 	util.execute_request(**appsflyerTrack)
# 	app_data['cal_open_time']=int(time.time())
	
# ======================================== 
#	  Apps Flyer T and events call 
# ========================================

def appsflyer_attr(campaign_data, app_data, device_data):
 	if not app_data.get('counter'):
		app_data['counter']=1
	else:
		app_data['counter']=app_data['counter']+1

 	method = "post"
	url = 'http://attr.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'
	headers = {
		"Accept-Encoding" : "gzip",
		"Content-Type" : "application/json",
		"User-Agent" : 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('brand')+' '+device_data.get('model')+' Build/'+  device_data.get('build')+')',

		}
	params = {
		"app_id" : campaign_data.get('package_name'),
		"buildnumber" : campaign_data.get('appsflyer').get('build_number'),

		}
	data = {
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_gcm_token" : app_data.get('af_gcm_token'),
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"android_id" : device_data.get('android_id'),
		"appUserId" : "",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "no",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
								},
						"sensors" : [{u'sN': u'LIS3DH Accelerometer -Wakeup Secondary', u'sVE': [0, 0, 10.002777], u'sV': u'STMicroelectronics', u'sVS': [-0.07846069, 0.039230347, 9.806656], u'sT': 1}, {u'sN': u'AK09911 Magnetometer', u'sVE': [-15.96, 42.48, -67.14], u'sV': u'AKM', u'sVS': [0, 0, 0], u'sT': 2}, {u'sN': u'LIS3DH Accelerometer', u'sVE': [0, 0, 10.002777], u'sV': u'STMicroelectronics', u'sVS': [-0.07846069, 0.039230347, 9.806656], u'sT': 1}, {u'sN': u'AK09911-pseudo-gyro', u'sVE': [-0.0006671094, 0.0124949375, -0.05001937], u'sV': u'akm', u'sVS': [0, 0, 0], u'sT': 4}],
		},
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "true",
		"isGaidWithGps" : "true",
		"ivc" : False,
		"lang" : util.get_language_name('en'),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : "WIFI",
		"open_referrer" : 'android-app://com.android.vending',
		"operator" : device_data.get('carrier'),
		"p_receipt" :util.get_random_string('google_token',571).replace('-','+').replace('_','/')+"=",
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"sc_o" : "p",
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : app_data.get('timepassedsincelastlaunch'),
		"uid" : app_data.get('uid'),

		}
	if app_data.get('referrer'):
		data['referrer'] = app_data.get('referrer')
	else:
		data['referrer'] = 'utm_source=(not%20set)&utm_medium=(not%20set)'

	

	
	string_for_afv 	 =  data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13]
	data['af_v']	 =  util.sha1(string_for_afv)
	
	string_for_af_v2 =  data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']	 =  util.sha1(util.md5(string_for_af_v2))
	
	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])
	
	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("build_number"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("build_number"))	
	data["kef"+kefKey] = kefVal

	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}
	
def appsflyer_track(campaign_data, app_data, device_data, event_call=False):
	make_battery_level(app_data)
	make_appsflyer_device_fingerprint_id(app_data)
	get_deviceData(app_data, device_data)
	url = 'http://t.appsflyer.com/api/v4/androidevent'
	
	if not app_data.get('isFirstCall'):
		app_data['isFirstCall']='true'
	else:
		app_data['isFirstCall']='false'
	
	if app_data.get('iaecounter')>=0:
		app_data['iaecounter']=app_data['iaecounter']+1 
	else:
		app_data['iaecounter']=0
		
	if not app_data.get('counter'):
		app_data['counter']=1
	else:
		app_data['counter']=app_data['counter']+1
	
	app_data['af_times']=str(int(time.time()*1000))
	
	header={
		'Content-Type': 'application/json',
		'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('brand')+' '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
		'Accept-Encoding': 'gzip'
	}
	params={
			'buildnumber': campaign_data.get('appsflyer').get('build_number'), 
			'app_id': campaign_data.get('package_name')
	}
	timeSinceLastLaunch_return(app_data)

	if not app_data.get('dpixy'):
		app_data['dpixy']= str(random.randint(200,300))
	
	data={
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		# "af_gcm_token" : app_data.get('af_gcm_token'),
		"af_preinstalled" : "false",
		"af_sdks" : "0000000000",
		"af_timestamp" : timestamp(),
		"android_id" : device_data.get('android_id'),
		"appUserId" : "",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"batteryLevel" : get_batteryLevel(app_data),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',34),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"btch" : "no",
						"btl" : get_batteryLevel(app_data),
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
						"sensors" : [{u'sN': u'LIS3DH Accelerometer -Wakeup Secondary', u'sVE': [0, 0, 10.002777], u'sV': u'STMicroelectronics', u'sVS': [-0.07846069, 0.039230347, 9.806656], u'sT': 1}, {u'sN': u'AK09911 Magnetometer', u'sVE': [-15.96, 42.48, -67.14], u'sV': u'AKM', u'sVS': [0, 0, 0], u'sT': 2}, {u'sN': u'LIS3DH Accelerometer', u'sVE': [0, 0, 10.002777], u'sV': u'STMicroelectronics', u'sVS': [-0.07846069, 0.039230347, 9.806656], u'sT': 1}, {u'sN': u'AK09911-pseudo-gyro', u'sVE': [-0.0006671094, 0.0124949375, -0.05001937], u'sV': u'akm', u'sVS': [0, 0, 0], u'sT': 4}],
		},
		# "deviceRm" : device_data.get('build'),
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : app_data.get('isFirstCall'),
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"sdk" : device_data.get('sdk'),
		"timepassedsincelastlaunch" : app_data.get('timepassedsincelastlaunch'),
		"uid" : app_data.get('uid'),
		'ivc':False,
		"open_referrer" : 'android-app://com.android.vending',
		"sc_o" : "p",
		"p_receipt" : util.get_random_string('google_token',571).replace('-','+').replace('_','/')+"=",

		
	}
	if event_call:
		del data['af_sdks']
		del data['batteryLevel']
		data['registeredUninstall']=True
	else:
		data['isFirstCall']="true"
	
	if app_data.get('referrer'):
		data['referrer']=urllib.quote(app_data.get('referrer'))
	else:
		data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'

	if app_data.get('afgcmtoken'):
		data['af_gcm_token']=app_data.get('afgcmtoken')

	# if app_data.get('counter')==1:
	# 	del data['af_gcm_token']

	if app_data.get('user_id'):
		data['appUserId'] = app_data.get('user_id')

	if int(app_data.get('counter'))>2:
		if data.get('deviceData').get('sensors'):
			del data['deviceData']['sensors']	

		del data['p_receipt']

	if app_data.get('counter')>1:
		data['open_referrer']="android-app://"+campaign_data.get("package_name")

		
	s1 = data.get('appsflyerKey')[0:7]
	s2 = data.get('uid')[0:7]
	s3 = data.get('af_timestamp')[-7:13]
	string_for_afv = s1+s2+s3
	data['af_v']=util.sha1(string_for_afv)
	af_v2 =data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']=util.sha1(util.md5(af_v2))

	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("build_number"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("build_number"))
	data["kef"+kefKey] = kefVal
		
	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params': params, 'data': json.dumps(data)}

def appsflyer_event(campaign_data, app_data, device_data, eventName=False, eventValue=False):
	make_battery_level(app_data)
	make_appsflyer_device_fingerprint_id(app_data)
	get_deviceData(app_data, device_data)
	
	url = 'http://events.appsflyer.com/api/v4/androidevent'
	
	if not app_data.get('isFirstCall'):
		app_data['isFirstCall']='true'
	else:
		app_data['isFirstCall']='false'
	
	if app_data.get('iaecounter')>=0:
		app_data['iaecounter']=app_data['iaecounter']+1 
	else:
		app_data['iaecounter']=0
		
	if not app_data.get('counter'):
		app_data['counter']=0
	# else:
	# 	app_data['counter']=app_data['counter']+1	
	
	app_data['af_times']=str(int(time.time()*1000))
	
	header={
		'Content-Type': 'application/json',
		'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('brand')+' '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
		'Accept-Encoding': 'gzip'
	}
	params={
			'buildnumber': campaign_data.get('appsflyer').get('build_number'), 
			'app_id': campaign_data.get('package_name')
	}
	# timeSinceLastLaunch_return(app_data)

	if not app_data.get('dpixy'):
		app_data['dpixy']= str(random.randint(200,300))
	
	data={
		"advertiserId" : device_data.get('adid'),
		"advertiserIdEnabled" : "true",
		"af_events_api" : "1",
		"af_preinstalled" : "false",
		"af_timestamp" : timestamp(),
		"android_id" : device_data.get('android_id'),
		"appUserId" : "",
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"appsflyerKey" : campaign_data.get('appsflyer').get('key'),
		"brand" : device_data.get('brand'),
		"carrier" : device_data.get('carrier'),
		"cksm_v1" : util.get_random_string('hex',32),
		"counter" : str(app_data.get('counter')),
		"country" : device_data.get('locale').get('country'),
		"date1" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"date2" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'),
		"device" : device_data.get('device'),
		"deviceData" : {
						"arch" : "",
						"build_display_id" : device_data.get('build'),
						"cpu_abi" : device_data.get("cpu_abi") if device_data.get("cpu_abi") else "",
						"cpu_abi2" : device_data.get("cpu_abi2") if device_data.get("cpu_abi2") else "",
						"dim" : {"d_dpi": device_data.get('dpi'),								
								"size": app_data.get('dim_size'),
								"x_px": device_data.get('resolution').split('x')[1],
								"xdp": app_data.get('xdp'),
								"y_px": device_data.get('resolution').split('x')[0],
								"ydp": app_data.get('ydp'),
							},
		},
		# "deviceRm" : device_data.get('build'),
		'ivc':False,
		"deviceType" : "user",
		"firstLaunchDate" : app_data.get('firstLaunchDate'),
		"iaecounter" : str(app_data.get('iaecounter')),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"installer_package" : "com.android.vending",
		"isFirstCall" : "true",
		"isGaidWithGps" : "true",
		"lang" : util.get_language_name(device_data.get('locale').get('language')),
		"lang_code" : device_data.get('locale').get('language'),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"platformextension" : "android_native",
		"product" : device_data.get('product'),
		"referrer" : 'utm_source=google-play&utm_medium=organic',
		"registeredUninstall" : False,
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),
		"sc_o" : "p",

	}
	
	data['eventName'] = eventName
	data['eventValue'] = eventValue
	data['isFirstCall'] = app_data.get('isFirstCall')
	if app_data.get('iaecounter')>1:
		data['registeredUninstall']=True

	# data['timepassedsincelastlaunch'] = str(app_data.get('timepassedsincelastlaunch'))
	
	if app_data.get('iaecounter')>1:
		if app_data.get('prev_event_time') and app_data.get('prev_event_value') and app_data.get('prev_event_name'):
			data['prev_event'] = json.dumps({"prev_event_timestamp":app_data.get('prev_event_time'),"prev_event_value":app_data.get('prev_event_value'),"prev_event_name":app_data.get('prev_event_name')})
			
	update_eventsRecords(app_data,eventName,eventValue)
	
	if app_data.get('referrer'):
		data['referrer']=app_data.get('referrer')
	else:
		data['referrer']='utm_source=(not%20set)&utm_medium=(not%20set)'

	if app_data.get('afgcmtoken'):
		data['af_gcm_token']=app_data.get('afgcmtoken')

	if app_data.get('user_id'):
		data['appUserId'] = app_data.get('user_id')	
		
	s1 = data.get('appsflyerKey')[0:7]
	s2 = data.get('uid')[0:7]
	s3 = data.get('af_timestamp')[-7:13]
	string_for_afv = s1+s2+s3
	data['af_v']=util.sha1(string_for_afv)
	af_v2 =data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')
	data['af_v2']=util.sha1(util.md5(af_v2))

	data['cksm_v1'] = cksm_v1(data['date1'],data['af_timestamp'])

	if not app_data.get("sensorCount"):
		app_data["sensorCount"] = str(random.randint(3,10))
	if not app_data.get("batteryTemp"):
		app_data["batteryTemp"] = random.choice(["300", "290", "280", "270"])
	kefVal = generateValue(b=app_data.get("batteryTemp"),x="0",s=app_data.get("sensorCount"),p=str(len(data.keys())),ts=data["af_timestamp"],fl=data["firstLaunchDate"],buildnumber=campaign_data.get("appsflyer").get("build_number"))
	kefKey = get_key_half(device_data.get('brand'),device_data.get('sdk'),data["lang"],data["af_timestamp"],buildnumber=campaign_data.get("appsflyer").get("build_number"))
	data["kef"+kefKey] = kefVal
		
	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params': params, 'data': json.dumps(data)}

def update_eventsRecords(app_data,eventName,eventValue):
	app_data['prev_event_name'] = eventName or ''
	app_data['prev_event_value'] = eventValue or ''
	app_data['prev_event_time'] = str(int(time.time()*1000)) or '1234567890000'	

def appsflyer_stats(campaign_data, app_data, device_data):

	make_appsflyer_device_fingerprint_id(app_data)
	get_deviceData(app_data, device_data)
	url = 'https://stats.appsflyer.com/stats'
	method = 'post'
	headers = {
		'Content-Type': 'application/json',
		'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('brand')+' '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
		'Accept-Encoding': 'gzip',
		}
	params = None
	data = {
		"platform": "Android",
		"gcd_conversion_data_timing": "0",
		"devkey": campaign_data.get('appsflyer').get('key'),
		"statType": "user_closed_app",
		"time_in_app": str(app_data.get('timepassedsincelastlaunch')),
		"deviceFingerPrintId": app_data.get('appsflyer_deviceFingerPrintId'),
		"app_id": campaign_data.get('package_name'),
		"uid": app_data.get('uid'),
		"channel": "(null)",
		"launch_counter": str(app_data.get('counter'))
		}
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}	
	
def appsflyer_register(campaign_data, app_data, device_data, temp=False):

	def_google_token(app_data)
	get_deviceData(app_data, device_data)
	app_data['afgcmtoken']=app_data.get('google_token')+':APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

	url = 'http://register.appsflyer.com/api/v4/androidevent'
	method = 'post'
	headers = {
		'User-Agent':app_data.get('ua'),
		'Content-Type':'application/json',
		'Accept-Encoding':'gzip',
		}
	params = {
		'buildnumber': campaign_data.get('appsflyer').get('build_number'),
		'app_id': campaign_data.get('package_name')
	}
	data = {
		"advertiserId" : device_data.get('adid'),
		"af_gcm_token" : app_data.get('afgcmtoken'),
		"app_name" : campaign_data.get('app_name'),
		"app_version_code" : campaign_data.get('app_version_code'),
		"app_version_name" : campaign_data.get('app_version_name'),
		"brand" : device_data.get('brand').upper(),
		"carrier" : device_data.get('carrier'),
		"devkey" : campaign_data.get('appsflyer').get('key'),
		"installDate" : datetime.datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
		"launch_counter" : str(app_data.get('counter')),
		"model" : device_data.get('model'),
		"network" : device_data.get('network').upper(),
		"operator" : device_data.get('carrier'),
		"sdk" : device_data.get('sdk'),
		"uid" : app_data.get('uid'),
		"appUserId" : '',
		}

	if temp:
		data['af_gcm_token'] = app_data.get('af_gcm_token')+','+app_data.get('afgcmtoken')

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}	
	

#########################################################################

def def_google_token(app_data):
	if not app_data.get('google_token'):
		app_data['google_token'] = ''.join(random.choice(string.digits + string.ascii_letters ) for _ in range(11))
		
def app_userId(app_data):
	if not app_data.get('app_user_id'):
		app_data['app_user_id']=str(uuid.uuid4())

def get_batteryLevel(app_data):
	if not app_data.get('btl'):
		app_data['btl']=str(random.randint(10,100))+".0"
	return app_data.get('btl')	

def get_deviceData(app_data, device_data):
	if not app_data.get('dim_size'):
 		app_data['dim_size']=str(random.randint(1,5))
 	if not app_data.get('xdp'):
 		app_data['xdp']='294.'+str(random.randint(600,999))
 	if not app_data.get('ydp'):
 		app_data['ydp']='295.'+str(random.randint(100,600))	

###########################################################
# Utility methods : MISC
#
###########################################################
def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def get_google_gcmToken(app_data):
	if not app_data.get('af_gcm_token'):
		app_data['af_gcm_token'] = app_data.get('afGoogleInstanceID') +':'+get_gcmToken()

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
	
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def sha256(data):
	import hashlib
	sha_obj = hashlib.sha256()
	sha_obj.update(data)
	return sha_obj.hexdigest()	
###################################################################
# appsflyer_cksm_generator
# parameter 			: date1,af_timestamp,
#
# Generates checksum value for appsflyer.
###################################################################
def change_char(s, p, r):
	return s[:p]+r+s[p+1:]
		
def md5(data, radix=16):
	import hashlib
	md5_obj = hashlib.md5()
	md5_obj.update(data)
	if radix == 16:
		return md5_obj.hexdigest()
	elif radix == 64:
		return base64(md5_obj.digest())

def cksm_v1(ins_date,ins_time):
	pk = campaign_data.get("package_name").split('.')
	pk[0], pk[-1] = pk[-1], pk[0]
	pkn='.'.join(pk)
	string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
	print "========cksm_v1========"
	print string
	print "======================="
	sha256_result = sha256(string)	
	md5_result = md5(sha256_result)	
		
	n = ins_time
	sb = md5_result
	n4 = 0

	sb = change_char(sb,17,'f')
	sb = change_char(sb,27,'f')

	for i in range(0,len(str(n))):
		n4 += int(str(n)[i])
		

	insert1 = list('{:02x}'.format(n4))
	sb = change_char(sb,7,insert1[0])
	sb = change_char(sb,8,insert1[1])
			
	j = 0
	n6 = 77
	n3 = 0
	for i in range(0,len(str(sb))):
		n3 += int(str(sb)[i],36)
		
	if n3>100:
		n8 = 90
		n3%=100

	if n3<10:
		n3 = '0'+str(n3)
		
	sb = insert_char(sb,23,str(n3))
	return sb		
	

"""
VALUE GENERATOR:-
	It uses few algorithms to generate value for the key.
"""


###################################################################
# appsflyer_kef_key and value generator
# parameter 			: brand, sdk, lang, af_ts, buildnumber, etc
#
# Generates kef key and value for appsflyer.
###################################################################
global temp, counter
temp = 0
counter = 1

"""
KEY GENERATOR:-
	Based on appsflyer sdk version it uses algorithms to generate kef key.
"""

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
	if buildnumber=="4.8.18":
		return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
	else:
		c = '\u0000'
		obj2 = af_ts[::-1]
		out = calculate(sdk, obj2, brand)
		i = len(out)
		if i > 4:
			i2 = globals()['counter']+65
			globals()['temp'] = i2 % 128
			if (1 if i2 % 2 != 0 else None) != 1:
				out.delete(4, i)
			else:
				out.delete(5, i)
		else:
			while i < 4:
				i3 = globals()['counter'] + 119
				globals()['temp'] = i3 % 128
				if (16 if i3 % 2 != 0 else 93) != 16:
					i += 1
					out+='1'
				else:
					i += 66
					out+='H'
				i3 = globals()['counter'] + 109
				globals()['temp'] = i3 % 128
				i3 %= 2
		return out.__str__()

def generateValue(b,x,s,p,ts,fl,buildnumber):
	part1=generateValue1get(ts,fl,buildnumber)
	str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, "utf-8")
	for i in range(len(str_)):
		str_[i] = int((str_[i] ^ ((i % 2) + 42)))

	stringBuilder = ""
	for toHexString in str_:
		toHexString2 = str(hex(int(toHexString)))[2:]
		if 1 == len(toHexString2):
			toHexString2 = "0"+str(toHexString2)
		stringBuilder+=toHexString2
	part2 = stringBuilder.__str__()
	return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
	gethash = sha256(ts+firstLaunch+buildnumber)
	return gethash[:16]

"""
UTIL FUNCTIONS USED:-
	Utility functions used to generate key and value for KEF field.
"""

def calculate(sdk, obj, brand):
	allList = [sdk, obj, brand]
	arrayList = []
	i = 0
	while True:
		flag = 1
		if i >= 3:
			break
		i2 = globals()['temp'] + 63
		globals()['counter'] = i2 % 128
		if i2 % 2 != 0:
			flag = None
		if flag != None:
			arrayList.append(len(allList[i]))
			i += 31
		else:
			arrayList.append(len(allList[i]))
			i += 1
	sorted(arrayList)
	intValue = int(arrayList[0])
	out = ""
	i3 = 0
	while i3 < intValue:
		i4 = globals()['counter']+99
		globals()['temp'] = i4 % 128
		i5 =  globals()['temp']+67
		globals()['counter'] = i5 % 128
		i5 %= 2
		number = None
		if i4 % 2 != 0:
			a = 57
		else:
			a = 83
		if a != 83:
			i4 = 1
		else:
			i4 = 0
		while i4 < 3:
			i5 = globals()['temp'] + 87
			globals()['counter'] = i5 % 128
			i5 %= 2
			i5 = ord(allList[i4][i3])
			if (92 if number == None else 39) != 39:
				i6 = globals()['temp'] + 55
				globals()['counter'] = i6 % 128
				i6 %= 2
				i6 = globals()['counter'] + 39
				globals()['temp'] = i6 % 128
				i6 %= 2
			else:
				i5 ^= int(number)
			number = i5
			i4 += 1
		out+=str(hex(int(number)))[2:]
		i3 += 1
	return out
###########################################################
				# Extra Funcation							   
############################################################

def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)
	
def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0
