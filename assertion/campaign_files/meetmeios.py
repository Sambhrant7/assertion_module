import uuid,json,random, time,datetime, urllib, string
from Crypto.Cipher import AES
from sdk import getsleep
from sdk import util,purchase, installtimenew
import clicker, Config

campaign_data = {
			'package_name':'com.myYearbook.MyYearbook',
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'ctr':6,
			'app_name' : 'MeetMe',
			'app_id':'372648912',
			'app_version_code': '14.6.0.0',#'14.2.0.0',#'14.0.1.0',#'14.0.0.3',#'13.19.0.0',#'13.18.0.0',#'13.17.0.0',#'13.16.0.1',#13.15.0.2, '13.14.0.0',#'13.12.0.1',#'13.11.0.0',#'13.10.0.0',#'13.9.0.0',#'13.8.0.0','13.7.0.0',#'13.1.0.0',#'13.0.0.0', #12.13.0.0, 13.3.0.0, 13.8.1.0
			'app_version_name': '14.6.0',#'14.2.0',#'14.0.1',#'14.0.0',#'13.19.0',#'13.18.0',#'13.17.0',#'13.16.0',#13.15.0, '13.14.0',#'13.12.0',#'13.11.0',#'13.10.0',#'13.9.0',#'13.8.0','13.7.0',#'13.1.0',#'13.0.0', #12.13.0, 13.3.0, 13.8.1
			'supported_os':'9.1',
			'supported_countries': 'WW',
			'device_targeting':True,
			'app_size':301.4,#286.3,#291.4,#289.8,#288.2,
			'sdk' : 'Android',
			'tracker':'adjust',
			'adjust':{
				'app_token': 'edv54aw2pibk',
				'sdk':'ios4.15.0', #'ios4.12.3'
				'app_updated_at': '2019-10-03T07:39:04.000Z',#'2019-08-08T23:49:38.000Z',#'2019-07-09T04:55:24.000Z',#'2019-06-29T00:30:38.000Z',#'2019-05-29T18:40:09.000Z',#'2019-05-15T20:53:50.000Z',#'2019-04-26T22:51:29.000Z',#'2019-04-12T22:29:01.000Z',#2019-04-04T06:37:27.000Z, '2019-03-21T04:46:57.000Z',#'2019-02-28T02:38:56.000Z+0530',#'2019-02-05T23:37:13.000Z',#'2019-01-25T02:31:28.000Z',#'2019-01-04T03:47:07.000Z',#2018-12-18T02:32:57.000Z, 2018-12-08T06:27:33.000Z,'2018-12-03T21:50:06.000Z',#'2018-09-06T18:48:39.000Z',#'2018-07-30T21:39:34.000Z'
				'secret_id':'3',
				'secret_key':'11885127799081152161193015707902995131',
			},

			'purchase':{
						'1':'4.99',
						'2':'14.99',
						'3':'24.99'
			},
			'retention':{
							1:50,
							2:48,
							3:46,
							4:44,
							5:40,
							6:38,
							7:35,
							8:32,
							9:31,
							10:30,
							11:29,
							12:28,
							13:27,
							14:26,
							15:25,
							16:24,
							17:23,
							18:22,
							19:21,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5
						}
		}
# BS = 16
# pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)

# def encrypt_AES(key, data):
# 	data=pad(data.encode('hex').upper())
# 	return AES.new(key, AES.MODE_ECB).encrypt(data).encode('hex').upper()
	
# def make_x_device_id(app_data):
# 	app_data['x_device_id'] = str(uuid.uuid4())
# 	return app_data.get('x_device_id')
	
# def make_memory_used(app_data):
# 	app_data['memory_used'] = random.randint(500000000,999999999)
# 	return app_data.get('memory_used')

def return_userinfo(app_data):
	
	gender_n = app_data.get('gender')
	if not gender_n:
		app_data['gender'] = random.choice(['male', 'female'])
		gender_n = app_data['gender']
		
	user_info = app_data.get('user_info')
	if not user_info:
		user_data = util.generate_name(gender=gender_n)
		user_info = {}
		# user_info['username'] = user_data.get('username').lower()
		user_info['email'] = user_data.get('username').lower() + '@gmail.com'
		user_info['f_name'] = user_data.get('firstname')
		# user_info['l_name'] = user_data.get('lastname')
		# user_info['sex'] = gender_n
		user_info['gender'] = str(1) if gender_n == 'female' else str(2)
		# user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
		user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
		user_info['password'] = util.get_random_string(type='all',size=16)
		app_data['user_info'] = user_info


def make_session_count(app_data,device_data):
	if not app_data.get('session_count'):
		app_data['session_count'] = 1
	else:
		app_data['session_count'] += 1
		
	return app_data.get('session_count')

def make_sec(app_data,device_data):	
	timez = device_data.get('timezone')
	sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec;
		
	return app_data.get('sec')	

def install(app_data, device_data):	
	print "wait..."

	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios",min_sleep=0)

	app_data['idfa_id'] = str(uuid.uuid4()).upper()	
	app_data['idfv_id'] = str(uuid.uuid4()).upper()
	install_receipt(app_data)
	return_userinfo(app_data)

	app_data['adjust_call_time']=int(time.time())
		
	if not device_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
		app_data['idfa_id'] = device_data.get('idfa_id')
		
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()
		
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('first_time'):
		app_data['first_time']=True

	app_data['registration_flag']=False
		
	if not app_data.get('sec'):
		make_sec(app_data,device_data)

	print 'lat and lon generator'
	lat_lon=current_location(device_data)
	lat_lon_result = util.execute_request(**lat_lon)
	try:
		json_lat_lon_result=json.loads(lat_lon_result.get('data')).get('geo')
		app_data['lat']=str(json_lat_lon_result.get('latitude'))
		app_data['lon']=str(json_lat_lon_result.get('longitude'))
	except:
		print "Exception"
		app_data['lat']= '77.318566'
		app_data['lon']= '28.5814742'

	print "device checkin call"
	req=devicecheckin(app_data,campaign_data,device_data)
	util.execute_request(**req)

	time.sleep(random.randint(1,2))

	global output
	print "meet me mobile settings"
	req=meetmesetting(app_data,campaign_data,device_data)
	res=util.execute_request(**req)
	try:
		output=res.get('res').headers.get('Set-Cookie').split(';')[0].split('PHPSESSID=')[1] 	
	except:
		output='e5934823cbd8972a8f11bd5100a844af'

	# print "meet me login"
	# req=meetmemobilelogin(app_data,campaign_data,device_data)
	# util.execute_request(**req)

	
	app_data['session_start_timestamp'] = int(time.time())
	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**adjustSession)


	print "\n----------------------------ADJUST sdk_click ------------------------------------"
	adjustSession = adjust_sdkclick(campaign_data, app_data, device_data)
	util.execute_request(**adjustSession)

	# app_open(campaign_data,app_data,device_data)

	print '-----------------------ADJUST attribution-----------------------------'
	adjust_event_req=adjust_attribution(campaign_data, app_data, device_data,t1=2,t2=4)
	util.execute_request(**adjust_event_req)
	
	print '-----------------------ADJUST attribution-----------------------------'
	adjust_event_req=adjust_attribution(campaign_data, app_data, device_data,t1=2,t2=5)
	util.execute_request(**adjust_event_req)
	time.sleep(random.randint(20,40))

	if random.randint(1,100) <= 30:

		profile_update(campaign_data, app_data, device_data)

		registration(campaign_data, app_data, device_data)

		chose = random.randint(1,100)
		if chose <= 35:
			reg_female(campaign_data, app_data, device_data)
		else:
			reg_male(campaign_data, app_data, device_data)

		app_data['registration_flag'] = True

		time.sleep(random.randint(60,180))

		if random.randint(0,100)<=78:
			time.sleep(random.randint(480,600))
			live_video(campaign_data, app_data, device_data)
			first_time(campaign_data, app_data, device_data)

		if random.randint(0,100)<=78:
			time.sleep(random.randint(60,120))
			live_video(campaign_data, app_data, device_data)

		
	# if random.randint(1,100)<=90:

	# 	time.sleep(random.randint(60,100))
	# 	registration(campaign_data,app_data,device_data)

	# 	chose=random.choice([0,1])
	# 	if chose==0:
	# 		time.sleep(random.randint(2,4))
	# 		reg_male(campaign_data,app_data,device_data)
	# 	else:
	# 		time.sleep(random.randint(4,6))
	# 		reg_female(campaign_data, app_data, device_data)
	# 	app_data['registration_flag']=True


	# choice=[1,2,3]
	# random.shuffle(choice)
	# for i in range(0,len(choice)):
	# 	choose=choice[i]

	# 	if choose==1 and random.randint(1,100)<=60:
	# 		time.sleep(random.randint(40,60))
	# 		profile_update(campaign_data,app_data,device_data)

	# 	if choose==2 and random.randint(1,100)<=65:
	# 		top = random.randint(1,3)
	# 		for i in range(top):
	# 			time.sleep(random.randint(15,300))
	# 			live_video(campaign_data, app_data, device_data)

	# 			if i==0 and app_data.get('first_time')==True:
	# 				time.sleep(random.randint(1,3))
	# 				first_time(campaign_data, app_data, device_data)
	# 				app_data['first_time']=False

	# 	if choose==3 and random.randint(1,100)<=40:
	# 		time.sleep(random.randint(25,180))
	# 		go_live(campaign_data, app_data, device_data)


	app_data['session_end_timestamp'] = int(time.time())
	
	return {"status" : "true"}


	
def open(app_data,device_data,day=1):

	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="ios")

	install_receipt(app_data)
	return_userinfo(app_data)
	
	if not app_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()	
	
	if not app_data.get('idfv_id'):
		app_data['idfv_id'] = str(uuid.uuid4()).upper()	

	if not app_data.get('sec'):
		make_sec(app_data,device_data)

	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	app_data['adjust_call_time']=int(time.time())

	if not app_data.get('registration_flag'):
		app_data['registration_flag'] = False

	if not app_data.get('first_time'):
		app_data['first_time']=True

	if not app_data.get('session_start_timestamp'):
		app_data['session_start_timestamp'] = int(time.time())

	if not app_data.get('session_end_timestamp'):
		app_data['session_end_timestamp'] = int(time.time())

	print "\n----------------------------ADJUST Session ----------------------------------------"
	adjustSession = adjust_session(campaign_data, app_data, device_data, type='open')
	util.execute_request(**adjustSession)
	app_data['session_start_timestamp'] = int(time.time())


	app_open(campaign_data,app_data,device_data)	

	if app_data.get('registration_flag')==False and random.randint(1,100)<=8:


		time.sleep(random.randint(2,3))
		profile_update(campaign_data,app_data,device_data)
		
		time.sleep(random.randint(20,60))
		registration(campaign_data,app_data,device_data)

		chose=random.randint(1,100)
		if chose <= 35:

			time.sleep(random.randint(2,4))
			reg_male(campaign_data,app_data,device_data)

		else:
			time.sleep(random.randint(4,6))
			reg_female(campaign_data, app_data, device_data)

		app_data['registration_flag']=True



	if app_data.get('registration_flag'):

		if random.randint(1,100)<=70:
			time.sleep(random.randint(60,180))
			live_video(campaign_data, app_data, device_data)
			first_time(campaign_data, app_data, device_data)

		if random.randint(1,100) <= 70:
			time.sleep(random.randint(20,40))
			go_live(campaign_data, app_data, device_data)


	# choice=[1,2]
	# random.shuffle(choice)
	# for i in range(0,len(choice)):
	# 	choose=choice[i]

	# 	if choose==1 and random.randint(1,100)<=60:
	# 		top = random.randint(1,3)
	# 		for i in range(top):
	# 			time.sleep(random.randint(15,60))
	# 			live_video(campaign_data, app_data, device_data)

	# 			if i==0 and app_data.get('first_time')==True:
	# 				time.sleep(random.randint(1,2))
	# 				first_time(campaign_data, app_data, device_data)
	# 				app_data['first_time']=False

	# 	if choose==2 and random.randint(1,100)<=35:
	# 		time.sleep(random.randint(20,40))
	# 		go_live(campaign_data, app_data, device_data)


	time.sleep(random.randint(10,15))
	if app_data.get('registration_flag'):
		purchasee1(campaign_data, device_data,app_data,day)

	app_data['session_end_timestamp'] = int(time.time())

	return {"status" : "true"}


def purchasee1(campaign_data, device_data,app_data,day):
	print '\nADJUST: PURCHASE____________________________________'
	if purchase.isPurchase(app_data,day,advertiser_demand=10):
		flag = 1
		randompurchase = random.randint(1,100)
		if randompurchase <= 80:
			num = "1"
		elif randompurchase > 80 and randompurchase <=95:
			num = "2"
		elif randompurchase > 95 and randompurchase <=100:
			num = "3"
		else:
			flag = 0				
		if flag == 1:
			print '-----------------------ADJUST Event-----------------------------'
			adjust_event_req=adjust_event(campaign_data, app_data, device_data,'kertil',revenue=campaign_data.get('purchase').get(num))
			util.execute_request(**adjust_event_req)
			

	

def app_open(campaign_data, app_data, device_data):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'g98qjg')
	util.execute_request(**adjust_event_req)

def registration(campaign_data, app_data, device_data):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'fzqrno')
	util.execute_request(**adjust_event_req)

def reg_male(campaign_data, app_data, device_data):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'yftp1o',t1=0,t2=2)
	util.execute_request(**adjust_event_req)

def reg_female(campaign_data, app_data, device_data):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'6pahhp',t1=0,t2=2)
	util.execute_request(**adjust_event_req)

def profile_update(campaign_data, app_data, device_data):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'ic3vgu',t1=0,t2=2)
	util.execute_request(**adjust_event_req)

def go_live(campaign_data, app_data, device_data):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'p13knn')
	util.execute_request(**adjust_event_req)

def live_video(campaign_data, app_data, device_data):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'7iipq2')
	util.execute_request(**adjust_event_req)

def first_time(campaign_data, app_data, device_data):
	print '-----------------------ADJUST Event-----------------------------'
	adjust_event_req=adjust_event(campaign_data, app_data, device_data,'f2edag')
	util.execute_request(**adjust_event_req)
			
			
def apple_receipt():
	return ''.join(random.choice(string.digits + string.ascii_letters+ '+\/' ) for _ in range(random.randint(6000,7000)))

# def meet_me_1(campaign_data, app_data, device_data):

# 	if not app_data.get('memory_used'):
# 		make_memory_used(app_data)
		
# 	if not app_data.get('x_device_id'):
# 		make_x_device_id(app_data)
	
# 	url = 'http://api.meetme.com/mobile/settings'
# 	headers = {
# 				'X-SupportedFeatures': 'chatSuggestions,messageStickers,StackedNotifications:v5,tags,purchaseRevamp,freeTrial,strictHttps,twoStepRegistration,realtimeAtLogin,meetQueueSayHi,MediaLinkMessages:v1,liveVideo',
# 				'X-Device': 'iphone,'+app_data.get('x_device_id')+','+campaign_data.get('app_version_code')+':'+campaign_data.get('app_version_code'),
# 				'X-NotificationTypes': 'friendAccept,newMatch,boostChat,smileSent,newMemberAlert',
# 				'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
# 				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')+'-'+device_data.get('locale').get('country'),
# 				'X-Stats': urllib.quote(json.dumps({"memUsed":str(app_data.get('memory_used')),"memTotal":"923947008","methods":[]}))
# 		}
		
# 	method = 'get'
	
# 	if not device_data.get('dpi'):
# 		dpi = '320'
# 	else:
# 		dpi = device_data.get('dpi')
		
	
# 	params = {	
# 				'dpi': dpi
# 	}

# 	data = None
	
# 	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
# def meet_me_registration(campaign_data, app_data, device_data):

# 	if not app_data.get('memory_used'):
# 		make_memory_used(app_data)
		
# 	if not app_data.get('x_device_id'):
# 		make_x_device_id(app_data)
	
# 	if not app_data.get('user_info'):
# 		return_userinfo(app_data)
		
# 	if not all([app_data.get('lat'),app_data.get('lon')]):
# 		app_data['lat'] = float("{0:.2f}".format(random.uniform(27,40)))
# 		app_data['lon'] = float("{0:.2f}".format(random.uniform(70,80)))
	
# 	url = 'http://ssl.meetme.com/mobile/registration'
# 	headers = {
# 				'X-Device': 'iphone,'+app_data.get('x_device_id')+','+campaign_data.get('app_version_code')+':'+campaign_data.get('app_version_code'),
# 				'X-NotificationTypes': 'friendAccept,newMatch,boostChat,smileSent,newMemberAlert',
# 				'Accept-Language': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
# 				'Content-Type': 'application/x-www-form-urlencoded',
# 				'X-Stats': urllib.quote(json.dumps({"memUsed":str(app_data.get('memory_used')),"memTotal":"1634222080","methods":[{"key":"tmProfiling","roundTrip":float("{0:.6f}".format(random.uniform(2,3)))}]})),
# 				'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
# 				'X-SupportedFeatures': 'X-SupportedFeatures: chatSuggestions,messageStickers,StackedNotifications:v5,tags,purchaseRevamp,freeTrial,strictHttps,twoStepRegistration,realtimeAtLogin,meetQueueSayHi,MediaLinkMessages:v1,liveVideo,PictureInPicture:v1,chatGiftMessage',
# 				'Cookie': app_data.get('cookie')+'; mybLocale='+device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
# 		}
	
# 	method = 'post'
	
# 	params = None
	
# 	if app_data.get('user_info').get('gender') == 0:
# 		gen_m = 'male'
# 	else:
# 		gen_m = 'female'

# 	data = {
# 				'conversionTrackingId': 'tune:5DFE93FF-8A35-433E-9705-097089878C3A,adjust:'+app_data.get('adjust_adid'),
# 				'dob': app_data.get('user_info').get('dob'),
# 				'emailId': app_data.get('user_info').get('email'),
# 				'firstName': app_data.get('user_info').get('f_name'),
# 				'formType': 'two_step_reg',
# 				'gender':gen_m,
# 				'lat': app_data.get('lat'),
# 				'long': app_data.get('lon'),
# 				'password': app_data.get('user_info').get('password'),
# 				'sessionId': app_data.get('session_id') if app_data.get('session_id') else make_session_id(app_data),
# 				'sessionState': 1,
# 				'skipResponseKeys': 'targeting',
# 				'systemInfo': json.dumps({"hardwareVersion":device_data.get('device_platform'),"osVersion":device_data.get('os_version'),"connectionType":"WiFi"})
# 	}
	
# 	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
# ###################################################
# 													#
# 				MAT Funcation 	 					#
# 													#
# ###################################################

def mat_serve(campaign_data, app_data, device_data,action,log=False,log1=False,site_event_name_val=False):

	url = 'http://%s.engine.mobileapptracking.com/serve' % campaign_data.get('mat').get('advertiser_id')
	headers = {
				'Accept-Encoding':'gzip, deflate',
				'Content-Type': 'application/json',
				'Accept-Language':device_data.get('locale').get('language').lower()+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+str(device_data.get('cfnetwork')).split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	method = 'post'
	params = {
				'action':action,
				'advertiser_id':campaign_data.get('mat').get('advertiser_id'),
				'package_name':campaign_data.get('package_name'),
				'response_format':'json',
				'sdk':campaign_data.get('mat').get('sdk'),
				'transaction_id':str(uuid.uuid4()).upper(),
				'ver':campaign_data.get('mat').get('ver'),
				'sdk_retry_attempt':'0',
		}
		
	'''if action == 'install':
		params['post_conversion'] = 1'''
		
	# if action == 'conversion' and flag == 0:
		# params['site_event_name'] = 'registration'
	# if action == 'conversion' and flag == 1:
		# params['site_event_name'] = 'reg_male'
		
	if action == 'conversion':
		params['site_event_name']=site_event_name_val
			
	if not device_data.get('dpi'):
		dpi = '326'
	else:
		dpi = device_data.get('dpi')
		
		
		
	if not app_data.get('insdate'):
		app_data['insdate'] = str(int(time.time())-100)

	if not app_data.get('mat_id'):
		app_data['mat_id'] = str(uuid.uuid4())
	if not app_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()	
	if not app_data.get('idfv_id'):		
		app_data['idfv_id'] = str(uuid.uuid4()).upper()


	data_value = {
			'app_name':campaign_data.get('app_name'),
			'app_version':campaign_data.get('app_version_code'),
			'app_version_name' : campaign_data.get('app_version_name'),
			'build':device_data.get('build'),
			'bundle_id' : campaign_data.get('package_name'),
			'conversion_user_agent':device_data.get('User-Agent'),
			'country_code':device_data.get('locale').get('country').upper(),
			'currency_code':'USD',
			'device_brand':'Apple',
			'device_carrier':device_data.get('carrier'),
			'device_cpu_subtype':1,
			'device_cpu_type':'16777228',
			'device_model':device_data.get('device_platform'),
			#'existing_user' : 1,
			'insdate':app_data.get('insdate'),
			'ios_ad_tracking':1,
			'ios_ifa':app_data.get('idfa_id'),
			'ios_ifv':app_data.get('idfv_id'),
			'ios_purchase_status':'-192837465',
			'is_testflight_build' : 0,
			'language':device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country'),
			'level':0,
			'locale':device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country'),
			'mat_id':app_data.get('mat_id'),
			'os_jailbroke':0,
			'os_version':device_data.get('os_version'),
			'package_name':campaign_data.get('package_name'),
			'quantity':0,
			'rating':0,
			'revenue':0,
			'screen_density':util.getdensity(dpi),
			'screen_size':device_data.get('resolution'),
			'system_date':int(time.time()),
			'location_auth_status' : '2'
			
		}

	# if all([app_data.get('lat'),app_data.get('lon')]):
	# 	data_value['altitude'] = float(app_data.get('lat'))+float(app_data.get('lon'))
		
	if(log):
		data_value['latitude'] = app_data.get('lat')
		data_value['location_auth_status'] = 2
		data_value['location_horizontal_accuracy'] = 65
		data_value['location_timestamp'] = str(int(time.time()))
		data_value['location_vertical_accuracy'] = 10
		data_value['longitude'] = app_data.get('lon')

	if all([app_data.get('open_log_id'),app_data.get('last_open_log_id')]):
		data_value['open_log_id'] = str(app_data['open_log_id'])
		data_value['last_open_log_id'] = str(app_data['last_open_log_id'])
		data_value['location_auth_status'] = 2
		
	if action == 'conversion':
		if app_data.get('user_info').get('gender') == 'male':
			gen_m = 0
		else:
			gen_m = 1
		
		data_value['gender'] = gen_m
		
		
	da_str = urllib.urlencode(data_value)

	key = campaign_data.get('mat').get('key')
	data_str = encrypt_AES(key,da_str)
	
	params['data'] = data_str
	
	data = '{}'
		
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}



##########################adjust#$$$$$$$$$$$$###################

def adjust_session(campaign_data, app_data, device_data,type='install'):
	url = 'http://app.adjust.com/session'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
	}
	
	params={}

	data = {
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			# 'ios_uuid':	app_data.get('ios_uuid'),
			'os_build':device_data.get('build'),
			# 'queue_size':	1,
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'sent_at':	datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'session_count': make_session_count(app_data,device_data),
			'tracking_enabled':	1,
			# 'queue_size':'1',
			'connectivity_type':'2',
			'install_receipt':app_data.get('install_receipt'),
			'installed_at':datetime.datetime.utcfromtimestamp( app_data.get('times').get('install_complete_time') +app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'mcc' : device_data.get('mcc'),
			'mnc' : device_data.get('mnc'),
			'network_type' : 'CTRadioAccessTechnologyLTE'
			}
	
	if type == "open":
		if not app_data.get('time_passes'):
			app_data['time_passes'] = int(time.time()*1000)
		
		data['last_interval'] = int(time.time())-app_data.get('session_end_timestamp')

		data['subsession_count'] = '1'
		
		# a = random.randint(20,35)
		# data['session_length'] = a
		# data['time_spent'] = a

		data['session_length'] = app_data.get('session_end_timestamp') - app_data.get('session_start_timestamp')
		data['time_spent'] = app_data.get('session_end_timestamp') - app_data.get('session_start_timestamp')

		data['tce'] = 1

	headers['Authorization']=get_signature(campaign_data, app_data, device_data,app_secret=str(campaign_data.get('adjust').get('secret_key')),action='session',idfa=str(data.get('idfa')),created_at=str(data.get('created_at')),)

		
	
	app_data['time_passes'] = int(time.time()*1000)

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adjust_sdkclick(campaign_data, app_data, device_data):
	url = 'http://app.adjust.com/sdk_click'
	method = 'post'
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

	}
	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	# if not app_data.get('event_count'):
	# 	app_data['event_count'] = 1
	# else:
	# 	app_data['event_count'] += 1

	params={}

	data = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'app_updated_at':campaign_data.get('adjust').get('app_updated_at')+device_data.get('timezone'),
			'app_version':	campaign_data.get('app_version_code'),
			'app_version_short': campaign_data.get('app_version_name'),
			'attribution_deeplink':	1,
			'bundle_id': campaign_data.get('package_name'),
			'country':device_data.get('locale').get('country').upper(),
			'cpu_type':	device_data.get('cpu_type'),
			'created_at': datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'device_name': device_data.get('device_platform'),
			'device_type': device_data.get('device_type'),
			'environment': 'production',
			'event_buffering_enabled': 0,
			'hardware_name': device_data.get('hardware'),
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			# 'ios_uuid':	app_data.get('ios_uuid'),
			'language':	device_data.get('locale').get('language'),
			'needs_response_details': 1,
			'os_name': 'ios',
			'os_version': device_data.get('os_version'),
			'persistent_ios_uuid': app_data.get('ios_uuid'),
			'sent_at':	datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'session_count': app_data.get('session_count'),
			'tracking_enabled':	1,
			# 'queue_size':'1',
			'source': 'iad3',
			'connectivity_type':'2',
			'os_build':device_data.get('build'),
			'details':	json.dumps({"Version3.1":{"iad-attribution":"false"}}),
			'installed_at':datetime.datetime.utcfromtimestamp(( app_data.get('times').get('install_complete_time') )+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'last_interval':'2',
			'session_length':int(time.time())-app_data.get('session_start_timestamp'),
			'subsession_count':'1',
			'time_spent':int(time.time())-app_data.get('session_start_timestamp'),
			'install_receipt':app_data.get('install_receipt'),
			'mcc' : device_data.get('mcc'),
			'mnc' : device_data.get('mnc'),
			'network_type' : 'CTRadioAccessTechnologyLTE'
			}
	headers['Authorization']=get_signature(campaign_data, app_data, device_data,app_secret=str(campaign_data.get('adjust').get('secret_key')),action='click',idfa=str(data.get('idfa')),created_at=str(data.get('created_at')),)

	

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
def adjust_attribution(campaign_data, app_data, device_data,t1=0,t2=0):
	url = 'http://app.adjust.com/attribution'
	method = 'head'
	headers = {
		'Client-Sdk': campaign_data.get('adjust').get('sdk'),
		'Accept': '*/*',
		'User-Agent': campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding': 'br, gzip, deflate',
	}

	created_at=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	time.sleep(random.randint(t1,t2))

	sent_at= datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	
	data={}

	params = {
			
			'app_token': campaign_data.get('adjust').get('app_token'),
			'attribution_deeplink' : 1,
			'created_at': datetime.datetime.utcfromtimestamp(time.time()+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			'environment' : 'production',
			'event_buffering_enabled': 0,
			'idfa': app_data.get('idfa_id'),
			'idfv':	app_data.get('idfv_id'),
			'needs_response_details': 1,
			'initiated_by':	'backend',
			'sent_at': datetime.datetime.utcfromtimestamp(time.time()+random.uniform(0,1)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone'),
			}

	headers['Authorization']=get_signature(campaign_data, app_data, device_data,app_secret=str(campaign_data.get('adjust').get('secret_key')),action='attribution',idfa=str(data.get('idfa')),created_at=str(data.get('created_at')),)
	
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		
	
def adjust_event(campaign_data, app_data, device_data,event_token,revenue='0',t1=0,t2=0):
		
	url = 'http://app.adjust.com/event'
	method = 'post'
	header = {
				'Client-Sdk':campaign_data.get('adjust').get('sdk'),
				'Accept-Encoding':'br, gzip, deflate',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1]
			}

	if not app_data.get('ios_uuid'):
		app_data['ios_uuid'] = str(uuid.uuid4())

	if not app_data.get('event_count'):
		app_data['event_count'] = 1
	else:
		app_data['event_count'] += 1

	created_at=datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	time.sleep(random.randint(t1,t2))

	sent_at= datetime.datetime.utcfromtimestamp((time.time())+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')

	data = {
			'hardware_name':device_data.get('hardware'),	
			'event_buffering_enabled':	0,
			'cpu_type':device_data.get('cpu_type'),
			'attribution_deeplink': '1',
			'install_receipt':app_data.get('install_receipt'),
			'persistent_ios_uuid':app_data.get('ios_uuid'),
			'os_name':'ios',
			'environment':'production',
			'needs_response_details':1,
			'event_count':app_data.get('event_count'),			
			'session_count':'1',
			'app_version_short':campaign_data.get('app_version_name'),
			'device_type':device_data.get('device_type'),
			'created_at': created_at,
			'event_token':event_token,
			'bundle_id':campaign_data.get('package_name'),
			'subsession_count':'1',
			'os_version':device_data.get('os_version'),
			'app_version': campaign_data.get('app_version_code'),
			'country':device_data.get('locale').get('country'),
			'language':	device_data.get('locale').get('language'),
			'idfa':app_data.get('idfa_id'),
			'idfv':app_data.get('idfv_id'),
			'session_length':int(time.time())-app_data.get('session_start_timestamp'),
			'time_spent':int(time.time())-app_data.get('session_start_timestamp'),
			'app_token':campaign_data.get('adjust').get('app_token'),
			'tracking_enabled': 1,
			'device_name':device_data.get('device_platform'),
			'sent_at': sent_at,
			'os_build':	device_data.get('build'),		
			'connectivity_type':'2',	
			#'install_receipt': app_data.get('receipt_id')
			'mcc' : device_data.get('mcc'),
			'mnc' : device_data.get('mnc'),
			'network_type' : 'CTRadioAccessTechnologyLTE'
			}
	# if event_token=='g98qjg':
	# 	data['queue_size']=1

	if event_token== 'kertil':
		data['revenue']= revenue
		data['currency']= 'USD'

	header['Authorization']=get_signature(campaign_data, app_data, device_data,app_secret=str(campaign_data.get('adjust').get('secret_key')),action='event',idfa=str(data.get('idfa')),created_at=str(data.get('created_at')),)
		
		
	return {'url': url, 'httpmethod': method, 'headers': header, 'params':None, 'data': data}

def devicecheckin(app_data,campaign_data,device_data):
	url='https://device-provisioning.googleapis.com/checkin'
	method='post'
	headers={
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
		'Content-Type': 'application/json',

	}
	data={"locale":device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').upper(),"digest":"","checkin":{"iosbuild":{"model":device_data.get('device_platform'),"os_version":"IOS_"+device_data.get('os_version')},"last_checkin_msec":0,"user_number":0,"type":2},"user_serial_number":0,"id":0,"timezone":device_data.get('local_tz_name'),"logging_id":200875988,"version":2,"security_token":0,"fragment":0}
	params=None

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':None, 'data': json.dumps(data)}

def meetmesetting(app_data,campaign_data,device_data):
	url='https://api.meetme.com/mobile/settings'
	method='get'
	headers={
		'Accept-Encoding': 'gzip',
		'Accept-Language': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper(),
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/'+util.getdensity(device_data.get('dpi'))+'0',

		# 'Content-Type': 'application/json',
		'X-Device': 'iphone,'+str(uuid.uuid4()).upper()+','+campaign_data.get('app_version_code')+':'+campaign_data.get('app_version_code'),
		'X-NotificationTypes': 'friendAccept,newMatch,boostChat,smileSent,newMemberAlert',
		'X-Stats':urllib.quote('{"memUsed":"1653719040","memTotal":"1791246336","methods":[]}'),
		'X-SupportedFeatures': 'chatSuggestions,messageStickers,StackedNotifications:v6,tags,purchaseRevamp,freeTrial,strictHttps,twoStepRegistration,realtimeAtLogin,meetQueueSayHi,MediaLinkMessages:v1,liveVideo,PictureInPicture:v1,chatGiftMessage,QuickChat:v1',
		


	}
	data=None
	params={
		'dpi':device_data.get('dpi'),
	}

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':params, 'data': data}

def meetmemobilelogin(app_data,campaign_data,device_data):
	url='https://ssl.meetme.com/mobile/login'
	method='post'
	headers={
		'Accept-Encoding': 'gzip',
		'Accept-Language': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper(),
		'User-Agent':campaign_data.get('app_name')+'/'+campaign_data.get('app_version_name')+' (iPhone; iOS '+device_data.get('os_version')+'; Scale/2.00)',
		
		# 'Content-Type': 'application/json',
		'X-Device': 'iphone,'+str(uuid.uuid4()).upper()+','+campaign_data.get('app_version_code')+':'+campaign_data.get('app_version_code'),
		'X-NotificationTypes': 'friendAccept,newMatch,boostChat,smileSent,newMemberAlert',
		'X-Stats':urllib.quote('{"memUsed":"1463369728","memTotal":"1536212992","methods":[]}'),
		'X-SupportedFeatures': 'chatSuggestions,messageStickers,StackedNotifications:v6,tags,purchaseRevamp,freeTrial,strictHttps,twoStepRegistration,realtimeAtLogin,meetQueueSayHi,MediaLinkMessages:v1,liveVideo,PictureInPicture:v1,chatGiftMessage,QuickChat:v1',
		'Cookie':'PHPSESSID='+str(output)+'; mybLocale='+device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').upper()

	}
	data={	'emailId'	:app_data.get('user_info').get('email'),
			'lat'	:app_data.get('lat'),
			'long'	:app_data.get('lon'),
			'password'	:app_data.get('user_info').get('password'),
			'skipResponseKeys':	'targeting',
			'systemInfo'	:json.dumps({"hardwareVersion":device_data.get('device_platform'),"osVersion":device_data.get('os_version'),"connectionType":"WiFi"}),
		}
	params=None

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params':params, 'data': data}

def current_location(device_data):
	url='http://lumtest.com/myip.json'
		
	return {'url': url, 'httpmethod': 'get', 'headers': None, 'params': None, 'data': None}





# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = device_data.get("device_id", str(int(time.time()*1000)))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def install_receipt(app_data):
	if not app_data.get('install_receipt'):
		app_data['install_receipt']= 'MIISogYJKoZIhvcNAQcCoIISkzCCEo8CAQExCzAJBgUrDgMCGgUAMIICQwYJKoZIhvcNAQcBoIICNASCAjAxggIsMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgCNMA0CAQoCAQEEBRYDMTcrMA0CAQsCAQEEBQIDA5KHMA0CAQ0CAQEEBQIDAdWIMA4CAQECAQEEBgIEFjYr0DAOAgEJAgEBBAYCBFAyNTIwDgIBEAIBAQQGAgQxjPbdMBACAQ8CAQEECAIGPeH5mx0YMBICARMCAQEECgwIMTIuNS4wLjEwEwIBAwIBAQQLDAkxMy4xOC4wLjAwFAIBAAIBAQQMDApQcm9kdWN0aW9uMBgCAQQCAQIEEFR13vg5hqa3v3gyvFdNJC8wHAIBBQIBAQQUfjvlM7wLKIJHcgMiTHP3ZYWoaS4wHgIBCAIBAQQWFhQyMDE5LTA1LTE3VDA1OjE3OjMzWjAeAgEMAgEBBBYWFDIwMTktMDUtMTdUMDU6MTc6MzRaMB4CARICAQEEFhYUMjAxNy0xMC0yM1QxMTo1MTowOFowIwIBAgIBAQQbDBljb20ubXlZZWFyYm9vay5NeVllYXJib29rMEACAQcCAQEEOJIAWL1FGaWtQ8UNEJKg8D1lWcx2VWPw85ZvAMIRWZylrd/xUHUMeOqau7xAXaHDycDwOIn45HIhMFYCAQYCAQEETh2BKeH2YbavZpaAyuxwDT14PDsYpL7S1A+ugBBsItL23+wmBOGI79zO5NhtVPZTwTOVUohzvRqx7XbZvTrd2gDdK5QX2Qp9JeNfyYB7OaCCDmUwggV8MIIEZKADAgECAggO61eH554JjTANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xNTExMTMwMjE1MDlaFw0yMzAyMDcyMTQ4NDdaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClz4H9JaKBW9aH7SPaMxyO4iPApcQmyz3Gn+xKDVWG/6QC15fKOVRtfX+yVBidxCxScY5ke4LOibpJ1gjltIhxzz9bRi7GxB24A6lYogQ+IXjV27fQjhKNg0xbKmg3k8LyvR7E0qEMSlhSqxLj7d0fmBWQNS3CzBLKjUiB91h4VGvojDE2H0oGDEdU8zeQuLKSiX1fpIVK4cCc4Lqku4KXY/Qrk8H9Pm/KwfU8qY9SGsAlCnYO3v6Z/v/Ca/VbXqxzUUkIVonMQ5DMjoEC0KCXtlyxoWlph5AQaCYmObgdEHOwCl3Fc9DfdjvYLdmIHuPsB8/ijtDT+iZVge/iA0kjAgMBAAGjggHXMIIB0zA/BggrBgEFBQcBAQQzMDEwLwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcjA0MB0GA1UdDgQWBBSRpJz8xHa3n6CK9E31jzZd7SsEhTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHgYDVR0gBIIBFTCCAREwggENBgoqhkiG92NkBQYBMIH+MIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDYGCCsGAQUFBwIBFipodHRwOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQANphvTLj3jWysHbkKWbNPojEMwgl/gXNGNvr0PvRr8JZLbjIXDgFnf4+LXLgUUrA3btrj+/DUufMutF2uOfx/kd7mxZ5W0E16mGYZ2+FogledjjA9z/Ojtxh+umfhlSFyg4Cg6wBA3LbmgBDkfc7nIBf3y3n8aKipuKwH8oCBc2et9J6Yz+PWY4L5E27FMZ/xuCk/J4gao0pfzp45rUaJahHVl0RYEYuPBX/UIqc9o2ZIAycGMs/iNAGS6WGDAfK+PdcppuVsq1h1obphC9UynNxmbzDscehlD86Ntv0hgBgw2kivs3hi1EdotI9CO/KBpnBcbnoB7OUdFMGEvxxOoMIIEIjCCAwqgAwIBAgIIAd68xDltoBAwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTEzMDIwNzIxNDg0N1oXDTIzMDIwNzIxNDg0N1owgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjgaYwgaMwHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwDgYDVR0PAQH/BAQDAgGGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQBPz+9Zviz1smwvj+4ThzLoBTWobot9yWkMudkXvHcs1Gfi/ZptOllc34MBvbKuKmFysa/Nw0Uwj6ODDc4dR7Txk4qjdJukw5hyhzs+r0ULklS5MruQGFNrCk4QttkdUGwhgAqJTleMa1s8Pab93vcNIx0LSiaHP7qRkkykGRIZbVf1eliHe2iK5IaMSuviSRSqpd1VAKmuu0swruGgsbwpgOYJd+W+NKIByn/c4grmO7i77LpilfMFY0GCzQ87HUyVpNur+cmV6U/kTecmmYHpvPm0KdIBembhLoz2IYrF+Hjhga6/05Cdqa3zr/04GpZnMBxRpVzscYqCtGwPDBUfMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCA7rV4fnngmNMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEAj/Q/u42Ds24hP5Y0NovkB/6ZKOOy349R06kSG9Da4VHQAcJtVTbjZQnOLN4Khex36WFsDBY6YoAm9VMRU1qWUhHb/bWulBCoIIz29i0zZ9sh5wJ9oAroDKzHhSioOnM3WaEYVSGpU6o3v4qbpegIG9bEELSc2fJZ0SgjD/Me9h/yElpah+qq60NXkUInZEJeSjg8QLRKcNgESfIKn2UEe4/h/JTkUdwURuEmuazKuuUfGjLLpeGQQxpJ2hh3UcbQvuMZ/m9B70x4qRmfGAsbxtAmKxgHSXxPNYEWmZtKWhc/l9CWJAy7J/B34iDbwULktylXH0X1cEarU4PfuNrGLQ=='

def get_signature(campaign_data, app_data, device_data,app_secret=False,action=False,idfa=False,created_at=False):
	auth= 'Signature secret_id="'+str(campaign_data.get('adjust').get('secret_id'))+'",signature="'+util.sha256(str(app_secret)+str(action)+str(idfa)+str(created_at))+'",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'
	return auth
