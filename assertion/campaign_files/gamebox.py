import json, uuid, random, time, string, datetime
from sdk import util,installtimenew
from sdk import getsleep
import clicker,Config

campaign_data = {
	'app_size'    : 9.1, #5.2
	'package_name': 'com.globocom.globocompapp',	
	'app_version_name':'4.1',#'3.9', #3.4
	'app_version_code': '251',#'249', #194
	'app_name' : 'GameZine',
	'supported_countries': 'WW',
	'supported_os':'4.4',
	'no_referrer' : False,
	'ctr'                : 6,
	'CREATE_DEVICE_MODE' : 2,
	'device_targeting'   : True,
	'tracker' : 'kochava',
	'kochava':{
		'sdk_version' : 'AndroidTracker 3.5.0',
		'sdk_protocol': '11',
		'kochava_app_id':'kogame-zine-3aqnbtw',
		'sdk_build_date' : '2018-10-31T20:43:48Z',
	},
	'api_branch':{
	    'branchkey'  :'key_live_kiPuschHv2ZsTyK37Mz69ghltEpbbmex',
	    'sdk'        :'android3.2.0',
	    },
	'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention':{
							1:50,
							2:48,
							3:46,
							4:44,
							5:40,
							6:38,
							7:35,
							8:32,
							9:31,
							10:30,
							11:29,
							12:28,
							13:27,
							14:26,
							15:25,
							16:24,
							17:23,
							18:22,
							19:21,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
	}
}
	
def install(app_data, device_data):
	print '---------INSTALLING---------------'
	installtimenew.main(app_data,device_data,app_size=5.2,os='android')
	app_data['click_time'] = int(app_data.get('times').get('click_time'))
	app_data['install_begin_time'] = int(app_data.get('times').get('download_end_time'))
	time.sleep(random.randint(15,30))

	set_ua(app_data,device_data)
	app_data['battery']=str(random.randint(5,95))

	# random_id(app_data)
	def_nt_id(app_data)
	if not app_data.get('kochava_device_id'):
		app_data['kochava_device_id'] ='KA'+str(random.randint(100,999))+str(int(time.time()))+str(uuid.uuid4()).replace('-','')
	t=int(time.time())
	
	
	if not app_data.get('user_id'):
		app_data['user_id'] = str(uuid.uuid4())

	app_data['msisdn'] = 	'7911'+util.get_random_string('decimal',5)

	app_data['call_run_time']=time.time()

	##################################################
	#     CALLS
	##################################################
	print 'kochava init'
	kochava_init= android_Kvinit(campaign_data, app_data, device_data)
	app_data['api_hit_time']=time.time()
	util.execute_request(**kochava_init)

	time.sleep(random.randint(1,2))
	
	print 'kochava tracker'
	kochava_tracker_1=android_kvTracker(campaign_data, app_data, device_data)
	util.execute_request(**kochava_tracker_1)

	print 'Event'
	event_data = {"name":"KCH_EVENT_KPI_APP_FIRST_OPEN","user_id":app_data.get('user_id')}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_APP_FIRST_OPEN")
	util.execute_request(**kochava_tracker_1)

	time.sleep(random.randint(5,10))

	print "\nandroid_kvQuery\n"
	request=android_kvQuery( campaign_data, device_data, app_data )
	util.execute_request(**request)

	time.sleep(random.randint(0,1))
	print "Branch______install"
	request=api_branch_install(campaign_data,app_data,device_data)
	result=util.execute_request(**request)
	try:
		json_result=json.loads(result.get('data'))
		app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
		app_data['identity_id']=json_result.get('identity_id')
		app_data['session_id']=json_result.get('session_id')
	except:
		app_data['device_fingerprint_id1']='613284344550945604'
		app_data['identity_id']='641581897171978611'
		app_data['session_id']='641581897189090110'

	time.sleep(random.randint(1,5))
	print 'Branch profile'
	req = api2_branch_profile( campaign_data, device_data, app_data ,identity='')
	util.execute_request(**req)

	if random.randint(1,100)<=95:
		time.sleep(random.randint(5,15))
		print 'Event'
		event_data = {"name":"KCH_EVENT_KPI_MOBILE_NUMBER_ENTERED","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"valid":"true","api_attempt":"1"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_MOBILE_NUMBER_ENTERED")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,2))
		custom_data={"flow_attempt": "1","msisdn": app_data.get('msisdn'),	"valid": "true","api_attempt": "1"}
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_MOBILE_NUMBER_ENTERED', custom_data=custom_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data = {"name":"KCH_EVENT_KPI_SUB_STATUS_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"url":"http://wap.globocom.info/mglobopay/getGlobalSubscriberStatus/"+app_data.get('msisdn')+"","api_attempt":"1"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SUB_STATUS_REQUEST")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,2))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_SUB_STATUS_REQUEST')
		util.execute_request(**request)

		time.sleep(random.randint(10,30))
		print 'Event'
		event_data = {"name":"KCH_EVENT_KPI_SUB_STATUS_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"isactive":"false","status":"inactive","url":"http://wap.globocom.info/mglobopay/getGlobalSubscriberStatus/"+app_data.get('msisdn')+"","api_attempt":"1"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SUB_STATUS_RESPONSE")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,2))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_SUB_STATUS_RESPONSE')
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={"name":"KCH_EVENT_KPI_HLR_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"hlrurl":"https://wap.globocom.info/mglobopay/getHlrLookUpByMdn/"+app_data.get('msisdn')+"","api_attempt":"1"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_HLR_REQUEST")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,2))
		custom_data={"flow_attempt": "1","msisdn": "27"+app_data.get('msisdn'),"hlrurl": "https://wap.globocom.info/mglobopay/getHlrLookUpByMdn/27"+app_data.get('msisdn'),"api_attempt": "1"	}
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_HLR_REQUEST', custom_data=custom_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={"name":"KCH_EVENT_KPI_HLR_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"valid_response":"true","response":"du,Dead","hlrurl":"https://wap.globocom.info/mglobopay/getHlrLookUpByMdn/"+app_data.get('msisdn')+"","api_attempt":"1"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_HLR_RESPONSE")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,2))
		custom_data={		"flow_attempt": "1",		"msisdn": "27"+app_data.get('msisdn'),		"valid_response": "true",		"response": device_data.get('carrier')+" ("+device_data.get('carrier')+"),Live",		"hlrurl": "https://wap.globocom.info/mglobopay/getHlrLookUpByMdn/27"+app_data.get('msisdn'),		"api_attempt": "1"	}
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_HLR_RESPONSE', custom_data=custom_data)
		util.execute_request(**request)

		if random.randint(1,100)<=5:
			time.sleep(random.randint(1,5))
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_SMS_READ_CONSENT_SUCCESS_null', custom_data={"flow_attempt":"0"})
			util.execute_request(**request)

			time.sleep(random.randint(1,5))
			print 'Event'
			event_data ={}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SMS_READ_CONSENT_SUCCESS_null")
			util.execute_request(**kochava_tracker_1)



		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_SMS_READ_HASH_SUCCESS', custom_data={"flow_attempt":"0"})
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SMS_READ_HASH_SUCCESS")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(2,4))
		print 'Event'
		event_data = {"name":"KCH_EVENT_KPI_APP_INSTALL_SOURCE","user_id":app_data.get('user_id'),"agency":"empty","campaign_group_name":"empty","install_site":"empty","tracker_id":"empty","network":"empty"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_APP_INSTALL_SOURCE")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_kpi_app_install_source')
		util.execute_request(**request)

		time.sleep(random.randint(2,4))
		print 'Event'
		event_data = {}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_APP_LOADING_TIME")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_APP_LOADING_TIME', custom_data={"loading_time": str(random.randint(10,30))+" seconds"})
		util.execute_request(**request)

		r=random.randint(1,100)

		manual_request=False

		if r<=70:

			print 'Event'
			event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"url":"https://wap.globocom.info/mglobopay/sendOTPWitHLRForStaging","api_attempt":"1"}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_REQUEST")
			util.execute_request(**kochava_tracker_1)

			time.sleep(random.randint(1,2))
			custom_data={		"flow_attempt": "1",		"msisdn":"27"+app_data.get('msisdn'),		"url": "http://wap.globocom.info/mglobopay/PIN-GENERATION?serviceId=246&ac_country_code=za&operatorName="+device_data.get('carrier')+"&msisdn=27"+app_data.get('msisdn')+"&androidId="+device_data.get('android_id'),		"api_attempt": "1"	}
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_PIN_GENERATE_REQUEST', custom_data=custom_data)
			util.execute_request(**request)			

		else:				

			time.sleep(random.randint(4,7))
			print 'Event'
			event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_MANUAL_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"operator_name":"Du","operator_code":"2009","url":"https://wap.globocom.info/mglobopay/resendOTPForStaging","api_attempt":"1"}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_MANUAL_REQUEST")
			util.execute_request(**kochava_tracker_1)

			time.sleep(random.randint(1,2))
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_PIN_GENERATE_MANUAL_REQUEST')
			util.execute_request(**request)

			manual_request=True

		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_SMS_READ_CONSENT_SUCCESS_47909', custom_data={"flow_attempt":"1"})
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SMS_READ_CONSENT_SUCCESS_47909")
		util.execute_request(**kochava_tracker_1)

		for i in range(1,10):
			r= random.randint(1,100)

			if r<=(50-5*i):
				time.sleep(random.randint(1,5))
				print '\nBranch : custom event____________________________________'
				request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_RETRY_URL_COUNT_'+str(i), custom_data={"url":"http://api.globoapps.in/app_json/com.globocom.globocompapp.bt.txt","retryCount":str(i),"maxRetries":"10"})
				util.execute_request(**request)

				time.sleep(random.randint(1,5))
				print 'Event'
				event_data ={}
				kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName='KCH_EVENT_KPI_RETRY_URL_COUNT_'+str(i))
				util.execute_request(**kochava_tracker_1)

			else:
				break

		if random.randint(1,100)<=45:

			time.sleep(random.randint(1,5))
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_APP_ID_LOOKUP_RESPONSE', custom_data={"error_code":"HTTP 404 ","error_message":"HTTP 404 ","response":"RetrofitError - HTTP 404 ","valid_response":"false","url":"http://api.globoapps.in/app_json/com.globocom.globocompapp.bt.txt"})
			util.execute_request(**request)
			
			time.sleep(random.randint(1,5))
			print 'Event'
			event_data ={}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName='KCH_EVENT_KPI_APP_ID_LOOKUP_RESPONSE')
			util.execute_request(**kochava_tracker_1)

			if random.randint(1,100)<=20:
				time.sleep(random.randint(1,5))
				print '\nBranch : custom event____________________________________'
				request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_kpi_app_exit_AFTER_KPI_APP_ID_LOOKUP_RESPONSE')
				util.execute_request(**request)
				
				time.sleep(random.randint(1,5))
				print 'Event'
				event_data ={		"name": "KCH_EVENT_kpi_app_exit_AFTER_KPI_APP_ID_LOOKUP_RESPONSE",	"user_id": app_data.get('user_id')		},
				kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName='KCH_EVENT_kpi_app_exit_AFTER_KPI_APP_ID_LOOKUP_RESPONSE')
				util.execute_request(**kochava_tracker_1)


		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="COMPLETE_REGISTRATION")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,5))
		request=branch_event_standard( campaign_data, device_data, app_data, eventName='COMPLETE_REGISTRATION' )
		util.execute_request(**request)

		if manual_request==False:
			print 'Event'
			event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"response":"SUCCESS","valid_response":"true","url":"http://wap.globocom.info/mglobopay/duPinGenerateApi?msisdn="+app_data.get('msisdn')+"&partner=globocom","api_attempt":"1"}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_RESPONSE")
			util.execute_request(**kochava_tracker_1)

			time.sleep(random.randint(1,2))		
			custom_data={		"flow_attempt": "1",		"msisdn": "27"+app_data.get('msisdn'),		"response": "SUCCESS",		"valid_response": "true",	"url": "http://wap.globocom.info/mglobopay/PIN-GENERATION?serviceId=246&ac_country_code=za&operatorName="+device_data.get('carrier')+"&msisdn=27"+app_data.get('msisdn')+"&androidId="+device_data.get('android_id'),	"operator_name": "vodacom",		"api_attempt": "1"	}
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_PIN_GENERATE_RESPONSE')
			util.execute_request(**request)

		else:
			time.sleep(random.randint(4,7))

			print 'Event'
			event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_MANUAL_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"response":"SUCCESS","valid_response":"true","operator_name":device_data.get('carrier'),"operator_code":"2009","api_attempt":"2"}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_MANUAL_RESPONSE")
			util.execute_request(**kochava_tracker_1)

			custom_data={		"flow_attempt": "1",		"msisdn": "27"+app_data.get('msisdn'),		"response": "SUCCESS",		"valid_response": "true",	"url": "http://wap.globocom.info/mglobopay/PIN-GENERATION?serviceId=246&ac_country_code=za&operatorName="+device_data.get('carrier')+"&msisdn=27"+app_data.get('msisdn')+"&androidId="+device_data.get('android_id'),	"operator_name": "vodacom",		"api_attempt": "1"	}

			time.sleep(random.randint(1,2))
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_PIN_GENERATE_MANUAL_RESPONSE', custom_data=custom_data)
			util.execute_request(**request)

		app_data['mobile_number_registered']=True

	if app_data.get('mobile_number_registered')==True and random.randint(1,100)<=60:
		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_CHECK_SMS_INBOX', custom_data={	"flow_attempt": "1","msisdn": "27"+app_data.get('msisdn'),"operator_name": device_data.get('carrier'),	"api_attempt": "1"	})
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_CHECK_SMS_INBOX")
		util.execute_request(**kochava_tracker_1)

	if random.randint(1,100)<=40:
		time.sleep(random.randint(10,60))
		print 'Event'
		event_data = {"name":"KCH_EVENT_KPI_APP_MINIMIZED","user_id":app_data.get('user_id')}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_APP_MINIMIZED")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_APP_MINIMIZED')
		util.execute_request(**request)

		if random.randint(1,100)<=75:
			time.sleep(random.randint(10,60))
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_APP_CLOSED')
			util.execute_request(**request)

	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)



	return {'status':True}
	
def open(app_data,device_data,day):

	if not app_data.get('times'):
		print '---------INSTALLING---------------'
		installtimenew.main(app_data,device_data,app_size=5.2,os='android')

	if not app_data.get('kochava_device_id'):
		app_data['kochava_device_id'] ='KA'+str(random.randint(100,999))+str(int(time.time()))+str(uuid.uuid4()).replace('-','')

	def_nt_id(app_data)
	
	if not app_data.get('user_id'):
		app_data['user_id'] = str(uuid.uuid4())

	t=int(time.time())

	if  not app_data.get('msisdn'):
		app_data['msisdn'] = str(random.randint(971527000000,971527999999))	

	app_data['call_run_time']=time.time()

	if not app_data.get('mobile_number_registered'):
		app_data['mobile_number_registered']=False

	##################################################
	#     CALLS
	##################################################
	print 'kochava init'
	kochava_init= android_Kvinit(campaign_data, app_data, device_data)
	app_data['api_hit_time']=time.time()
	util.execute_request(**kochava_init)

	if not app_data.get('device_fingerprint_id1') or not app_data.get('identity_id') or not app_data.get('session_id'):
		time.sleep(random.randint(0,1))
		print "Branch______install"
		request=api_branch_install(campaign_data,app_data,device_data)
		result=util.execute_request(**request)
		try:
			json_result=json.loads(result.get('data'))
			app_data['device_fingerprint_id1']=json_result.get('device_fingerprint_id')
			app_data['identity_id']=json_result.get('identity_id')
			app_data['session_id']=json_result.get('session_id')
		except:
			app_data['device_fingerprint_id1']='613284344550945604'
			app_data['identity_id']='641581897171978611'
			app_data['session_id']='641581897189090110'

	else:

		request=api_branch_open(campaign_data, app_data, device_data)
		util.execute_request(**request)

	if random.randint(1,100)<=85:
		time.sleep(random.randint(5,15))
		print 'Event'
		event_data = {"name":"KCH_EVENT_KPI_MOBILE_NUMBER_ENTERED","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"valid":"true","api_attempt":"1"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_MOBILE_NUMBER_ENTERED")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,2))
		custom_data={"flow_attempt": "1","msisdn": app_data.get('msisdn'),	"valid": "true","api_attempt": "1"}
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_MOBILE_NUMBER_ENTERED', custom_data=custom_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data = {"name":"KCH_EVENT_KPI_SUB_STATUS_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"url":"http://wap.globocom.info/mglobopay/getGlobalSubscriberStatus/"+app_data.get('msisdn')+"","api_attempt":"1"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SUB_STATUS_REQUEST")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,2))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_SUB_STATUS_REQUEST')
		util.execute_request(**request)

		time.sleep(random.randint(10,30))
		print 'Event'
		event_data = {"name":"KCH_EVENT_KPI_SUB_STATUS_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"isactive":"false","status":"inactive","url":"http://wap.globocom.info/mglobopay/getGlobalSubscriberStatus/"+app_data.get('msisdn')+"","api_attempt":"1"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SUB_STATUS_RESPONSE")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,2))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_SUB_STATUS_RESPONSE')
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={"name":"KCH_EVENT_KPI_HLR_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"hlrurl":"https://wap.globocom.info/mglobopay/getHlrLookUpByMdn/"+app_data.get('msisdn')+"","api_attempt":"1"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_HLR_REQUEST")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,2))
		custom_data={"flow_attempt": "1","msisdn": "27"+app_data.get('msisdn'),"hlrurl": "https://wap.globocom.info/mglobopay/getHlrLookUpByMdn/27"+app_data.get('msisdn'),"api_attempt": "1"	}
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_HLR_REQUEST', custom_data=custom_data)
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={"name":"KCH_EVENT_KPI_HLR_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"valid_response":"true","response":"du,Dead","hlrurl":"https://wap.globocom.info/mglobopay/getHlrLookUpByMdn/"+app_data.get('msisdn')+"","api_attempt":"1"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_HLR_RESPONSE")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,2))
		custom_data={		"flow_attempt": "1",		"msisdn": "27"+app_data.get('msisdn'),		"valid_response": "true",		"response": device_data.get('carrier')+" ("+device_data.get('carrier')+"),Live",		"hlrurl": "https://wap.globocom.info/mglobopay/getHlrLookUpByMdn/27"+app_data.get('msisdn'),		"api_attempt": "1"	}
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_HLR_RESPONSE', custom_data=custom_data)
		util.execute_request(**request)

		if random.randint(1,100)<=5:
			time.sleep(random.randint(1,5))
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_SMS_READ_CONSENT_SUCCESS_null', custom_data={"flow_attempt":"0"})
			util.execute_request(**request)

			time.sleep(random.randint(1,5))
			print 'Event'
			event_data ={}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SMS_READ_CONSENT_SUCCESS_null")
			util.execute_request(**kochava_tracker_1)



		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_SMS_READ_HASH_SUCCESS', custom_data={"flow_attempt":"0"})
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SMS_READ_HASH_SUCCESS")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(2,4))
		print 'Event'
		event_data = {"name":"KCH_EVENT_KPI_APP_INSTALL_SOURCE","user_id":app_data.get('user_id'),"agency":"empty","campaign_group_name":"empty","install_site":"empty","tracker_id":"empty","network":"empty"}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_APP_INSTALL_SOURCE")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_kpi_app_install_source')
		util.execute_request(**request)

		time.sleep(random.randint(2,4))
		print 'Event'
		event_data = {}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_APP_LOADING_TIME")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_APP_LOADING_TIME', custom_data={"loading_time": str(random.randint(10,30))+" seconds"})
		util.execute_request(**request)

		r=random.randint(1,100)

		manual_request=False

		if r<=70:

			print 'Event'
			event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"url":"https://wap.globocom.info/mglobopay/sendOTPWitHLRForStaging","api_attempt":"1"}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_REQUEST")
			util.execute_request(**kochava_tracker_1)

			time.sleep(random.randint(1,2))
			custom_data={		"flow_attempt": "1",		"msisdn":"27"+app_data.get('msisdn'),		"url": "http://wap.globocom.info/mglobopay/PIN-GENERATION?serviceId=246&ac_country_code=za&operatorName="+device_data.get('carrier')+"&msisdn=27"+app_data.get('msisdn')+"&androidId="+device_data.get('android_id'),		"api_attempt": "1"	}
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_PIN_GENERATE_REQUEST', custom_data=custom_data)
			util.execute_request(**request)			

		else:				

			time.sleep(random.randint(4,7))
			print 'Event'
			event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_MANUAL_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"operator_name":"Du","operator_code":"2009","url":"https://wap.globocom.info/mglobopay/resendOTPForStaging","api_attempt":"1"}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_MANUAL_REQUEST")
			util.execute_request(**kochava_tracker_1)

			time.sleep(random.randint(1,2))
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_PIN_GENERATE_MANUAL_REQUEST')
			util.execute_request(**request)

			manual_request=True

		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_SMS_READ_CONSENT_SUCCESS_47909', custom_data={"flow_attempt":"1"})
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SMS_READ_CONSENT_SUCCESS_47909")
		util.execute_request(**kochava_tracker_1)

		for i in range(1,10):
			r= random.randint(1,100)

			if r<=(50-5*i):
				time.sleep(random.randint(1,5))
				print '\nBranch : custom event____________________________________'
				request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_RETRY_URL_COUNT_'+str(i), custom_data={"url":"http://api.globoapps.in/app_json/com.globocom.globocompapp.bt.txt","retryCount":str(i),"maxRetries":"10"})
				util.execute_request(**request)

				time.sleep(random.randint(1,5))
				print 'Event'
				event_data ={}
				kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName='KCH_EVENT_KPI_RETRY_URL_COUNT_'+str(i))
				util.execute_request(**kochava_tracker_1)

			else:
				break

		if random.randint(1,100)<=45:

			time.sleep(random.randint(1,5))
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_APP_ID_LOOKUP_RESPONSE', custom_data={"error_code":"HTTP 404 ","error_message":"HTTP 404 ","response":"RetrofitError - HTTP 404 ","valid_response":"false","url":"http://api.globoapps.in/app_json/com.globocom.globocompapp.bt.txt"})
			util.execute_request(**request)
			
			time.sleep(random.randint(1,5))
			print 'Event'
			event_data ={}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName='KCH_EVENT_KPI_APP_ID_LOOKUP_RESPONSE')
			util.execute_request(**kochava_tracker_1)

			if random.randint(1,100)<=20:
				time.sleep(random.randint(1,5))
				print '\nBranch : custom event____________________________________'
				request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_kpi_app_exit_AFTER_KPI_APP_ID_LOOKUP_RESPONSE')
				util.execute_request(**request)
				
				time.sleep(random.randint(1,5))
				print 'Event'
				event_data ={		"name": "KCH_EVENT_kpi_app_exit_AFTER_KPI_APP_ID_LOOKUP_RESPONSE",	"user_id": app_data.get('user_id')		},
				kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName='KCH_EVENT_kpi_app_exit_AFTER_KPI_APP_ID_LOOKUP_RESPONSE')
				util.execute_request(**kochava_tracker_1)


		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="COMPLETE_REGISTRATION")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,5))
		request=branch_event_standard( campaign_data, device_data, app_data, eventName='COMPLETE_REGISTRATION' )
		util.execute_request(**request)

		if manual_request==False:
			print 'Event'
			event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"response":"SUCCESS","valid_response":"true","url":"http://wap.globocom.info/mglobopay/duPinGenerateApi?msisdn="+app_data.get('msisdn')+"&partner=globocom","api_attempt":"1"}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_RESPONSE")
			util.execute_request(**kochava_tracker_1)

			time.sleep(random.randint(1,2))		
			custom_data={		"flow_attempt": "1",		"msisdn": "27"+app_data.get('msisdn'),		"response": "SUCCESS",		"valid_response": "true",	"url": "http://wap.globocom.info/mglobopay/PIN-GENERATION?serviceId=246&ac_country_code=za&operatorName="+device_data.get('carrier')+"&msisdn=27"+app_data.get('msisdn')+"&androidId="+device_data.get('android_id'),	"operator_name": "vodacom",		"api_attempt": "1"	}
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_PIN_GENERATE_RESPONSE')
			util.execute_request(**request)

		else:
			time.sleep(random.randint(4,7))

			print 'Event'
			event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_MANUAL_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"response":"SUCCESS","valid_response":"true","operator_name":device_data.get('carrier'),"operator_code":"2009","api_attempt":"2"}
			kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_MANUAL_RESPONSE")
			util.execute_request(**kochava_tracker_1)

			custom_data={		"flow_attempt": "1",		"msisdn": "27"+app_data.get('msisdn'),		"response": "SUCCESS",		"valid_response": "true",	"url": "http://wap.globocom.info/mglobopay/PIN-GENERATION?serviceId=246&ac_country_code=za&operatorName="+device_data.get('carrier')+"&msisdn=27"+app_data.get('msisdn')+"&androidId="+device_data.get('android_id'),	"operator_name": "vodacom",		"api_attempt": "1"	}

			time.sleep(random.randint(1,2))
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_PIN_GENERATE_MANUAL_RESPONSE', custom_data=custom_data)
			util.execute_request(**request)

		app_data['mobile_number_registered']=True

	if app_data.get('mobile_number_registered')==True and random.randint(1,100)<=40:
		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_CHECK_SMS_INBOX', custom_data={	"flow_attempt": "1","msisdn": "27"+app_data.get('msisdn'),"operator_name": device_data.get('carrier'),	"api_attempt": "1"	})
		util.execute_request(**request)

		time.sleep(random.randint(1,5))
		print 'Event'
		event_data ={}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_CHECK_SMS_INBOX")
		util.execute_request(**kochava_tracker_1)

	if random.randint(1,100)<=30:
		time.sleep(random.randint(10,60))
		print 'Event'
		event_data = {"name":"KCH_EVENT_KPI_APP_MINIMIZED","user_id":app_data.get('user_id')}
		kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_APP_MINIMIZED")
		util.execute_request(**kochava_tracker_1)

		time.sleep(random.randint(1,5))
		print '\nBranch : custom event____________________________________'
		request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_APP_MINIMIZED')
		util.execute_request(**request)

		if random.randint(1,100)<=75:
			time.sleep(random.randint(10,60))
			print '\nBranch : custom event____________________________________'
			request=api_branch_custom(campaign_data, app_data, device_data,ename='KCH_EVENT_KPI_APP_CLOSED')
			util.execute_request(**request)

	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)

	request=api_branch_close(campaign_data, app_data, device_data)
	util.execute_request(**request)


	
	
	return {'status':True}

def test(app_data, device_data):
	print '---------INSTALLING---------------'
	installtimenew.main(app_data,device_data,app_size=5.2,os='android')
	app_data['click_time'] = int(app_data.get('times').get('click_time'))
	app_data['install_begin_time'] = int(app_data.get('times').get('download_end_time'))
	time.sleep(random.randint(15,30))

	set_ua(app_data,device_data)
	app_data['battery']=str(random.randint(5,95))
	random_email(app_data)
	random_id(app_data)
	def_nt_id(app_data)
	if not app_data.get('kochava_device_id'):
		app_data['kochava_device_id'] ='KA'+str(random.randint(100,999))+str(int(time.time()))+str(uuid.uuid4()).replace('-','')
	t=int(time.time())
		
	if not app_data.get('user_id'):
		app_data['user_id'] = str(uuid.uuid4())

	app_data['msisdn'] = str(random.randint(971527000000,971527999999))	

	app_data['call_run_time']=time.time()

	##################################################
	#     CALLS
	##################################################
	print 'kochava init'
	kochava_init= android_Kvinit(campaign_data, app_data, device_data)
	util.execute_request(**kochava_init)

	time.sleep(random.randint(1,2))
	
	print 'kochava tracker'
	kochava_tracker_1=android_kvTracker(campaign_data, app_data, device_data)
	util.execute_request(**kochava_tracker_1)

	print 'Event'
	event_data = {"name":"KCH_EVENT_KPI_APP_FIRST_OPEN","user_id":app_data.get('user_id')}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_APP_FIRST_OPEN")
	util.execute_request(**kochava_tracker_1)

	time.sleep(random.randint(5,10))

	print "\nandroid_kvQuery\n"
	request=android_kvQuery( campaign_data, device_data, app_data )
	util.execute_request(**request)

	time.sleep(random.randint(2,4))

	print 'Event'
	event_data = {"name":"KCH_EVENT_KPI_APP_INSTALL_SOURCE","user_id":app_data.get('user_id'),"agency":"empty","campaign_group_name":"empty","install_site":"empty","tracker_id":"empty","network":"empty"}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_APP_INSTALL_SOURCE")
	util.execute_request(**kochava_tracker_1)

	time.sleep(random.randint(20,30))

	print 'Event'
	event_data = {"name":"KCH_EVENT_KPI_MOBILE_NUMBER_ENTERED","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":"527288656","valid":"true","api_attempt":"1"}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_MOBILE_NUMBER_ENTERED")
	util.execute_request(**kochava_tracker_1)

	print 'Event'
	event_data = {"name":"KCH_EVENT_KPI_SUB_STATUS_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"url":"http://wap.globocom.info/mglobopay/getGlobalSubscriberStatus/"+app_data.get('msisdn')+"","api_attempt":"1"}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SUB_STATUS_REQUEST")
	util.execute_request(**kochava_tracker_1)

	time.sleep(random.randint(10,30))

	print 'Event'
	event_data = {"name":"KCH_EVENT_KPI_SUB_STATUS_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"isactive":"false","status":"inactive","url":"http://wap.globocom.info/mglobopay/getGlobalSubscriberStatus/"+app_data.get('msisdn')+"","api_attempt":"1"}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_SUB_STATUS_RESPONSE")
	util.execute_request(**kochava_tracker_1)

	time.sleep(random.randint(20,30))

	print 'Event'
	event_data ={"name":"KCH_EVENT_KPI_HLR_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"hlrurl":"https://wap.globocom.info/mglobopay/getHlrLookUpByMdn/"+app_data.get('msisdn')+"","api_attempt":"1"}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_HLR_REQUEST")
	util.execute_request(**kochava_tracker_1)

	print 'Event'
	event_data ={"name":"KCH_EVENT_KPI_HLR_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"valid_response":"true","response":"du,Dead","hlrurl":"https://wap.globocom.info/mglobopay/getHlrLookUpByMdn/"+app_data.get('msisdn')+"","api_attempt":"1"}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_HLR_RESPONSE")
	util.execute_request(**kochava_tracker_1)

	print 'Event'
	event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"url":"https://wap.globocom.info/mglobopay/sendOTPWitHLRForStaging","api_attempt":"1"}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_REQUEST")
	util.execute_request(**kochava_tracker_1)

	print 'Event'
	event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"response":"OTP_GENERATION_FAIL","valid_response":"false","url":"http://wap.globocom.info/mglobopay/duPinGenerateApi?msisdn="+app_data.get('msisdn')+"&partner=globocom","api_attempt":"1"}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_RESPONSE")
	util.execute_request(**kochava_tracker_1)

	time.sleep(random.randint(4,7))

	print 'Event'
	event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_MANUAL_REQUEST","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"operator_name":"Du","operator_code":"2009","url":"https://wap.globocom.info/mglobopay/resendOTPForStaging","api_attempt":"1"}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_MANUAL_REQUEST")
	util.execute_request(**kochava_tracker_1)

	time.sleep(random.randint(4,7))

	print 'Event'
	event_data ={"name":"KCH_EVENT_KPI_PIN_GENERATE_MANUAL_RESPONSE","user_id":app_data.get('user_id'),"flow_attempt":"1","msisdn":app_data.get('msisdn'),"response":"false","valid_response":"TransactionPending","operator_name":"Du","operator_code":"2009","api_attempt":"2"}
	kochava_tracker_1=android_session_event(campaign_data, app_data, device_data,action='event',state_active_count=True,event_data=event_data,eventName="KCH_EVENT_KPI_PIN_GENERATE_MANUAL_RESPONSE")
	util.execute_request(**kochava_tracker_1)

	return {'status':True}
###########################################################################
#
#
#             Kochava
#
###########################################################################	
def android_Kvinit(campaign_data, app_data, device_data):
	
	header={
		'User-Agent':app_data.get('ua'),
		'Content-Type' : 'application/json; charset=UTF-8',
		'Accept-Encoding':'gzip',
	}
	data={
		'action':'init',
		"data":
			{ "usertime":str(int(time.time())),
			  "uptime":"0."+str(random.randint(100,200)),
			  "platform":"android",
			  'timezone' : device_data.get('local_tz_name'),
			  'package'  : campaign_data.get('package_name'),
			  'locale'   : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country')
			 },
		'sdk_version' : campaign_data.get('kochava').get('sdk_version'),
		'kochava_app_id': campaign_data.get('kochava').get('kochava_app_id'),
		'sdk_protocol': campaign_data.get('kochava').get('sdk_protocol'),
		'kochava_device_id': app_data.get('kochava_device_id'),
		"send_date"   : get_date(app_data,device_data),
		"nt_id"	   : app_data.get('nt_id'),
		"sdk_build_date" : campaign_data.get('kochava').get('sdk_build_date'),
	}

	app_data['previous_Call_time']=time.time()
	
	return {'url': 'http://kvinit-prod.api.kochava.com/track/kvinit', 'httpmethod': 'post', 'headers': header, 'params': None, 'data': json.dumps(data)}


def android_kvQuery( campaign_data, device_data, app_data ):
	url= "http://control.kochava.com/track/kvquery"
	method= "post"
	headers= {       
		'Content-Type' : 'application/json; charset=UTF-8',
		'User-Agent'   : app_data.get('ua'),
		'Accept-Encoding':'gzip',
        }

	app_data['uptime']=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),3)

	params= None

	data= {       
			'action': 'get_attribution',
	        'data': {       
		        "uptime":app_data.get('uptime'),
		        "usertime":int(time.time()),
	        },
	        'kochava_app_id': campaign_data.get('kochava').get('kochava_app_id'),
			'sdk_version': campaign_data.get('kochava').get('sdk_version'),
			'kochava_device_id': app_data.get('kochava_device_id'),
			"sdk_protocol": campaign_data.get('kochava').get('sdk_protocol'),
			"send_date"   : get_date(app_data,device_data),
			"nt_id"	   : app_data.get('nt_id'),
        }

	app_data['previous_Call_time']=time.time()

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data)}

def android_kvTracker(campaign_data, app_data, device_data):
	
	header={
		'Content-Type' : 'application/json; charset=UTF-8',
		'User-Agent'   : app_data.get('ua'),
		'Accept-Encoding':'gzip',
	}

	app_data['uptime']=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),3)

	data={	"action":"initial",
			'kochava_app_id': campaign_data.get('kochava').get('kochava_app_id'),
			'sdk_version': campaign_data.get('kochava').get('sdk_version'),
			'kochava_device_id': app_data.get('kochava_device_id'),
			"sdk_protocol": campaign_data.get('kochava').get('sdk_protocol'),
			"send_date"   : get_date(app_data,device_data),
			"nt_id"	   : app_data.get('nt_id'),
			"data":  {
						"conversion_data" : app_data.get('referrer') if app_data.get('referrer') else 'utm_source=(not%20set)&utm_medium=(not%20set)',
						"conversion_type" : "gplay",
						"usertime":str(int(time.time())),
						"uptime":app_data.get('uptime'),
						#"updelta":str(upnum),
						#"last_post_time":'2.'+util.get_random_string("decimal",3),
						"screen_brightness":'0.'+util.get_random_string("decimal",3),
						"device_orientation":"portrait",
						"network_conn_type":device_data.get('network'),
						"volume":1,
						"ssid":device_data.get('carrier'),
						"bssid":device_data.get('mac'),
						"carrier_name":device_data.get('carrier'),
						"adid":device_data.get('adid'),
						"device":device_data.get('model'),
						"disp_h":device_data.get('resolution').split('x')[1],
						"disp_w":device_data.get('resolution').split('x')[0],
						"package":campaign_data.get('package_name'),
						"app_version":campaign_data.get('app_name')+" "+campaign_data.get('app_version_code'),
						"app_short_string":campaign_data.get('app_version_name'),
						"android_id":device_data.get('android_id'),
						"os_version":"Android"+device_data.get('os_version'),
						"app_limit_tracking":False,
						"device_limit_tracking":False,
						"language":device_data.get('locale').get('language')+"-"+device_data.get('locale').get('country'),
						"screen_dpi":device_data.get('dpi'),
						"screen_inches":'5',
						"manufacturer":device_data.get('brand'),
						"product_name":device_data.get('product'),
						"architecture":device_data.get('build'),
						"battery_status":"discharging",
						"battery_level":app_data.get('battery'),
						"device_cores":device_data.get('cpu_core'),
						"network_metered" : False,
						# "connected_devices" : {"Keyboard" : "mtk-tpd"},
						"ui_mode"           : "Normal",
						"notification_enabled" : True,
						"is_genuine"           : True,
						"installed_date"    : int(time.time()),
						"installer_package" : 'com.android.vending',
						"state_active"      : True,
						"timezone"          : device_data.get('local_tz_name'),
						"locale"            : device_data.get('locale').get('language')+"-"+device_data.get('locale').get('country'),
						"install_referrer" : {
						                     'attempt_count' : 1,
						                     'duration' : "0.0"+str(random.randint(10,100)),
						                     'install_begin_time' : app_data.get('install_begin_time'),
						                     'referrer' : app_data.get('referrer') if app_data.get('referrer') else 'utm_source=(not%20set)&utm_medium=(not%20set)',
						                     'referrer_click_time' : app_data.get('click_time'),
						                     'status' : 'ok',
						                     },
			}
		}

	app_data['previous_Call_time']=time.time()

	
	return {'url': 'http://control.kochava.com/track/json', 'httpmethod': 'post', 'headers': header, 'params': None, 'data': json.dumps(data)}

def android_session_event(campaign_data, app_data, device_data,action='event',eventName='',event_data={},state='pause',state_active_count=False):
	
	header={
		'Content-Type' : 'application/json; charset=UTF-8',
		'User-Agent'   : app_data.get('ua'),
		'Accept-Encoding':'gzip',
	}
	app_data['uptime']=round(app_data.get('previous_Call_time')-app_data.get('call_run_time'),3)

	data={'{}':{"action":action,
			'kochava_app_id': campaign_data.get('kochava').get('kochava_app_id'),
			'sdk_version': campaign_data.get('kochava').get('sdk_version'),
			'kochava_device_id': app_data.get('kochava_device_id'),
			"sdk_protocol": campaign_data.get('kochava').get('sdk_protocol'),
			"send_date"   : get_date(app_data,device_data),
			"nt_id"	   : app_data.get('nt_id'),
			"data":  {
						"usertime":str(int(time.time())),
						"uptime":app_data.get('uptime'),
						#"updelta":str(upnum),
						#"last_post_time":'2.'+util.get_random_string("decimal",3),
						"screen_brightness":'0.'+util.get_random_string("decimal",3),
						"device_orientation":"portrait",
						"network_conn_type":device_data.get('network'),
						"volume":0,
						"ssid":device_data.get('carrier'),
						"bssid":device_data.get('mac'),
						"carrier_name":device_data.get('carrier'),
						#"adid":device_data.get('adid'),
						"device":device_data.get('model'),
						"disp_h":device_data.get('resolution').split('x')[1],
						"disp_w":device_data.get('resolution').split('x')[0],
						#"package":campaign_data.get('package_name'),
						"app_version":campaign_data.get('app_name')+" "+campaign_data.get('app_version_code'),
						"app_short_string":campaign_data.get('app_version_name'),
						#"android_id":device_data.get('android_id'),
						"os_version":"Android"+device_data.get('os_version'),
						#"app_limit_tracking":False,
						#"device_limit_tracking":False,
						#"language":device_data.get('locale').get('language')+"-"+device_data.get('locale').get('country'),
						"screen_dpi":device_data.get('dpi'),
						#"screen_inches":'5',
						"manufacturer":device_data.get('brand'),
						"product_name":device_data.get('product'),
						"architecture":device_data.get('build'),
						"battery_status":"discharging",
						"battery_level":app_data.get('battery'),
						#"device_cores":device_data.get('cpu_core'),
						# "connected_devices" : {"Keyboard": "mtk-tpd"},
						"ui_mode"           : "Normal",
						"notifications_enabled" : True,
						"locale"            : device_data.get('locale').get('language')+"-"+device_data.get('locale').get('country'),
						#"package" : campaign_data.get('package_name'),
						"state_active"      : True,
						"timezone"          : device_data.get('local_tz_name'),
						"network_metered" : False,
						"instant_app" : False,
			}
		}}
	if action == 'event':
	   data['{}']['data']['event_data'] = event_data
	   data['{}']['data']['event_name'] = eventName	
	else:
	   data['{}']['data']['state'] = state
	   if state_active_count:
	   		data['{}']['data']['state_active_count'] = 1   

	app_data['previous_Call_time']=time.time()	   		
	
	return {'url': 'http://control.kochava.com/track/json', 'httpmethod': 'post', 'headers': header, 'params': None, 'data': json.dumps(data)}

######### BRANCH CALLS ################

def api_branch_install(campaign_data,app_data,device_data):
	method='post'
	url='http://api2.branch.io/v1/install'

	headers = {
				'User-Agent'		:app_data.get('ua'),
				'Accept-Encoding': 'gzip',
				'Content-Type':'application/json',
				'Accept':'application/json',
			}

	data={       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "cd": {       "mv": "-1", "pn": campaign_data.get('package_name')},
        "country": device_data.get('locale').get('country'),
        "debug": False,
        "environment": "FULL_APP",
        "facebook_app_link_checked": False,
        "first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "instrumentation": {       "v1/install-qwt": "0"},
        "is_hardware_id_real": True,
        "is_referrable": 1,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "latest_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "latest_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "previous_update_time": 0,
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "update": 0,
        "clicked_referrer_ts":int(app_data.get('times').get('click_time')),
        "install_begin_ts":int(app_data.get('times').get('download_begin_time')),
        "wifi": True} 

	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}

def api_branch_open(campaign_data, app_data, device_data):
	url= "http://api2.branch.io/v1/open"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent":app_data.get('ua'),}

	params= None

	data= {       "app_version": campaign_data.get('app_version_name'),
        "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "cd": {       "mv": "-1", "pn": campaign_data.get('package_name')},
        "country": device_data.get('locale').get('country'),
        "debug": False,
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "environment": "FULL_APP",
        "facebook_app_link_checked": False,
        "first_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id":  app_data.get('identity_id'),
        "instrumentation": {       "v1/open-qwt": "0"},
        "is_hardware_id_real": True,
        "is_referrable": 1,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "latest_install_time": int(app_data.get('times').get('install_complete_time')*1000),
        "latest_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "previous_update_time": int(app_data.get('times').get('install_complete_time')*1000),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "update": 1,
        "wifi": True} 

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def api2_branch_profile( campaign_data, device_data, app_data, identity='',call=1):
	url= "http://api2.branch.io/v1/profile"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": app_data.get('ua'),}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),
        "brand": device_data.get('brand'),
        "country": device_data.get('country'),
        "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {       "v1/install-brtt": "1179",
                                   "v1/profile-qwt": "2043"},
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val":0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {       },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('api_branch').get('sdk'),
        "session_id": app_data.get('session_id'),
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True}

	if call == 2:
		data['instrumentation'] = {"v1/profile-qwt":"1","v1/profile-brtt":"285"}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def branch_event_standard( campaign_data, device_data, app_data, eventName ):
	url= "http://api2.branch.io/v2/event/standard"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',}

	params= None

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),

        "instrumentation": {       "v2/event/custom-brtt": "1193",
		"v2/event/standard-qwt": "2"},
        "metadata": {       },
        "name": eventName,
        "retryNumber": 0,
        "user_data": {       "aaid": device_data.get('adid'),
                             "android_id": device_data.get('android_id'),
                             "app_version": campaign_data.get('app_version_name'),
                             "brand": device_data.get('brand'),
                             "developer_identity": device_data.get('android_id'),
                             "device_fingerprint_id": app_data.get('device_fingerprint_id1'),
                             "environment": "FULL_APP",
                             "language": device_data.get('locale').get('language'),
                             "limit_ad_tracking": 0,
                             "local_ip": device_data.get('private_ip'),
                             "model": device_data.get('model'),
                             "os": "Android",
                             "os_version": int(device_data.get('sdk')),
                             "screen_dpi": int(device_data.get('dpi')),
                             "screen_height": int(device_data.get('resolution').split('x')[0]),
                             "screen_width": int(device_data.get('resolution').split('x')[1]),
                             "sdk": "android",
                             "sdk_version": campaign_data.get('api_branch').get('sdk').split('android')[1],
                             "user_agent": device_data.get('User-Agent')}}

	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def api_branch_custom(campaign_data, app_data, device_data,ename='', custom_data=''):
	url= "http://api2.branch.io/v2/event/custom"
	method= "post"
	headers= {       "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Content-Type": "application/json",
        "User-Agent": app_data.get('ua'),
        }

	params= None

	# if not app_data.get('user_id'):
	# 	developer_identity='bnc_no_value'
	# else:
	# 	developer_identity=str(app_data['user_id'])

	data= {       "branch_key": campaign_data.get('api_branch').get('branchkey'),	
        "instrumentation": {"v2/event/custom-brtt":"591","v2/event/custom-qwt":"4236"},
        "name": ename,
        "metadata": {},
        "retryNumber": 0,
        "user_data": {       "aaid": device_data.get('adid'),
                             "android_id": device_data.get('android_id'),
                             "app_version": campaign_data.get('app_version_name'),
                             "brand": device_data.get('brand'),
                             "country": device_data.get('locale').get('country'),
                             "developer_identity": device_data.get('android_id'),
                             "device_fingerprint_id": app_data['device_fingerprint_id1'],
                             "environment": "FULL_APP",
                             "language": device_data.get('locale').get('language'),
                             "limit_ad_tracking": 0,
                             "local_ip": device_data.get('private_ip'),
                             "model": device_data.get('model'),
                             "os": "Android",
                             "os_version": int(device_data.get('sdk')),
                             "screen_dpi":int(device_data.get('dpi')),
                             "screen_height": int(device_data.get('resolution').split('x')[0]),
                             "screen_width": int(device_data.get('resolution').split('x')[1]),
                             "sdk": "android",
                             "sdk_version": '3.2.0',
                             "user_agent":device_data.get('User-Agent')}}


	if custom_data:
		data['custom_data']=custom_data
	return {"url": url, "httpmethod": method, "headers": headers, "params": params, "data": json.dumps(data)}

def api_branch_close(campaign_data, app_data, device_data):	
	
	url='http://api2.branch.io/v1/close'
	header={
		'X-NewRelic-ID': app_data.get('cross_process_id'),
		'Content-Type': 'application/json',
		'Accept':'application/json',
		'User-Agent':'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')',
		'Accept-Encoding':'gzip',
	}
	params={}
	data={
			"identity_id":app_data.get('identity_id'),
			"device_fingerprint_id":app_data.get('device_fingerprint_id1'),
			"session_id":app_data.get('session_id'),
			"app_version": campaign_data.get('app_version_name'),
			'google_advertising_id': device_data.get('adid'),
			"hardware_id": device_data.get('android_id'),
			"is_hardware_id_real": True,
			"brand": device_data.get('brand').upper(),
			"model":device_data.get('model'),
			"screen_dpi": int(device_data.get('dpi')),
			"screen_height": int(device_data.get('resolution').split('x')[0]),
			"screen_width": int(device_data.get('resolution').split('x')[1]),
			"wifi": True,
			"ui_mode":"UI_MODE_TYPE_NORMAL",
			"os": "Android",
			"os_version": int(device_data.get('sdk')),		
			"country":device_data.get('locale').get('country'),
			"lat_val":0,
			"language":device_data.get('locale').get('language'),
			"local_ip":device_data.get('private_ip'),
			"metadata":{},
			"instrumentation":{"v2/event/custom-brtt":"411","v1/close-qwt":"1"},
			"sdk":campaign_data.get('api_branch').get('sdk'),
			"retryNumber":0,
			"branch_key":campaign_data.get('api_branch').get('branchkey'),	
	}

	return {'url': url, 'httpmethod': 'post', 'headers': header, 'params':params, 'data': json.dumps(data)}

##################################################################
#
#       Utility  Methods
#
##################################################################

def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def random_email(app_data,len=5):
	app_data['email'] = ''.join([random.choice(string.ascii_lowercase) for _ in range(len)])

def random_id(app_data,l1=4,l2=5):
	app_data['id'] = ''.join([random.choice(string.ascii_lowercase) for _ in range(l1)])+'-'+"".join([random.choice(string.ascii_lowercase) for _ in range(l2)])			

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'#+device_data.get('timezone')
	return date		

def def_nt_id(app_data):
	if not app_data.get('nt_id'):
		app_data['nt_id'] = random_string(5)+"-"+str(uuid.uuid4())

def set_ua(app_data,device_data):
	if not app_data.get('ua'):
		if int(device_data.get("sdk")) >19:
			app_data['ua'] = 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
		else:
			app_data['ua'] = 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'	
	

###########################################################
#
#      Neccessary  Methods
#
###########################################################
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0
	
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)
