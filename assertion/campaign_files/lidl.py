from sdk import util
from sdk import NameLists
from sdk import getsleep
import time,json,urllib
import random
import datetime
import uuid
import string
from sdk import installtimenew
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
	'package_name'		 :'de.sec.mobile',
	'app_name' 			 :'Lidl - Offers & Leaflets',
	'app_version_name'	 : '4.2.1(#44)',#'4.1.1(#42)',#4.1.0(#41)',#4.0.0(#35)',#'3.24.0(#34)',#3.23.1(#33)',#3.23.0(#32)',#3.22.1(#31)',#'3.22.0(#30)',#'3.21.0(#29)',#'3.20.0(#27)',#3.19.4(#26), 3.19.3(#25)',#'3.19.2(#24)',3.19.1(#23)',#'3.18.2(#21)',#3.16.1(#16)',3.17.0(#18), 3.20.1(#28)
	'ctr'				 : 6,
	# 'app_version_code'	 :'31700',#'31601',
	'no_referrer'		 : True,
	'supported_os'		 :'5.0',	 #4.4	
	'supported_countries':'WW',
	'device_targeting':True,
	'tracker'			 :'adjust',
	'app_size'			 : 18.8,#13.75,#15.3,#15.2,
	'night_optimzation':{
									"percentage": 70,
									"time_window": [0, 8]
								},
	'adjust':{
		'app_token'	: 'z1d6vewex3wg',
		'sdk'		: 'android4.17.0',#'android4.15.0',#'android4.14.0',	
	},
	'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
	'retention' :{
		1:70,
		2:68,
		3:66,
		4:64,
		5:61,
		6:59,
		7:57,
		8:52,
		9:50,
		10:47,
		11:45,
		12:43,
		13:40,
		14:37,
		15:35,
		16:31,
		17:30,
		18:28,
		19:26,
		20:24,
		21:21,
		22:20,
		23:17,
		24:16,
		25:15,
		26:14,
		27:13,
		28:12,
		29:11,
		30:10,
		31:9,
		32:8,
		33:7,
		34:6,
		35:5
	},
}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print 'please wait installing_______________________'
	def_sec(app_data,device_data)

	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

	click_time=int(app_data.get('times').get('click_time'))
	intall_time=int(app_data.get('times').get('install_complete_time'))

	app_data['click_time'] = datetime.datetime.utcfromtimestamp((click_time)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	app_data['install_complete_time'] = datetime.datetime.utcfromtimestamp((intall_time)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	
	app_data['adjust_call_time']=int(time.time())
	def_(app_data,'session_count')
	inc_(app_data,'session_count')
	def_(app_data,'subsession_count')
	inc_(app_data,'subsession_count')
	########################################3
	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)

	###############################################
	global click_time_sdk
	click_time_sdk = get_date(app_data,device_data)
	time.sleep(random.randint(7,10))

	time.sleep(random.randint(10,12))
	global att_created_at
	att_created_at=get_date(app_data,device_data)

	time.sleep(random.randint(1,2))
	global created_at
	created_at=get_date(app_data,device_data)
	
	time.sleep(random.randint(1,2))
	global sent_at
	sent_at=get_date(app_data,device_data)
	
	time.sleep(random.randint(1,2))
	global click_sent
	click_sent=get_date(app_data,device_data)

	time.sleep(random.randint(1,2))
	global att_sent_at
	att_sent_at=get_date(app_data,device_data)
	
	####################################################
	###################################################

	global items_list
	items_list=["18465","18528","16917","16918","16919","18621","18740","18725","18549","15360","15361","15362"]
	choose_item=[]

	###########		CALLS		################	
	

	print '\nAdjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,source='install_referrer')
	util.execute_request(**request)

	print '\nAdjust : SDK CLICK____________________________________'
	request=adjust_sdkclick(campaign_data, app_data, device_data,source='reftag')
	util.execute_request(**request)

	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,initiated_by='backend')
	util.execute_request(**request)

	print '\nAdjust : ATTRIBUTION____________________________________'
	request=adjust_attribution(campaign_data, app_data, device_data,initiated_by='sdk')
	util.execute_request(**request)

	for i in range(0,random.randint(1,2)):
		if random.randint(1,100)<=90:
			time.sleep(random.randint(40,60))
			for i in range(0,random.randint(1,3)):
				rr=random.randint(0,len(items_list)-1)

				if not items_list[rr] in choose_item:
					choose_item.append(items_list[rr])
			category_list(app_data,device_data,campaign_data,choose_item)

			if random.randint(1,100)<=95:
				time.sleep(random.randint(10,30))
				val=random.choice(choose_item)
				content_view(app_data,device_data,campaign_data,val)

			if random.randint(1,100)<=95:	
				time.sleep(random.randint(14,27))
				add_to_fav(app_data,device_data,campaign_data)

		if random.randint(1,100)<=90:
			time.sleep(random.randint(30,40))

			for i in range(0,random.randint(1,3)):
				rr=random.randint(0,len(items_list)-1)
				if not items_list[rr] in choose_item:
					choose_item.append(items_list[rr])
			search(app_data,device_data,campaign_data,choose_item)

			if random.randint(1,100)<=95:
				time.sleep(random.randint(10,30))
				val=random.choice(choose_item)
				content_view(app_data,device_data,campaign_data,val)

			if random.randint(1,100)<=90:	
				time.sleep(random.randint(5,10))
				add_to_fav(app_data,device_data,campaign_data)
							

		if random.randint(1,100)<=90:
			time.sleep(random.randint(10,30))
			download_leaflet(app_data,device_data,campaign_data)

	
	###########		FINALIZE	################
	set_appCloseTime(app_data)
	
	return {'status':True, 'installedAt':app_data.get('times').get('install_complete_time'), 'installTime': app_data.get('api_hit_time')}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############

	def_sec(app_data,device_data)
	if not app_data.get('times'):
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os="android",min_sleep=0)

		click_time=int(app_data.get('times').get('click_time'))
		intall_time=int(app_data.get('times').get('install_complete_time'))

		app_data['click_time'] = datetime.datetime.utcfromtimestamp((click_time)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
		app_data['install_complete_time'] = datetime.datetime.utcfromtimestamp((intall_time)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	


	time.sleep(random.randint(1,2))
	global created_at
	created_at=get_date(app_data,device_data)
	
	time.sleep(random.randint(1,2))
	global sent_at
	sent_at=get_date(app_data,device_data)
	
	def_(app_data,'session_count')
	inc_(app_data,'session_count')
	def_(app_data,'subsession_count')
	inc_(app_data,'subsession_count')



	##################################################################
	#################################################################

	global items_list
	items_list=["18465","18528","16917","16918","16919","18621","18740","18725","18549","15360","15361","15362"]
	choose_item=[]


	print '\nAdjust : SESSION____________________________________'
	request=adjust_session(campaign_data, app_data, device_data,isOpen=True)
	util.execute_request(**request)

	app_data['adjust_call_time']=int(time.time())

	
	for i in range(0,random.randint(1,2)):
		if random.randint(1,100)<=80:
			time.sleep(random.randint(40,60))
			for i in range(0,random.randint(1,3)):
				rr=random.randint(0,len(items_list)-1)
				if not items_list[rr] in choose_item:
					choose_item.append(items_list[rr])
			category_list(app_data,device_data,campaign_data,choose_item)

			if random.randint(1,100)<=80:
				time.sleep(random.randint(10,30))
				val=random.choice(choose_item)
				content_view(app_data,device_data,campaign_data,val)

			if random.randint(1,100)<=80:	
				time.sleep(random.randint(14,27))
				add_to_fav(app_data,device_data,campaign_data)

		if random.randint(1,100)<=80:
			time.sleep(random.randint(30,40))
			for i in range(0,random.randint(1,3)):
				rr=random.randint(0,len(items_list)-1)
				if not items_list[rr] in choose_item:
					choose_item.append(items_list[rr])
			search(app_data,device_data,campaign_data,choose_item)

			if random.randint(1,100)<=80:
				time.sleep(random.randint(10,30))
				val=random.choice(choose_item)
				content_view(app_data,device_data,campaign_data,val)

			if random.randint(1,100)<=90:	
				time.sleep(random.randint(5,10))
				add_to_fav(app_data,device_data,campaign_data)					

		if random.randint(1,100)<=80:
			time.sleep(random.randint(10,30))
			download_leaflet(app_data,device_data,campaign_data)
			
	
	return {'status':'true'}



def category_list(app_data,device_data,campaign_data,choose_item):
	print '\nAdjust : Event_____________category_list_______________________'
	partner_params=json.dumps({"criteo_p":urllib.quote(str(choose_item)),"criteo_partner_id":campaign_data.get('package_name')})
	request=adjust_event(campaign_data, app_data, device_data,'qi27nq',partner_params=partner_params)
	util.execute_request(**request)


def search(app_data,device_data,campaign_data,choose_item):
	print '\nAdjust : Event_____________search_______________________'
	partner_params=json.dumps({"criteo_p":urllib.quote(str(choose_item)),"criteo_partner_id":campaign_data.get('package_name')})
	request=adjust_event(campaign_data, app_data, device_data,'3zwu4d',partner_params=partner_params)
	util.execute_request(**request)


def content_view(app_data,device_data,campaign_data,items):
	print '\nAdjust : Event_____________contenct_view_______________________'
	partner_params=json.dumps({"criteo_p":str(items),"criteo_partner_id":campaign_data.get('package_name')})
	request=adjust_event(campaign_data, app_data, device_data,'93w687',partner_params=partner_params)
	util.execute_request(**request)


def add_to_fav(app_data,device_data,campaign_data):
	print '\nAdjust : Event_____________add to fav_______________________'
	request=adjust_event(campaign_data, app_data, device_data,'loysu1')
	util.execute_request(**request)

def download_leaflet(app_data,device_data,campaign_data):
	print '\nAdjust : Event_____________download_______________________'
	request=adjust_event(campaign_data, app_data, device_data,'xob5zq')
	util.execute_request(**request)



################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

###################################################################
# adjust_session()	: method
# parameter 		: campaign_data, app_data, device_data,isOpen
#
# Simulates Adjust's behaviour whenever the App gets open.
###################################################################
def adjust_session(campaign_data, app_data, device_data,isOpen=False):
	set_androidUUID(app_data)

	url 	= 'http://app.adjust.com/session'
	method 	= 'post'
	headers = {
		'Client-SDK'		: campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	: 'gzip',
		'Content-Type'		: 'application/x-www-form-urlencoded',
		'User-Agent'		: get_ua(device_data),
		 }

	created_at = get_date(app_data,device_data)
	time.sleep(random.randint(2,4))
	sent_at = get_date(app_data,device_data)

	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'connectivity_type'		:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'gps_adid_src'			:'service',
		'hardware_name'			:device_data.get('hardware'),
		'installed_at'			:app_data.get('install_complete_time'),
		'language'				:device_data.get('locale').get('language'),
		'mcc'					:device_data.get('mcc'),
		'mnc'					:device_data.get('mnc'),
		'needs_response_details':'1',
		'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'tracking_enabled'		:'1',
		'updated_at'			:app_data.get('install_complete_time'),
		# 'vm_isa'				:'arm64',
		}

	if isOpen:
		if not app_data.get('adjust_call_time'):
			app_data['adjust_call_time'] = int(time.time())
			
		sessionLength=int(time.time())-app_data.get('adjust_call_time')
		def_(app_data,'subsession_count')
		data['last_interval'] = get_lastInterval(app_data)
		data['session_length'] = sessionLength
		data['subsession_count'] = app_data.get('subsession_count')
		data['time_spent'] = sessionLength


	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

###################################################################
# adjust_sdkclick()	: method
# parameter 		: campaign_data, app_data, device_data
#
# Simulates Adjust's behaviour incase of In-organic install.
###################################################################
def adjust_sdkclick(campaign_data, app_data, device_data,source=''):
	set_androidUUID(app_data)
	def_sec(app_data,device_data)
	install_begin=datetime.datetime.utcfromtimestamp((app_data.get('times').get('download_begin_time'))+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	sessionLength=int(time.time())-app_data.get('adjust_call_time')

	url 	= 'http://app.adjust.com/sdk_click'
	method 	='post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
	
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'connectivity_type'		:'1',
		'click_time'			:app_data.get('click_time'),
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'gps_adid_src'			:'service',
		'hardware_name'			:device_data.get('hardware'),
		'install_begin_time'	:install_begin,
		'installed_at'			:app_data.get('install_complete_time'),
		'language'				:device_data.get('locale').get('language'),
		'mcc'					:device_data.get('mcc'),
		'mnc'					:device_data.get('mnc'),
		'needs_response_details':'1',
		'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'referrer'				:urllib.unquote(app_data.get('referrer')),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'session_length'		:sessionLength,
		'source'				:source,
		'subsession_count'		:app_data.get('subsession_count'),
		'time_spent'			:sessionLength,
		'tracking_enabled'		:'1',
		'updated_at'			:app_data.get('install_complete_time'),
		# 'vm_isa'				:'arm64',
		}
	
	if source=='reftag':
		data['raw_referrer']=urllib.unquote(app_data.get('referrer'))
		data['referrer']=urllib.unquote(app_data.get('referrer'))
		data['click_time']=click_time_sdk#app_data.get('click_time')
		data['last_interval']=int(time.time())-app_data.get('adjust_call_time')
		del data['install_begin_time']
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adjust_sdkinfo(campaign_data, app_data, device_data):
	url 	= 'http://app.adjust.com/sdk_info'
	method 	='post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	data = {
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'attribution_deeplink'	:'1',
		'created_at'			:get_date(app_data.device_data),
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'gps_adid_src'			:'service',
		'needs_response_details':'1',
		'push_token'			:get_gcmToken(app_data),
		'sent_at'				:get_date(app_data.device_data),
		'source'				:'push',
		'tracking_enabled'		:'1',

		}
	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}


###################################################################
# adjust_attribution()	: method
# parameter 			: campaign_data, app_data, device_data
#
# To acknowledge and check the nature of install
###################################################################
def adjust_attribution(campaign_data, app_data, device_data,initiated_by=''):
	url 	= 'http://app.adjust.com/attribution'
	method 	='head'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'User-Agent'		:get_ua(device_data),
		 }
	params = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'created_at'			:att_created_at,
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'gps_adid_src'			:'service',
		'initiated_by'			:initiated_by,
		'needs_response_details':'1',
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'sent_at'				:att_sent_at,
		'tracking_enabled'		:'1',
		}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':None}


###################################################################
# adjust_event(): method
# parameter 	: campaign_data, app_data, device_data,
#				  event_token,callback_params,partner_params
#
# Simulates Adjust's behaviour incase of an in-app event.
###################################################################
def adjust_event(campaign_data, app_data, device_data,event_token,partner_params=None,callback_params=None,t1=1,t2=2):
	inc_(app_data,'event_count')
	created_at=get_date(app_data,device_data)
	time.sleep(random.randint(t1,t2))
	sent_at=get_date(app_data,device_data)

	set_androidUUID(app_data)
	sessionLength=int(time.time())-app_data.get('adjust_call_time')

	url 	= 'http://app.adjust.com/event'
	method 	= 'post'
	headers = {
		'Client-SDK'		:campaign_data.get('adjust').get('sdk'),
		'Accept-Encoding'	:'gzip',
		'Content-Type'		:'application/x-www-form-urlencoded',
		'User-Agent'		:get_ua(device_data),
		 }
	params=None
	data = {
		'android_uuid'			:app_data.get('android_uuid'),
		'api_level'				:device_data.get('sdk'),
		'app_token'				:campaign_data.get('adjust').get('app_token'),
		'app_version'			:campaign_data.get('app_version_name'),
		'attribution_deeplink'	:'1',
		'connectivity_type'		:'1',
		'country'				:device_data.get('locale').get('country'),
		'cpu_type'				:device_data.get('cpu_abi'),
		'created_at'			:created_at,
		'device_manufacturer'	:device_data.get('manufacturer'),
		'device_name'			:device_data.get('model'),
		'device_type'			:device_data.get('device_type'),
		'display_height'		:device_data.get('resolution').split('x')[0],
		'display_width'			:device_data.get('resolution').split('x')[1],
		'event_count'			:app_data.get('event_count'),
		'event_token'			:event_token,
		'environment'			:'production',
		'event_buffering_enabled':'0',
		'gps_adid'				:device_data.get('adid'),
		'gps_adid_src'			:'service',
		'hardware_name'			:device_data.get('hardware'),
		'language'				:device_data.get('locale').get('language'),
		'mcc'					:device_data.get('mcc'),
		'mnc'					:device_data.get('mnc'),
		'needs_response_details':'1',
		'network_type'			:'0',
		'os_build'				:device_data.get('build'),
		'os_name'				:'android',
		'os_version'			:device_data.get('os_version'),
		'package_name'			:campaign_data.get('package_name'),
		'screen_density'		:get_screen_density(device_data),
		'screen_format'			:get_screen_format(device_data),
		'screen_size'			:'normal',
		'sent_at'				:sent_at,
		'session_count'			:app_data.get('session_count'),
		'session_length'		:sessionLength,
		'subsession_count'		:app_data.get('subsession_count'),
		'tracking_enabled'		:'1',
		'time_spent'			:sessionLength,
		# 'vm_isa'				:'arm64',
		}

	if callback_params:
		data['callback_params']	= callback_params
	if partner_params:
		data['partner_params']	= partner_params

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'android'):

	package_name = campaign_data.get('package_name');
	serial 		  = device_data.get('serial')
	agent_id 	  = Config.AGENTID
	random_number = random.randint(1,10)
	source_id 	  = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"
	st 	 = device_data.get("device_id", str(int(time.time()*1000)))
	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)

def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def get_screen_density(device_data):
	dpi = int(device_data.get('dpi'))
	if dpi >= 320:
		return 'high'
	elif dpi >= 180:
		return 'medium'
	else:
		return 'low'
		
def get_screen_format(device_data):
	resolution = device_data.get('resolution')
	b = resolution.split('x')
	c = float(b[1])/float(b[0])
	if c >= 1.77:
		return 'long'
	else:
		return 'normal'

def set_androidUUID(app_data):
	if not app_data.get('android_uuid'):
		app_data['android_uuid'] = str(uuid.uuid4())

def dev_token(app_data):
	if not app_data.get('device_token'):
		app_data['device_token']=str(uuid.uuid4())		

def u_id(app_data):
	if not app_data.get('u_id'):
		app_data['u_id']="13"+str(util.get_random_string('decimal',7))


###########################################################
# Utility methods : ADJUST
#
# Define/declare/Set/Unset/Increment various parameters 
# required by Adjust 
###########################################################	
def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def set_installedAT(app_data,device_data):
	if not app_data.get('installed_at'):
		app_data['installed_at'] =  get_date(app_data,device_data,early=random.uniform(5,10))

def set_appCloseTime(app_data):
	app_data['appCloseTime'] = int(time.time())

def check_appCloseTime(app_data):
	if not app_data.get('appCloseTime'):
		set_appCloseTime(app_data)

def get_lastInterval(app_data):
	check_appCloseTime(app_data)
	return int(time.time()) - app_data.get('appCloseTime')

def def_sessionLength(app_data,forced=False):
	if not app_data.get('sessionLength') or forced:
		app_data['sessionLength'] = 0	

def set_sessionLength(app_data,forced=False,length=0):
	def_sessionLength(app_data,forced)
	app_data['sessionLength'] += length

def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def get_auth(app_data,activity_kind,gps_adid,created_at):
	final_str=str(created_at)+str(campaign_data.get('adjust').get('secret_key'))+str(gps_adid)+str(activity_kind)
	sign=util.sha256(final_str)
	auth='Signature secret_id="'+str(campaign_data.get('adjust').get('secret_id'))+'",signature="'+str(sign)+'",algorithm="sha256",headers="created_at app_secret gps_adid activity_kind "'
	return auth

def get_gcmToken(app_data):
	if not app_data.get('push_token'):
		d=''.join(random.choice(string.digits + string.ascii_letters) for _ in range(11))
		app_data['push_token']=d+':'+'APA91b'+''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))
	return app_data.get('push_token')