# -*- coding: utf-8 -*-
from sdk import util,installtimenew
from sdk import getsleep,purchase
from sdk import util
from sdk import NameLists
import time
import random
import json
import urlparse
import string
import datetime
import uuid
import clicker
import Config

#######################################################
# Campaign Data : dictionary
#
# Contains App's predefined strings like app versions,
# package name, sdk and retention information, etc
#######################################################
campaign_data = {
			'package_name':'jp.co.cygames.Shadowverse',
			'country':[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
			'ctr':6,
			'app_name' : 'シャドウバース (Shadowverse)',
			'app_size':224.7,
			'app_id':'1050059017',
			'no_referrer': True,
			'device_targeting' : True,
			'app_version_name': '2.7.10',#'2.7.0',#'2.6.22', #2.3.2
			'supported_os':'9.0',
			'supported_countries': 'WW',
			'tracker' : 'adforce',
			'adforce':{
				'app_id'     :'5417',
				'sdk_version':'4.6.0', #4.5.0
				#'sdk'        :'2.0.3.2'
	             },
	        'purchase_variation':{
								'revenue1':'0.99',
								'revenue2':'5.99',
								'revenue3':'7.99'

						},     
			'retention':{
							1:70,
							2:68,
							3:66,
							4:64,
							5:61,
							6:59,
							7:57,
							8:52,
							9:50,
							10:47,
							11:45,
							12:43,
							13:40,
							14:37,
							15:35,
							16:31,
							17:30,
							18:28,
							19:26,
							20:20,
							21:19,
							22:18,
							23:17,
							24:16,
							25:15,
							26:14,
							27:13,
							28:12,
							29:11,
							30:10,
							31:9,
							32:8,
							33:7,
							34:6,
							35:5,
						}
		}


#######################################################
# install() : method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned for first time 
#######################################################
def install(app_data, device_data):	
	###########		INITIALIZE		############	
	print "Please wait installing..."
	installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')

	if not device_data.get('idfv_id') and not app_data.get('idfv_id'):
	    app_data['idfv_id'] = str(uuid.uuid4()).upper()
	else:
	     app_data['idfv_id'] = device_data.get('idfv_id')    
	if not device_data.get('idfa_id') and not app_data.get('idfa_id'):
		app_data['idfa_id'] = str(uuid.uuid4()).upper()
	else:
	    app_data['idfa_id'] = device_data.get('idfa_id')	

	a=str({"Version3.1":{"iad-attribution":"false"}})
	app_data['searchAds']=util.base64(a)

	app_data['install_id'] = str(uuid.uuid4()).upper()


		
	###########		CALLS		################

	print '----------------------------- adforce Analytics -----------------------------'	
	request = adforce_analytics(campaign_data, app_data, device_data,isFirstCall=True)
	app_data['api_hit_time'] = time.time()
	util.execute_request(**request)

	print '----------------------------- Ad-Force tmck -----------------------------'	
	request = adforce_tmck(campaign_data, app_data, device_data)
	result = util.execute_request(**request)
	try:
		app_data['tmck_uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]
	except:
		app_data['tmck_uid']='foxGG0L51gEYky'

	print '----------------------------- Ad-Force collect -----------------------------'	
	request = adforce_collect(campaign_data, app_data, device_data)
	util.execute_request(**request)	

	print '----------------------------- adforce fprc -----------------------------'	
	request = adforce_fprc(campaign_data, app_data, device_data)
	util.execute_request(**request)
	
	print '----------------------------- adforce fp -----------------------------'	
	request = adforce_fp(campaign_data, app_data, device_data)
	result = util.execute_request(**request)
	try:
		uid=json.loads(result.get('data'))
		app_data['fp_id'] = uid.get('id') 
		app_data['fp_tdl'] = uid.get('tdl') 
	except:
		app_data['fp_id'] = '6%2E8%2E2%3AB28CED68A5C179AC777FFA4692BA1EFC93578823%7Ctid%3AtGA0LX1gEYoY' 
		app_data['fp_tdl'] = '6831962' 

	print '----------------------------- adforce_cv-----------------------------'	
	request = adforce_cv(campaign_data, app_data, device_data)
	result = util.execute_request(**request)
	try:
		app_data['cv_uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]
	except:
		app_data['cv_uid']='3C826056%2D8AC6%2D41CA%2DB4C6%2DDD35E7B98D36'

	if random.randint(1,100)<=90:
		time.sleep(random.randint(30,60))

		print '----------------------------- adforce_cv-----------------------------'	
		request = adforce_cv2(campaign_data, app_data, device_data)
		result = util.execute_request(**request)

		print '----------------------------- adforce Analytics -----------------------------'	
		request = adforce_analytics(campaign_data, app_data, device_data)
		util.execute_request(**request)

		app_data['registration_complete']=True

	if app_data.get('registration_complete')==True and random.randint(1,100)<=90:

		time.sleep(random.randint(4*60,5*60))
		print '----------------------------- adforce Analytics -----------------------------'	
		request = adforce_analytics(campaign_data, app_data, device_data)
		util.execute_request(**request)

		print '----------------------------- adforce_cv-----------------------------'	
		request = adforce_cv2(campaign_data, app_data, device_data)
		result = util.execute_request(**request)

		app_data['tutorial_complete']=True



	
	
	
	
	###########		FINALIZE	################
		
	return {'status':'true'}


#######################################################
# Open 		: method
# parameter : app_data,device_data 
# 
# Contains method calls to simulate App's behaviour
# when the App was openned after first-open 
#######################################################
def open(app_data, device_data,day=1):	
	###########		INITIALIZE		############

	if not app_data.get('times'):
		print "Please wait installing..."
		installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios')

	if not app_data.get('idfv_id'):
		if not device_data.get('idfv_id'):
		    app_data['idfv_id'] = str(uuid.uuid4()).upper()
		else:
		     app_data['idfv_id'] = device_data.get('idfv_id')   

	if not app_data.get('idfa_id'): 
		if not device_data.get('idfa_id'):
			app_data['idfa_id'] = str(uuid.uuid4()).upper()
		else:
		    app_data['idfa_id'] = device_data.get('idfa_id')

	if not app_data.get('searchAds'):	
		a=str({"Version3.1":{"iad-attribution":"false"}})
		app_data['searchAds']=util.base64(a)

	if not app_data.get('install_id'):
		app_data['install_id'] = str(uuid.uuid4()).upper()

	

	###########		CALLS		################

	

	if not app_data.get('tmck_uid'):
		print '----------------------------- Ad-Force tmck -----------------------------'	
		request = adforce_tmck(campaign_data, app_data, device_data)
		result = util.execute_request(**request)
		try:
			app_data['tmck_uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]
		except:
			app_data['tmck_uid']='foxGG0L51gEYky'

	if not app_data.get('fp_id') or not app_data.get('fp_tdl'):

		print '----------------------------- adforce fp -----------------------------'	
		request = adforce_fp(campaign_data, app_data, device_data)
		result = util.execute_request(**request)
		try:
			uid=json.loads(result.get('data'))
			app_data['fp_id'] = uid.get('id') 
			app_data['fp_tdl'] = uid.get('tdl') 
		except:
			app_data['fp_id'] = '6%2E8%2E2%3AB28CED68A5C179AC777FFA4692BA1EFC93578823%7Ctid%3AtGA0LX1gEYoY' 
			app_data['fp_tdl'] = '6831962' 

	if not app_data.get('cv_uid'):

		print '----------------------------- adforce_cv-----------------------------'	
		request = adforce_cv(campaign_data, app_data, device_data)
		result = util.execute_request(**request)
		try:
			app_data['cv_uid']=result.get('res').headers.get('Set-Cookie').split('uid=')[2].split(';')[0]
		except:
			app_data['cv_uid']='3C826056%2D8AC6%2D41CA%2DB4C6%2DDD35E7B98D36'

	print '----------------------------- adforce Analytics -----------------------------'	
	request = adforce_analytics(campaign_data, app_data, device_data)
	util.execute_request(**request)

	if not app_data.get('registration_complete'):
		time.sleep(random.randint(30,60))

		print '----------------------------- adforce_cv-----------------------------'	
		request = adforce_cv2(campaign_data, app_data, device_data)
		result = util.execute_request(**request)

		print '----------------------------- adforce Analytics -----------------------------'	
		request = adforce_analytics(campaign_data, app_data, device_data)
		util.execute_request(**request)

		app_data['registration_complete']=True

	if not app_data.get('tutorial_complete'):

		time.sleep(random.randint(4*60,5*60))
		print '----------------------------- adforce Analytics -----------------------------'	
		request = adforce_analytics(campaign_data, app_data, device_data)
		util.execute_request(**request)

		print '----------------------------- adforce_cv-----------------------------'	
		request = adforce_cv2(campaign_data, app_data, device_data)
		result = util.execute_request(**request)

		app_data['tutorial_complete']=True

	if purchase.isPurchase(app_data,day,advertiser_demand=10):
		time.sleep(random.randint(20,30))
		make_purchase(app_data, device_data)	

	
	return {'status':'true'}

###################################################################

def make_purchase(app_data, device_data):
	pur_var=random.randint(1,100)
	if pur_var<=70:			
		product_id='com.cygames.shadowverse.crystal.tier001'
		price=campaign_data.get('purchase_variation').get('revenue1')
	elif pur_var>70 and  pur_var<=90:		
		product_id='com.cygames.shadowverse.crystal.tier006'
		price=campaign_data.get('purchase_variation').get('revenue2')
	else:
		product_id='com.cygames.shadowverse.crystal.tier008'
		price=campaign_data.get('purchase_variation').get('revenue3')

	

	print '----------------------------- adforce CV Purchase -----------------------------'	
	request = adforce_cv_short_purchase(campaign_data, app_data, device_data,revenue_val=price,product_id=product_id)
	util.execute_request(**request)

	print '----------------------------- adforce Analytics Purchase -----------------------------'	
	request = adforce_analytics(campaign_data, app_data, device_data)
	util.execute_request(**request)	


###################################################################
# appsflyer_track()	: method
# parameter 		: campaign_data, app_data, device_data,
#					  timeSinceLastCall, isFirstCall, isOpen
#
# Simulates Appsflyer's request whenever the App gets open.
###################################################################
def adforce_tmck(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/p/tmck'
	method='get'
	headers = {
		'Accept': '*/*',
		'User-Agent': 'ADMAGE SMPH SDK/iPhone/'+campaign_data.get('adforce').get('sdk_version')+'/CYZ/'+device_data.get('os_version')+'/iPhone/GL',#+device_data.get('model')+'/'
		'Accept-Language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding':'br, gzip, deflate',

	}
	params = {
				'_app':	campaign_data.get('adforce').get('app_id'),
				'_bundle_id': campaign_data.get('package_name'),
				'_bv':	campaign_data.get('app_version_name'),
				'_model': device_data.get('device_platform'),
				'_os_ver':	device_data.get('os_version'),
				'_sdk_ver':	campaign_data.get('adforce').get('sdk_version')
	}

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}
	
def adforce_analytics(campaign_data, app_data, device_data,isFirstCall=False):
	url = 'http://analytics.app-adforce.jp/fax/analytics'
	method='post'
	headers = {
		'Accept': '*/*',
		'User-Agent': 'ADMAGE SMPH SDK/iPhone/'+campaign_data.get('adforce').get('sdk_version')+'/CYZ/'+device_data.get('os_version')+'/iPhone/GL',#+device_data.get('model')+'/'
		'Content-Type': 'application/x-www-form-urlencoded',
		'Accept-Language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding':'br, gzip, deflate',
		
	}
	if not isFirstCall:
		if app_data.get('cv_uid'):
			headers['Cookie'] = 'uid='+app_data.get('cv_uid')

	data = {
				'd' :0,
				'e': 1,
				'p':'00'+util.md5(app_data.get('idfa_id')),
				'v' : campaign_data.get('adforce').get('sdk_version')
	}

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':None, 'data':data}	
	
def adforce_cv(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/p/cv'
	method='get'
	headers = {
		'Accept': '*/*',
		'User-Agent': 'ADMAGE SMPH SDK/iPhone/'+campaign_data.get('adforce').get('sdk_version')+'/CYZ/'+device_data.get('os_version')+'/iPhone/GL',#+device_data.get('model')+'/'
		'Accept-Language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding':'br, gzip, deflate',
	    'Cookie': 'uid='+app_data.get('tmck_uid'),
	}
	params = {
				'_adid':'00'+util.md5(app_data.get('idfa_id')),
				'_adte': 1,
				'_app':	campaign_data.get('adforce').get('app_id'),
				'_buid' :'',
				'_build': device_data.get('build'),
				'_bundle_id': campaign_data.get('package_name'),
				'_bv' : campaign_data.get('app_version_name'),
				'_carrier' : device_data.get('carrier'),
				'_country' : device_data.get('locale').get('country'),
				'_cv_target' : 'true',
				'_hash': util.get_random_string('hex',40),
				'_idfv' : app_data.get('idfv_id'),
				'_install_id' : app_data.get('install_id'),
				'_language' : device_data.get('locale').get('language'),
				'_model':device_data.get('device_platform'),
				'_os_ver':device_data.get('os_version'),
				'_rurl' : 'default',
				'_sdk_ver':campaign_data.get('adforce').get('sdk_version'),
				'_searchAds' : app_data.get('searchAds'),
				'_searchAdsRetryCount':'0',
				'_ua': device_data.get('User-Agent'),
				'_use_bw' : 'false',
				'_xevent' : 1,
				'_xuid'   : '',
				'_xuniq': app_data.get('idfa_id'),				
				'_xuniq_type' : 2,
				'_fpid'	: urlparse.unquote(app_data.get('fp_id')),
				'_fptdl':app_data.get('fp_tdl'),
	}
	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}

def adforce_cv2(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/p/cv'
	method='get'
	headers = {
		'Accept': '*/*',
		'User-Agent': 'ADMAGE SMPH SDK/iPhone/'+campaign_data.get('adforce').get('sdk_version')+'/CYZ/'+device_data.get('os_version')+'/iPhone/GL',#+device_data.get('model')+'/'
		'Accept-Language' : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Accept-Encoding':'br, gzip, deflate',
	    'Cookie': 'uid='+app_data.get('cv_uid'),
	}
	params = {
				'_adid':'00'+util.md5(app_data.get('idfa_id')),
				'_adte': 1,
				'_app':	campaign_data.get('adforce').get('app_id'),
				'_buid' :'218608304',
				'_carrier' : device_data.get('carrier'),
				'_install_id' : app_data.get('install_id'),
				'_model':device_data.get('device_platform'),
				'_cvpoint' : random.randint(7000,8000),
				'_rurl' : '',
				'_sdk_ver':campaign_data.get('adforce').get('sdk_version'),
				'_xtid' : app_data.get('idfv_id'),
				'_xuniq': app_data.get('idfa_id'),	
				'_build': campaign_data.get('app_version_name'),
				'_country': device_data.get('locale').get('country'),
				'_language': device_data.get('locale').get('language'),
				# 'age' : str(random.randint(20,30)),
				# 'gender' : 'male',
				# 'location' : '5',			
	}
	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}


def adforce_collect(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/view/collect_ios.html'
	
	method='get'
	
	headers = {	
				'User-Agent': device_data.get('User-Agent'),
				'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Accept-Encoding': 'br, gzip, deflate',
				'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
				
				
	}
	params = None

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

def adforce_fprc(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/i/fprc'
	method='get'
	headers = {
		'User-Agent': device_data.get('User-Agent'),
		'Accept': '*/*',
		'Referer': 'https://app-adforce.jp/ad/view/collect_ios.html',
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),

	}
	params = None	

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	
	
def adforce_fp(campaign_data, app_data, device_data):
	url = 'http://app-adforce.jp/ad/p/fp'
	method='get'
	headers = {
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'User-Agent': device_data.get('User-Agent'),		
		'Accept-Encoding': 'br, gzip, deflate',
		'Accept-Language': device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
		'Cookie': 'uid='+app_data.get('tmck_uid'),
	}
	
	params = {
				'_fpsd' : 'kWa44j1d7lY5BNvcKyAdMUDFBpBeA0fUmCbiyIkIr2oazOvjZwxoPwdKsEcpMlgebiYMpz6y8GGEDd5iD01PngnU77qZoOSix5ezdstlYyWesj2Xa4HxfeLd7FmrpwoNN5uQ4s5uQ5nrU8gPyPB94UXuGlfUmCbiyIkIr2oazOvjZwxoPwdKsEcpMlgebiYMpz6y8GGEDd5iD01PngnU77qZoOSix5ezdstlYyWesj2Xa4Hxfwvw6HVMpwoNRe9B5JuUshpDObeAuyPBDjaY2ftckuyPB884akHGOg4B3Wq.bhAq.etgnMW_DW.RdPQSuXjodUW2SBdfV09BStQs3dNk8HkdGlNI_3DivmdUWvEnHruAV0ASBdfgFY5BNlrK1BNlY0a5rVvMj5ZvjJfrHd0uX6MsTfwjOyPMyaCBBQ4U..07M'
	}	

	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}		


	
def adforce_cv_short_purchase(campaign_data, app_data, device_data,revenue_val,product_id):
	url = 'http://app-adforce.jp/ad/p/cv'
	method='get'
	headers = {
		'User-Agent': 'ADMAGE SMPH SDK/iPhone/'+campaign_data.get('adforce').get('sdk_version')+'/CYZ/'+device_data.get('os_version')+'/iPhone/GL',
	}
	params = {
				'_adid':'00'+util.md5(app_data.get('idfa_id')),
				'_adte': 1,
				'_app':	campaign_data.get('adforce').get('app_id'),
				'_build' : campaign_data.get('app_version_name'),
				'_bundle_id': campaign_data.get('package_name'),
				'_carrier' : device_data.get('carrier'),	
                '_country' : device_data.get('locale').get('country'),
				'_install_id' :	app_data.get('install_id'),
				'_language' : device_data.get('locale').get('language'),
				'_currency' : 'USD',
				'_cvpoint' : '7622',
				'_buid' : "218608304",
				'_hash': util.get_random_string('hex',40),
				'_icc' : '',
				'_local_xuniq':	'1',
				'_model':device_data.get('device_platform'),
				'_os_ver':device_data.get('os_version'),
				'_sdk_ver':campaign_data.get('adforce').get('sdk_version'),
				'_xtid' : app_data.get('idfv_id'),
				'_xuniq': app_data.get('idfa_id'),					
				'_xuniq_type' : 4,
				'_sim_carrier' : device_data.get('carrier'),
				'_sim_icc'	 : device_data.get('locale').get('country').lower(),
				'_price':revenue_val,
				'_sku':product_id,
		
	}


	data=None

	return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':data}	

#######################################################
# Utility methods : DEFAULT 
#
# Mandatory methods which should be present
#######################################################
def click(device_data=None, camp_type='market', camp_plat = 'ios'):
	package_name = campaign_data.get('app_id');
	serial = device_data.get('serial')
	agent_id = Config.AGENTID
	random_number = random.randint(1,10)
	gaid = device_data.get('idfa_id')
	source_id = ""
	if random_number < 5:
		source_id = "728"
	elif random_number < 8:
		source_id = "517"
	elif random_number < 9:
		source_id = "604"
	else:
		source_id = "386"

	st = str(int(time.time()*1000))

	link = campaign_data['link']
	link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
	return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)
def get_country():
	weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
	country_list 	 = [val for val, cnt in weighted_choices for i in range(cnt)]
	return random.choice(country_list)

def get_retention(day):
	return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0

def def_sec(app_data,device_data):
	timez = device_data.get('timezone')
	sec   = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
	if int(timez) < 0:
		sec = (-1) * sec
	if not app_data.get('sec'):
		app_data['sec'] = sec

def get_ua(device_data):
	if int(device_data.get("sdk")) >=19:
		return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
	else:
		return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def register_user(app_data,country='united states'):
	if not app_data.get('user'):
		gender 		= random.choice(['male','female'])
		first_names = NameLists.NAMES[country][gender]
		first_name 	= random.choice(first_names)
		last_names 	= NameLists.NAMES[country]['lastnames']
		last_name 	= random.choice(last_names)
		number 		= str(random.randint(100, 9000))
		symbol 		= random.choice(['', '.', '_'])
		usernamelst = [first_name, last_name, number, symbol]
		while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
			random.shuffle(usernamelst)
		username 	= ''.join(usernamelst)
		app_data['user']				={}
		app_data['user']['sex'] 		= gender
		app_data['user']['dob'] 		= util.get_random_date('1975-01-01', '2000-01-01', random.random())
		app_data['user']['firstname'] 	= first_name
		app_data['user']['lastname'] 	= last_name
		app_data['user']['username'] 	= username
		app_data['user']['email'] 		= username+"@gmail.com"


###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	
def def_deviceFingerPrintId(app_data):
	if not app_data.get('deviceFingerPrintId'):
		app_data['deviceFingerPrintId'] = 'ffffffff-'+random_string(4)+'-'+random_string(4)+'-0000-0000'+random_string(8)

def get_batteryLevel():
	return str(random.randint(10,100))+".0"

def get_afGoogleInstanceID():
	return util.get_random_string('char_all',11)


###########################################################
# Utility methods : MISC
#
###########################################################
def get_date(app_data,device_data,early=0):
	def_sec(app_data,device_data)
	date = datetime.datetime.utcfromtimestamp((time.time()-early)+app_data.get('sec')).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'+device_data.get('timezone')
	return date

def random_string(len,typ='hex'):
	if typ=='hex':
		return ''.join([random.choice("0123456789abcdef") for _ in range(len)])
	if typ=='numb':
		return ''.join([random.choice("123456789") for _ in range(len)])

def timestamp():
	return str(int(time.time()*1000))

def def_(app_data,paramName):
	if not app_data.get(paramName):
		app_data[paramName] = 0

def inc_(app_data,paramName):
	def_(app_data,paramName)
	app_data[paramName] += 1

def get_gcmToken():
	return 'APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

def catch(response,app_data,paramName):
	try:
		jsonData = json.loads(response.get('data'))
		if paramName=="appsflyer":
			app_data['installAttribution'] = jsonData
	except:
		print "Exception:: Couldn't fetch "+paramName+"'s data from response"