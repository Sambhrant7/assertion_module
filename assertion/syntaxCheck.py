import os
import sys


class runner:
    def __init__(self, campaignModule):
        self.campaignModule = campaignModule

    def start(self):
        self.performCheck()

    def performCheck(self):
        print("-"*35)
        print("Checking Syntax")
        getPath = os.getcwd()
        sys.path.insert(0, getPath + '/campaign_files')
        os.system("pyflakes campaign_files/"+fileName)

def startAssertion(campaignName):
    global fileName
    fileName= campaignName + '.py'
    getPath=os.getcwd()
    sys.path.insert(0, getPath + '/campaign_files')
    campaignModule = __import__(campaignName)
    runner(campaignModule).start()