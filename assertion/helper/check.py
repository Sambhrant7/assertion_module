#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import inspect
import time

def attributes(module, moduleList):
    print("\t *{}: ".format(module)),

    if module in moduleList:
        print("Y")
    else:
        print("N")


def checkInFile(self,source,key,get_length=[0,-1]):
    lines = inspect.getsource(source)[get_length[0]:get_length[1]]
    if key == 'return':
        if 'return' in lines and 'status' in lines and ('true' in lines or 'True' in lines):
            return True
    elif key == 'comp_plat':
        if 'ios' in self.campaignModule.__name__:
            if 'ios' in lines:
                return True
        else:
            if 'android' in lines:
                return True
    else:
        if key in lines:
            return True

def createTempFile(self):
    lines = inspect.getsource(self.campaignModule)
    lines=lines.replace('time.sleep','')
    lines=lines.replace('saveProxyConsumptionRawdata','#saveProxyConsumptionRawdata')
    lines=lines.replace('#saveProxyConsumptionRawdata(data):','saveProxyConsumptionRawdata(data):')
    lines=lines.replace('Config.AGENTID','0')    
    new = open('temp/'+self.campaignModule.__name__+'_x_.py', 'w+')
    new.write(lines)
    new.close()
    # time.sleep(25)

def createRandomnessFreeTempFile(self):
    lines = inspect.getsource(self.campaignModule)
    lines=lines.replace('time.sleep','')
    lines=lines.replace('random.randint(0,100)','random.randint(0,0)')
    lines=lines.replace('random.randint(1,100)','random.randint(0,0)')
    lines=lines.replace('saveProxyConsumptionRawdata','#saveProxyConsumptionRawdata')
    lines=lines.replace('#saveProxyConsumptionRawdata(data):','saveProxyConsumptionRawdata(data):')
    lines=lines.replace('Config.AGENTID','0')
    new = open('temp/'+self.campaignModule.__name__+'_x_.py', 'w+')
    new.write(lines)
    new.close()
    # time.sleep(25)

def removeTempFiles(self,*path):
    for x in path:
        mydir = os.getcwd()+x
        filelist = [f for f in os.listdir(mydir) if f.endswith("_x_.py") or f.endswith(".pyc")]
        for f in filelist:
            os.remove(os.path.join(mydir, f))


def blockPrint():
    sys.stdout = open(os.devnull, 'w')

def enablePrint():
    sys.stdout = sys.__stdout__
