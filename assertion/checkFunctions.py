#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys,inspect
import types
from helper import check


class runner:
    def __init__(self, campaignModule):
        self.campaignModule = campaignModule
        self.campaignMethods= self.getCampaignAttributes()

    def getCampaignAttributes(self):
        campaignAttributes = dir(self.campaignModule)
        methods = []
        for attr in campaignAttributes:
            if isinstance(getattr(self.campaignModule, attr), types.FunctionType):
                methods += [attr]
        return methods
    
    def perform_CTIT(self):
        source1=inspect.getsource(self.campaignModule)
        print "-----------------------------------\nPerforming CTIT related checks:"
        
        if not "api_hit_time" in str(source1):  
            print("\t*api_hit_time => api_hit_time present in file: N")
        else:
            print("\t*api_hit_time => api_hit_time present in file: Y")

        install_f=str(source1).split("def install(")[1].split("return {")[0]
        
        if not "installtimenew" in install_f:  
            print("\t*installtimenew => CTIT sleep module used: N")
        else:
            print("\t*installtimenew => CTIT sleep module used: Y")
            if not "app_size" in install_f:
                print("\t\t->installtimenew => APP SIZE NOT DEFINED IN CAMPAIGN DATA: N")

    def performAdjustNecessaryChecks(self,methods):

        source=inspect.getsource(self.campaignModule)
        
        if "t.appsflyer.com/api/" in source:
            track_function = source.split('t.appsflyer.com/api/')[1].split('return')[0]
            if "p_receipt" in track_function and not ".strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone')" in track_function:
                print("----------------------------------\nAppsflyer timeZone Check:-")
                print("\t ->time_zone => check time zone with date1 and date2: N")

        #DEVICENAME---> MODEL CHECK
        if not 'ios' in self.campaignModule.__name__:
            if "app.adjust.com/session" in source:
                print("\t *adjust device_name check:")
                session_function = source.split('app.adjust.com/session')[1].split('return')[0]
                if "device_name" in session_function:
                    device_name= session_function.split('device_name')[1].split(',')[0]
                    if "device_data.get('model')" in device_name or 'device_data.get("model")' in device_name:
                        print("\t\t ->device_name => model IN adjust_session(): Y")
                    else:
                        print("\t\t ->device_name => model IN adjust_session(): N")

            if "app.adjust.com/event" in source:
                event_function = source.split('app.adjust.com/event')[1].split('return')[0]
                if "device_name" in event_function:
                    device_name= event_function.split('device_name')[1].split(',')[0]
                    if "device_data.get('model')" in device_name or 'device_data.get("model")' in device_name:
                        print("\t\t ->device_name => model IN adjust_event(): Y")
                    else:
                        print("\t\t ->device_name => model IN adjust_event(): N")

            if "app.adjust.com/sdk_click" in source:    
                click_function = source.split('app.adjust.com/sdk_click')[1].split('return')[0]
                if "device_name" in click_function:
                    device_name= click_function.split('device_name')[1].split(',')[0]
                    if "device_data.get('model')" in device_name or 'device_data.get("model")' in device_name:
                        print("\t\t ->device_name => model IN adjust_sdkclick(): Y")
                    else:
                        print("\t\t ->device_name => model IN adjust_sdkclick(): N")

        #ATTRIBUTION---> HEAD CHECK

        if "adjust_attribution" in methods:
            attribution_source=inspect.getsource(self.campaignModule.adjust_attribution)
            if not '"head"' in attribution_source and not "'head'" in attribution_source:
                print("\t **Attribution executed by HEAD method: N")
            else:
                print("\t **Attribution executed by HEAD method: Y")

    def performFunctionCheck(self,functionList):
        print("-"*35)
        print("Performing mandatory Functions checks:")
        
        check.attributes("install", functionList)
        if check.checkInFile(self,self.campaignModule.install,'return',get_length=[-40,-1]):
            print("\t\t *return {'status':'true'}: Y")
        else:
            print("\t\t *return {'status':'true'}: N")
        
        check.attributes("open", functionList)
        if check.checkInFile(self,self.campaignModule.open,'return',get_length=[-40,-1]):
            print("\t\t *return {'status':'true'}: Y")
        else:
            print("\t\t *return {'status':'true'}: N")
        
        check.attributes("test", functionList)
        if "test" in functionList:
            if check.checkInFile(self,self.campaignModule.test,'return',get_length=[-40,-1]):
                print("\t\t *return {'status':'true'}: Y")
            else:
                print("\t\t *return {'status':'true'}: N")
        
        check.attributes("click", functionList)

        if check.checkInFile(self,self.campaignModule.click, 'gaid', get_length=[-200, -1]):
            print("\t\t *gaid: Y")
        else:
            print("\t\t *gaid: N")
        if 'ios' in self.campaignModule.__name__:
            if check.checkInFile(self,self.campaignModule.click, 'ios', get_length=[0, 90]):
                print("\t\t *camp_plat: Y")
            else:
                print("\t\t *camp_plat: N")
        else:
            if check.checkInFile(self,self.campaignModule.click, 'android', get_length=[0, 90]):
                print("\t\t *camp_plat: Y")
            else:
                print("\t\t *camp_plat: N")
        if check.checkInFile(self, self.campaignModule.click, 'package_name', get_length=[-150, -1]):
            print("\t\t *package_name: Y")
        else:
            print("\t\t *package_name: N")

        check.attributes("get_country", functionList)
        check.attributes("get_retention", functionList)

    def start(self):
        self.perform_CTIT()
        self.performFunctionCheck(self.campaignMethods)
        self.performAdjustNecessaryChecks(self.campaignMethods)

def startAssertion(campaignName):
    getPath=os.getcwd()
    sys.path.insert(0, getPath + '/campaign_files')
    campaignModule = __import__(campaignName)
    runner(campaignModule).start()
