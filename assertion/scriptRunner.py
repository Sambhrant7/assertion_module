import os
import sys
from helper import check
from helper import data


class runner:
    def __init__(self, campaignModule):
        self.campaignModule = campaignModule

    def runScript(self,proxy=True,randomess=True):
        if proxy:
            print("-"*35)
            print("Runnig Script")
            os.environ['http_proxy'] = "http://127.0.0.1:8888"
            os.environ['https_proxy'] = "https://127.0.0.1:8888"

        if 'ios' in self.campaignModule.__name__:
            device_data = data.device_data_list[1]
        else:
            device_data = data.device_data_list[0]

        app_data = data.app_data

        if randomess:
            check.createTempFile(self)
        else:
            check.createRandomnessFreeTempFile(self)

        getPath = os.getcwd()
        sys.path.insert(0, getPath + '/temp')
        getFile = __import__(self.campaignModule.__name__+'_x_')

        check.blockPrint()
        getFile.install(app_data,device_data)
        if proxy:
            for x in range(int(days)):
                getFile.open(app_data,device_data,x)
        check.enablePrint()

        if not proxy:
            return str(app_data)

        if proxy:
            check.removeTempFiles(self,'/' ,'/temp','/campaign_files')

    def start(self,randomess=True):
        self.runScript(randomess=randomess)


def startAssertion(campaignName,r=True):
    global days
    days=raw_input("Please Enter the No. of days you want to run open : ")
    getPath=os.getcwd()
    sys.path.insert(0, getPath + '/campaign_files')
    campaignModule = __import__(campaignName)
    runner(campaignModule).start(r)

