import sys
import subprocess


def install(package):
    subprocess.call([sys.executable, "-m", "pip", "install", package])

if __name__ == '__main__'or __name__=="init":
    package='pyflakes'
    try:
        __import__(package)
    except ImportError:
        print "Pyflakes not installed "
        print "Please wait installing Pyflakes..."
        install(package)