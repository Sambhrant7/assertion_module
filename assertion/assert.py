import os
import argparse
import checkImports
import checkFunctions
import scriptRunner
import device_dataCheck
import app_dataCheck
import campaign_dataCheck
import syntaxCheck
import unusedFunctions
import init


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--campaign", required=True)
    campaignName = str(parser.parse_args().campaign)
    choice = raw_input('''Please Enter Your Choice:"
   1: Check Script
   2: Run Script
   3: Run Script on 100% randomness
   ''')
    if choice == '1':
        checkImports.startAssertion(campaignName)
        campaign_dataCheck.startAssertion(campaignName)
        checkFunctions.startAssertion(campaignName)
        device_dataCheck.startAssertion(campaignName)
        unusedFunctions.startAssertion(campaignName)
        syntaxCheck.startAssertion(campaignName)
        app_dataCheck.startAssertion(campaignName)
    elif choice == '2':
        scriptRunner.startAssertion(campaignName)
    elif choice == '3':
        scriptRunner.startAssertion(campaignName,False)
    else:
        print "Please Enter a Valid Keyword."
