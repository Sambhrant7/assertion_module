import os
import sys
import types
from helper import check


class runner:
    def __init__(self, campaignModule):
        self.campaignModule = campaignModule
        self.campaignImports = self.getCampaignAttributes()

    def getCampaignAttributes(self):
        campaignAttributes = dir(self.campaignModule)
        methods = []
        imports = []
        for attr in campaignAttributes:
            if isinstance(getattr(self.campaignModule, attr), types.FunctionType):
                methods += [attr]
            elif isinstance(getattr(self.campaignModule, attr), types.ModuleType):
                imports += [attr]
        return imports

    def performImportCheck(self,importsList):
        print("-"*35)
        print("Performing import related checks:")
        check.attributes("installtimenew", importsList)
        check.attributes("getsleep", importsList)
        check.attributes("clicker", importsList)
        check.attributes("Config", importsList)
        check.attributes("util", importsList)

        if 'mat' in self.campaignModule.campaign_data.keys():
            check.attributes("AES", importsList)

        if check.checkInFile(self,self.campaignModule.install,'.isPurchase'):
            check.attributes("purchase",importsList)

    @staticmethod
    def getAsciiString(_str):
        return ''.join(str(ord(c)) for c in _str)

    def start(self):
        self.performImportCheck(self.campaignImports)


def startAssertion(campaignName):
    getPath=os.getcwd()
    sys.path.insert(0, getPath + '/campaign_files')
    campaignModule = __import__(campaignName)
    runner(campaignModule).start()