#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os
import sys
import types

from helper import check


class runner:
    def __init__(self, campaignModule):
        self.campaignModule = campaignModule
        self.campaignMethods, self.campaignImports = self.getCampaignAttributes()

    def getCampaignAttributes(self):
        campaignAttributes = dir(self.campaignModule)
        methods = []
        imports = []
        for attr in campaignAttributes:
            if isinstance(getattr(self.campaignModule, attr), types.FunctionType):
                methods += [attr]
            elif isinstance(getattr(self.campaignModule, attr), types.ModuleType):
                imports += [attr]
        return methods, imports

    def performImportCheck(self,importsList):
        all_keys = []
        tracker_keys = []
        mendatory = ['app_name','app_version_code','app_version_name','country','ctr','package_name','retention','supported_countries','supported_os','tracker','device_targeting']
        tracker = ['adjust','appsflyer','kochava','mat','apsalar','ad-brix','adforce','tenjin','branch']
        adjust = ['app_token','sdk','secret_key','secret_id']
        appsflyer = ['key','dkh','buildnumber','version']
        mat = ['advertiser_id','iv','key','sdk','ver','version']
        kochava = ['kochava_app_id','sdk_version','sdk_protocol','x_unity_version','sdk_build_date']
        apsalar = ['version','key']
        adbrix = ['app_key','ver','key_getreferral','iv_getreferral','key_tracking','iv_tracking']
        adforce = ['app_id','sdk_version']
        tenjin = ['sdk','apikey']
        branch = ['sdk','key']

        if 'ios' in self.campaignModule.__name__:
            branch.append('latest_update_time')

        print("-"*35)
        print("Performing campaign_data related checks:")
        all_keys = self.campaignModule.campaign_data.keys()
        if 'supported_os' in all_keys:
            SupportedOs= self.campaignModule.campaign_data.get('supported_os')

        if 'ad-brix' in all_keys:
            key_tracking = self.campaignModule.campaign_data.get('ad-brix').get('key_tracking')

            if 'key_tracking' in adbrix and key_tracking!='srkterowgawrsozerruly82nfij625w9' and key_tracking!=None:
                print ("key_tracking is incorrect")

            iv_tracking = self.campaignModule.campaign_data.get('ad-brix').get('iv_tracking')

            if 'iv_tracking' in adbrix and iv_tracking!='srkterowgawrsoze' and iv_tracking!=None:
                print ("iv_tracking is incorrect")

        if 'mat' in all_keys:
            if not self.campaignModule.campaign_data.get('mat').get('ver') and self.campaignModule.campaign_data.get('mat').get('version'):
                mat.remove('ver')
            else:
                if self.campaignModule.campaign_data.get('mat').get('ver'):
                    mat.remove('version')
                else:
                    mat.remove('ver')


        if 'appsflyer' in all_keys:
            mendatory.append('CREATE_DEVICE_MODE')

        if 'CREATE_DEVICE_MODE' in mendatory:
            CREATE_DEVICE_MODE = self.campaignModule.campaign_data.get('CREATE_DEVICE_MODE')

            camapaign_check('CREATE_DEVICE_MODE',mendatory)

            if 'CREATE_DEVICE_MODE' in mendatory and CREATE_DEVICE_MODE!=3 and CREATE_DEVICE_MODE!=None:
                print ("CREATE_DEVICE_MODE should be equal to 3")
        
        if not self.campaignModule.campaign_data.get('retention').get(35):
            print ("\t **Retention day 35 not defined in Campaign Data: N")

        if not self.campaignModule.campaign_data.get('tracker'):
            print ("\t **No TRACKER NAME defined in Campaign Data: N")
        elif not self.campaignModule.campaign_data.get('tracker').lower() in tracker:
            print ("\t **Invalid TRACKER NAME defined in Campaign Data: N")

        if 'ios' in self.campaignModule.__name__:
            camapaign_check('app_id',all_keys)
            if 'supported_os' in all_keys and float(SupportedOs)<7.0:
                print ("Supported_Os is less than 7.0")
        else:
            camapaign_check('no_referrer',all_keys)
            if 'supported_os' in all_keys and float(SupportedOs)<4.4:
                print ("Supported_Os is less than 4.4")

        for i in mendatory:
            camapaign_check(i, all_keys)


        for i in tracker:
            check_true(i, all_keys)
            if i in all_keys:
                tracker_keys = self.campaignModule.campaign_data.get(i).keys()

                if i == 'adjust':
                    for x in adjust:
                        inside_check(x, tracker_keys)
                if i == 'apsalar':
                    for x in apsalar:
                        inside_check(x,tracker_keys )
                if i == 'appsflyer':
                    for x in appsflyer:
                        inside_check(x,tracker_keys)
                if i == 'kochava':
                    for x in kochava:
                        inside_check(x,tracker_keys)
                if i == 'mat':
                    for x in mat:
                        inside_check(x,tracker_keys)
                if i == 'ad-brix':
                    for x in adbrix:
                        inside_check(x,tracker_keys)
                if i == 'adforce':
                    for x in adforce:
                        inside_check(x,tracker_keys)
                if i == 'tenjin':
                    for x in tenjin:
                        inside_check(x,tracker_keys)
                if i == 'branch':
                    for x in branch:
                        inside_check(x,tracker_keys)

            tracker_keys = []


    def start(self):
        self.performImportCheck(self.campaignImports)

def camapaign_check(module, moduleList):
    if module in moduleList:
        pass
    else:
        print("\t *{}: ".format(module)),
        print("N")

def check_true(module, moduleList):
    if module not in moduleList:
        pass
    else:
        print("\t *{}: ".format(module)),
        print("Y")

def inside_check(module, moduleList):
    if module in moduleList:
        print("\t\t *{}: ".format(module)),
        print("Y")
    else:
        print("\t\t *{}: ".format(module)),
        print("N")

def startAssertion(campaignName):
    getPath=os.getcwd()
    sys.path.insert(0, getPath + '/campaign_files')
    campaignModule = __import__(campaignName)
    runner(campaignModule).start()



