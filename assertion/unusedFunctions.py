#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys,inspect
import types
from helper import check


class runner:
    def __init__(self, campaignModule):
        self.campaignModule = campaignModule
        self.campaignMethods= self.getCampaignAttributes()

    def getCampaignAttributes(self):
        campaignAttributes = dir(self.campaignModule)
        methods = []
        for attr in campaignAttributes:
            if isinstance(getattr(self.campaignModule, attr), types.FunctionType):
                methods += [attr]
        return methods
    
    def checkUnusedFunctions(self):
        source = inspect.getsourcelines(self.campaignModule)
        print "-----------------------------------\nChecking for unused functions defined in script:"
        unusedFunctions=[]
        for methods in self.campaignMethods:
        	if methods not in ["get_country","get_retention"]:
	            if str(source).count(str(methods)+"(")==1:
	            	unusedFunctions.append(str(methods))
    	
    	if len(unusedFunctions)>0:
	    	print("\t*: The following functions are defined but not used:-")
	    	for un in unusedFunctions:
		    	print("\t-->"+un)

    def start(self):
        self.checkUnusedFunctions()

def startAssertion(campaignName):
    getPath=os.getcwd()
    sys.path.insert(0, getPath + '/campaign_files')
    campaignModule = __import__(campaignName)
    runner(campaignModule).start()
