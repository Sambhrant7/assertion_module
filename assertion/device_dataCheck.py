# -*- coding: utf-8 -*-
import os
import sys
import inspect
from helper import data


class runner:
    def __init__(self, campaignModule):
        self.campaignModule = campaignModule

    def start(self):
        self.performCheck()

    def performCheck(self):
        print("-"*35)
        print("Performing device_data checks:")
        if 'ios' in self.campaignModule.__name__:
            allKeys = data.device_data_list[1]
        else:
            allKeys = data.device_data_list[0]

        lines = inspect.getsource(self.campaignModule)
        new=[]
        ll=lines.split("device_data.get('")
        for x in range(1, len(ll)):
            ke=ll[x].split("')")[0]
            if not "," in ke:
                new.append(ke)
            else:
                try:
                    new.append((ll[x].split("',")[0]).strip(" "))
                except:
                    try:
                        new.append((ll[x].split("' ,")[0]).strip(" "))
                    except:
                        pass


        ll=lines.split('device_data.get("')
        for x in range(1, len(ll)):
            kee=ll[x].split('")')[0]
            if not "," in kee:
                new.append(kee)
            else:
                try:
                    new.append((ll[x].split('",')[0]).strip(" "))
                except:
                    try:
                        new.append((ll[x].split('" ,')[0]).strip(" "))
                    except:
                        pass
                        
        ll=lines.split("device_data['")
        for x in range(1, len(ll)):
            new.append((ll[x].split("']")[0]))

        ll=lines.split('device_data["')
        for x in range(1, len(ll)):
            new.append((ll[x].split('"]')[0]))

        print '\nKeys not found in device_data'
        flag = False
        for x in set(new):
            if x not in allKeys:
                flag=True
                print '\t*'+x
        if not flag:
            print '\t* No such key found'

def startAssertion(campaignName):
    getPath=os.getcwd()
    sys.path.insert(0, getPath + '/campaign_files')
    campaignModule = __import__(campaignName)
    runner(campaignModule).start()